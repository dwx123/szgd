package com.szgd.aop;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

@SuppressWarnings("deprecation")
public class CustomLoginUrlAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {

	 public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
	            throws IOException, ServletException {
	        if(isAjaxRequest(request)){
	        	HttpSession session = request.getSession();
	        	response.setHeader("sessionStatus", "timeout");
	            response.sendError(601, "session timeout.");
	        }
	        super.commence(request, response, authException);
	    }
	 
	    public static boolean isAjaxRequest(HttpServletRequest request) {
	        String ajaxFlag = request.getHeader("X-Requested-With");
	        return ajaxFlag != null && "XMLHttpRequest".equals(ajaxFlag);
	    }
}