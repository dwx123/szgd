package com.szgd.aop;

import java.lang.reflect.Method;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.szgd.util.SetLog;
import com.szgd.util.SetLogBefore;
import com.szgd.util.UserUtil;

@Component
@Aspect
public class getLogAspect {

	Logger logger = Logger.getLogger("record");

	@Pointcut("@annotation(com.szgd.util.SetLog)")
	public void getLog() {

	}
	
	@Pointcut("@annotation(com.szgd.util.SetLogBefore)")
	public void getLogBefore() {

	}

	@After("getLog()")
	public void getLogAfter(JoinPoint joinPoint) throws NoSuchMethodException, SecurityException {
		Signature signature = joinPoint.getSignature();
		MethodSignature methodSignature = (MethodSignature) signature;
		Method method = joinPoint.getTarget().getClass().getDeclaredMethod(signature.getName(),
				methodSignature.getMethod().getParameterTypes());

		// 接收到请求，记录请求内容
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = attributes.getRequest();

		SetLog annotation = method.getAnnotation(SetLog.class);
		if(annotation != null){
			MDC.put("ip", request.getLocalAddr());
			MDC.put("opt", annotation.opt());
			MDC.put("account",  UserUtil.getUserInfomationBeanFromSession(request.getSession()).getLoginId());
			MDC.put("username", UserUtil.getUserInfomationBeanFromSession(request.getSession()).getUsername());
			MDC.put("classname", joinPoint.getTarget().getClass().getName());
			MDC.put("methodname", method.getName());
			MDC.put("param", joinPoint.getArgs().toString());
			logger.info("用户："+MDC.get("username")+"在"+new Date()+"进行了"+annotation.opt()+"操作。");
		}
	}
	
	@Before("getLogBefore()")
	public void getLogBefore(JoinPoint joinPoint) throws NoSuchMethodException, SecurityException {
		Signature signature = joinPoint.getSignature();
		MethodSignature methodSignature = (MethodSignature) signature;
		Method method = joinPoint.getTarget().getClass().getDeclaredMethod(signature.getName(),
				methodSignature.getMethod().getParameterTypes());

		// 接收到请求，记录请求内容
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = attributes.getRequest();

		SetLogBefore annotation = method.getAnnotation(SetLogBefore.class);
		if(annotation != null){
			MDC.put("ip", request.getLocalAddr());
			MDC.put("opt", annotation.opt());
			MDC.put("account",  UserUtil.getUserInfomationBeanFromSession(request.getSession()).getLoginId());
			MDC.put("username", UserUtil.getUserInfomationBeanFromSession(request.getSession()).getUsername());
			MDC.put("classname", joinPoint.getTarget().getClass().getName());
			MDC.put("methodname", method.getName());
			MDC.put("param", joinPoint.getArgs().toString());
			logger.info("用户："+MDC.get("username")+"在"+new Date()+"进行了"+annotation.opt()+"操作。");
		}
	}
}
