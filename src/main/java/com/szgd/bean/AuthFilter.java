package com.szgd.bean;

import java.io.Serializable;

public class AuthFilter implements Serializable {
   

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String CLASS_NAME;
	private String METHOD_NAME;
	private String COLUMN_NAME;
	private String FILTER_TYPE;
	public String getCLASS_NAME() {
		return CLASS_NAME;
	}
	public void setCLASS_NAME(String cLASS_NAME) {
		CLASS_NAME = cLASS_NAME;
	}
	public String getMETHOD_NAME() {
		return METHOD_NAME;
	}
	public void setMETHOD_NAME(String mETHOD_NAME) {
		METHOD_NAME = mETHOD_NAME;
	}
	public String getCOLUMN_NAME() {
		return COLUMN_NAME;
	}
	public void setCOLUMN_NAME(String cOLUMN_NAME) {
		COLUMN_NAME = cOLUMN_NAME;
	}
	public String getFILTER_TYPE() {
		return FILTER_TYPE;
	}
	public void setFILTER_TYPE(String fILTER_TYPE) {
		FILTER_TYPE = fILTER_TYPE;
	}
	
}