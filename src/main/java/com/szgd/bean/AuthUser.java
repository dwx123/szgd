package com.szgd.bean;

import java.io.Serializable;

public class AuthUser implements Serializable {
   

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String LOGIN_ID;
	private String USER_NAME;
	private String DEPT_CD;
	public String getLOGIN_ID() {
		return LOGIN_ID;
	}
	public void setLOGIN_ID(String lOGIN_ID) {
		LOGIN_ID = lOGIN_ID;
	}
	public String getUSER_NAME() {
		return USER_NAME;
	}
	public void setUSER_NAME(String uSER_NAME) {
		USER_NAME = uSER_NAME;
	}
	public String getDEPT_CD() {
		return DEPT_CD;
	}
	public void setDEPT_CD(String dEPT_CD) {
		DEPT_CD = dEPT_CD;
	}
	
	
}