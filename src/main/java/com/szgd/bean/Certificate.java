package com.szgd.bean;

import org.springframework.web.multipart.MultipartFile;

public class Certificate {

    private MultipartFile[] certificateAttach;

    private Integer id;

    private Integer userId;

    private String parentId;

    private String name;

    private String path;

    private String type;

    private String source;

    private String creator;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public MultipartFile[] getCertificateAttach() {
        return certificateAttach;
    }

    public void setCertificateAttach(MultipartFile[] certificateAttach) {
        this.certificateAttach = certificateAttach;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
