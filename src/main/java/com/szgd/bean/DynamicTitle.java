package com.szgd.bean;

import java.io.Serializable;

public class DynamicTitle implements Serializable {
   

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String data;
	private String title;
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	

}