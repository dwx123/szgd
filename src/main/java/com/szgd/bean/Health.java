package com.szgd.bean;

public class Health {
    private Integer id;
    private String PERSONNEL_ID;
    private String HOSPITAL;
    private String PHYSICAL_DATE;
    private String PHYSICAL_RESULT;
    private String REMARK;
    private String CREATOR;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPERSONNEL_ID() {
        return PERSONNEL_ID;
    }

    public void setPERSONNEL_ID(String PERSONNEL_ID) {
        this.PERSONNEL_ID = PERSONNEL_ID;
    }

    public String getHOSPITAL() {
        return HOSPITAL;
    }

    public void setHOSPITAL(String HOSPITAL) {
        this.HOSPITAL = HOSPITAL;
    }

    public String getPHYSICAL_DATE() {
        return PHYSICAL_DATE;
    }

    public void setPHYSICAL_DATE(String PHYSICAL_DATE) {
        this.PHYSICAL_DATE = PHYSICAL_DATE;
    }

    public String getPHYSICAL_RESULT() {
        return PHYSICAL_RESULT;
    }

    public void setPHYSICAL_RESULT(String PHYSICAL_RESULT) {
        this.PHYSICAL_RESULT = PHYSICAL_RESULT;
    }

    public String getREMARK() {
        return REMARK;
    }

    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    public String getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(String CREATOR) {
        this.CREATOR = CREATOR;
    }
}
