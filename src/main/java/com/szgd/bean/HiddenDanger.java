package com.szgd.bean;

import org.springframework.web.multipart.MultipartFile;

public class HiddenDanger {
    private Long siteId;//车站
    private String description;//隐患描述
    private Long reportPerson;//上报人
    private String creator;//创建人
    private Long rectifyPerson;//整改人
    private Long rectifyDeadline;//整改期限
    private String theme;//隐患主题
    private String findTime;//隐患发现时间
    private String rectifyBeginTime;//上报时间
    private String rectifyTime;//整改时间
    private String status;//隐患当前进度状态
    private String uploader;//更新人
    private String siteName;

    private Long id;
    private String rectifyContent;
    private String rectifyAttachId;

    private MultipartFile[] picAttach;

    public MultipartFile[] getPicAttach() {
        return picAttach;
    }

    public void setPicAttach(MultipartFile[] picAttach) {
        this.picAttach = picAttach;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public String getRectifyTime() {
        return rectifyTime;
    }

    public void setRectifyTime(String rectifyTime) {
        this.rectifyTime = rectifyTime;
    }

    public String getRectifyAttachId() {
        return rectifyAttachId;
    }

    public void setRectifyAttachId(String rectifyAttachId) {
        this.rectifyAttachId = rectifyAttachId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRectifyContent() {
        return rectifyContent;
    }

    public void setRectifyContent(String rectifyContent) {
        this.rectifyContent = rectifyContent;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getReportPerson() {
        return reportPerson;
    }

    public void setReportPerson(Long reportPerson) {
        this.reportPerson = reportPerson;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getRectifyPerson() {
        return rectifyPerson;
    }

    public void setRectifyPerson(Long rectifyPerson) {
        this.rectifyPerson = rectifyPerson;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getFindTime() {
        return findTime;
    }

    public void setFindTime(String findTime) {
        this.findTime = findTime;
    }

    public String getRectifyBeginTime() {
        return rectifyBeginTime;
    }

    public void setRectifyBeginTime(String rectifyBeginTime) {
        this.rectifyBeginTime = rectifyBeginTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getRectifyDeadline() {
        return rectifyDeadline;
    }

    public void setRectifyDeadline(Long rectifyDeadline) {
        this.rectifyDeadline = rectifyDeadline;
    }
}
