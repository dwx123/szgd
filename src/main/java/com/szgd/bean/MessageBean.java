package com.szgd.bean;

import com.google.gson.Gson;

public class MessageBean {
    private String errmsg;
    private String msg;
    private String busId;
	public String getErrmsg() {
		return errmsg;
	}
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public final static String json(MessageBean mb){
    	return new Gson().toJson(mb);
	}
	public String getBusId() {
		return busId;
	}
	public void setBusId(String busId) {
		this.busId = busId;
	}
    
}