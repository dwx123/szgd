package com.szgd.bean;

public class PersonnelInfo {
    private String contractNumber;
    private String name;
    private String sex;
    private String idNumber;
    private String address;
    private String educationName;
    private String tel;
    private String workTypeName;
    private String companyName;
    private String isResidence;
    private String inDate;
    private String outDate;
    private String lastPhysicalDate;
    private String lastPhysicalResultName;
    private String lastTrainDate;
    private int lastTrainScore;

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idNumber;
    }

    public void setIdCard(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEducationName() {
        return educationName;
    }

    public void setEducationName(String educationName) {
        this.educationName = educationName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getWorkTypeName() {
        return workTypeName;
    }

    public void setWorkTypeName(String workTypeName) {
        this.workTypeName = workTypeName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getInDate() {
        return inDate;
    }

    public void setInDate(String inDate) {
        this.inDate = inDate;
    }

    public String getOutDate() {
        return outDate;
    }

    public void setOutDate(String outDate) {
        this.outDate = outDate;
    }

    public String getLastPhysicalDate() {
        return lastPhysicalDate;
    }

    public void setLastPhysicalDate(String lastPhysicalDate) {
        this.lastPhysicalDate = lastPhysicalDate;
    }

    public String getLastPhysicalResultName() {
        return lastPhysicalResultName;
    }

    public void setLastPhysicalResultName(String lastPhysicalResultName) {
        this.lastPhysicalResultName = lastPhysicalResultName;
    }

    public String getLastTrainDate() {
        return lastTrainDate;
    }

    public void setLastTrainDate(String lastTrainDate) {
        this.lastTrainDate = lastTrainDate;
    }

    public int getLastTrainScore() {
        return lastTrainScore;
    }

    public void setLastTrainScore(int lastTrainScore) {
        this.lastTrainScore = lastTrainScore;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIsResidence() {
        return isResidence;
    }

    public void setIsResidence(String isResidence) {
        this.isResidence = isResidence;
    }
}
