package com.szgd.bean;

import org.springframework.web.multipart.MultipartFile;

public class PmFile {
    private MultipartFile[] picAttach;
    private Long id;
    private String creator;
    private String fileName;
    private String fileType;
    private String createTime;

    public MultipartFile[] getPicAttach() {
        return picAttach;
    }

    public void setPicAttach(MultipartFile[] picAttach) {
        this.picAttach = picAttach;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
