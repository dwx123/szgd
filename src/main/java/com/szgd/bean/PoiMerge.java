package com.szgd.bean;

import java.io.Serializable;

/**
 * POI Excel报表导出，列合并实体<br>
 * 
 * @author WQ
 *
 */
public class PoiMerge implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String content;  
      
    private String oldContent;  
  
    private int rowIndex;  
  
    private int cellIndex;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOldContent() {
        return oldContent;
    }

    public void setOldContent(String oldContent) {
        this.oldContent = oldContent;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getCellIndex() {
        return cellIndex;
    }

    public void setCellIndex(int cellIndex) {
        this.cellIndex = cellIndex;
    }

    public PoiMerge() {
    }

    public PoiMerge(String content, String oldContent, int rowIndex,
            int cellIndex) {
        this.content = content;
        this.oldContent = oldContent;
        this.rowIndex = rowIndex;
        this.cellIndex = cellIndex;
    }

    @Override
    public String toString() {
        return "PoiModel [content=" + content + ", oldContent=" + oldContent
                + ", rowIndex=" + rowIndex + ", cellIndex=" + cellIndex + "]";
    }  
  
}