package com.szgd.bean;

import java.util.Date;

public class Remind {
    /**
     * 序列号
     */
    private Long id;

    /**
     * 业务表ID
     */
    private Long businessId;

    /**
     * 提醒配置表ID
     */
    private Long configureId;

    /**
     * 站点
     */
    private Long siteId;

    /**
     * 站点
     */
    private String siteName;

    /**
     * 提醒字段1
     */
    private String field1;

    /**
     * 提醒字段2
     */
    private String field2;

    /**
     * 提醒字段3
     */
    private String field3;

    /**
     * 提醒字段4
     */
    private String field4;

    /**
     * 提醒字段5
     */
    private String field5;

    /**
     * 提醒字段6
     */
    private String field6;

    /**
     * 提醒字段7
     */
    private String field7;

    /**
     * 提醒字段8
     */
    private String field8;

    /**
     * 提醒字段9
     */
    private String field9;

    /**
     * 提醒字段10
     */
    private String field10;

    /**
     * 提醒类型
     */
    private String remindType;
    private String remindTypeStr;

    /**
     * 提醒时间
     */
    private Date remindTime;
    
    private String remindTimeSxStr;
    
    private String remindTimeStr;

    /**
     * 提醒内容
     */
    private String content;

    /**
     * 创建时间
     */
    private Date createTime;
    private String createTimeStr;
    /**
     * 创建人
     */
    private String creator;

    /**
     * 更新时间
     */
    private Date updateTime;
    private String updateTimeStr;

    /**
     * 更新人
     */
    private String uploader;

    /**
     * 删除标记
     */
    private Long delflag;

    //实际在场人数
    private  Long realPresentNumber;
    //要求岗位
    private  String workType;

    //要求岗位
    private  String workTypeName;

    //实际在场天数
    private  Long realPresentDays;

    /**
     * 车辆
     */
    private Long vehicleId;

    /**
     * 通过时间
     */
    private Date passTime;
    private String passTimeStr;

    /**
     * 提醒是否达到要求
     */
    private int isOk;
    /**
     * 序列号
     * @return ID 序列号
     */
    public Long getId() {
        return id;
    }

    /**
     * 序列号
     * @param id 序列号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 业务表ID
     * @return BUSINESS_ID 业务表ID
     */
    public Long getBusinessId() {
        return businessId;
    }

    /**
     * 业务表ID
     * @param businessId 业务表ID
     */
    public void setBusinessId(Long businessId) {
        this.businessId = businessId;
    }

    /**
     * 提醒配置表ID
     * @return CONFIGURE_ID 提醒配置表ID
     */
    public Long getConfigureId() {
        return configureId;
    }

    /**
     * 提醒配置表ID
     * @param configureId 提醒配置表ID
     */
    public void setConfigureId(Long configureId) {
        this.configureId = configureId;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     * 提醒字段1
     * @return FIELD_1 提醒字段1
     */
    public String getField1() {
        return field1;
    }

    /**
     * 提醒字段1
     * @param field1 提醒字段1
     */
    public void setField1(String field1) {
        this.field1 = field1 == null ? null : field1.trim();
    }

    /**
     * 提醒字段2
     * @return FIELD_2 提醒字段2
     */
    public String getField2() {
        return field2;
    }

    /**
     * 提醒字段2
     * @param field2 提醒字段2
     */
    public void setField2(String field2) {
        this.field2 = field2 == null ? null : field2.trim();
    }

    /**
     * 提醒字段3
     * @return FIELD_3 提醒字段3
     */
    public String getField3() {
        return field3;
    }

    /**
     * 提醒字段3
     * @param field3 提醒字段3
     */
    public void setField3(String field3) {
        this.field3 = field3 == null ? null : field3.trim();
    }

    /**
     * 提醒字段4
     * @return FIELD_4 提醒字段4
     */
    public String getField4() {
        return field4;
    }

    /**
     * 提醒字段4
     * @param field4 提醒字段4
     */
    public void setField4(String field4) {
        this.field4 = field4 == null ? null : field4.trim();
    }

    /**
     * 提醒字段5
     * @return FIELD_5 提醒字段5
     */
    public String getField5() {
        return field5;
    }

    /**
     * 提醒字段5
     * @param field5 提醒字段5
     */
    public void setField5(String field5) {
        this.field5 = field5 == null ? null : field5.trim();
    }

    /**
     * 提醒字段6
     * @return FIELD_6 提醒字段6
     */
    public String getField6() {
        return field6;
    }

    /**
     * 提醒字段6
     * @param field6 提醒字段6
     */
    public void setField6(String field6) {
        this.field6 = field6 == null ? null : field6.trim();
    }

    /**
     * 提醒字段7
     * @return FIELD_7 提醒字段7
     */
    public String getField7() {
        return field7;
    }

    /**
     * 提醒字段7
     * @param field7 提醒字段7
     */
    public void setField7(String field7) {
        this.field7 = field7 == null ? null : field7.trim();
    }

    /**
     * 提醒字段8
     * @return FIELD_8 提醒字段8
     */
    public String getField8() {
        return field8;
    }

    /**
     * 提醒字段8
     * @param field8 提醒字段8
     */
    public void setField8(String field8) {
        this.field8 = field8 == null ? null : field8.trim();
    }

    /**
     * 提醒字段9
     * @return FIELD_9 提醒字段9
     */
    public String getField9() {
        return field9;
    }

    /**
     * 提醒字段9
     * @param field9 提醒字段9
     */
    public void setField9(String field9) {
        this.field9 = field9 == null ? null : field9.trim();
    }

    /**
     * 提醒字段10
     * @return FIELD_10 提醒字段10
     */
    public String getField10() {
        return field10;
    }

    /**
     * 提醒字段10
     * @param field10 提醒字段10
     */
    public void setField10(String field10) {
        this.field10 = field10 == null ? null : field10.trim();
    }

    /**
     * 提醒类型
     * @return REMIND_TYPE 提醒类型
     */
    public String getRemindType() {
        return remindType;
    }

    /**
     * 提醒类型
     * @param remindType 提醒类型
     */
    public void setRemindType(String remindType) {
        this.remindType = remindType == null ? null : remindType.trim();
    }

    /**
     * 提醒时间
     * @return REMIND_TIME 提醒时间
     */
    public Date getRemindTime() {
        return remindTime;
    }

    /**
     * 提醒时间
     * @param remindTime 提醒时间
     */
    public void setRemindTime(Date remindTime) {
        this.remindTime = remindTime;
    }

    /**
     * 提醒内容
     * @return CONTENT 提醒内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 提醒内容
     * @param content 提醒内容
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * 创建时间
     * @return CREATETIME 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    /**
     * 创建人
     * @return CREATOR 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 创建人
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    /**
     * 更新时间
     * @return UPDATETIME 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 更新时间
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateTimeStr() {
        return updateTimeStr;
    }

    public void setUpdateTimeStr(String updateTimeStr) {
        this.updateTimeStr = updateTimeStr;
    }

    /**
     * 更新人
     * @return UPLOADER 更新人
     */
    public String getUploader() {
        return uploader;
    }

    /**
     * 更新人
     * @param uploader 更新人
     */
    public void setUploader(String uploader) {
        this.uploader = uploader == null ? null : uploader.trim();
    }

    /**
     * 删除标记
     * @return DELFLAG 删除标记
     */
    public Long getDelflag() {
        return delflag;
    }

    /**
     * 删除标记
     * @param delflag 删除标记
     */
    public void setDelflag(Long delflag) {
        this.delflag = delflag;
    }

	public String getRemindTimeSxStr() {
		return remindTimeSxStr;
	}

	public void setRemindTimeSxStr(String remindTimeSxStr) {
		this.remindTimeSxStr = remindTimeSxStr;
	}

	public String getRemindTimeStr() {
		return remindTimeStr;
	}

	public void setRemindTimeStr(String remindTimeStr) {
		this.remindTimeStr = remindTimeStr;
	}

	public String getRemindTypeStr() {
		return remindTypeStr;
	}

	public void setRemindTypeStr(String remindTypeStr) {
		this.remindTypeStr = remindTypeStr;
	}

    public Long getRealPresentNumber() {
        return realPresentNumber;
    }

    public void setRealPresentNumber(Long realPresentNumber) {
        this.realPresentNumber = realPresentNumber;
    }


    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getWorkTypeName() {
        return workTypeName;
    }

    public void setWorkTypeName(String workTypeName) {
        this.workTypeName = workTypeName;
    }

    public Long getRealPresentDays() {
        return realPresentDays;
    }

    public void setRealPresentDays(Long realPresentDays) {
        this.realPresentDays = realPresentDays;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Date getPassTime() {
        return passTime;
    }

    public void setPassTime(Date passTime) {
        this.passTime = passTime;
    }

    public String getPassTimeStr() {
        return passTimeStr;
    }

    public void setPassTimeStr(String passTimeStr) {
        this.passTimeStr = passTimeStr;
    }

    public int getIsOk() {
        return isOk;
    }

    public void setIsOk(int isOk) {
        this.isOk = isOk;
    }
}