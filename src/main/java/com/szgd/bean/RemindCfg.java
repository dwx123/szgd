package com.szgd.bean;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class RemindCfg {
    /**
     * 序列号
     */
    private Long id;

    /**
     * 提前提醒天数
     */
    private Long beforeDay;

    /**
     * 开始时间
     */
    private String startTime;

    /**
     * 结束时间
     */
    private String endTime;

    /**
     * 结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
     * null
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /**
     * 在场人数
     */
    private Long presentNumber;

    /**
     * 在场天数
     */
    private Long presentDays;

    /**
     * 多个站点
     */
    private String siteIds;

    private String siteNames;

    /**
     * 岗位，多个逗号分隔
     */
    private String workType;
    private String workTypeName;
    /**
     * 多个通知角色
     */
    private String roleIds;

    private String roleNames;

    //车辆信息ID
    private String vehicleIds;

    //车辆车牌号
    private String vehiclePlateNumbers;

    //车辆类型
    private String vehicleTypes;

    //车辆类型
    private String vehicleTypeNames;
    /**
     * 提醒类型
     */
    private String remindType;

    /**
     * 有效标记
     */
    private Long effectiveflag;
    private String effectiveflagstr;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 更新时间
     */
    private Date updatetime;

    /**
     * 更新人
     */
    private String uploader;

    /**
     * 删除标记
     */
    private Long delflag;
    private String modalFlag;
    private String remindTypeStr;
    private String remindDayStr;
    /**
     * 序列号
     * @return ID 序列号
     */
    public Long getId() {
        return id;
    }

    /**
     * 序列号
     * @param id 序列号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 提前提醒天数
     * @return BEFORE_DAY 提前提醒天数
     */
    public Long getBeforeDay() {
        return beforeDay;
    }

    /**
     * 提前提醒天数
     * @param beforeDay 提前提醒天数
     */
    public void setBeforeDay(Long beforeDay) {
        this.beforeDay = beforeDay;
    }

    /**
     * 开始时间
     * @return START_TIME 开始时间
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * 开始时间
     * @param startTime 开始时间
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime == null ? null : startTime.trim();
    }

    /**
     * 结束时间
     * @return END_TIME 结束时间
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * 结束时间
     * @param endTime 结束时间
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime == null ? null : endTime.trim();
    }

    /**
     * 结束日期
     * @return START_DATE 结束日期
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * 结束日期
     * @param startDate 结束日期
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * null
     * @return END_DATE null
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * null
     * @param endDate null
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * 在场人数
     * @return OTHER_1 在场人数
     */
    public Long getPresentNumber() {
        return presentNumber;
    }

    /**
     * 在场人数
     * @param presentNumber 在场人数
     */
    public void setPresentNumber(Long presentNumber) {
        this.presentNumber = presentNumber;
    }

    public Long getPresentDays() {
        return presentDays;
    }

    public void setPresentDays(Long presentDays) {
        this.presentDays = presentDays;
    }

    /**
     * 所选站点
     * @return siteIds 所选站点
     */
    public String getSiteIds() {
        return siteIds;
    }

    /**
     * 所选站点
     * @param siteIds 所选站点
     */
    public void setSiteIds(String siteIds) {
        this.siteIds = siteIds;
    }

    /**
     * 岗位
     * @return WORK_TYPE 岗位
     */
    public String getWorkType() {
        return workType;
    }

    /**
     * 岗位
     * @param workType 岗位
     */
    public void setWorkType(String workType) {
        this.workType = workType == null ? null : workType.trim();
    }

    public String getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(String roleIds) {
        this.roleIds = roleIds;
    }

    /**
     * 提醒类型
     * @return REMIND_TYPE 提醒类型
     */
    public String getRemindType() {
        return remindType;
    }

    /**
     * 提醒类型
     * @param remindType 提醒类型
     */
    public void setRemindType(String remindType) {
        this.remindType = remindType == null ? null : remindType.trim();
    }

    /**
     * 有效标记
     * @return EFFECTIVEFLAG 有效标记
     */
    public Long getEffectiveflag() {
        return effectiveflag;
    }

    /**
     * 有效标记
     * @param effectiveflag 有效标记
     */
    public void setEffectiveflag(Long effectiveflag) {
        this.effectiveflag = effectiveflag;
    }

    /**
     * 创建时间
     * @return CREATETIME 创建时间
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * 创建时间
     * @param createtime 创建时间
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * 创建人
     * @return CREATOR 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 创建人
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    /**
     * 更新时间
     * @return UPDATETIME 更新时间
     */
    public Date getUpdatetime() {
        return updatetime;
    }

    /**
     * 更新时间
     * @param updatetime 更新时间
     */
    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    /**
     * 更新人
     * @return UPLOADER 更新人
     */
    public String getUploader() {
        return uploader;
    }

    /**
     * 更新人
     * @param uploader 更新人
     */
    public void setUploader(String uploader) {
        this.uploader = uploader == null ? null : uploader.trim();
    }

    /**
     * 删除标记
     * @return DELFLAG 删除标记
     */
    public Long getDelflag() {
        return delflag;
    }

    /**
     * 删除标记
     * @param delflag 删除标记
     */
    public void setDelflag(Long delflag) {
        this.delflag = delflag;
    }

	public String getModalFlag() {
		return modalFlag;
	}

	public void setModalFlag(String modalFlag) {
		this.modalFlag = modalFlag;
	}

	public String getRemindTypeStr() {
		return remindTypeStr;
	}

	public void setRemindTypeStr(String remindTypeStr) {
		this.remindTypeStr = remindTypeStr;
	}

	public String getRemindDayStr() {
		return remindDayStr;
	}

	public void setRemindDayStr(String remindDayStr) {
		this.remindDayStr = remindDayStr;
	}

	public String getEffectiveflagstr() {
		return effectiveflagstr;
	}

	public void setEffectiveflagstr(String effectiveflagstr) {
		this.effectiveflagstr = effectiveflagstr;
	}

    public String getSiteNames() {
        return siteNames;
    }

    public void setSiteNames(String siteNames) {
        this.siteNames = siteNames;
    }

    public String getWorkTypeName() {
        return workTypeName;
    }

    public void setWorkTypeName(String workTypeName) {
        this.workTypeName = workTypeName;
    }

    public String getRoleNames() {
        return roleNames;
    }

    public void setRoleNames(String roleNames) {
        this.roleNames = roleNames;
    }

    public String getVehicleIds() {
        return vehicleIds;
    }

    public void setVehicleIds(String vehicleIds) {
        this.vehicleIds = vehicleIds;
    }

    public String getVehiclePlateNumbers() {
        return vehiclePlateNumbers;
    }

    public void setVehiclePlateNumbers(String vehiclePlateNumbers) {
        this.vehiclePlateNumbers = vehiclePlateNumbers;
    }

    public String getVehicleTypes() {
        return vehicleTypes;
    }

    public void setVehicleTypes(String vehicleTypes) {
        this.vehicleTypes = vehicleTypes;
    }

    public String getVehicleTypeNames() {
        return vehicleTypeNames;
    }

    public void setVehicleTypeNames(String vehicleTypeNames) {
        this.vehicleTypeNames = vehicleTypeNames;
    }
}