package com.szgd.bean;


public class RemindCfgTitle {
    /**
     * datatable字段名
     */
    private String data;

    /**
     * datatable展示标题
     */
    private String title;

    /**
     * datatable样式
     */
    private String className;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

 
}