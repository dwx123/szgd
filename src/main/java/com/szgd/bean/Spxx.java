package com.szgd.bean;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Spxx {
    /**
     * 主键
     */
    private String id;

    /**
     * 经费申请id
     */
    private String jfsqid;

    /**
     * 操作人
     */
    private String czr;

    /**
     * 操作结果
     */
    private Short czjg;

    /**
     * 操作意见
     */
    private String czyj;

    /**
     * 操作时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date czsj;

    /**
     * 节点情况
     */
    private String jdqk;

    /**
     * 备注
     */
    private String bz;

    /**
     * 主键
     * @return ID 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 经费申请id
     * @return JFSQID 经费申请id
     */
    public String getJfsqid() {
        return jfsqid;
    }

    /**
     * 经费申请id
     * @param jfsqid 经费申请id
     */
    public void setJfsqid(String jfsqid) {
        this.jfsqid = jfsqid == null ? null : jfsqid.trim();
    }

    /**
     * 操作人
     * @return CZR 操作人
     */
    public String getCzr() {
        return czr;
    }

    /**
     * 操作人
     * @param czr 操作人
     */
    public void setCzr(String czr) {
        this.czr = czr == null ? null : czr.trim();
    }

    /**
     * 操作结果
     * @return CZJG 操作结果
     */
    public Short getCzjg() {
        return czjg;
    }

    /**
     * 操作结果
     * @param czjg 操作结果
     */
    public void setCzjg(Short czjg) {
        this.czjg = czjg;
    }

    /**
     * 操作意见
     * @return CZYJ 操作意见
     */
    public String getCzyj() {
        return czyj;
    }

    /**
     * 操作意见
     * @param czyj 操作意见
     */
    public void setCzyj(String czyj) {
        this.czyj = czyj == null ? null : czyj.trim();
    }

    /**
     * 操作时间
     * @return CZSJ 操作时间
     */
    public Date getCzsj() {
        return czsj;
    }

    /**
     * 操作时间
     * @param czsj 操作时间
     */
    public void setCzsj(Date czsj) {
        this.czsj = czsj;
    }

    /**
     * 节点情况
     * @return JDQK 节点情况
     */
    public String getJdqk() {
        return jdqk;
    }

    /**
     * 节点情况
     * @param jdqk 节点情况
     */
    public void setJdqk(String jdqk) {
        this.jdqk = jdqk == null ? null : jdqk.trim();
    }

    /**
     * 备注
     * @return BZ 备注
     */
    public String getBz() {
        return bz;
    }

    /**
     * 备注
     * @param bz 备注
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }
}