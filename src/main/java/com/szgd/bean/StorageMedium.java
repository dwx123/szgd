package com.szgd.bean;


public class StorageMedium {

    private String id;

    /**
     * 移动存储介质名称
     */
    private String name;

    /**
     * 移动存储介质类型
     */
    private String type;

    /**
     * 移动存储介质规格型号
     */
    private String ggxh;

    /**
     * 移动存储介质品牌
     */
    private String brand;

    /**
     * 移动存储介质机身号
     */
    private String jishenhao;

    /**
     * 移动存储介质序列号
     */
    private String xuliehao;

    /**
     * 移动存储介质状态
     */
    private String status;

    /**
     * 移动存储介质去向
     */
    private String quxiang;

    /**
     * 移动存储介质录入时间
     */
    private String date_luru;

    /**
     * 移动存储介质密级属性
     */
    private String miji;

    /**
     * 移动存储介质所属部门
     */
    private String dept;

    /**
     * 介质编号
     */
    private String bianhao;

    /**
     * 部门的Id
     */
    private Long deptId;

    /**
     * 密级属性对应字典表的id
     */
    private String mijiId;

    /**
     * 存储类型对应字典表的id
     */
    private String typeId;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getMijiId() {
        return mijiId;
    }

    public void setMijiId(String mijiId) {
        this.mijiId = mijiId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getBianhao() {
        return bianhao;
    }

    public void setBianhao(String bianhao) {
        this.bianhao = bianhao;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGgxh() {
        return ggxh;
    }

    public void setGgxh(String ggxh) {
        this.ggxh = ggxh;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getJishenhao() {
        return jishenhao;
    }

    public void setJishenhao(String jishenhao) {
        this.jishenhao = jishenhao;
    }

    public String getXuliehao() {
        return xuliehao;
    }

    public void setXuliehao(String xuliehao) {
        this.xuliehao = xuliehao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQuxiang() {
        return quxiang;
    }

    public void setQuxiang(String quxiang) {
        this.quxiang = quxiang;
    }

    public String getDate_luru() {
        return date_luru;
    }

    public void setDate_luru(String date_luru) {
        this.date_luru = date_luru;
    }

    public String getMiji() {
        return miji;
    }

    public void setMiji(String miji) {
        this.miji = miji;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }
}
