package com.szgd.bean;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class SysDict {

    /**
     * 字典的id
     */
    private String id;

    /***
     * 字典的code
     */
    private String code;

    /***
     * 字典的code
     */
    private String parentCode;

    /***
     * 字典值
     */
    private String dictValue;

    /***
     * 字典值排列序号
     */
    private int dictSort;

    /***
     * 父级的字典id
     */
    private String dictParentId;
    
    private String level;

    /**
     * 是否可用
     */
    private boolean dictAvailable;

    /***
     * 字典的描述
     */
    private String dictDesc;

    /***
     * 其他值
     */
    private String otherValue;

    /**
     * 创建人
     */
    private User user;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    /***
     * 创建日期
     */

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date optDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

    public int getDictSort() {
        return dictSort;
    }

    public void setDictSort(int dictSort) {
        this.dictSort = dictSort;
    }

    public String getDictParentId() {
        return dictParentId;
    }

    public void setDictParentId(String dictParentId) {
        this.dictParentId = dictParentId;
    }

    public boolean isDictAvailable() {
        return dictAvailable;
    }

    public void setDictAvailable(boolean dictAvailable) {
        this.dictAvailable = dictAvailable;
    }

    public String getDictDesc() {
        return dictDesc;
    }

    public void setDictDesc(String dictDesc) {
        this.dictDesc = dictDesc;
    }

    public Date getOptDate() {
        return optDate;
    }

    public void setOptDate(Date optDate) {
        this.optDate = optDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getOtherValue() {
        return otherValue;
    }

    public void setOtherValue(String otherValue) {
        this.otherValue = otherValue;
    }

    public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Override
    public String toString() {
        return "SysDict{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", dictValue='" + dictValue + '\'' +
                ", dictSort=" + dictSort +
                ", dictParentId='" + dictParentId + '\'' +
                ", dictAvailable=" + dictAvailable +
                ", dictDesc='" + dictDesc + '\'' +
                ", user=" + user +
                ", optDate=" + optDate +
                '}';
    }
}
