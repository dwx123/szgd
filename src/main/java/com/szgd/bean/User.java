/**
 * 
 */
package com.szgd.bean;

/**
 * @author jiayb
 *
 */
public class User {
	private Integer userId;
	private String loginId;
	private String userPwd;
	private String userName;
	private String deptCd;
	private int deptParentId;
	private String deptName;
	private Integer sortNumber;
	private String loginIP; // 登录用户IP
	private String identityNo;
	private int personnelId;

	public int getPersonnelId() {
		return personnelId;
	}

	public void setPersonnelId(int personnelId) {
		this.personnelId = personnelId;
	}

	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	@Override
	public String toString() {
		return "User [userId=" + userId + ", loginId=" + loginId + ", userPwd="
				+ userPwd + ", userName=" + userName + ", deptCd=" + deptCd
				+ ", deptName=" + deptName
				+ ", sortNumber=" + sortNumber + "]";
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getDeptCd() {
		return deptCd;
	}
	public void setDeptCd(String deptCd) {
		this.deptCd = deptCd;
	}

	public int getDeptParentId() {
		return deptParentId;
	}

	public void setDeptParentId(int deptParentId) {
		this.deptParentId = deptParentId;
	}

	public Integer getSortNumber() {
		return sortNumber;
	}
	public void setSortNumber(Integer sortNumber) {
		this.sortNumber = sortNumber;
	}
	
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getLoginIP() {
		return loginIP;
	}
	public void setLoginIP(String loginIP) {
		this.loginIP = loginIP;
	}
	
}
