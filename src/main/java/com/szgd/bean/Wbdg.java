package com.szgd.bean;

import java.util.Date;

public class Wbdg {
    /**
     * 主键
     */
    private Long id;

    /**
     * 序号
     */
    private String xh;

    /**
     * 工点名称
     */
    private String gdmc;

    /**
     * 盾构编号
     */
    private String dgxh;

    /**
     * 盾构类型
     */
    private String dglx;

    /**
     * 进度
     */
    private String jd;

    /**
     * 当前环
     */
    private String dqh;

    /**
     * 工况
     */
    private String gk;

    /**
     * 压力(1号)
     */
    private String ylyh;

    /**
     * 注浆压力
     */
    private String zjyl;

    /**
     * 注浆量
     */
    private String zjl;

    /**
     * 平均压力（1号）
     */
    private String pjylyh;

    /**
     * 上行下行
     */
    private String sxxx;

    /**
     * 备注
     */
    private String bz;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 更新时间
     */
    private Date updatetime;

    /**
     * 更新人
     */
    private String uploader;

    /**
     * 删除标记
     */
    private Long delflag;

    /**
     * 关联标段
     */
    private String bidName;

    /**
     * 关联标段ID
     */
    private Long bidId;

    /**
     * 主键
     * @return ID 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 序号
     * @return XH 序号
     */
    public String getXh() {
        return xh;
    }

    /**
     * 序号
     * @param xh 序号
     */
    public void setXh(String xh) {
        this.xh = xh == null ? null : xh.trim();
    }

    /**
     * 工点名称
     * @return GDMC 工点名称
     */
    public String getGdmc() {
        return gdmc;
    }

    /**
     * 工点名称
     * @param gdmc 工点名称
     */
    public void setGdmc(String gdmc) {
        this.gdmc = gdmc == null ? null : gdmc.trim();
    }

    /**
     * 盾构编号
     * @return DGXH 盾构编号
     */
    public String getDgxh() {
        return dgxh;
    }

    /**
     * 盾构编号
     * @param dgxh 盾构编号
     */
    public void setDgxh(String dgxh) {
        this.dgxh = dgxh == null ? null : dgxh.trim();
    }

    /**
     * 盾构类型
     * @return DGLX 盾构类型
     */
    public String getDglx() {
        return dglx;
    }

    /**
     * 盾构类型
     * @param dglx 盾构类型
     */
    public void setDglx(String dglx) {
        this.dglx = dglx == null ? null : dglx.trim();
    }

    /**
     * 进度
     * @return JD 进度
     */
    public String getJd() {
        return jd;
    }

    /**
     * 进度
     * @param jd 进度
     */
    public void setJd(String jd) {
        this.jd = jd == null ? null : jd.trim();
    }

    /**
     * 当前环
     * @return DQH 当前环
     */
    public String getDqh() {
        return dqh;
    }

    /**
     * 当前环
     * @param dqh 当前环
     */
    public void setDqh(String dqh) {
        this.dqh = dqh == null ? null : dqh.trim();
    }

    /**
     * 工况
     * @return GK 工况
     */
    public String getGk() {
        return gk;
    }

    /**
     * 工况
     * @param gk 工况
     */
    public void setGk(String gk) {
        this.gk = gk == null ? null : gk.trim();
    }

    /**
     * 压力(1号)
     * @return YLYH 压力(1号)
     */
    public String getYlyh() {
        return ylyh;
    }

    /**
     * 压力(1号)
     * @param ylyh 压力(1号)
     */
    public void setYlyh(String ylyh) {
        this.ylyh = ylyh == null ? null : ylyh.trim();
    }

    /**
     * 注浆压力
     * @return ZJYL 注浆压力
     */
    public String getZjyl() {
        return zjyl;
    }

    /**
     * 注浆压力
     * @param zjyl 注浆压力
     */
    public void setZjyl(String zjyl) {
        this.zjyl = zjyl == null ? null : zjyl.trim();
    }

    /**
     * 注浆量
     * @return ZJL 注浆量
     */
    public String getZjl() {
        return zjl;
    }

    /**
     * 注浆量
     * @param zjl 注浆量
     */
    public void setZjl(String zjl) {
        this.zjl = zjl == null ? null : zjl.trim();
    }

    /**
     * 平均压力（1号）
     * @return PJYLYH 平均压力（1号）
     */
    public String getPjylyh() {
        return pjylyh;
    }

    /**
     * 平均压力（1号）
     * @param pjylyh 平均压力（1号）
     */
    public void setPjylyh(String pjylyh) {
        this.pjylyh = pjylyh == null ? null : pjylyh.trim();
    }

    /**
     * 上行下行
     * @return SXXX 上行下行
     */
    public String getSxxx() {
        return sxxx;
    }

    /**
     * 上行下行
     * @param sxxx 上行下行
     */
    public void setSxxx(String sxxx) {
        this.sxxx = sxxx == null ? null : sxxx.trim();
    }

    /**
     * 备注
     * @return BZ 备注
     */
    public String getBz() {
        return bz;
    }

    /**
     * 备注
     * @param bz 备注
     */
    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    /**
     * 创建时间
     * @return CREATETIME 创建时间
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * 创建时间
     * @param createtime 创建时间
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * 创建人
     * @return CREATOR 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 创建人
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    /**
     * 更新时间
     * @return UPDATETIME 更新时间
     */
    public Date getUpdatetime() {
        return updatetime;
    }

    /**
     * 更新时间
     * @param updatetime 更新时间
     */
    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    /**
     * 更新人
     * @return UPLOADER 更新人
     */
    public String getUploader() {
        return uploader;
    }

    /**
     * 更新人
     * @param uploader 更新人
     */
    public void setUploader(String uploader) {
        this.uploader = uploader == null ? null : uploader.trim();
    }

    /**
     * 删除标记
     * @return DELFLAG 删除标记
     */
    public Long getDelflag() {
        return delflag;
    }

    /**
     * 删除标记
     * @param delflag 删除标记
     */
    public void setDelflag(Long delflag) {
        this.delflag = delflag;
    }

    public String getBidName() {
        return bidName;
    }

    public void setBidName(String bidName) {
        this.bidName = bidName;
    }

    public Long getBidId() {
        return bidId;
    }

    public void setBidId(Long bidId) {
        this.bidId = bidId;
    }
}