package com.szgd.bean;

import java.util.Date;

public class Wbjc {
    /**
     * 主键
     */
    private Long id;

    /**
     * 所属轴号
     */
    private String sszh;

    /**
     * 测点
     */
    private String cd;

    /**
     * 测点类型
     */
    private String cdlx;

    /**
     * 当前安全状态
     */
    private String dqaqzt;

    /**
     * 累计变化值
     */
    private String ljbhz;

    /**
     * 变形速率
     */
    private String bxsl;

    /**
     * 最大值
     */
    private String zdz;

    /**
     * 最小值
     */
    private String zxz;

    /**
     * 最后测量时间
     */
    private String zhclsj;

    /**
     * 累计报警值
     */
    private String ljbjz;

    /**
     * 速率报警值
     */
    private String slbjz;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 更新时间
     */
    private Date updatetime;

    /**
     * 更新人
     */
    private String uploader;

    /**
     * 删除标记
     */
    private Long delflag;

    /**
     * 关联站点
     */
    private String siteName;

    /**
     * 关联站点ID
     */
    private Long siteId;

    /**
     * 主键
     * @return ID 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 主键
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 所属轴号
     * @return SSZH 所属轴号
     */
    public String getSszh() {
        return sszh;
    }

    /**
     * 所属轴号
     * @param sszh 所属轴号
     */
    public void setSszh(String sszh) {
        this.sszh = sszh == null ? null : sszh.trim();
    }

    /**
     * 测点
     * @return CD 测点
     */
    public String getCd() {
        return cd;
    }

    /**
     * 测点
     * @param cd 测点
     */
    public void setCd(String cd) {
        this.cd = cd == null ? null : cd.trim();
    }

    /**
     * 测点类型
     * @return CDLX 测点类型
     */
    public String getCdlx() {
        return cdlx;
    }

    /**
     * 测点类型
     * @param cdlx 测点类型
     */
    public void setCdlx(String cdlx) {
        this.cdlx = cdlx == null ? null : cdlx.trim();
    }

    /**
     * 当前安全状态
     * @return DQAQZT 当前安全状态
     */
    public String getDqaqzt() {
        return dqaqzt;
    }

    /**
     * 当前安全状态
     * @param dqaqzt 当前安全状态
     */
    public void setDqaqzt(String dqaqzt) {
        this.dqaqzt = dqaqzt == null ? null : dqaqzt.trim();
    }

    /**
     * 累计变化值
     * @return LJBHZ 累计变化值
     */
    public String getLjbhz() {
        return ljbhz;
    }

    /**
     * 累计变化值
     * @param ljbhz 累计变化值
     */
    public void setLjbhz(String ljbhz) {
        this.ljbhz = ljbhz == null ? null : ljbhz.trim();
    }

    /**
     * 变形速率
     * @return BXSL 变形速率
     */
    public String getBxsl() {
        return bxsl;
    }

    /**
     * 变形速率
     * @param bxsl 变形速率
     */
    public void setBxsl(String bxsl) {
        this.bxsl = bxsl == null ? null : bxsl.trim();
    }

    /**
     * 最大值
     * @return ZDZ 最大值
     */
    public String getZdz() {
        return zdz;
    }

    /**
     * 最大值
     * @param zdz 最大值
     */
    public void setZdz(String zdz) {
        this.zdz = zdz == null ? null : zdz.trim();
    }

    /**
     * 最小值
     * @return ZXZ 最小值
     */
    public String getZxz() {
        return zxz;
    }

    /**
     * 最小值
     * @param zxz 最小值
     */
    public void setZxz(String zxz) {
        this.zxz = zxz == null ? null : zxz.trim();
    }

    /**
     * 最后测量时间
     * @return ZHCLSJ 最后测量时间
     */
    public String getZhclsj() {
        return zhclsj;
    }

    /**
     * 最后测量时间
     * @param zhclsj 最后测量时间
     */
    public void setZhclsj(String zhclsj) {
        this.zhclsj = zhclsj == null ? null : zhclsj.trim();
    }

    /**
     * 累计报警值
     * @return LJBJZ 累计报警值
     */
    public String getLjbjz() {
        return ljbjz;
    }

    /**
     * 累计报警值
     * @param ljbjz 累计报警值
     */
    public void setLjbjz(String ljbjz) {
        this.ljbjz = ljbjz == null ? null : ljbjz.trim();
    }

    /**
     * 速率报警值
     * @return SLBJZ 速率报警值
     */
    public String getSlbjz() {
        return slbjz;
    }

    /**
     * 速率报警值
     * @param slbjz 速率报警值
     */
    public void setSlbjz(String slbjz) {
        this.slbjz = slbjz == null ? null : slbjz.trim();
    }

    /**
     * 创建时间
     * @return CREATETIME 创建时间
     */
    public Date getCreatetime() {
        return createtime;
    }

    /**
     * 创建时间
     * @param createtime 创建时间
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * 创建人
     * @return CREATOR 创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 创建人
     * @param creator 创建人
     */
    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    /**
     * 更新时间
     * @return UPDATETIME 更新时间
     */
    public Date getUpdatetime() {
        return updatetime;
    }

    /**
     * 更新时间
     * @param updatetime 更新时间
     */
    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    /**
     * 更新人
     * @return UPLOADER 更新人
     */
    public String getUploader() {
        return uploader;
    }

    /**
     * 更新人
     * @param uploader 更新人
     */
    public void setUploader(String uploader) {
        this.uploader = uploader == null ? null : uploader.trim();
    }

    /**
     * 删除标记
     * @return DELFLAG 删除标记
     */
    public Long getDelflag() {
        return delflag;
    }

    /**
     * 删除标记
     * @param delflag 删除标记
     */
    public void setDelflag(Long delflag) {
        this.delflag = delflag;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }
}