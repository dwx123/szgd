package com.szgd.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szgd.bean.Wbdg;
import com.szgd.bean.Wbjc;
import com.szgd.service.sys.WbDataService;

@Controller
@RequestMapping(value = "/wbsj")
public class WbDataController extends BaseController {
    private static Logger logger = Logger.getLogger(WbDataController.class);

    @Autowired
    WbDataService wbDataService;
    
    @RequestMapping(value = "/wbjc-save", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveWbjc(@RequestBody List<Wbjc> jcList, HttpSession session, HttpServletRequest request) throws Exception {
		com.szgd.bean.MessageBean mb = new com.szgd.bean.MessageBean();
		wbDataService.saveWbjc(jcList);
		mb.setMsg("外部监测数据保存成功！");
		return com.szgd.bean.MessageBean.json(mb);
	}
    
    @RequestMapping(value = "/wbdg-save", method = { RequestMethod.GET,
			RequestMethod.POST }, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String saveWbdg(@RequestBody List<Wbdg> dgList, HttpSession session, HttpServletRequest request) throws Exception {
		com.szgd.bean.MessageBean mb = new com.szgd.bean.MessageBean();
		wbDataService.saveWbdg(dgList);
		mb.setMsg("外部盾构数据保存成功！");
		return com.szgd.bean.MessageBean.json(mb);
	}
}
