package com.szgd.controller.elseData;

import com.szgd.controller.BaseController;
import com.szgd.controller.weixin.WeChatController;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/pit")
public class PitController extends BaseController {
    private static Logger logger = Logger.getLogger(PitController.class);

    @RequestMapping(value = "toPit", method = RequestMethod.GET)
    public ModelAndView toTestFrame(HttpServletRequest request, HttpSession session) {
        ModelAndView mv = getModelAndView(request,session,"/elseData/pit");
        return mv;
    }


}
