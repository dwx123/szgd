package com.szgd.controller.elseData;

import com.szgd.controller.BaseController;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/tunnel")
public class TunnelController extends BaseController {
    private static Logger logger = Logger.getLogger(TunnelController.class);

    @RequestMapping(value = "toTunnel", method = RequestMethod.GET)
    public ModelAndView toTestFrame(HttpServletRequest request, HttpSession session) {
        ModelAndView mv = getModelAndView(request,session,"/elseData/tunnel");
        return mv;
    }
}
