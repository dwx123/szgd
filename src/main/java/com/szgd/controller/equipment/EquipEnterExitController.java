package com.szgd.controller.equipment;

import com.szgd.controller.BaseController;
import com.szgd.service.equipment.EquipEnterExitService;
import com.szgd.service.equipment.EquipmentService;
import com.szgd.util.PropertiesUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/equipEnterExit")
public class EquipEnterExitController extends BaseController {
    @Autowired
    EquipEnterExitService equipEnterExitService;
    @Autowired
    EquipmentService equipmentService;

    private static Logger logger = Logger.getLogger(EquipEnterExitController.class);

    @RequestMapping(value = "toEquipEnterExitList", method = RequestMethod.GET)
    public ModelAndView toEquipEnterExitList(HttpServletRequest request, HttpSession httpSession) {
        ModelAndView mv = this.getModelAndView(request,httpSession,"equipment/equipEnterExit/equipEnterExitList");
        return mv;
    }

    @RequestMapping(value = "toEquipEnterExitForm", method = RequestMethod.GET)
    public ModelAndView toEquipEnterExitForm(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/equipment/equipEnterExit/equipEnterExitForm");
        List<Map<String,Object>> equipList = equipmentService.getEquipmentInfoListNoPage(null);
        mv.addObject("equipList",equipList);
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0) {
            Map<String, Object> equipEnterExitMap = equipEnterExitService.getEnterExit(id);
            mv.addObject("equipEnterExit",equipEnterExitMap);
        }
        return mv;
    }

    @RequestMapping(value = "/getEquipEnterAndExitList", method = RequestMethod.GET)
    public void  getEquipEnterAndExitList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = equipEnterExitService.getEquipEnterAndExitList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/getEquipEnter_ExitList", method = RequestMethod.GET)
    public void  getEquipEnter_ExitList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = equipEnterExitService.getEquipEnter_ExitList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/saveEnterExit")
    @ResponseBody
    public Map<String,Object> saveEnterExit(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String enterTime = paramMap.get("enterTime")== null?"":paramMap.get("enterTime").toString().replace("null","");
        String exitTime = paramMap.get("exitTime")== null?"":paramMap.get("exitTime").toString().replace("null","");
        //String siteId = paramMap.get("siteId")== null?"":paramMap.get("siteId").toString().replace("null","");
        String message;
        try {
            if (id.length() == 0){//新增
                paramMap.put("creator",this.getLoginId(request.getSession()));
                paramMap.put("passTime",enterTime);
                paramMap.put("passFlag",1);
                //paramMap.put("siteId",siteId);
                equipEnterExitService.insertEquipEnterExit(paramMap);
                if(exitTime.length() > 0){
                    paramMap.put("passTime",exitTime);
                    paramMap.put("passFlag",2);
                    paramMap.put("enterId",paramMap.get("id"));
                    equipEnterExitService.insertEquipEnterExit(paramMap);
                }
            } else {
                paramMap.put("creator",this.getLoginId(request.getSession()));
                //paramMap.put("siteId",siteId);
                paramMap.put("passTime",enterTime);
                equipEnterExitService.updateEquipEnterExit(paramMap);
                Map<String, Object> paramMap1 = new HashMap<>();
                paramMap1.put("enterId",id);
                List<Map<String, Object>> enterExitList = equipEnterExitService.getEnterOrExitListNoPage(paramMap1);
                if (exitTime.length() > 0)
                {
                    paramMap.put("passTime",exitTime);
                    if (enterExitList != null && enterExitList.size() > 0)
                    {
                        paramMap.put("id",enterExitList.get(0).get("id"));
                        equipEnterExitService.updateEquipEnterExit(paramMap);
                    }else
                    {
                        paramMap.put("enterId",id);
                        paramMap.put("passFlag",2);
                        equipEnterExitService.insertEquipEnterExit(paramMap);
                    }
                }else
                {
                    if (enterExitList != null && enterExitList.size() > 0)
                    {
                        paramMap.put("id",enterExitList.get(0).get("id"));
                        equipEnterExitService.deleteEnterExit(paramMap);
                    }
                }
            }
            message = "设备进出信息保存成功!";
            return getSuccMap(message);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("设备进出信息保存失败："+e.getMessage());
            return getErrorMap("设备进出信息保存失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/deleteEnterExit")
    @ResponseBody
    public Map<String,Object> deleteEnterExit(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        try{
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            equipEnterExitService.deleteEnterExit(paramMap);

            String message = "进出信息删除成功！";
            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("进出信息删除失败："+e.getMessage());
            return getSuccMap("进出信息删除失败："+e.getMessage());
        }
    }

    @RequestMapping("/downloadRecord")
    public void downloadRecord(HttpServletRequest request, HttpServletResponse response) throws Exception{
        Map<String,Object> map = this.getMapParams(request);
        Map<String, String> fieldMapping = equipEnterExitService.getFieldMap();
        String fileName = "设备进出记录.xls";
        String filePath = PropertiesUtil.get("downloadPath");
        String realPath = request.getServletContext().getRealPath(filePath);
        filePath = filePath.replace("\\", "/");
        File f = new File(realPath);
        if (!f.exists()) {
            f.mkdirs();
        }
        realPath = realPath + File.separator + fileName;
        List<Map<String, Object>> dataList = equipEnterExitService.getEnterAndExitListNoPage(map);
        exportExcelByTemplate(fieldMapping,dataList, realPath,"设备进出记录导出模板.xls");
        String downloadPath = filePath + "/" + fileName;
        realPath = request.getServletContext().getRealPath(downloadPath);
        try {
            this.download(response, realPath);
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        new File(realPath).delete();

    }
}
