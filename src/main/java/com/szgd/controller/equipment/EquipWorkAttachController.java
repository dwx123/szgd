package com.szgd.controller.equipment;

import com.szgd.bean.Hoist;
import com.szgd.controller.BaseController;
import com.szgd.service.equipment.EquipWorkAttachService;
import com.szgd.util.FileUtil;
import com.szgd.util.PropertiesUtil;
import com.szgd.util.SysUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/equipWorkAttach")
public class EquipWorkAttachController extends BaseController {
    private static Logger logger = Logger.getLogger(EquipWorkAttachController.class);

    @Autowired
    EquipWorkAttachService equipWorkAttachService;


    @RequestMapping(value = "/saveEquipWorkAttach")
    @ResponseBody
    public Map<String,Object> saveEquipWorkAttach(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        try {
            paramMap.put("creator",this.getLoginId(request.getSession()));
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            paramMap.put("uploadPerson",this.getUserId(request.getSession()));
            paramMap.put("uploadPersonName",this.getUserName(request.getSession()));
            equipWorkAttachService.saveEquipWorkAttach(paramMap);
            return getSuccMap("附件保存成功！");
        }catch(Exception e){
            e.printStackTrace();
            return getSuccMap("附件保存失败："+e.getMessage());
        }
    }


    @RequestMapping(value = "/showEquipWorkAttach", method = RequestMethod.GET)
    @ResponseBody
    public String showEquipWorkAttach(HttpServletRequest request, HttpServletResponse response) {

        try {

            //String picPath = URLDecoder.decode(request.getParameter("fileName"),"utf-8");
            String id = request.getParameter("id");
            OutputStream outputStream = response.getOutputStream();
            response.setContentType("application/octet-stream ");
            response.setHeader("Connection", "close"); // 表示不能用浏览器直接打开
            response.setHeader("Accept-Ranges", "bytes");// 告诉客户端允许断点续传多线程连接下载
            response.setContentType("multipart/form-data");
            response.setContentType("UTF-8");

            Map<String, Object>  attachMap = equipWorkAttachService.getEquipWorkAttach(id);
            String subPath = attachMap.get("path").toString();
            String fileName = attachMap.get("name").toString();
            String fileUploadPath = PropertiesUtil.get("uploadPath");
            String filePath = FileUtil.existFile(fileUploadPath,subPath,fileName);
            InputStream inputStream = new FileInputStream(filePath +  File.separator + fileName);
            int len = 0;
            byte[] buf = new byte[1024];
            while ((len = inputStream.read(buf, 0, 1024)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/getEquipWorkAttachList", method = RequestMethod.GET)
    public void  getEquipWorkAttachList(HttpServletResponse response,HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = equipWorkAttachService.getEquipWorkAttachList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/deleteEquipWorkAttach")
    @ResponseBody
    public Map<String,Object> deleteEquipWorkAttach(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        try{
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            equipWorkAttachService.deleteEquipWorkAttach(paramMap);

            String message = "附件删除成功！";
            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("附件删除失败："+e.getMessage());
            return getSuccMap("附件删除失败："+e.getMessage());
        }
    }
}
