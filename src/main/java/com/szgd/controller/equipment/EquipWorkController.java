package com.szgd.controller.equipment;

import com.szgd.controller.BaseController;
import com.szgd.service.equipment.EquipWorkService;
import com.szgd.service.equipment.EquipmentService;
import com.szgd.service.personnel.PersonnelInfoService;
import com.szgd.service.personnel.TrainService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/equipWork")
public class EquipWorkController extends BaseController {
    private static Logger logger = Logger.getLogger(EquipWorkController.class);

    @Autowired
    EquipWorkService equipWorkService;
    @Autowired
    TrainService trainService;

    @RequestMapping(value = "toEquipWorkList", method = RequestMethod.GET)
    public String toEquipWorkList() {
        return "/equipment/equipmentWork/equipWorkList";
    }

    @RequestMapping(value = "/getEquipWorkList", method = RequestMethod.GET)
    public void  getEquipWorkList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = equipWorkService.getEquipWorkList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping("/toEquipWorkView")
    public ModelAndView toEquipWorkView(HttpServletRequest request, HttpSession session) throws Exception{
        Map<String, Object> mapParams = getMapParams(request);
        ModelAndView mav = new ModelAndView("/equipment/equipmentWork/equipWorkView");
        String id = mapParams.get("id") == null ? "" : mapParams.get("id").toString();
        String name = mapParams.get("name") == null ? "" : mapParams.get("name").toString();
        String craneFlag;
        if (name.indexOf("CRANE") != -1){
            craneFlag = "crane";
        }else {
            craneFlag = "else";
        }
        mav.addObject("craneFlag", craneFlag);
        mav.addObject("id", id);
        return mav;
    }

    @RequestMapping("/toEquipWorkUpload")
    public ModelAndView toEquipWorkUpload(HttpServletRequest request, HttpSession session) throws Exception{
        Map<String, Object> mapParams = getMapParams(request);
        ModelAndView mav = new ModelAndView("/equipment/equipmentWork/equipWorkUpload");
        String id = mapParams.get("id") == null ? "" : mapParams.get("id").toString();
        String modalFlag = mapParams.get("modalFlag") == null ? "" : mapParams.get("modalFlag").toString();
        String equipNumber = mapParams.get("equipNumber") == null ? "" : mapParams.get("equipNumber").toString();
        String equipName = mapParams.get("equipName") == null ? "" : mapParams.get("equipName").toString();
        List<Map<String,Object>> personList = trainService.getAllPerson();

        mav.addObject("personList", personList);
        mav.addObject("equipId", id);
        mav.addObject("modalFlag", modalFlag);
        mav.addObject("equipNumber", equipNumber);
        mav.addObject("equipName", equipName);

        return mav;
    }
}
