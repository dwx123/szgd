package com.szgd.controller.gatherEquip;

import com.szgd.controller.BaseController;
import com.szgd.service.equipment.EquipmentService;
import com.szgd.service.gatherEquip.GatherEquipService;
import com.szgd.util.HttpUtil;
import com.szgd.util.PropertiesUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/gatherEquip")
public class GatherEquipController extends BaseController {
    private static Logger logger = Logger.getLogger(GatherEquipController.class);

    @Autowired
    GatherEquipService gatherEquipService;

    @RequestMapping(value = "toGatherEquipList", method = RequestMethod.GET)
    public ModelAndView toGatherEquipList(HttpServletRequest request, HttpSession session) {
        ModelAndView mv = this.getModelAndView(request,session,"gatherEquip/gatherEquip/gatherEquipList");
        return mv;
    }

    @RequestMapping(value = "toGatherEquipView", method = RequestMethod.GET)
    public ModelAndView toGatherEquipView(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"gatherEquip/gatherEquip/gatherEquipView");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0)//修改
        {
            Map<String, Object> gatherEquipMap = gatherEquipService.getGatherEquip(id);
            mv.addObject("gatherEquip",gatherEquipMap);
        }
        return mv;
    }

    @RequestMapping(value = "toGatherEquipForm", method = RequestMethod.GET)
    public ModelAndView toGatherEquipForm(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"gatherEquip/gatherEquip/gatherEquipForm");
        Map<String, Object> gatherEquipMap  = new HashMap<>();
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0)//修改
        {
            gatherEquipMap = gatherEquipService.getGatherEquip(id);
        }
        mv.addObject("gatherEquip",gatherEquipMap);
        return mv;
    }

    @RequestMapping(value = "/getGatherEquipList", method = RequestMethod.GET)
    public void  getGatherEquipList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = gatherEquipService.getGatherEquipList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/getPeopleGatherEquipListOfSite", method = RequestMethod.POST)
    @ResponseBody
    public List<Map<String, Object>>  getPeopleGatherEquipListOfSite(@RequestBody HashMap<String, Object> paramMap,HttpServletResponse response, HttpServletRequest request){
        String siteId = paramMap.get("siteId")== null?"":paramMap.get("siteId").toString().replace("null","");
        List<Map<String, Object>> dataList = gatherEquipService.getPeopleGatherEquipListOfSite(siteId);
        return  dataList;
    }

    @RequestMapping(value = "/saveGatherEquip")
    @ResponseBody
    public Map<String,Object> saveGatherEquip(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String message = "";
        try {
            Map<String, Object> param1 = new HashMap<>();
            param1.put("ip",paramMap.get("ip"));
            List<Map<String, Object>>  gatherEquipList = gatherEquipService.getGatherEquipListNoPage(param1);
            if (gatherEquipList != null && gatherEquipList.size() > 0)
            {
                boolean isHave = true;
                if (id.length() != 0)
                {
                    Map<String, Object> gatherEquipMap = gatherEquipList.get(0);
                    String tempId = gatherEquipMap.get("id").toString();
                    if (id.equalsIgnoreCase(tempId))
                    {
                        isHave = false;
                    }
                }
                if (isHave)
                {
                    message = "表里已经存在相同IP!";
                    return getErrorMap(message);
                }

            }

            if (id.length() == 0)//新增
            {
                paramMap.put("creator",this.getLoginId(request.getSession()));
                gatherEquipService.insertGatherEquip(paramMap);
                message = "采集设备信息新增成功！";
            } else {
                paramMap.put("uploader",this.getLoginId(request.getSession()));
                gatherEquipService.updateGatherEquip(paramMap);
                message = "采集设备信息保存成功！";
            }
            return getSuccMap(message);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("采集设备信息保存失败："+e.getMessage());
            return getSuccMap("采集设备信息保存失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/deleteGatherEquip", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> deleteGatherEquip(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        try{
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            paramMap.put("delflag",1);
            gatherEquipService.updateGatherEquip(paramMap);
            String message = "采集设备删除成功！";
            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("采集设备删除失败："+e.getMessage());
            return getErrorMap("采集设备删除失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/setPassword", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> setPassword(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        String message = "";
        try{
            String oldPass = paramMap.get("oldPassword")== null?"":paramMap.get("oldPassword").toString().replace("null","");
            String newPass = paramMap.get("newPassword")== null?"":paramMap.get("newPassword").toString().replace("null","");
            String ip = paramMap.get("ip")== null?"":paramMap.get("ip").toString().replace("null","");
            message = gatherEquipService.setPassword(oldPass,newPass,ip);
            if (message.length() == 0)
            {
                paramMap.put("password",newPass);
                gatherEquipService.updateGatherEquip(paramMap);
                message = "密码设置成功！";
                return getSuccMap(message);
            }else
            {
                return getErrorMap(message);
            }

        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("密码设置失败："+e.getMessage());
            return getErrorMap("密码设置失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/setCallBack", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> setCallBack(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        String message = "";
        try{
            String pass = paramMap.get("password")== null?"":paramMap.get("password").toString().replace("null","");
            String callBack = paramMap.get("callBack")== null?"":paramMap.get("callBack").toString().replace("null","");
            String ip = paramMap.get("ip")== null?"":paramMap.get("ip").toString().replace("null","");
            message = gatherEquipService.setCallBack(pass,callBack,ip);
            if (message.length() == 0)
            {
                paramMap.put("callBack",callBack);
                gatherEquipService.updateGatherEquip(paramMap);
                message = "回调接口设置成功！";
                return getSuccMap(message);
            }else
            {
                return getErrorMap(message);
            }

        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("回调接口设置失败："+e.getMessage());
            return getErrorMap("回调接口设置失败："+e.getMessage());
        }
    }
}
