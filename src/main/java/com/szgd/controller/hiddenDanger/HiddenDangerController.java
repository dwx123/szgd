package com.szgd.controller.hiddenDanger;

import com.szgd.bean.Certificate;
import com.szgd.bean.HiddenDanger;
import com.szgd.bean.User;
import com.szgd.controller.BaseController;
import com.szgd.service.hiddenDanger.HiddenDangerService;
import com.szgd.service.personnel.AttachService;
import com.szgd.service.personnel.TrainService;
import com.szgd.service.project.BidService;
import com.szgd.service.project.SiteService;
import com.szgd.service.sys.UserService;
import com.szgd.util.Constant;
import com.szgd.util.ExcelUtil;
import com.szgd.util.PropertiesUtil;
import com.szgd.util.TimeUtil;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "/hiddenDanger")
public class HiddenDangerController extends BaseController {
    private static Logger logger = Logger.getLogger(HiddenDangerController.class);

    @Autowired
    HiddenDangerService hiddenDangerService;
    @Autowired
    TrainService trainService;
    @Autowired
    AttachService attachService;
    @Autowired
    SiteService siteService;
    @Autowired
    UserService userService;

    @RequestMapping(value = "toHiddenDangerList", method = RequestMethod.GET)
    public ModelAndView toHiddenDangerList(HttpServletRequest request,HttpSession httpSession) {
        ModelAndView mv = this.getModelAndView(request,httpSession,"/hiddenDanger/hiddenDangerList");
        return mv;
    }

    @RequestMapping(value = "/getHiddenDangerList", method = RequestMethod.GET)
    public void  getHiddenDangerList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
//        String roleIds = paramMap.get("roleIds")==null?"":paramMap.get("roleIds").toString();
//        if (roleIds.indexOf("hidden_danger_approver") >=0)
//        {
//            paramMap.put("approveFlag",1);
//        }else if (roleIds.indexOf("hidden_danger_confirmer") >=0)
//        {
//            paramMap.put("approveFlag",2);
//        }else
//        {
//            paramMap.put("approveFlag",0);
//        }
        String isRectify = paramMap.get("isRectify")==null?"":paramMap.get("isRectify").toString().replace("null","");
        if(isRectify.length() == 0){
            paramMap.put("isRectify",null);
        }
        String siteId = paramMap.get("siteId")==null?"":paramMap.get("siteId").toString().replace("null","");
        if(siteId.length() == 0){
            paramMap.put("siteId",null);
        }
        String bidId = paramMap.get("bidId")==null?"":paramMap.get("bidId").toString().replace("null","");
        if(bidId.length() == 0){
            paramMap.put("bidId",null);
        }
        String status = paramMap.get("status")==null?"":paramMap.get("status").toString().replace("null","");
        if(status.length() == 0){
            paramMap.put("status",null);
            paramMap.put("excludeStatus","CONFIRM");
        }else if(!status.equalsIgnoreCase("CONFIRM")){
            paramMap.put("excludeStatus","CONFIRM");//隐患列表页默认只显示待整改和已整改的隐患
        }
        List<Map<String, Object>> dataList = hiddenDangerService.getHiddenDangerList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "toHiddenDangerForm", method = RequestMethod.GET)
    public ModelAndView toHiddenDangerForm(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/hiddenDanger/hiddenDangerForm");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");;
        if (id.length() > 0)//修改
        {
            Map<String, Object> hiddenDangerMap = hiddenDangerService.getHiddenDanger(id);
            mv.addObject("hiddenDanger",hiddenDangerMap);
        }
        List<User> rcList = userService.getRcOrCfPerson("hidden_danger_approver");
        mv.addObject("rcList", rcList);
        List<User> cfList = userService.getRcOrCfPerson("hidden_danger_confirmer");
        mv.addObject("cfList", cfList);
        List<User> rpList = userService.getAllRpPerson();
        mv.addObject("rpList", rpList);
        return mv;
    }

    @RequestMapping(value = "toHiddenDangerApproval", method = RequestMethod.GET)
    public ModelAndView toHiddenDangerApproval(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/hiddenDanger/hiddenDangerApproval");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");;
        if (id.length() > 0)//修改
        {
            Map<String, Object> hiddenDangerMap = hiddenDangerService.getHiddenDanger(id);
            mv.addObject("hiddenDanger",hiddenDangerMap);
        }
        List<User> rcList = userService.getRcOrCfPerson("hidden_danger_approver");
        mv.addObject("rcList", rcList);
        List<User> cfList = userService.getRcOrCfPerson("hidden_danger_confirmer");
        mv.addObject("cfList", cfList);
        List<User> rpList = userService.getAllRpPerson();
        mv.addObject("rpList", rpList);
        return mv;
    }

    @RequestMapping(value = "toHiddenDangerConfirm", method = RequestMethod.GET)
    public ModelAndView toHiddenDangerConfirm(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/hiddenDanger/hiddenDangerConfirm");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");;
        if (id.length() > 0)//修改
        {
            Map<String, Object> hiddenDangerMap = hiddenDangerService.getHiddenDanger(id);
            mv.addObject("hiddenDanger",hiddenDangerMap);
        }
        List<User> rcList = userService.getRcOrCfPerson("hidden_danger_approver");
        mv.addObject("rcList", rcList);
        List<User> cfList = userService.getRcOrCfPerson("hidden_danger_confirmer");
        mv.addObject("cfList", cfList);
        List<User> rpList = userService.getAllRpPerson();
        mv.addObject("rpList", rpList);
        return mv;
    }

    @RequestMapping(value = "toHiddenDangerView", method = RequestMethod.GET)
    public ModelAndView toHiddenDangerView(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/hiddenDanger/hiddenDangerView");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0)//修改
        {
            Map<String, Object> hiddenDangerMap = hiddenDangerService.getHiddenDanger(id);
            mv.addObject("hiddenDanger",hiddenDangerMap);
        }
        List<User> rcList = userService.getRcOrCfPerson("hidden_danger_approver");
        mv.addObject("rcList", rcList);
        List<User> cfList = userService.getRcOrCfPerson("hidden_danger_confirmer");
        mv.addObject("cfList", cfList);
        List<User> rpList = userService.getAllRpPerson();
        mv.addObject("rpList", rpList);
        return mv;
    }

    //跳转批量上报/整改
    @RequestMapping("/toBatchUploadExcel")
    public ModelAndView toBatchUploadExcel(HttpServletRequest request, HttpSession session) throws Exception{
        Map<String, Object> mapParams = getMapParams(request);
        ModelAndView mav = new ModelAndView("/hiddenDanger/batchUploadExcel");
        String modalFlag = mapParams.get("modalFlag") == null ? "" : mapParams.get("modalFlag").toString();
        mav.addObject("modalFlag", modalFlag);
        return mav;
    }

    //跳转批量上传隐患照片/整改照片
    @RequestMapping("/toBatchUploadPic")
    public ModelAndView toBatchUploadPic(HttpServletRequest request, HttpSession session) throws Exception{
        Map<String, Object> mapParams = getMapParams(request);
        ModelAndView mav = new ModelAndView("/hiddenDanger/batchUploadPic");
        String modalFlag = mapParams.get("modalFlag") == null ? "" : mapParams.get("modalFlag").toString();
        mav.addObject("modalFlag", modalFlag);
        return mav;
    }

    /*
    隐患上报/编辑
     */
    @RequestMapping(value = "/saveHiddenDanger")
    @ResponseBody
    public Map<String,Object> saveHiddenDanger(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String loginId = this.getLoginId(request.getSession());
        String message = "";
        try {
            if (id.length() == 0) {
                paramMap.put("creator",loginId);
                paramMap.put("status","REPORT");
                hiddenDangerService.insertHiddenDanger(paramMap,"web",loginId,"report",request);
                if(paramMap.get("id") == null || paramMap.get("id").toString().length() == 0){
                    message = "duplicated";
                }else{
                    id = paramMap.get("id").toString();
                    message = "隐患信息保存成功";
                }
            } else {
                paramMap.put("uploader",loginId);
                hiddenDangerService.updateHiddenDanger(paramMap,"web",loginId,"report",request);
                message = "隐患信息保存成功";
            }
            return getSuccMap(message);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("隐患信息保存失败,隐患ID:"+id+"---"+e.getMessage());
            return getSuccMap("隐患信息保存失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/deleteHiddenDanger")
    @ResponseBody
    public Map<String,Object> deleteHiddenDanger(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        try{
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            hiddenDangerService.deleteHiddenDanger(paramMap);

            String message = "隐患信息删除成功！";
            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("隐患信息删除失败："+e.getMessage());
            return getSuccMap("隐患信息删除失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/confirmHiddenDanger")
    @ResponseBody
    public Map<String,Object> confirmHiddenDanger(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        try{
            paramMap.put("uploader",this.getLoginId(request.getSession()));
//            paramMap.put("userId",this.getUserId(request.getSession()));
            paramMap.put("isRectify",1);
            paramMap.put("status","CONFIRM");
            hiddenDangerService.confirmHidden(paramMap);

            String message = "隐患确认成功!";
            return getSuccMap(message);
        }catch (Exception e) {
            e.printStackTrace();
            logger.error("隐患确认失败："+e.getMessage());
            return getSuccMap("隐患确认失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/rectifyHiddenDanger")
    @ResponseBody
    public Map<String,Object> rectifyHiddenDanger(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String loginId = this.getLoginId(request.getSession());
        try{
            paramMap.put("uploader",loginId);
            paramMap.put("status","RECTIFY");
            hiddenDangerService.updateHiddenDanger(paramMap,"web",loginId,"rectify",request);
            return getSuccMap("整改成功");
        }catch (Exception e) {
            e.printStackTrace();
            logger.error("整改失败,隐患ID:"+id+"---"+e.getMessage());
            return getSuccMap("整改失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/reRectifyHiddenDanger")
    @ResponseBody
    public Map<String,Object> reRectifyHiddenDanger(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String loginId = this.getLoginId(request.getSession());
        try{
            paramMap.put("uploader",loginId);
            paramMap.put("status","REPORT");
            paramMap.put("cleanNotice","true");
            hiddenDangerService.updateHiddenDanger(paramMap,"web",loginId,"noPic",request);
            String message = "操作成功";
            return getSuccMap(message);
        }catch (Exception e) {
            e.printStackTrace();
            logger.error("重新整改操作失败,隐患ID:"+id+"---"+e.getMessage());
            return getSuccMap("操作失败："+e.getMessage());
        }
    }

    /*@RequestMapping(value = "/getSites")
    @ResponseBody
    public List<Map<String,Object>> getSites(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        String bidId = paramMap.get("bidId")==null?"":paramMap.get("bidId").toString().replace("null","");
        if(bidId.length() == 0){
            paramMap.put("bidId",null);
        }
        return siteService.getSiteListNoPage(paramMap);
    }*/

    /*
    下载隐患信息
     */
    @RequestMapping("/downloadRecord")
    public void downloadRecord(HttpServletRequest request, HttpServletResponse response) throws Exception{
//        Map<String,Object> map = this.getMapParams(request);
//        String siteId = map.get("siteId")==null?"":map.get("siteId").toString().replace("null","");
//        if(siteId.length() == 0){
//            map.put("siteId",null);
//        }
//        List<Map<String, Object>> dataList = hiddenDangerService.getHiddenDangerListNoPage(map);
//        // 生成Excel
//        //excel标题
//        String[] title={"隐患主题","标段","车站","整改期限(天)","隐患发现时间","隐患照片","隐患描述","上报人","上报时间","整改人","整改时间","整改照片","整改内容","确认人","确认时间","状态"};
//        //excel名称
//        String fileName = "隐患信息.xls";
//        //sheet名
//        String sheetName = "隐患信息";
//
//        HSSFWorkbook wb = ExcelUtil.getHSSFWorkbook(sheetName, title, dataList,null);
//        //响应到客户端
//        try {
//            this.setResponseHeader(response, fileName);
//            OutputStream os = response.getOutputStream();
//            wb.write(os);
//            os.flush();os.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.err.println(e.getMessage());
//        }

        Map<String,Object> map = this.getMapParams(request);
        String siteId = map.get("siteId")==null?"":map.get("siteId").toString().replace("null","");
        if(siteId.length() == 0){
            map.put("siteId",null);
        }
        String bidId = map.get("bidId")==null?"":map.get("bidId").toString().replace("null","");
        if(bidId.length() == 0){
            map.put("bidId",null);
        }
        String status = map.get("status")==null?"":map.get("status").toString().replace("null","");
        if(status.length() == 0){
            map.put("status",null);
            map.put("excludeStatus","CONFIRM");
        }else if(!status.equalsIgnoreCase("CONFIRM")){
            map.put("excludeStatus","CONFIRM");
        }
        Map<String, String> fieldMapping = hiddenDangerService.getDownlaodMap();
        String fileName = "隐患信息.xls";
        String filePath = PropertiesUtil.get("downloadPath");
        String realPath = request.getServletContext().getRealPath(filePath);
        filePath = filePath.replace("\\", "/");
        File f = new File(realPath);
        if (!f.exists()) {
            f.mkdirs();
        }
        realPath = realPath + File.separator + fileName;
        List<Map<String, Object>> dataList = hiddenDangerService.getHiddenDangerListNoPage(map);
        exportExcelByTemplate(fieldMapping,dataList, realPath,"隐患批量导入模板.xls");
        String downloadPath = filePath + "/" + fileName;
        realPath = request.getServletContext().getRealPath(downloadPath);
        try {
            this.download(response, realPath);
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        new File(realPath).delete();
    }


    //发送响应流方法
    public void setResponseHeader(HttpServletResponse response, String fileName) {
        try {
            try {
                fileName = new String(fileName.getBytes(),"ISO8859-1");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            response.setContentType("application/octet-stream;charset=ISO8859-1");
            response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @RequestMapping(value = "/getHiddenDangerCount")
    @ResponseBody
    public Integer getHiddenDangerCount(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        paramMap.put("status","REPORT");
        return hiddenDangerService.getHiddenDangerCount(paramMap);
    }

    /*
    批量上传隐患excel
     */
    @ResponseBody
    @RequestMapping(value = "/batchUploadExcel", method = {RequestMethod.GET, RequestMethod.POST})
    public void batchUploadExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        long startTime = System.currentTimeMillis();
        InputStream in = null;
        List<List<Object>> listob = null;
        MultipartFile file = multipartRequest.getFile("file");
        if (file.isEmpty()) {
            throw new Exception("文件不存在！");
        }
        in = file.getInputStream();
        PrintWriter out = null;
        response.setCharacterEncoding("UTF-8");  //防止ajax接受到的中文信息乱码
        response.setHeader("Content-type", "text/html;charset=UTF-8");   //跟上面同时使用可解决乱码问题
        out = response.getWriter();
        try {

            Map<String, String> fieldMapping = hiddenDangerService.getFieldMap();
            List<Map<String, Object>> dataList = ExcelUtil.getListByExcel(in, file.getOriginalFilename(), fieldMapping, false);
            String batchNo = TimeUtil.formatDate(Calendar.getInstance().getTime(), "yyyyMMddhhmmss");
            Map<String, Object> resultMap = hiddenDangerService.batchImportExcel(dataList, this.getLoginId(request.getSession()), batchNo);

            long endTime = System.currentTimeMillis();
            List<Map<String, Object>> successList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_SUCCESS);
            List<Map<String, Object>> ignoreList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_IGNORE);
            List<Map<String, Object>> failedList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_FAILED);
            List<Map<String, Object>> updatedList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_UPDATED);
            String errorHtml = failedList.size() + "条";

            if (failedList.size() > 0) {
                //生成失败记录文件
                String fileName = "隐患信息批量导入" + batchNo + ".xls";
                String filePath = PropertiesUtil.get("batchImportErrorPath");
                String realPath = request.getServletContext().getRealPath(filePath);
                filePath = filePath.replace("\\", "/");
                File f = new File(realPath);
                if (!f.exists()) {
                    f.mkdirs();
                }
                realPath = realPath + File.separator + fileName;
                exportExcelByTemplate(fieldMapping,failedList, realPath,"隐患批量导入模板.xls");
                String downloadPath = filePath + "/" + URLEncoder.encode(fileName,"UTF-8");
                errorHtml = "<a href='#WEBROOT#/downloadErrorRecord?path=" + downloadPath + "'>" + failedList.size() + "条（点击查看失败原因）</a>";
            }
            out.print("本次导入共处理" + dataList.size() + "条数据[新增：" + successList.size() + "条，更新："
                    + updatedList.size() + "条，<font color='red'>失败：" + errorHtml
                    + "</font>]，耗时： "
                    + (endTime - startTime) / 1000 + "秒！");
        } catch (Exception e) {
            out.print("文件导入失败！");
            logger.error(e.getMessage());
        }
        out.flush();
        out.close();
    }

    //批量上传隐患照片
    @RequestMapping("/uploadReportPic")
    @ResponseBody
    public void uploadReportPic(HiddenDanger hiddenDanger, HttpServletRequest request, HttpServletResponse response) throws Exception{
        PrintWriter out = response.getWriter();
        try{
            hiddenDanger.setCreator(this.getLoginId(request.getSession()));
            String res = hiddenDangerService.uploadReportPic(hiddenDanger);
            out.print(res);
        }catch (Exception e) {
            out.print("照片导入失败！");
            logger.error(e.getMessage());
        }
        out.flush();
        out.close();
    }

    /*
    批量整改
     */
    @ResponseBody
    @RequestMapping(value = "/batchUploadRectifyExcel", method = {RequestMethod.GET, RequestMethod.POST})
    public void batchUploadRectifyExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        long startTime = System.currentTimeMillis();
        InputStream in = null;
        List<List<Object>> listob = null;
        MultipartFile file = multipartRequest.getFile("file");
        if (file.isEmpty()) {
            throw new Exception("文件不存在！");
        }
        in = file.getInputStream();
        PrintWriter out = null;
        response.setCharacterEncoding("UTF-8");  //防止ajax接受到的中文信息乱码
        response.setHeader("Content-type", "text/html;charset=UTF-8");   //跟上面同时使用可解决乱码问题
        out = response.getWriter();
        try {

            Map<String, String> fieldMapping = hiddenDangerService.getFieldMap();
            List<Map<String, Object>> dataList = ExcelUtil.getListByExcel(in, file.getOriginalFilename(), fieldMapping, false);
            String batchNo = TimeUtil.formatDate(Calendar.getInstance().getTime(), "yyyyMMddhhmmss");
            Map<String, Object> resultMap = hiddenDangerService.batchImportRectifyExcel(dataList, this.getLoginId(request.getSession()), batchNo);

            long endTime = System.currentTimeMillis();
            List<Map<String, Object>> successList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_SUCCESS);
            List<Map<String, Object>> ignoreList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_IGNORE);
            List<Map<String, Object>> failedList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_FAILED);
            List<Map<String, Object>> updatedList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_UPDATED);
            String errorHtml = failedList.size() + "条";

            if (failedList.size() > 0) {
                //生成失败记录文件
                String fileName = "隐患信息批量导入" + batchNo + ".xls";
                String filePath = PropertiesUtil.get("batchImportErrorPath");
                String realPath = request.getServletContext().getRealPath(filePath);
                filePath = filePath.replace("\\", "/");
                File f = new File(realPath);
                if (!f.exists()) {
                    f.mkdirs();
                }
                realPath = realPath + File.separator + fileName;
                exportExcelByTemplate(fieldMapping,failedList, realPath,"隐患批量导入模板.xls");
                String downloadPath = filePath + "/" + URLEncoder.encode(fileName,"UTF-8");
                errorHtml = "<a href='#WEBROOT#/downloadErrorRecord?path=" + downloadPath + "'>" + failedList.size() + "条（点击查看失败原因）</a>";
            }
            out.print("本次导入共处理" + dataList.size() + "条数据[新增：" + successList.size() + "条，更新："
                    + updatedList.size() + "条，<font color='red'>失败：" + errorHtml
                    + "</font>]，耗时： "
                    + (endTime - startTime) / 1000 + "秒！");
        } catch (Exception e) {
            out.print("文件导入失败！");
            logger.error(e.getMessage());
        }
        out.flush();
        out.close();
    }

    //批量上传整改照片
    @RequestMapping("/uploadRectifyPic")
    @ResponseBody
    public void uploadRectifyPic(HiddenDanger hiddenDanger, HttpServletRequest request, HttpServletResponse response) throws Exception{
        PrintWriter out = response.getWriter();
        try{
            hiddenDanger.setCreator(this.getLoginId(request.getSession()));
            String res = hiddenDangerService.uploadRectifyPic(hiddenDanger);
            out.print(res);
        }catch (Exception e) {
            out.print("照片导入失败！");
            logger.error(e.getMessage());
        }
        out.flush();
        out.close();
    }
}
