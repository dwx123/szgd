package com.szgd.controller.home;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.szgd.dao.ecdata.elseData.TunnelMapper;
import com.szgd.service.elseData.PitService;
import com.szgd.service.elseData.TunnelService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.szgd.bean.Remind;
import com.szgd.controller.BaseController;
import com.szgd.service.equipment.EquipEnterExitService;
import com.szgd.service.gatherEquip.CameraService;
import com.szgd.service.hiddenDanger.HiddenDangerService;
import com.szgd.service.personnel.EnterExitService;
import com.szgd.service.personnel.RemindService;
import com.szgd.service.project.SiteService;
import com.szgd.service.schedule.AccessorStructureScheduleService;
import com.szgd.service.schedule.MainWorkScheduleService;
import com.szgd.service.schedule.SegmentProduceScheduleService;
import com.szgd.service.schedule.TunnellingWorkScheduleService;
import com.szgd.service.vehicle.VehicleEnterExitService;
import com.szgd.timer.RemindTimer;
import com.szgd.util.PropertiesUtil;
import com.szgd.util.SetLog;

@Controller
@RequestMapping(value = "/main")
public class MainController extends BaseController {
    private static Logger logger = Logger.getLogger(MainController.class);

    @Autowired
    SiteService siteService;
    @Autowired
    MainWorkScheduleService mainWorkScheduleService;
    @Autowired
    SegmentProduceScheduleService segmentProduceScheduleService;
    @Autowired
    TunnellingWorkScheduleService tunnellingWorkScheduleService;
    @Autowired
    AccessorStructureScheduleService accessorStructureScheduleService;
    @Autowired
    EnterExitService enterExitService;
    @Autowired
    VehicleEnterExitService vehicleEnterExitService;
    @Autowired
    EquipEnterExitService equipEnterExitService;
    @Autowired
    RemindTimer remindTimer;
    @Autowired
    HiddenDangerService hiddenDangerService;
    @Autowired
    PitService pitService;
    @Autowired
    TunnelService tunnelService;
    @Autowired
    RemindService remindService;
    @Autowired
    CameraService cameraService;
    @SetLog(opt="登录系统")
    @RequestMapping("/toMain")
    public ModelAndView toMain(HttpSession session) throws IOException, URISyntaxException{
        String roleIds = this.getUserMap(session).get("roleIds")==null?null:this.getUserMap(session).get("roleIds").toString();
        ModelAndView mav = null;
        
        String GetSuzDataFlag = PropertiesUtil.get("GetSuzDataFlag");
        
        if (roleIds != null && roleIds.indexOf("screeadmins") > -1 )
        {
            List<Map<String, Object>> siteList  = siteService.getSiteList();
            if("1".equals(GetSuzDataFlag)){
                mav = new ModelAndView("/home2-suz");
            }else{
            	mav = new ModelAndView("/home2");
                mav.addObject("siteList",siteList);
            }

            //苏州外部项目测试
            /*String imgUrl = "http://61.155.204.87:8888/jsp/ValidateCodeServlet";
            String image = OcrUtil.getCode(HttpUtils.downLoadJpgFromOtherSystem(imgUrl));
            mav.addObject("image",image);*/
        }else
        {
            mav = new ModelAndView("/main");
        }

        return mav;
    }
    @RequestMapping("/home")
    public ModelAndView toHome(){
        System.out.println("----------------------");
        ModelAndView mav = new ModelAndView("/home");
        return mav;
    }

    @RequestMapping("/home2")
    public ModelAndView toHome2(){
        System.out.println("----------------------");
        ModelAndView mav = new ModelAndView("/home2");
        return mav;
    }

    /**
     * 总的项目进度百分比
     * @param response
     * @param request
     */
    @RequestMapping(value = "/getTotalSchedulePercentage", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object>  getTotalSchedulePercentage(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);


        //维护结构百分比
        String projectName = "围护结构";
        paramMap.put("projectName",projectName);
        Double envelopePercentage = mainWorkScheduleService.mainProjectCompleteRateAvg(paramMap);
        //土方开挖百分比
        projectName = new String("土方开挖");
        paramMap.put("projectName",projectName);
        Double excavationPercentage = mainWorkScheduleService.mainProjectCompleteRateAvg(paramMap);
        //主体结构百分比
        projectName = new String("主体结构");
        paramMap.put("projectName",projectName);
        Double mainPercentage = mainWorkScheduleService.mainProjectCompleteRateAvg(paramMap);
        //盾构施工进度百分比
        Double tunnellingPercentage = tunnellingWorkScheduleService.tunnellingCompleteRateAvg(paramMap);
        //附属结构施工进度百分比
        Double accessorPercentage = accessorStructureScheduleService.accessorCompleteRateAvg(paramMap);

        DecimalFormat df = new DecimalFormat("0.#");
        Map<String, Object> schedulePercentageMap = new HashMap<>();
        schedulePercentageMap.put("envelopePercentage",df.format(envelopePercentage));
        schedulePercentageMap.put("excavationPercentage",df.format(excavationPercentage));
        schedulePercentageMap.put("mainPercentage",df.format(mainPercentage));
        schedulePercentageMap.put("tunnellingPercentage",df.format(tunnellingPercentage));
        schedulePercentageMap.put("accessorPercentage",df.format(accessorPercentage));

        return schedulePercentageMap;
    }

    /**
     * 大屏显示人员进出
     * @param response
     * @param request
     */
    @RequestMapping(value = "/getPersonnelEnterExitList", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object>  getPersonnelEnterExitList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);

        int days = Integer.parseInt(PropertiesUtil.get("PresentDays"));
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance(); //得到日历
        calendar.setTime(new Date());//把当前时间赋给日历
        calendar.add(Calendar.DAY_OF_MONTH, -1*days);  //设置为前一天
        String beginPassTime = sdf1.format(calendar.getTime());

        paramMap.put("rownum",PropertiesUtil.get("EnterExitRecentRecordNum"));
        paramMap.put("enterDateFrom",beginPassTime);
        List<Map<String, Object>> enterExitList = enterExitService.getTopNEnter_ExitList(paramMap);

        paramMap.put("beginPassTime",beginPassTime);
        int personnelBePresentCount = enterExitService.getPersonnelBePresentCount(paramMap);//在场数量
/*        List<Map<String, Object>> resultEnterExitList = new ArrayList<>();
        for (int i = 0; i < rownum; i++) {
            resultEnterExitList.add(enterExitList.get(i));
        }*/
        paramMap.put("personnelEnterExitList",enterExitList);
        paramMap.put("personnelBePresentCount",personnelBePresentCount);
        return paramMap;
    }


    /**
     * 大屏显示车辆进出
     * @param response
     * @param request
     */
    @RequestMapping(value = "/getVehicleEnterAndExitList", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object>  getVehicleEnterAndExitList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);

        int days = Integer.parseInt(PropertiesUtil.get("PresentDays"));
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance(); //得到日历
        calendar.setTime(new Date());//把当前时间赋给日历
        calendar.add(Calendar.DAY_OF_MONTH, -1*days);  //设置为前一天
        String beginPassTime = sdf1.format(calendar.getTime());

        paramMap.put("enterDateFrom",beginPassTime);
        paramMap.put("rownum",PropertiesUtil.get("EnterExitRecentRecordNum"));
        List<Map<String, Object>> enterExitList = vehicleEnterExitService.getTopNVehicleEnter_ExitList(paramMap);

        paramMap.put("beginPassTime",beginPassTime);
        int vehicleBePresentCount = vehicleEnterExitService.getVehicleBePresentCount(paramMap);//在场数量
        paramMap.put("vehicleEnterExitList",enterExitList);
        paramMap.put("vehicleBePresentCount",vehicleBePresentCount);
        return paramMap;
    }

    /**
     * 大屏显示设备
     * @param response
     * @param request
     */
    @RequestMapping(value = "/getEquipEnterAndExitList", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object>  getEquipEnterAndExitList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        paramMap.put("rownum",PropertiesUtil.get("EquipEnterExitRecentRecordNum"));
        List<Map<String, Object>> enterExitList = equipEnterExitService.getTopNEquipEnter_ExitList(paramMap);
        //int equipBePresentCount = equipEnterExitService.getEquipBePresentCount(paramMap);//在场数量
        paramMap.put("equipEnterExitList",enterExitList);
        //paramMap.put("equipBePresentCount",equipBePresentCount);
        return paramMap;
    }

    /**
     * 大屏显示隐患信息
     * @param response
     * @param request
     */
    @RequestMapping(value = "/getHiddenDangerInfoList", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object>  getHiddenDangerInfoList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        paramMap.put("rownum",PropertiesUtil.get("EquipEnterExitRecentRecordNum"));
        paramMap.put("excludeStatus","CONFIRM");
        List<Map<String, Object>> hiddenDangerList = hiddenDangerService.getHiddenDangerListNoPage(paramMap);
        //int equipBePresentCount = equipEnterExitService.getEquipBePresentCount(paramMap);//在场数量
        paramMap.put("hiddenDangerList",hiddenDangerList);
        //paramMap.put("equipBePresentCount",equipBePresentCount);
        return paramMap;
    }

    /**
     * 大屏显示基坑监测数据
     * @param response
     * @param request
     */
    @RequestMapping(value = "/getPitInfoList", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object>  getPitInfoList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        paramMap.put("rownum",PropertiesUtil.get("ElseDataRecordNum"));
        List<Map<String, Object>> pitList = pitService.getTopPitList(paramMap);
        paramMap.put("pitList",pitList);
        return paramMap;
    }

    /**
     * 大屏显示盾构机数据
     * @param response
     * @param request
     */
    @RequestMapping(value = "/getTunnelInfoList", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object>  getTunnelInfoList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        paramMap.put("rownum",PropertiesUtil.get("ElseDataRecordNum"));
        List<Map<String, Object>> tunnelList = tunnelService.getTopTunnelList(paramMap);
        paramMap.put("tunnelList",tunnelList);
        return paramMap;
    }

    /**
     * 大屏显示每天提醒
     * @param response
     * @param request
     */
    @RequestMapping(value = "/getRemindCount", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object>  getRemindCount(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        String certificationRemindCount  = remindTimer.getCertificationRemindCount();
        String peoplesRemindCount  = remindTimer.getPeoplesRemindCount();
        String physicalExaminationRemindCount  = remindTimer.getPhysicalExaminationRemindCount();
        String equipmentChkRemindCount  = remindTimer.getEquipmentChkRemindCount();
        String equipmentInsureRemindCount  = remindTimer.getEquipmentInsureRemindCount();

        paramMap.put("excludeStatus","CONFIRM");
        int hiddenDangerCount = hiddenDangerService.getHiddenDangerCount(paramMap);

        paramMap.put("certificationRemindCount",certificationRemindCount);
        //paramMap.put("peoplesRemindCount",peoplesRemindCount);
        paramMap.put("physicalExaminationRemindCount",physicalExaminationRemindCount);
        paramMap.put("equipmentChkRemindCount",equipmentChkRemindCount);
        paramMap.put("equipmentInsureRemindCount",equipmentInsureRemindCount);
        //paramMap.put("hiddenDangerCount",hiddenDangerCount);

        return paramMap;
    }

    /**
     * 大屏显示及时提醒
     * @param response
     * @param request
     */
    @RequestMapping(value = "/getRemindCount_Timely", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object>  getRemindCount_Timely(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        String certificationRemindCount  = remindTimer.getCertificationRemindCount();
        String peoplesRemindCount  = remindTimer.getPeoplesRemindCount();
        String physicalExaminationRemindCount  = remindTimer.getPhysicalExaminationRemindCount();
        String equipmentChkRemindCount  = remindTimer.getEquipmentChkRemindCount();
        String equipmentInsureRemindCount  = remindTimer.getEquipmentInsureRemindCount();

        paramMap.put("excludeStatus","CONFIRM");
        int hiddenDangerCount = hiddenDangerService.getHiddenDangerCount(paramMap);

        //paramMap.put("certificationRemindCount",certificationRemindCount);
        paramMap.put("peoplesRemindCount",peoplesRemindCount);
        //paramMap.put("physicalExaminationRemindCount",physicalExaminationRemindCount);
        //paramMap.put("equipmentChkRemindCount",equipmentChkRemindCount);
        //paramMap.put("equipmentInsureRemindCount",equipmentInsureRemindCount);
        paramMap.put("hiddenDangerCount",hiddenDangerCount);

        return paramMap;
    }

    /**
     * 大屏显示摄像头
     * @param response
     * @param request
     */
    @RequestMapping(value = "/getCameraList", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object>  getCameraList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        paramMap.put("webOrder","true");
        List<Map<String, Object>> cameraList = cameraService.getCameraInfoListNoPage(paramMap);
        paramMap.put("cameraList",cameraList);
        return paramMap;
    }

    /**
     * 大屏显示跑马灯
     * @param response
     * @param request
     */
    @RequestMapping(value = "/getMarquee", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object>  getMarquee(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);

        paramMap.put("remindType","PEOPLES_REMIND");
        paramMap.put("beginTime","2019-06-21 17:00:00");
        List<Remind>  remindList = remindService.getRemindDataNoPage(paramMap);

        String marquee = null;
        for (int i = 0; i < remindList.size(); i++) {
            Remind remind = remindList.get(i);
            String content = remind.getContent();
            if (marquee == null)
                marquee = content;
            else
                marquee = marquee + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +content;

        }

        paramMap.put("excludeStatus","CONFIRM");
        List<Map<String,Object>> hiddenDangerList = hiddenDangerService.getHiddenDangerListNoPage(paramMap);

        for (int i = 0; i < hiddenDangerList.size(); i++) {
            Map<String,Object> hiddenDangerMap = hiddenDangerList.get(i);
            String theme = hiddenDangerMap.get("theme")==null?"":hiddenDangerMap.get("theme").toString();
            String siteName = hiddenDangerMap.get("siteName")==null?"":hiddenDangerMap.get("siteName").toString();
            String statusName = hiddenDangerMap.get("statusName")==null?"":hiddenDangerMap.get("statusName").toString();
            if (marquee == null)
                marquee = siteName + "的&nbsp;[" +  theme + "]&nbsp;处于" + statusName + "状态";
            else
                marquee = marquee + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +siteName + "的&nbsp;[" +  theme + "]&nbsp;处于" + statusName + "状态";

        }
        paramMap.put("marquee",marquee);
        return paramMap;
    }
}
