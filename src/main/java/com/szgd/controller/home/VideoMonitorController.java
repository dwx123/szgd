package com.szgd.controller.home;

import com.szgd.controller.BaseController;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/videoMonitor")
public class VideoMonitorController extends BaseController {
    private static Logger logger = Logger.getLogger(VideoMonitorController.class);
    @RequestMapping(value = "toCameraList", method = RequestMethod.GET)
    public ModelAndView toCameraList(HttpServletRequest request, HttpSession session) {
        ModelAndView mv = this.getModelAndView(request,session,"videoMonitor/videoMonitor");
        return mv;
    }
}
