package com.szgd.controller.personnel;

import com.szgd.controller.BaseController;
import com.szgd.service.personnel.AttachService;
import com.szgd.service.personnel.CertificateService;
import com.szgd.service.personnel.PersonnelInfoService;
import com.szgd.util.FileUtil;
import com.szgd.util.PropertiesUtil;
import com.szgd.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping(value = "/attach")
public class AttachController extends BaseController {

    @Autowired
    AttachService  attachService;


    @RequestMapping(value = "/saveAttach")
    @ResponseBody
    public Map<String,Object> saveAttach(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        try {
            paramMap.put("creator",this.getLoginId(request.getSession()));
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            attachService.saveAttach(paramMap);
            return getSuccMap("附件保存成功！");
        }catch(Exception e){
            e.printStackTrace();
            return getSuccMap("附件保存失败："+e.getMessage());
            //throw new Exception("人员信息保存失败："+e.getMessage());
        }
    }


    @RequestMapping(value = "/showAttach", method = RequestMethod.GET)
    @ResponseBody
    public String showAttach(HttpServletRequest request, HttpServletResponse response) {

        try {

            //String picPath = URLDecoder.decode(request.getParameter("fileName"),"utf-8");
            String id = request.getParameter("id");
            OutputStream outputStream = response.getOutputStream();
            response.setContentType("application/octet-stream ");
            response.setHeader("Connection", "close"); // 表示不能用浏览器直接打开
            response.setHeader("Accept-Ranges", "bytes");// 告诉客户端允许断点续传多线程连接下载
            response.setContentType("multipart/form-data");
            response.setContentType("UTF-8");

            Map<String, Object>  attachMap = attachService.getAttach(id);
            String subPath = attachMap.get("path").toString();
            String fileName = attachMap.get("name").toString();
            String fileUploadPath = PropertiesUtil.get("uploadPath");
            String filePath = FileUtil.existFile(fileUploadPath,subPath,fileName);
            InputStream inputStream = new FileInputStream(filePath +  File.separator + fileName);
            int len = 0;
            byte[] buf = new byte[1024];
            while ((len = inputStream.read(buf, 0, 1024)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
