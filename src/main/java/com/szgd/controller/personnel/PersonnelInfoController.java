package com.szgd.controller.personnel;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.szgd.bean.Certificate;
import com.szgd.bean.MessageBean;
import com.szgd.bean.PersonnelInfo;
import com.szgd.bean.SysLog;
import com.szgd.controller.BaseController;
import com.szgd.controller.sys.LoginController;
import com.szgd.service.gatherEquip.GatherEquipService;
import com.szgd.service.personnel.*;
import com.szgd.service.project.SiteService;
import com.szgd.service.sys.SysLogService;
import com.szgd.service.sys.UserService;
import com.szgd.util.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;

@Controller
@RequestMapping(value = "/personnelInfo")
public class PersonnelInfoController extends BaseController {

    @Autowired
    PersonnelInfoService personnelInfoService;
    @Autowired
    GatherEquipService gatherEquipService;
    @Autowired
    PersonnelFaceIdService personnelFaceIdService;
    @Autowired
    UserService userService;
    @Autowired
    CertificateService certificateService;
    @Autowired
    AttachService attachService;
    @Autowired
    PersonnelGatherEquipService personnelGatherEquipService;
    @Autowired
    SiteService siteService;
    @Autowired
    PersonnelBidService personnelBidService;
    private static Logger logger = Logger.getLogger(PersonnelInfoController.class);
    @RequestMapping(value = "toPersonnelInfoList", method = RequestMethod.GET)
    public ModelAndView toPersonnelInfoList(HttpServletRequest request, HttpSession session) {
        ModelAndView mv = getModelAndView(request,session,"/personnel/personnelInfo/personnelInfoList");
        mv.addObject("peopleGatherEquipList",gatherEquipService.getPeopleGatherEquipList());

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("isStarted",1);
        paramMap.put("isFinished",0);
        paramMap.put("orderType","web");
        mv.addObject("startedSiteList",siteService.getStartedSiteList(paramMap));
        return mv;
    }

    @RequestMapping(value = "toPersonnelInfoForm", method = RequestMethod.GET)
    public ModelAndView toPersonnelInfoForm(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/personnel/personnelInfo/personnelInfoForm");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");;
        if (id.length() > 0)//修改
        {
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("personnelId",id);
            List<Map<String, Object>>  list = personnelBidService.getPersonnelBidListNoPage(paramsMap);
            String bidNames = null;
            for (Map<String, Object> map:list) {
                String bidName = map.get("bidName").toString();
                if (bidNames == null)
                    bidNames = bidName;
                else
                    bidNames = bidNames + "," + bidName;
            }

            Map<String, Object> personnelInfoMap = personnelInfoService.getPersonnelInfo(id,getUserId(session));
            personnelInfoMap.put("bidNames",bidNames);

            Map<String, Object> certificateMap = certificateService.getCertificateParentId(id);

            mv.addObject("personnelInfo",personnelInfoMap);
            mv.addObject("certificate",certificateMap);

        }

        return mv;
    }

    @RequestMapping(value = "toPersonnelInfoView", method = RequestMethod.GET)
    public ModelAndView toPersonnelInfoView(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/personnel/personnelInfo/personnelInfoView");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");;
        if (id.length() > 0)//修改
        {
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("personnelId",id);
            List<Map<String, Object>>  list = personnelBidService.getPersonnelBidListNoPage(paramsMap);
            String bidNames = null;
            for (Map<String, Object> map:list) {
                String bidName = map.get("bidName").toString();
                if (bidNames == null)
                    bidNames = bidName;
                else
                    bidNames = bidNames + "," + bidName;
            }

            Map<String, Object> personnelInfoMap = personnelInfoService.getPersonnelInfo(id,getUserId(session));
            personnelInfoMap.put("bidNames",bidNames);

            Map<String, Object> certificateMap = certificateService.getCertificateParentId(id);
            mv.addObject("personnelInfo",personnelInfoMap);
            mv.addObject("certificate",certificateMap);
        }

        return mv;
    }
    @RequestMapping(value = "/getPersonnelInfoListNoPage")
    @ResponseBody
    public  Map<String, Object>  getPersonnelInfoListNoPage(@RequestBody HashMap<String, Object> paramMap,HttpServletResponse response,HttpServletRequest request,HttpSession session){
        paramMap.put("userId",getUserId(session));
        List<Map<String, Object>> dataList = personnelInfoService.getPersonnelInfoListNoPage(paramMap);
        return getSuccMap(dataList);
    }

    @RequestMapping(value = "/getPersonnelInfoList", method = RequestMethod.GET)
    public void  getPersonnelInfoList(HttpServletResponse response,
                                     HttpServletRequest request,HttpSession session){
        Map<String, Object> paramMap = getMapParams(request);
        paramMap.put("userId",getUserId(session));
        List<Map<String, Object>> dataList = personnelInfoService.getPersonnelInfoList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/savePersonnelInfo")
    @ResponseBody
    public Map<String,Object> savePersonnelInfo(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String name = paramMap.get("name")== null?"":paramMap.get("name").toString().replace("null","");
        String idNumber = paramMap.get("idNumber")== null?"":paramMap.get("idNumber").toString().replace("null","");
        String personId = paramMap.get("personId")== null?"":paramMap.get("personId").toString().replace("null","");
        String faceId = paramMap.get("faceId")== null?"":paramMap.get("faceId").toString().replace("null","");
        HashMap<String, Object> certificateMap = (HashMap<String, Object>)paramMap.get("certificate");
        HashMap<String, Object> headPictureAttachMap = (HashMap<String, Object>)paramMap.get("headPictureAttach");
        HashMap<String, Object> certificateAttachMap = (HashMap<String, Object>)paramMap.get("certificateAttach");

        List<String> bidIdList = new ArrayList<>();
        Object idObject = paramMap.get("bidId");
        if (idObject != null)
        {
            if (idObject instanceof String) {
                bidIdList.add(idObject.toString());
            } else if (idObject instanceof ArrayList) {
                bidIdList = (ArrayList<String>) idObject;
            }
        }

        String message = "";
        boolean isSavePersonnelInfo = false;
        boolean isSaveCertificate = false;
        try {


            certificateMap.put("creator",this.getLoginId(request.getSession()));
            certificateMap.put("uploader",this.getLoginId(request.getSession()));
            boolean isHaveSamePersonnelInfo = personnelInfoService.isHaveSamePersonnelInfo(name,idNumber);
            if (id.length() == 0)//新增
            {
                paramMap.put("creator",this.getLoginId(request.getSession()));
                if (isHaveSamePersonnelInfo)
                {
                    logger.error("人员信息保存失败：数据库已存在该员工");
                    return getErrorMap("人员信息保存失败：数据库已存在该员工");
                }

                //同时向小麦人脸识别注册人员 begin
                /*String personnelToOtherSystemFlag = PropertiesUtil.get("PersonnelToOtherSystemFlag");//是否开通了向人脸识别设备注册功能
                if (personnelToOtherSystemFlag.equalsIgnoreCase("1"))
                {
                    personId = Pinyin4j .getLowerCase(name+TimeUtil.format(new Date(),"yyyyMMdd"),true);
                    String idNumber = paramMap.get("idNumber")==null?"":paramMap.get("idNumber").toString();
                    String result = personnelInfoService.personnelCreateToOtherSystem(personId,idNumber,name);
                    if (!personId.equalsIgnoreCase(result))//表示失败
                    {
                        message = StringUtil.splitJoint(message,result);
                        logger.error(message);
                        paramMap.put("personId",null);
                    }else
                        paramMap.put("personId",personId);
                }*/
                ////同时向小麦人脸识别注册人员 end

                personnelInfoService.insertPersonnelInfo(paramMap);

                isSavePersonnelInfo = true;
                try{
                    certificateMap.put("personnelId",paramMap.get("id"));
                    certificateService.insertCertificate(certificateMap);
                    isSaveCertificate = true;
                }catch(Exception e){
                    e.printStackTrace();
                    message = "证书保存失败，请重新编辑此信息的证书再保存";
                    logger.error("证书保存失败，请重新编辑此信息的证书再保存:人员ID："+id+"---"+e.getMessage());
                }

            } else {//修改
                paramMap.put("uploader",this.getLoginId(request.getSession()));
                personnelInfoService.updatePersonnelInfo(paramMap);

                //判断用户名是否改变，如果改变则需要同步更新到人脸识别
                if (!isHaveSamePersonnelInfo)//如果没有说明修改了姓名
                {
                    ////同时向小麦人脸识别更新人员 begin
                    /*String personnelToOtherSystemFlag = PropertiesUtil.get("PersonnelToOtherSystemFlag");//是否开通了向人脸识别设备注册功能
                    if (personnelToOtherSystemFlag.equalsIgnoreCase("1"))
                    {
                        personId = Pinyin4j .getLowerCase(name+TimeUtil.format(new Date(),"yyyyMMdd"),true);
                        String idNumber = paramMap.get("idNumber")==null?"":paramMap.get("idNumber").toString();
                        String result = personnelInfoService.personnelUpdateToOtherSystem(personId,idNumber,name);
                        if (!personId.equalsIgnoreCase(result))//表示失败
                        {
                            message = StringUtil.splitJoint(message,result);
                            logger.error(message);
                        }else
                        {
                            paramMap.put("personId",personId);
                            personnelInfoService.updateOtherSystemId(paramMap);
                        }
                    }*/
                    ////同时向小麦人脸识别更新人员 end
                }

                String certificateId = certificateMap.get("id")== null?"":certificateMap.get("id").toString().replace("null","");;
                try{
                    if (certificateId.length() == 0)//新增
                    {
                        certificateMap.put("personnelId",id);
                        certificateService.insertCertificate(certificateMap);
                    }else
                    {
                        certificateService.updateCertificate(certificateMap);
                    }
                    isSaveCertificate = true;
                }catch(Exception e){
                    e.printStackTrace();
                    message = "证书保存失败，请重新编辑此信息再保存";
                    logger.error("证书保存失败，请重新编辑此信息再保存:人员ID："+id+"---"+e.getMessage());
                }
            }
            //插入人员与标段关联
            personnelBidService.deletePersonnelBidEX(paramMap.get("id").toString());
            for (int i = 0; bidIdList != null && i < bidIdList.size(); i++) {
                personnelBidService.insertPersonnelBidEX(paramMap.get("id").toString(),this.getLoginId(request.getSession()),bidIdList.get(i));
            }
            //更新人员所属标段数量
            personnelInfoService.updateBidCount(paramMap.get("id").toString(),bidIdList.size());

            try{
                headPictureAttachMap.put("creator",this.getLoginId(request.getSession()));
                headPictureAttachMap.put("uploader",this.getLoginId(request.getSession()));
                String isHeadPictureChanged = headPictureAttachMap.get("isHeadPictureChanged").toString();
                if (isHeadPictureChanged.equalsIgnoreCase("1"))//图片已经改变
                {
                    String parentId = headPictureAttachMap.get("parentId")== null?"":headPictureAttachMap.get("parentId").toString().replace("null","");;
                    if (parentId.length() == 0)
                        headPictureAttachMap.put("parentId",paramMap.get("id"));

                    Map<String, Object> attachMap = attachService.saveAttach(headPictureAttachMap);
                    String fullPath= attachMap.get("fullPath").toString();

                    /*String personnelToOtherSystemFlag = PropertiesUtil.get("PersonnelToOtherSystemFlag");//是否开通了向人脸识别设备注册功能
                    if (personnelToOtherSystemFlag.equalsIgnoreCase("1"))//向小麦注册照片
                    {
                        String imgBase64 = ImageUtil.getImageStr(fullPath);
                        if (imgBase64.length() > 0)
                        {
                            String result = "";
                            if (faceId.length() > 0)
                                result = personId;//修改基本信息不向小麦更新照片，如果要更新，可以通过更新图片按钮更新。personnelInfoService.faceUpdateToOtherSystem(personId,personId,imgBase64);
                            else
                                result = personnelInfoService.faceCreateToOtherSystem(personId,personId,imgBase64);
                            if (!personId.equalsIgnoreCase(result))//表示失败
                            {
                                message = StringUtil.splitJoint(message,result);
                                logger.info(message);
                            }else
                            {
                                paramMap.put("faceId",personId);
                                personnelInfoService.updateOtherSystemId(paramMap);
                            }
                        }else
                        {
                            message = StringUtil.splitJoint(message,"向人脸识别设备注册照片失败，错误原因是没找到上传的图片文件");
                            logger.info(message);
                        }

                    }*/
                }
            }catch(Exception e){
                e.printStackTrace();
                message = message + ",头像保存失败，请重新编辑此信息再保存";
                logger.error("头像保存失败，请重新编辑此信息再保存:人员ID："+id+"---"+e.getMessage());
            }

            certificateAttachMap.put("creator",this.getLoginId(request.getSession()));
            certificateAttachMap.put("uploader",this.getLoginId(request.getSession()));
            try{
                String isCertificateAttachChanged = certificateAttachMap.get("isCertificateAttachChanged").toString();
                if (isCertificateAttachChanged.equalsIgnoreCase("1") && isSaveCertificate)
                {
                    String parentId = certificateAttachMap.get("parentId")== null?"":certificateAttachMap.get("parentId").toString().replace("null","");;
                    if (parentId.length() == 0)
                        certificateAttachMap.put("parentId",certificateMap.get("id"));
                    attachService.saveAttach(certificateAttachMap);
                }
            }catch(Exception e){
                e.printStackTrace();
                message = message + ",证书保存失败，请重新编辑此信息再保存";
                logger.error("证书保存失败，请重新编辑此信息再保存:人员ID："+id+"---"+e.getMessage());
            }
            if (message.length() > 0 )
                message = "人员信息保存成功！" + " 同时" + message;
            else
                message = "人员信息保存成功！";
            return getSuccMap(message);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("人员信息保存失败："+e.getMessage());
            return getErrorMap("人员信息保存失败："+e.getMessage());
            //throw new Exception("人员信息保存失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/delPersonnelInfo")
    @ResponseBody
    public Map<String,Object> delPersonnelInfo(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        String id = paramMap.get("id") == null ? "0" : paramMap.get("id").toString().replace("null","0");
        String certificateId = paramMap.get("certificateId") == null ? "0" : paramMap.get("certificateId").toString().replace("null","0");
        String gatherEquipId = paramMap.get("gatherEquipId") == null ? "0" : paramMap.get("gatherEquipId").toString().replace("undefined","0").replace("null","0");

        try{
            String message = "";

            /*HashMap<String, Object> tempMap = new HashMap<>();
            tempMap.put("parentId",id);
            tempMap.put("source","ATTACH_PERSONNEL_INFO");
            attachService.deleteAttach(tempMap);

            tempMap.put("parentId",certificateId);
            tempMap.put("source","ATTACH_CERTIFICATE");
            attachService.deleteAttach(tempMap);

            HashMap<String, Object> tempMap1 = new HashMap<>();
            tempMap1.put("id",certificateId);
            certificateService.deleteCertificate(tempMap1);
*/
            HashMap<String, Object> tempMap1 = new HashMap<>();
            tempMap1.put("id",id);
            tempMap1.put("delflag",1);
            personnelInfoService.updatePersonnelInfo(tempMap1);

            ////同时向小麦人脸识别删除人员 begin
            String personnelToOtherSystemFlag = PropertiesUtil.get("PersonnelToOtherSystemFlag");//是否开通了向人脸识别设备注册功能
            if (personnelToOtherSystemFlag.equalsIgnoreCase("1"))
            {
                /*Map<String, Object>  personnelInfo = personnelInfoService.getPersonnelInfo(id);
                String personId = personnelInfo.get("personId")==null?"":personnelInfo.get("personId").toString();*/
                List<Map<String, Object>> personnelFaceIdList = personnelFaceIdService.getPersonnelFaceIdListByPersonnelIdAndSiteId(id,null);
                for (int i = 0; i < personnelFaceIdList.size(); i++) {
                    Map<String, Object> personnelFaceIdMap = personnelFaceIdList.get(i);

                    String isFinished = personnelFaceIdMap.get("isFinished")==null?"":personnelFaceIdMap.get("isFinished").toString();
                    if (isFinished.equalsIgnoreCase("1"))//如果站点已完工，不用删除注册，跳过
                        continue;
                    String siteId = personnelFaceIdMap.get("site").toString();
                    String siteName = personnelFaceIdMap.get("siteName").toString();
                    String personId = personnelFaceIdMap.get("personId")==null?"":personnelFaceIdMap.get("personId").toString();


                    if (personId.length() > 0)
                    {
                        String result = personnelInfoService.personnelDeleteToOtherSystem(id,personId,"",siteId);
                        if (result.length() > 0)//表示失败
                        {
                            message = StringUtil.splitJoint(message,result);
                            logger.error(message);
                        }else
                        {
                            message = StringUtil.splitJoint(message,"此人员在"+siteName+"站点中的人脸注册信息删除成功！");
                            logger.error(message);
                        }

                        HashMap<String, Object> tempMap = new HashMap<String, Object>();
                        tempMap.put("personnelId",id);
                        tempMap.put("siteId",siteId);
                        int count = personnelGatherEquipService.getCount(tempMap);
                        if (count == 0)//说明此站点所属设备都删除成功了
                        {
                            paramMap.put("personId",null);
                            paramMap.put("faceId",null);
                            //personnelInfoService.updateOtherSystemId(paramMap);
                            paramMap.put("personnelId",id);
                            paramMap.put("siteId",siteId);
                            personnelFaceIdService.updatePersonnelFaceIdNull(paramMap);

                            HashMap<String, Object> siteUserMap  = new HashMap<String, Object>();
                            siteUserMap.put("PERSONNEL_ID",id);
                            siteUserMap.put("SITE_ID",siteId);
                            userService.deleteSiteUser(siteUserMap);
                        }
                    }
                }

            }
            ////end

            if (message.length() > 0 )
                message = "人员信息删除成功！" + " 同时" + message;
            else
                message = "人员信息删除成功！";
            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("人员信息删除失败："+e.getMessage());
            return getSuccMap("人员信息删除失败："+e.getMessage());
        }

    }

    @ResponseBody
    @RequestMapping(value = "/batchUploadPersonnelInfo", method = {RequestMethod.GET, RequestMethod.POST})
    public void batchUploadPersonnelInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        long startTime = System.currentTimeMillis();
        InputStream in = null;
        List<List<Object>> listob = null;
        MultipartFile file = multipartRequest.getFile("file");
        if (file.isEmpty()) {
            throw new Exception("文件不存在！");
        }
        in = file.getInputStream();
        PrintWriter out = null;
        response.setCharacterEncoding("UTF-8");  //防止ajax接受到的中文信息乱码
        response.setHeader("Content-type", "text/html;charset=UTF-8");   //跟上面同时使用可解决乱码问题
        out = response.getWriter();
        try {

            Map<String, String> fieldMapping = personnelInfoService.getFieldMap();
            List<Map<String, Object>> dataList = ExcelUtil.getListByExcel(in, file.getOriginalFilename(), fieldMapping, false);
            String batchNo = TimeUtil.formatDate(Calendar.getInstance().getTime(), "yyyyMMddhhmmss");
            Map<String, Object> resultMap = personnelInfoService.batchImportPersonnelInfo(dataList, this.getLoginId(request.getSession()), batchNo);

            long endTime = System.currentTimeMillis();
            List<Map<String, Object>> successList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_SUCCESS);
            List<Map<String, Object>> ignoreList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_IGNORE);
            List<Map<String, Object>> failedList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_FAILED);
            List<Map<String, Object>> updatedList = (List<Map<String, Object>>) resultMap.get(Constant.IMPORT_HE_UPDATED);
            String errorHtml = failedList.size() + "条";

            if (failedList.size() > 0) {
                //生成失败记录文件
                String fileName = "人员信息批量导入" + batchNo + ".xls";
                String filePath = PropertiesUtil.get("batchImportErrorPath");
                String realPath = request.getServletContext().getRealPath(filePath);
                filePath = filePath.replace("\\", "/");
                File f = new File(realPath);
                if (!f.exists()) {
                    f.mkdirs();
                }
                realPath = realPath + File.separator + fileName;
                exportExcelByTemplate(fieldMapping,failedList, realPath,"人员信息批量导入模版.xls");
                String downloadPath = filePath + "/" + URLEncoder.encode(fileName,"UTF-8");
                errorHtml = "<a href='#WEBROOT#/downloadErrorRecord?path=" + downloadPath + "'>" + failedList.size() + "条（点击查看失败原因）</a>";
            }
            out.print("本次导入共处理" + dataList.size() + "条数据[新增：" + successList.size() + "条，更新："
                    + updatedList.size() + "条，<font color='red'>失败：" + errorHtml
                    + "</font>]，耗时： "
                    + (endTime - startTime) / 1000 + "秒！");
        } catch (Exception e) {
            out.print("文件导入失败！");
            logger.error(e.getMessage());
        }
        out.flush();
        out.close();
    }

    /***********下载人员台账*********/
    @ResponseBody
    @RequestMapping(value = "/downloadPersonLog", method = {RequestMethod.GET, RequestMethod.POST})
    public void downloadPersonLog(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, String> fieldMapping = personnelInfoService.getLogMap();
        String fileName = "人员台账.xls";
        String filePath = PropertiesUtil.get("downloadPath");
        String realPath = request.getServletContext().getRealPath(filePath);
        filePath = filePath.replace("\\", "/");
        File f = new File(realPath);
        if (!f.exists()) {
            f.mkdirs();
        }
        realPath = realPath + File.separator + fileName;
        List<Map<String, Object>> dataList = personnelInfoService.getPersonnelLog();
        exportExcelByTemplate(fieldMapping,dataList, realPath,"人员信息批量导入模版.xls");
        String downloadPath = filePath + "/" + fileName;
        realPath = request.getServletContext().getRealPath(downloadPath);
        try {
            this.download(response, realPath);
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        new File(realPath).delete();
    }

    @RequestMapping(value = "/getCertificateList", method = RequestMethod.GET)
    public void  getCertificateList(HttpServletResponse response,HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = certificateService.getCertificateList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    /**
     * 注册人员信息
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/personnelCreateToOtherSystem")
    @ResponseBody
    public Map<String,Object> personnelCreateToOtherSystem(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        String id = paramMap.get("id") == null ? "0" : paramMap.get("id").toString().replace("null","0");
        String ip = paramMap.get("ip") == null ? "" : paramMap.get("ip").toString().replace("null","");
        String gatherEquipId = paramMap.get("gatherEquipId") == null ? "0" : paramMap.get("gatherEquipId").toString().replace("undefined","0").replace("null","0");
        String siteId = paramMap.get("siteId") == null ? null : paramMap.get("siteId").toString().replace("null","");

        try{
            String message = "";
            Map<String, Object> personnelInfo= personnelInfoService.getPersonnelInfo(id,getUserId(request.getSession()));
            String name =personnelInfo.get("name").toString();
            //同时向小麦人脸识别注册人员 begin
            String personnelToOtherSystemFlag = PropertiesUtil.get("PersonnelToOtherSystemFlag");//是否开通了向人脸识别设备注册功能
            if (personnelToOtherSystemFlag.equalsIgnoreCase("1"))
            {
                //String personId = id;//Pinyin4j .getLowerCase(name+TimeUtil.format(new Date(),"yyyyMMdd"),true);
                String sPersonId = null;
                HashMap<String, Object> tempMap = new HashMap<String, Object>();
                tempMap.put("personnelId",id);
                tempMap.put("siteId",siteId);
                List<Map<String, Object>> personnelFaceIdList = personnelFaceIdService.getPersonnelFaceIdListNoPage(tempMap);
                if (personnelFaceIdList != null && personnelFaceIdList.size() > 0)
                {
                    String tempPersonId = personnelFaceIdList.get(0).get("personId")==null?null:personnelFaceIdList.get(0).get("personId").toString();
                    if (tempPersonId == null)
                    {
                        int personId = personnelFaceIdService.getMaxPersonId(siteId);
                        sPersonId = String.valueOf(personId+1);//原来登记号加1
                    }else
                        sPersonId = tempPersonId;
                }
                else
                {
                    int personId = personnelFaceIdService.getMaxPersonId(siteId);
                    sPersonId = String.valueOf(personId+1);//原来登记号加1
                }

                String idNumber = personnelInfo.get("idNumber")==null?"":personnelInfo.get("idNumber").toString();
                String result = personnelInfoService.personnelCreateToOtherSystem(id,sPersonId,idNumber,name,ip,this.getLoginId(request.getSession()),siteId);
                //if (!personId.equalsIgnoreCase(result))//表示失败
                if (result.length() > 0)//表示失败
                {
                    message = StringUtil.splitJoint(message,result);
                    logger.error(message);
                    //paramMap.put("personId",null);
                }else
                {
                    paramMap.put("personId",sPersonId);
                    //personnelInfoService.updatePersonId(paramMap);
                    tempMap = new HashMap<String, Object>();
                    tempMap.put("personnelId",id);
                    tempMap.put("siteId",siteId);
                    int count = personnelFaceIdService.getCount(tempMap);
                    if (count == 0)//如果用户和人脸设备登记ID表中没有记录，则插入
                    {
                        String tempPersonnelId = id;
                        String tempPersonId  = sPersonId;
                        String creator = this.getLoginId(request.getSession());
                        personnelFaceIdService.insertPersonnelFaceIdEx(tempPersonnelId,tempPersonId,null,creator,siteId);

                        HashMap<String, Object> siteUserMap  = new HashMap<String, Object>();
                        siteUserMap.put("PERSONNEL_ID",id);
                        siteUserMap.put("SITE_ID",siteId);
                        userService.insertSiteUser(siteUserMap);


                    }else
                    {
                        tempMap.put("personId",sPersonId);
                        personnelFaceIdService.updatePersonId(tempMap);
                    }


                    message= "注册人员成功！";
                    logger.error(message);
                }
            }else
            {
                message = StringUtil.splitJoint(message,"暂停使用");
            }
            ////同时向小麦人脸识别注册人员 end

            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("人员信息注册失败："+e.getMessage());
            return getSuccMap("人员信息注册失败："+e.getMessage());
        }

    }

    /**
     * 删除人员和照片
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/personnelDeleteToOtherSystem")
    @ResponseBody
    public Map<String,Object> personnelDeleteToOtherSystem(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        String id = paramMap.get("id") == null ? "0" : paramMap.get("id").toString().replace("null","0");
        String ip = paramMap.get("ip") == null ? "" : paramMap.get("ip").toString().replace("null","");
        String gatherEquipId = paramMap.get("gatherEquipId") == null ? "0" : paramMap.get("gatherEquipId").toString().replace("undefined","0").replace("null","0");
        String siteId = paramMap.get("siteId") == null ? null : paramMap.get("siteId").toString().replace("null","");

        try{
            String message = "";
            //向小麦人脸识别删除人员 begin
            String personnelToOtherSystemFlag = PropertiesUtil.get("PersonnelToOtherSystemFlag");//是否开通了向人脸识别设备注册功能
            if (personnelToOtherSystemFlag.equalsIgnoreCase("1"))
            {
                /*Map<String, Object>  personnelInfo = personnelInfoService.getPersonnelInfo(id);
                String personId = personnelInfo.get("personId")==null?"":personnelInfo.get("personId").toString();*/
                HashMap<String, Object> tempMap = new HashMap<String, Object>();
                tempMap.put("personnelId",id);
                tempMap.put("siteId",siteId);
                List<Map<String, Object>> personnelFaceIdList = personnelFaceIdService.getPersonnelFaceIdListNoPage(tempMap);
                if (personnelFaceIdList.size() > 0)
                {
                    Map<String, Object> personnelFaceIdMap = personnelFaceIdList.get(0);
                    String siteName = personnelFaceIdMap.get("siteName").toString();
                    String personId = personnelFaceIdMap.get("personId")==null?"":personnelFaceIdMap.get("personId").toString();
                    if (personId.length() > 0)
                    {
                        String result = personnelInfoService.personnelDeleteToOtherSystem(id,personId,ip,siteId);
                        if (result.length() > 0)//表示失败
                        {
                            message = StringUtil.splitJoint(message,result);
                            logger.error(message);
                        }else
                        {

                            message= "删除注册成功！";
                            logger.error(message);

                        }
/*                        HashMap<String, Object> tempMap1 = new HashMap<String, Object>();
                        tempMap1.put("personnelId",id);*/
                        int count = personnelGatherEquipService.getCount(tempMap);
                        if (count == 0)
                        {
                            paramMap.put("personId",null);
                            paramMap.put("faceId",null);
                            //personnelInfoService.updateOtherSystemId(paramMap);
                            paramMap.put("personnelId",id);
                            paramMap.put("siteId",siteId);
                            personnelFaceIdService.updatePersonnelFaceIdNull(paramMap);

                            HashMap<String, Object> siteUserMap  = new HashMap<String, Object>();
                            siteUserMap.put("PERSONNEL_ID",id);
                            siteUserMap.put("SITE_ID",siteId);
                            userService.deleteSiteUser(siteUserMap);
                        }
                    }
                }else
                {
                    message = StringUtil.splitJoint(message,"没有注册人脸设备，无需删除注册信息");
                }

            }else
            {
                message = StringUtil.splitJoint(message,"暂停使用");
            }
            ////同时向小麦人脸识别注册人员 end
            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("人员注册信息注册失败："+e.getMessage());
            return getSuccMap("人员注册信息删除失败："+e.getMessage());
        }

    }

    /**
     * 注册人脸照片
     * @param paramMap
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/faceCreateToOtherSystem")
    @ResponseBody
    public Map<String,Object> faceCreateToOtherSystem(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        String id = paramMap.get("id") == null ? "0" : paramMap.get("id").toString().replace("null","0");
        String personId = paramMap.get("personId") == null ? "0" : paramMap.get("personId").toString().replace("null","0");
        String ip = paramMap.get("ip") == null ? "" : paramMap.get("ip").toString().replace("null","");
        String gatherEquipId = paramMap.get("gatherEquipId") == null ? "0" : paramMap.get("gatherEquipId").toString().replace("undefined","0").replace("null","0");
        String siteId = paramMap.get("siteId") == null ? null : paramMap.get("siteId").toString().replace("null","");
        try{
            String message = "";

            String personnelToOtherSystemFlag = PropertiesUtil.get("PersonnelToOtherSystemFlag");//是否开通了向人脸识别设备注册功能
            if (personnelToOtherSystemFlag.equalsIgnoreCase("1"))//向小麦注册照片
            {
                /*int personId = personnelFaceIdService.getMaxPersonId(siteId);
                String sPersonId = String.valueOf(personId+1);//原来登记号加1*/

                String uploadPath = PropertiesUtil.get("uploadPath");
                Map<String, Object> attach = attachService.getAttach(id,"ATTACH_PERSONNEL_INFO");
                String fullPath = uploadPath + File.separator + attach.get("path")+ File.separator +  attach.get("name");
                String imgBase64 = ImageUtil.getImageStr(fullPath);
                if (imgBase64.length() > 0)
                {
                    String result = "";
                    result = personnelInfoService.faceCreateToOtherSystem(id,personId,personId,imgBase64,ip,this.getLoginId(request.getSession()),siteId);
                    if (result.length() > 0)//表示失败
                    {
                        message = StringUtil.splitJoint(message,result);
                        logger.info(message);
                    }else
                    {
                        paramMap.put("faceId",personId);
                        //personnelInfoService.updateFaceId(paramMap);
                        HashMap<String, Object> tempMap = new HashMap<String, Object>();
                        tempMap.put("personnelId",id);
                        tempMap.put("faceId",personId);
                        tempMap.put("siteId",siteId);
                        personnelFaceIdService.updateFaceId(tempMap);
                        message= "注册图片成功！";
                        logger.error(message);
                    }
                }else
                {
                    message = StringUtil.splitJoint(message,"向人脸识别设备注册注册失败，错误原因是没找到上传的图片文件");
                    logger.info(message);
                }
            }else
            {
                message = StringUtil.splitJoint(message,"暂停使用");
            }

            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("照片创建失败："+e.getMessage());
            return getSuccMap("照片创建失败："+e.getMessage());
        }

    }

    @RequestMapping(value = "/faceUpdateToOtherSystem")
    @ResponseBody
    public Map<String,Object> faceUpdateToOtherSystem(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        String id = paramMap.get("id") == null ? "0" : paramMap.get("id").toString().replace("null","0");
        //String faceId = paramMap.get("faceId") == null ? "0" : paramMap.get("faceId").toString().replace("null","0");
        String ip = paramMap.get("ip") == null ? "" : paramMap.get("ip").toString().replace("null","");
        String siteId = paramMap.get("siteId") == null ? null : paramMap.get("siteId").toString().replace("null","");
        try{
            String message = "";
            //Map<String, Object>  personnelInfo = personnelInfoService.getPersonnelInfo(id);
            String personnelToOtherSystemFlag = PropertiesUtil.get("PersonnelToOtherSystemFlag");//是否开通了向人脸识别设备注册功能
            if (personnelToOtherSystemFlag.equalsIgnoreCase("1"))//向小麦注册照片
            {
                HashMap<String, Object> tempMap = new HashMap<String, Object>();
                tempMap.put("personnelId",id);
                tempMap.put("siteId",siteId);
                List<Map<String, Object>> personnelFaceIdList = personnelFaceIdService.getPersonnelFaceIdListNoPage(tempMap);
                if (personnelFaceIdList != null && personnelFaceIdList.size() > 0)
                {
                    /*String personId = personnelInfo.get("personId") == null?"":personnelInfo.get("personId").toString();
                    String faceId = personnelInfo.get("faceId") == null?"":personnelInfo.get("faceId").toString();*/
                    Map<String, Object> personnelFaceIdMap = personnelFaceIdList.get(0);
                    String personId = personnelFaceIdMap.get("personId")==null?"":personnelFaceIdMap.get("personId").toString();
                    String faceId = personnelFaceIdMap.get("faceId")==null?"":personnelFaceIdMap.get("faceId").toString();
                    String uploadPath = PropertiesUtil.get("uploadPath");
                    Map<String, Object> attach = attachService.getAttach(id,"ATTACH_PERSONNEL_INFO");
                    String fullPath = uploadPath + File.separator + attach.get("path")+ File.separator +  attach.get("name");
                    String imgBase64 = ImageUtil.getImageStr(fullPath);

                    if (imgBase64.length() > 0)
                    {
                        String result = "";
                        if (faceId.length() > 0)
                            result = personnelInfoService.faceUpdateToOtherSystem(personId,personId,imgBase64,ip,siteId);
                        else
                            result = personnelInfoService.faceCreateToOtherSystem(id,personId,personId,imgBase64,ip,this.getLoginId(request.getSession()),siteId);
                        if (result.length() > 0)//表示失败
                        {
                            message = StringUtil.splitJoint(message,result);
                            logger.info(message);
                        }else
                        {
                            //personnelInfo.put("faceId",personId);
                            //personnelInfoService.updateFaceId(personnelInfo);
                            tempMap = new HashMap<String, Object>();
                            tempMap.put("personnelId",id);
                            tempMap.put("faceId",personId);
                            tempMap.put("siteId",siteId);
                            personnelFaceIdService.updateFaceId(tempMap);
                            message= "更新图片成功！";
                            logger.error(message);
                        }
                    }else
                    {
                        message = StringUtil.splitJoint(message,"向人脸识别设备注册照片失败，错误原因是没找到上传的图片文件");
                        logger.info(message);
                    }
                }else
                {
                    message = StringUtil.splitJoint(message,"向人脸识别设备更新照片失败，错误原因是人员还没先注册好");
                    logger.info(message);
                }


            }else
            {
                message = StringUtil.splitJoint(message,"暂停使用");
            }

            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("照片更新失败："+e.getMessage());
            return getSuccMap("照片更新失败："+e.getMessage());
        }

    }

    @RequestMapping(value = "/batchPersonnelCreateToOtherSystem")
    @ResponseBody
    public Map<String,Object> batchPersonnelCreateToOtherSystem(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        logger.info("batchPersonnelCreateToOtherSystem:----"+new Gson().toJson(paramMap));
        String parmName = paramMap.get("name") == null ? "" : paramMap.get("name").toString().replace("null","");
        String paramIdNumber = paramMap.get("idNumber") == null ? "" : paramMap.get("idNumber").toString().replace("null","");
        String paramIp = paramMap.get("ip") == null ? "" : paramMap.get("ip").toString().replace("null","");
        String gatherEquipId = paramMap.get("gatherEquipId") == null ? "0" : paramMap.get("gatherEquipId").toString().replace("undefined","0").replace("null","0");
        String id = paramMap.get("id") == null ? "0" : paramMap.get("id").toString().replace("null","0");
        String siteId = paramMap.get("siteId") == null ? null : paramMap.get("siteId").toString().replace("null","");
        paramMap.put("isHavePersonId",0);
        paramMap.put("userId",getUserId(session));

        try{
            String message = "";
            List<Map<String, Object>> list = personnelInfoService.getPersonnelInfoListNoPage(paramMap);
            int sussNum = 0;
            int failNum = 0;
            int sussFaceNum = 0;
            int failFaceNum = 0;
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object>  personnelInfo = list.get(i);
                /*String personId = personnelInfo.get("personId")==null?"":personnelInfo.get("personId").toString();
                String faceId = personnelInfo.get("faceId")==null?"":personnelInfo.get("faceId").toString();*/
                String name =personnelInfo.get("name").toString();
                String idNumber =personnelInfo.get("idNumber").toString();
                /*if (personId.length() > 0)
                    continue;*/
                //同时向小麦人脸识别注册人员 begin
                String personnelToOtherSystemFlag = PropertiesUtil.get("PersonnelToOtherSystemFlag");//是否开通了向人脸识别设备注册功能
                if (personnelToOtherSystemFlag.equalsIgnoreCase("1"))
                {
                    //personId = personnelInfo.get("id").toString();//Pinyin4j .getLowerCase(name+TimeUtil.format(new Date(),"yyyyMMdd"),true);

                    String sPersonId = null;
                    HashMap<String, Object> tempMap = new HashMap<String, Object>();
                    tempMap.put("personnelId",id);
                    tempMap.put("siteId",siteId);
                    List<Map<String, Object>> personnelFaceIdList = personnelFaceIdService.getPersonnelFaceIdListNoPage(tempMap);
                    if (personnelFaceIdList != null && personnelFaceIdList.size() > 0)
                    {
                        String tempPersonId = personnelFaceIdList.get(0).get("personId")==null?null:personnelFaceIdList.get(0).get("personId").toString();
                        if (tempPersonId == null)
                        {
                            int personId = personnelFaceIdService.getMaxPersonId(siteId);
                            sPersonId = String.valueOf(personId+1);//原来登记号加1
                        }else
                            sPersonId = tempPersonId;
                    }
                    else
                    {
                        int personId = personnelFaceIdService.getMaxPersonId(siteId);
                        sPersonId = String.valueOf(personId+1);//原来登记号加1
                    }

                    /*int personId = personnelFaceIdService.getMaxPersonId(siteId);
                    String sPersonId = String.valueOf(personId+1);//原来登记号加1*/
                    //String idNumber = paramMap.get("idNumber")==null?"":paramMap.get("idNumber").toString();
                    //List<Map<String, Object>>  gatherEquipList = gatherEquipService.getPeopleGatherEquipList();
                    List<Map<String, Object>>  gatherEquipList = gatherEquipService.getPeopleGatherEquipListOfSite(siteId);
                    for (int j = 0; j < gatherEquipList.size(); j++) {

                        Map<String, Object> gatherEquipMap = gatherEquipList.get(j);
                        String password = gatherEquipMap.get("password")==null?"":gatherEquipMap.get("password").toString();
                        String ip = gatherEquipMap.get("ip").toString();
                        String tempSiteId = gatherEquipMap.get("siteId").toString();
                        if (!ip.equalsIgnoreCase(paramIp) && paramIp.length() > 0)
                            continue;
                        logger.info("向"+ip+"人脸识别设备注册人员信息");
                        String result = personnelInfoService.personnelCreateToOtherSystemByIp(id,sPersonId,idNumber,name,ip,password,gatherEquipMap.get("id").toString(),this.getLoginId(request.getSession()),tempSiteId);
                        if (!sPersonId.equalsIgnoreCase(result))//表示失败
                        {
                            message = StringUtil.splitJoint(message,result);
                            logger.error(message);
                            //personnelInfo.put("personId",null);
                            failNum ++;
                        }else
                        {
                            personnelInfo.put("personId",sPersonId);
                            sussNum ++;

                            //注册人员
                            tempMap = new HashMap<String, Object>();
                            tempMap.put("personnelId",id);
                            tempMap.put("siteId",tempSiteId);
                            int count = personnelFaceIdService.getCount(tempMap);
                            if (count == 0)//如果用户和人脸设备登记ID表中没有记录，则插入
                            {
                                String tempPersonnelId = id;
                                String tempPersonId  = sPersonId;
                                String creator = this.getLoginId(request.getSession());
                                personnelFaceIdService.insertPersonnelFaceIdEx(tempPersonnelId,tempPersonId,null,creator,tempSiteId);

                                HashMap<String, Object> siteUserMap  = new HashMap<String, Object>();
                                siteUserMap.put("PERSONNEL_ID",id);
                                siteUserMap.put("SITE_ID",tempSiteId);
                                userService.insertSiteUser(siteUserMap);
                            }else
                            {
                                tempMap.put("personId",sPersonId);
                                //tempMap.put("faceId",personnelInfo.get("faceId")==null?null:personnelInfo.get("faceId").toString());
                                personnelFaceIdService.updatePersonnelFaceId(tempMap);
                            }

                            //注册照片
                            String uploadPath = PropertiesUtil.get("uploadPath");
                            Map<String, Object> attach = attachService.getAttach(personnelInfo.get("id").toString(),"ATTACH_PERSONNEL_INFO");
                            String fullPath = uploadPath + File.separator + attach.get("path")+ File.separator +  attach.get("name");
                            String imgBase64 = ImageUtil.getImageStr(fullPath);
                            if (imgBase64.length() > 0)
                            {
                                result = "";
                                result = personnelInfoService.faceCreateToOtherSystemByIp(id,sPersonId,sPersonId,imgBase64,ip,password,gatherEquipMap.get("id").toString(),this.getLoginId(request.getSession()),tempSiteId);
                                //result = personnelInfoService.faceCreateToOtherSystem(personId,personId,imgBase64);
                                if (!sPersonId.equalsIgnoreCase(result))//表示失败
                                {
                                    result = "向"+ip+"人脸识别设备注册"+name+"图片失败：" + result;
                                    message = StringUtil.splitJoint(message,result);
                                    //personnelInfo.put("faceId",null);
                                    logger.info(message);
                                    failFaceNum++;
                                }else
                                {
                                    sussFaceNum++;
                                    personnelInfo.put("faceId",sPersonId);
                                    //personnelInfoService.updateFaceId(paramMap);
                                    tempMap = new HashMap<String, Object>();
                                    tempMap.put("personnelId",id);
                                    tempMap.put("faceId",sPersonId);
                                    tempMap.put("siteId",tempSiteId);
                                    personnelFaceIdService.updateFaceId(tempMap);
                                }
                            }else
                            {
                                failFaceNum++;
                                message = StringUtil.splitJoint(message,"向"+ip+"人脸识别设备注册"+name+"图片失败，错误原因是没找到上传的图片文件");
                                logger.info(message);
                            }
                            //personnelInfoService.updateOtherSystemId(personnelInfo);

                        }

                    }

                }else
                {
                    message = StringUtil.splitJoint(message,"暂停使用");
                }
            }


            if (sussNum==0)
            {
                message = StringUtil.splitJoint("所选设备全部注册失败：",message);
            }else if (failNum == 0 && failFaceNum == 0)
            {
                message = "所选设备全部注册成功";
            }else
            {
                message = StringUtil.splitJoint("所选设备部分注册成功，其中：",message);
            }
            ////同时向小麦人脸识别注册人员 end

            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("人员信息注册失败："+e.getMessage());
            return getSuccMap("人员信息注册失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/getRectifyPerson")
    @ResponseBody
    public Map<String,Object> getRectifyPerson(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        String siteId = paramMap.get("siteId")==null?"":paramMap.get("siteId").toString().replace("null","");
        if(siteId.length() == 0){
            paramMap.put("siteId",null);
        }
        Map<String,Object> data = new HashMap<>();
        data.put("special",personnelInfoService.getPersonBySite(paramMap));
        data.put("all",personnelInfoService.getAllRectifyPerson());
        return data;
    }
    
    /**
	 * 获取人员数据给用户提供
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/getPersonnelByUser")
	public void getRemindData(HttpServletRequest request, HttpServletResponse response) {
		outTableJSONDataCommon(response, personnelInfoService.getPersonnelByUser(getMapParams(request)));
	}

    @RequestMapping(value = "/getPersonnelFaceIdByPersonnelIdAndSiteId")
    @ResponseBody
    public Map<String,Object> getPersonnelFaceIdByPersonnelIdAndSiteId(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> paramMap = getMapParams(request);
        String id = paramMap.get("id")==null?null:paramMap.get("id").toString().replace("null","");
        String siteId = paramMap.get("siteId")==null?null:paramMap.get("siteId").toString().replace("null","");
        List<Map<String, Object>> personnelFaceIdList = personnelFaceIdService.getPersonnelFaceIdListByPersonnelIdAndSiteId(id,siteId);
        return getSuccMap(personnelFaceIdList);
    }

    //批量上传证书照片
    @RequestMapping("/importCertificate")
    @ResponseBody
    public void importCertificate(Certificate certificate, HttpServletRequest request, HttpServletResponse response) throws Exception{
        PrintWriter out = response.getWriter();
        try{
            certificate.setCreator(this.getLoginId(request.getSession()));
            certificate.setUserId(this.getUserId(request.getSession()));
            String res = personnelInfoService.importCertificate(certificate);
            out.print(res);
        }catch (Exception e) {
            out.print("照片导入失败！");
            logger.error(e.getMessage());
        }
        out.flush();
        out.close();
    }
}
