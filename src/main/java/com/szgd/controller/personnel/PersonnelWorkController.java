package com.szgd.controller.personnel;

import com.szgd.controller.BaseController;
import com.szgd.controller.sys.LoginController;
import com.szgd.service.personnel.*;
import com.szgd.service.project.ProjectService;
import com.szgd.service.project.WorkSiteService;
import com.szgd.util.PropertiesUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/personnelWork")
public class PersonnelWorkController extends BaseController {

    @Autowired
    EnterExitService enterExitService;
    @Autowired
    ViolationService violationService;
    @Autowired
    PersonnelInfoService personnelInfoService;
    @Autowired
    ProjectService projectService;
    @Autowired
    WorkSiteService workSiteService;
    private static Logger logger = Logger.getLogger(PersonnelWorkController.class);

    @RequestMapping(value = "toPersonnelWork", method = RequestMethod.GET)
    public ModelAndView toPersonnelWork(HttpServletRequest request,HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"personnel/personnelWork/personnelWork");
        Map<String, Object> temp = new HashMap<>();
        temp.put("userId",getUserId(session));
        List<Map<String, Object>>  personnelInfoList = personnelInfoService.getPersonnelInfoListNoPage(temp);
        List<Map<String, Object>>  projectList = projectService.getProjectListNoPage(null);
        List<Map<String, Object>>  workSiteList = workSiteService.getWorkSiteListNoPage(null);
        mv.addObject("personnelInfoList",personnelInfoList);
        mv.addObject("projectList",projectList);
        mv.addObject("workSiteList",workSiteList);
        return mv;
    }
    @RequestMapping(value = "toPersonnelEnterExitList")
    public ModelAndView toPersonnelEnterExitList(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        ModelAndView mv = getModelAndView(request,session,"personnel/personnelWork/personnelEnterExitList");
        return mv;
    }

    /*
    跳转领导带班记录页面
     */
    @RequestMapping(value = "toLeaderShiftRecord")
    public ModelAndView toLeaderShiftRecord(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        ModelAndView mv = getModelAndView(request,session,"personnel/leaderShiftRecord/leaderShiftRecord");
        return mv;
    }

    /*
    跳转早晚查记录页面
     */
    @RequestMapping(value = "toMNRecord")
    public ModelAndView toMNRecord(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        ModelAndView mv = getModelAndView(request,session,"personnel/morningAndNightCheck/morningAndNightCheck");
        return mv;
    }

    @RequestMapping(value = "toEnterExitList")
    public ModelAndView toEnterExitList(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        ModelAndView mv = getModelAndView(request,session,"personnel/personnelWork/personnelEnterExitList");
        return mv;
    }

    @RequestMapping(value = "toViolationList")
    public ModelAndView toViolationList(HttpServletRequest request,HttpServletResponse response,HttpSession session) {
        Map<String, Object> temp = new HashMap<>();
        temp.put("userId",getUserId(session));
        List<Map<String, Object>>  personnelInfoList = personnelInfoService.getPersonnelInfoListNoPage(temp);
        ModelAndView mv = getModelAndView(request,session,"personnel/personnelWork/personnelViolationList");
        mv.addObject("personnelInfoList",personnelInfoList);
        return mv;
    }
    @RequestMapping(value = "/getEnterAndExitList", method = RequestMethod.GET)
    public void  getEnterExitList(HttpServletResponse response,
                                     HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = enterExitService.getEnterAndExitList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/getEnter_ExitList", method = RequestMethod.GET)
    public void  getEnter_ExitList(HttpServletResponse response,
                                  HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = enterExitService.getEnter_ExitList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/getViolationList", method = RequestMethod.GET)
    public void  getViolationList(HttpServletResponse response,
                                      HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = violationService.getViolationList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

/*    @RequestMapping(value = "toViolationForm", method = RequestMethod.GET)
    public String toViolationForm() {
        return "personnel/personnelWork/violationForm";
    }*/


    @RequestMapping(value = "/saveViolation")
    @ResponseBody
    public Map<String,Object> saveViolation(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        try {
            paramMap.put("creator",this.getLoginId(request.getSession()));
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            if (id.length() == 0)//新增
            {
                violationService.insertViolation(paramMap);

            } else {
                violationService.updateViolation(paramMap);
            }

            return getSuccMap("违法记录保存成功！");
        }catch(Exception e){
            e.printStackTrace();
            return getSuccMap("违法记录保存失败："+e.getMessage());
            //throw new Exception("人员信息保存失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/delViolation")
    @ResponseBody
    public Map<String,Object> delViolation(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        String id = paramMap.get("id") == null ? "0" : paramMap.get("id").toString().replace("null","0");
        try{
            HashMap<String, Object> tempMap1 = new HashMap<>();
            tempMap1.put("id",id);
            violationService.deleteViolation(tempMap1);
            String message = "违规删除成功！";
            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("人员信息删除失败："+e.getMessage());
            return getSuccMap("人员信息删除失败："+e.getMessage());
        }

    }

    /*
    导出人员进出记录
     */
    @RequestMapping("/downloadRecord")
    public void downloadRecord(HttpServletRequest request, HttpServletResponse response) throws Exception{
        Map<String,Object> map = this.getMapParams(request);
        Map<String, String> fieldMapping = enterExitService.getFieldMap();
        String fileName = "人员进出记录.xls";
        String filePath = PropertiesUtil.get("downloadPath");
        String realPath = request.getServletContext().getRealPath(filePath);
        filePath = filePath.replace("\\", "/");
        File f = new File(realPath);
        if (!f.exists()) {
            f.mkdirs();
        }
        realPath = realPath + File.separator + fileName;
        List<Map<String, Object>> dataList = enterExitService.getEnter_ExitListNoPage(map);
        exportExcelByTemplate(fieldMapping,dataList, realPath,"人员进出记录导出模板.xls");
        String downloadPath = filePath + "/" + fileName;
        realPath = request.getServletContext().getRealPath(downloadPath);
        try {
            this.download(response, realPath);
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        new File(realPath).delete();

    }

    /*
    查询领导带班统计记录
    */
    @RequestMapping(value = "/getLeaderRecord", method = RequestMethod.GET)
    public void  getLeaderRecord(HttpServletResponse response,
                                   HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = enterExitService.getLeaderRecord(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    /*
    导出领导带班记录
     */
    @RequestMapping("/downloadLeaderRecord")
    public void downloadLeaderRecord(HttpServletRequest request, HttpServletResponse response) throws Exception{
        Map<String,Object> map = this.getMapParams(request);
        Map<String, String> fieldMapping = enterExitService.getLeaderFieldMap();
        Map<String, String> fieldMapping2 = enterExitService.getFieldMap();
        String fileName = "领导带班记录.xls";
        String filePath = PropertiesUtil.get("downloadPath");
        String realPath = request.getServletContext().getRealPath(filePath);
        filePath = filePath.replace("\\", "/");
        File f = new File(realPath);
        if (!f.exists()) {
            f.mkdirs();
        }
        realPath = realPath + File.separator + fileName;
        List<Map<String, Object>> dataList = enterExitService.getLeaderRecordNoPage(map);
        List<Map<String, Object>> dataList2 = enterExitService.getEnter_ExitListNoPage(map);
        exportExcel(fieldMapping,fieldMapping2,dataList,dataList2, realPath,"领导带班记录导出模板.xls");
        String downloadPath = filePath + "/" + fileName;
        realPath = request.getServletContext().getRealPath(downloadPath);
        try {
            this.download(response, realPath);
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        new File(realPath).delete();
    }

    /*
    查询早晚查统计记录
    */
    @RequestMapping(value = "/getMNRecord", method = RequestMethod.GET)
    public void  getMNRecord(HttpServletResponse response,HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = enterExitService.getMNRecord(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    /*
    导出早晚查记录
     */
    @RequestMapping("/downloadMNRecord")
    public void downloadMNRecord(HttpServletRequest request, HttpServletResponse response) throws Exception{
        Map<String,Object> map = this.getMapParams(request);
        Map<String, String> fieldMapping = enterExitService.getMNFieldMap();
        Map<String, String> fieldMapping2 = enterExitService.getFieldMap();
        String fileName = "早晚查记录.xls";
        String filePath = PropertiesUtil.get("downloadPath");
        String realPath = request.getServletContext().getRealPath(filePath);
        filePath = filePath.replace("\\", "/");
        File f = new File(realPath);
        if (!f.exists()) {
            f.mkdirs();
        }
        realPath = realPath + File.separator + fileName;
        map.put("flag","mn");
        List<Map<String, Object>> dataList = enterExitService.getMNRecordNoPage(map);
        List<Map<String, Object>> dataList2 = enterExitService.getEnter_ExitListNoPage(map);
        exportExcel(fieldMapping,fieldMapping2,dataList,dataList2, realPath,"早晚查记录导出模板.xls");
        String downloadPath = filePath + "/" + fileName;
        realPath = request.getServletContext().getRealPath(downloadPath);
        try {
            this.download(response, realPath);
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        new File(realPath).delete();
    }
}
