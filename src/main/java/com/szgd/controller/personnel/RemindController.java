package com.szgd.controller.personnel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.szgd.bean.Remind;
import com.szgd.service.sys.WfRoleService;
import com.szgd.service.vehicle.VehicleService;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.szgd.bean.RemindCfg;
import com.szgd.bean.SysDict;
import com.szgd.controller.BaseController;
import com.szgd.service.personnel.RemindCfgService;
import com.szgd.service.personnel.RemindService;
import com.szgd.service.sys.SysDictService;
import com.szgd.util.BusinessName;
import com.szgd.util.StringUtil;

@Controller
@RequestMapping(value = "/remind")
public class RemindController extends BaseController {
	@Autowired
	protected RemindService remindService;
	@Autowired
	protected RemindCfgService remindCfgService;
	@Autowired
	protected SysDictService sysDictService;
	@Autowired
	WfRoleService wfRoleService;
	@Autowired
	VehicleService vehicleService;

	private static Logger logger = Logger.getLogger(RemindController.class);


	@InitBinder    
	public void initBinder(WebDataBinder binder) {    
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));    
	}

	/**
	 * 人员资质提醒跳转页面
	 * 
	 * @param request
	 * @return
	 */
	
	@RequestMapping("/certification-list")
	public ModelAndView certificationList(HttpServletRequest request,HttpSession session) {
		System.out.println("----------------------");

		ModelAndView mav = getModelAndView(request,session,"personnel/remind/remind-list") ;
		mav.addObject("pageFlag", BusinessName.ZZTX.getValue());
		return mav;
	}
	
	/**
	 * 人员人数提醒跳转页面
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/peoples-list")
	public ModelAndView peoplesList(HttpServletRequest request,HttpSession session) {
		System.out.println("----------------------");
		ModelAndView mav = getModelAndView(request,session,"personnel/remind/remind-list") ;
		mav.addObject("pageFlag", BusinessName.RSTX.getValue());
		Map<String, Object> mapParams = getMapParams(request);
		mapParams.put("isStarted",1);
		mav.addObject("gds", remindCfgService.getWorkSiteInfo(mapParams));
		return mav;
	}

	/**
	 * 人员次数提醒跳转页面
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping("/peoples-month-list")
	public ModelAndView peoplesMonthList(HttpServletRequest request,HttpSession session) {
		System.out.println("----------------------");
		ModelAndView mav = getModelAndView(request,session,"personnel/remind/remind-list") ;
		mav.addObject("pageFlag", BusinessName.TSTX.getValue());
		Map<String, Object> mapParams = getMapParams(request);
		mapParams.put("isStarted",1);
		mav.addObject("gds", remindCfgService.getWorkSiteInfo(mapParams));
		return mav;
	}

	/**
	 * 车辆监理旁站提醒列表
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping("/vehicle-on-site-supervise-list")
	public ModelAndView vehicleOnSiteSuperviseList(HttpServletRequest request,HttpSession session) {
		System.out.println("----------------------");
		ModelAndView mav = getModelAndView(request,session,"personnel/remind/remind-list") ;
		mav.addObject("pageFlag", BusinessName.JLPZTX.getValue());
		Map<String, Object> mapParams = getMapParams(request);
		mapParams.put("isStarted",1);
		mav.addObject("gds", remindCfgService.getWorkSiteInfo(mapParams));
		return mav;
	}

	/**
	 * 人员体检提醒跳转页面
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/examination-list")
	public ModelAndView examinationList(HttpServletRequest request,HttpSession session) {
		System.out.println("----------------------");
		ModelAndView mav = getModelAndView(request,session,"personnel/remind/remind-list") ;
		mav.addObject("pageFlag", BusinessName.TJTX.getValue());
		return mav;
	}

	/**
	 * 设备检测提醒跳转页面
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping("/check-list")
	public ModelAndView checkList(HttpServletRequest request,HttpSession session) {
		System.out.println("----------------------");
		ModelAndView mav = getModelAndView(request,session,"personnel/remind/remind-list") ;
		mav.addObject("pageFlag", BusinessName.SBJCTX.getValue());
		return mav;
	}

	/**
	 * 设备保险提醒跳转页面
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping("/insurance-list")
	public ModelAndView insuranceList(HttpServletRequest request,HttpSession session) {
		System.out.println("----------------------");
		ModelAndView mav = getModelAndView(request,session,"personnel/remind/remind-list") ;
		mav.addObject("pageFlag", BusinessName.SBBXTX.getValue());
		return mav;
	}

	/**
	 * 项目进度提醒跳转页面
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping("/schedule-list")
	public ModelAndView scheduleList(HttpServletRequest request,HttpSession session) {
		System.out.println("----------------------");

		ModelAndView mav = getModelAndView(request,session,"personnel/remind/remind-list") ;
		mav.addObject("pageFlag", BusinessName.XMJDTX.getValue());
		return mav;
	}

	/**
	 * 提醒设置跳转页面
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/remind-cfg")
	public ModelAndView remindCfg(HttpServletRequest request) {
		System.out.println("----------------------");
		ModelAndView mav = new ModelAndView("/personnel/remind/remind-cfg");
		mav.addObject("txlxs", sysDictService.getByParentCode(BusinessName.TXCODE.getValue()));
		mav.addObject("pageFlag", BusinessName.ZZTX.getValue());
		return mav;
	}
	
	/**
	 * 
	 * @param request
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/remindCfg-detail")
   	public ModelAndView remindCfgDetail(HttpServletRequest request,HttpSession session) throws Exception{
    	Map<String, Object> mapParams = getMapParams(request);
   		ModelAndView mav = new ModelAndView("/personnel/remind/remind-cfg-detail");
   		String modalFlag = mapParams.get("modalFlag") == null ? "" : mapParams.get("modalFlag").toString();
   		String txlx = mapParams.get("txlx") == null ? "" : mapParams.get("txlx").toString();
   		String pzid = mapParams.get("pzid") == null ? "" : mapParams.get("pzid").toString();
   		List<SysDict> workTypeList = sysDictService.getByParentCode("WORK_TYPE");
        List<Map<String,Object>> roleList = wfRoleService.getAllWfRoles(null);
   		if(StringUtil.isNotBlank(pzid)){
   			mav.addObject("tjpz", remindCfgService.selectByPrimaryKey(Long.valueOf(pzid)));
   		}
   		mav.addObject("txlx", txlx);
   		mav.addObject("modalFlag", modalFlag);
		mapParams.put("isStarted",1);
   		mav.addObject("gds", remindCfgService.getWorkSiteInfo(mapParams));
   		mav.addObject("txtss", sysDictService.getByParentCode(BusinessName.TX_DAY_CODE.getValue()));
   		mav.addObject("workTypeList", workTypeList);
		mav.addObject("roleList", roleList);
		List<Map<String, Object>> vehicleTypeList = vehicleService.getOnSiteSuperviseVehicleList();
		mav.addObject("vehicleTypeList", vehicleTypeList);
   		return mav;
   	}
	
	/**
	 * 获取提醒数据
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/getRemindData")
	public void getRemindData(HttpServletRequest request, HttpServletResponse response) {
		outTableJSONDataCommon(response, remindService.getRemindData(getMapParams(request)));
	}

	@RequestMapping(value = "/getRemindDataNoPage")
	public void getRemindDataNoPage(HttpServletRequest request, HttpServletResponse response) {
		outTableJSONDataCommon(response, remindService.getRemindDataNoPage(getMapParams(request)));
	}


	/**
	 * 获得datatable配置的数标题
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/getRemindCfgTitle",method={RequestMethod.GET,RequestMethod.POST},produces="text/html;charset=UTF-8")
	@ResponseBody
	public String getRemindCfgTitle(HttpServletRequest request){
    	JSONObject obj = new JSONObject();
    	obj.put("colList", remindCfgService.getRemindCfgTitle(getMapParams(request)));
    	return obj.toString();
	}
    
    /**
     * 获得datatable配置的数据
     * @param request
     * @return
     */
    @RequestMapping(value="/getRemindCfgData")
	@ResponseBody
	public String getRemindCfgData(HttpServletRequest request){
    	JSONObject obj = new JSONObject();
    	obj.put("data", remindCfgService.getRemindCfgData(getMapParams(request)));
    	return obj.toString();
	}
    
    /**
     * 提醒配置保存
     * @param rc
     * @param request
     * @param session
     * @return
     * @throws Exception
     */
    @RequestMapping("/saveTxpz")
   	public String saveTxpz(RemindCfg rc,HttpServletRequest request,HttpSession session,RedirectAttributes attr) throws Exception{
    	remindCfgService.saveTxpz(rc, request, session);
		attr.addFlashAttribute("remindType", rc.getRemindType());//跳转地址不带上u2参数
   		return "redirect:/remind/remind-cfg";
   	}
    
    /**
     * 提醒配置删除
     * @param pzId
     * @param
     * @return
     */
    @RequestMapping(value = "/remind-cfg-delete/{pzId}")
	public String remindCfgDelete(@PathVariable("pzId") String pzId) {
    	remindCfgService.deleteByPrimaryKey(Long.valueOf(pzId));
		return "redirect:/remind/remind-cfg";
	}

}
