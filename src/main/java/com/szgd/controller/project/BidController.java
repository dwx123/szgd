package com.szgd.controller.project;

import com.szgd.controller.BaseController;
import com.szgd.service.project.BidService;
import com.szgd.service.project.BidService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/bid")
public class BidController extends BaseController {
    private static Logger logger = Logger.getLogger(BidController.class);

    @Autowired
    BidService bidService;

    @RequestMapping(value = "toBidList", method = RequestMethod.GET)
    public ModelAndView toBidList(HttpServletRequest request, HttpSession session) {
        ModelAndView mv = this.getModelAndView(request,session,"project/bid/bidList");
        return mv;
    }

    @RequestMapping(value = "toBidView", method = RequestMethod.GET)
    public ModelAndView toBidView(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/project/bid/bidView");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0)//修改
        {
            Map<String, Object> bidInfoMap = bidService.getBid(id);
            mv.addObject("bid",bidInfoMap);
        }
        return mv;
    }

    @RequestMapping(value = "toBidForm", method = RequestMethod.GET)
    public ModelAndView toBidForm(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/project/bid/bidForm");
        Map<String, Object> bidInfoMap  = new HashMap<>();
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0)//修改
        {
            bidInfoMap = bidService.getBid(id);
        }
        bidInfoMap.put("lineNo",6);
        mv.addObject("bid",bidInfoMap);
        return mv;
    }

    @RequestMapping(value = "/getBidList", method = RequestMethod.GET)
    public void  getBidList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = bidService.getBidList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/saveBid")
    @ResponseBody
    public Map<String,Object> saveBid(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String message = "";
        try {
            Map<String, Object> param1 = new HashMap<>();
            param1.put("lineNo",paramMap.get("lineNo"));
            param1.put("name",paramMap.get("name"));
            List<Map<String, Object>>  bidList = bidService.getBidListNoPage(param1);
            if (bidList != null && bidList.size() > 0)
            {
                boolean isHave = true;
                if (id.length() != 0)
                {
                    Map<String, Object> bidMap = bidList.get(0);
                    String tempId = bidMap.get("id").toString();
                    if (id.equalsIgnoreCase(tempId))
                    {
                        isHave = false;
                    }
                }
                if (isHave)
                {
                    message = "标段表里已经存在相同的线路和标段!";
                    return getErrorMap(message);
                }

            }

            if (id.length() == 0)//新增
            {

                paramMap.put("creator",this.getLoginId(request.getSession()));
                bidService.insertBid(paramMap);
                message = "标段信息新增成功！";
            } else {
                paramMap.put("uploader",this.getLoginId(request.getSession()));
                bidService.updateBid(paramMap);
                message = "标段信息保存成功！";
            }
            return getSuccMap(message);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("标段信息保存失败："+e.getMessage());
            return getSuccMap("标段信息保存失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/deleteBid", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> deleteBid(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        try{
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            bidService.updateBid(paramMap);

            String message = "标段删除成功！";
            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("标段删除失败："+e.getMessage());
            return getSuccMap("标段删除失败："+e.getMessage());
        }
    }
}
