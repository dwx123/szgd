package com.szgd.controller.project;

import com.szgd.controller.BaseController;

import com.szgd.service.project.ProjectScheduleConfigService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/projectScheduleConfig")
public class ProjectScheduleConfigController extends BaseController {
    @Autowired
    ProjectScheduleConfigService projectScheduleConfigService;

    private static Logger logger = Logger.getLogger(ProjectScheduleConfigController.class);

    @RequestMapping(value = "toProjectScheduleConfigList", method = RequestMethod.GET)
    public ModelAndView toProjectScheduleConfigList(HttpServletRequest request, HttpSession httpSession) {
        ModelAndView mv = this.getModelAndView(request,httpSession,"project/projectScheduleConfig/projectScheduleConfigList");
        return mv;
    }

    @RequestMapping(value = "toProjectScheduleConfigForm", method = RequestMethod.GET)
    public ModelAndView toProjectScheduleConfigForm(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/project/projectScheduleConfig/projectScheduleConfigForm");

        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0)//修改
        {
            Map<String,Object> projectScheduleConfigMap = projectScheduleConfigService.getProjectScheduleConfig(id);
            mv.addObject("projectScheduleConfig",projectScheduleConfigMap);
        }


        return mv;
    }

    @RequestMapping(value = "/getProjectScheduleConfigList", method = RequestMethod.GET)
    public void  getProjectScheduleConfigList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = projectScheduleConfigService.getProjectScheduleConfigList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/saveProjectScheduleConfig")
    @ResponseBody
    public Map<String,Object> saveProjectScheduleConfig(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");

        String message;
        try {
            if (id.length() == 0){//新增
                paramMap.put("creator",this.getLoginId(request.getSession()));
                projectScheduleConfigService.insertProjectScheduleConfig(paramMap);

            } else {
                paramMap.put("uploader",this.getLoginId(request.getSession()));
                projectScheduleConfigService.updateProjectScheduleConfig(paramMap);
            }
            message = "项目进度配置保存成功!";
            return getSuccMap(message);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("项目进度配置保存失败："+e.getMessage());
            return getSuccMap("项目进度配置保存失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/deleteProjectScheduleConfig")
    @ResponseBody
    public Map<String,Object> deleteProjectScheduleConfig(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        try{
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            projectScheduleConfigService.deleteProjectScheduleConfig(paramMap);
            String message = "项目进度配置删除成功！";
            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("项目进度配置删除失败："+e.getMessage());
            return getSuccMap("项目进度配置删除失败："+e.getMessage());
        }
    }

}
