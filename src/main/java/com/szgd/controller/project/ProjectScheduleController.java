package com.szgd.controller.project;

import com.szgd.bean.Schedule;
import com.szgd.controller.BaseController;
import com.szgd.service.project.ProjectScheduleService;
import com.szgd.service.project.ProjectService;
import com.szgd.util.BusinessName;
import com.szgd.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/projectSchedule")
public class ProjectScheduleController extends BaseController {
    private static Logger logger = Logger.getLogger(ProjectScheduleController.class);

    @Autowired
    ProjectScheduleService projectScheduleService;

    @RequestMapping("/scheduleModel")
    public ModelAndView scheduleModel(HttpServletRequest request, HttpSession session) throws Exception{
        Map<String, Object> mapParams = getMapParams(request);
        ModelAndView mav = new ModelAndView("/project/projectInfo/schedule");
        String id = mapParams.get("id") == null ? "" : mapParams.get("id").toString();
        String name = mapParams.get("name") == null ? "" : mapParams.get("name").toString();
        String projectStage = mapParams.get("projectStage") == null ? "" : mapParams.get("projectStage").toString();
        String completion = mapParams.get("completion") == null ? "" : mapParams.get("completion").toString();
        String remark = mapParams.get("remark") == null ? "" : mapParams.get("remark").toString();
        String sid = mapParams.get("sid") == null ? "" : mapParams.get("sid").toString();
        if(id.equalsIgnoreCase("null")){
            id = "";
        }
        if(name.equalsIgnoreCase("null")){
            name = "";
        }
        if(projectStage.equalsIgnoreCase("null")){
            projectStage = "";
        }
        if(completion.equalsIgnoreCase("null")){
            completion = "";
        }
        if(remark.equalsIgnoreCase("null")){
            remark = "";
        }
        if(sid.equalsIgnoreCase("null")){
            sid = "";
        }
        mav.addObject("id", id);
        mav.addObject("name", name);
        mav.addObject("projectStage", projectStage);
        mav.addObject("completion", completion);
        mav.addObject("remark", remark);
        mav.addObject("sid", sid);
        return mav;
    }

    @RequestMapping("/saveSchedule")
    public String saveSchedule(Schedule schedule, HttpServletRequest request, HttpSession session) throws Exception{
        String userId = this.getLoginId(request.getSession());
        schedule.setCreator(userId);
        schedule.setUploader(userId);
        projectScheduleService.saveSchedule(schedule, request, session);
        return "redirect:/project/toProjectInfoList";
    }
}
