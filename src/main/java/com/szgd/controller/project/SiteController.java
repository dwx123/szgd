package com.szgd.controller.project;

import com.szgd.controller.BaseController;
import com.szgd.service.project.SiteService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/site")
public class SiteController extends BaseController {
    private static Logger logger = Logger.getLogger(SiteController.class);

    @Autowired
    SiteService siteService;

    @RequestMapping(value = "toSiteList", method = RequestMethod.GET)
    public String toSiteList() {
        return "/project/site/siteList";
    }

    @RequestMapping(value = "toSiteView", method = RequestMethod.GET)
    public ModelAndView toSiteView(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/project/site/siteView");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0)//修改
        {
            Map<String, Object> siteInfoMap = siteService.getSite(id);
            mv.addObject("site",siteInfoMap);
        }
        return mv;
    }

    @RequestMapping(value = "toSiteForm", method = RequestMethod.GET)
    public ModelAndView toSiteForm(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/project/site/siteForm");
        Map<String, Object> siteInfoMap  = new HashMap<>();
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0)//修改
        {
            siteInfoMap = siteService.getSite(id);
        }
        siteInfoMap.put("lineNo",6);
        mv.addObject("site",siteInfoMap);
        return mv;
    }

    @RequestMapping("/toUploadLayoutPic")
    public ModelAndView toUploadLayoutPic(HttpServletRequest request, HttpSession session) throws Exception{
        Map<String, Object> mapParams = getMapParams(request);
        ModelAndView mav = new ModelAndView("/project/site/uploadLayoutPic");
        String id = mapParams.get("id") == null ? "" : mapParams.get("id").toString();
        mav.addObject("id", id);
        return mav;
    }

    @RequestMapping(value = "/getSiteList", method = RequestMethod.GET)
    public void  getSiteList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = siteService.getSiteList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/getSites", method = RequestMethod.POST)
    @ResponseBody
    public List<Map<String,Object>> getSites(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request, HttpServletResponse response) {
        String bidId = paramMap.get("bidId")==null?"":paramMap.get("bidId").toString().replace("null","");
        if(bidId.length() == 0){
            paramMap.put("bidId",null);
        }
        String siteId = paramMap.get("siteId")==null?"":paramMap.get("siteId").toString().replace("null","");
        if(siteId.length() == 0){
            paramMap.put("siteId",null);
        }
        List<Map<String, Object>> list = siteService.getSiteListNoPage(paramMap);
    	return list;
    }

    @RequestMapping(value = "/getStartedSiteList", method = RequestMethod.POST)
    @ResponseBody
    public List<Map<String,Object>> getStartedSiteList(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request, HttpServletResponse response) {
        String bidId = paramMap.get("bidId")==null?"":paramMap.get("bidId").toString().replace("null","");
        if(bidId.length() == 0){
            paramMap.put("bidId",null);
        }
        paramMap.put("orderType","web");
        return siteService.getStartedSiteList(paramMap);
    }

    @RequestMapping(value = "/saveSite")
    @ResponseBody
    public Map<String,Object> saveSite(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String message = "";
        try {
            Map<String, Object> param1 = new HashMap<>();
            param1.put("lineNo",paramMap.get("lineNo"));
            param1.put("name",paramMap.get("name"));
            List<Map<String, Object>>  siteList = siteService.getSiteListNoPage(param1);
            if (siteList != null && siteList.size() > 0)
            {
                boolean isHave = true;
                if (id.length() != 0)
                {
                    Map<String, Object> siteMap = siteList.get(0);
                    String tempId = siteMap.get("id").toString();
                    if (id.equalsIgnoreCase(tempId))
                    {
                        isHave = false;
                    }
                }
                if (isHave)
                {
                    message = "已经存在相同的线路和站点!";
                    return getErrorMap(message);
                }

            }

            String  defaultShow = paramMap.get("defaultShow")==null?"":paramMap.get("defaultShow").toString();
            if (defaultShow.equalsIgnoreCase("1"))
            {
                Map<String, Object> param2 = new HashMap<>();
                param2.put("defaultShow",defaultShow);
                if (id.length() == 0){
                    siteList = siteService.getSiteListNoPage(param2);
                    if (siteList != null && siteList.size() > 0)
                    {
                        message = "已经有站点被设置为默认大屏显示!只能有一个站点被设置为默认大屏显示";
                        return getErrorMap(message);
                    }
                }else{
                    param2.put("elseId",id);
                    siteList = siteService.getSiteListNoPage(param2);
                    if (siteList != null && siteList.size() > 0)
                    {
                        message = "已经有站点被设置为默认大屏显示!只能有一个站点被设置为默认大屏显示";
                        return getErrorMap(message);
                    }
                }

            }

            if (id.length() == 0)//新增
            {
                paramMap.put("creator",this.getLoginId(request.getSession()));
                siteService.insertSite(paramMap);
                message = "站点信息新增成功！";
            } else {
                paramMap.put("uploader",this.getLoginId(request.getSession()));
                siteService.updateSite(paramMap);
                message = "站点信息保存成功！";
            }
            return getSuccMap(message);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("站点信息保存失败："+e.getMessage());
            return getSuccMap("站点信息保存失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/deleteSite", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> deleteSite(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        try{
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            siteService.updateSite(paramMap);

            String message = "站点删除成功！";
            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("站点删除失败："+e.getMessage());
            return getSuccMap("站点删除失败："+e.getMessage());
        }
    }

    //上传摄像头布局图
    @RequestMapping(value = "/uploadLayoutPic")
    @ResponseBody
    public Map<String,Object> uploadLayoutPic(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        try {
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            siteService.uploadLayoutPic(paramMap);
            return getSuccMap("图片上传成功！");
        }catch(Exception e){
            e.printStackTrace();
            return getSuccMap("图片上传失败："+e.getMessage());
        }
    }
}
