package com.szgd.controller.project;

import com.szgd.controller.BaseController;
import com.szgd.dao.ecdata.project.ProjectMapper;
import com.szgd.service.project.WorkSiteService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/workSite")
public class WorkSiteController extends BaseController {
    private static Logger logger = Logger.getLogger(WorkSiteController.class);

    @Autowired
    WorkSiteService workSiteService;
    @Autowired
    ProjectMapper projectMapper;

    @RequestMapping(value = "toWorkSiteList", method = RequestMethod.GET)
    public String toWorkSiteList() {
        return "/project/workSite/workSiteList";
    }

    @RequestMapping(value = "toWorkSiteView", method = RequestMethod.GET)
    public ModelAndView toWorkSiteView(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/project/workSite/workSiteView");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0)//修改
        {
            Map<String, Object> workSiteInfoMap = workSiteService.getWorkSite(id);
            mv.addObject("workSite",workSiteInfoMap);
        }
        return mv;
    }

    @RequestMapping(value = "toWorkSiteForm", method = RequestMethod.GET)
    public ModelAndView toWorkSiteForm(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/project/workSite/workSiteForm");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0)//修改
        {
            Map<String, Object> workSiteInfoMap = workSiteService.getWorkSite(id);
            mv.addObject("workSite",workSiteInfoMap);
        }
        List<Map<String,Object>> projectList = projectMapper.getProjectList(null);
        mv.addObject("projectList",projectList);
        return mv;
    }

    @RequestMapping(value = "/getWorkSiteList", method = RequestMethod.GET)
    public void  getWorkSiteList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = workSiteService.getWorkSiteList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/saveWorkSite")
    @ResponseBody
    public Map<String,Object> saveWorkSite(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String message = "";
        try {
            if (id.length() == 0)//新增
            {
                paramMap.put("creator",this.getLoginId(request.getSession()));
                workSiteService.insertWorkSite(paramMap);
                message = "工地信息新增成功！";
            } else {
                paramMap.put("uploader",this.getLoginId(request.getSession()));
                workSiteService.updateWorkSite(paramMap);
                message = "工地信息保存成功！";
            }
            return getSuccMap(message);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("工地信息保存失败："+e.getMessage());
            return getSuccMap("工地信息保存失败："+e.getMessage());
        }
    }

    @RequestMapping(value = "/deleteWorkSite")
    @ResponseBody
    public Map<String,Object> deleteWorkSite(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        try{
            paramMap.put("uploader",this.getLoginId(request.getSession()));
            workSiteService.deleteWorkSite(paramMap);

            String message = "工地信息删除成功！";
            return getSuccMap(message);
        }catch (Exception e)
        {
            e.printStackTrace();
            logger.error("工地信息删除失败："+e.getMessage());
            return getSuccMap("工地信息删除失败："+e.getMessage());
        }
    }
}
