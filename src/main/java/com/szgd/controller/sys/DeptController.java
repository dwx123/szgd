package com.szgd.controller.sys;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.szgd.controller.BaseController;
import com.szgd.service.sys.DeptService;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 部门信息管理的Action类
 * @author lish
 *
 */
@Controller
@RequestMapping("/dept")
public class DeptController extends BaseController {
	private static final long serialVersionUID = -7241734441340928041L;
	
	@Autowired
	DeptService deptService;

	
	/**
	 * 查询所有的部门信息
	 */
	@RequestMapping("/getAllDeptInfo")
	public void getAllDeptInfo(HttpServletRequest request,HttpServletResponse response){
		outJSONData(response, new JSONArray(deptService.getAllDeptInfo(new HashMap())));
	}
	
	/**
	 * 获得当前支队的所有人员信息. 总队可以展现所有
	 */
	public void getOwenDeptUsers(HttpServletRequest request,HttpServletResponse response){
		Map params = new HashMap();
		params.put("ROLE_ID", getParam(request,"ROLE_ID"));
		
		//String wf_role = (String)docApplyService.getWfRole(this.getUserId(request));
		String wf_role = "";

		//1: 获取当前用户所属支队的dept_id  add by wcj 20160719
		String dept_cd = "";
		Integer zhiDui_deptId = deptService.getZhiDui_deptId(dept_cd);
		//2: 递归查询出 dept_id 下所有中队的信息
		params.put("zhiDui_deptId", zhiDui_deptId);
		
		if(! ("ZongDui_LD".equalsIgnoreCase(wf_role)  || "ZongDui_GLY".equalsIgnoreCase(wf_role)) ){ //总队管理员,总队领导
			outJSONData(response , new JSONArray(deptService.getOwenDeptUsers(params)));
		}else{
			outJSONData(response , new JSONArray(deptService.getNoGrantUsers(params)));
		}
		
	}

	public void getNoGrantUsers(HttpServletRequest request,HttpServletResponse response){
		Map params = new HashMap();
		params.put("ROLE_ID", getParam(request,"ROLE_ID"));
		outJSONData(response , new JSONArray(deptService.getNoGrantUsers(params)));
	}


	/**
	 * 新增流程角色
	 */
	@RequestMapping("/insertDept")
	public void insertDept(HttpServletRequest  request, HttpServletResponse response){
		JSONObject json=getJson();
		json.put("success", true);
		json.put("message", "添加部门成功!");
		try{
			Map<String, Object> map = getMapParams(request);
			Integer deptId = deptService.getNewDeptId();
			Integer sort = deptService.getNewSort();
			map.put("DEPT_ID", deptId);
			map.put("SORT_NUMBER", sort);
			boolean mark=deptService.insertDept(map)>0?true:false;
			if(!mark){
				json.put("message", "添加部门失败!");
			}
		}catch(Exception e){
			e.printStackTrace();
			json.put("message", "添加部门失败!");
		}
		
		outJSONData(response , json);
	}
	
	/**
	 * 更新流程角色
	 */
	@RequestMapping("/updateDept")
	public void updateDept(HttpServletRequest  request, HttpServletResponse response){
		JSONObject json=getJson();
		json.put("success", true);
		json.put("message", "更新部门成功!");
		try{
			boolean mark=deptService.updateDept(getMapParams(request))>0?true:false;
			if(!mark){
				json.put("message", "更新部门失败!");
			}
		}catch(Exception e){
			e.printStackTrace();
			json.put("message", "更新部门失败!");
		}
		outJSONData(response , json);
	}

	@RequestMapping(value = "/getAllChildById", method = RequestMethod.POST)
	public void getAllChildById(String DEPT_ID,HttpServletResponse response ) {
		outJSONData(response, new JSONArray(deptService.getAllChildById(DEPT_ID)));
	}
}
