package com.szgd.controller.sys;

import com.szgd.controller.BaseController;
import com.szgd.service.sys.LogService;
import com.szgd.service.sys.RightService;
import com.szgd.service.sys.UserService;
import com.szgd.service.sys.WfRoleService;
import com.szgd.util.TimeUtil;
import com.szgd.util.UserUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/log")
public class LogController extends BaseController {
    private static Logger logger = Logger.getLogger(LogController.class);

    @Autowired
    LogService logService;
    @Autowired
    UserService userService;
    @Autowired
    WfRoleService wfRoleService;
    @Autowired
    RightService rightService;

    @RequestMapping(value = "toLogList", method = RequestMethod.GET)
    public ModelAndView toLogList(HttpServletRequest request, HttpSession session) {
        ModelAndView mv = getModelAndView(request,session,"/sys/log/logList");
        mv.addObject("userList",userService.getUsersOfRole(null));

        List<Map<String, Object>> dataList = rightService.getAllLeafMenus(null);
        Map<String, Object> map1 = new HashMap<>();
        map1.put("menuId","LOGIN_OUT");
        map1.put("menuName","退出系统");
        map1.put("parentMenuName","系统管理");
        dataList.add(0,map1);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("menuId","LOGIN_IN");
        map2.put("menuName","登录系统");
        map2.put("parentMenuName","系统管理");
        dataList.add(0,map2);
        mv.addObject("menuList",dataList);
        return mv;
    }


    @RequestMapping(value = "toLogView", method = RequestMethod.GET)
    public ModelAndView toLogView(HttpServletRequest request, HttpSession session)
    {
        ModelAndView mv = getModelAndView(request,session,"/sys/log/logView");
        String id = mv.getModelMap().get("id")== null?"":mv.getModelMap().get("id").toString().replace("null","");
        if (id.length() > 0)//修改
        {
            Map<String, Object> logMap = logService.getLog(id);
            mv.addObject("log",logMap);
        }
        return mv;
    }

    @RequestMapping(value = "/getLeafMenuList", method = RequestMethod.POST)
    @ResponseBody
    public List<Map<String, Object>>  getPeopleGatherEquipListOfSite(@RequestBody HashMap<String, Object> paramMap, HttpServletResponse response, HttpServletRequest request){
        String sysFlag = paramMap.get("sysFlag")== null?"":paramMap.get("sysFlag").toString().replace("null","");
        List<Map<String, Object>> dataList = null;
        if (sysFlag.equalsIgnoreCase("1"))
        {
            dataList = rightService.getAllLeafMenus(null);

            Map<String, Object> map1 = new HashMap<>();
            map1.put("menuId","LOGIN_OUT");
            map1.put("menuName","退出系统");
            map1.put("parentMenuName","系统管理");
            dataList.add(0,map1);

            Map<String, Object> map2 = new HashMap<>();
            map2.put("menuId","LOGIN_IN");
            map2.put("menuName","登录系统");
            map2.put("parentMenuName","系统管理");
            dataList.add(0,map2);

        }
        if (sysFlag.equalsIgnoreCase("2"))
            dataList = wfRoleService.getAppLeafMenu(null);
        return  dataList;
    }

    @RequestMapping(value = "/getLogList", method = RequestMethod.GET)
    public void  getLogList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> logList = logService.getLogList(paramMap);
        outTableJSONDataCommon(response, logList);
    }

    @RequestMapping(value = "/getOpLogList", method = RequestMethod.GET)
    public void  getOpLogList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        /*paramMap.put("dataType","0");
        paramMap.put("opType","");*/

        List<Map<String, Object>> logList = logService.getOpLogList(paramMap);
        outTableJSONDataCommon(response, logList);
    }

    @RequestMapping(value = "/insertLog")
    @ResponseBody
    public Map<String,Object>  insertLog(HttpServletRequest request,HttpServletResponse response) {
        Map<String, Object> paramMap = getMapParams(request);
        String message = "";
        try {
            User user = (User)request.getSession().getAttribute(UserUtil.USERINFOMATION);
            paramMap.put("opLoginId",user.getLoginId());
            paramMap.put("opName",user.getUserCnName());
            //paramsMap.put("menuId","LOGIN_OUT");
            //paramsMap.put("menuName","退出系统");
            //paramsMap.put("sysFlag","1");
            paramMap.put("opTime", TimeUtil.formatTime(new Date()));
            paramMap.put("creator",user.getLoginId());
            logService.insertLog(paramMap);
            message = "日志信息新增成功！";
            return getSuccMap(message);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("日志信息新增失败："+e.getMessage());
            return getSuccMap("日志信息新增失败："+e.getMessage());
        }

    }
}
