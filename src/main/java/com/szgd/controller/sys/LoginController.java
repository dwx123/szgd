package com.szgd.controller.sys;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.szgd.service.sys.LogService;
import com.szgd.util.TimeUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.szgd.controller.BaseController;
import com.szgd.util.SetLogBefore;
import com.szgd.util.UserUtil;

@Controller
public class LoginController extends BaseController {
    private static Logger logger = Logger.getLogger(LoginController.class);
	@Autowired
    LogService logService;
    @RequestMapping(value = "/getUser")
    public ModelAndView getUser(@RequestParam("userId") String userId, HttpSession session) {
        UserUtil.saveUserToSession(session, userId);
        //List<Group> groupList = identityService.createGroupQuery().groupMember(UserUtil.getUserFromSession(session)).list();
        List<String> gList = new ArrayList<String>();
        /*for(Group g : groupList){
        	gList.add(g.getId());
        }*/
        UserUtil.saveObjectToSession(session, userId, gList);
        ModelAndView mav = new ModelAndView("/login/success");
        return mav;
    }

	@RequestMapping(value = "/login")
	public ModelAndView login(HttpServletRequest request,HttpServletResponse response) throws IOException {
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", 0);
		if (request.getHeader("x-requested-with") != null
				&& request.getHeader("x-requested-with").equalsIgnoreCase(
						"XMLHttpRequest")) { // ajax 超时处理  
			response.getWriter().print("timeout");// 设置超时标识
			response.getWriter().close();
		} else {
			ModelAndView mav = new ModelAndView("/login");
			return mav;
		}
		return null;
	}
	@RequestMapping(value = "/timeout")
	public void sessionTimeout(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		/*if (request.getHeader("x-requested-with") != null
				&& request.getHeader("x-requested-with").equalsIgnoreCase(
						"XMLHttpRequest")) { // ajax 超时处理  
			response.getWriter().print("timeout");// 设置超时标识
			response.getWriter().close();
		} else {
			response.sendRedirect("/login");
		}*/
	}
    @SetLogBefore(opt="退出系统")
    @RequestMapping(value = "logout")
    public String logout(HttpServletRequest request) {
        try {
            HttpSession session = request.getSession();

            User user = (User)session.getAttribute(UserUtil.USERINFOMATION);
            Map<String, Object> paramsMap  = new HashMap<>();
            paramsMap.put("opLoginId",user.getLoginId());
            paramsMap.put("opName",user.getUserCnName());
            paramsMap.put("menuId","LOGIN_OUT");
            paramsMap.put("menuName","退出系统");
            paramsMap.put("sysFlag","1");
            paramsMap.put("opTime", TimeUtil.formatTime(new Date()));
            paramsMap.put("creator",user.getLoginId());
            logService.insertLog(paramsMap);


            session.removeAttribute(UserUtil.USER);
            session.removeAttribute("userInfo");
            session.removeAttribute("userDeptMap");
            session.removeAttribute(UserUtil.USERINFOMATION);
            session.invalidate();



        } catch (java.lang.Exception e) {
            logger.error(e.getMessage());
        }
        return "login";
    }
}
