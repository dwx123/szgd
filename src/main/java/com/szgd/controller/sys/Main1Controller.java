package com.szgd.controller.sys;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.szgd.util.SetLog;

@Controller
public class Main1Controller {
	@SetLog(opt="登录系统")
	@RequestMapping("/main")
	public ModelAndView getUserList(){
		System.out.println("----------------------");
		ModelAndView mav = new ModelAndView("/main");
		return mav;
	}
	@RequestMapping("/todo")
	public ModelAndView getToDoList(){
		System.out.println("----------------------");
		ModelAndView mav = new ModelAndView("/todo");
		return mav;
	}
	@RequestMapping("/list")
	public ModelAndView getList(){
		System.out.println("----------------------");
		ModelAndView mav = new ModelAndView("/list");
		return mav;
	}
	@RequestMapping("/form")
	public ModelAndView getFormList(){
		System.out.println("----------------------");
		ModelAndView mav = new ModelAndView("/form");
		return mav;
	}
}
