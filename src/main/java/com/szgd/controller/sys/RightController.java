package com.szgd.controller.sys;

import com.szgd.controller.BaseController;
import com.szgd.service.sys.RightService;
import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RequestMapping("/right")
@Controller
public class RightController extends BaseController {
	private static final long serialVersionUID = -174059817747975183L;
	private static Logger logger = Logger.getLogger(RightController.class);
	
	@Autowired
	RightService rightService;

	@RequestMapping("/toRightList")
	public ModelAndView toList(){

		ModelAndView mav = new ModelAndView("/sys/right/sysRight");
		return mav;
	}

	@RequestMapping(value="/getUserRight",method={RequestMethod.POST})
	public void getUserRight(HttpServletRequest request, HttpServletResponse response){
		String userId = getParam(request,"userId");
		String rightId = getParam(request,"M");
		outJSONData(response,new JSONArray(rightService.getRightByUserIDAndRightId(userId,rightId)));
	}

	@RequestMapping(value="/getAllUserRight",method={RequestMethod.POST})
	public void getAllUserRight(HttpServletRequest request, HttpServletResponse response){
		String userId = getParam(request,"userId");
		outJSONData(response,new JSONArray(rightService.getAllRightByUserID(userId)));
	}
	
	@RequestMapping(value="/getAllRoleRightByUserId",method={RequestMethod.POST})
	public void getAllRoleRightByUserId(HttpServletRequest request, HttpServletResponse response){
		String userId = getParam(request,"userId");
		List<Map<String, Object>> rightList =  rightService.getAllRoleRightByUserId(userId);
        List<Map<String, Object>> menuList = new ArrayList<>();
        for (int i = 0; i < rightList.size(); i++) {
            Map<String, Object> rightMap =  rightList.get(i);
            if (rightMap.get("PARENTID").toString().equalsIgnoreCase("ROOT"))
                menuList.add(rightMap);
        }
        for (int i = 0; i < menuList.size(); i++) {
            Map<String, Object> menuMap =  menuList.get(i);
            String parentId = menuMap.get("RIGHTID").toString();
            rightService.getAllSubRight(menuMap,parentId,rightList);

        }
		outJSONData(response,new JSONArray(menuList));
	}
	
	@RequestMapping(value="/getAllRoleRight",method={RequestMethod.POST})
	public void getAllRoleRight(HttpServletRequest request, HttpServletResponse response){
		String roleId = getParam(request,"roleId");
		outJSONData(response,new JSONArray(rightService.getAllRightByRoleID(roleId)));
	}

	@RequestMapping("/saveUserRight")
	public void saveUserRight(HttpServletRequest request, HttpServletResponse response){
		JSONObject json=getJson();
		boolean result = rightService.saveUserRight(this.getMapParams(request));
		if(result){
			json.put("success", true);
			json.put("message", "保存成功!");
		}
		else{
			json.put("success", false);
			json.put("message", "保存失败!");
		}
		outJSONData(response,json);
	}

	@RequestMapping("/saveRoleRight")
	public void saveRoleRight(HttpServletRequest request, HttpServletResponse response){
		JSONObject json=getJson();
		boolean result = rightService.saveRoleRight(this.getMapParams(request));
		if(result){
			json.put("success", true);
			json.put("message", "保存成功!");
		}
		else{
			json.put("success", false);
			json.put("message", "保存失败!");
		}
		outJSONData(response,json);
	}
}
