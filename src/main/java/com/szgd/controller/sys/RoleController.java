package com.szgd.controller.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.szgd.controller.BaseController;
import com.szgd.util.Pinyin4j;
import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.szgd.service.sys.WfRoleService;

/**
 * 流程角色Action操作
 *
 */
@Controller
@RequestMapping("/role")
public class RoleController extends BaseController {
	private static final long serialVersionUID = -5610500130530671047L;
	private static Logger logger = Logger.getLogger(RoleController.class);
	
	@Autowired
	WfRoleService wfRoleService;

	@RequestMapping("/toRoleList")
	public ModelAndView toWfRoleManPage(){

		ModelAndView mav = new ModelAndView("/sys/roles/wfRoleList");
		return mav;
	}

	/**
	 * 查询所有的流程角色信息
	 */
	@RequestMapping("/getAllRole")
	public void getAllRole(HttpServletRequest  request, HttpServletResponse response){
		outTableJSONDataCommon(response,wfRoleService.getRoleListByPage(getMapParams(request)));
	}
	
	/**
	 * 获得流程角色已授权用户列表
	 */
	@RequestMapping("/getWfRoleGrantUsers")
	public void getWfRoleGrantUsers(HttpServletRequest  request, HttpServletResponse response){
		outTableJSONData(response ,wfRoleService.getWfRoleGrantUsers(getMapParams(request)));
	}
	
	// 查询该流程角色下尚未被赋值菜单的人员
	@RequestMapping("/getNotGrantRightApproveUsers")
	public void getNotGrantRightApproveUsers(HttpServletRequest  request, HttpServletResponse response){
		outTableJSONData(response , wfRoleService.getNotGrantRightApproveUsers(getMapParams(request)));
	}
	
	
	
	
	/**
	 * 新增流程角色
	 */
	@RequestMapping("/insertWfRole")
	public void insertWfRole(HttpServletRequest  request, HttpServletResponse response){
		JSONObject json=getJson();
		json.put("success", true);
		json.put("message", "添加角色成功!");
		try{
			Map<String, Object> map = getMapParams(request);
			String ROLE_NAME = map.get("ROLE_NAME").toString();
			map.put("ROLE_ID", Pinyin4j.getUpperCase(ROLE_NAME,false));
			map.put("SFYXSC", "0");
			boolean mark=wfRoleService.insertWfRole(map)>0?true:false;
			if(!mark){
				json.put("message", "添加角色失败!");
			}
		}catch(Exception e){
			logger.error("插入角色失败！",e);
			json.put("message", "添加角色失败!");
		}
		
		outJSONData(response , json);
	}
	
	/**
	 * 更新流程角色
	 */
	@RequestMapping("/updateWfRole")
	public void updateWfRole(HttpServletRequest  request, HttpServletResponse response){
		JSONObject json=getJson();
		json.put("success", true);
		json.put("message", "更新角色成功!");
		try{
			boolean mark=wfRoleService.updateWfRole(getMapParams(request))>0?true:false;
			if(!mark){
				json.put("message", "更新角色失败!");
			}
		}catch(Exception e){
			logger.error("更新角色失败！",e);
			json.put("message", "更新角色失败!");
		}
		outJSONData(response , json);
	}
	
	/**
	 * 删除流程角色
	 */
	@RequestMapping("/deleteWfRole")
	public void deleteWfRole(HttpServletRequest  request, HttpServletResponse response){
		JSONObject json=getJson();
		json.put("success", true);
		json.put("message", "删除角色成功!");
		try{
			boolean mark=wfRoleService.deleteWfRole(getParam(request,"ROLE_ID"))>0?true:false;
			if(!mark){
				json.put("message", "删除角色失败!");
			}
		}catch(Exception e){
			logger.error("删除角色失败！",e);
			json.put("message", "删除角色失败!");
		}
		outJSONData(response , json);
	}
	
	/**
	 * 删除流程角色用户
	 */
	@RequestMapping("/deleteWfRoleUser")
	public void deleteWfRoleUser(HttpServletRequest  request, HttpServletResponse response){
		JSONObject json=getJson();
		json.put("success", true);
		json.put("message", "删除角色用户成功!");
		try{
			boolean mark=wfRoleService.deleteWfRoleUser(getMapParams(request))>0?true:false;
			if(!mark){
				json.put("message", "删除角色失败!");
			}
		}catch(Exception e){
			logger.error("删除角色用户失败！",e);
			json.put("message", "删除角色用户失败!");
		}
		outJSONData(response , json);
	}
	
	
	/**
	 * 根据ROLE_ID，获取未授权到该角色下的所有用户信息（包含组织机构，以simpledata格式提供给ztree）
	 */
	@RequestMapping("/getNoGrantUsers")
	public void getNoGrantUsers(HttpServletRequest  request, HttpServletResponse response){
		Map params = new HashMap();
		params.put("ROLE_ID", getParam(request,"ROLE_ID"));
		List<Map<String,Object>> noGrantUserList = wfRoleService.getNoGrantUsers(params);
		outJSONData(response , new JSONArray(noGrantUserList));
	}
	
	/**
	 * 根据ROLE_ID，获取未授权到该角色下的所有用户信息（包含组织机构，以simpledata格式提供给ztree）
	 */
	@RequestMapping("/getNoGrantSites")
	public void getNoGrantSites(HttpServletRequest  request, HttpServletResponse response){
		Map params = new HashMap();
		List<Map<String,Object>> noGrantUserList = wfRoleService.getNoGrantSites(params);
		outJSONData(response , new JSONArray(noGrantUserList));
	}

	@RequestMapping("/batchGrantUserToWfRole")
	public void batchGrantUserToWfRole(HttpServletRequest  request, HttpServletResponse response){
		JSONObject json=getJson();
		json.put("success", true);
		json.put("message", "批量授权用户到流程角色组成功!");
		boolean mark = wfRoleService.batchGrantUserToWfRole(getMapParams(request));
		if(!mark){
			json.put("success", false);
			json.put("message", "批量授权用户到流程角色组失败!");
		}
		outJSONData(response , json);
	}
}
