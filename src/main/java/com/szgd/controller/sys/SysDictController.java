package com.szgd.controller.sys;


import com.szgd.bean.SysDict;
import com.szgd.controller.BaseController;
import com.szgd.service.sys.SysDictService;
import com.szgd.util.Pinyin4j;
import org.activiti.engine.impl.util.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;


@Controller
@RequestMapping(value = "/dict")
public class SysDictController extends BaseController {


    @Autowired
    SysDictService sysDictService;

    @RequestMapping(value = "/insertDict", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> insert(
            @ModelAttribute SysDict sysDict,
            HttpSession httpSession
    ) {
        Map map = new HashMap();
        try {
            String code = sysDict.getParentCode()+"_"+Pinyin4j.getUpperCase(sysDict.getDictValue(),false);
            sysDict.setCode(code);
            String id = sysDictService.getIdByCode(sysDict.getCode());
            if (id != null && id.length() > 0)
            {
                map.put("result", false);
                map.put("message", "保存失败！已经存在此Code！");
            }else
            {

                boolean result = sysDictService.insert(sysDict, httpSession) > 0 ? true : false;
                if (result) {
                    map.put("result", result);
                    map.put("message", "保存成功！");
                }
            }

        } catch (Exception e) {
            map.put("result", false);
            map.put("message", "保存失败！---"+e.getMessage());
        }
        return map;
    }

    @RequestMapping(value = "/toDictList", method = RequestMethod.GET)
    public String index() {
        return "sys/dict/dictList";
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public void getAllDict(
            HttpServletResponse httpServletResponse
    ) {

        outJSONData(httpServletResponse, new JSONArray(sysDictService.getAllSysDict(null)));
    }

    @RequestMapping(value = "/toUpdate", method = RequestMethod.GET)
    public ModelAndView toUpdate(
            @RequestParam("dictId") String id
    ) {
        ModelAndView modelAndView = new ModelAndView("sys/dict/update");
        modelAndView.addObject(sysDictService.getByDictId(id));
        return modelAndView;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateSysDict(
            SysDict sysDict,
            HttpServletResponse response
    ) {
        response.setCharacterEncoding("UTF-8");
        Map<String, Object> map = new HashMap<>();
        try {
            boolean result = sysDictService.update(sysDict) > 0 ? true : false;
            map.put("result", result);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("result", false);
        }

        return map;
    }

    @RequestMapping(value = "/getByDictId", method = RequestMethod.POST)
    @ResponseBody
    public Map getById(String selDeptID) {
        System.out.println(selDeptID);
        Map sysDictMap = new HashMap();
        sysDictMap.put("sysDict", sysDictService.getByDictId(selDeptID));
        return sysDictMap;
    }

    @RequestMapping(value = "/getChildById", method = RequestMethod.POST)
    public void getChildById(String selDeptID,HttpServletResponse response ) {
        outJSONData(response, new JSONArray(sysDictService.getChildById(selDeptID)));
    }

    @RequestMapping(value = "/getAllChildById", method = RequestMethod.POST)
    public void getAllChildById(String selDeptID,HttpServletResponse response ) {
        outJSONData(response, new JSONArray(sysDictService.getAllChildById(selDeptID)));
    }

    @RequestMapping(value = "/getByIds", method = RequestMethod.GET)
    public ModelAndView getByIds(
            @RequestParam("dictIds") String[] dictIds
    ) {
        ModelAndView modelAndView = new ModelAndView();
        List<SysDict> sysDicts = sysDictService.getByDictIds(Arrays.asList(dictIds));
        modelAndView.addObject("sysDicts", sysDicts);
        modelAndView.setViewName(null);
        return modelAndView;
    }
}
