package com.szgd.controller.sys;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.szgd.bean.SysLog;
import com.szgd.controller.BaseController;

import com.szgd.service.sys.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/sysLog")
public class SysLogController extends BaseController {

    /*@Autowired
    SysLogService sysLogService;

    @RequestMapping(value = "toLogList", method = RequestMethod.GET)
    public String index() {
        return "/sys/log/logList";
    }

    @RequestMapping(value = "/getSysLogData", method = RequestMethod.GET)
    public void getSysLogData(
            HttpServletResponse httpServletResponse,
            HttpServletRequest httpServletRequest,
            String jsonString
    ) {

        Map<String, Object> mapParams = getMapParams(httpServletRequest);
        mapParams.put("model", fromJsontoMode(jsonString));
        List<SysLog> byModel = sysLogService.getByModel(mapParams);
        outTableJSONDataCommon(httpServletResponse, byModel);
    }

    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    public ModelAndView delete(
            ModelAndView modelAndView,
            @PathVariable("id") String id
    ) {
        sysLogService.deleteById(id);
        List<SysLog> allSysLog = sysLogService.getAllSysLog();
        modelAndView.setViewName("sys/log/logList");
        modelAndView.addObject("sysLogs", allSysLog);
        return modelAndView;
    }

    @RequestMapping(value = "/deleteBatch", method = RequestMethod.GET)
    public ModelAndView deleteBatch(
            ModelAndView modelAndView,
            @RequestParam("ids") String[] ids
    ) {

        modelAndView.addObject("sysLogs", sysLogService.getAllSysLog());
        modelAndView.setViewName("sys/log/logList");
        return modelAndView;
    }

    private SysLog fromJsontoMode(String jsonString) {
        SysLog sysLog = new SysLog();
        if (jsonString != null) {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                sysLog = objectMapper.readValue(jsonString, SysLog.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sysLog;
    }*/

}
