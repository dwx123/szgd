package com.szgd.controller.vehicle;

import com.szgd.bean.RemindCfg;
import com.szgd.bean.SysDict;
import com.szgd.controller.BaseController;
import com.szgd.service.vehicle.VehicleEnterExitService;
import com.szgd.util.PropertiesUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/vehicleEnterExit")
public class VehicleEnterExitController extends BaseController {
    @Autowired
    VehicleEnterExitService vehicleEnterExitService;

    private static Logger logger = Logger.getLogger(VehicleEnterExitController.class);

    @RequestMapping(value = "toVehicleEnterExitList", method = RequestMethod.GET)
    public ModelAndView toVehicleEnterExitList(HttpServletRequest request, HttpSession httpSession) {
        ModelAndView mv = this.getModelAndView(request,httpSession,"vehicle/vehicleEnterExit/vehicleEnterExitList");
        return mv;
    }

    @RequestMapping(value = "/getVehicleEnterAndExitList", method = RequestMethod.GET)
    public void  getVehicleEnterAndExitList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = vehicleEnterExitService.getVehicleEnterAndExitList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping(value = "/getVehicleEnter_ExitList", method = RequestMethod.GET)
    public void  getVehicleEnter_ExitList(HttpServletResponse response, HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = vehicleEnterExitService.getVehicleEnter_ExitList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }

    @RequestMapping("/downloadRecord")
    public void downloadRecord(HttpServletRequest request, HttpServletResponse response) throws Exception{
        Map<String,Object> map = this.getMapParams(request);
        Map<String, String> fieldMapping = vehicleEnterExitService.getFieldMap();
        String fileName = "车辆进出记录.xls";
        String filePath = PropertiesUtil.get("downloadPath");
        String realPath = request.getServletContext().getRealPath(filePath);
        filePath = filePath.replace("\\", "/");
        File f = new File(realPath);
        if (!f.exists()) {
            f.mkdirs();
        }
        realPath = realPath + File.separator + fileName;
        List<Map<String, Object>> dataList = vehicleEnterExitService.getEnterAndExitListNoPage(map);
        exportExcelByTemplate(fieldMapping,dataList, realPath,"车辆进出记录导出模板.xls");
        String downloadPath = filePath + "/" + fileName;
        realPath = request.getServletContext().getRealPath(downloadPath);
        try {
            this.download(response, realPath);
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        new File(realPath).delete();

    }

    /*@RequestMapping(value = "/synVehicleEnterExitRecord", method = RequestMethod.GET)
    public Map<String,Object>  synVehicleEnterExitRecord(HttpServletResponse response, HttpServletRequest request, HttpSession session){
        Map<String, Object> paramMap = getMapParams(request);
        String vehicleEnterExitRecordSynFlag = PropertiesUtil.get("VehicleEnterExitRecordSynFlag");
        if (vehicleEnterExitRecordSynFlag.equalsIgnoreCase("0"))
        {
            String message = "没有启动车辆识别记录导入！";
            return getErrorMap(message);
        }
        String plateNumber = paramMap.get("plateNumber")==null?"":paramMap.get("plateNumber").toString();
        String plateType = paramMap.get("plateType")==null?"":paramMap.get("plateType").toString();
        String plateColor = paramMap.get("plateColor")==null?"":paramMap.get("plateColor").toString();
        String vehicleBrand = paramMap.get("vehicleBrand")==null?"":paramMap.get("vehicleBrand").toString();
        String vehicleColor = paramMap.get("vehicleColor")==null?"":paramMap.get("vehicleColor").toString();
        String vehicleSize = paramMap.get("vehicleSize")==null?null:paramMap.get("vehicleSize").toString();
        ArrayList<SysDict> plateTypeList = (ArrayList<SysDict>)session.getAttribute("PLATE_TYPE");
        ArrayList<SysDict> colorTypeList = (ArrayList<SysDict>)session.getAttribute("COLOR_TYPE");
        ArrayList<SysDict> vehicleBrandList = (ArrayList<SysDict>)session.getAttribute("VEHICLE_BRAND");
        for (int i = 0; i < plateTypeList.size(); i++) {
            SysDict sd = plateTypeList.get(i);
            if (sd.getOtherValue().equalsIgnoreCase(plateType))
            {
                plateType = sd.getCode();
                break;
            }
        }
        for (int i = 0; i < colorTypeList.size(); i++) {
            SysDict sd = colorTypeList.get(i);
            if (sd.getOtherValue().equalsIgnoreCase(plateColor))
            {
                plateColor = sd.getCode();
                break;
            }
        }
        for (int i = 0; i < vehicleBrandList.size(); i++) {
            SysDict sd = vehicleBrandList.get(i);
            if (sd.getOtherValue().equalsIgnoreCase(vehicleBrand))
            {
                vehicleBrand = sd.getCode();
                break;
            }
        }

        for (int i = 0; i < colorTypeList.size(); i++) {
            SysDict sd = colorTypeList.get(i);
            if (sd.getOtherValue().equalsIgnoreCase(vehicleColor))
            {
                vehicleColor = sd.getCode();
                break;
            }
        }


        //vehicleEnterExitService.synVehicleEnterExitRecord(plateNumber,plateType,plateColor,vehicleBrand,vehicleColor,vehicleSize );
        String message = "车辆识别记录导入成功！";
        return getSuccMap(message);
    }*/
}
