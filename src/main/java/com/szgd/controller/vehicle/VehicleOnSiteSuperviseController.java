package com.szgd.controller.vehicle;

import com.szgd.controller.BaseController;
import com.szgd.service.vehicle.VehicleOnSiteSuperviseService;
import com.szgd.service.vehicle.VehicleService;
import com.szgd.util.Constant;
import com.szgd.util.ExcelUtil;
import com.szgd.util.PropertiesUtil;
import com.szgd.util.TimeUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/vehicleOnSiteSupervise")
public class VehicleOnSiteSuperviseController extends BaseController {
    @Autowired
    VehicleOnSiteSuperviseService vehicleOnSiteSuperviseService;

    private static Logger logger = Logger.getLogger(VehicleOnSiteSuperviseController.class);


    @RequestMapping(value = "/getVehicleOnSiteSuperviseList", method = RequestMethod.GET)
    public void  getVehicleOnSiteSuperviseList(HttpServletResponse response,HttpServletRequest request){
        Map<String, Object> paramMap = getMapParams(request);
        List<Map<String, Object>> dataList = vehicleOnSiteSuperviseService.getVehicleOnSiteSuperviseList(paramMap);
        outTableJSONDataCommon(response, dataList);
    }


    @RequestMapping(value = "toVehicleOnSiteSuperviseList", method = RequestMethod.GET)
    public String toVehicleOnSiteSuperviseList() {
        return "/vehicle/vehicleOnSiteSuperviseList";
    }

}
