package com.szgd.controller.weixin;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.szgd.bean.*;
import com.szgd.license.LicenseClient;
import com.szgd.service.elseData.PitService;
import com.szgd.service.elseData.TunnelService;
import com.szgd.service.personnel.*;
import com.szgd.service.vehicle.VehicleOnSiteSuperviseService;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;
import org.apache.axis.holders.ImageHolder;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.szgd.util.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.Exception.ExpiredException;
import org.springframework.security.Exception.NoLicenseException;
import org.springframework.security.Exception.NoLocalMachineCodeException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.szgd.controller.BaseController;
import com.szgd.service.equipment.EquipEnterExitService;
import com.szgd.service.equipment.EquipWorkAttachService;
import com.szgd.service.equipment.EquipmentService;
import com.szgd.service.gatherEquip.CameraService;
import com.szgd.service.hiddenDanger.HiddenDangerService;
import com.szgd.service.project.BidService;
import com.szgd.service.project.SiteService;
import com.szgd.service.schedule.AccessorStructureScheduleService;
import com.szgd.service.schedule.MainWorkScheduleService;
import com.szgd.service.schedule.TunnellingWorkScheduleService;
import com.szgd.service.sys.LogService;
import com.szgd.service.sys.SysDictService;
import com.szgd.service.sys.UserService;
import com.szgd.service.sys.WfRoleService;
import com.szgd.service.weixin.WeChatService;
import com.szgd.util.PropertiesUtil;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/app")
public class WeChatController extends BaseController {
    private static Logger logger = Logger.getLogger(WeChatController.class);
    @Autowired
    WeChatService weChatService;
    @Autowired
    HiddenDangerService hiddenDangerService;
    @Autowired
    AttachService attachService;
    @Autowired
    EquipWorkAttachService equipWorkAttachService;
    @Autowired
    EquipmentService equipmentService;
    @Autowired
    EquipEnterExitService equipEnterExitService;
    @Autowired
    SysDictService sysDictService;
    @Autowired
    SiteService siteService;
    @Autowired
    BidService bidService;
    @Autowired
    MainWorkScheduleService mainWorkScheduleService;
    @Autowired
    TunnellingWorkScheduleService tunnellingWorkScheduleService;
    @Autowired
    AccessorStructureScheduleService accessorStructureScheduleService;
    @Autowired
    CameraService cameraService;
    @Autowired
    WfRoleService wfRoleService;
    @Autowired
    UserService userService;
    @Autowired
    LogService logService;
    @Autowired
    EnterExitService enterExitService;
    @Autowired
    VehicleOnSiteSuperviseService vehicleOnSiteSuperviseService;
    @Autowired
    RemindService remindService;
    @Autowired
    PersonnelInfoService personnelInfoService;
    @Autowired
    CertificateService certificateService;
    @Autowired
    PitService pitService;
    @Autowired
    TunnelService tunnelService;

    /*------------------------------隐患排查BEGIN----------------------------*/

    /*#####隐患上报#####*/
    //***************--APP--隐患获取标段、站点、上报人********************//
    @RequestMapping(value = "/getBidAndSiteAndRP")
    @ResponseBody
    public Map<String, Object> getBidAndSiteAndRP(){
        return weChatService.getBidAndSiteAndRP();
    }

    //***************--APP--隐患上报********************//
    @RequestMapping(value = "/app_reportHiddenDanger", method = {RequestMethod.POST})
    @ResponseBody
    public Map<String,Object> app_reportHiddenDanger(HiddenDanger hiddenDanger, HttpServletRequest request, HttpServletResponse response) {
        String id = null;
        HashMap<String,Object> paramMap = new HashMap<>();
        paramMap.put("siteId",hiddenDanger.getSiteId());
        paramMap.put("description",hiddenDanger.getDescription());
        paramMap.put("reportPerson",hiddenDanger.getReportPerson());
        paramMap.put("creator",hiddenDanger.getCreator());
        paramMap.put("rectifyDeadline",hiddenDanger.getRectifyDeadline());
        paramMap.put("status","REPORT");
        paramMap.put("theme",hiddenDanger.getTheme());
        paramMap.put("findTime",hiddenDanger.getFindTime());
        paramMap.put("rectifyBeginTime",hiddenDanger.getRectifyBeginTime());
        paramMap.put("siteName",hiddenDanger.getSiteName());
        try {
            hiddenDangerService.insertHiddenDanger(paramMap,"app",hiddenDanger.getCreator(),"report",request);
            if(paramMap.get("id") == null || paramMap.get("id").toString().length() == 0){
                return getErrorMap("隐患上报失败，请重新填写上报");
            }else{
                id = paramMap.get("id").toString();
                logger.info("隐患上报成功,隐患ID："+id+"---");
                return getSuccMap("隐患上报成功");
            }
        }catch(Exception e){
            e.printStackTrace();
            logger.error("隐患上报失败,隐患ID："+id+"---"+e.getMessage());
            return getSuccMap("隐患上报失败："+e.getMessage());
        }
    }

    /*#####隐患整改#####*/
    //***************--APP--获取登录用户所需整改的隐患list********************//
    @RequestMapping(value = "/getRLByUser")
    @ResponseBody
    public Map<String, Object> getRLByUser (@RequestBody HashMap<String, Object> paramMap) throws IOException{
        String finish = paramMap.get("finish")==null?"":paramMap.get("finish").toString();
        String ipAddress = paramMap.get("ipAddress")==null?"":paramMap.get("ipAddress").toString();
        String fileUploadPath = PropertiesUtil.get("uploadPath");
        List<String> appStatusList = new ArrayList<>();
        if(finish.equalsIgnoreCase("0")){ //获取待整改的隐患
            appStatusList.add("REPORT");
        }else if(finish.equalsIgnoreCase("1")){   //获取未确认的隐患
            appStatusList.add("REPORT");
            appStatusList.add("RECTIFY");
        }else if(finish.equalsIgnoreCase("2")){   //获取已确认的隐患
            appStatusList.add("CONFIRM");
        }
        paramMap.put("appStatusList",appStatusList);
        paramMap.put("isApp","1");
        List<Map<String,Object>> res = hiddenDangerService.getAppHiddenDangerList(paramMap);
        int count = hiddenDangerService.getAppHiddenDangerCount(paramMap).size();
        if(finish.equalsIgnoreCase("0")) { //待整改的隐患
            if(res != null && res.size() > 0){
                for(int i = 0; i < res.size(); i++){
                    Map<String,Object> tempMap = res.get(i);
                    String path = tempMap.get("path")==null?"":tempMap.get("path").toString().replace("null","");
                    String attachName = tempMap.get("attachName")==null?"":tempMap.get("attachName").toString().replace("null","");
                    tempMap.put("reportPic",getPicToUrl(path,attachName,ipAddress,fileUploadPath));
                }
            }
        }
        Map<String, Object> result = new HashMap<>();
        result.put("list",res);
        result.put("count",count);
        return result;
    }

    public static String getPicBase64(String attachName,String path){
        String[] strArray = attachName.split("\\.");
        int suffixIndex = strArray.length -1;
        String type = strArray[suffixIndex];
        String fileUploadPath = PropertiesUtil.get("uploadPath");
        String fileAbsolutePath = fileUploadPath + File.separator + path +  File.separator + attachName;
        String fis = ImageUtil.getImageStr(fileAbsolutePath);
        String res = "";
        if(fis.length() > 0){
            res = "data:image/" + type + ";base64," + fis;
        }
        return res;
    }
    //***************--APP--根据隐患ID获取对应的信息********************//
    @RequestMapping(value = "/app_getHDInfoById")
    @ResponseBody
    public Map<String,Object> app_getHDInfoById(@RequestBody HashMap<String, Object> paramMap){
        paramMap.put("isApp","1");
        String ipAddress = paramMap.get("ipAddress")==null?"":paramMap.get("ipAddress").toString();
        String uploadPath = PropertiesUtil.get("uploadPath");
        List<Map<String,Object>> hiddenDangerList = hiddenDangerService.getHiddenDangerListNoPage(paramMap);
        if (hiddenDangerList != null && hiddenDangerList.size() > 0){
            Map<String,Object> res = hiddenDangerList.get(0);
            String path = res.get("path")==null?"":res.get("path").toString().replace("null","");
            String attachName = res.get("attachName")==null?"":res.get("attachName").toString().replace("null","");
            String rectifyPath = res.get("rectifyPath")==null?"":res.get("rectifyPath").toString().replace("null","");
            String rectifyAttachName = res.get("rectifyAttachName")==null?"":res.get("rectifyAttachName").toString().replace("null","");
            String reportPic = this.getPicToUrl(path,attachName,ipAddress,uploadPath);
            res.put("reportPic",reportPic);
            String rectifyPic = this.getPicToUrl(rectifyPath,rectifyAttachName,ipAddress,uploadPath);
            res.put("rectifyPic",rectifyPic);
            List<User> rcList = userService.getRcOrCfPerson("hidden_danger_approver");
            List<User> cfList = userService.getRcOrCfPerson("hidden_danger_confirmer");
            Map<String,Object> result = new HashMap<>();
            result.put("data",res);
            result.put("rcList", rcList);
            result.put("cfList", cfList);
            return result;
        }else{
            return null;
        }
    }

    //***************--APP--上传整改内容********************//
    @RequestMapping(value = "/app_rectifyHiddenDanger")
    @ResponseBody
    public Map<String,Object> app_rectifyHiddenDanger(HiddenDanger hiddenDanger, HttpServletRequest request, HttpServletResponse response) {
        try{
            HashMap<String,Object> paramMap = new HashMap<>();
            paramMap.put("id",hiddenDanger.getId());
            paramMap.put("rectifyPerson",hiddenDanger.getRectifyPerson());
            paramMap.put("rectifyContent",hiddenDanger.getRectifyContent());
            paramMap.put("rectifyTime",hiddenDanger.getRectifyTime());
            paramMap.put("uploader",hiddenDanger.getUploader());
            paramMap.put("status","RECTIFY");
            paramMap.put("rectifyAttachId",hiddenDanger.getRectifyAttachId());
            paramMap.put("theme",hiddenDanger.getTheme());
            paramMap.put("siteName",hiddenDanger.getSiteName());
            hiddenDangerService.updateHiddenDanger(paramMap,"app",hiddenDanger.getUploader(),"rectify",request);
            return getSuccMap("隐患整改成功");
        }catch (Exception e) {
            e.printStackTrace();
            logger.error("隐患整改失败,隐患ID:"+hiddenDanger.getId()+"---"+e.getMessage());
            return getSuccMap("隐患整改失败："+e.getMessage());
        }
    }

    /*#####隐患确认#####*/
    //***************--APP--获取需确认的隐患list********************//
    @RequestMapping(value = "/app_getHDConfirmList")
    @ResponseBody
    public Map<String, Object> app_getHDConfirmList(@RequestBody HashMap<String, Object> paramMap){
        paramMap.put("status","RECTIFY");
        paramMap.put("isApp","1");
        String ipAddress = paramMap.get("ipAddress")==null?"":paramMap.get("ipAddress").toString();
        String uploadPath = PropertiesUtil.get("uploadPath");
        List<Map<String,Object>> res = hiddenDangerService.getAppHiddenDangerList(paramMap);
        int count = hiddenDangerService.getAppHiddenDangerCount(paramMap).size();
        if(res != null && res.size() > 0){
            for(int i = 0; i < res.size(); i++){
                Map<String,Object> tempMap = res.get(i);
                String rectifyPath = tempMap.get("rectifyPath")==null?"":tempMap.get("rectifyPath").toString().replace("null","");
                String rectifyAttachName = tempMap.get("rectifyAttachName")==null?"":tempMap.get("rectifyAttachName").toString().replace("null","");
                String rectifyPic = this.getPicToUrl(rectifyPath,rectifyAttachName,ipAddress,uploadPath);
                tempMap.put("rectifyPic",rectifyPic);
            }
        }
        Map<String, Object> result = new HashMap<>();
        result.put("list",res);
        result.put("count",count);
        return result;
    }

    //***************--APP--批量确认隐患********************//
    @RequestMapping(value = "/app_confirmHD")
    @ResponseBody
    public Map<String,Object> app_confirmHD(@RequestBody HashMap<String, Object> paramMap){
        List<Long> ids = (List<Long>)paramMap.get("ids") ;
        String str = StringUtils.join(ids.toArray(), ",");
        paramMap.put("confirmer",paramMap.get("confirmer"));
        paramMap.put("uploader",paramMap.get("uploader"));
        paramMap.put("status","CONFIRM");
        paramMap.put("idArray",str);
        paramMap.put("rectifyEndTime",paramMap.get("rectifyEndTime"));
        try {
            hiddenDangerService.confirmHidden(paramMap);
            return getSuccMap("隐患确认整改成功!");
        }catch(Exception e){
            e.printStackTrace();
            logger.error("隐患确认整改失败："+e.getMessage());
            return getSuccMap("隐患确认整改失败："+e.getMessage());
        }
    }

    //***************--APP--逐个确认隐患/重新整改********************//
    @RequestMapping(value = "/app_singleConfirmHD")
    @ResponseBody
    public Map<String,Object> app_singleConfirmHD(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request){
        String refuse = paramMap.get("refuse")==null?"":paramMap.get("refuse").toString();
        if(refuse.equalsIgnoreCase("false")){
            paramMap.put("status","CONFIRM");
            try {
                hiddenDangerService.confirmHidden(paramMap);
                return getSuccMap("隐患确认成功!");
            }catch(Exception e){
                e.printStackTrace();
                logger.error("隐患确认失败："+e.getMessage());
                return getSuccMap("隐患确认失败："+e.getMessage());
            }
        }else if(refuse.equalsIgnoreCase("true")){
            paramMap.put("status","REPORT");
            paramMap.put("cleanNotice","true");
            try {
                String loginId = paramMap.get("uploader")==null?"":paramMap.get("uploader").toString();
                hiddenDangerService.updateHiddenDanger(paramMap,"app",loginId,"noPic",request);
                return getSuccMap("隐患驳回成功!");
            }catch(Exception e){
                e.printStackTrace();
                logger.error("隐患驳回失败："+e.getMessage());
                return getSuccMap("隐患驳回失败："+e.getMessage());
            }
        }else{
            return getSuccMap("");
        }

    }

    /*------------------------------隐患排查END----------------------------*/



    /*------------------------------设备管理BEGIN----------------------------*/

    /*#####新增设备#####*/
    //***************--APP--新增设备********************//
    @RequestMapping(value = "/app_insertEquip")
    @ResponseBody
    public Map<String,Object> app_insertEquip(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request,HttpServletResponse response) {
        try {
            equipmentService.insertEquipment(paramMap);
            String message = "设备新增成功!";
            return getSuccMap(message);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("设备新增失败："+e.getMessage());
            return getSuccMap("设备新增失败："+e.getMessage());
        }
    }

    /*#####上传吊装条件验收照片#####*/
    //***************--APP--根据设备编号或设备型号查找吊装设备********************//
    @RequestMapping(value = "/app_getEquip")
    @ResponseBody
    public List<Map<String, Object>> app_getEquip(@RequestBody HashMap<String, Object> paramMap){
        String allTypes = paramMap.get("allTypes")==null?"":paramMap.get("allTypes").toString();
        if(allTypes.equalsIgnoreCase("false")){
            paramMap.put("appCrane","true");
        }
        paramMap.put("ifOrder","true");
        return equipmentService.getEquipmentInfoListNoPage(paramMap);
    }

    //***************--APP--上传吊装条件验收照片********************//
    @RequestMapping(value = "/app_reportHoist")
    @ResponseBody
    public Map<String,Object> app_reportHoist(Hoist hoist, HttpServletRequest request, HttpServletResponse response) {
        HashMap<String,Object> paramMap = new HashMap<>();
        paramMap.put("uploadPerson",hoist.getUploadPerson());
        paramMap.put("equipId",hoist.getEquipId());
        paramMap.put("type","EQUIP_WORK_HOISTING");
        paramMap.put("creator",hoist.getCreator());
        paramMap.put("equipNumber",hoist.getEquipNumber());
        paramMap.put("equipName",hoist.getEquipName());
        paramMap.put("isEquipAttachChanged",1);
        paramMap.put("createTime",hoist.getCreateTime());
        paramMap.put("uploadPersonName",hoist.getUploadPersonName());
        try {
            String picName = weChatService.uploadPic(request);
            paramMap.put("name",picName);
            equipWorkAttachService.saveEquipWorkAttach(paramMap);
            return getSuccMap("吊装令照片上传成功");
        }catch(Exception e){
            e.printStackTrace();
            logger.error("吊装令照片上传失败："+e.getMessage());
            return getSuccMap("吊装令照片上传失败："+e.getMessage());
        }
    }

    /*#####记录设备进出信息#####*/

    //***************--APP--获取站点********************//
    @RequestMapping(value = "/app_getSites")
    @ResponseBody
    public List<Map<String, Object>> app_getSites(){
        Map<String, Object> param = new HashMap<>();
        return siteService.getSiteListNoPage(param);
    }

    //***************--APP--获取还未离开的设备********************//
    @RequestMapping(value = "/getNotLeaveEquip")
    @ResponseBody
    public List<Map<String, Object>> getNotLeaveEquip(@RequestBody HashMap<String, Object> paramMap){
        paramMap.put("isApp","1");
        return equipEnterExitService.getEquipRecord(paramMap);
    }

    //***************--APP--根据ID获取对应的记录信息********************//
    @RequestMapping(value = "/getRecordById")
    @ResponseBody
    public Map<String, Object> getRecordById(@RequestParam(name = "id") Long id){
        return equipEnterExitService.getEnterExit(id.toString());
    }

    //***************--APP--保存记录********************//
    @RequestMapping(value = "/app_equipEnterExit")
    @ResponseBody
    public Map<String,Object> app_equipEnterExit(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String beginTime = paramMap.get("beginTime")== null?"":paramMap.get("beginTime").toString().replace("null","");
        String endTime = paramMap.get("endTime")== null?"":paramMap.get("endTime").toString().replace("null","");
        String message;
        try {
            if (id.length() == 0){//新增
                paramMap.put("passTime",beginTime);
                paramMap.put("passFlag",1);
                equipEnterExitService.insertEquipEnterExit(paramMap);
                if(endTime.length() > 0){
                    paramMap.put("passTime",endTime);
                    paramMap.put("passFlag",2);
                    paramMap.put("enterId",paramMap.get("id"));
                    equipEnterExitService.insertEquipEnterExit(paramMap);
                }
            } else {
                paramMap.put("passTime",beginTime);
                equipEnterExitService.updateEquipEnterExit(paramMap);

                Map<String, Object> paramMap1 = new HashMap<>();
                paramMap1.put("enterId",id);
                List<Map<String, Object>> enterExitList = equipEnterExitService.getEnterOrExitListNoPage(paramMap1);
                if (endTime.length() > 0)
                {
                    paramMap.put("passTime",endTime);
                    if (enterExitList != null && enterExitList.size() > 0)
                    {
                        paramMap.put("id",enterExitList.get(0).get("id"));
                        equipEnterExitService.updateEquipEnterExit(paramMap);
                    }else
                    {
                        paramMap.put("enterId",id);
                        paramMap.put("passFlag",2);
                        equipEnterExitService.insertEquipEnterExit(paramMap);
                    }
                }else
                {
                    if (enterExitList != null && enterExitList.size() > 0)
                    {
                        paramMap.put("id",enterExitList.get(0).get("id"));
                        equipEnterExitService.deleteEnterExit(paramMap);
                    }
                }
            }
            message = "设备进出信息保存成功!";
            return getSuccMap(message);
        }catch(Exception e){
            e.printStackTrace();
            logger.error("设备进出信息保存失败："+e.getMessage());
            return getSuccMap("设备进出信息保存失败："+e.getMessage());
        }
    }

    /*#####查看设备进出记录#####*/
    //***************--APP--查看设备进出记录********************//
    @RequestMapping(value = "/app_getEquipEnterExit")
    @ResponseBody
    public Map<String, Object> app_getEquipEnterExit(@RequestBody HashMap<String, Object> paramMap){
        paramMap.put("isApp","1");
        List<Map<String, Object>> list = equipEnterExitService.getEquipRecord(paramMap);
        int count = equipEnterExitService.getEquipRecordCount(paramMap);
        Map<String, Object> res = new HashMap<>();
        res.put("list",list);
        res.put("count",count);
        return res;
    }

    //***************--APP--查看设备信息********************//
    @RequestMapping(value = "/app_getEquipList")
    @ResponseBody
    public List<Map<String, Object>> app_getEquipList(@RequestBody HashMap<String, Object> paramMap){
        paramMap.put("isApp","1");
        paramMap.put("ifOrder","true");
        String ipAddress = paramMap.get("ipAddress")==null?"":paramMap.get("ipAddress").toString();
        String type = paramMap.get("type")==null?"":paramMap.get("type").toString();
        if(type.equalsIgnoreCase("hoist")){
            paramMap.put("appCrane","true");
        }
        String uploadPath = PropertiesUtil.get("uploadPath");
        List<Map<String, Object>> equipList = equipmentService.getEquipmentInfoListNoPage(paramMap);
        for(int i = 0; i < equipList.size(); i++){
            Map<String,Object> tempMap = equipList.get(i);
            String equipName = tempMap.get("equipName")==null?"":tempMap.get("equipName").toString();
            if(equipName.contains("吊")){
                tempMap.put("ifCrane",1);
            }else{
                tempMap.put("ifCrane",0);
            }
            String attachPath = tempMap.get("attachPath")==null?"":tempMap.get("attachPath").toString().replace("null","");
            String attachName = tempMap.get("attachName")==null?"":tempMap.get("attachName").toString().replace("null","");
            String acceptPath = tempMap.get("acceptPath")==null?"":tempMap.get("acceptPath").toString().replace("null","");
            String acceptName = tempMap.get("acceptName")==null?"":tempMap.get("acceptName").toString().replace("null","");
            String testingPath = tempMap.get("testingPath")==null?"":tempMap.get("testingPath").toString().replace("null","");
            String testingName = tempMap.get("testingName")==null?"":tempMap.get("testingName").toString().replace("null","");

            String hoistPic = this.getPicToUrl(attachPath,attachName,ipAddress,uploadPath);
            tempMap.put("hoistPic",hoistPic);


            String acceptPic = this.getPicToUrl(acceptPath,acceptName,ipAddress,uploadPath);
            tempMap.put("acceptPic",acceptPic);


            String testingPic = this.getPicToUrl(testingPath,testingName,ipAddress,uploadPath);
            tempMap.put("testingPic",testingPic);
        }
        return equipList;
    }
    @RequestMapping(value = "/app_getEquipListSimple")
    @ResponseBody
    public Map<String, Object> app_getEquipListSimple(@RequestBody HashMap<String, Object> paramMap){
        paramMap.put("isApp","1");
        paramMap.put("ifOrder","true");
        String ipAddress = paramMap.get("ipAddress")==null?"":paramMap.get("ipAddress").toString();
        String type = paramMap.get("type")==null?"":paramMap.get("type").toString();
        if(type.equalsIgnoreCase("hoist")){
            paramMap.put("appCrane","true");
        }
        List<Map<String, Object>> equipList = equipmentService.getAppEquipmentList(paramMap);
        int count = equipmentService.getAppEquipmentCount(paramMap);
        for(int i = 0; i < equipList.size(); i++){
            Map<String,Object> tempMap = equipList.get(i);
            String equipName = tempMap.get("equipName")==null?"":tempMap.get("equipName").toString();
            if(equipName.contains("吊")){
                tempMap.put("ifCrane",1);
            }else{
                tempMap.put("ifCrane",0);
            }
            String attachPath = tempMap.get("attachPath")==null?"":tempMap.get("attachPath").toString().replace("null","");
            String attachName = tempMap.get("attachName")==null?"":tempMap.get("attachName").toString().replace("null","");
            Map<String,Object> picMap = new HashMap<>();
            picMap.put("name",attachName);
            picMap.put("path",attachPath);
            String hoistPic = this.getEquipAttachPic(picMap,ipAddress);
            tempMap.put("hoistPic",hoistPic);
        }
        Map<String,Object> result = new HashMap<>();
        result.put("list",equipList);
        result.put("count",count);
        return result;
    }
    //***************--APP--查看设备详情-吊装令、验收单、检测报告列表********************//
    @RequestMapping(value = "/app_getEquipAttachInfo")
    @ResponseBody
    public Map<String, Object> app_getEquipAttachInfo(@RequestBody HashMap<String, Object> paramMap){
        Map<String, Object> res = new HashMap<>();
        String type = paramMap.get("type")==null?"":paramMap.get("type").toString();
        String ipAddress = paramMap.get("ipAddress")==null?"":paramMap.get("ipAddress").toString();
        paramMap.put("ifOrder","true");
        if(type.equalsIgnoreCase("hisHoist")){
            paramMap.put("type","hoisting");
            List<Map<String, Object>> hoistingList = equipWorkAttachService.getAppEquipWorkAttachList(paramMap);
            int hoistCount = equipWorkAttachService.getAppEquipWorkAttachCount(paramMap);
            for(Map<String, Object> hoistMap : hoistingList){
                String hoistPic = this.getEquipAttachPic(hoistMap,ipAddress);
                hoistMap.put("hoistPic",hoistPic);
            }
            res.put("hoist",hoistingList);
            res.put("count",hoistCount);
        }else if(type.equalsIgnoreCase("hisAccept")){
            paramMap.put("type","accept");
            List<Map<String, Object>> acceptList = equipWorkAttachService.getAppEquipWorkAttachList(paramMap);
            int acceptCount = equipWorkAttachService.getAppEquipWorkAttachCount(paramMap);
            for(Map<String, Object> acceptMap : acceptList){
                String acceptPic = this.getEquipAttachPic(acceptMap,ipAddress);
                acceptMap.put("acceptPic",acceptPic);
            }
            res.put("accept",acceptList);
            res.put("count",acceptCount);
        }else if(type.equalsIgnoreCase("hisTest")){
            paramMap.put("type","testing");
            List<Map<String, Object>> testingList = equipWorkAttachService.getAppEquipWorkAttachList(paramMap);
            int testingCount = equipWorkAttachService.getAppEquipWorkAttachCount(paramMap);
            for(Map<String, Object> testingListMap : testingList){
                String testingPic = this.getEquipAttachPic(testingListMap,ipAddress);
                testingListMap.put("testingPic",testingPic);
            }
            res.put("testing",testingList);
            res.put("count",testingCount);
        }
        return res;
    }

    private String getEquipAttachList(Map<String, Object> map){
        String pic = "";
        String attachPath = map.get("path")==null?"":map.get("path").toString().replace("null","");
        String attachName = map.get("name")==null?"":map.get("name").toString().replace("null","");
        if(attachPath.length() > 0 && attachName.length() > 0){
            pic = getPicBase64(attachName,attachPath);
        }
        return pic;
    }

    private String getEquipAttachPic(Map<String, Object> map,String ipAddress){
        String pic = "";
        String fileUploadPath = PropertiesUtil.get("uploadPath");
        String attachPath = map.get("path")==null?"":map.get("path").toString().replace("null","");
        String attachName = map.get("name")==null?"":map.get("name").toString().replace("null","");
        if(attachPath.length() > 0 && attachName.length() > 0){
            pic = "http://" + ipAddress + "/szgd/app/app_showPic?name="+attachName+"&path="+attachPath+"&uploadPath="+fileUploadPath;
        }
        return pic;
    }

    private String getPicToUrl(String attachPath,String attachName,String ipAddress,String uploadPath){
        String pic = "";
        if(attachPath.length() > 0 && attachName.length() > 0){
            pic = "http://" + ipAddress + "/szgd/app/app_showPic?name="+attachName+"&path="+attachPath+"&uploadPath="+uploadPath;
        }
        return pic;
    }

    //***************--APP--查看设备吊装令信息********************//
    @RequestMapping(value = "/app_getEquipHoistList")
    @ResponseBody
    public List<Map<String, Object>> app_getEquipHoistList(@RequestBody HashMap<String, Object> paramMap){
        paramMap.put("type","hoisting");
        paramMap.put("ifOrder","true");
        List<Map<String, Object>> equipAttachList = equipWorkAttachService.getEquipWorkAttachListNoPage(paramMap);
        for(int i = 0; i < equipAttachList.size(); i++){
            Map<String,Object> tempMap = equipAttachList.get(i);
            String hoistPic = this.getEquipAttachList(tempMap);
            tempMap.put("hoistPic",hoistPic);
        }
        return equipAttachList;
    }

    //***************--APP--下载excel********************//
    @RequestMapping(value = "/app_getEquipmentExcel")
    @ResponseBody
    public List<Map<String, Object>> app_getEquipmentExcel(@RequestBody HashMap<String, Object> paramMap){
        List<Map<String, Object>> dataList = equipmentService.getEquipmentExcel(paramMap);
        return dataList;
    }

    /*------------------------------设备管理END----------------------------*/



    /*------------------------------进度查看BEGIN----------------------------*/

    /*#####主体施工进度查看#####*/
    //***************--APP--获取图表数据********************//
    @RequestMapping(value = "/app_mainGetChart")
    @ResponseBody
    public Map<String,Object> app_mainGetChart(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        paramMap.put("isApp","1");
        Map<String,Object> res = mainWorkScheduleService.getChart(paramMap);
        List<String> xAxis = (List<String>)res.get("xAxis");
        List<String> legend = (List<String>)res.get("legend");
        List<List<Double>> series = new ArrayList<>();
        List<List<Double>> current = new ArrayList<>();
        List<List<Double>> cumulative = new ArrayList<>();
        List<List<Double>> total = new ArrayList<>();
        if(xAxis.size() == 0){
            xAxis.add("暂无数据");
            List<Double> temp = new ArrayList<>();
            temp.add(0.0);
            for(int i = 0; i < legend.size(); i++){
                series.add(temp);
                current.add(temp);
                cumulative.add(temp);
                total.add(temp);
            }
            res.put("series",series);
            res.put("current",current);
            res.put("cumulative",cumulative);
            res.put("total",total);
            return res;
        }else {
            return res;
        }

    }

    //***************--APP--获取查询条件下拉选择********************//
    @RequestMapping(value = "/getHistoryAndSites")
    @ResponseBody
    public Map<String, Object> getHistoryTime(){
        Map<String, Object> res = new HashMap<>();
        Map<String, Object> param = new HashMap<>();
        List<Map<String, Object>> sites = siteService.getSiteListNoPage(param);
        List<Map<String, Object>> history = mainWorkScheduleService.getTime(param);
        res.put("sites",sites);
        res.put("history",history);
        return res;
    }

    /*#####盾构施工进度查看#####*/
    //***************--APP--获取图表数据********************//
    @RequestMapping(value = "/app_tunnellingGetChart")
    @ResponseBody
    public Map<String,Object> app_tunnellingGetChart(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        paramMap.put("isApp","1");
        return tunnellingWorkScheduleService.getChart(paramMap);
    }

    //***************--APP--盾构获取查询条件下拉选择********************//
    @RequestMapping(value = "/app_getHistoryAndBid")
    @ResponseBody
    public Map<String, Object> app_getHistoryAndBid(){
        Map<String, Object> res = new HashMap<>();
        Map<String, Object> param = new HashMap<>();
        List<Map<String, Object>> bids = bidService.getBidListNoPage(param);
        List<Map<String, Object>> history = tunnellingWorkScheduleService.getTime(param);
        res.put("bids",bids);
        res.put("history",history);
        return res;
    }

    /*#####附属结构施工进度查看#####*/
    //***************--APP--获取图表数据********************//
    @RequestMapping(value = "/app_accessorGetChart")
    @ResponseBody
    public Map<String,Object> app_accessorGetChart(@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) {
        paramMap.put("isApp","1");
        return accessorStructureScheduleService.getChart(paramMap);
    }

    //***************--APP--附属结构获取查询条件下拉选择********************//
    @RequestMapping(value = "/app_getHistoryAndStation")
    @ResponseBody
    public Map<String, Object> app_getHistoryAndStation(){
        Map<String, Object> res = new HashMap<>();
        Map<String, Object> param = new HashMap<>();
        List<Map<String, Object>> sites = siteService.getSiteListNoPage(param);
        List<Map<String, Object>> history = accessorStructureScheduleService.getTime(param);
        res.put("sites",sites);
        res.put("history",history);
        return res;
    }

    /*#####整体施工进度查看#####*/
    //***************--APP--获取图表数据********************//
    @RequestMapping(value = "/app_totalGetChart")
    @ResponseBody
    public Map<String, Object> app_totalGetChart(@RequestBody HashMap<String, Object> paramMap) {
        paramMap.put("isApp","1");
        return weChatService.getTotalChart(paramMap);
    }

    //***************--APP--历史记录列表********************//
    @RequestMapping(value = "/app_chartHistoryExcel")
    @ResponseBody
    public List<Map<String,Object>> app_chartHistoryExcel(@RequestBody HashMap<String, Object> paramMap){
        String mType = paramMap.get("mType")==null?"":paramMap.get("mType").toString();
        List<Map<String,Object>> res = null;
        if(mType.equalsIgnoreCase("main")) {
            res = mainWorkScheduleService.getHistoryAttach(paramMap);
        }else if(mType.equalsIgnoreCase("tunnel")) {
            res = tunnellingWorkScheduleService.getHistoryAttach(paramMap);
        }else if(mType.equalsIgnoreCase("accessor")) {
            res = accessorStructureScheduleService.getHistoryAttach(paramMap);
        }
        return res;
    }

    //***************--APP--下载excel********************//
    @RequestMapping(value = "/app_downloadScheduleExcel")
    public void app_downloadScheduleExcel(HttpServletRequest request, HttpServletResponse response){
        String fileUploadPath = PropertiesUtil.get("uploadPath");
        String attachId = request.getParameter("attachId");
        try {
            Map<String,Object> attachMap = attachService.getAttach(attachId);
            String subPath = attachMap.get("path")==null?"":attachMap.get("path").toString();
            String fileName = attachMap.get("name")==null?"":attachMap.get("name").toString();
            String fullPath = fileUploadPath + File.separator + subPath + File.separator +  fileName;
            this.download(response, fullPath);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*------------------------------进度查看END----------------------------*/




    /*------------------------------视频监控BEGIN----------------------------*/

    /*#####摄像头列表#####*/
    //***************--APP--获取摄像头list********************//
    @RequestMapping(value = "/app_getCameraList")
    @ResponseBody
    public Map<String, Object> app_getCameraList (@RequestBody HashMap<String, Object> paramMap, HttpServletRequest request, HttpServletResponse response) throws IOException{
        Map<String, Object> res = new HashMap<>();
        List<Map<String,Object>> categoryOneArray = new ArrayList<>();
        List<Map<String,Object>> categoryTwoArray = new ArrayList<>();
        Map<String, Object> categoryOneArrayItem = new HashMap<>();
        paramMap.put("usable",1);
        paramMap.put("isStarted",1);
        paramMap.put("isFinished",0);
        paramMap.put("isApp","1");
        List<Map<String,Object>> cameraList = cameraService.getCameraInfoListNoPage(paramMap);
        String siteId = cameraList.get(0).get("siteId")==null?"":cameraList.get(0).get("siteId").toString();
        String siteNameTemp = "";
        String siteIdTemp = "";
        String ipAddress = paramMap.get("ipAddress")==null?"218.80.193.39:8080":paramMap.get("ipAddress").toString();
        String fileUploadPath = PropertiesUtil.get("snapImagePath");//摄像头图片
        String uploadPath = PropertiesUtil.get("uploadPath");//布局图
        int i;
        for(i = 0; i < cameraList.size(); i++){
            Map<String,Object> tempMap = cameraList.get(i);
            String path = tempMap.get("path")==null?"":tempMap.get("path").toString().replace("null","");
            String camPath ="";
            String fileName = tempMap.get("fileName")==null?"":tempMap.get("fileName").toString()
                    .replace("null","");
            if(path.equals("")){
                camPath=this.getPicToUrl(path,fileName,ipAddress,fileUploadPath);
            }else{
                String tempPath = fileUploadPath+"\\"+path+"\\"+fileName;
                String thumName = "thumbnail."+fileName;
                camPath =fileUploadPath+"\\"+path+"\\thumbnail."+fileName;
                File tempFile = new File(camPath);
                camPath=this.getPicToUrl(path,thumName,ipAddress,fileUploadPath);
                if(!tempFile.exists()){
                    Thumbnails.of(tempPath)
                            .size(300, 200)
                            .outputFormat("jpg")
                            .toFile(tempFile);
                }
            }
            tempMap.put("cameraPic",camPath);
//            this.getPicToUrl(path,fileName,ipAddress,fileUploadPath)
            tempMap.put("channel",tempMap.get("appChannel"));

            siteIdTemp = tempMap.get("siteId")==null?"":tempMap.get("siteId").toString();
            siteNameTemp = tempMap.get("siteName")==null?"":tempMap.get("siteName").toString();
            if(siteIdTemp.equalsIgnoreCase(siteId)){
                categoryTwoArray.add(tempMap);
            }else{
                String preName = cameraList.get(i-1).get("siteName")==null?"":cameraList.get(i-1).get("siteName").toString();
                String preSiteId = cameraList.get(i-1).get("siteId")==null?"":cameraList.get(i-1).get("siteId").toString();
                String layoutPath = cameraList.get(i-1).get("layoutPath")==null?"":cameraList.get(i-1).get("layoutPath").toString();
                String layoutFileName = cameraList.get(i - 1).get("layoutFileName") == null ? ""
                        : cameraList.get(i - 1).get("layoutFileName").toString();
                String camLayPath = "";
                if(layoutPath.equals("")){
                    camLayPath=this.getPicToUrl(layoutPath,layoutFileName,ipAddress,uploadPath);
                }else{
//                    String tempLay = this.getPicToUrl(layoutPath,layoutFileName,ipAddress,uploadPath);
                    String thumLayName = "thumbnail." + layoutFileName;
                    String tempLayPath = uploadPath + "\\" + layoutPath + "\\" + layoutFileName;
                    camLayPath = uploadPath + "\\" + layoutPath + "\\thumbnail." + layoutFileName;
                    File tempLayFile = new File(camLayPath);
                    camLayPath = this.getPicToUrl(layoutPath, thumLayName, ipAddress, uploadPath);
                    if (!tempLayFile.exists()) {
                        Thumbnails.of(tempLayPath)
                                .size(300, 200)
                                .outputFormat("jpg")
                                .toFile(tempLayFile);
                    }
                }
                categoryOneArrayItem.put("layoutPic", camLayPath);
                categoryOneArrayItem.put("categoryTwoArray",categoryTwoArray);
                categoryOneArrayItem.put("name",preName);
                categoryOneArrayItem.put("siteId",preSiteId);
                categoryOneArrayItem.put("listCount",categoryTwoArray.size());
                categoryOneArray.add(categoryOneArrayItem);
                siteId = siteIdTemp;
                categoryTwoArray = new ArrayList<>();
                categoryOneArrayItem = new HashMap<>();
                categoryTwoArray.add(tempMap);

            }
        }
        categoryOneArrayItem.put("categoryTwoArray",categoryTwoArray);
        categoryOneArrayItem.put("name",siteNameTemp);
        categoryOneArrayItem.put("siteId",siteIdTemp);
        categoryOneArrayItem.put("listCount",categoryTwoArray.size());
        String layoutPath = cameraList.get(i-1).get("layoutPath")==null?"":cameraList.get(i-1).get("layoutPath").toString();
        String layoutFileName = cameraList.get(i-1).get("layoutFileName")==null?"":cameraList.get(i-1).get("layoutFileName").toString();
        categoryOneArrayItem.put("layoutPic",this.getPicToUrl(layoutPath,layoutFileName,ipAddress,uploadPath));

        categoryOneArray.add(categoryOneArrayItem);
        res.put("categoryOneArray",categoryOneArray);
        return res;
    }

    //***************--APP--获取人员进出信息列表by站点id********************//
    @RequestMapping(value = "/app_getCameraListBySite")
    @ResponseBody
    public List<Map<String,Object>> app_getCameraListBySite(@RequestBody HashMap<String, Object> paramMap,HttpServletResponse response, HttpServletRequest request){
        return cameraService.getCameraListForApp(paramMap);
    }

    //***************--APP--查看摄像头布局图********************//
    @RequestMapping(value = "/app_viewLayoutPic")
    @ResponseBody
    public Map<String,Object> app_viewLayoutPic(@RequestBody HashMap<String, Object> paramMap,HttpServletRequest request, HttpServletResponse response) {
        String id = paramMap.get("siteId")==null?"":paramMap.get("siteId").toString();
        if(id.length() > 0){
            Map<String, Object> tempMap = siteService.getSite(id);
            String path = tempMap.get("path")==null?"":tempMap.get("path").toString().replace("null","");
            String fileName = tempMap.get("fileName")==null?"":tempMap.get("fileName").toString().replace("null","");
            if(path.length() > 0 && fileName.length() > 0) {
                String fileUploadPath = PropertiesUtil.get("uploadPath");
                FileUtil.appShowPic(path,fileName,fileUploadPath,response);
                return getSuccMap("success");
            }else{
                return getSuccMap("暂无布局图");
            }
        }else{
            return getSuccMap("暂无布局图");
        }
    }


    /*------------------------------视频监控END----------------------------*/




    /*------------------------------人员管理BEGIN----------------------------*/

    //***************--APP--人员进出信息+人数提醒********************//
    @RequestMapping(value = "/app_getPersonnelRecord")
    @ResponseBody
    public Map<String,Object> app_getPersonnelRecord(@RequestBody HashMap<String, Object> paramMap,HttpServletResponse response, HttpServletRequest request){
        return weChatService.getPersonnelRecord(paramMap);
    }

    //***************--APP--获取人员进出信息列表by站点id********************//
    @RequestMapping(value = "/app_getEnterExitList")
    @ResponseBody
    public List<Map<String,Object>> app_getEnterExitList(@RequestBody HashMap<String, Object> paramMap,HttpServletResponse response, HttpServletRequest request){
        return enterExitService.getEnterExitListForApp(paramMap);
    }

    //***************--APP--获取人员具体信息********************//
    @RequestMapping(value = "/app_getPersonnnelInfo")
    @ResponseBody
    public Map<String, Object> app_getPersonnnelInfo(@RequestBody HashMap<String, Object> paramMap){
        paramMap.put("id",paramMap.get("personnelId"));
        paramMap.put("isApp","1");
        String ipAddress = paramMap.get("ipAddress")==null?"":paramMap.get("ipAddress").toString();
        String uploadPath = PropertiesUtil.get("uploadPath");
        Map<String,Object> res = new HashMap<>();
        List<Map<String,Object>>  list = personnelInfoService.getPersonnelInfoListNoPage(paramMap);
        if (list != null && list.size() >0) {
            res = list.get(0);
        }
        Map<String, Object> certificateMap = certificateService.getCertificateParentId(paramMap.get("personnelId").toString());
        if(certificateMap != null){
            String path = certificateMap.get("path")==null?"":certificateMap.get("path").toString().replace("null","");
            String attachName = certificateMap.get("attachName")==null?"":certificateMap.get("attachName").toString().replace("null","");
            String[] strArray = attachName.split("\\.");
            int suffixIndex = strArray.length -1;
            String type = strArray[suffixIndex];
            if(!type.equalsIgnoreCase("pdf")){
                String cerPic = this.getPicToUrl(path,attachName,ipAddress,uploadPath);
                res.put("certificatePic",cerPic);
            }
        }
        return res;
    }

    //***************--APP--获取人员进出信息列表********************//
    @RequestMapping(value = "/app_getEERecord")
    @ResponseBody
    public Map<String,Object> app_getEERecord(@RequestBody HashMap<String, Object> paramMap,HttpServletResponse response, HttpServletRequest request){
        return enterExitService.app_getEERecord(paramMap);
    }

    //***************--APP--获取领导带班列表********************//
    @RequestMapping(value = "/app_getLeaderRecord")
    @ResponseBody
    public Map<String,Object> app_getLeaderRecord(@RequestBody HashMap<String, Object> paramMap,HttpServletResponse response, HttpServletRequest request){
        return enterExitService.app_getLeaderRecord(paramMap);
    }

    //***************--APP--获取早晚查列表********************//
    @RequestMapping(value = "/app_getMNRecord")
    @ResponseBody
    public Map<String,Object> app_getMNRecord(@RequestBody HashMap<String, Object> paramMap,HttpServletResponse response, HttpServletRequest request){
        return enterExitService.app_getMNRecord(paramMap);
    }

    //***************--APP--获取监理旁站列表********************//
    @RequestMapping(value = "/app_getSupervisionRecord")
    @ResponseBody
    public Map<String,Object> app_getSupervisionRecord(@RequestBody HashMap<String, Object> paramMap,HttpServletResponse response, HttpServletRequest request){
        return vehicleOnSiteSuperviseService.app_getSupervisionRecord(paramMap);
    }

    //***************--APP--获取监理旁站提醒列表********************//
    @RequestMapping(value = "/app_getSupervisionRemind")
    @ResponseBody
    public Map<String,Object> app_getSupervisionRemind(@RequestBody HashMap<String, Object> paramMap,HttpServletResponse response, HttpServletRequest request){
        return remindService.app_getSupervisionRemind(paramMap);
    }

    /*------------------------------人员管理END----------------------------*/



    /*------------------------------基坑监测BEGIN----------------------------*/

    //***************--APP--获取基坑监测数据钢琴键布局********************//
    @RequestMapping(value = "/app_getPitList")
    @ResponseBody
    public Map<String,Object> app_getPitList(@RequestBody HashMap<String, Object> paramMap){
        return weChatService.app_getPitList(paramMap);
    }

    //***************--APP--获取基坑监测数据列表by站点id********************//
    @RequestMapping(value = "/app_getPitListBySite")
    @ResponseBody
    public List<Map<String,Object>> app_getPitListBySite(@RequestBody HashMap<String, Object> paramMap){
        return pitService.getPitListForApp(paramMap);
    }

    //***************--APP--根据ID获取基坑监测数据********************//
    @RequestMapping(value = "/app_getPitListById")
    @ResponseBody
    public Map<String, Object> app_getPitListById(@RequestBody HashMap<String, Object> paramMap){
        return pitService.getPitInfo(paramMap);
    }

    /*------------------------------基坑监测END----------------------------*/


    /*------------------------------盾构监测BEGIN----------------------------*/

    //***************--APP--获取盾构监测数据钢琴键布局********************//
    @RequestMapping(value = "/app_getTunnelList")
    @ResponseBody
    public Map<String,Object> app_getTunnelList(@RequestBody HashMap<String, Object> paramMap){
        return weChatService.app_getTunnelList(paramMap);
    }

    //***************--APP--获取盾构监测数据列表by标段id********************//
    @RequestMapping(value = "/app_getTunnelListByBid")
    @ResponseBody
    public List<Map<String,Object>> app_getTunnelListByBid(@RequestBody HashMap<String, Object> paramMap){
        return tunnelService.getTunnelListForApp(paramMap);
    }

    //***************--APP--根据ID获取盾构监测数据********************//
    @RequestMapping(value = "/app_getTunnelListById")
    @ResponseBody
    public Map<String, Object> app_getTunnelListById(@RequestBody HashMap<String, Object> paramMap){
        return tunnelService.getTunnelInfo(paramMap);
    }

    /*------------------------------盾构监测END----------------------------*/


    /*------------------------------用户登录BEGIN----------------------------*/

    /*#####--APP--判断是否存在该用户,若存在则返回该用户相关信息#####*/
    @RequestMapping(value = "/app_login")
    @ResponseBody
    public Map<String,Object> app_login(@RequestBody HashMap<String, Object> paramMap) {
        try {
            int license_state = userService.getLicenseState();
            URL url = this.getClass().getClassLoader().getResource("application.license");
            if (url == null)
            {
                return getSuccMap("没有许可license文件！");
            }
            String filePath = url.getPath();//获取文件路径
            //licenseClient.validateLicense(new File(filePath));
            if (license_state/*licenseClient.getLicense_state()*/== LicenseClient.STATE_CODEERR)
            {
                return getSuccMap("在非许可的机器上运行");
            }else
            if (license_state/*licenseClient.getLicense_state()*/==LicenseClient.STATE_TIMEOUT)
            {
                return getSuccMap("许可证过期");
            }


            Map<String, Object> params = new HashMap<>();
            String userPwd = paramMap.get("userPwd")==null?"":paramMap.get("userPwd").toString();
            String loginId = paramMap.get("loginId")==null?"":paramMap.get("loginId").toString();
            String registrationID = paramMap.get("registrationID")==null?"":paramMap.get("registrationID").toString();
            String psw = AEncryptHelper.encrypt(userPwd, loginId);
            params.put("loginId",loginId);
            params.put("userPwd",psw);
            List<Map<String,Object>> ifHasUser = weChatService.login(params);
            if(ifHasUser != null && ifHasUser.size() > 0){
                String userId = ifHasUser.get(0).get("userId").toString();
                params.put("userId",userId);
                Map<String,Object> userInfo = weChatService.getUserInfo(params);
                userInfo.put("resultCode","1");
                userInfo.put("resultMsg","登录成功");
                //用户登录后是否决定发送通知
//                List<Map<String, Object>> result = hiddenDangerService.chkSendNotice(params);
//                if(result != null && result.size() != 0){
//                	for(Map<String, Object> res : result){
//	                	String yhtype = res.get("YH_TYPE")== null ? "" : res.get("YH_TYPE").toString().replace("null","");
//	                	String siteName = res.get("SITE_NAME")== null ? "" : res.get("SITE_NAME").toString().replace("null","");
//	                	String theme = res.get("THEME")== null ? "" : res.get("THEME").toString().replace("null","");
//	                	if(BusinessName.YH_REPORT.getValue().equals(yhtype)){
//	                    	JpushClientUtil.sendToRegistrationId(registrationID, "站点："+siteName+"，主题："+theme, BusinessName.TZ_MC_YHZG.getValue(), BusinessName.TZ_MC_YHZG.getValue(), res.get("YH_ID") == null? "":res.get("YH_ID").toString());
//	                	}else if(BusinessName.YH_RECTIFY.getValue().equals(yhtype)){
//	                       	JpushClientUtil.sendToRegistrationId(registrationID, "站点："+siteName+"，主题："+theme, BusinessName.TZ_MC_YHQR.getValue(), BusinessName.TZ_MC_YHQR.getValue(), res.get("YH_ID") == null? "":res.get("YH_ID").toString());
//	                	}
//                	}
//                }
                //发送隐患通知
                Map<String, Object> queryMap = new HashMap<>();
                queryMap.put("userId",userId);
                queryMap.put("tagId","1");
                queryMap.put("flag","rectify");
                queryMap.put("status","REPORT");
                int reportNum = weChatService.appGetHiddenRemindCount(queryMap);
                queryMap.put("tagId","2");
                queryMap.put("flag","confirm");
                queryMap.put("status","RECTIFY");
                int rectifyNum = weChatService.appGetHiddenRemindCount(queryMap);
                if(reportNum > 0 || rectifyNum > 0){
                    String tempStr = "";
                    if(reportNum > 0){
                        tempStr += "隐患待整改共有" + reportNum + "条";
                    }
                    if(rectifyNum > 0){
                        tempStr += ";隐患待确认共有" + rectifyNum + "条";
                    }
                    tempStr += "(详情请点击后查看)";
                    JpushClientUtil.sendToRegistrationId(registrationID, tempStr, BusinessName.TZ_MC_YH.getValue(), BusinessName.TZ_MC_YH.getValue(), "");
                }


                //发送吊装令通知
//                List<Map<String, Object>> result2 = hiddenDangerService.chkSendNoticeDzl(params);
//                if(result2 != null && result2.size() != 0){
//                	for(Map<String, Object> res : result2){
//	                	String dz_name = res.get("DZ_NAME")== null ? "" : res.get("DZ_NAME").toString().replace("null","");
//	                	String dz_bh = res.get("DZ_BH")== null ? "" : res.get("DZ_BH").toString().replace("null","");
//	                	String dz_scr = res.get("DZ_SCR")== null ? "" : res.get("DZ_SCR").toString().replace("null","");
//	                	String dz_sj = res.get("DZ_SJ")== null ? "" : res.get("DZ_SJ").toString().replace("null","");
//	                	JpushClientUtil.sendToRegistrationId(registrationID, "设备名称："+dz_name+"，上传时间："+dz_sj+"，上传人："+dz_scr+"，设备编号："+dz_bh, BusinessName.TZ_MC_DZL.getValue(), BusinessName.TZ_MC_DZL.getValue(), res.get("DZ_ID") == null? "":res.get("DZ_ID").toString());
//                	}
//                }
                int hoistNum = weChatService.appGetHoistRemindCount(queryMap);
                if(hoistNum > 0){
                    String tempStr = "吊装令上传共有" + hoistNum + "条(详情请点击后查看)";
                    JpushClientUtil.sendToRegistrationId(registrationID, tempStr, BusinessName.TZ_MC_DZL.getValue(), BusinessName.TZ_MC_DZL.getValue(), "");
                }


                Date date = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String beginDate = formatter.format(date) + " 00:00:01";
                String endDate = formatter.format(date) + " 23:59:59";
                queryMap.put("beginDate",beginDate);
                queryMap.put("endDate",endDate);

                //发送人员提醒通知
                queryMap.put("remindType","PEOPLES_REMIND");
                int result3 = weChatService.chkSendNoticePeopleRemind(queryMap);
                if(result3 > 0){
                    String tempStr = "人员到场提醒共有" + result3 + "条(详情请点击后查看)";
                    JpushClientUtil.sendToRegistrationId(registrationID, tempStr, BusinessName.TZ_MC_REMIND.getValue(), BusinessName.TZ_MC_REMIND.getValue(), "");
                }

                //发送人员次数提醒通知
                queryMap.put("remindType","PEOPLES_MONTH_REMIND");
                int result4 = weChatService.chkSendNoticePeopleRemind(queryMap);
                if(result4 > 0){
                    String tempStr = "人员进出工地次数提醒共有" + result4 + "条(详情请点击后查看)";
                    JpushClientUtil.sendToRegistrationId(registrationID, tempStr, BusinessName.TZ_MC_MONTH_REMIND.getValue(), BusinessName.TZ_MC_MONTH_REMIND.getValue(), "");
                }

                //发送监理旁站提醒通知
                queryMap.put("remindType","VEHICLE_SUPERVISE_REMIND");
                int result5 = weChatService.chkSendNoticePeopleRemind(queryMap);
                if(result5 > 0){
                    String tempStr = "监理旁站提醒共有" + result5 + "条(详情请点击后查看)";
                    JpushClientUtil.sendToRegistrationId(registrationID, tempStr, BusinessName.TZ_MC_SUPERVISION_REMIND.getValue(), BusinessName.TZ_MC_SUPERVISION_REMIND.getValue(), "");
                }

                return userInfo;
            }else{
                return getSuccMap("登录失败,不存在该用户");
            }
        }catch(Exception e){
            e.printStackTrace();
            return getSuccMap("fail："+e.getMessage());
        }
    }

    /*------------------------------用户登录END----------------------------*/


  //***************--APP--获取登录用户的菜单********************//
    @RequestMapping(value = "/getMenuList")
    @ResponseBody
    public List<Map<String, Object>> getMenuList(@RequestBody HashMap<String, Object> paramMap){
//        List<Map<String, Object>> res = new ArrayList<>();
//        return res;
        return wfRoleService.getAppMenu(paramMap);
    }
    
  //***************--APP--获取推送的tag和对应用户********************//
    @RequestMapping(value = "/app_getUsersAndTags")
    @ResponseBody
    public List<Map<String, Object>> app_getUsersAndTags(){
        return wfRoleService.getUserByYhRoleForTag(null);
    }
    
  //***************--APP--更新用户点击通知的信息********************//
    @RequestMapping(value = "/app_updRectify")
    @ResponseBody
    public String app_updRectify(@RequestBody HashMap<String, Object> paramMap){
    	String title = paramMap.get("title")== null ? "" : paramMap.get("title").toString().replace("null","");
    	if(BusinessName.TZ_MC_YHZG.getValue().equals(title) || BusinessName.TZ_MC_YHQR.getValue().equals(title)){
    		paramMap.put("tzsj", new DateConverter().convert(String.class, new Date()).toString());
            hiddenDangerService.updRectify(paramMap);
    	}else if(BusinessName.TZ_MC_DZL.getValue().equals(title)){
    		paramMap.put("tzsj", new DateConverter().convert(String.class, new Date()).toString());
            hiddenDangerService.updDzl(paramMap);
    	}else if(BusinessName.TZ_MC_REMIND.getValue().equals(title)){
            paramMap.put("tzsj", new DateConverter().convert(String.class, new Date()).toString());
            weChatService.updPeopleRemind(paramMap);
        }
    	
        return "";
    }
    
  //***************--APP--用户登录后是否决定发送通知********************//
    @RequestMapping(value = "/app_chkSendNotice")
    @ResponseBody
    public String app_chkSendNotice(@RequestBody HashMap<String, Object> paramMap){
//    	Map<String, Object> result = hiddenDangerService.chkSendNotice(paramMap);
//        String registrationID = paramMap.get("registrationID")== null ? "" : paramMap.get("registrationID").toString().replace("null","");
//        if(result != null){
//        	String yhtype = paramMap.get("yh_type")== null ? "" : paramMap.get("yh_type").toString().replace("null","");
//        	if(BusinessName.YH_REPORT.getValue().equals(yhtype)){
//            	JpushClientUtil.sendToRegistrationId(registrationID, BusinessName.TZ_MC_YHZG.getValue(), "收到一条隐患整改通知", "收到一条隐患整改通知", BusinessName.hidden_danger_approver.getValue());
//        	}else if(BusinessName.YH_RECTIFY.getValue().equals(yhtype)){
//               	JpushClientUtil.sendToRegistrationId(registrationID, BusinessName.TZ_MC_YHQR.getValue(), "收到一条隐患确认通知", "收到一条隐患确认通知", BusinessName.hidden_danger_confirmer.getValue());
//        	}
//        }
        return "";
    }

    //***************--APP--获取枚举值********************//
    @RequestMapping(value = "/app_getDict")
    @ResponseBody
    public List<SysDict> app_getDict(@RequestBody HashMap<String, Object> paramMap) {
        return sysDictService.getAllSysDict(paramMap);
    }

     /*
    获取岗位、站点下拉列表
     */
     @RequestMapping(value = "/app_getWorkTypeAndSite")
     @ResponseBody
     public Map<String,Object> app_getWorkTypeAndSite(@RequestBody HashMap<String, Object> paramMap) {
         return weChatService.app_getWorkTypeAndSite(paramMap);
     }

  //***************--APP--登录记录日志********************//
    @RequestMapping(value = "/app_saveLog")
    @ResponseBody
    public Map<String, Object> app_saveLog(@RequestBody HashMap<String, Object> paramMap) {
        logService.insertLog(paramMap);
        return getSuccMap("日志保存成功");
    }

    //***************--APP--获取图片流********************//
    @RequestMapping(value = "/app_showPic", method = RequestMethod.GET)
    @ResponseBody
    public String app_showPic(HttpServletRequest request, HttpServletResponse response) {
        try {
            String subPath = request.getParameter("path");
            String fileName = request.getParameter("name");
            String fileUploadPath = request.getParameter("uploadPath");
            OutputStream outputStream = response.getOutputStream();
            response.setContentType("application/octet-stream ");
            response.setHeader("Connection", "close"); // 表示不能用浏览器直接打开
            response.setHeader("Accept-Ranges", "bytes");// 告诉客户端允许断点续传多线程连接下载
            response.setContentType("multipart/form-data");
            response.setContentType("UTF-8");

            String filePath = FileUtil.existFile(fileUploadPath,subPath,fileName);
            InputStream inputStream = new FileInputStream(filePath +  File.separator + fileName);
            int len = 0;
            byte[] buf = new byte[1024];
            while ((len = inputStream.read(buf, 0, 1024)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //***************--APP--下载excel********************//
    @RequestMapping(value = "/app_downloadExcel")
    public void app_downloadExcel(HttpServletRequest request, HttpServletResponse response) {
        try {
            String subPath = request.getParameter("path");
            String fileName = request.getParameter("name");
            String fileUploadPath = request.getParameter("uploadPath");
            String fullPath = fileUploadPath + File.separator + subPath + File.separator +  fileName;
            this.download(response, fullPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
  //***************--APP--每30秒发送一次通知********************//
    @RequestMapping(value = "/app_SendNotice")
    @ResponseBody
    public Map<String, Object> app_SendNotice(@RequestBody HashMap<String, Object> paramMap) {
    	String registrationID = paramMap.get("registrationID")==null?"":paramMap.get("registrationID").toString();
    	List<Map<String, Object>> result = hiddenDangerService.chkSendNoticeReverse(paramMap);
        if(result != null && result.size() != 0){
        	for(Map<String, Object> res : result){
            	String yhtype = res.get("YH_TYPE")== null ? "" : res.get("YH_TYPE").toString().replace("null","");
            	String siteName = res.get("SITE_NAME")== null ? "" : res.get("SITE_NAME").toString().replace("null","");
            	String theme = res.get("THEME")== null ? "" : res.get("THEME").toString().replace("null","");
            	if(BusinessName.YH_REPORT.getValue().equals(yhtype)){
                	JpushClientUtil.sendToRegistrationId(registrationID, "站点："+siteName+"，主题："+theme, BusinessName.TZ_MC_YHZG.getValue(), BusinessName.TZ_MC_YHZG.getValue(), res.get("YH_ID") == null? "":res.get("YH_ID").toString());
                	hiddenDangerService.updReverseRectify(res.get("YH_ID").toString());
            	}else if(BusinessName.YH_RECTIFY.getValue().equals(yhtype)){
                   	JpushClientUtil.sendToRegistrationId(registrationID, "站点："+siteName+"，主题："+theme, BusinessName.TZ_MC_YHQR.getValue(), BusinessName.TZ_MC_YHQR.getValue(), res.get("YH_ID") == null? "":res.get("YH_ID").toString());
                   	hiddenDangerService.updReverseConfirm(res.get("YH_ID").toString());
            	}
        	}
        }

    	return getSuccMap("调用成功");
    }

    //跳转app下载界面
    @RequestMapping(value = "toAppDownload", method = RequestMethod.GET)
    public ModelAndView toAppDownload(HttpServletRequest request, HttpSession session) {
        ModelAndView mv = getModelAndView(request,session,"/appDownload");
        String userFilePath = PropertiesUtil.get("userFilePath");
        File file = new File(userFilePath);
        String apkName = getApkName(file);
        String fileName = apkName.substring(0,apkName.lastIndexOf("."));
        String[] strArray = fileName.split("_");
        int suffixIndex = strArray.length -1;
        String version = strArray[suffixIndex];
        mv.addObject("apkName",apkName);
        mv.addObject("version",version);
        return mv;
    }

    //跳转查看app发布履历界面
    @RequestMapping(value = "toAppHistory", method = RequestMethod.GET)
    public ModelAndView toAppHistory(HttpServletRequest request, HttpSession session) {
        ModelAndView mv = getModelAndView(request,session,"/appHistory");
        return mv;
    }

    //动态获取apk名称
    private static String getApkName(File file) {
        String apkName = "";
        // 判断是否为文件夹
        File[] listFiles = file.listFiles();
        for (File f : listFiles) {
            if (f.getName().endsWith(".apk")) {
                apkName = f.getName();
            }
        }
        return apkName;
    }

    //下载apk
    @RequestMapping(value = "/downloadUserFileApp", method = RequestMethod.GET)
    public void downloadUserFileApp(HttpServletRequest request,HttpServletResponse response) throws IOException {
        String fileName = URLDecoder.decode(request.getParameter("fileName"),"UTF-8");
        String userFiledPath = PropertiesUtil.get("userFilePath");
        String fileAbsolutePath = userFiledPath + File.separator + fileName;
        File file = new File(fileAbsolutePath);
        if (!file.exists())
        {
            response.setContentType("text/html;character=utf-8");
            PrintWriter pw = response.getWriter();
            pw.println("暂不提供该文件下载！");
            pw.flush();
            pw.close();
        }else
        {
            InputStream fis = new BufferedInputStream(new FileInputStream(file));
            byte[] buffer;
            buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            response.reset();

            fileName = new String(fileName.getBytes(),"ISO8859-1");
            long fileLength = file.length();
            response.addHeader("Content-Disposition", "attachment;filename="+ fileName);
            response.addHeader("Content-Length", "" + String.valueOf(fileLength));
            response.setContentType("application/octet-stream");    //自动匹配文件类型
            OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            outputStream.write(buffer);
            outputStream.flush();
            outputStream.close();
        }

    }

    //上传日志
    @RequestMapping(value = "/app_postLog",method = RequestMethod.POST)
    public void postLog(HttpServletRequest request,HttpServletResponse response) throws IOException {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if(isMultipart) {
//            String fileName = URLDecoder.decode(request.getParameter("file"),"UTF-8");
            MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
            MultipartFile multipartFile = req.getFile("file");
            if(multipartFile != null){
                String userFiledPath = PropertiesUtil.get("logPath");
                String fileAbsolutePath = userFiledPath + File.separator ;
                //裁剪用户id
                String originalFirstName = multipartFile.getOriginalFilename();
                String firstName = originalFirstName.substring(0, originalFirstName.indexOf("."));
                logger.info(originalFirstName+" "+firstName);
                //取得图片的格式后缀
                String originalLastName = multipartFile.getOriginalFilename();
                String lastName = originalLastName.substring(originalLastName.lastIndexOf("."));
                logger.info(originalLastName+" "+lastName);
                String fileName = firstName + "-" + lastName;
                logger.info(fileName);
                File dir = new File(fileAbsolutePath,fileName);
                if (!dir.exists()) {
                    dir.mkdirs();
                    System.out.println("创建文件目录成功：" + dir.getAbsolutePath());
                }
                multipartFile.transferTo(dir);
            }
        }
    }

    //***************--APP--项管部-上传文件图片********************//
    @RequestMapping(value = "/app_pmUploadFile", method = {RequestMethod.POST})
    @ResponseBody
    public Map<String,Object> app_pmUploadFile(PmFile pmFile, HttpServletRequest request, HttpServletResponse response) {
        String id = null;
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("fileName",pmFile.getFileName());
        paramMap.put("fileType",pmFile.getFileType());
        paramMap.put("creator",pmFile.getCreator());
        try {
            weChatService.insertPmFile(paramMap,request);
            if(paramMap.get("id") == null || paramMap.get("id").toString().length() == 0){
                return getErrorMap("信息提交失败，请重新填写提交");
            }else{
                id = paramMap.get("id").toString();
                return getSuccMap("提交成功");
            }
        }catch(Exception e){
            e.printStackTrace();
            logger.error("信息提交失败,照片ID："+id+"---"+e.getMessage());
            return getSuccMap("信息提交失败："+e.getMessage());
        }
    }

    //***************--APP--项管部-获取文件图片********************//
    @RequestMapping(value = "/app_getPmFiles")
    @ResponseBody
    public Map<String, Object> app_getPmFiles (@RequestBody HashMap<String, Object> paramMap) throws IOException{
        List<Map<String,Object>> res = weChatService.getPmFileList(paramMap);
        int count = weChatService.getPmFileCount(paramMap).size();
        Map<String, Object> result = new HashMap<>();
        result.put("list",res);
        result.put("count",count);
        return result;
    }
}
