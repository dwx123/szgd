package com.szgd.converter;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

/**
 * 
 * 日期转换函数
 */
public class TimeTrans  implements Converter<String, Date> {
	
	private static Logger log = Logger.getLogger(TimeTrans.class); 
	
	@Override
	public Date convert(String htmlString) {
	
		SimpleDateFormat  simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			return  simpleDateFormat.parse(htmlString);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		//参数绑定失败返回 Null
		return null;
	}

}
