package com.szgd.dao.ecdata.elseData;

import java.util.List;
import java.util.Map;

public interface PitMapper {
    public List<Map<String,Object>> getTopPitList(Map<String, Object> params);

    public List<Map<String,Object>> getPitListForApp(Map<String, Object> params);

    public int getPitCountForApp(Map<String, Object> params);

    public List<Map<String,Object>> getInitPitListForApp(Map<String, Object> params);
}
