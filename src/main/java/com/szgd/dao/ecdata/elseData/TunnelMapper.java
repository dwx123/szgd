package com.szgd.dao.ecdata.elseData;

import java.util.List;
import java.util.Map;

public interface TunnelMapper {
    public List<Map<String,Object>> getTopTunnelList(Map<String, Object> params);

    public List<Map<String,Object>> getTunnelListForApp(Map<String, Object> params);

    public int getTunnelCountForApp(Map<String, Object> params);

    public List<Map<String,Object>> getInitTunnelListForApp(Map<String, Object> params);
}
