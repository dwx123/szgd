package com.szgd.dao.ecdata.equipment;

import java.util.List;
import java.util.Map;

public interface EquipEnterExitMapper {
    public List<Map<String,Object>> getEquipEnterAndExitList(Map<String, Object> params);
    public List<Map<String,Object>> getEquipEnterOrExitList(Map<String, Object> params);
    public List<Map<String,Object>> getTopNEquipEnterOrExitList(Map<String, Object> params);
    public List<Map<String,Object>> getTopNEquipEnterAndExitList(Map<String, Object> params);
    public List<Map<String,Object>> getTopNEquipEnter_ExitList(Map<String, Object> params);
    public List<Map<String,Object>> getEquipEnter_ExitList(Map<String, Object> params);
    public List<Map<String,Object>> getEquipRecord(Map<String, Object> params);
    public void updateEquipEnterExit(Map<String, Object> params);
    public void insertEquipEnterExit(Map<String, Object> params);
    public void deleteEnterExit(Map<String, Object> params);
    public int getEquipEnterOrExitCount(Map<String, Object> params);
    public int getBePresentCount(Map<String, Object> params);

    public List<Map<String,Object>> getEquipRecordCount(Map<String, Object> params);
}
