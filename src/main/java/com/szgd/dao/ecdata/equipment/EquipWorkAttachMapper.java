package com.szgd.dao.ecdata.equipment;

import java.util.List;
import java.util.Map;


public interface EquipWorkAttachMapper {
	public List<Map<String,Object>> getEquipWorkAttachList(Map<String, Object> params);
	public void updateEquipWorkAttach(Map<String, Object> params);
	public void insertEquipWorkAttach(Map<String, Object> params);
	public void deleteEquipWorkAttach(Map<String, Object> params);
	public int getEquipWorkAttachCount(Map<String, Object> params);

	public List<Map<String,Object>> getAppEquipWorkAttachList(Map<String, Object> params);
	public int getAppEquipWorkAttachCount(Map<String, Object> params);
}
