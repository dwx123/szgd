package com.szgd.dao.ecdata.equipment;

import java.util.List;
import java.util.Map;


public interface EquipWorkMapper {
	public List<Map<String,Object>> getEquipWorkList(Map<String, Object> params);
	public void updateEquipWork(Map<String, Object> params);
	public void insertEquipWork(Map<String, Object> params);
	public void deleteEquipWork(Map<String, Object> params);
}
