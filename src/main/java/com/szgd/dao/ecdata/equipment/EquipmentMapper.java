package com.szgd.dao.ecdata.equipment;

import java.util.List;
import java.util.Map;


public interface EquipmentMapper {
	public List<Map<String,Object>> getEquipmentList(Map<String, Object> params);
	public List<Map<String,Object>> getEquipmentExcel(Map<String, Object> params);
	public int getEquipmentCount(Map<String, Object> params);
	public void updateEquipment(Map<String, Object> params);
	public void insertEquipment(Map<String, Object> params);
	public void deleteEquipment(Map<String, Object> params);

	public List<Map<String,Object>> getAppEquipmentList(Map<String,Object> params);
	public int getAppEquipmentCount(Map<String,Object> params);
}
