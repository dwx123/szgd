package com.szgd.dao.ecdata.gatherEquip;

import java.util.List;
import java.util.Map;


public interface CameraInfoMapper {
	public List<Map<String,Object>> getCameraInfoList(Map<String, Object> params);
	public void updateCameraInfo(Map<String, Object> params);
	public void insertCameraInfo(Map<String, Object> params);
	public void deleteCameraInfo(Map<String, Object> params);

	public List<Map<String,Object>> getCameraListForApp(Map<String, Object> params);
}
