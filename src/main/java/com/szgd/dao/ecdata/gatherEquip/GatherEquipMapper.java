package com.szgd.dao.ecdata.gatherEquip;

import java.util.List;
import java.util.Map;


public interface GatherEquipMapper {
	public List<Map<String,Object>> getGatherEquipList(Map<String, Object> params);
	public void updateGatherEquip(Map<String, Object> params);
	public void insertGatherEquip(Map<String, Object> params);
	public void deleteGatherEquip(Map<String, Object> params);
}
