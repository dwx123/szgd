package com.szgd.dao.ecdata.hiddenDanger;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;


public interface HiddenDangerMapper {
	public List<Map<String,Object>> getHiddenDangerList(Map<String, Object> params);
	public int getHiddenDangerCount(Map<String, Object> params);
	public void updateHiddenDanger(Map<String, Object> params);
	public void insertHiddenDanger(Map<String, Object> params);
	public void deleteHiddenDanger(Map<String, Object> params);
	public List<Map<String,Object>> getHiddenConfirmList(Map<String, Object> params);
	public void confirmHidden(Map<String, Object> params);
	public void updRectify(Map<String, Object> params);
	public void updDzl(Map<String, Object> params);
	public void updReverseRectify(@Param("yhid") String yhId);
	public void updReverseConfirm(@Param("yhid") String yhId);
	List<Map<String,Object>> chkSendNotice(Map<String, Object> params);
	List<Map<String,Object>> chkSendNoticeReverse(Map<String, Object> params);
	List<Map<String,Object>> chkSendNoticeDzl(Map<String, Object> params);
	public List<Map<String,Object>> getAppHiddenDangerList(Map<String, Object> params);
	public List<Map<String,Object>> getAppHiddenDangerCount(Map<String, Object> params);
}
