package com.szgd.dao.ecdata.personnel;

import java.util.List;
import java.util.Map;


public interface AttachMapper {
	public List<Map<String,Object>> getAttachList(Map<String, Object> params);
	public void updateAttach(Map<String, Object> params);
	public void insertAttach(Map<String, Object> params);
	public void deleteAttach(Map<String, Object> params);
}
