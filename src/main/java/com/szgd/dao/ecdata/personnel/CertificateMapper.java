package com.szgd.dao.ecdata.personnel;

import java.util.List;
import java.util.Map;


public interface CertificateMapper {
	public List<Map<String,Object>> getCertificateList(Map<String, Object> params);
	public void updateCertificate(Map<String, Object> params);
	public void insertCertificate(Map<String, Object> params);
	public void deleteCertificate(Map<String, Object> params);
}
