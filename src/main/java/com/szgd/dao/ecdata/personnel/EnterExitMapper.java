package com.szgd.dao.ecdata.personnel;

import java.util.List;
import java.util.Map;


public interface EnterExitMapper {
	public List<Map<String,Object>> getEnterAndExitList(Map<String, Object> params);
	public List<Map<String,Object>> getEnterOrExitList(Map<String, Object> params);
	public List<Map<String,Object>> getTopNEnterOrExitList(Map<String, Object> params);
	public List<Map<String,Object>> getTopNEnterAndExitList(Map<String, Object> params);
	public List<Map<String,Object>> getEnterExitList(Map<String, Object> params);
	public List<Map<String,Object>> getTopNEnter_ExitList(Map<String, Object> params);
	public List<Map<String,Object>> getEnter_ExitList(Map<String, Object> params);
	public List<Map<String,Object>> getEnterExitFromList(Map<String, Object> params);
	public void updateEnterExit(Map<String, Object> params);
	public void insertEnterExit(Map<String, Object> params);
	public void insertEnterExitFrom(Map<String, Object> params);
	public int getEnterOrExitCount(Map<String, Object> params);
	public int getBePresentCount(Map<String, Object> params);
	public void updateDataStatus(Map<String, Object> params);

	public List<Map<String,Object>> getEnterExitListForApp(Map<String, Object> params);
	Integer countElusivePeople(Map<String, Object> params);

	public List<Map<String,Object>> getTop10Enter_ExitList(Map<String, Object> params);
	public List<Map<String,Object>> getEnterExitListCount(Map<String, Object> params);
	public List<Map<String,Object>> getPresentPersonCount(Map<String, Object> params);

	public List<Map<String,Object>> getLeaderRecord(Map<String, Object> params);

	public List<Map<String,Object>> getMNRecord(Map<String, Object> params);

	public List<Map<String,Object>> app_getEEList(Map<String, Object> params);
	public List<Map<String,Object>> app_getEECount(Map<String, Object> params);
	public List<Map<String,Object>> app_getLeaderList(Map<String, Object> params);
	public List<Map<String,Object>> app_getLeaderCount(Map<String, Object> params);
	public List<Map<String,Object>> app_getMNList(Map<String, Object> params);
	public List<Map<String,Object>> app_getMNCount(Map<String, Object> params);
}
