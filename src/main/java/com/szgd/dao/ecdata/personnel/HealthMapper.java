package com.szgd.dao.ecdata.personnel;

import com.szgd.bean.Attach;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface HealthMapper {
    public List<Map<String,Object>> getHealthList(Map<String, Object> params);

    public Map<String,Object> getHealthDetailInfo(@Param("id") Long id);

    public List<Map<String,Object>> getAllPerson();

    public Map<String,Object> getPersonnelInfo(Map<String, Object> params);

    public void saveHealthInfo(Map<String, Object> params);

    public int deleteHealth(Map<String, Object> params);

    public void insertHealthInfo(Map<String, Object> params);

    public void insertHealthAttach(Attach attach);

    public Map<String,Object> getHealthAttach(@Param("id") Long id);
}
