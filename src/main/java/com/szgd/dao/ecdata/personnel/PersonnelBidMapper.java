package com.szgd.dao.ecdata.personnel;

import java.util.List;
import java.util.Map;


public interface PersonnelBidMapper {
	public List<Map<String,Object>> getPersonnelBidList(Map<String, Object> params);
	public void updatePersonnelBid(Map<String, Object> params);
	public void insertPersonnelBid(Map<String, Object> params);
	public void deletePersonnelBid(Map<String, Object> params);
	public void deletePersonnelBidById(Map<String, Object> params);
}
