package com.szgd.dao.ecdata.personnel;

import java.util.List;
import java.util.Map;


public interface PersonnelFaceIdMapper {

	public int getCount(Map<String, Object> params);
	public List<Map<String,Object>> getPersonnelFaceIdList(Map<String, Object> params);
	public List<Map<String,Object>> getMaxPersonId(Map<String, Object> params);
	public void updatePersonnelFaceId(Map<String, Object> params);
	public void insertPersonnelFaceId(Map<String, Object> params);
	public void deletePersonnelFaceId(Map<String, Object> params);
	public void deletePersonnelFaceIdById(Map<String, Object> params);
	public void updatePersonId(Map<String, Object> params);
	public void updateFaceId(Map<String, Object> params);
	public void updatePersonnelFaceIdNull(Map<String, Object> params);
}
