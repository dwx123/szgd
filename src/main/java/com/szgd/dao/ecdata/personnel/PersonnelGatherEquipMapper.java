package com.szgd.dao.ecdata.personnel;

import java.util.List;
import java.util.Map;


public interface PersonnelGatherEquipMapper {

	public int getCount(Map<String, Object> params);
	public List<Map<String,Object>> getPersonnelGatherEquipList(Map<String, Object> params);
	public void updatePersonnelGatherEquip(Map<String, Object> params);
	public void insertPersonnelGatherEquip(Map<String, Object> params);
	public void deletePersonnelGatherEquip(Map<String, Object> params);
	public void deletePersonnelGatherEquipById(Map<String, Object> params);

}
