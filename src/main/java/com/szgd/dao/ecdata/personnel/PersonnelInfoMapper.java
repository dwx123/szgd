package com.szgd.dao.ecdata.personnel;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;


public interface PersonnelInfoMapper {
	public List<Map<String,Object>> getPersonnelInfoList(Map<String, Object> params);
	public List<Map<String,Object>> getPersonnelInfo(Map<String, Object> params);
	public List<Map<String,Object>> getPersonnelInfoByPersonId(Map<String, Object> params);

	public List<Map<String,Object>> getPersonnelExcel();
	public List<Map<String,Object>> getSiteByUser(Map<String, Object> params);
	public List<Map<String,Object>> getPersonBySite(Map<String, Object> params);
	public List<Map<String,Object>> getAllRectifyPerson();

	public void updatePersonnelInfo(Map<String, Object> params);
	public void updateOtherSystemId(Map<String, Object> params);
	public void updatePersonId(Map<String, Object> params);
	public void updateFaceId(Map<String, Object> params);
	public void updateBidCount(Map<String, Object> params);

	public void insertPersonnelInfo(Map<String, Object> params);
	public void deletePersonnelInfo(Map<String, Object> params);
	public void updateWorker(Map<String, Object> params);
	public void updateResetZJJ(Map<String, Object> params);
	public List<Map<String,Object>> getPersonnelByUser(Map<String, Object> params);
	public Map<String,Object> selectByPrimaryKey(@Param("id")Integer id);
}
