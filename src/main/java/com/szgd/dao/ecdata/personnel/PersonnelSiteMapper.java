package com.szgd.dao.ecdata.personnel;

import java.util.List;
import java.util.Map;


public interface PersonnelSiteMapper {
	public List<Map<String,Object>> getPersonnelSiteList(Map<String, Object> params);
	public void updatePersonnelSite(Map<String, Object> params);
	public void insertPersonnelSite(Map<String, Object> params);
	public void deletePersonnelSite(Map<String, Object> params);
	public void deletePersonnelSiteById(Map<String, Object> params);
}
