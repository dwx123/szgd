package com.szgd.dao.ecdata.personnel;

import java.util.List;
import java.util.Map;

import com.szgd.bean.RemindCfg;
import com.szgd.bean.RemindCfgTitle;

public interface RemindCfgMapper {
    /**
     *
     * @mbggenerated 2019-04-01
     */
    int deleteByPrimaryKey(Long id);

    /**
     *
     * @mbggenerated 2019-04-01
     */
    int insert(RemindCfg record);

    /**
     *
     * @mbggenerated 2019-04-01
     */
    int insertSelective(RemindCfg record);

    /**
     *
     * @mbggenerated 2019-04-01
     */
    RemindCfg selectByPrimaryKey(Long id);

    /**
     *
     * @mbggenerated 2019-04-01
     */
    int updateByPrimaryKeySelective(RemindCfg record);

    /**
     *
     * @mbggenerated 2019-04-01
     */
    int updateByPrimaryKey(RemindCfg record);
    
    List<RemindCfg> getRemindCfgData(Map<String, Object> params);
    
    List<RemindCfgTitle> getRemindCfgTitle(Map<String, Object> params);
    
    List<Map<String, Object>> getWorkSiteInfo(Map<String, Object> params);
}