package com.szgd.dao.ecdata.personnel;

import java.util.List;
import java.util.Map;

import com.szgd.bean.Remind;

public interface RemindMapper {
    /**
     *
     * @mbggenerated 2019-04-01
     */
    int deleteByPrimaryKey(Long id);

    /**
     *
     * @mbggenerated 2019-04-01
     */
    int insert(Remind record);

    /**
     *
     * @mbggenerated 2019-04-01
     */
    int insertSelective(Remind record);

    /**
     *
     * @mbggenerated 2019-04-01
     */
    Remind selectByPrimaryKey(Long id);

    /**
     *
     * @mbggenerated 2019-04-01
     */
    int updateByPrimaryKeySelective(Remind record);

    /**
     *
     * @mbggenerated 2019-04-01
     */
    int updateByPrimaryKey(Remind record);
    
    List<Remind> getCertificationRemindInfo(Map<String, Object> params);
    
    List<Remind> getPeoplesRemindInfo(Map<String, Object> params);
    List<Remind> getPeoplesMonthRemindInfo(Map<String, Object> params);
    List<Remind> getVehicleOnSiteSuperviseRemindInfo(Map<String, Object> params);

    List<Remind> getExaminationRemindInfo(Map<String, Object> params);
    
    List<Remind> getEquipmentChkRemindInfo(Map<String, Object> params);
    
    List<Remind> getEquipmentInsureRemindInfo(Map<String, Object> params);

    int getRemindDataCnt(Remind record);
    List<Remind> getRemindList(Remind record);
    int updateRemindTimer(Remind record);
    int updateRemindContent(Remind record);

    int updateRemindDelFlag(Map<String, Object> params);
    int updateRemindIsOk(Map<String, Object> params);

    int deleteRemind(Map<String, String> params);

    List<Remind> getRemindData(Map<String, Object> params);

    List<Map<String,Object>> getAppRemindContent(Map<String, Object> params);

    List<Remind> app_getSupervisionRemindList(Map<String, Object> params);
}