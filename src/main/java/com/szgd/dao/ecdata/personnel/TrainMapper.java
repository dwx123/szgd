package com.szgd.dao.ecdata.personnel;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TrainMapper {
    public List<Map<String,Object>> getTrainList(Map<String, Object> params);

    public Map<String,Object> getTrainDetailInfo(@Param("id") Long id);

    public List<Map<String,Object>> getTrainRecordList(Map<String, Object> params);

    public void saveTrainInfo(Map<String, Object> params);

    public int deleteTrain(Map<String, Object> params);

    public List<Map<String,Object>> getAllPerson();

    public void insertTrainInfo(Map<String, Object> params);
    public void insertTrainRecord(Map<String, Object> params);

}
