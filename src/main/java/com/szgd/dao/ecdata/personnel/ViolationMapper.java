package com.szgd.dao.ecdata.personnel;

import java.util.List;
import java.util.Map;


public interface ViolationMapper {
	public List<Map<String,Object>> getViolationList(Map<String, Object> params);
	public void updateViolation(Map<String, Object> params);
	public void insertViolation(Map<String, Object> params);
	public void deleteViolation(Map<String, Object> params);
}
