package com.szgd.dao.ecdata.project;

import java.util.List;
import java.util.Map;


public interface BidMapper {
	public List<Map<String,Object>> getBidList(Map<String, Object> params);
	public void updateBid(Map<String, Object> params);
	public void insertBid(Map<String, Object> params);
	public void deleteBid(Map<String, Object> params);
}
