package com.szgd.dao.ecdata.project;

import java.util.List;
import java.util.Map;

public interface ProjectMapper {
	public List<Map<String,Object>> getProjectList(Map<String, Object> params);
	public void updateProject(Map<String, Object> params);
	public void insertProject(Map<String, Object> params);
	public void deleteProject(Map<String, Object> params);
}
