package com.szgd.dao.ecdata.project;

import java.util.List;
import java.util.Map;


public interface ProjectScheduleConfigMapper {
	public List<Map<String,Object>> getProjectScheduleConfigList(Map<String, Object> params);
	public void updateProjectScheduleConfig(Map<String, Object> params);
	public void insertProjectScheduleConfig(Map<String, Object> params);
	public void deleteProjectScheduleConfig(Map<String, Object> params);
}
