package com.szgd.dao.ecdata.project;

import com.szgd.bean.Schedule;

import java.util.List;
import java.util.Map;


public interface ProjectScheduleMapper {
	public List<Map<String,Object>> getProjectScheduleList(Map<String, Object> params);
	public void updateProjectSchedule(Schedule schedule);
	public void insertProjectSchedule(Schedule schedule);
	public void deleteProjectSchedule(Map<String, Object> params);
}
