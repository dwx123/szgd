package com.szgd.dao.ecdata.project;

import java.util.List;
import java.util.Map;


public interface SiteMapper {
	public List<Map<String,Object>> getSiteList(Map<String, Object> params);
	public List<Map<String,Object>> getStartedSiteList(Map<String, Object> params);
	public void updateSite(Map<String, Object> params);
	public void insertSite(Map<String, Object> params);
	public void deleteSite(Map<String, Object> params);

	public List<Map<String,Object>> app_getSiteList(Map<String, Object> params);
}
