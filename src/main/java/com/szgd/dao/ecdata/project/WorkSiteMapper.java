package com.szgd.dao.ecdata.project;

import java.util.List;
import java.util.Map;


public interface WorkSiteMapper {
	public List<Map<String,Object>> getWorkSiteList(Map<String, Object> params);
	public void updateWorkSite(Map<String, Object> params);
	public void insertWorkSite(Map<String, Object> params);
	public void deleteWorkSite(Map<String, Object> params);
}
