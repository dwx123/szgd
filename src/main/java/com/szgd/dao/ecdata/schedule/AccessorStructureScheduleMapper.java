package com.szgd.dao.ecdata.schedule;

import java.util.List;
import java.util.Map;

public interface AccessorStructureScheduleMapper {
	public List<Map<String,Object>> getAccessorStructureScheduleList(Map<String, Object> params);
	public List<Map<String,Object>> getChart(Map<String, Object> params);
    public List<Map<String,Object>> getTime(Map<String, Object> params);
	public List<Map<String,Object>> getChartName(Map<String, Object> params);
	public List<Map<String,Object>> getHistoryAttach(Map<String, Object> params);
	public void updateAccessorStructureSchedule(Map<String, Object> params);
	public void insertAccessorStructureSchedule(Map<String, Object> params);
	public void deleteAccessorStructureSchedule(Map<String, Object> params);
	public Double accessorCompleteRateAvg(Map<String, Object> params);
}
