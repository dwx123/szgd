package com.szgd.dao.ecdata.schedule;

import java.util.List;
import java.util.Map;

public interface MainWorkScheduleMapper {
	public List<Map<String,Object>> getMainWorkScheduleList(Map<String, Object> params);
	public List<Map<String,Object>> getChart(Map<String, Object> params);
	public List<String> getChartName(Map<String, Object> params);
	public List<Map<String,Object>> getTime(Map<String, Object> params);
	public List<Map<String,Object>> getHistoryAttach(Map<String, Object> params);
	public void updateMainWorkSchedule(Map<String, Object> params);
	public void insertMainWorkSchedule(Map<String, Object> params);
	public void deleteMainWorkSchedule(Map<String, Object> params);
	public Double siteCompleteRateAvg(Map<String, Object> params);
	public Double sectionCompleteRateAvg(Map<String, Object> params);
	public Double mainProjectCompleteRateAvg(Map<String, Object> params);
	public List<Map<String,Object>> getXAxisNum(Map<String, Object> params);
}
