package com.szgd.dao.ecdata.schedule;

import java.util.List;
import java.util.Map;

public interface SegmentProduceScheduleMapper {
	public List<Map<String,Object>> getSegmentProduceScheduleList(Map<String, Object> params);
	public void updateSegmentProduceSchedule(Map<String, Object> params);
	public void insertSegmentProduceSchedule(Map<String, Object> params);
	public void deleteSegmentProduceSchedule(Map<String, Object> params);
	public List<Map<String,Object>> getChart(Map<String, Object> params);
	public List<String> getChartName(Map<String, Object> params);
	public List<Map<String,Object>> getTime(Map<String, Object> params);
	public Double segmentCompleteRateAvg(Map<String, Object> params);
}
