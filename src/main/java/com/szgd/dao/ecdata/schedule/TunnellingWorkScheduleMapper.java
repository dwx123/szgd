package com.szgd.dao.ecdata.schedule;

import java.util.List;
import java.util.Map;

public interface TunnellingWorkScheduleMapper {
	public List<Map<String,Object>> getTunnellingWorkScheduleList(Map<String, Object> params);
	public List<Map<String,Object>> getChart(Map<String, Object> params);
    public List<Map<String,Object>> getTime(Map<String, Object> params);
    public List<String> getTableBidName(Map<String, Object> params);
	public List<Map<String,Object>> getHistoryAttach(Map<String, Object> params);
	public void updateTunnellingWorkSchedule(Map<String, Object> params);
	public void insertTunnellingWorkSchedule(Map<String, Object> params);
	public void deleteTunnellingWorkSchedule(Map<String, Object> params);
	public Double tunnellingCompleteRateAvg(Map<String, Object> params);
}
