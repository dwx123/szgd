package com.szgd.dao.ecdata.sys;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;


public interface DeptMapper {

	public List<Map<String,Object>> getDeptListRecord(Map<String, Object> params);


	public List<Map<String,Object>> getParentDept(String deptCD);
	
	public String getParentDeptName(@Param("deptCD") String deptCD);
	
	public List<Map<String,Object>> getAllDeptInfo(Map<String, Object> params);
	public List<Map<String,Object>>  getAllChildById(@Param("id") String id);
	public List<Map<String,String>> getFirstLevelDept(Map<String, Object> params);
	public List<Map<String,String>> getSecondLevelDept(Map<String, Object> params);

	public List<String> getFirstLevelByDept();

	public void batchInsertDept(List<Map<String, Object>> params);

	public void deleteTempDept(String s);
	public void insertNewDept(String s);
	public void updateDelDept(String s);
	public void updateAllDept(String s);
	
	
	public List<Map<String,Object>> getNoGrantUsers(Map<String, Object> params);
	
	public List<Map<String,Object>> getOwenDeptUsers(Map<String, Object> params);
	
	public Integer getZhiDui_deptId(String dept_cd);
	List<Map<String,String>> getDeptCDByName(Map<String, Object> params);
	
	public Integer getNewDeptId();
	
	public Integer getNewSort();
	
	public int insertDept(Map<String, Object> params) throws Exception;
	
	public int updateDept(Map<String, Object> params) throws Exception;
}
