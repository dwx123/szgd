package com.szgd.dao.ecdata.sys;

import java.util.List;
import java.util.Map;


public interface LogMapper {
	public List<Map<String,Object>> getLogList(Map<String, Object> params);
	public void insertLog(Map<String, Object> params);
}
