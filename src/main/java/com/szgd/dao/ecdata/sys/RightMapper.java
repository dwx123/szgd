package com.szgd.dao.ecdata.sys;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RightMapper {
	public List<Map<String, String>> getUserRight(Map<String, String> map);

    public List<Map<String, String>> getMenuByUserId(String userId);
    public List<Map<String,String>> getAllRightByUserID(String userId);
    public List<Map<String,Object>> getAllRoleRightByUserId(String userId);
    public List<Map<String,String>> getAllRightByRoleID(String roleId);
    public List<Map<String,String>> getRightByUserIDAndRightId(@Param("userID") String userId,@Param("rightId")  String rightId);
    public List<Map<String, Object>> getAllLeafMenus(Map<String, Object> map);
    public void deleteUserRightByUserId(String userId);
    public void deleteUserRightByRoleId(String roleId);
    public void insertUserRight(Map<String, String> map);
    public void insertRoleRight(Map<String, String> map);

}
