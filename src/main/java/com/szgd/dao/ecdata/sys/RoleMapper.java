package com.szgd.dao.ecdata.sys;

import java.util.List;
import java.util.Map;

public interface RoleMapper {


	/* @param loginId
	 * @param rightType MENU/ACTION
	 * @param isEnabled 0/1
	 * @return 根据登录id获取他的权限列表
	 */
	public List<Map<String, Object>> findUserRightListByLoginId(Map<String, Object> params);
	
	public List<Map<String, Object>> findRoleRightListByLoginId(Map<String, Object> params);
	
	public List<Map<String, Object>> getAllRole(Map<String, Object> params);
	
	public List<Map<String, Object>> loadRole(Map<String, Object> params);
	
	public int insertRole(Map<String, Object> params) throws Exception;
	
	public int updateRole(Map<String, Object> params) throws Exception;
	
	public int deleteRole(String params) throws Exception;
	public int deleteRoleRight(String params) throws Exception;
	public int deleteRoleUser(String params) throws Exception;
	public List<Map<String, String>> getRoleTree(Map<String, Object> map);
}
