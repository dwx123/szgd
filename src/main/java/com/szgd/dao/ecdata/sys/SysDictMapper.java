package com.szgd.dao.ecdata.sys;


import com.szgd.bean.SysDict;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
public interface SysDictMapper {

    int insert (@Param("model") SysDict sysDict);

    int update(@Param("model") SysDict sysDict);

    SysDict getById(@Param("dictId") String dictId);

    String getIdByCode(@Param("code") String code);

    List<SysDict> getAllDict(Map<String,Object> params);

    List<SysDict> getByDictIds(@Param("ids") Collection<String> ids);

    List<SysDict> getByName(@Param("name") String name);

    List<SysDict> getByCode(@Param("code") String code);

    SysDict getSysByCode(@Param("code") String code);
    SysDict getSysById(@Param("id") String id);
    List<SysDict> getChildById(@Param("id") String id);
    List<SysDict> getAllChildById(@Param("id") String id);
    List<SysDict> getByParentCode(@Param("code") String code);

    List<SysDict> getAllChildDictByParentCode(@Param("code") String parentCode);
    List<SysDict> getChildDictByParentCodeAndDictValue(Map<String,Object> params);
    
    List<SysDict> getParentByChildCodeOfXhp(@Param("code") String childCode);
    
    List<SysDict> getAllChildCodeOfXhp(@Param("code") String childCode);

}
