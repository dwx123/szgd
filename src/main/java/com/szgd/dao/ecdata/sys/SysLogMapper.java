package com.szgd.dao.ecdata.sys;

import com.szgd.bean.SysLog;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SysLogMapper {

    List<SysLog> getAllLog();

    SysLog getById(String id);

    void deleteById(String id);

    List<SysLog> getByModel(@Param("model") Map<String,Object> mapParams);


}
