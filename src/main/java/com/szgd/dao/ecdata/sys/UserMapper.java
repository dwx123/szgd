package com.szgd.dao.ecdata.sys;

import com.szgd.bean.User;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;


public interface UserMapper {
	public User getUserByLoginId(String loginId);
	public User getUserByUserId(String userId);
	public User getDataByUserId(String userId);
	public User getUserByPersonId(@Param("personId")Integer personId);
	public List<User> getRcOrCfPerson(@Param("roleId")String roleId);
	public List<User> getAllRpPerson();

	public List<Map<String, String>> getUserTree(Map<String, Object> map);
	public List<Map<String, String>> getUserList(Map<String, Object> params);

	public List<Map<String, String>> getUserForCombobox(Map<String, Object> params);

	public void batchInsertUser(List<Map<String, Object>> params);

	public List<Map<String, Object>> getUserParentDept(Map<String, Object> params);

	public List<Map<String, String>> getRoleGrantUsers(Map<String, Object> params);
	
	public int batchUpdateUserDeptName(Map<String, Object> params) throws Exception;
	
	public int batchGrantUserToRole(List<Map<String, Object>> params) throws Exception;
	
	public int delUserAndRoleRelated(Map<String, Object> params) throws Exception;
	
	public int createTempUser(Map<String, Object> params);
	
	public int grantUserToRole(Map<String, Object> params);
	
	/**
	 * @return 获取自增的临时用户ID（外来用户可能没有警号UserID，故给他自动赋值一个，9900001-9999999之间）
	 */
	public int getNextTempUserId();
	
	public List<Map<String, String>> getZongDuiLd(Map<String, Object> params) throws Exception;
	
	public void deleteTempUser(String s);
	public void insertNewUser(String s);
	public void updateDelUser(String s);
	public void updateAllUser(String s);
	public List<Map<String, String>> getQuery(Map<String, Object> params) throws Exception;
	
	public String getUserApproveRoleId_ByMenuRoleId(Map<String, Object> params);
	
	public Map<String,String> setPassword(Map<String, Object> map);
	public Map<String, String> login(Map<String, Object> map);
	
	public void granDefaultMenu();

	public Map<String, String> getUserRole(Map<String, Object> map);
	public String getApproverStrByRoleAndDept(Map<String, String> map);
	public List<Map<String, String>> getApproverListByRoleAndDept(Map<String, Object> params);
	public List<Map<String, String>> getUserListByDept(Map<String, Object> params);
	
	public Integer getNewUserId();
	
	public int insertUser(Map<String, Object> params) throws Exception;
	
	public int updateUser(Map<String, Object> params) throws Exception;
	public int updateUserByPerson(Map<String, Object> params) throws Exception;

	public int delUser(Map<String, Object> params);
	
	public List<Map<String,Object>> getSiteAuth(Map<String, Object> params);
	public List<Map<String,Object>> getSiteGrantUsers(Map<String, Object> params);
	public List<Map<String,Object>> getUserGrantSites(Map<String, Object> params);
	public List<Map<String,Object>> getSiteUserList(Map<String, Object> params);
	public int deleteSiteUser(Map<String, Object> params) throws Exception;
	public int insertSiteUser(Map<String, Object> params) throws Exception;
	public int batchGrantUserToSite(List<Map<String, Object>> params) throws Exception;

	public List<Map<String,Object>> getUsersOfRole(Map<String, Object> params);

	public List<Map<String, Object>> getBidInfoByUserName(Map<String, Object> params);
}
