package com.szgd.dao.ecdata.sys;

import com.szgd.bean.Wbdg;

import java.util.Map;

public interface WbdgMapper {
    /**
     *
     * @mbggenerated 2019-11-12
     */
    int insert(Wbdg record);

    /**
     *
     * @mbggenerated 2019-11-12
     */
    int insertSelective(Wbdg record);

    void truncateWbdg(Map<String,Object> params);
}