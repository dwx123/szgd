package com.szgd.dao.ecdata.sys;

import com.szgd.bean.Wbjc;

import java.util.Map;

public interface WbjcMapper {
    /**
     *
     * @mbggenerated 2019-11-12
     */
    int insert(Wbjc record);

    /**
     *
     * @mbggenerated 2019-11-12
     */
    int insertSelective(Wbjc record);
    
    void truncateWbjc(Map<String,Object> params);
}