package com.szgd.dao.ecdata.sys;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;


public interface WfRoleMapper {

	public List<Map<String,Object>> getAppLeafMenu(Map<String, Object> params);
	public List<Map<String,Object>> getAllWfRoles(Map<String, Object> params);
	public List<Map<String,Object>> getWfRolesByMemo(Map<String, Object> params);
	public List<Map<String,Object>> getWfRolesByDeptCD(Map<String, Object> params);
	public List<Map<String,String>> getUsersOfWfRole(Map<String, Object> params);
	public List<Map<String,Object>> getWfRolesByUserId(Map<String, Object> params);
	public List<Map<String,Object>> getAppMenu(Map<String, Object> params);
	public List<String> getTagByRole(@Param("roleid")String roleid,@Param("tagType")String tagType);
	public List<String> getTagByUser(@Param("userid")Integer userid,@Param("tagType")String tagType);
	public List<String> getUserByYhTag(@Param("tagid")Integer tagid,@Param("tagType")String tagType);
	public List<Map<String,Object>> getUserByYhRoleForTag(@Param("tagType")String tagType);
	
	public List<Map<String,Object>> getWfRoleGrantUsers(Map<String, Object> params);
	
	public List<Map<String,Object>> getNotGrantRightApproveUsers(Map<String, Object> params);
	
	public int insertWfRole(Map<String, Object> params) throws Exception;
	
	public int updateWfRole(Map<String, Object> params) throws Exception;
	
	public int deleteWfRole(String params) throws Exception;
	
	public int deleteWfRoleUser(Map<String, Object> params) throws Exception;
	
	public List<Map<String,Object>> getNoGrantUsers(Map<String, Object> params);
	
	public List<Map<String,Object>> getNoGrantSites(Map<String, Object> params);
	
	public int batchGrantUserToWfRole(List<Map<String, Object>> params) throws Exception;
	
	public int checkUnique(@Param("roleId")String roleId,@Param("userId")String userId);
}
