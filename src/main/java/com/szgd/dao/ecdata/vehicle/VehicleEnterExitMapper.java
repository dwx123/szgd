package com.szgd.dao.ecdata.vehicle;

import java.util.List;
import java.util.Map;

public interface VehicleEnterExitMapper {
    public List<Map<String,Object>> getVehicleEnterAndExitList(Map<String, Object> params);
    public List<Map<String,Object>> getVehicleEnterOrExitList(Map<String, Object> params);
    public List<Map<String,Object>> getTopNVehicleEnterOrExitList(Map<String, Object> params);
    public List<Map<String,Object>> getTopNVehicleEnterAndExitList(Map<String, Object> params);
    public List<Map<String,Object>> getTopNVehicleEnter_ExitList(Map<String, Object> params);
    public List<Map<String,Object>> getVehicleEnter_ExitList(Map<String, Object> params);
    public void updateVehicleEnterExit(Map<String, Object> params);
    public void insertVehicleEnterExit(Map<String, Object> params);
    public int getVehicleEnterOrExitCount(Map<String, Object> params);
    public int getBePresentCount(Map<String, Object> params);

    public List<Map<String,Object>> getOnSiteSuperviseVehicleEnterList(Map<String, Object> params);
}
