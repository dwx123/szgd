package com.szgd.dao.ecdata.vehicle;

import java.util.List;
import java.util.Map;

public interface VehicleMapper {
    public List<Map<String,Object>> getVehicleInfoList(Map<String, Object> params);
    public List<Map<String,Object>> getVehicleTypeList(Map<String, Object> params);
    public List<Map<String,Object>> getVehicleInfo(Map<String, Object> params);
    public void updateVehicleInfo(Map<String, Object> params);
    public void insertVehicleInfo(Map<String, Object> params);
    public void deleteVehicleInfo(Map<String, Object> params);
}
