package com.szgd.dao.ecdata.vehicle;

import java.util.List;
import java.util.Map;

public interface VehicleOnSiteSuperviseMapper {
    public List<Map<String,Object>> getVehicleOnSiteSuperviseList(Map<String, Object> params);
    public void insertVehicleOnSiteSupervise(Map<String, Object> params);
    public void deleteVehicleOnSiteSupervise(Map<String, Object> params);

    public List<Map<String,Object>> app_getSupervisionList(Map<String, Object> params);
    public List<Map<String,Object>> app_getSupervisionCount(Map<String, Object> params);
}
