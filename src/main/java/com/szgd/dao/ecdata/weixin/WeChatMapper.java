package com.szgd.dao.ecdata.weixin;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface WeChatMapper {
    public Map<String,Object> getUserInfo(Map<String,Object> params);

    public List<Map<String,Object>> login(Map<String,Object> params);

    public void updateOpenId(Map<String,Object> params);

    public long getPersonBySiteId(Map<String,Object> params);

    public List<Map<String,Object>> totalCompleteRateAvg(Map<String,Object> params);

    public void insertPmFile(Map<String,Object> params);

    public List<Map<String,Object>> getPmFileList(Map<String, Object> params);
    public List<Map<String,Object>> getPmFileCount(Map<String, Object> params);

    public int chkSendNoticePeopleRemind(Map<String, Object> params);

    public void  updPeopleRemind(Map<String, Object> params);

    public int appGetHiddenRemindCount(Map<String, Object> params);
    public int appGetHoistRemindCount(Map<String, Object> params);
}
