package com.szgd.dao.inf;

import org.springframework.stereotype.Component;

import java.util.Map;

public interface EnrollDataMapper {
    public void insertEnrollData(Map<String,Object> param);
}
