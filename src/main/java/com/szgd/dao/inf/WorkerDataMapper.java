package com.szgd.dao.inf;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

public interface WorkerDataMapper {
    public List<Map<String,Object>> getWorkerList(Map<String,Object> params);
}
