package com.szgd.hikvision;
import com.sun.jna.NativeLong;
import com.sun.jna.ptr.IntByReference;
import com.szgd.hikvision.MonitorCameraInfo;
import com.szgd.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HikvisionUtil {

    //抓拍图片
    public String getDVRPic(MonitorCameraInfo cameraInfo,String basePath) throws IOException {

        String fileName = null;
        //设置通道号，其中1正常，-1不正常
        NativeLong chanLong = new NativeLong(1);
        cameraInfo.setChannel(chanLong);

        //初始化sdk
        HCNetSDK sdk = HCNetSDK.INSTANCE;
        if (!sdk.NET_DVR_Init()) {
            System.out.println("SDK初始化失败");
            return null;
        }

        //注册设备
        HCNetSDK.NET_DVR_DEVICEINFO_V30 devinfo = new HCNetSDK.NET_DVR_DEVICEINFO_V30();
        NativeLong id = sdk.NET_DVR_Login_V30(cameraInfo.getCameraIp(), (short) cameraInfo.getCameraPort(),
                cameraInfo.getUserName(), cameraInfo.getUserPwd(), devinfo);
        cameraInfo.setUserId(id);
        if (cameraInfo.getUserId().intValue() < 0) {
            System.out.println("设备注册失败"+sdk.NET_DVR_GetLastError());
            return null;
        } else {
	        System.out.println("id：" + cameraInfo.getUserId().intValue());
        }

        // 返回Boolean值，判断是否获取设备能力
        HCNetSDK.NET_DVR_WORKSTATE_V30 devwork = new HCNetSDK.NET_DVR_WORKSTATE_V30();
        if (!sdk.NET_DVR_GetDVRWorkState_V30(cameraInfo.getUserId(), devwork)) {
	        System.out.println("返回设备状态失败");
        }

        //JPEG图像信息结构体
        HCNetSDK.NET_DVR_JPEGPARA jpeg = new HCNetSDK.NET_DVR_JPEGPARA();
        jpeg.wPicSize = 2;// 设置图片的分辨率
        jpeg.wPicQuality = 2;// 设置图片质量

        //设置图片大小
        IntByReference a = new IntByReference();

        //创建图片目录
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        Date date = new Date();
        String subPath = sdf.format(date);
        FileUtil.createSubFolder(basePath,subPath);
        //int random = (int)(Math.random()*1000);
        String fileNameString = basePath+ File.separator+subPath+File.separator+
                cameraInfo.getCameraIp().replace(".","-")+".jpg";

        //设置字节缓存
        ByteBuffer jpegBuffer = ByteBuffer.allocate(1024 * 1024);

        //抓图到文件
        boolean is = sdk.NET_DVR_CaptureJPEGPicture(cameraInfo.getUserId(),cameraInfo.getChannel(),jpeg, fileNameString);
        if (is) {
	        System.out.println("抓取成功,返回长度：" + a.getValue());
            fileName = cameraInfo.getCameraIp().replace(".","-")+".jpg";
        } else {
	        System.out.println("抓取失败："+sdk.NET_DVR_GetLastError());
        }

        sdk.NET_DVR_Logout(cameraInfo.getUserId());
        sdk.NET_DVR_Cleanup();
        return fileName;
    }


    public static void main(String[] args) throws IOException {
        HikvisionUtil app = new HikvisionUtil();
        MonitorCameraInfo cameraInfo = new MonitorCameraInfo();//需要新建MonitorCameraInfo类
        cameraInfo.setCameraIp("10.10.10.70");
        cameraInfo.setCameraPort((short)8000);
        cameraInfo.setUserName("admin");
        cameraInfo.setUserPwd("Ecdatainfo.com");
        app.getDVRPic(cameraInfo,"d://szgd-snap-image");
    }


}

