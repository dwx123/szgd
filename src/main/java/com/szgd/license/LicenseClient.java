package com.szgd.license;

import java.io.File;
import java.text.SimpleDateFormat;

import com.szgd.service.personnel.EnterExitService;
import com.szgd.util.crypto.B64Utils;
import com.szgd.util.crypto.RSAUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * 简易的License检查方案，提供4种模型：<br/>
 * 0、单机模式：仅验证license的有效性<br/>
 * 1、时限模式：验证license的有效性，并验证运行时长<br/>
 * 2、联网模式：通过验证服务器验证本地的License是否有效<br/>
 * @author
 * @version 1.0
 *       
 */
@Component("licenseClient")
public class LicenseClient {
	/** 单机模式  **/
	public static final int		MODE_SINGLE		= 0;
	/** 时限模式  **/
	public static final int		MODE_INTIME		= 1;
	/** 联网模式  **/
	public static final int		MODE_ONLINE		= 2;
	
	/** License加载 **/
	public static final int		STATE_NONE			= 0;
	/** License无效或文件错误等 **/
	public static final int		STATE_FAILD			= 1;
	/** License超时 **/
	public static final int		STATE_TIMEOUT		= 2;
	/** License非本机运行 **/
	public static final int		STATE_CODEERR		= 3;
	/** License正常 **/
	public static final int		STATE_SUCCESS		= 9;
	
	/** 验证模式 **/
	public static final int		CONTENT_MODE		= 0;
	/** 机器码 **/
	public static final int		CONTENT_CODE		= 1;
	/** 总可用时长 **/
	public static final int		CONTENT_TIME		= 2;
	
	/** 验证状态 **/
	private static int			license_state		= STATE_NONE;
	/** License内容 **/
	private static byte[]		license_content;

	private static long		expire_time;

	/** 用户公钥 **/
	private static byte[]		license_keys;
	private static Logger log = Logger.getLogger(LicenseClient.class);

	public  int getLicense_state() {
		return license_state;
	}

	public void setLicense_state(int license_state) {
		LicenseClient.license_state = license_state;
	}

	public  long getExpire_time() {
		return expire_time;
	}
	/**
	 * 启用服务10秒后，加载License，并检查有效性，无效时将停止服务。
	 */
	public void scheduledOnStart() {
		log.info( "检查License是否有效..." );
		File file = new File( "" , "application.license" );
		String[] txt = LicenseUtils.s_read_license( file );
		
		license_keys = B64Utils.s_decode( txt[ 0 ] );

		license_content = B64Utils.s_decode( txt[ 1 ] );

		_validateLicense();
		log.info( "License有效，服务器可正常运行。" );
	}
	
	/**
	 * 服务启动后检查License有效性，每24小时检查一次
	 */
	public void scheduledOnRunnig() {
		log.info( "检查License是否有效..." );
		_validateLicense();
		log.info( "License有效，服务器可正常运行。" );
	}
	
	/**
	 * 检查License的内容
	 */
	private void _validateLicense(){
		String[] d_txt = null;
		try {
			d_txt = LicenseUtils.s_decode_string( RSAUtils.s_decrypt_public( license_keys , license_content ) );
		} catch ( Exception e ) {
			license_state = STATE_FAILD;
		}
		int mode = Integer.parseInt( d_txt[CONTENT_MODE] );
		switch( license_state ){
		case STATE_NONE:
			mode = Integer.parseInt( d_txt[CONTENT_MODE] );
			break;
		case STATE_FAILD:
		case STATE_TIMEOUT:
		case STATE_CODEERR:
			log.error( "用户License信息无效，将停止服务器运行。错误代码：" + license_state );
			System.exit( 0 );
			break;
		case STATE_SUCCESS:
			break;
		}
		
		switch( mode ){
		case MODE_SINGLE:
			_validateSingle( d_txt[CONTENT_CODE] );
			break;
		case MODE_INTIME:
			_validateIntime(d_txt[CONTENT_CODE],d_txt[CONTENT_TIME]);
			break;
		case MODE_ONLINE:
			break;
		}
	}
	
	/**
	 * 时长验证
	 */
	private void _validateIntime( String _code ,String  _time){
		long time = Long.parseLong(_time);
		expire_time = time;
		long nowTime = System.currentTimeMillis();

		if( _code.equalsIgnoreCase(LicenseSequences.s_sequence()) && nowTime <=  time){
			license_state = STATE_SUCCESS;
		}else{
			if( !_code.equalsIgnoreCase(LicenseSequences.s_sequence()) ) {
				license_state = STATE_CODEERR;
				log.error("用户License信息无效,非本机运行，将再下次运行时停止服务器运行。");
			}
			if( nowTime >  time ) {
				license_state = STATE_TIMEOUT;
				log.error("用户License信息无效,超过时限，将再下次运行时停止服务器运行。");
			}
		}
	}

	/**
	 * 单机验证
	 */
	private void _validateSingle( String _code ){
		if( _code == LicenseSequences.s_sequence() ){
			license_state = STATE_SUCCESS;
		}else{
			license_state = STATE_CODEERR;
			log.error( "用户License信息无效，将再下次运行时停止服务器运行。" );
		}
	}

	public void validateLicense(File file) {
		log.info( "检查License是否有效..." );
		//File file = new File( "" , "application.license" );
		String[] txt = LicenseUtils.s_read_license( file );

		license_keys = B64Utils.s_decode( txt[ 0 ] );

		license_content = B64Utils.s_decode( txt[ 1 ] );

		_validateLicense();

	}
	public static void main(String[] args) {
		new LicenseClient().scheduledOnStart();
	}
}
