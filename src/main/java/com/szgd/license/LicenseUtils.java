package com.szgd.license;

import com.szgd.util.crypto.B64Utils;
import com.szgd.util.crypto.RSAUtils;

import java.io.*;
import java.security.Key;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;


/**
 * 用于生成应用所需的license文件
 * 
 * @author
 *
 */
public class LicenseUtils {


	/**
	 * 解码密文内容
	 * @param _data
	 * @return
	 */
	public static String[] s_decode_string( byte[] _data ){
		StringTokenizer st = new StringTokenizer( new String( _data ) , ":" );
		
		String[] str = new String[3];
		int idx = 0;
		
		while( st.hasMoreTokens() )
			str[ idx++ ] = st.nextToken();
		
		return str;
	}


	/**
	 * 读取License文件信息
	 * 
	 * @param _file
	 *            License文件
	 * @return [2]-0:Key 1:Txt
	 */
	public static String[] s_read_license( File _file ) {
		try {
			String[] txt = new String[ 2 ];
			FileReader fr = new FileReader( _file );
			BufferedReader br = new BufferedReader( fr );

			txt[ 0 ] = br.readLine();
			txt[ 1 ] = br.readLine();

			br.close();
			fr.close();

			return txt;
		} catch ( Exception e ) {
			System.err.println( "License文件读取失败：" + e.toString() );
		}
		return null;
	}

}
