package com.szgd.service.elseData;

import com.szgd.dao.ecdata.elseData.PitMapper;
import com.szgd.service.personnel.EnterExitService;
import com.szgd.service.sys.SimulateBaseService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PitService extends SimulateBaseService {
    @Autowired
    PitMapper pitMapper;

    private static Logger logger = Logger.getLogger(PitService.class);

    public List<Map<String,Object>> getTopPitList(Map<String, Object> params){
        return pitMapper.getTopPitList(params);
    }

    public List<Map<String,Object>> getPitListForApp(Map<String, Object> params){
        return pitMapper.getPitListForApp(params);
    }

    public int getPitCountForApp(Map<String, Object> params){
        return pitMapper.getPitCountForApp(params);
    }

    public Map<String,Object> getPitInfo(Map<String,Object> params){
        List<Map<String,Object>> list = pitMapper.getTopPitList(params);
        if(list != null && list.size() > 0){
            return list.get(0);
        }else{
            return null;
        }
    }

    public List<Map<String,Object>> getInitPitListForApp(Map<String, Object> params){
        return pitMapper.getInitPitListForApp(params);
    }
}
