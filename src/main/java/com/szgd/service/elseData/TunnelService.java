package com.szgd.service.elseData;

import com.szgd.dao.ecdata.elseData.TunnelMapper;
import com.szgd.service.sys.SimulateBaseService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TunnelService extends SimulateBaseService {
    @Autowired
    TunnelMapper tunnelMapper;

    private static Logger logger = Logger.getLogger(TunnelService.class);

    public List<Map<String,Object>> getTopTunnelList(Map<String, Object> params){
        return tunnelMapper.getTopTunnelList(params);
    }

    public List<Map<String,Object>> getTunnelListForApp(Map<String, Object> params){
        return tunnelMapper.getTunnelListForApp(params);
    }

    public int getTunnelCountForApp(Map<String, Object> params){
        return tunnelMapper.getTunnelCountForApp(params);
    }

    public Map<String,Object> getTunnelInfo(Map<String,Object> params){
        List<Map<String,Object>> list = tunnelMapper.getTopTunnelList(params);
        if(list != null && list.size() > 0){
            return list.get(0);
        }else{
            return null;
        }
    }

    public List<Map<String,Object>> getInitTunnelListForApp(Map<String, Object> params){
        return tunnelMapper.getInitTunnelListForApp(params);
    }
}
