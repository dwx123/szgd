package com.szgd.service.equipment;

import com.szgd.dao.ecdata.equipment.EquipWorkAttachMapper;
import com.szgd.dao.ecdata.sys.WfRoleMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.BusinessName;
import com.szgd.util.FileUtil;
import com.szgd.util.JpushClientUtil;
import com.szgd.util.PropertiesUtil;
import com.szgd.util.StringUtil;
import com.szgd.util.SysUtil;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EquipWorkAttachService extends SimulateBaseService {
    @Autowired
    EquipWorkAttachMapper equipWorkAttachMapper;
    @Autowired
    EquipmentService equipmentService;
    @Autowired
	WfRoleMapper wfRoleMapper;

    public List<Map<String, Object>> getEquipWorkAttachListNoPage(Map<String, Object> params) {
        List<Map<String, Object>>  equipmentAttachList = equipWorkAttachMapper.getEquipWorkAttachList(params);
        return equipmentAttachList;
    }

    public List<Map<String, Object>> getEquipWorkAttachList(Map<String, Object> params) {
        String sortColumn = "ID"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        return getSqlSession().selectList("com.szgd.dao.ecdata.equipment.EquipWorkAttachMapper.getEquipWorkAttachList", params,pageBounds);
    }

    public Map<String, Object> getEquipWorkAttach(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> equipWorkAttachList = equipWorkAttachMapper.getEquipWorkAttachList(paramsMap);
        if (equipWorkAttachList == null || equipWorkAttachList.size() == 0)
            return null;
        return equipWorkAttachList.get(0);
    }

    public void insertEquipWorkAttach(Map<String, Object> paramsMap) {

        equipWorkAttachMapper.insertEquipWorkAttach(paramsMap);
    }

    public void updateEquipWorkAttach(Map<String, Object> paramsMap) {

        equipWorkAttachMapper.updateEquipWorkAttach(paramsMap);
    }

    public void deleteEquipWorkAttach(Map<String, Object> paramsMap){
        String uploadPath = PropertiesUtil.get("uploadPath");

        List<Map<String, Object>> equipWorkAttachList = equipWorkAttachMapper.getEquipWorkAttachList(paramsMap);
        for (int i = 0; i < equipWorkAttachList.size(); i++) {
            Map<String, Object> equipWorkAttachMap = equipWorkAttachList.get(i);
            String fileName = equipWorkAttachMap.get("name")==null?"":equipWorkAttachMap.get("name").toString();
            String  subPath = equipWorkAttachMap.get("path")==null?"":equipWorkAttachMap.get("path").toString();
            if (fileName.length() > 0 && subPath.length() > 0)
            {
                String filePath = FileUtil.existFile(uploadPath,subPath,fileName);
                FileUtil.deleteFile(filePath+"/"+fileName);
            }
        }
        if (equipWorkAttachList.size() > 0) {
            equipWorkAttachMapper.deleteEquipWorkAttach(paramsMap);
        }
        int count = equipWorkAttachMapper.getEquipWorkAttachCount(paramsMap);
        if(count == 0){
            String type = paramsMap.get("type")== null?"":paramsMap.get("type").toString().replace("null","");
            String id = paramsMap.get("equipId")== null?"":paramsMap.get("equipId").toString().replace("null","");
            Map<String, Object> params = new HashMap<>();
            params.put("id",id);
            if(type.equalsIgnoreCase("EQUIP_WORK_ACCEPT")){
                params.put("haveAccept",0);
            }else if(type.equalsIgnoreCase("EQUIP_WORK_HOISTING")){
                params.put("haveHoisting",0);
            }else if(type.equalsIgnoreCase("EQUIP_WORK_TESTING")){
                params.put("haveTesting",0);
            }
            equipmentService.updateEquipment(params);
        }
    }

    public void saveEquipWorkAttach( HashMap<String, Object> paramMap) {
        Logger logger = Logger.getLogger(EquipWorkAttachService.class);
        logger.debug("enter saveEquipWorkAttach");
        String id = paramMap.get("equipId")== null?"":paramMap.get("equipId").toString().replace("null","");
        String isEquipAttachChanged = paramMap.get("isEquipAttachChanged")== null?"":paramMap.get("isEquipAttachChanged").toString().replace("null","");
        String type = paramMap.get("type")== null?"":paramMap.get("type").toString().replace("null","");
        paramMap.put("path",SysUtil.getDateStr(null,"yyyyMM"));
        if (id.length() > 0 && isEquipAttachChanged.equalsIgnoreCase("1")) {
            insertEquipWorkAttach(paramMap);
            Map<String, Object> params = new HashMap<>();
            params.put("id",id);
            if(type.equalsIgnoreCase("EQUIP_WORK_ACCEPT")){
                params.put("haveAccept",1);
            }else if(type.equalsIgnoreCase("EQUIP_WORK_HOISTING")){
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                paramMap.put("createTime",sdf.format(new Date()));
                params.put("haveHoisting",1);
                List<String> tagList = wfRoleMapper.getTagByRole(BusinessName.app_dzltsjs.getValue(), BusinessName.TZLX_DZ.getValue());
                JpushClientUtil.sendToTagList(tagList, "设备名称："+paramMap.get("equipName")+"，上传时间："+paramMap.get("createTime")+"，上传人："+paramMap.get("uploadPersonName")+"，设备编号："+paramMap.get("equipNumber"), BusinessName.TZ_MC_DZL.getValue(), BusinessName.TZ_MC_DZL.getValue(), id == null? "":id);
            }else if(type.equalsIgnoreCase("EQUIP_WORK_TESTING")){
                params.put("haveTesting",1);
            }
            equipmentService.updateEquipment(params);
        }
    }

    public List<Map<String,Object>> getAppEquipWorkAttachList(Map<String, Object> params){
        return equipWorkAttachMapper.getAppEquipWorkAttachList(params);
    }

    public int getAppEquipWorkAttachCount(Map<String, Object> params){
        return equipWorkAttachMapper.getAppEquipWorkAttachCount(params);
    }
}
