package com.szgd.service.equipment;

import com.szgd.dao.ecdata.equipment.EquipWorkMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EquipWorkService extends SimulateBaseService {
    @Autowired
    EquipWorkMapper equipWorkMapper;


    public List<Map<String, Object>> getEquipWorkList(Map<String, Object> params) {
        String sortColumn = "ID"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        return getSqlSession().selectList("com.szgd.dao.ecdata.equipment.EquipWorkMapper.getEquipWorkList", params,pageBounds);
    }

    public Map<String, Object> getEquipWork(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> equipWorkList = equipWorkMapper.getEquipWorkList(paramsMap);
        if (equipWorkList == null || equipWorkList.size() == 0)
            return null;
        return equipWorkList.get(0);
    }

    public void insertEquipWork(Map<String, Object> paramsMap) {

        equipWorkMapper.insertEquipWork(paramsMap);
    }

    public void updateEquipWork(Map<String, Object> paramsMap) {

        equipWorkMapper.updateEquipWork(paramsMap);
    }
    public void deleteEquipWork(Map<String, Object> paramsMap) {
        equipWorkMapper.deleteEquipWork(paramsMap);
    }

}
