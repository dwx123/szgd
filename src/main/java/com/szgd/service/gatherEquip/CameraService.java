package com.szgd.service.gatherEquip;

import com.szgd.dao.ecdata.gatherEquip.CameraInfoMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CameraService extends SimulateBaseService {
    @Autowired
    CameraInfoMapper cameraInfoMapper;


    public List<Map<String, Object>> getCameraInfoListNoPage(Map<String, Object> paramsMap) {
        List<Map<String,Object>> cameraInfoList= cameraInfoMapper.getCameraInfoList(paramsMap);
        return cameraInfoList;
    }

    public List<Map<String, Object>> getCameraList(Map<String, Object> params) {
        String siteId = params.get("siteId")==null?"":params.get("siteId").toString().replace("null","");
        if(siteId.length() == 0){
            params.put("siteId",null);
        }
        String sortColumn = "START_SORT,APP_ORDER"; //默认的排序列
        String sortType = "ASC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  cameraList = getSqlSession().selectList("com.szgd.dao.ecdata.gatherEquip.CameraInfoMapper.getCameraInfoList", params,pageBounds);
        return cameraList;
    }

    public Map<String, Object> getCameraInfo(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> cameraInfoList = cameraInfoMapper.getCameraInfoList(paramsMap);
        if (cameraInfoList == null || cameraInfoList.size() == 0)
            return null;
        return cameraInfoList.get(0);
    }

    public void insertCameraInfo(Map<String, Object> paramsMap) {

        cameraInfoMapper.insertCameraInfo(paramsMap);
    }

    public void updateCameraInfo(Map<String, Object> paramsMap) {

        cameraInfoMapper.updateCameraInfo(paramsMap);
    }
    public void deleteCameraInfo(Map<String, Object> paramsMap) {
        cameraInfoMapper.deleteCameraInfo(paramsMap);
    }

    public List<Map<String,Object>> getCameraListForApp(Map<String, Object> params){
        return cameraInfoMapper.getCameraListForApp(params);
    }
}
