package com.szgd.service.gatherEquip;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.szgd.dao.ecdata.gatherEquip.GatherEquipMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.HttpUtil;
import com.szgd.util.PropertiesUtil;
import com.szgd.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GatherEquipService extends SimulateBaseService {

    @Autowired
    GatherEquipMapper gatherEquipMapper;

    private List<Map<String, Object>>  peopleGatherEquipList = null;//所有人脸识别设备
    private List<Map<String, Object>>  peopleEnterGatherEquipList = null;//所有人脸进入识别设备
    private List<Map<String, Object>>  peopleExitGatherEquipList = null;//所有人脸离开识别设备
    private List<Map<String, Object>>  vehicleEnterGatherEquipList = null;//所有车辆进入识别设备
    private List<Map<String, Object>>  vehicleExitGatherEquipList = null;//所有车辆离开识别设备
    private List<Map<String, Object>>  vehicleGatherEquipList = null;//所有车辆离开识别设备

    private static Logger logger = Logger.getLogger(GatherEquipService.class);

    public List<Map<String, Object>> getPeopleGatherEquipListOfSite(String siteId) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("type","GATHER_EQUIP_FACE");
        paramsMap.put("siteId",siteId);
        List<Map<String, Object>> peopleGatherEquipListOfSite = getGatherEquipListNoPage(paramsMap);
        return peopleGatherEquipListOfSite;
    }

    public List<Map<String, Object>> getPeopleGatherEquipList() {
        if (peopleGatherEquipList == null)
        {
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("type","GATHER_EQUIP_FACE");
            peopleGatherEquipList = getGatherEquipListNoPage(paramsMap);
        }
        return peopleGatherEquipList;
    }

    public List<Map<String, Object>> getPeopleEnterGatherEquipList() {
        if (peopleEnterGatherEquipList == null)
        {
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("type","GATHER_EQUIP_FACE");
            paramsMap.put("passFlag",1);
            peopleEnterGatherEquipList = getGatherEquipListNoPage(paramsMap);
        }
        return peopleEnterGatherEquipList;
    }


    public List<Map<String, Object>> getPeopleExitGatherEquipList() {
        if (peopleExitGatherEquipList == null)
        {
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("type","GATHER_EQUIP_FACE");
            paramsMap.put("passFlag",2);
            peopleExitGatherEquipList = getGatherEquipListNoPage(paramsMap);
        }
        return peopleExitGatherEquipList;
    }


    public List<Map<String, Object>> getVehicleEnterGatherEquipList() {
        if (vehicleEnterGatherEquipList == null)
        {
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("type","GATHER_EQUIP_VEHICLE");
            paramsMap.put("passFlag",1);
            vehicleEnterGatherEquipList = getGatherEquipListNoPage(paramsMap);
        }
        return vehicleEnterGatherEquipList;
    }


    public List<Map<String, Object>> getVehicleExitGatherEquipList() {
        if (vehicleExitGatherEquipList == null)
        {
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("type","GATHER_EQUIP_VEHICLE");
            paramsMap.put("passFlag",2);
            vehicleExitGatherEquipList = getGatherEquipListNoPage(paramsMap);
        }
        return vehicleExitGatherEquipList;
    }

    public List<Map<String, Object>> getVehicleGatherEquipList() {
        if (vehicleGatherEquipList == null)
        {
            Map<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("type","GATHER_EQUIP_VEHICLE");
            vehicleGatherEquipList = getGatherEquipListNoPage(paramsMap);
        }
        return vehicleGatherEquipList;
    }


    public List<Map<String, Object>> getGatherEquipListNoPage(Map<String, Object> paramsMap) {
        List<Map<String,Object>> gatherEquipList = gatherEquipMapper.getGatherEquipList(paramsMap);
        return gatherEquipList;
    }

    public List<Map<String, Object>> getGatherEquipList(Map<String, Object> params) {
        String sortColumn = "ID"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  gatherEquipList = getSqlSession().selectList("com.szgd.dao.ecdata.gatherEquip.GatherEquipMapper.getGatherEquipList", params,pageBounds);
        return gatherEquipList;
    }


    public Map<String, Object> getGatherEquip(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> gatherEquipList = gatherEquipMapper.getGatherEquipList(paramsMap);
        if (gatherEquipList == null || gatherEquipList.size() == 0)
            return null;
        return gatherEquipList.get(0);
    }


    public void insertGatherEquip(Map<String, Object> paramsMap) {

        gatherEquipMapper.insertGatherEquip(paramsMap);
        peopleGatherEquipList = null;//所有人脸识别设备
        peopleEnterGatherEquipList = null;//所有人脸进入识别设备
        peopleExitGatherEquipList = null;//所有人脸离开识别设备
        vehicleEnterGatherEquipList = null;//所有车辆进入识别设备
        vehicleExitGatherEquipList = null;//所有车辆离开识别设备
        vehicleGatherEquipList = null;//所有车辆离开识别设备

    }

    public void updateGatherEquip(Map<String, Object> paramsMap) {

        gatherEquipMapper.updateGatherEquip(paramsMap);
        peopleGatherEquipList = null;//所有人脸识别设备
        peopleEnterGatherEquipList = null;//所有人脸进入识别设备
        peopleExitGatherEquipList = null;//所有人脸离开识别设备
        vehicleEnterGatherEquipList = null;//所有车辆进入识别设备
        vehicleExitGatherEquipList = null;//所有车辆离开识别设备
        vehicleGatherEquipList = null;//所有车辆离开识别设备
    }

    public void deleteGatherEquip(Map<String, Object> paramsMap) {
        gatherEquipMapper.deleteGatherEquip(paramsMap);
    }

    public  String setPassword(String oldPass,String newPass,String ip)
    {

        String url = PropertiesUtil.get("SetPasswordUrl");
        url = url.replace("{ip}",ip);
        //String param = "oldPass="+oldPass+"&newPass="+newPass;
        String contentType = "application/x-www-form-urlencoded";
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("oldPass",oldPass);
        map.put("newPass",newPass);
        String resultStr = HttpUtil.sendPost(url,map,contentType);
        String result = "";
        if (resultStr.length() == 0)
        {
            logger.info("访问人脸识别服务器"+ip+"失败，设备密码设置失败");
            result = StringUtil.splitJoint(result,"访问人脸识别服务器"+ip+"失败，设备密码设置失败");

            return result;
        }
        JsonObject returnData = new JsonParser().parse(resultStr).getAsJsonObject();
        String success = returnData.get("success").getAsString();
        if(success.equalsIgnoreCase("true"))//调用结果成功
        {
            result = returnData.get("data").getAsString();
            result = "";
            logger.info(ip+"设备密码设置成功");

        }else
        {
            String msg = returnData.get("msg").getAsString();
            logger.info(ip +"设备密码设置失败："+msg);
            result = StringUtil.splitJoint(result,ip +"设备密码设置失败："+msg);
        }

        return  result;
    }

    public  String setCallBack(String pass,String callBack,String ip)
    {

        String url = PropertiesUtil.get("SetIdentifyCallBackUrl");
        url = url.replace("{ip}",ip);
        //String param = "pass="+pass+"&callBack="+callBack;
        String contentType = "application/x-www-form-urlencoded";
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("pass",pass);
        map.put("callbackUrl",callBack);
        String resultStr = HttpUtil.sendPost(url,map,contentType);
        String result = "";
        if (resultStr.length() == 0)
        {
            logger.info("访问人脸识别服务器"+ip+"失败，回调接口设置失败");
            result = StringUtil.splitJoint(result,"访问人脸识别服务器"+ip+"失败，回调接口设置失败");
            return result;
        }
        JsonObject returnData = new JsonParser().parse(resultStr).getAsJsonObject();
        String success = returnData.get("success").getAsString();
        if(success.equalsIgnoreCase("true"))//调用结果成功
        {
            result = returnData.get("data").getAsString();
            result = "";
            logger.info(ip+"回调接口设置成功");

        }else
        {
            String msg = returnData.get("msg").getAsString();
            logger.info(ip +"回调接口设置失败："+msg);
            result = StringUtil.splitJoint(result,ip +"回调接口设置失败："+msg);
        }

        return  result;
    }
}
