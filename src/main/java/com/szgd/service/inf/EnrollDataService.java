package com.szgd.service.inf;

import com.szgd.dao.inf.EnrollDataMapper;
import com.szgd.service.personnel.EnterExitService;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.FileUtil;
import com.szgd.util.HttpUtil;
import com.szgd.util.PropertiesUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

@Service
public class EnrollDataService extends SimulateBaseService {
    @Autowired
    EnrollDataMapper enrollDataMapper;
    @Autowired
    EnterExitService enterExitService;

    private static Logger logger = Logger.getLogger(EnrollDataService.class);


    public void  insertEnrollData(Map<String,Object> params){
        enterExitService.updateDataStatus(params);

        List<Map<String,Object>> list =  (List<Map<String,Object>>)params.get("list");
        for (int i = 0; i < list.size(); i++) {
            Map<String,Object> tempMap = list.get(i);
            String path = tempMap.get("path")==null?"":tempMap.get("path").toString();
            String enrollId = tempMap.get("enrollId")==null?"":tempMap.get("enrollId").toString();
            if (enrollId.length() == 0)
                continue;
            byte[] b = HttpUtil.downLoadJpgFromOtherSystem(path);
            /*String fileName = tempMap.get("fileName")==null?"":tempMap.get("fileName").toString();
            byte[] b = new byte[0];
            try {
                b = FileUtil.getFileByte(PropertiesUtil.get("snapImagePath"),path,fileName);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }*/
            tempMap.put("img",b);
            tempMap.put("enrollTableName",params.get("enrollTableName"));
            enrollDataMapper.insertEnrollData(tempMap);
        }

    }
}
