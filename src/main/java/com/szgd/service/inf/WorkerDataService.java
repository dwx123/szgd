package com.szgd.service.inf;

import com.szgd.dao.inf.WorkerDataMapper;
import com.szgd.service.sys.SimulateBaseService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WorkerDataService extends SimulateBaseService {
    @Autowired
    WorkerDataMapper workerDataMapper;

    private static Logger logger = Logger.getLogger(WorkerDataService.class);

    public List<Map<String, Object>> getWorkerList(Map<String,Object> params) {
        return workerDataMapper.getWorkerList(params);
    }
}
