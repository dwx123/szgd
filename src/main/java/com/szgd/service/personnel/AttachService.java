package com.szgd.service.personnel;

import com.szgd.dao.ecdata.personnel.AttachMapper;
import com.szgd.dao.ecdata.personnel.AttachMapper;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.FileUtil;
import com.szgd.util.PropertiesUtil;
import com.szgd.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AttachService extends SimulateBaseService {
    @Autowired
    AttachMapper attachMapper;


    public List<Map<String, Object>> getAttachList(Map<String, Object> paramsMap) {
        List<Map<String,Object>> attachList= attachMapper.getAttachList(paramsMap);
        return attachList;
    }

    public Map<String, Object> getAttach(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> attachList = attachMapper.getAttachList(paramsMap);
        if (attachList == null || attachList.size() == 0)
            return null;
        return attachList.get(0);
    }

    public Map<String, Object> getAttach(String parentId,String source) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("parentId",parentId);
        paramsMap.put("source",source);
        List<Map<String,Object>> attachList = attachMapper.getAttachList(paramsMap);
        if (attachList == null || attachList.size() == 0)
            return null;
        return attachList.get(0);
    }

    public void insertAttach(Map<String, Object> paramsMap) {

        attachMapper.insertAttach(paramsMap);
    }

    public void updateAttach(Map<String, Object> paramsMap) {

        attachMapper.updateAttach(paramsMap);
    }
    public void deleteAttach(Map<String, Object> paramsMap){
        String uploadPath = PropertiesUtil.get("uploadPath");
/*        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        paramsMap.put("parentId",parentId);
        paramsMap.put("parentId",parentId);*/
        List<Map<String, Object>> attachList = attachMapper.getAttachList(paramsMap);
        for (int i = 0; i < attachList.size(); i++) {
            Map<String, Object> attachMap = attachList.get(i);
            String fileName = attachMap.get("name")==null?"":attachMap.get("name").toString();
            String  subPath = attachMap.get("path")==null?"":attachMap.get("path").toString();
            if (fileName.length() > 0 && subPath.length() > 0)
            {
                String filePath = FileUtil.existFile(uploadPath,subPath,fileName);
                FileUtil.deleteFile(filePath+"/"+fileName);
            }
        }
        if (attachList.size() > 0)
            attachMapper.deleteAttach(paramsMap);
    }

    public Map<String, Object>  saveAttach( Map<String, Object> paramMap) {
        String uploadPath = PropertiesUtil.get("uploadPath");
        String fullPath = "";
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String parentId = paramMap.get("parentId")== null?"":paramMap.get("parentId").toString().replace("null","");
        paramMap.put("path",SysUtil.getDateStr(null,"yyyyMM"));
        if (id.length() == 0)//新增
        {
            insertAttach(paramMap);

        } else {
            deleteAttach(paramMap);
            insertAttach(paramMap);
        }
        fullPath = uploadPath + File.separator + paramMap.get("path")+ File.separator +  paramMap.get("name");
        paramMap.put("fullPath",fullPath);
        return paramMap;
    }
}
