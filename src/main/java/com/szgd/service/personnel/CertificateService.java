package com.szgd.service.personnel;

import com.szgd.dao.ecdata.personnel.CertificateMapper;
import com.szgd.dao.ecdata.personnel.PersonnelInfoMapper;
import com.szgd.service.sys.SimulateBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CertificateService extends SimulateBaseService {

    @Autowired
    CertificateMapper certificateMapper;


    public List<Map<String, Object>> getCertificateList(Map<String, Object> paramsMap) {
        List<Map<String,Object>> certificateList = certificateMapper.getCertificateList(paramsMap);
        return certificateList;
    }

    public Map<String, Object> getCertificate(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> certificateList = certificateMapper.getCertificateList(paramsMap);
        if (certificateList == null || certificateList.size() == 0)
            return null;
        return certificateList.get(0);
    }

    public Map<String, Object> getCertificateParentId(String personnelId) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("personnelId",personnelId);
        List<Map<String,Object>> personnelInfoList = certificateMapper.getCertificateList(paramsMap);
        if (personnelInfoList == null || personnelInfoList.size() == 0)
            return null;
        return personnelInfoList.get(0);
    }

    public void insertCertificate(Map<String, Object> paramsMap) {

        certificateMapper.insertCertificate(paramsMap);
    }

    public void updateCertificate(Map<String, Object> paramsMap) {

        certificateMapper.updateCertificate(paramsMap);
    }

    public void deleteCertificate(Map<String, Object> paramsMap) {
        certificateMapper.deleteCertificate(paramsMap);
    }
}
