package com.szgd.service.personnel;

import com.szgd.dao.ecdata.personnel.PersonnelBidMapper;
import com.szgd.dao.ecdata.personnel.PersonnelFaceIdMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PersonnelBidService extends SimulateBaseService {

    @Autowired
    PersonnelBidMapper personnelBidMapper;


    public List<Map<String, Object>> getPersonnelBidListNoPage(Map<String, Object> paramsMap) {
        List<Map<String,Object>> bidList = personnelBidMapper.getPersonnelBidList(paramsMap);
        return bidList;
    }

    public Map<String, Object> getPersonnelBid(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> bidList = personnelBidMapper.getPersonnelBidList(paramsMap);
        if (bidList == null || bidList.size() == 0)
            return null;
        return bidList.get(0);
    }


    public void insertPersonnelBid(Map<String, Object> paramsMap) {

        personnelBidMapper.insertPersonnelBid(paramsMap);
    }

    public void insertPersonnelBidEX(String personnelId,String creator,String bidId) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("personnelId",personnelId);
        paramsMap.put("creator",creator);
        paramsMap.put("bidId",bidId);
        paramsMap.put("delflag",0);

        personnelBidMapper.insertPersonnelBid(paramsMap);
    }

    public void updatePersonnelBid(Map<String, Object> paramsMap) {

        personnelBidMapper.updatePersonnelBid(paramsMap);
    }

    public void deletePersonnelBidEX(String personnelId) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("personnelId",personnelId);
        personnelBidMapper.deletePersonnelBid(paramsMap);
    }

    public void deletePersonnelBid(Map<String, Object> paramsMap) {
        personnelBidMapper.deletePersonnelBid(paramsMap);
    }

    public void deletePersonnelBidById(Map<String, Object> paramsMap) {
        String id = paramsMap.get("id")==null?null:paramsMap.get("id").toString();
        if (id == null || id.length() == 0)
        {
            List<Map<String,Object>> personnelBidList = personnelBidMapper.getPersonnelBidList(paramsMap);
            if (personnelBidList != null  && personnelBidList.size() > 0)
            {
                id = personnelBidList.get(0).get("id").toString();
                paramsMap.put("id",id);

            }else
                return;
        }
        personnelBidMapper.deletePersonnelBidById(paramsMap);
    }


}
