package com.szgd.service.personnel;

import com.szgd.dao.ecdata.personnel.PersonnelFaceIdMapper;
import com.szgd.dao.ecdata.personnel.PersonnelFaceIdMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PersonnelFaceIdService extends SimulateBaseService {

    @Autowired
    PersonnelFaceIdMapper personnelFaceIdMapper;


    public int getCount(Map<String, Object> paramsMap) {
        return personnelFaceIdMapper.getCount(paramsMap);
    }

    public List<Map<String, Object>> getPersonnelFaceIdListNoPage(Map<String, Object> paramsMap) {
        List<Map<String,Object>> personnelFaceIdList = personnelFaceIdMapper.getPersonnelFaceIdList(paramsMap);
        return personnelFaceIdList;
    }

    public List<Map<String, Object>> getPersonnelFaceIdListByPersonnelIdAndSiteId(String personnelId,String siteId) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("personnelId",personnelId);
        paramsMap.put("siteId",siteId);
        List<Map<String,Object>> personnelFaceIdList = personnelFaceIdMapper.getPersonnelFaceIdList(paramsMap);
        return personnelFaceIdList;
    }

    public int getMaxPersonId(String siteId) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("siteId",siteId);
        List<Map<String,Object>> list = personnelFaceIdMapper.getMaxPersonId(paramsMap);
        if (list == null || list.size() == 0)
            return 0;
        int personId =  list.get(0).get("personId")==null?0:Integer.parseInt(list.get(0).get("personId").toString());
        return personId;
    }

    public List<Map<String, Object>> getPersonnelFaceIdList(Map<String, Object> params) {
        String sortColumn = "ID"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  personnelFaceIdList = getSqlSession().selectList("com.szgd.dao.ecdata.personnel.PersonnelFaceIdMapper.getPersonnelFaceIdList", params,pageBounds);
        return personnelFaceIdList;
    }

    public Map<String, Object> getPersonnelFaceId(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> personnelFaceIdList = personnelFaceIdMapper.getPersonnelFaceIdList(paramsMap);
        if (personnelFaceIdList == null || personnelFaceIdList.size() == 0)
            return null;
        return personnelFaceIdList.get(0);
    }


    public void insertPersonnelFaceId(Map<String, Object> paramsMap) {

        personnelFaceIdMapper.insertPersonnelFaceId(paramsMap);
    }

    public void insertPersonnelFaceIdEx(String personnelId,String personId,String faceId,String creator,String siteId) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("personnelId",personnelId);
        paramsMap.put("personId",personId);
        paramsMap.put("faceId",faceId);
        paramsMap.put("creator",creator);
        paramsMap.put("siteId",siteId);
        personnelFaceIdMapper.insertPersonnelFaceId(paramsMap);
    }

    public void updatePersonnelFaceId(Map<String, Object> paramsMap) {

        personnelFaceIdMapper.updatePersonnelFaceId(paramsMap);
    }

    public void deletePersonnelFaceId(Map<String, Object> paramsMap) {

        personnelFaceIdMapper.deletePersonnelFaceId(paramsMap);
    }

    public void deletePersonnelFaceIdById(Map<String, Object> paramsMap) {
        /*Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("personnelId",personnelId);*/
        personnelFaceIdMapper.deletePersonnelFaceIdById(paramsMap);
    }

    public void updatePersonId(Map<String, Object> paramsMap) {
        personnelFaceIdMapper.updatePersonId(paramsMap);
    }

    public void updateFaceId(Map<String, Object> paramsMap) {
        personnelFaceIdMapper.updateFaceId(paramsMap);
    }

    public void updatePersonnelFaceIdNull(Map<String, Object> paramsMap) {

        personnelFaceIdMapper.updatePersonnelFaceIdNull(paramsMap);
    }


}
