package com.szgd.service.personnel;

import com.szgd.dao.ecdata.personnel.PersonnelGatherEquipMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PersonnelGatherEquipService extends SimulateBaseService {

    @Autowired
    PersonnelGatherEquipMapper personnelGatherEquipMapper;


    public int getCount(Map<String, Object> paramsMap) {
        return personnelGatherEquipMapper.getCount(paramsMap);
    }

    public List<Map<String, Object>> getPersonnelGatherEquipListNoPage(Map<String, Object> paramsMap) {
        List<Map<String,Object>> personnelGatherEquipList = personnelGatherEquipMapper.getPersonnelGatherEquipList(paramsMap);
        return personnelGatherEquipList;
    }
    public List<Map<String, Object>> getPersonnelGatherEquipList(Map<String, Object> params) {
        String sortColumn = "ID"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  personnelGatherEquipList = getSqlSession().selectList("com.szgd.dao.ecdata.personnel.PersonnelGatherEquipMapper.getPersonnelGatherEquipList", params,pageBounds);
        return personnelGatherEquipList;
    }

    public Map<String, Object> getPersonnelGatherEquip(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> personnelGatherEquipList = personnelGatherEquipMapper.getPersonnelGatherEquipList(paramsMap);
        if (personnelGatherEquipList == null || personnelGatherEquipList.size() == 0)
            return null;
        return personnelGatherEquipList.get(0);
    }


    public void insertPersonnelGatherEquip(Map<String, Object> paramsMap) {

        personnelGatherEquipMapper.insertPersonnelGatherEquip(paramsMap);
    }

    public void insertPersonnelGatherEquipEx(String personnelId,String gatherEquipId,String flag,String creator,String siteId) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("personnelId",personnelId);
        paramsMap.put("gatherEquipId",gatherEquipId);
        paramsMap.put("flag",flag);
        paramsMap.put("creator",creator);
        paramsMap.put("siteId",siteId);
        personnelGatherEquipMapper.insertPersonnelGatherEquip(paramsMap);
    }

    public void updatePersonnelGatherEquip(Map<String, Object> paramsMap) {

        personnelGatherEquipMapper.updatePersonnelGatherEquip(paramsMap);
    }

    public void deletePersonnelGatherEquip(Map<String, Object> paramsMap) {

        personnelGatherEquipMapper.deletePersonnelGatherEquip(paramsMap);
    }

    public void deletePersonnelGatherEquipById(Map<String, Object> paramsMap) {
        /*Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("personnelId",personnelId);*/
        personnelGatherEquipMapper.deletePersonnelGatherEquipById(paramsMap);
    }

    public void deletePersonnelGatherEquipByIds(String personnelId,String gatherEquipId,String siteId) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("personnelId",personnelId);
        paramsMap.put("gatherEquipId",gatherEquipId);
        paramsMap.put("siteId",siteId);
        personnelGatherEquipMapper.deletePersonnelGatherEquip(paramsMap);
    }

}
