package com.szgd.service.personnel;

import com.szgd.dao.ecdata.personnel.PersonnelSiteMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PersonnelSiteService extends SimulateBaseService {

    @Autowired
    PersonnelSiteMapper personnelSiteMapper;


    public List<Map<String, Object>> getPersonnelSiteListNoPage(Map<String, Object> paramsMap) {
        List<Map<String,Object>> siteList = personnelSiteMapper.getPersonnelSiteList(paramsMap);
        return siteList;
    }
    public List<Map<String, Object>> getPersonnelSiteList(Map<String, Object> params) {
        String sortColumn = "ID"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  siteList = getSqlSession().selectList("com.szgd.dao.ecdata.personnel.PersonnelSiteMapper.getPersonnelSiteList", params,pageBounds);
        return siteList;
    }

    public Map<String, Object> getPersonnelSite(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> siteList = personnelSiteMapper.getPersonnelSiteList(paramsMap);
        if (siteList == null || siteList.size() == 0)
            return null;
        return siteList.get(0);
    }


    public void insertPersonnelSite(Map<String, Object> paramsMap) {

        personnelSiteMapper.insertPersonnelSite(paramsMap);
    }

    public void insertPersonnelSiteEX(String personnelId,String creator,String siteId) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("personnelId",personnelId);
        paramsMap.put("creator",creator);
        paramsMap.put("siteId",siteId);
        List<Map<String,Object>> personnelSiteList = personnelSiteMapper.getPersonnelSiteList(paramsMap);
        if (personnelSiteList == null  || personnelSiteList.size() == 0)
            personnelSiteMapper.insertPersonnelSite(paramsMap);
    }

    public void updatePersonnelSite(Map<String, Object> paramsMap) {

        personnelSiteMapper.updatePersonnelSite(paramsMap);
    }

    public void deletePersonnelSite(Map<String, Object> paramsMap) {
        personnelSiteMapper.deletePersonnelSite(paramsMap);
    }

    public void deletePersonnelSiteById(Map<String, Object> paramsMap) {
        String id = paramsMap.get("id")==null?null:paramsMap.get("id").toString();
        if (id == null || id.length() == 0)
        {
            List<Map<String,Object>> personnelSiteList = personnelSiteMapper.getPersonnelSiteList(paramsMap);
            if (personnelSiteList != null  && personnelSiteList.size() > 0)
            {
                id = personnelSiteList.get(0).get("id").toString();
                paramsMap.put("id",id);

            }else
                return;
        }
        personnelSiteMapper.deletePersonnelSiteById(paramsMap);
    }

}
