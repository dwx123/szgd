package com.szgd.service.personnel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.szgd.bean.RemindCfg;
import com.szgd.bean.RemindCfgTitle;
import com.szgd.dao.ecdata.personnel.RemindCfgMapper;
import com.szgd.service.sys.SimulateBaseService;

@Service
public class RemindCfgService extends SimulateBaseService {

    @Autowired
    RemindCfgMapper remindCfgMapper;

    public int deleteByPrimaryKey(Long id){
    	return remindCfgMapper.deleteByPrimaryKey(id);
    }

    public int insert(RemindCfg record){
    	return remindCfgMapper.insert(record);
    }


    public RemindCfg selectByPrimaryKey(Long id){
    	return remindCfgMapper.selectByPrimaryKey(id);
    }


    public int updateByPrimaryKey(RemindCfg record){
    	return remindCfgMapper.updateByPrimaryKey(record);
    }
    
    public List<RemindCfg> getRemindCfgData(Map<String, Object> params){
    	return remindCfgMapper.getRemindCfgData(params);
    }
    
    public List<RemindCfgTitle> getRemindCfgTitle(Map<String, Object> params){
    	return remindCfgMapper.getRemindCfgTitle(params);
    }
    
    public List<Map<String, Object>> getWorkSiteInfo(Map<String, Object> params){
		return remindCfgMapper.getWorkSiteInfo(params);
    	
    }
    
    public void saveTxpz(RemindCfg rc,HttpServletRequest request,HttpSession session){
    	if(rc != null){
    		if(rc.getId() != null){
    			remindCfgMapper.updateByPrimaryKey(rc);
			}else{
				remindCfgMapper.insert(rc);
			}
    	}
    }

}
