package com.szgd.service.personnel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.szgd.util.BusinessName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.szgd.bean.Remind;
import com.szgd.dao.ecdata.personnel.RemindMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;

@Service
public class RemindService extends SimulateBaseService {

    @Autowired
    RemindMapper remindMapper;

    public int deleteByPrimaryKey(Long id){
    	return remindMapper.deleteByPrimaryKey(id);
    }

    public int insert(Remind record){
    	return remindMapper.insert(record);
    }

    public Remind selectByPrimaryKey(Long id){
    	return remindMapper.selectByPrimaryKey(id);
    }


    public int updateByPrimaryKey(Remind record){
    	return remindMapper.updateByPrimaryKey(record);
    }
    
    public List<Remind> getRemindData(Map<String, Object> params) {
        String siteId = params.get("siteId")==null?"":params.get("siteId").toString().replace("null","");
        String beginDate = params.get("beginDate")==null?"":params.get("beginDate").toString().replace("null","");
        String endDate = params.get("endDate")==null?"":params.get("endDate").toString().replace("null","");
        if(siteId.length() == 0){
            params.put("siteId",null);
        }
        if(beginDate.length() > 0){
            params.put("beginDate",beginDate+" 00:00:01");
        }
        if(endDate.length() > 0){
            params.put("endDate",endDate+" 23:59:59");
        }
		String sortColumn = "REMIND_TIME"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
		String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
		PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
		return getSqlSession().selectList("com.szgd.dao.ecdata.personnel.RemindMapper.getRemindData", params,pageBounds);
	}

    public List<Remind> getRemindDataNoPage(Map<String, Object> params) {
        String siteId = params.get("siteId")==null?"":params.get("siteId").toString().replace("null","");
        String beginDate = params.get("beginDate")==null?"":params.get("beginDate").toString().replace("null","");
        String endDate = params.get("endDate")==null?"":params.get("endDate").toString().replace("null","");
        if(siteId.length() == 0){
            params.put("siteId",null);
        }
        if(beginDate.length() > 0){
            params.put("beginDate",beginDate+" 00:00:01");
        }
        if(endDate.length() > 0){
            params.put("endDate",endDate+" 23:59:59");
        }
        return remindMapper.getRemindData(params);
    }

    public List<Remind> getCertificationRemindInfo(Map<String, Object> params){
    	return remindMapper.getCertificationRemindInfo(params);
    }
    
    public List<Remind> getPeoplesRemindInfo(Map<String, Object> params){
    	return remindMapper.getPeoplesRemindInfo(params);
    }

    public List<Remind> getPeoplesMonthRemindInfo(Map<String, Object> params){
        return remindMapper.getPeoplesMonthRemindInfo(params);
    }

    public List<Remind> getVehicleOnSiteSuperviseRemindInfo(Map<String, Object> params){
        return remindMapper.getVehicleOnSiteSuperviseRemindInfo(params);
    }


    public List<Remind> getExaminationRemindInfo(Map<String, Object> params){
    	return remindMapper.getExaminationRemindInfo(params);
    }
    
    public List<Remind> getEquipmentChkRemindInfo(Map<String, Object> params){
    	return remindMapper.getEquipmentChkRemindInfo(params);
    }
    
    public List<Remind> getEquipmentInsureRemindInfo(Map<String, Object> params){
    	return remindMapper.getEquipmentInsureRemindInfo(params);
    }
    
    public int getRemindDataCount(Remind record){
    	return remindMapper.getRemindDataCnt(record);
    }

    public List<Remind> getRemindList(Remind record){
        return remindMapper.getRemindList(record);
    }

    public int updateRemindContent(Remind record)
    {
        return  remindMapper.updateRemindContent(record);
    }
    public int updateRemindTimer(Remind record){
    	return remindMapper.updateRemindTimer(record);
    }
    public int updateRemindDelFlag(Map<String, Object> params){
    	return remindMapper.updateRemindDelFlag(params);
    }

    public int updateRemindIsOk(Map<String, Object> params){
        return remindMapper.updateRemindIsOk(params);
    }


    public int deleteRemindByRemindType(String remindType){
        Map<String, String> params  = new HashMap<>();
        params.put("remindType", remindType);
        return remindMapper.deleteRemind(params);
    }

    public List<String> getAppRemindContent(Map<String, Object> params) {
        List<String> result = new ArrayList<>();
        List<Map<String,Object>> list = remindMapper.getAppRemindContent(params);
        if (list != null && list.size() > 0) {
            String siteId = list.get(0).get("siteId").toString();
            String marquee = null;
            for (int i = 0; i < list.size(); i++) {
                Map<String, Object> map = list.get(i);
                String siteIdCur = map.get("siteId").toString();
                String content = map.get("content")==null?"":map.get("content").toString();
                if(siteIdCur.equalsIgnoreCase(siteId)){
                    if (marquee == null) {
                        marquee = content;
                    } else {
                        marquee = marquee + "\u3000\u3000\u3000\u3000\u3000\u3000" + content;
                    }
                }else{
                    result.add(marquee);
                    marquee = null;
                    siteId = siteIdCur;
                    marquee = content;
                }
            }
            result.add(marquee);
        }
        return result;
    }

    public Map<String, Object> app_getSupervisionRemind(Map<String, Object> params) {
        params.put("isApp","1");
        params.put("remindType","VEHICLE_SUPERVISE_REMIND");
        String siteId = params.get("siteId")==null?"":params.get("siteId").toString().replace("null","");
        String beginDate = params.get("beginDate")==null?"":params.get("beginDate").toString().replace("null","");
        String endDate = params.get("endDate")==null?"":params.get("endDate").toString().replace("null","");
        if(siteId.length() == 0 || siteId.equalsIgnoreCase("all")){
            params.put("siteId",null);
        }
        if(beginDate.length() > 0){
            params.put("beginDate",beginDate+" 00:00:01");
        }
        if(endDate.length() > 0){
            params.put("endDate",endDate+" 23:59:59");
        }
        List<Remind> list = remindMapper.app_getSupervisionRemindList(params);
        List<Remind> count = remindMapper.getRemindData(params);
        Map<String, Object> res = new HashMap<>();
        res.put("list",list);
        res.put("count",count.size());
        return res;
    }
}
