package com.szgd.service.personnel;

import com.szgd.dao.ecdata.personnel.CertificateMapper;
import com.szgd.dao.ecdata.personnel.ViolationMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ViolationService extends SimulateBaseService {

    @Autowired
    ViolationMapper violationMapper;


    public List<Map<String, Object>> getViolationListNoPage(Map<String, Object> paramsMap) {
        List<Map<String,Object>> violationList = violationMapper.getViolationList(paramsMap);
        return violationList;
    }
    public List<Map<String, Object>> getViolationList(Map<String, Object> params) {
        String sortColumn = "OCCUR_TIME_S"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  violationList = getSqlSession().selectList("com.szgd.dao.ecdata.personnel.ViolationMapper.getViolationList", params,pageBounds);
        return violationList;
    }
    public Map<String, Object> getViolation(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> violationList = violationMapper.getViolationList(paramsMap);
        if (violationList == null || violationList.size() == 0)
            return null;
        return violationList.get(0);
    }


    public void insertViolation(Map<String, Object> paramsMap) {

        violationMapper.insertViolation(paramsMap);
    }

    public void updateViolation(Map<String, Object> paramsMap) {

        violationMapper.updateViolation(paramsMap);
    }

    public void deleteViolation(Map<String, Object> paramsMap) {

        violationMapper.deleteViolation(paramsMap);
    }
}
