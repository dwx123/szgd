package com.szgd.service.project;

import com.szgd.dao.ecdata.project.BidMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.*;

@Service
public class BidService extends SimulateBaseService {

    @Autowired
    BidMapper bidMapper;
    private List<Map<String,Object>> bidList = null;
    public void setAllBid(HttpSession httpSession) {
        if (bidList == null)
            bidList = bidMapper.getBidList(null);
        List<Map<String,Object>> bidList = bidMapper.getBidList(null);
        if (bidList == null || bidList.size() == 0)
            return;
        httpSession.setAttribute("bidList", bidList);
    }
    public List<Map<String, Object>> getBidListNoPage(Map<String, Object> paramsMap) {
        bidList = bidMapper.getBidList(paramsMap);
        return bidList;
    }

    public List<Map<String, Object>> getBidList(Map<String, Object> params) {
        String sortColumn = "SORT"; //默认的排序列
        String sortType = "ASC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  bidList = getSqlSession().selectList("com.szgd.dao.ecdata.project.BidMapper.getBidList", params,pageBounds);
        return bidList;
    }

    public Map<String, Object> getBid(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> bidList = bidMapper.getBidList(paramsMap);
        if (bidList == null || bidList.size() == 0)
            return null;
        return bidList.get(0);
    }

    public Map<String, Object> getBidByName(String name) {
        if (bidList == null)
            bidList = bidMapper.getBidList(null);
        for (int i = 0; i < bidList.size(); i++) {
            Map<String, Object> bidMap = bidList.get(i);
            String tempName = bidMap.get("name").toString();
            if (tempName.equalsIgnoreCase(name))
            {
                return bidMap;
            }
        }
        return null;
    }

    public void insertBid(Map<String, Object> paramsMap) {
        bidMapper.insertBid(paramsMap);
        bidList = bidMapper.getBidList(null);
    }

    public void updateBid(Map<String, Object> paramsMap) {

        bidMapper.updateBid(paramsMap);
        bidList = bidMapper.getBidList(null);
    }


    public void deleteBid(Map<String, Object> paramsMap) {
        bidMapper.deleteBid(paramsMap);
    }

    //判断是否为数字
    public static boolean isNumeric(String str){
        for (int i = str.length();--i>=0;){
            if (!Character.isDigit(str.charAt(i))){
                return false;
            }
        }
        return true;
    }
}
