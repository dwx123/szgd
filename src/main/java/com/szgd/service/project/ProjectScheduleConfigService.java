package com.szgd.service.project;

import com.szgd.dao.ecdata.project.ProjectScheduleConfigMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProjectScheduleConfigService extends SimulateBaseService {
    @Autowired
    ProjectScheduleConfigMapper projectScheduleConfigMapper;


    public List<Map<String, Object>> getProjectScheduleConfigListNoPage(Map<String, Object> paramsMap) {
        List<Map<String,Object>> projectScheduleConfigList = projectScheduleConfigMapper.getProjectScheduleConfigList(paramsMap);
        return projectScheduleConfigList;
    }

    public List<Map<String, Object>> getProjectScheduleConfigList(Map<String, Object> params) {
        String sortColumn = "ID"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  projectList = getSqlSession().selectList("com.szgd.dao.ecdata.project.ProjectScheduleConfigMapper.getProjectScheduleConfigList", params,pageBounds);
        return projectList;
    }

    public Map<String, Object> getProjectScheduleConfig(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> projectScheduleConfigList = projectScheduleConfigMapper.getProjectScheduleConfigList(paramsMap);
        if (projectScheduleConfigList == null || projectScheduleConfigList.size() == 0)
            return null;
        return projectScheduleConfigList.get(0);
    }


    public void insertProjectScheduleConfig(Map<String, Object> paramsMap) {

        projectScheduleConfigMapper.insertProjectScheduleConfig(paramsMap);
    }

    public void updateProjectScheduleConfig(Map<String, Object> paramsMap) {

        projectScheduleConfigMapper.updateProjectScheduleConfig(paramsMap);
    }

    public void deleteProjectScheduleConfig(Map<String, Object> paramsMap) {

        projectScheduleConfigMapper.deleteProjectScheduleConfig(paramsMap);
    }
}
