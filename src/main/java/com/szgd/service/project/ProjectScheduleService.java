package com.szgd.service.project;

import com.szgd.bean.Schedule;
import com.szgd.dao.ecdata.project.ProjectScheduleMapper;
import com.szgd.service.sys.SimulateBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProjectScheduleService extends SimulateBaseService {

    @Autowired
    ProjectScheduleMapper projectScheduleMapper;


    public List<Map<String, Object>> getProjectScheduleListNoPage(Map<String, Object> paramsMap) {
        List<Map<String,Object>> projectScheduleList = projectScheduleMapper.getProjectScheduleList(paramsMap);
        return projectScheduleList;
    }

    public Map<String, Object> getProjectSchedule(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> projectScheduleList = projectScheduleMapper.getProjectScheduleList(paramsMap);
        if (projectScheduleList == null || projectScheduleList.size() == 0)
            return null;
        return projectScheduleList.get(0);
    }


    public void insertProjectSchedule(Schedule schedule) {

        projectScheduleMapper.insertProjectSchedule(schedule);
    }

    public void updateProjectSchedule(Schedule schedule) {

        projectScheduleMapper.updateProjectSchedule(schedule);
    }

    public void saveSchedule(Schedule schedule, HttpServletRequest request, HttpSession session){
        if(schedule != null){
            if(schedule.getId() != null){
                projectScheduleMapper.updateProjectSchedule(schedule);
            }else{
                projectScheduleMapper.insertProjectSchedule(schedule);
            }
        }
    }
}
