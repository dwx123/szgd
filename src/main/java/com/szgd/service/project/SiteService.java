package com.szgd.service.project;

import com.szgd.dao.ecdata.project.SiteMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;
import com.szgd.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SiteService extends SimulateBaseService {

    @Autowired
    SiteMapper siteMapper;
    private List<Map<String,Object>> siteList = null;

    public List<Map<String, Object>> getSiteListNoPage(Map<String, Object> paramsMap) {
        List<Map<String,Object>> siteList = siteMapper.getSiteList(paramsMap);
        return siteList;
    }

    public List<Map<String, Object>> getSiteList(Map<String, Object> params) {
        String sortColumn = "SORT"; //默认的排序列
        String sortType = "ASC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  siteList = getSqlSession().selectList("com.szgd.dao.ecdata.project.SiteMapper.getSiteList", params,pageBounds);
        return siteList;
    }

    public Map<String, Object> getSite(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> siteList = siteMapper.getSiteList(paramsMap);
        if (siteList == null || siteList.size() == 0)
            return null;
        return siteList.get(0);
    }

    public Map<String, Object> getSiteByName(String name) {
        if (siteList == null)
            siteList = siteMapper.getSiteList(null);
        for (int i = 0; i < siteList.size(); i++) {
            Map<String, Object> siteMap = siteList.get(i);
            String tempName = siteMap.get("name").toString();
            if (tempName.equalsIgnoreCase(name))
            {
                return siteMap;
            }
        }
        return null;
    }

    public void insertSite(Map<String, Object> paramsMap) {

        siteMapper.insertSite(paramsMap);
        siteList = siteMapper.getSiteList(null);
    }

    public void updateSite(Map<String, Object> paramsMap) {

        siteMapper.updateSite(paramsMap);
        siteList = siteMapper.getSiteList(null);
    }

    public void deleteSite(Map<String, Object> paramsMap) {

        siteMapper.deleteSite(paramsMap);
    }

    public void setAllSite(HttpSession httpSession) {
        if (siteList == null)
            siteList = siteMapper.getSiteList(null);
        httpSession.setAttribute("siteList", siteList);
    }

    public List<Map<String, Object>> getSiteList() {
        if (siteList == null)
            siteList = siteMapper.getSiteList(null);
        return siteList;
    }

    public List<Map<String, Object>> getStartedSiteList(Map<String, Object> paramsMap) {
        List<Map<String,Object>> startedSiteList  = siteMapper.getStartedSiteList(paramsMap);
        return startedSiteList;
    }

    public void setSiteList(List<Map<String, Object>> siteList) {
        this.siteList = siteList;
    }

    public void uploadLayoutPic(Map<String, Object> paramMap) {
        String id = paramMap.get("id")== null?"":paramMap.get("id").toString().replace("null","");
        String isCameraAttachChanged = paramMap.get("isCameraAttachChanged")== null?"":paramMap.get("isCameraAttachChanged").toString().replace("null","");
        paramMap.put("path", SysUtil.getDateStr(null,"yyyyMM"));
        if (id.length() > 0 && isCameraAttachChanged.equalsIgnoreCase("1")) {
            siteMapper.updateSite(paramMap);
        }
    }

    public List<Map<String, Object>> app_getSiteList(Map<String, Object> paramsMap) {
        List<Map<String,Object>> siteList = siteMapper.app_getSiteList(paramsMap);
        Map<String,Object> tempMap = new HashMap<>();
        tempMap.put("id","all");
        tempMap.put("name","全部");
        siteList.add(0,tempMap);
        return siteList;
    }
}
