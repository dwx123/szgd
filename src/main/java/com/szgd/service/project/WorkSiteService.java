package com.szgd.service.project;

import com.szgd.dao.ecdata.project.WorkSiteMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WorkSiteService extends SimulateBaseService {

    @Autowired
    WorkSiteMapper workSiteMapper;


    public List<Map<String, Object>> getWorkSiteListNoPage(Map<String, Object> paramsMap) {
        List<Map<String,Object>> workSiteList = workSiteMapper.getWorkSiteList(paramsMap);
        return workSiteList;
    }
    public List<Map<String, Object>> getWorkSiteList(Map<String, Object> params) {
        String sortColumn = "ID"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  workSiteList = getSqlSession().selectList("com.szgd.dao.ecdata.project.WorkSiteMapper.getWorkSiteList", params,pageBounds);
        return workSiteList;
    }

    public Map<String, Object> getWorkSite(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> workSiteList = workSiteMapper.getWorkSiteList(paramsMap);
        if (workSiteList == null || workSiteList.size() == 0)
            return null;
        return workSiteList.get(0);
    }


    public void insertWorkSite(Map<String, Object> paramsMap) {

        workSiteMapper.insertWorkSite(paramsMap);
    }

    public void updateWorkSite(Map<String, Object> paramsMap) {

        workSiteMapper.updateWorkSite(paramsMap);
    }

    public void deleteWorkSite(Map<String, Object> paramsMap) {

        workSiteMapper.deleteWorkSite(paramsMap);
    }

    public void setAllWorkSite(HttpSession httpSession) {
        List<Map<String,Object>> workSiteList = workSiteMapper.getWorkSiteList(null);
        if (workSiteList == null || workSiteList.size() == 0)
            return;
        httpSession.setAttribute("workSiteList", workSiteList);
    }
}
