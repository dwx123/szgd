package com.szgd.service.sys;

import com.szgd.bean.SysLog;
import com.szgd.dao.ecdata.sys.LogMapper;
import com.szgd.dao.ecdata.sys.SysLogMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.paging.PageList;
import com.szgd.util.StringUtil;
import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LogService extends SimulateBaseService {

    @Autowired
    LogMapper logMapper;

    public List<Map<String, Object>> getLogList(Map<String, Object> params) {
        String sortColumn = "OP_TIME"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  logList = getSqlSession().selectList("com.szgd.dao.ecdata.sys.LogMapper.getLogList", params,pageBounds);
        return logList;
    }

    public List<Map<String, Object>> getOpLogList(Map<String, Object> params) {
        String sortColumn = "opTime"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        List<Map<String, Object>>  logList = getSqlSession().selectList("com.szgd.dao.ecdata.sys.LogMapper.getOpLogList", params,pageBounds);
        return logList;
    }

    public Map<String, Object> getLog(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> logList = logMapper.getLogList(paramsMap);
        if (logList == null || logList.size() == 0)
            return null;
        return logList.get(0);
    }

    public void insertLog(Map<String, Object> paramsMap) {

        logMapper.insertLog(paramsMap);
    }
}
