package com.szgd.service.sys;


import java.util.*;

import javax.transaction.Transactional;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.szgd.dao.ecdata.sys.RightMapper;
import com.szgd.util.StringUtil;

/**
 * 权限配置业务逻辑处理
 *
 */
@Service
@Transactional
public class RightService extends SimulateBaseService {
	private static Logger logger = Logger.getLogger(RightService.class);
	@Autowired
	RightMapper rightMapper;
	
	public List<Map<String, String>> getUserRight(Map<String, String> map) {
		return rightMapper.getUserRight(map);
	}
	public List<Map<String, Object>> getAllLeafMenus(Map<String, Object> map){
		return rightMapper.getAllLeafMenus(map);
	}
	public List<Map<String, String>> getAllRightByUserID(String userId){
		return rightMapper.getAllRightByUserID(userId);
	}
	
	public List<Map<String, Object>> getAllRoleRightByUserId(String userId){
		return rightMapper.getAllRoleRightByUserId(userId);
	}

	public List<Map<String, Object>> getAllSubRight(Map<String, Object> parentMap,String parentId,List<Map<String, Object>> rightList){
		List<Map<String, Object>> subMenuList = new ArrayList<>();
		for (int i = 0; i < rightList.size(); i++) {
			Map<String, Object> subRightMap =  rightList.get(i);
			if (subRightMap.get("PARENTID").toString().equalsIgnoreCase(parentId))
			{
				subMenuList.add(subRightMap);
				String subRightId = subRightMap.get("RIGHTID").toString();
				getAllSubRight(subRightMap,subRightId,rightList);
			}
		}
		if (subMenuList.size()>0)
			parentMap.put(parentId,subMenuList);
		return null;
	}

	public List<Map<String, String>> getAllRightByRoleID(String roleId){
		return rightMapper.getAllRightByRoleID(roleId);
	}

	
	public List<Map<String, String>> getRightByUserIDAndRightId(String userId,String rightId){
		if(StringUtil.isBlank(rightId)){
			rightId = "ROOT";
		}
		return rightMapper.getRightByUserIDAndRightId(userId, rightId);
	}
	
	public List<Map<String, String>> getMenuByUserID(String userID) {
		return rightMapper.getMenuByUserId(userID);
	}

	public boolean saveUserRight(Map<String,Object> map){
		try {
			String rightIds = map.get("rightIds").toString();
			String userIds = map.get("userIds").toString();
			String userId = map.get("userId").toString();	//废弃
			JSONArray jsonArrays= new JSONArray(rightIds);
			Set<String> rightids=new HashSet<String>();	//将本次权限配置的已选择权限全部放入该set
			for (int i = 0;i< jsonArrays.length(); i++){
				JSONObject jsonObject  = (JSONObject)jsonArrays.get(i);
				rightids.add(jsonObject.get("RIGHTID").toString());
			}

			JSONArray userIdsJsonArray= new JSONArray(userIds);
			for (int i = 0;i< userIdsJsonArray.length(); i++){
				JSONObject jsonObject  = (JSONObject)userIdsJsonArray.get(i);
				userId = jsonObject.get("userId").toString();

				rightMapper.deleteUserRightByUserId(userId);
				Map<String,String> tmp = new HashMap<String,String>();
				tmp.put("userId", userId);
				tmp.put("rightId", StringUtils.join(rightids, ","));
				rightMapper.insertUserRight(tmp);
			}
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return false;
		}
	}
	
	public boolean saveRoleRight(Map<String,Object> map){
		try {
			String rightIds = map.get("rightIds").toString();
			String roleIds = map.get("roleIds").toString();
			String roleId = map.get("roleId").toString();	//废弃
			JSONArray jsonArrays= new JSONArray(rightIds);
			Set<String> rightids=new HashSet<String>();	//将本次权限配置的已选择权限全部放入该set
			for (int i = 0;i< jsonArrays.length(); i++){
				JSONObject jsonObject  = (JSONObject)jsonArrays.get(i);
				rightids.add(jsonObject.get("RIGHTID").toString());
			}

			JSONArray roleIdsJsonArray= new JSONArray(roleIds);
			for (int i = 0;i< roleIdsJsonArray.length(); i++){
				JSONObject jsonObject  = (JSONObject)roleIdsJsonArray.get(i);
				roleId = jsonObject.get("roleId").toString();

				rightMapper.deleteUserRightByRoleId(roleId);
				Map<String,String> tmp = new HashMap<String,String>();
				tmp.put("roleId", roleId);
				tmp.put("rightId", StringUtils.join(rightids, ","));
				rightMapper.insertRoleRight(tmp);
			}
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return false;
		}
	}


}
