package com.szgd.service.sys;

import org.activiti.engine.impl.util.json.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

/**
 * @author Luis
 */
public class SimulateBaseService {

    @Resource
	protected SqlSessionTemplate sqlSessionTemplate;

	public SqlSessionTemplate getSqlSession() {
		return sqlSessionTemplate;
	}

	/**
	 * 将json对象数据转换成Map对象
	 * @param jsonObject
	 * @return
	 */
	protected Map<String,Object> JsonObject2Map(JSONObject jsonObject){
		Map<String,Object> map = new HashMap<>();
		if(jsonObject == null){
			return map;
		}
		Iterator jsonKeys = jsonObject.keys();
		while (jsonKeys.hasNext()){
			String keyName = (String)jsonKeys.next();
			map.put(keyName,jsonObject.get(keyName));
		}
		return map;
	}

	/**
	 * 获得32位大写的唯一编码
	 * @return
	 */
	protected String getSysUUID(){
		return UUID.randomUUID().toString().replace("-","").toUpperCase();
	}

	public static void main(String [] args)
	{
		System.out.println(new SimulateBaseService().getSysUUID());
	}
}
