package com.szgd.service.sys;



import java.util.*;

import javax.servlet.http.HttpSession;

import com.szgd.bean.SysDict;
import com.szgd.bean.User;
import com.szgd.dao.ecdata.sys.SysDictMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SysDictService extends SimulateBaseService {

    @Autowired
    SysDictMapper sysDictMapper;

    public void setAllParentChildMap(HttpSession httpSession) {
        List<SysDict> dictList = sysDictMapper.getAllDict(null);
        for (int i = 0; i < dictList.size(); i++) {
            SysDict sysDict = dictList.get(i);
            String dictCode = sysDict.getCode();
            String parentCode = sysDict.getParentCode();
            if (parentCode != null && parentCode.equalsIgnoreCase("root")) {
                ArrayList<SysDict> tempList = new ArrayList<SysDict>();
                for (int j = 0; j < dictList.size(); j++) {
                    SysDict tempDict = dictList.get(j);
                    if (tempDict.getParentCode() != null && tempDict.getParentCode().equalsIgnoreCase(dictCode)) {
                        tempList.add(tempDict);
                    }
                }
                httpSession.setAttribute(dictCode, tempList);
            }
        }
    }
    public int insert(SysDict sysDict, HttpSession httpSession) {
        sysDict.setId(getUUId());

        Map<String,Object> userMap = (Map<String,Object>) httpSession.getAttribute("userInfo");
        User user = new User();
        user.setUserName((String)userMap.get("userCnName"));
        user.setUserId((Integer)userMap.get("userId"));
        sysDict.setUser(user);
        sysDict.setOptDate(new Date());
        sysDict.setDictAvailable(true);
        return sysDictMapper.insert(sysDict);
    }

    public int update(SysDict sysDict) {
        //如果更新的是项目进度节点的名字,则项目进度管理里面项目进度录入的节点的名字也要更新
        return sysDictMapper.update(sysDict);
    }


    public SysDict getByDictId(String dictId) {
        return sysDictMapper.getById(dictId);
    }

    public List<SysDict>  getChildById(String dictId) {
        return sysDictMapper.getChildById(dictId);
    }

    public List<SysDict>  getAllChildById(String dictId) {
        return sysDictMapper.getAllChildById(dictId);
    }
    public String getIdByCode(String code) {
        return sysDictMapper.getIdByCode(code);
    }

    public List<SysDict> getAllSysDict(Map<String,Object> params) {
        return sysDictMapper.getAllDict(params);
    }



    public List<SysDict> getByDictIds(Collection<String> ids) {
        return sysDictMapper.getByDictIds(ids);
    }


    public List<SysDict> getByName(String dictName) {
        return sysDictMapper.getByName(dictName);
    }


    public List<SysDict> getByCode(String code) {
        return sysDictMapper.getByCode(code);
    }


    public List<SysDict> getByParentCode(String code) {
        return sysDictMapper.getByParentCode(code);
    }


    public List<SysDict> getAllChildDictByParentCode(String parentCode) {
        return sysDictMapper.getAllChildDictByParentCode(parentCode);
    }

    public List<SysDict> getChildDictByParentCodeAndDictValue(Map<String,Object> params) {
        return sysDictMapper.getChildDictByParentCodeAndDictValue(params);
    }

    private String getUUId() {
        String  s = UUID.randomUUID().toString();
        return s.substring(0,8) + s.substring(9,13) + s.substring(14,18) + s.substring(19,23) + s.substring(24);
    }



  public SysDict getSySByCode(String code){
        return  sysDictMapper.getSysByCode(code);
  }


public List<SysDict> getParentByChildCodeOfXhp(String childCode) {
	return sysDictMapper.getParentByChildCodeOfXhp(childCode);
}


public List<SysDict> getAllChildCodeOfXhp(String childCode) {
	return sysDictMapper.getAllChildCodeOfXhp(childCode);
}

}
