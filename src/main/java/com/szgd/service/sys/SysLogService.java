package com.szgd.service.sys;

import com.szgd.bean.SysLog;
import com.szgd.dao.ecdata.sys.SysLogMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.paging.PageList;
import com.szgd.util.StringUtil;
import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

@Service
public class SysLogService extends SimulateBaseService {

    @Autowired
    SysLogMapper sysLogMapper;

    public List<SysLog> getAllSysLog() {
        return sysLogMapper.getAllLog();
    }

    public SysLog getById(String id) {
        return sysLogMapper.getById(id);
    }

    public void deleteById(String id) {
        sysLogMapper.deleteById(id);
    }

    public List<SysLog> getByModel(Map<String, Object> mapParams) {
        String sortColumn = "SORT_NUMBER"; //默认的排序列
        String sortType = "asc";
        String iSortCol_0 = (String) mapParams.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) mapParams.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) mapParams.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }

        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(mapParams, Order.formString(sortString));	//绑定排序及相关参数
        List<SysLog> objs = getSqlSession().selectList("com.shzb.dao.sys.SysLogMapper.getByModel",mapParams, pageBounds);
        return getSqlSession().selectList("com.shzb.dao.sys.SysLogMapper.getByModel", mapParams,pageBounds);
    }

    public  <T> void outDataToTable(List<T> lists, HttpServletResponse response) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            JSONObject json = new JSONObject();
            if (lists instanceof PageList) {
                PageList<T> pageList = (PageList<T>) lists;
                json.put("aaData", new JSONArray(pageList));
                json.put("iTotalRecords", pageList.getPaginator().getTotalCount());
                json.put("iTotalDisplayRecords", pageList.getPaginator().getTotalCount());
            } else {
                json.put("aaData", new JSONArray(lists));
                json.put("iTotalRecords", lists.size());
                json.put("iTotalDisplayRecords", lists.size());
            }
            out.print(json);
            out.flush();
            out.close();
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
        catch (Exception e) {
           e.printStackTrace();
        }
    }
}
