package com.szgd.service.sys;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.szgd.license.LicenseClient;
import com.szgd.util.*;
import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.Exception.ExpiredException;
import org.springframework.security.Exception.NoLicenseException;
import org.springframework.security.Exception.NoLocalMachineCodeException;
import org.springframework.stereotype.Service;

import com.szgd.bean.User;
import com.szgd.dao.ecdata.personnel.PersonnelInfoMapper;
import com.szgd.dao.ecdata.sys.RoleMapper;
import com.szgd.dao.ecdata.sys.UserMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Service("userService")
public class UserService extends SimulateBaseService{
	private static Logger logger = Logger.getLogger(UserService.class);
	
	@Autowired
	private RoleMapper roleMapper;
	
	@Autowired
	private UserMapper userMapper;
	@Autowired
	LicenseClient licenseClient;
	@Autowired
    PersonnelInfoMapper personnelInfoMapper;
	public static final String method = "queryUserList";// 调用方法名

	private static int licenseState;//license验证状态
	private static long expireTime;//license到期时间

	public int getLicenseState() {
		return licenseState;
	}

	public void setLicenseState(int licenseState) {
		this.licenseState = licenseState;
	}

	public static long getExpireTime() {
		return expireTime;
	}

	public Map<String, Object> getUserParentDept(String loginId){
		Map<String, Object> params = new HashMap<>();
		params.put("LOGIN_ID" , loginId);
		List<Map<String, Object>> result = userMapper.getUserParentDept(params);
		Map<String, Object> map = new HashMap<>();
		if(result != null && result.size() >0 ){
			map = result.get(0);
		}
		return map;
	}

	/**
	 * 获得
	 * @param params
	 * @return
	 */
	public String getUserRole(Map<String, Object> params){
		String roleIds = "";
		Map<String, String> resultMap = userMapper.getUserRole(params);
		if(resultMap != null){
			roleIds = resultMap.get("ROLEID");
		}
		return roleIds;
	}

	/**
	 * 根据条件获得用户列表，供下拉框使用
	 * @param params map结构，map的参数可选择传dept_id与否
	 * @return map的key包括（USER_ID,LOGIN_ID,USER_NAME,DEPT_CD,DEPT_NAME,USER_NAME_WITH_DEPT）
	 */
	public List<Map<String, String>> getUserForCombobox(Map<String, Object> params){
		return userMapper.getUserForCombobox(params);
	}

	//调用十所提供的WS路径，获取全部最新的内网用户信息，更新用户表
	public String userInfoUpdate2(String synpath){
		String serviceAddres = PropertiesUtil.getPropValue("/ws.properties", "mhws");
		String userXML = (String) WebServiceUtil.callWs(serviceAddres, method, null);
		return insertIntoCsv2(userXML,synpath);
	}
	
	/**
	 * 解析XML字符串并插入CSV文件
	 * 样式：
	 *  <UserInfo>
	 *	    <userId>990588619</userId>
	 *	    <caCard>
	 *		</caCard>
	 *	    <userCa>
	 *		                                  </userCa>
	 *	    <loginId>4855kyh</loginId>
	 *	    <userPwd>DD4B21E9EF71E1291183A46B913AE6F2</userPwd>
	 *	    <userName>匡煜华</userName>
	 *	    <deptCd>SHJC10K00Z00</deptCd>
	 *	    <identityNo>320504199002213754</identityNo>
	 *	    <dutyCd>D5</dutyCd>
	 *	    <dutyName> </dutyName>
	 *	    <adminFlag>0000000000</adminFlag>
	 *	    <pcName> </pcName>
	 *	    <pcIp> </pcIp>
	 *	    <eMail> </eMail>
	 *	    <eMailPwd> </eMailPwd>
	 *	    <publicTel> </publicTel>
	 *	    <mobile> </mobile>
	 *	    <operateStatus>3</operateStatus>
	 *	    <pubFlag>0</pubFlag>
	 *	    <sortNumber>1</sortNumber>
	 *	  </UserInfo>
	 * @param userXML
	 */
	public String insertIntoCsv2(String userXML,String synpath){
		String[] headTitle = {"用户ID","CA卡号","用户CA号","用户登录名","用户密码","用户姓名","用户所属机构代码",
				     "证件号码","用户职务代码","用户职务名称","管理员标志","用户终端主机名","用户终端IP地址","邮件地址",
				     "邮件地址密码","办公电话","手机","操作状态","记录是否已发布(0：未发布  1：已发布)","排序号"};
		String[] childrenAttribute = {"userId","caCard","userCa","loginId","userPwd","userName",
				     "deptCd","identityNo","dutyCd","dutyName","adminFlag","pcName","pcIp",
				     "eMail","eMailPwd","publicTel","mobile","operateStatus","pubFlag","sortNumber"};
		try {
			if(null!=userXML && !"".equals(userXML) && userXML.length()>0){
				DateFormat dateTimeFormat=new SimpleDateFormat("yyyyMMddHHmmss");
				File targetFilePath = new File(synpath);
				if(!targetFilePath.exists())
					targetFilePath.mkdirs();
				String csvFilePath = synpath + "\\user"+dateTimeFormat.format(new Date())+".csv";;
				File targetFile = new File(csvFilePath);
				if(!targetFile.exists())
					targetFile.createNewFile();
				CsvUtil.createCSVHead(headTitle, targetFile);
				CsvUtil.appendCsvContent(userXML, "UserInfo", childrenAttribute, targetFile);
				return csvFilePath;
			}
		} catch (IOException e) {
			logger.error("解析并插入更新的用户信息到CSV文件异常！",e);
			return "-1";
		}
		return "-1";
	}	

	public boolean synchro(){
		boolean result = false;
		logger.info("开始同步用户数据!");
		String syn_pathuser = "";
		//部署在服務器使用
		String syn_path = PropertiesUtil.getPropValue("/ws.properties","syn_path");
		logger.info("syn_path1="+syn_path);
		syn_pathuser=userInfoUpdate2(syn_path);
		logger.info("syn_path2="+syn_pathuser);
		File targetFile2 = new File(syn_pathuser);
		String flag=CsvUtil.parseCSVFile2A(targetFile2,"GBK",2);		//插入用户至临时表			 Add comment by Jiayb at 2017/4/24
		if(flag.equals("1")){
			String transferFlag=batchInsertUserAfter();			//如果机构同步成功，开始同步用户 ,将临时表用户更新至用户表      Add comment by Jiayb at 2017/4/24
			if("1".equals(transferFlag)){
				logger.info("同步用户数据成功!");
				try {
					batchUpdateUserDeptName(null);
					logger.info("更新用户部门数据成功!");
				} catch (Exception e) {
					logger.error("更新用户部门数据失败!"+e.getMessage());
				}
				result = true;
			}
		}
		return result;
	}

	
	public List<Map<String,String>> getUserTree(Map<String, Object> map){
		return userMapper.getUserTree(map);
	}
	
	public List<Map<String,String>> getRoleTree(Map<String, Object> map){
		return roleMapper.getRoleTree(map);
	}
	
	/* @param loginId
	 * @param rightType MENU/ACTION
	 * @param isEnabled 0/1
	 * @return 根据登录id获取他的权限列表
	 */
	public List<Map<String, Object>> findUserRightListByLoginId(String loginId,String rightType,int isEnabled){
		Map<String, Object> param = new HashMap<String,Object>();
		param.put("loginId", loginId);
		param.put("rightType", rightType);
		param.put("isEnabled", isEnabled);
		return roleMapper.findUserRightListByLoginId(param);
	}
	
	public List<Map<String, Object>> findRoleRightListByLoginId(String loginId,String rightType,int isEnabled){
		Map<String, Object> param = new HashMap<String,Object>();
		param.put("loginId", loginId);
		param.put("rightType", rightType);
		param.put("isEnabled", isEnabled);
		return roleMapper.findRoleRightListByLoginId(param);
	}
	
	
	public User getUserByLoginId(String loginId){
		return userMapper.getUserByLoginId(loginId);
	}
	
	public User getUserByUserId(String userId){
		return userMapper.getUserByUserId(userId);
	}
	
	public User getDataByUserId(String userId){
		return userMapper.getDataByUserId(userId);
	}

	public List<User> getRcOrCfPerson(String roleId){
		return userMapper.getRcOrCfPerson(roleId);
	}

	public List<User> getAllRpPerson(){
		return userMapper.getAllRpPerson();
	}

	/**
	 * 转换xml为用户对象
	 * @param xml
	 * @return
	 */
	public User formatUserFromXML(String xml){
		User u = null;
		try {
			Document document = DocumentHelper.parseText(xml);
			Element root = document.getRootElement();
			Iterator iter = root.elementIterator();
			if (iter.hasNext()){
				u = new User();
				u.setUserId(Integer.parseInt(root.elementTextTrim("userId").replace("\r\n", "").replace("\n","")));
				u.setLoginId(root.elementTextTrim("loginId").replace("\r\n", "").replace("\n",""));
				u.setUserName(root.elementTextTrim("userName").replace("\r\n", "").replace("\n",""));
				u.setUserPwd(root.elementTextTrim("userPwd").replace("\r\n", "").replace("\n",""));
				u.setDeptName(root.elementTextTrim("deptName").replace("\r\n", "").replace("\n",""));
				u.setDeptCd(root.elementTextTrim("deptCd").replace("\r\n", "").replace("\n",""));
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return u;
	}

	
	/**
	 * 根据用户名和密码得到用户信息 （调用门户WS接口做验证）
	 * @param loginId
	 * @param pwd
	 * @return
	 */
	public User getWSUserInfo(String loginId,String pwd){
		User u = null;
		String serviceAddres = PropertiesUtil.getPropValue("/ws.properties", "mhws");
		String method = "queryUserInfoByLoginId";
		String returnMsg = (String) WebServiceUtil.callWs(serviceAddres, method, loginId,pwd);
		u = formatUserFromXML(returnMsg);
		return u;
	}
	
	
	/**
	 * 根据UserID获取用户信息 （调用门户WS接口做验证）
	 * @param userID
	 * @return
	 */
	public User getWSUserInfo(int userID){
		User u = null;
		String serviceAddres = PropertiesUtil.getPropValue("/ws.properties", "mhws");
		String method = "queryUserInfo ";
		String returnMsg = (String) WebServiceUtil.callWs(serviceAddres, method, userID);
		u = formatUserFromXML(returnMsg);
		return u;
	}
	
	/**
	 * 获取用户列表，使用分页查询
	 * @param params
	 * @return
	 */
	public List<Map<String,String>> getUserList(Map<String, Object> params){
		String sortColumn = "SORT_NUMBER"; //默认的排序列
		String sortType = "asc";
		String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
		String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
		if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
			sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
			sortType = sSortDir_0;
		}

		String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
		PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
		return getSqlSession().selectList("com.szgd.dao.ecdata.sys.UserMapper.getUserList", params,pageBounds);
	}

	/**
	 * 获取已经分配角色的用户
	 * @param params
	 * @return
	 */
	public List<Map<String,Object>> getUsersOfRole(Map<String, Object> params){
		return userMapper.getUsersOfRole(params);
	}

	/**
	 * 批量更新用户信息
	 * @param params
	 * @throws Exception
	 */
	public void batchInsertUser(List<Map<String,Object>> params){
		userMapper.deleteTempUser("");
		userMapper.batchInsertUser(params);
	}

	public String  batchInsertUserAfter(){
		try{
			userMapper.insertNewUser("");
			userMapper.updateAllUser("");
			userMapper.updateDelUser("");
			
			//分配默认权限
			userMapper.granDefaultMenu();
			return "1";
		}catch(Exception e){
			return "0";
		}
	}
	public List<Map<String,String>> getAllUserInfo(Map<String, Object> params){
		return userMapper.getUserList(params);
	}
	
	/**
	 * 分页查询角色授权的部门用户信息
	 * @param params
	 * @return
	 */
	public List<Map<String, String>> getRoleGrantUsers(Map<String,Object> params){
		return userMapper.getRoleGrantUsers(params);
	}
	
	/**
	 * 批量更新用户的部门名称（大平台同步部门、用户成功后调用）
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	public int batchUpdateUserDeptName(Map<String,Object> params) throws Exception{
		return userMapper.batchUpdateUserDeptName(params);
	}
	
	/**
	 * 批量授权用户到角色组
	 * @param map
	 * @return
	 */
	public boolean batchGrantUserToRole(Map<String,Object> map){
		boolean mark = true;
		try {
			//String userIds = map.get("userIds").toString();
			String USERIDS = map.get("userIds").toString();
			String roleid = map.get("ROLE_ID").toString();
			//JSONArray jsonArrays=JSONArray.fromObject(USERIDS);
			String[] userIds = USERIDS.split(",");
			List<Map<String,Object>> params = new ArrayList<Map<String,Object>>();
			for (Object object : userIds) {
				Map<String,Object> dataright = new HashMap<String,Object>();
				//JSONObject json=JSONObject.fromObject(object);
				dataright.put("USER_ID", object.toString());
				dataright.put("ROLE_ID", roleid);
				params.add(dataright);
			}
			userMapper.batchGrantUserToRole(params);
		} catch (Exception e) {
			mark = false;
			logger.error("批量授权用户到角色组失败！",e);
		}
		return mark;
	}
	
	/**
	 * 从角色组移除用户
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public int delUserAndRoleRelated(Map<String,Object> params) throws Exception{
		return userMapper.delUserAndRoleRelated(params);
	}
	
	
	/**
	 * @param USER_ID 警号
	 * @param IDENTITY_NO 身份证号，使用该号码登录
	 * @param USER_NAME
	 * @param DEPT_NAME
	 * @return 创建临时用户，外部调阅用
	 */
	public Map<String,Object> createTempUser(String USER_ID,String IDENTITY_NO,String USER_NAME,String DEPT_NAME){
		Map<String,Object> resMap = new HashMap<String,Object>();
		boolean exisit = false;
		int res = 0;
		User user = this.getUserByLoginId(IDENTITY_NO);
		if(user != null){
			//临时用户已经存在，不再创建
			exisit = true;
			USER_ID = String.valueOf(user.getUserId());
		}else{
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("USER_ID", USER_ID);
			params.put("USER_NAME", USER_NAME);
			params.put("DEPT_CD", "Temp");
			params.put("DEPT_NAME", DEPT_NAME);
			params.put("REMARK", "外部调阅临时账号："+DEPT_NAME);
			params.put("USER_SOURCE", "Temp");
			params.put("LOGIN_ID", IDENTITY_NO);
			params.put("IDENTITY_NO", IDENTITY_NO);
			String pwd = IDENTITY_NO.length()>5 ? IDENTITY_NO.substring(IDENTITY_NO.length()-6,IDENTITY_NO.length()) : IDENTITY_NO;
			params.put("USER_PWD", AEncryptHelper.encrypt(pwd, IDENTITY_NO));
			res =  userMapper.createTempUser(params);
			if(res==1){
				params.put("ROLE_ID", "Temp_user");
				res = userMapper.grantUserToRole(params);
			}
		}
		resMap.put("exisit", exisit);
		resMap.put("res", res);
		resMap.put("USER_ID", USER_ID);
		return resMap;
	}
	
	public int getNextTempUserId(){
		return  userMapper.getNextTempUserId();
	}
	
	public List<Map<String, String>> getZongDuiLd(Map<String,Object> params) throws Exception{
		return userMapper.getZongDuiLd(params);
	}
	
	public String getUserApproveRoleId_ByMenuRoleId(Map<String,Object> params) throws Exception{
		return userMapper.getUserApproveRoleId_ByMenuRoleId(params);
	}
	
	
    public Map<String,String> setPassword(Map<String, Object> map){
    	return (Map<String, String>) userMapper.setPassword(map);
    }
    public Map<String,String> login(Map<String, Object> map){
    	return (Map<String, String>) userMapper.login(map);
    }

	public String getApproverStrByRoleAndDept(Map<String, String> params) {
		return userMapper.getApproverStrByRoleAndDept(params);
	}

	public List<Map<String, String>> getApproverListByRoleAndDept(Map<String,Object> params) {
		return userMapper.getApproverListByRoleAndDept(params);
	}

	public List<Map<String, String>> getUserListByDept(Map<String,Object> params) {
		return userMapper.getUserListByDept(params);
	}
	
	public Integer getNewUserId(){
		return userMapper.getNewUserId();
	}
	
	public int insertUser(Map<String, Object> params) throws Exception{
		return userMapper.insertUser(params);
	}
	
	public int insertUserAndRole(Map<String, Object> params) throws Exception{
		userMapper.insertUser(params);
		Map<String, Object> rolemap = new HashMap<String, Object>();
		rolemap.put("ROLE_ID", BusinessName.DEFAULT_ROLE.getValue());
		rolemap.put("USER_ID", (Integer)params.get("USER_ID"));
		return userMapper.grantUserToRole(rolemap);
	}
	public int updateUser(Map<String, Object> params) throws Exception{
		return userMapper.updateUser(params);
	}
	
	public String insertUserFromPerson(String flag,String dept,String[] personIds) throws Exception{
		int ins = 0;
		int upd = 0;
		if(StringUtil.isNotBlank(flag) && StringUtil.isNotBlank(dept) && personIds != null && personIds.length != 0){
			for(String id :personIds){
				Integer personId = Integer.valueOf(id);
				Map<String, Object> personMap = personnelInfoMapper.selectByPrimaryKey(personId);
				if(personMap != null){
					String name = personMap.get("NAME") == null ? "":personMap.get("NAME").toString();
					if(StringUtil.isNotBlank(name)){
						String zjhm = personMap.get("ID_NUMBER") == null ? "":personMap.get("ID_NUMBER").toString();
						String loginId = Pinyin4j.getLowerCase(name, true);
						if(StringUtil.isNotBlank(loginId)){
							Map<String, Object> map = new HashMap<String, Object>();
							Integer userid = getNewUserId();
							map.put("USER_ID", userid);
							map.put("LOGIN_ID", loginId);
							map.put("USER_PWD", AEncryptHelper.encrypt("123456",loginId));
							map.put("USER_NAME", name);
							map.put("DEPT_CD",dept);
							map.put("PERSONNEL_ID",personId);
							map.put("IDENTITY_NO",zjhm);
							map.put("FLAG",flag);
							User user = userMapper.getUserByPersonId(personId);
							if(user != null){
								userMapper.updateUserByPerson(map);
								upd++;
							}else{
								userMapper.insertUser(map);
								Map<String, Object> rolemap = new HashMap<String, Object>();
								rolemap.put("ROLE_ID", BusinessName.DEFAULT_ROLE.getValue());
								rolemap.put("USER_ID", userid);
								userMapper.grantUserToRole(rolemap);
								ins++;
							}
						}else{
							throw new Exception("人员："+name+"无法添加到用户表中，根据用户名转换登录名失败！"); 
						}
					}else{
						throw new Exception("人员id："+id+"的用户名不存在！"); 
					}
				}else{
					throw new Exception("人员id："+id+"的数据已经不存在！"); 
				}
			}
		}else{
			throw new Exception("缺少传入人员参数，请核查！"); 
		}
		return "插入"+ins+"条，更新"+upd+"条。";
	}

	public int delUser(Map<String, Object> params) throws Exception{
		return userMapper.delUser(params);
	}
	
	public List<Map<String, String>> getSiteAuth(Map<String,Object> params){
		PageBounds pageBounds = new PageBounds(params, null);
		return getSqlSession().selectList("com.szgd.dao.ecdata.sys.UserMapper.getSiteAuth", params,pageBounds);
	}
	
	public List<Map<String, String>> getSiteGrantUsers(Map<String,Object> params){
		PageBounds pageBounds = new PageBounds(params, null);
		return getSqlSession().selectList("com.szgd.dao.ecdata.sys.UserMapper.getSiteGrantUsers", params,pageBounds);
	}
	
	public List<Map<String, String>> getUserGrantSites(Map<String,Object> params){
		PageBounds pageBounds = new PageBounds(params, null);
		return getSqlSession().selectList("com.szgd.dao.ecdata.sys.UserMapper.getUserGrantSites", params,pageBounds);
	}

	public List<Map<String, Object>> getSiteUserList(Map<String,Object> params){
		return userMapper.getSiteUserList(params);
	}

	public int deleteSiteUser(Map<String, Object> params) throws Exception{
		String userId = params.get("USER_ID") == null?null:params.get("USER_ID").toString();
		if (userId == null)
		{
			String personId = params.get("PERSONNEL_ID") == null?null:params.get("PERSONNEL_ID").toString();
			if (personId != null)
			{
				User user = userMapper.getUserByPersonId(Integer.parseInt(personId));
				if (user != null)
				{
					params.put("USER_ID",user.getUserId());
				}else
					return -1;
			}else
				return  -1;

		}

		return userMapper.deleteSiteUser(params);
	}

	public int insertSiteUser(Map<String, Object> params) throws Exception{
		String userId = params.get("USER_ID") == null?null:params.get("USER_ID").toString();
		if (userId == null)
		{
			String personId = params.get("PERSONNEL_ID") == null?null:params.get("PERSONNEL_ID").toString();
			if (personId != null)
			{
				User user = userMapper.getUserByPersonId(Integer.parseInt(personId));
				if (user != null)
				{
					params.put("USER_ID",user.getUserId());
				}else
					return -1;
			}else
				return  -1;

		}
		List<Map<String,Object>> list = userMapper.getSiteUserList(params);
		if (list == null || list.size() == 0)
			return userMapper.insertSiteUser(params);
		else
			return -1;
	}

	/**
	 * 更新站点用户中间表
	 * @param map
	 * @return
	 */
	public boolean batchGrantUserToSite(Map<String,Object> map){
		boolean mark = true;
		try {
			String userIds = map.get("userIds").toString();
			String roleid = map.get("USER_ID").toString();
			JSONArray jsonArrays = new JSONArray(userIds);
			List<Map<String,Object>> params = new ArrayList<Map<String,Object>>();
			for (int i=0; i < jsonArrays.length() ; i++) {
				Map<String,Object> dataright = new HashMap<String,Object>();
				JSONObject json = jsonArrays.getJSONObject(i);
				dataright.put("SITE_ID", json.get("SITE_ID").toString());
				dataright.put("USER_ID", roleid);
				params.add(dataright);
			}
			userMapper.batchGrantUserToSite(params);
		} catch (Exception e) {
			mark = false;
			logger.error("批量授权用户到站点组失败！",e);
		}
		return mark;
	}

	/*
	根据登录用户名获取该用户所配置的标段id及标段名称
	 */
	public void setUserBid(HttpSession httpSession){
		Map<String, Object> params = new HashMap<>();
		org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) httpSession.getAttribute(UserUtil.USERINFOMATION);
		params.put("loginId",user.getLoginId());
		List<Map<String, Object>> list = userMapper.getBidInfoByUserName(params);
		httpSession.setAttribute("userBidList", list);
		String userBidIds = null;
		for (Map<String, Object> map:list) {
			String bidId = map.get("bidId").toString();
			if (userBidIds == null)
				userBidIds = bidId;
			else
				userBidIds = userBidIds + "," + bidId;
		}
		httpSession.setAttribute("userBidIds", userBidIds);

	}

	public void checkLicense() {

		URL url = this.getClass().getClassLoader().getResource("application.license");
		if (url == null)
		{
			//throw new NoLicenseException("no license file!");
		}
		String filePath = url.getPath();//获取文件路径

		licenseClient.validateLicense(new File(filePath));
		expireTime = licenseClient.getExpire_time();
		if (licenseClient.getLicense_state()== LicenseClient.STATE_CODEERR)
		{
			//throw new NoLocalMachineCodeException("Running on a not licensed machine");
		}else
		if (licenseClient.getLicense_state()==LicenseClient.STATE_TIMEOUT)
		{
			//throw new ExpiredException("license expired");
		}

	}
}
