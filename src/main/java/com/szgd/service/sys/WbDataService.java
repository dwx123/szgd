package com.szgd.service.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.szgd.service.project.BidService;
import com.szgd.service.project.SiteService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.szgd.bean.Wbdg;
import com.szgd.bean.Wbjc;
import com.szgd.service.sys.SimulateBaseService;

@Service
public class WbDataService extends SimulateBaseService {
    @Autowired
    com.szgd.dao.ecdata.sys.WbjcMapper WbjcMapper;
    @Autowired
    com.szgd.dao.ecdata.sys.WbdgMapper WbdgMapper;
	@Autowired
	SiteService siteService;
	@Autowired
	BidService bidService;

    private static Logger logger = Logger.getLogger(WbDataService.class);


    public void saveWbjc(List<Wbjc> jcList){
    	if(jcList != null && jcList.size() != 0){
    		String siteName = jcList.get(0).getSiteName();
    		Map<String,Object> params = new HashMap<>();
    		params.put("siteName",siteName);
    		List<Map<String,Object>> siteList = siteService.getSiteListNoPage(params);
    		String siteId = "";
    		if(siteList != null && siteList.size() > 0){
    			siteId = siteList.get(0).get("id").toString();
			}
			params.put("siteId",siteId);
    		WbjcMapper.truncateWbjc(params);
    		for(Wbjc jc : jcList){
    			if(jc != null){
    				jc.setSiteId(Long.valueOf(siteId));
    				WbjcMapper.insert(jc);
    			}
    		}
    	}
    }
    
    public void saveWbdg(List<Wbdg> dgList){
    	if(dgList != null && dgList.size() != 0){
			String bidName = dgList.get(0).getBidName();
			Map<String,Object> params = new HashMap<>();
			params.put("bidName",bidName);
			List<Map<String,Object>> bidList = bidService.getBidListNoPage(params);
			String bidId = "";
			if(bidList != null && bidList.size() > 0){
				bidId = bidList.get(0).get("id").toString();
			}
			params.put("bidId",bidId);
    		WbdgMapper.truncateWbdg(params);
    		for(Wbdg dg : dgList){
    			if(dg != null){
    				dg.setBidId(Long.valueOf(bidId));
    				WbdgMapper.insert(dg);
    			}
    		}
    	}
    }
}
