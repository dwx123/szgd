package com.szgd.service.sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.szgd.dao.ecdata.sys.WfRoleMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.util.StringUtil;


@Service("wfRoleService")
public class WfRoleService extends SimulateBaseService{
	private static Logger logger = Logger.getLogger(WfRoleService.class);
	@Autowired
	WfRoleMapper wfRoleMapper;
	
	/**
	 * 查询所有的流程角色
	 * @param params
	 * @return
	 */
	public List<Map<String,Object>> getAllWfRoles(Map<String, Object> params){
		return wfRoleMapper.getAllWfRoles(params);
	}

	/**
	 * 根据备注查询流程角色
	 * @param params
	 * @return
	 */
	public List<Map<String,Object>> getWfRolesByMemo(Map<String, Object> params){
		return wfRoleMapper.getWfRolesByMemo(params);
	}

	/**
	 * 根据部门编号查询流程角色
	 * @param params
	 * @return
	 */
	public List<Map<String,Object>> getWfRolesByDeptCD(Map<String, Object> params){
		return wfRoleMapper.getWfRolesByDeptCD(params);
	}

	/**
	 * 根据用户id查找角色List
	 * @param userid
	 * @return
	 */
	public List<Map<String,Object>> getWfRoleListByUserId(int userid){
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("USER_ID", userid);
		return wfRoleMapper.getWfRolesByUserId(params);
	}

	/**
	 * 根据用户id查找角色串
	 * @param userid
	 * @return
	 */
	public String getWfRolesByUserId(int userid){
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("USER_ID", userid);
		List<Map<String,Object>> list = wfRoleMapper.getWfRolesByUserId(params);
		if (list == null || list.size() == 0)
			return null;
		String roleIds = null;
		for (int i = 0; i < list.size(); i++) {
			String roleid = list.get(i).get("ROLE_ID").toString();
			if (roleIds == null)
				roleIds = roleid;
			else
				roleIds = roleIds + "," + roleid;
		}
		return roleIds;
	}


	/**
	 * 根据角色ID查询其包含的用户
	 * @param params
	 * @return
	 */
	public List<Map<String,String>> getUsersOfWfRole(Map<String, Object> params){
		return wfRoleMapper.getUsersOfWfRole(params);
	}

	/**
	 * 分页查询流程角色的已授权用户列表
	 * @param params
	 * @return
	 */
	public List<Map<String, String>> getWfRoleGrantUsers(Map<String,Object> params){
		String sortString = "DEPT_CD.asc";
		PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));
		return getSqlSession().selectList("com.szgd.dao.ecdata.sys.WfRoleMapper.getWfRoleGrantUsers", params,pageBounds);
	}
	
	// 查询该流程角色下尚未被赋值菜单的人员 
	public List<Map<String, String>> getNotGrantRightApproveUsers(Map<String,Object> params){
		String sortString = "DEPT_CD.asc";
		PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));
		return getSqlSession().selectList("com.szgd.dao.ecdata.sys.WfRoleMapper.getNotGrantRightApproveUsers", params,pageBounds);
	}
	
	
	
	
	/**
	 * 新增角色
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	public int insertWfRole(Map<String, Object> params) throws Exception{
		return wfRoleMapper.insertWfRole(params);
	}
	
	/**
	 * 更新角色
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	public int updateWfRole(Map<String, Object> params) throws Exception{
		return wfRoleMapper.updateWfRole(params);
	}
	
	/**
	 * 删除角色
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	public int deleteWfRole(String params) throws Exception{
		return wfRoleMapper.deleteWfRole(params);
	}
	/**
	 * 删除角色用户
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	public int deleteWfRoleUser(Map<String, Object> params) throws Exception{
		return wfRoleMapper.deleteWfRoleUser(params);
	}
	
	
	
	/**
	 * 获得流程角色未授权的用户列表
	 * @param params
	 * @return
	 */
	public List<Map<String,Object>> getNoGrantUsers(Map<String, Object> params){
		return wfRoleMapper.getNoGrantUsers(params);
	}
	
	public List<Map<String,Object>> getNoGrantSites(Map<String, Object> params){
		return wfRoleMapper.getNoGrantSites(params);
	}
	
	/**
	 * 更新流程角色中间表
	 * @param map
	 * @return
	 */
	public boolean batchGrantUserToWfRole(Map<String,Object> map){
		boolean mark = true;
		try {
			String userIds = map.get("userIds").toString();
			String roleid = map.get("ROLE_ID").toString();
			JSONArray jsonArrays = new JSONArray(userIds);
			List<Map<String,Object>> params = new ArrayList<Map<String,Object>>();
			for (int i=0; i < jsonArrays.length() ; i++) {
				Map<String,Object> dataright = new HashMap<String,Object>();
				JSONObject json = jsonArrays.getJSONObject(i);
				dataright.put("USER_ID", json.get("USER_ID").toString());
				dataright.put("ROLE_ID", roleid);
				params.add(dataright);
			}
			wfRoleMapper.batchGrantUserToWfRole(params);
		} catch (Exception e) {
			mark = false;
			logger.error("批量授权用户到流程角色组失败！",e);
		}
		return mark;
	}
	
	public int checkUnique(String roleId, String userId) throws Exception{
		return wfRoleMapper.checkUnique(roleId, userId);
	}
	
	public List<Map<String,Object>> getRoleListByPage(Map<String, Object> params) {
		String sortColumn = "ROLE_NAME"; //默认的排序列
		String sortType = "asc";
		String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
		String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
		if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
			sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
			sortType = sSortDir_0;
		}
		String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
		PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
		return getSqlSession().selectList("com.szgd.dao.ecdata.sys.WfRoleMapper.getAllWfRoles", params,pageBounds);
	}
	public List<Map<String,Object>> getAppMenu(Map<String, Object> params){
		return wfRoleMapper.getAppMenu(params);
	}

	public List<Map<String,Object>> getAppLeafMenu(Map<String, Object> params){
		return wfRoleMapper.getAppLeafMenu(params);
	}

	public List<String> getTagByRole(String params,String params2){
		return wfRoleMapper.getTagByRole(params,params2);
	}
	
	public List<String> getTagByUser(Integer params,String params2){
		return wfRoleMapper.getTagByUser(params,params2);
	}
	
	public List<String> getUserByYhTag(Integer params,String params2){
		return wfRoleMapper.getUserByYhTag(params,params2);
	}
	
	public List<Map<String,Object>> getUserByYhRoleForTag(String params2){
		return wfRoleMapper.getUserByYhRoleForTag(params2);
	}

}
