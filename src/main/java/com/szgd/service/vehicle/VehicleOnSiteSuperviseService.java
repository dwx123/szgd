package com.szgd.service.vehicle;

import com.szgd.bean.SysDict;
import com.szgd.dao.ecdata.vehicle.VehicleMapper;
import com.szgd.dao.ecdata.vehicle.VehicleOnSiteSuperviseMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.DeptService;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.service.sys.SysDictService;
import com.szgd.util.Constant;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class VehicleOnSiteSuperviseService extends SimulateBaseService {
    @Autowired
    VehicleOnSiteSuperviseMapper vehicleOnSiteSuperviseMapper;

    public List<Map<String, Object>> getVehicleOnSiteSuperviseList(Map<String, Object> params) {
        String siteId = params.get("siteId")==null?"":params.get("siteId").toString().replace("null","");
        if(siteId.length() == 0){
            params.put("siteId",null);
        }
        String sortColumn = "ID"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        return getSqlSession().selectList("com.szgd.dao.ecdata.vehicle.VehicleOnSiteSuperviseMapper.getVehicleOnSiteSuperviseList", params,pageBounds);
    }


    public void deleteVehicleOnSiteSupervise(Map<String, Object> paramsMap) {
        vehicleOnSiteSuperviseMapper.deleteVehicleOnSiteSupervise(paramsMap);
    }



    public void insertVehicleOnSiteSupervise(Map<String, Object> paramsMap) {

        vehicleOnSiteSuperviseMapper.insertVehicleOnSiteSupervise(paramsMap);
    }


    public List<Map<String, Object>> getVehicleOnSiteSuperviseListNoPage(Map<String, Object> params) {
        return vehicleOnSiteSuperviseMapper.getVehicleOnSiteSuperviseList(params);
    }

    public Map<String, Object> app_getSupervisionRecord(Map<String, Object> params) {
        params.put("isApp","1");
        String siteId = params.get("siteId")==null?"":params.get("siteId").toString().replace("null","");
        if(siteId.length() == 0 || siteId.equalsIgnoreCase("all")){
            params.put("siteId",null);
        }
        List<Map<String,Object>> list = vehicleOnSiteSuperviseMapper.app_getSupervisionList(params);
        for(Map<String,Object> temp : list){
            String presentFlag = temp.get("presentFlag")==null?"":temp.get("presentFlag").toString();
            if(presentFlag.equalsIgnoreCase("0")){
                temp.put("presentFlag","否");
            }else if(presentFlag.equalsIgnoreCase("1")){
                temp.put("presentFlag","是");
            }
        }
        List<Map<String,Object>> count = vehicleOnSiteSuperviseMapper.getVehicleOnSiteSuperviseList(params);
        Map<String, Object> res = new HashMap<>();
        res.put("list",list);
        res.put("count",count.size());
        return res;
    }
}
