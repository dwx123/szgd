package com.szgd.service.vehicle;

import com.szgd.bean.SysDict;
import com.szgd.dao.ecdata.vehicle.VehicleMapper;
import com.szgd.paging.Order;
import com.szgd.paging.PageBounds;
import com.szgd.service.sys.DeptService;
import com.szgd.service.sys.SimulateBaseService;
import com.szgd.service.sys.SysDictService;
import com.szgd.util.Constant;
import com.szgd.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class VehicleService extends SimulateBaseService {
    @Autowired
    VehicleMapper vehicleMapper;
    @Autowired
    DeptService deptService;
    @Autowired
    SysDictService sysDictService;

    public List<Map<String, Object>> getVehicleInfoList(Map<String, Object> params) {
        String sortColumn = "ID"; //默认的排序列
        String sortType = "DESC";
        String iSortCol_0 = (String) params.get("iSortCol_0");	//排序字段，前台传进来的为数值，列从0开始
        String sSortDir_0 = (String) params.get("sSortDir_0");	//排序类型，asc或desc
        if(StringUtil.isNotBlank(iSortCol_0) && StringUtil.isNotBlank(sSortDir_0)){
            sortColumn = (String) params.get("mDataProp_" + iSortCol_0);
            sortType = sSortDir_0;
        }
        String sortString = sortColumn + "." + sortType;	//排序字段及排序类型
        PageBounds pageBounds = new PageBounds(params, Order.formString(sortString));	//绑定排序及相关参数
        return getSqlSession().selectList("com.szgd.dao.ecdata.vehicle.VehicleMapper.getVehicleInfoList", params,pageBounds);
    }

    /**
     * 获得需要监理旁站的车辆
     * @return
     */
    public List<Map<String,Object>> getOnSiteSuperviseVehicleList() {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("isOnSiteSupervise",1);
        List<Map<String,Object>> vehicleTypeList = vehicleMapper.getVehicleTypeList(paramsMap);
        return vehicleTypeList;
    }

    public Map<String, Object> getVehicleInfo(Map<String, Object> params) {
        List<Map<String,Object>>  list = vehicleMapper.getVehicleInfo(params);
        if (list != null && list.size() >0)
        {
            return  list.get(0);
        }
        else
            return  null;
    }

    public void deleteVehicleInfo(Map<String, Object> paramsMap) {
        vehicleMapper.deleteVehicleInfo(paramsMap);
    }

    public Map<String, Object> getVehicleInfo(String id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id",id);
        List<Map<String,Object>> vehicleInfoList = vehicleMapper.getVehicleInfoList(paramsMap);
        if (vehicleInfoList == null || vehicleInfoList.size() == 0)
            return null;
        return vehicleInfoList.get(0);
    }

    public void updateVehicleInfo(Map<String, Object> paramsMap) {

        vehicleMapper.updateVehicleInfo(paramsMap);
    }

    public void insertVehicleInfo(Map<String, Object> paramsMap) {

        vehicleMapper.insertVehicleInfo(paramsMap);
    }

    public Map<String, String> getFieldMap(){
        Map<String, String> fieldMapping = new LinkedHashMap<>();

//        fieldMapping.put("order","序号");
        fieldMapping.put("plateNumber","车牌号");
        fieldMapping.put("vehicleType","车型");
        fieldMapping.put("useDept","所属单位");
        fieldMapping.put("inCharge","联系人");
        fieldMapping.put("inChargeTel","联系人电话");

        return fieldMapping;
    }

    public Map<String, Object> batchImportVehicleInfo(List<Map<String, Object>> vehicleInfoList,String userId,String batchNo) {
        Map<String, Object> resultMap = new HashMap<>();
        List<Map<String, Object>> successList = new ArrayList<>();   //记录导入成功的数据
        List<Map<String, Object>> updateList = new ArrayList<>();   //记录更新的数据
        List<Map<String, Object>> failedList = new ArrayList<>();   //记录导入失败的数据
        List<Map<String, Object>> ignoreList = new ArrayList<>();   //记录忽略的数据


        for (Map<String, Object> vehicleInfoMap : vehicleInfoList) {

            HashMap<String, Object> sourceDataMap = new HashMap<String, Object>(vehicleInfoMap);
            try {
                //判断车牌号是否存在
                if(vehicleInfoMap.get("plateNumber") != null && StringUtil.isNotBlank(String.valueOf(vehicleInfoMap.get("plateNumber")))) {
                }else{
                    sourceDataMap.put("ERROR_MSG","车牌号为必填项");
                    failedList.add(sourceDataMap);
                    continue;
                }

                //车型
                if(vehicleInfoMap.get("vehicleType") != null && StringUtil.isNotBlank(String.valueOf(vehicleInfoMap.get("vehicleType")))) {
                    vehicleInfoMap.put("PARENT_CODE", "VEHICLE_TYPE");
                    vehicleInfoMap.put("DICT_VALUE", vehicleInfoMap.get("vehicleType"));
                    List<SysDict> vehicleList = sysDictService.getAllSysDict(vehicleInfoMap);
                    if (vehicleList != null && vehicleList.size() > 0) {
                        SysDict dict = vehicleList.get(0);
                        vehicleInfoMap.put("vehicleType", dict.getCode());
                    } else {
                        sourceDataMap.put("ERROR_MSG","车型填写不正确(参考数据字典)");
                        failedList.add(sourceDataMap);
                        continue;
                    }
                }

//                //车牌类型
//                if(vehicleInfoMap.get("plateType") != null && StringUtil.isNotBlank(String.valueOf(vehicleInfoMap.get("plateType")))) {
//                    vehicleInfoMap.put("PARENT_CODE", "PLATE_TYPE");
//                    vehicleInfoMap.put("DICT_VALUE", vehicleInfoMap.get("plateType"));
//                    List<SysDict> plateList = sysDictService.getAllSysDict(vehicleInfoMap);
//                    if (plateList != null && plateList.size() > 0) {
//                        SysDict dict = plateList.get(0);
//                        vehicleInfoMap.put("plateType", dict.getCode());
//                    } else {
//                        sourceDataMap.put("ERROR_MSG","车牌类型填写有误");
//                        failedList.add(sourceDataMap);
//                        continue;
//                    }
//                }
//
//                //车牌颜色
//                if(vehicleInfoMap.get("plateColor") != null && StringUtil.isNotBlank(String.valueOf(vehicleInfoMap.get("plateColor")))) {
//                    vehicleInfoMap.put("PARENT_CODE", "COLOR_TYPE");
//                    vehicleInfoMap.put("DICT_VALUE", vehicleInfoMap.get("plateColor"));
//                    List<SysDict> vehicleList = sysDictService.getAllSysDict(vehicleInfoMap);
//                    if (vehicleList != null && vehicleList.size() > 0) {
//                        SysDict dict = vehicleList.get(0);
//                        vehicleInfoMap.put("plateColor", dict.getCode());
//                    } else {
//                        sourceDataMap.put("ERROR_MSG","车牌颜色填写有误");
//                        failedList.add(sourceDataMap);
//                        continue;
//                    }
//                }
//
//                //品牌
//                if(vehicleInfoMap.get("vehicleBrand") != null && StringUtil.isNotBlank(String.valueOf(vehicleInfoMap.get("vehicleBrand")))) {
//                    vehicleInfoMap.put("PARENT_CODE", "VEHICLE_BRAND");
//                    vehicleInfoMap.put("DICT_VALUE", vehicleInfoMap.get("vehicleBrand"));
//                    List<SysDict> vehicleList = sysDictService.getAllSysDict(vehicleInfoMap);
//                    if (vehicleList != null && vehicleList.size() > 0) {
//                        SysDict dict = vehicleList.get(0);
//                        vehicleInfoMap.put("vehicleBrand", dict.getCode());
//                    } else {
//                        sourceDataMap.put("ERROR_MSG","品牌填写有误");
//                        failedList.add(sourceDataMap);
//                        continue;
//                    }
//                }
//
//                //车辆颜色
//                if(vehicleInfoMap.get("vehicleColor") != null && StringUtil.isNotBlank(String.valueOf(vehicleInfoMap.get("vehicleColor")))) {
//                    vehicleInfoMap.put("PARENT_CODE", "COLOR_TYPE");
//                    vehicleInfoMap.put("DICT_VALUE", vehicleInfoMap.get("vehicleColor"));
//                    List<SysDict> vehicleList = sysDictService.getAllSysDict(vehicleInfoMap);
//                    if (vehicleList != null && vehicleList.size() > 0) {
//                        SysDict dict = vehicleList.get(0);
//                        vehicleInfoMap.put("vehicleColor", dict.getCode());
//                    } else {
//                        sourceDataMap.put("ERROR_MSG","车辆颜色填写有误");
//                        failedList.add(sourceDataMap);
//                        continue;
//                    }
//                }
//
//                //车辆大小
//                if(vehicleInfoMap.get("vehicleSize") != null && StringUtil.isNotBlank(String.valueOf(vehicleInfoMap.get("vehicleSize")))) {
//                    String vehicleSize = vehicleInfoMap.get("vehicleSize").toString();
//                    if (vehicleSize.equalsIgnoreCase("1"))
//                        vehicleInfoMap.put("vehicleSize","大");
//                    else if (vehicleSize.equalsIgnoreCase("2"))
//                        vehicleInfoMap.put("vehicleSize","中");
//                    else if (vehicleSize.equalsIgnoreCase("3"))
//                        vehicleInfoMap.put("vehicleSize","小");
//                    else
//                        vehicleInfoMap.put("vehicleSize",null);
//                }

                // 所属单位
                if(vehicleInfoMap.get("useDept") != null && StringUtil.isNotBlank(String.valueOf(vehicleInfoMap.get("useDept")))) {
                    String useDept = String.valueOf(vehicleInfoMap.get("useDept"));
                    vehicleInfoMap.put("DEPT_NAME",useDept);
                    List<Map<String, String>> deptList = deptService.getDeptCDByName(vehicleInfoMap);
                    if (deptList != null && deptList.size() > 0) {
                        Map<String, String> deptMap = deptList.get(0);
                        vehicleInfoMap.put("useDept", deptMap.get("DEPT_CD"));
                    } else {
                        sourceDataMap.put("ERROR_MSG","所属单位填写不正确(参考用户管理)");
                        failedList.add(sourceDataMap);
                        continue;
                    }
                }





                Map<String, Object> paramMap = new HashMap<String, Object>();
                paramMap.put("plateNumber",vehicleInfoMap.get("plateNumber"));
                List<Map<String, Object>>  vehicleList =  this.getVehicleInfoListNoPage(paramMap);
                if (vehicleList != null && vehicleList.size() > 0)
                {
                    vehicleInfoMap.put("id",vehicleList.get(0).get("id"));
                    vehicleInfoMap.put("uploader",userId);
                    vehicleMapper.updateVehicleInfo(vehicleInfoMap);
                    updateList.add(vehicleInfoMap);
                }
                else {
                    vehicleInfoMap.put("creator",userId);
                    vehicleMapper.insertVehicleInfo(vehicleInfoMap);
                    successList.add(vehicleInfoMap);
                }

            }catch (Exception e){
                String errorMsg = e.getMessage();
                if(errorMsg.indexOf("###",10) != -1){
                    errorMsg = errorMsg.substring(0,errorMsg.indexOf("###",10));
                }
                sourceDataMap.put("ERROR_MSG",errorMsg);
                //记录导入失败记录
                failedList.add(sourceDataMap);
            }
        }

        //组装本次导入的结果
        resultMap.put(Constant.IMPORT_HE_SUCCESS,successList);
        resultMap.put(Constant.IMPORT_HE_IGNORE,ignoreList);
        resultMap.put(Constant.IMPORT_HE_FAILED,failedList);
        resultMap.put(Constant.IMPORT_HE_UPDATED,updateList);
        return resultMap;
    }

    public List<Map<String, Object>> getVehicleInfoListNoPage(Map<String, Object> params) {
        return vehicleMapper.getVehicleInfoList(params);
    }
}
