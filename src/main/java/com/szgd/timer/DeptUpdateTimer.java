package com.szgd.timer;

import com.szgd.service.sys.DeptService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


/**
 * 上海门户的用户更新任务
 * （西安十所提供）
 * @author lish
 *
 */
@Component
public class DeptUpdateTimer {
	@Autowired
	DeptService deptService;
	private static Logger logger = Logger.getLogger(DeptUpdateTimer.class);

	/*
	 * 同步部门数据 author by 00
	 */
	/*@Scheduled(cron="0 0 1 * * ?")
	public void synchro(){
		boolean result = deptService.synchro();
		System.out.println(result?"同步部门数据成功!":"同步部门数据失败!");
	}*/

}
