package com.szgd.timer;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import com.szgd.service.gatherEquip.GatherEquipService;
import com.szgd.service.personnel.EnterExitService;
import com.szgd.util.PropertiesUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 人员出入记录同步
 * @author lish
 */
@Component
public class PersonnelEnterExitRecordSynTimer {
	@Autowired
	EnterExitService enterExitService;
	@Autowired
	GatherEquipService gatherEquipService;

	private static Logger logger = Logger.getLogger(PersonnelEnterExitRecordSynTimer.class);
	//同步用户数据 
	//@Scheduled(cron="${PersonnelEnterExitRecordSynCron}")
	public void synEnterExitRecord(){
		String personnelEnterExitRecordSynFlag = PropertiesUtil.get("PersonnelEnterExitRecordSynFlag");
		if (personnelEnterExitRecordSynFlag.equalsIgnoreCase("0"))
		{
			return;
		}

		List<Map<String, Object>>  enterGatherEquipList = gatherEquipService.getPeopleEnterGatherEquipList();
		List<Map<String, Object>>  exitGatherEquipList = gatherEquipService.getPeopleExitGatherEquipList();

/*		if (enterGatherEquipList == null)
		{
			Map<String, Object> paramsMap = new HashMap<>();
			paramsMap.put("type","GATHER_EQUIP_FACE");
			paramsMap.put("passFlag",1);
			enterGatherEquipList = gatherEquipService.getGatherEquipList(paramsMap);
			gatherEquipService.setPeopleEnterGatherEquipList(enterGatherEquipList);
		}*/
		if (enterGatherEquipList.size()==0)
			return;

		for (int i = 0; i < enterGatherEquipList.size(); i++) {//循环同步进入闸机记录
			Map<String, Object> gatherEquipMap = enterGatherEquipList.get(i);
			String projectId = gatherEquipMap.get("projectId").toString();
			String workSiteId = gatherEquipMap.get("workSiteId").toString();
			String siteId = gatherEquipMap.get("siteId").toString();
			String gatherEquipId = gatherEquipMap.get("id").toString();
			String password = gatherEquipMap.get("password").toString();
			int passFlag  = Integer.parseInt(gatherEquipMap.get("passFlag").toString());
			String ip = gatherEquipMap.get("ip").toString();
			boolean result = enterExitService.synEnterExitRecord(password,ip,passFlag,projectId,workSiteId,siteId,gatherEquipId);
			System.out.println(result?"同步人员进出记录成功!":"同步人员进出记录失败!");
		}

		/*if (exitGatherEquipList == null)
		{
			Map<String, Object> paramsMap = new HashMap<>();
			paramsMap.put("type","GATHER_EQUIP_FACE");
			paramsMap.put("passFlag",2);
			exitGatherEquipList = gatherEquipService.getGatherEquipList(paramsMap);
			gatherEquipService.setPeopleExitGatherEquipList(exitGatherEquipList);
		}*/
		if (exitGatherEquipList.size()==0)
			return;

		for (int i = 0; i < exitGatherEquipList.size(); i++) {//循环同步离开闸机记录
			Map<String, Object> gatherEquipMap = exitGatherEquipList.get(i);
			String projectId = gatherEquipMap.get("projectId").toString();
			String workSiteId = gatherEquipMap.get("workSiteId").toString();
			String siteId = gatherEquipMap.get("siteId").toString();
			String gatherEquipId = gatherEquipMap.get("id").toString();
			String password = gatherEquipMap.get("password").toString();
			int passFlag  = Integer.parseInt(gatherEquipMap.get("passFlag").toString());
			String ip = gatherEquipMap.get("ip").toString();
			boolean result = enterExitService.synEnterExitRecord(password,ip,passFlag,projectId,workSiteId,siteId,gatherEquipId);
			System.out.println(result?"同步人员进出记录成功!":"同步人员进出记录失败!");
		}
	}
}
