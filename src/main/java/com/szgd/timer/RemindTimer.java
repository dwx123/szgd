package com.szgd.timer;

import java.text.SimpleDateFormat;
import java.util.*;

import com.szgd.bean.RemindCfg;
import com.szgd.bean.SysDict;
import com.szgd.dao.ecdata.vehicle.VehicleOnSiteSuperviseMapper;
import com.szgd.service.personnel.RemindCfgService;
import com.szgd.service.sys.SysDictService;
import com.szgd.service.vehicle.VehicleEnterExitService;
import com.szgd.service.vehicle.VehicleOnSiteSuperviseService;
import com.szgd.util.SysUtil;
import com.szgd.util.TimeUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.szgd.bean.Remind;
import com.szgd.service.personnel.RemindService;
import com.szgd.util.BusinessName;
import com.szgd.util.PropertiesUtil;


/**
 * 定时取所有需要提醒的信息
 * @author sunc
 *
 */
@Component
@PropertySource(value = "classpath:application.properties")
public class RemindTimer {
	@Autowired
	RemindService remindService;
	@Autowired
	SysDictService sysDictService;
	@Autowired
	RemindCfgService remindCfgService;
	@Autowired
	VehicleEnterExitService vehicleEnterExitService;
	@Autowired
	VehicleOnSiteSuperviseService vehicleOnSiteSuperviseService;
	private static Logger logger = Logger.getLogger(RemindTimer.class);

	private String certificationRemindCount = "";
	private String peoplesRemindCount = "";
	private String peoplesMonthRemindCount = "";
	private String physicalExaminationRemindCount = "";
	private String equipmentChkRemindCount = "";
	private String equipmentInsureRemindCount = "";

	@Scheduled(cron="${CertificationRemindCron}")
	public void addCertificationRemind(){
		//System.out.println("CertificationRemindCron-------------------${CertificationRemindCron}"+ new Date().toString());
		String certificationRemindFlag = PropertiesUtil.get("CertificationRemindFlag");
		if (certificationRemindFlag.equalsIgnoreCase("0"))
		{
			return;
		}
		String nowTime = TimeUtil.formatDate(new Date(),"yyyy-MM-dd HH:mm:ss");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("remindType", BusinessName.ZZTX.getValue());
		params.put("updateTimeStr",nowTime);
		//提醒之前先把之前的提醒记录清掉
		remindService.updateRemindDelFlag(params);
		List<Remind> zzList = remindService.getCertificationRemindInfo(params);
		if(zzList != null && zzList.size() > 0){
			for(Remind zz : zzList){

				zz.setRemindType(BusinessName.ZZTX.getValue());
				zz.setRemindTimeStr(nowTime);
				zz.setUpdateTimeStr(nowTime);
				zz.setCreateTimeStr(nowTime);
				zz.setContent(zz.getField2() + "的《" + zz.getField4() + "》证书将在"+ zz.getField5() + "到期，请及时办理！");
				if(remindService.getRemindDataCount(zz) > 0){
					zz.setDelflag(0L);
					remindService.updateRemindTimer(zz);
				}else{
					remindService.insert(zz);
				}
			}
			certificationRemindCount = String.valueOf(zzList.size());
		}else{
			System.out.println("没有人员资质期限提醒！");
		}
	}
	
	@Scheduled(cron="${PeoplesRemindCron}")
	public void addPeoplesRemind(){
		String peoplesRemindFlag = PropertiesUtil.get("PeoplesRemindFlag");
		if (peoplesRemindFlag.equalsIgnoreCase("0"))
		{
			return;
		}
		String nowTime = TimeUtil.formatDate(new Date(),"yyyy-MM-dd HH:mm:ss");
		List<SysDict> workTypeList = sysDictService.getByParentCode("WORK_TYPE");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("remindType", BusinessName.RSTX.getValue());
		params.put("updateTimeStr",nowTime);
		//提醒之前先把之前的提醒记录清掉
		//remindService.updateRemindDelFlag(params);
		//remindService.deleteRemindByRemindType(BusinessName.RSTX.getValue());
		List<RemindCfg> cfgList = remindCfgService.getRemindCfgData(params);
		for (RemindCfg remindCfg:cfgList) {//每个配置要检查一遍
			String siteIds = remindCfg.getSiteIds();
			String[] workTypeArray = remindCfg.getWorkType().split(",");//已配置的岗位
			String[] siteIdArray = siteIds.split(",");
			String startDateS = TimeUtil.formatDate(new Date(),"yyyy-MM-dd")+" " + remindCfg.getStartTime()+":00";
			String endDateS = TimeUtil.formatDate(new Date(),"yyyy-MM-dd")+" " + remindCfg.getEndTime()+":00";

			String curDay = TimeUtil.formatDate(new Date(),"yyyy-MM-dd HH:mm:ss");//获取当前时间
			if (curDay.compareTo(startDateS) < 0 || curDay.compareTo(endDateS) > 0)//不在时间段内，不做处理
			{
				continue;
			}

			for (String siteId:siteIdArray) {//每个配置下多个站点，每个站点分别检查一遍
				Remind remindParam = new Remind();
				Remind remind = new Remind();
				remindParam.setSiteId(Long.valueOf(siteId));
				remindParam.setConfigureId(remindCfg.getId());
				remindParam.setRemindType(BusinessName.RSTX.getValue());
				List<Remind> remindList = remindService.getRemindList(remindParam);
				if (remindList != null && remindList.size() > 0)
				{
					remind.setId(remindList.get(0).getId());
					int isOk = remindList.get(0).getIsOk();
					if (isOk == 1)//如果提醒已经达到要求了，不作处理
					{
						continue;
					}
				}else//先默认插入提醒信息，后面会更新，默认没达到要求
				{
					remind.setSiteId(Long.valueOf(siteId));
					remind.setConfigureId(remindCfg.getId());
					remind.setRemindType(BusinessName.RSTX.getValue());
					remind.setRemindTimeStr(nowTime);
					remind.setCreateTimeStr(nowTime);
					remindService.insert(remind);
				}
				params.put("siteId",siteId);
				params.put("workTypeArray",workTypeArray);
				params.put("beginPassTime",startDateS);
				params.put("endPassTime",endDateS);

				List<Remind> rsList = remindService.getPeoplesRemindInfo(params);
				boolean requireRemind = false;
				long totalRealPresentNumber = 0;
				String workTypes = null;//当前已在场的岗位
				String requireRemindWorkTypePresentNumber = null; //需要提醒的岗位在场数
				String s1 = "不足岗位：";
				String s2 = "缺少岗位：";
				if(rsList != null && rsList.size() > 0){//当前站点下岗位和其对应的实际在场人数，每个岗位一条记录
					for(Remind rs : rsList){
						String workType =rs.getWorkType();
						String workTypeName =rs.getWorkTypeName();
						if (workTypes == null)
							workTypes = workType;
						else
							workTypes = workTypes+","+workType;
						Long realPresentNumber =rs.getRealPresentNumber();
						if (remindCfg.getPresentNumber() > realPresentNumber)
						{
							requireRemind = true;
							if (requireRemindWorkTypePresentNumber == null)
								requireRemindWorkTypePresentNumber = s1+workTypeName+":"+ realPresentNumber;
							else
								requireRemindWorkTypePresentNumber = requireRemindWorkTypePresentNumber + ", " + workTypeName+":"+ realPresentNumber;
						}
						totalRealPresentNumber = totalRealPresentNumber + realPresentNumber;
					}

					if (workTypes.split(",").length < workTypeArray.length)
					{
						if (requireRemindWorkTypePresentNumber == null)
							requireRemindWorkTypePresentNumber = s2+SysUtil.removeDuplicateArray(workTypeArray,workTypes,workTypeList)+"人";
						else if (requireRemindWorkTypePresentNumber.indexOf(s2) >=0)
							requireRemindWorkTypePresentNumber = requireRemindWorkTypePresentNumber + ", " + SysUtil.removeDuplicateArray(workTypeArray,workTypes,workTypeList)+"人";
						else
							requireRemindWorkTypePresentNumber = requireRemindWorkTypePresentNumber + ", " +s2+ SysUtil.removeDuplicateArray(workTypeArray,workTypes,workTypeList)+"人";
					}
				}else
					requireRemind = true;

				remind.setRemindTimeStr(nowTime);
				remind.setUpdateTimeStr(nowTime);
				remind.setRealPresentNumber(totalRealPresentNumber);

				if (requireRemind)//如果有提醒信息
				{
					if (requireRemindWorkTypePresentNumber == null)
					{
						remind.setContent(s2+remindCfg.getWorkTypeName());
					}else
						remind.setContent(remindCfg.getStartTime()+"到"+remindCfg.getEndTime()+"之间，要求有"+remindCfg.getWorkType().split(",").length+"个岗位每岗位至少"+
							remindCfg.getPresentNumber()+"人在场，"+ requireRemindWorkTypePresentNumber);

				}else
				{
					remind.setContent("已满足要求");
					remind.setIsOk(1);//已达到要求
				}
				remindService.updateRemindTimer(remind);

				peoplesRemindCount = String.valueOf(totalRealPresentNumber);
			}
		}
	}


	@Scheduled(cron="${PeoplesMonthRemindCron}")
	public void addPeoplesMonthRemind(){
		String peoplesMonthRemindFlag = PropertiesUtil.get("PeoplesMonthRemindFlag");
		if (peoplesMonthRemindFlag.equalsIgnoreCase("0"))
		{
			return;
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		// 获取前月的第一天
		Calendar cale = Calendar.getInstance();
		cale.add(Calendar.MONTH, 0);
		cale.set(Calendar.DAY_OF_MONTH, 1);
		String firstday = format.format(cale.getTime());
		// 获取前月的最后一天
		cale = Calendar.getInstance();
		cale.add(Calendar.MONTH, 1);
		cale.set(Calendar.DAY_OF_MONTH, 0);
		String lastday = format.format(cale.getTime());

		List<SysDict> workTypeList = sysDictService.getByParentCode("WORK_TYPE");

		String nowTime = TimeUtil.formatDate(new Date(),"yyyy-MM-dd HH:mm:ss");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("remindType", BusinessName.TSTX.getValue());
		params.put("updateTimeStr",nowTime);
		//提醒之前先把之前的提醒记录清掉
		//remindService.updateRemindDelFlag(params);
		//remindService.deleteRemindByRemindType(BusinessName.RSTX.getValue());
		List<RemindCfg> cfgList = remindCfgService.getRemindCfgData(params);
		for (RemindCfg remindCfg:cfgList) {//每个配置要检查一遍
			String siteIds = remindCfg.getSiteIds();
			String[] workTypeArray = remindCfg.getWorkType().split(",");//已配置的岗位
			String[] siteIdArray = siteIds.split(",");
			String startDateS = TimeUtil.formatDate(remindCfg.getStartDate(),"yyyy-MM-dd");
			if (firstday.compareToIgnoreCase(startDateS) > 0)//如果本月第一天大于开始日期
			{
				startDateS = firstday+" 00:00:01";
			}
			String endDateS = TimeUtil.formatDate(remindCfg.getEndDate(),"yyyy-MM-dd");
			if (lastday.compareToIgnoreCase(endDateS) < 0)//如果本月最后一天小于结束日期
			{
				endDateS = lastday+" 23:59:59";;
			}
			String curDay = TimeUtil.formatDate(new Date(),"yyyy-MM-dd HH:mm:ss");//获取当前时间
			if (curDay.compareTo(startDateS) < 0 || curDay.compareTo(endDateS) > 0)//不在时间段内，不做处理
			{
				continue;
			}
			//计算今天是本月的第几天
			Calendar calendar = Calendar.getInstance();
			Date today = new Date();
			calendar.setTime(today);// 此处可换为具体某一时间
			int monthDay = calendar.get(Calendar.DAY_OF_MONTH);
			if(remindCfg.getPresentDays() > monthDay)//如果配置的天数大于当前本月天数，则不作提醒
			{
				continue;
			}
			for (String siteId:siteIdArray) {//每个配置下多个站点，每个站点分别检查一遍

				Remind remindParam = new Remind();
				Remind remind = new Remind();
				remindParam.setSiteId(Long.valueOf(siteId));
				remindParam.setConfigureId(remindCfg.getId());
				remindParam.setRemindType(BusinessName.TSTX.getValue());
				List<Remind> remindList = remindService.getRemindList(remindParam);
				if (remindList != null && remindList.size() > 0)
				{
					remind.setId(remindList.get(0).getId());
					int isOk = remindList.get(0).getIsOk();
					if (isOk == 1)//如果提醒已经达到要求了，不作处理
					{
						continue;
					}
				}else//先默认插入提醒信息，后面会更新，默认没达到要求
				{
					remind.setSiteId(Long.valueOf(siteId));
					remind.setConfigureId(remindCfg.getId());
					remind.setRemindType(BusinessName.TSTX.getValue());
					remind.setRemindTimeStr(nowTime);
					remind.setCreateTimeStr(nowTime);
					remindService.insert(remind);
				}

				params.put("siteId",siteId);
				params.put("workTypeArray",workTypeArray);
				params.put("beginPassTime",startDateS);
				params.put("endPassTime",endDateS);

				List<Remind> rsList = remindService.getPeoplesMonthRemindInfo(params);
				boolean requireRemind = false;
				long totalRealPresentDays = 0;//总天数
				String workTypes = null;//当前已在场的岗位
				String requireRemindWorkTypePresentDays = null; //需要提醒的岗位在场天数
				String s1 = "不足岗位：";
				String s2 = "缺少岗位：";
				if(rsList != null && rsList.size() > 0){//当前站点下岗位和其对应的实际在场天数，每个岗位一条记录

					for(Remind rs : rsList){
						String workType =rs.getWorkType();
						String workTypeName =rs.getWorkTypeName();
						if (workTypes == null)
							workTypes = workType;
						else
							workTypes = workTypes+","+workType;
						Long realPresentDays =rs.getRealPresentDays();
						if (remindCfg.getPresentDays() > realPresentDays)
						{
							requireRemind = true;
							if (requireRemindWorkTypePresentDays == null)
								requireRemindWorkTypePresentDays = s1+workTypeName+":"+ realPresentDays+"天";
							else
								requireRemindWorkTypePresentDays = requireRemindWorkTypePresentDays + ", " + workTypeName+":"+ realPresentDays+"天";
						}
						totalRealPresentDays = totalRealPresentDays + realPresentDays;
					}


					if (workTypes.split(",").length < workTypeArray.length)
					{
						if (requireRemindWorkTypePresentDays == null)
							requireRemindWorkTypePresentDays = s2+SysUtil.removeDuplicateArray(workTypeArray,workTypes,workTypeList);
						else if (requireRemindWorkTypePresentDays.indexOf(s2) >=0)
							requireRemindWorkTypePresentDays = requireRemindWorkTypePresentDays + ", " + SysUtil.removeDuplicateArray(workTypeArray,workTypes,workTypeList);
						else
							requireRemindWorkTypePresentDays = requireRemindWorkTypePresentDays + ", " +s2 +  SysUtil.removeDuplicateArray(workTypeArray,workTypes,workTypeList);

					}
				}else
					requireRemind = true;


				remind.setRemindTimeStr(nowTime);
				remind.setUpdateTimeStr(nowTime);
				remind.setRealPresentNumber(totalRealPresentDays);
				if (requireRemind)//如果有提醒信息
				{
					if (requireRemindWorkTypePresentDays == null)
					{
						remind.setContent(s2+remindCfg.getWorkTypeName());
					}else
						remind.setContent(startDateS+"到"+endDateS+"之间，要求每月有"+remindCfg.getWorkType().split(",").length+"个岗位每岗位至少"+
							remindCfg.getPresentDays()+"天在场，"+ requireRemindWorkTypePresentDays);

				}else{
					remind.setContent("已满足要求");
					remind.setIsOk(1);//已达到要求
				}
				remindService.updateRemindTimer(remind);
				peoplesMonthRemindCount = String.valueOf(totalRealPresentDays);
			}
		}
	}

	@Scheduled(cron="${VehicleOnSiteSuperviseRemindCron}")
	public void addVehicleOnSiteSuperviseRemind(){
		String vehicleOnSiteSuperviseRemindFlag = PropertiesUtil.get("VehicleOnSiteSuperviseRemindFlag");
		if (vehicleOnSiteSuperviseRemindFlag.equalsIgnoreCase("0"))
		{
			return;
		}

		List<SysDict> workTypeList = sysDictService.getByParentCode("WORK_TYPE");

		String nowTime = TimeUtil.formatDate(new Date(),"yyyy-MM-dd HH:mm:ss");
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, Object> vehicleOnSiteSuperviseMap = new HashMap<String, Object>();
		params.put("remindType", BusinessName.JLPZTX.getValue());
		params.put("updateTimeStr",nowTime);
		//提醒之前先把之前的提醒记录清掉
		//remindService.updateRemindDelFlag(params);
		List<RemindCfg> cfgList = remindCfgService.getRemindCfgData(params);
		for (RemindCfg remindCfg:cfgList) {//每个配置要检查一遍
			String siteIds = remindCfg.getSiteIds();
			String[] siteIdArray = siteIds.split(",");

			String vehicleIds = remindCfg.getVehicleIds();//配置需要监理旁站的车辆
			String[] vehicleIdArray = vehicleIds.split(",");

			String[] workTypeArray = remindCfg.getWorkType().split(",");//已配置的岗位

			String startDateS = TimeUtil.formatDate(remindCfg.getStartDate(),"yyyy-MM-dd")+" 00:00:01";
			String endDateS = TimeUtil.formatDate(remindCfg.getEndDate(),"yyyy-MM-dd")+" 23:59:59";

			String curDay = TimeUtil.formatDate(new Date(),"yyyy-MM-dd HH:mm:ss");//获取当前时间
			if (curDay.compareTo(startDateS) < 0 || curDay.compareTo(endDateS) > 0)//不在时间段内，不做处理
			{
				continue;
			}

			vehicleOnSiteSuperviseMap.put("configureId",remindCfg.getId());
			for (String siteId:siteIdArray) {//每个配置下多个站点，每个站点分别检查一遍
				params.put("vehicleIdArray",vehicleIdArray);
				params.put("siteId",siteId);
				vehicleOnSiteSuperviseMap.put("siteId",siteId);
				List<Map<String, Object>> onSiteSuperviseVehicleEnterList = vehicleEnterExitService.getOnSiteSuperviseVehicleEnterList(params);//监理旁站的车辆的进入记录
				for (Map<String, Object> map:onSiteSuperviseVehicleEnterList) {
					String vehicleId = map.get("vehicleId").toString();
					String plateNumber = map.get("plateNumber").toString();
					String vehicleTypeName = map.get("vehicleTypeName").toString();
					String passTime = map.get("passTime").toString();

					Remind remindParam = new Remind();
					Remind remind = new Remind();
					remindParam.setSiteId(Long.valueOf(siteId));
					remindParam.setConfigureId(remindCfg.getId());
					remindParam.setRemindType(BusinessName.JLPZTX.getValue());
					remindParam.setVehicleId(Long.valueOf(vehicleId));
					remindParam.setPassTimeStr(passTime);
					List<Remind> remindList = remindService.getRemindList(remindParam);
					if (remindList != null && remindList.size() > 0)
					{
						remind.setId(remindList.get(0).getId());
						int isOk = remindList.get(0).getIsOk();
						if (isOk == 1)//如果提醒已经达到要求了，不作处理
						{
							continue;
						}
					}else//先默认插入提醒信息，后面会更新，默认没达到要求
					{
						remind.setSiteId(Long.valueOf(siteId));
						remind.setConfigureId(remindCfg.getId());
						remind.setRemindType(BusinessName.JLPZTX.getValue());
						remind.setRemindTimeStr(nowTime);
						remind.setPassTimeStr(passTime);
						remind.setCreateTimeStr(nowTime);
						remind.setVehicleId(Long.valueOf(vehicleId));
						remindService.insert(remind);
					}

					params.put("vehicleId",vehicleId);
					vehicleOnSiteSuperviseMap.put("vehicleId",vehicleId);
					vehicleOnSiteSuperviseMap.put("passTime",passTime);

					params.put("beginPassTime",passTime);
					params.put("passTime",passTime);
					params.put("workTypeArray",workTypeArray);
					params.put("remindId",remind.getId());
					vehicleOnSiteSuperviseService.deleteVehicleOnSiteSupervise(params);//删除此站点，此车辆，此进入时间点的提醒信息

					List<Remind> rsList = remindService.getVehicleOnSiteSuperviseRemindInfo(params);

					vehicleOnSiteSuperviseMap.put("remindId",remind.getId());
					boolean requireRemind = false;
					String workTypes = null;//当前已在场的岗位
					if(rsList != null && rsList.size() > 0){//当前站点下岗位和其对应的实际在场人数，每个岗位一条记录
						if (rsList.size() < workTypeArray.length)//当前实际在岗的岗位数小于应到场的岗位数
						{
							requireRemind = true;
						}


						for(Remind rs : rsList){//在场的岗位插入表
							String workType =rs.getWorkType();
							if (workTypes == null)
								workTypes = workType;
							else
								workTypes = workTypes+","+workType;
							vehicleOnSiteSuperviseMap.put("workType",workType);
							vehicleOnSiteSuperviseMap.put("presentFlag",1);

							vehicleOnSiteSuperviseService.insertVehicleOnSiteSupervise(vehicleOnSiteSuperviseMap);
						}
					}else
					{
						requireRemind = true;
					}

					remind.setRemindTimeStr(nowTime);
					remind.setUpdateTimeStr(nowTime);
					String lackWorkTypes;
					String lackWorkTypeNames;
					if (requireRemind)//说明有没到场的岗位，生成提醒
					{
						lackWorkTypes = SysUtil.removeDuplicateArray(workTypeArray,workTypes,null);
						for (String lackWorkType:lackWorkTypes.split(",")) {//不在场的岗位插入表
							vehicleOnSiteSuperviseMap.put("workType",lackWorkType);
							vehicleOnSiteSuperviseMap.put("presentFlag",0);
							vehicleOnSiteSuperviseService.insertVehicleOnSiteSupervise(vehicleOnSiteSuperviseMap);
						}
						lackWorkTypeNames = "缺少岗位："+SysUtil.removeDuplicateArray(workTypeArray,workTypes,workTypeList);
						String content = plateNumber+"("+vehicleTypeName+")在"+passTime+"进入工地，要求有"+remindCfg.getWorkType().split(",").length+"个岗位在场；"+lackWorkTypeNames;
						remind.setContent(content);

					}else
					{
						remind.setContent("已满足要求");
						remind.setIsOk(1);//已达到要求
					}
					remindService.updateRemindTimer(remind);
				}
			}
		}
	}

	@Scheduled(cron="${PhysicalExaminationRemindCron}")
	public void addPhysicalExaminationRemind(){
		String physicalExaminationRemindFlag = PropertiesUtil.get("PhysicalExaminationRemindFlag");
		if (physicalExaminationRemindFlag.equalsIgnoreCase("0"))
		{
			return;
		}

		String nowTime = TimeUtil.formatDate(new Date(),"yyyy-MM-dd HH:mm:ss");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("remindType", BusinessName.TJTX.getValue());
		params.put("updateTimeStr",nowTime);
		//提醒之前先把之前的提醒记录清掉
		remindService.updateRemindDelFlag(params);
		List<Remind> tjList = remindService.getExaminationRemindInfo(params);
		if(tjList != null && tjList.size() > 0){
			for(Remind tj : tjList){
				tj.setRemindType(BusinessName.TJTX.getValue());
				tj.setRemindTime(new Date());
				tj.setContent(tj.getField2() + "的上次体检日期为" + tj.getField3() + "，请及时进行每年一次的体检！");
				if(remindService.getRemindDataCount(tj) > 0){
					tj.setDelflag(0L);
					remindService.updateRemindTimer(tj);
				}else{
					remindService.insert(tj);
				}
			}
			physicalExaminationRemindCount = String.valueOf(tjList.size());
		}else{
			System.out.println("没有人员体检提醒！");
		}
	}
	
	@Scheduled(cron="${EquipmentChkRemindCron}")
	public void addEquipmentChkRemind(){
		String equipmentChkRemindFlag = PropertiesUtil.get("EquipmentChkRemindFlag");
		if (equipmentChkRemindFlag.equalsIgnoreCase("0"))
		{
			return;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("remindType", BusinessName.SBJCTX.getValue());
		params.put("updateTime",TimeUtil.formatDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
		//提醒之前先把之前的提醒记录清掉
		remindService.updateRemindDelFlag(params);
		List<Remind> sbjcList = remindService.getEquipmentChkRemindInfo(params);
		if(sbjcList != null && sbjcList.size() > 0){
			for(Remind sbjc : sbjcList){
				sbjc.setRemindType(BusinessName.SBJCTX.getValue());
				sbjc.setRemindTime(new Date());
				sbjc.setContent("编号"+sbjc.getField1() + "的设备上次检测日期为" + sbjc.getField3() + "，请及时进行每年一次的检测！");
				if(remindService.getRemindDataCount(sbjc) > 0){
					sbjc.setDelflag(0L);
					remindService.updateRemindTimer(sbjc);
				}else{
					remindService.insert(sbjc);
				}
			}
			equipmentChkRemindCount = String.valueOf(sbjcList.size());
		}else{
			System.out.println("没有设备检测提醒！");
		}
	}
	
	@Scheduled(cron="${EquipmentInsureRemindCron}")
	public void addEquipmentInsureRemind(){
		String equipmentInsureRemindFlag = PropertiesUtil.get("EquipmentInsureRemindFlag");
		if (equipmentInsureRemindFlag.equalsIgnoreCase("0"))
		{
			return;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("remindType", BusinessName.SBBXTX.getValue());
		params.put("updateTime",TimeUtil.formatDate(new Date(),"yyyy-MM-dd HH:mm:ss"));
		//提醒之前先把之前的提醒记录清掉
		remindService.updateRemindDelFlag(params);
		List<Remind> sbbxList = remindService.getEquipmentInsureRemindInfo(params);
		if(sbbxList != null && sbbxList.size() > 0){
			for(Remind sbbx : sbbxList){
				sbbx.setRemindType(BusinessName.SBBXTX.getValue());
				sbbx.setRemindTime(new Date());
				sbbx.setContent("编号"+sbbx.getField1() + "的设备保险将在"+ sbbx.getField3() + "到期，请及时进行办理！");
				if(remindService.getRemindDataCount(sbbx) > 0){
					sbbx.setDelflag(0L);
					remindService.updateRemindTimer(sbbx);
				}else{
					remindService.insert(sbbx);
				}
			}
			equipmentInsureRemindCount = String.valueOf(sbbxList.size());
		}else{
			System.out.println("没有设备保险提醒！");
		}
	}


	public String getCertificationRemindCount() {
		return certificationRemindCount;
	}

	public void setCertificationRemindCount(String certificationRemindCount) {
		this.certificationRemindCount = certificationRemindCount;
	}

	public String getPeoplesRemindCount() {
		return peoplesRemindCount;
	}

	public void setPeoplesRemindCount(String peoplesRemindCount) {
		this.peoplesRemindCount = peoplesRemindCount;
	}

	public String getPhysicalExaminationRemindCount() {
		return physicalExaminationRemindCount;
	}

	public void setPhysicalExaminationRemindCount(String physicalExaminationRemindCount) {
		this.physicalExaminationRemindCount = physicalExaminationRemindCount;
	}

	public String getEquipmentChkRemindCount() {
		return equipmentChkRemindCount;
	}

	public void setEquipmentChkRemindCount(String equipmentChkRemindCount) {
		this.equipmentChkRemindCount = equipmentChkRemindCount;
	}

	public String getEquipmentInsureRemindCount() {
		return equipmentInsureRemindCount;
	}

	public void setEquipmentInsureRemindCount(String equipmentInsureRemindCount) {
		this.equipmentInsureRemindCount = equipmentInsureRemindCount;
	}

	public String getPeoplesMonthRemindCount() {
		return peoplesMonthRemindCount;
	}

	public void setPeoplesMonthRemindCount(String peoplesMonthRemindCount) {
		this.peoplesMonthRemindCount = peoplesMonthRemindCount;
	}
}
