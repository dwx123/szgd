package com.szgd.timer;

import com.szgd.service.sys.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 上海门户的用户更新任务
 * （西安十所提供）
 * @author lish
 */
@Component
public class UserUpdateTimer {
	@Autowired
	UserService userService;
	private static Logger logger = Logger.getLogger(UserUpdateTimer.class);
	//同步用户数据 
	@Scheduled(cron="0 0 2 * * ?")
	public void synchro(){
		/*boolean result = userService.synchro();
		System.out.println(result?"同步用户数据成功!":"同步用户数据失败!");*/
	}
}
