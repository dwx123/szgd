package com.szgd.timer;

import com.szgd.bean.Remind;
import com.szgd.service.inf.EnrollDataService;
import com.szgd.service.inf.WorkerDataService;
import com.szgd.service.personnel.EnterExitService;
import com.szgd.service.personnel.PersonnelInfoService;
import com.szgd.util.BusinessName;
import com.szgd.util.FileUtil;
import com.szgd.util.PropertiesUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@PropertySource(value = "classpath:application.properties")
public class WorkerTimer {
    @Autowired
    PersonnelInfoService personnelInfoService;
    @Autowired
    WorkerDataService workerDataService;
    @Autowired
    EnrollDataService enrollDataService;
    @Autowired
    EnterExitService enterExitService;

    private static Logger logger = Logger.getLogger(WorkerTimer.class);

    @Scheduled(cron="${ZjjSynCron}")
    @Transactional(rollbackFor = Exception.class)
    public void syn(){
        String ZjjSynFlag = PropertiesUtil.get("ZjjSynFlag");
        if (ZjjSynFlag.equalsIgnoreCase("0"))
        {
            return;
        }
        String zjjBidNos = PropertiesUtil.get("ZjjBidNos");//7,10,11:2,11:1-3
        if (zjjBidNos.length() == 0)
        {
            return;
        }
        System.out.println("开始同步需要上报住建局的人员----------------");
        String[]  zjjBidNoArray  = zjjBidNos.split(",");//各标段逗号分隔
        for (int i = 0; i < zjjBidNoArray.length; i++) {
            Map<String,Object> param = new HashMap<>();
            String bidNo_SiteId = zjjBidNoArray[i];//7或11:2或11:1-3
            String[] bidNoArray = bidNo_SiteId.split(":");
            String bidNo = bidNoArray[0];
            param.put("bidNo",bidNoArray[0]);
            if (bidNoArray.length == 2)
            {
                String[] siteIdArray = bidNoArray[1].split("-");
                bidNo = bidNo+"_"+ siteIdArray[0];//表名后缀，标段加第一个站点，
                param.put("siteIdArray",siteIdArray);
            }
            System.out.println("开始同步需要上报住建局标段"+bidNo+"的人员----------------");
            String workerTableName = "WORKERDATA"+bidNo;
            String enrollTableName = "ENROLLDATA"+bidNo;
            param.put("workerTableName",workerTableName);
            param.put("enrollTableName",enrollTableName);
            List<Map<String,Object>> workerList = workerDataService.getWorkerList(param);
                    /*for (int i = 0; i < workerList.size(); i++) {
                        byte[] b = (byte[])workerList.get(i).get("img");
                        String identityId = workerList.get(i).get("identityId").toString();
                        String filename = identityId + ".jpg";
                        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                        String date = df.format(new Date());
                        try {
                            new FileUtil().uploadFile(b,filename,date,PropertiesUtil.get("uploadPath"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }*/

            param.put("workerList",workerList);
            personnelInfoService.updateWorker(param);
            logger.info("开始同步标段"+bidNo+"的考勤信息-------------------");
            System.out.println("开始同步标段"+bidNo+"的考勤信息-------------------");

            List<Map<String,Object>> list = enterExitService.getEnterExitFromList(param);
            //Map<String,Object> param2 = new HashMap<>();
            //param2.put("list",list);
            param.put("list",list);
            if(list != null && list.size() > 0){
                enrollDataService.insertEnrollData(param);
            }
            logger.info("结束同步标段"+bidNo+"的考勤信息-------------------");
        }


    }

}
