package com.szgd.util;

import java.util.Date;
import java.util.UUID;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;





public class AEncryptHelper {

	
	public static Md5PasswordEncoder encoder = new Md5PasswordEncoder();
	public static String defaultPass = "123456";//系统默认密码
	public static String encrypt(String str,String salt) {
		if (str == null) {
			return null;
		}
		return encoder.encodePassword(str,salt);
	}

	public static void main(String[] args) {
		
		System.out.println(encrypt("123456", "admin"));
		System.out.println(TimeUtil.getCurrentTime().getTime());
		System.out.println(TimeUtil.formatTime(new Date(1440748979612L)));
		System.out.println(UUID.randomUUID());
	}
	 
	
	
	
}
