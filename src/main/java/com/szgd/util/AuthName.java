package com.szgd.util;

/**
 * 过滤数据权限时用到的变量名
 * @author Administrator
 *
 */
public enum AuthName {
	
	jfsq("jfsq","经费申请过滤权限相关")
	;
	private AuthName (String value, String text) {
		this.value = value;
		this.text = text;
	}

	private String text;
	
	private String value;

	public String getText (){
		return this.text;
	}
	public String getValue (){
		return this.value;
	}
}
