package com.szgd.util;

/**
 * 流程key和名字定义
 * @author Administrator
 *
 */
public enum BusinessName {
	
	ZZTX("APTITUDE_REMIND","资质提醒"),
	RSTX("PEOPLES_REMIND","人数提醒"),
	TSTX("PEOPLES_MONTH_REMIND","天数提醒"),
	TJTX("PHYSICAL_EXAMINATION_REMIND","体检提醒"),
	SBJCTX("EQUIPMENT_CHECK_REMIND","设备检测提醒"),
	SBBXTX("EQUIPMENT_INSURANCE_REMIND","设备保险提醒"),
	XMJDTX("PROJECT_SCHEDULE_REMIND","项目进度提醒"),
	JLPZTX("VEHICLE_SUPERVISE_REMIND","监理旁站提醒"),
	TXCODE("REMIND_TYPE","字典提醒code"),
	TX_DAY_CODE("REMIND_DAY_TYPE","字典提醒天数code"),
	MODAL_XZ("MODAL_XZ","modal页面新增操作"),
	MODAL_XG("MODAL_XG","modal页面修改操作"),
	MODAL_CX("MODAL_CX","modal页面查看操作"),
	GLRYLX("ROLE_ADMIN","管理人员"),
	USER_FILTER("USER_FILTER","数据权限-根据用户过滤"),
	DEPT_FILTER("DEPT_FILTER","数据权限-根据部门过滤"),
	SITE_FILTER("SITE_FILTER","数据权限-根据站点过滤"),
	BID_FILTER("BID_FILTER","数据权限-根据标段过滤（配置的站点所属标段）"),
	PERSON_FILTER("PERSON_FILTER","数据权限-根据人员过滤（间接的标段过滤）"),
	APP_SITE_FILTER("APP_SITE_FILTER","数据权限-根据站点过滤(app专用)"),
	DEFAULT_ROLE("default","默认角色"),
	hidden_danger_approver("hidden_danger_approver","隐患整改人"),
	hidden_danger_confirmer("hidden_danger_confirmer","隐患确认人"),
	app_dzltsjs("app_dzltsjs","吊装令通知人"),
	TZLX_YH("1","通知类型-隐患"),
	TZLX_DZ("2","通知类型-吊装令"),
	YH_REPORT("REPORT","隐患上报"),
	YH_RECTIFY("RECTIFY","隐患整改"),
	TZ_MC_YHZG("隐患整改","隐患整改"),
	TZ_MC_YHQR("隐患确认","隐患确认"),
	TZ_MC_YH("隐患","隐患"),
	TZ_MC_DZL("吊装令","吊装令"),
	TZ_MC_REMIND("人员到场提醒","人员到场提醒"),
	TZ_MC_MONTH_REMIND("人员进出工地次数提醒","人员进出工地次数提醒"),
	TZ_MC_SUPERVISION_REMIND("监理旁站提醒","监理旁站提醒"),
	OK("1","OK"),
	NG("0","NG")
	
	;
	private BusinessName (String value, String text) {
		this.value = value;
		this.text = text;
	}

	private String text;
	
	private String value;

	public String getText (){
		return this.text;
	}
	public String getValue (){
		return this.value;
	}
}
