package com.szgd.util;

public class Constant {
    /**
     * 权限使用
     */
    public static final String VALIDATE_CODE_NAME = "random";

    /**
     * 导入核销数据成功的记录所使用的key
     */
    public static final String IMPORT_HE_SUCCESS = "SUCCESS";

    /**
     * 导入核销数据忽略的记录所使用的key
     */
    public static final String IMPORT_HE_IGNORE = "IGNORE";

    /**
     * 导入核销数据失败记录所使用的key
     */
    public static final String IMPORT_HE_FAILED = "FAILED";

    /**
     * 记录更新的key
     */
    public static final String IMPORT_HE_UPDATED = "UPDATE";

    /**
     * 系统参数配置文件
     */
    public static final String APP_PROPERTIES_FILE = "/application.properties";

    /**
     * 状态-草稿
     */
    public static final String STATUS_DRAFT = "DRAFT";
    /**
     * 状态-审核中
     */
    public static final String STATUS_REVIEWING = "REVIEWING";
    /**
     * 状态-审批通过
     */
    public static final String STATUS_AGREED = "AGREED";
    /**
     * 状态-审批拒绝
     */
    public static final String STATUS_REJECTED = "REJECTED";
    /**
     * 装备批次表数据来源-手工录入
     */
    public static final String ZB_BATCH_SOURCE_HAND = "HAND";
    /**
     * 装备批次表数据来源-批量导入
     */
    public static final String ZB_BATCH_SOURCE_EXCEL = "EXCEL";

}


