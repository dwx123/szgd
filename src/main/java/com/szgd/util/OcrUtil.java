/*package com.szgd.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import com.asprise.ocr.Ocr;


public class OcrUtil {
    public static String ocrImage(byte[] imgs)
    {
        getFile(imgs,"c:\\img","orcimg.jpg");
        String ocrResult = "";
        try{
            ITesseract instance = new Tesseract();
            instance.setDatapath("C:\\Program Files (x86)\\Tesseract-OCR\\tessdata");
            //instance.setLanguage("chi_sim");
            File imgDir = new File("c:\\img\\orcimg.jpg");
            //long startTime = System.currentTimeMillis();
            ocrResult = instance.doOCR(imgDir);
            System.out.println("ocrResult:"+ocrResult);
        }catch (TesseractException e){
            e.printStackTrace();
        }
        return ocrResult;
    }
    
    public static String getCode(byte[] imgs) throws IOException, URISyntaxException
    {
    	getFile(imgs,"c:\\img","orcimg.jpg");
    	File filepicF=new File("c:\\img\\orcimg.jpg");
		Ocr.setUp(); // one time setup
		Ocr ocr = new Ocr(); // create a new OCR engine
		ocr.startEngine("eng", Ocr.SPEED_FASTEST); // English
		String s = ocr.recognize(new File[] {filepicF},Ocr.RECOGNIZE_TYPE_TEXT, Ocr.OUTPUT_FORMAT_PLAINTEXT);
		System.out.println("Result: " + s);
		System.out.println("图片文字为:" + s.replace(",", "").replace("i", "1").replace(" ", "").replace("'", "").replace("o", "0").replace("O", "0").replace("g", "6").replace("B", "8").replace("s", "5").replace("z", "2"));
		// ocr more images here ...
		ocr.stopEngine();
		return s;
    }
    public static void main(String[] args) throws Exception {
        String path = "D:\\img\\1.jpg";

        String ocrResult = "";
        try{
            ITesseract instance = new Tesseract();
            instance.setDatapath("C:\\Program Files (x86)\\Tesseract-OCR\\tessdata");
            //instance.setLanguage("chi_sim");
            File imgDir = new File(path);
            //long startTime = System.currentTimeMillis();
            ocrResult = instance.doOCR(imgDir);
            System.out.println("ocrResult:"+ocrResult);
        }catch (TesseractException e){
            e.printStackTrace();
        }


        String dir = "D:\\img";
        File imageFile = new File(path);
        ITesseract instance = new Tesseract();
        //图片二值化，增加识别率
        BufferedImage grayImage = null;
        try {
            grayImage = ImageHelper.convertImageToBinary(ImageIO.read(imageFile));
        } catch (IOException e2) {

            e2.printStackTrace();
        }
        try {
            ImageIO.write(grayImage, "png", new File(dir, "vc1.png"));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        String path1 = dir + "/vc1.png";

        File imageFile1 = new File(path1);
        String result = null;
        try {
            result = instance.doOCR(imageFile1);
        } catch (TesseractException e1) {
            e1.printStackTrace();
        }
        result=result.replaceAll("[^a-z^A-Z^0-9]", "");

    }

    *//**
     * 根据byte数组，生成文件
     *//*
    public static void getFile(byte[] bfile, String filePath,String fileName) {
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        File file = null;
        try {
            File dir = new File(filePath);
            if(!dir.exists()&&dir.isDirectory()){//判断文件目录是否存在
                dir.mkdirs();
            }
            file = new File(filePath+"\\"+fileName);
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bfile);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
*/