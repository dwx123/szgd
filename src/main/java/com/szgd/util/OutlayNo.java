package com.szgd.util;

import java.util.Date;
import java.util.Random;

public final class OutlayNo {
	private OutlayNo(){}
	
	public static final synchronized String getNo(){
		return String.valueOf("SQ"+new Date().getTime()+""+(new Random().nextInt(9999-1000+1)+1000));
	}
	
	public static final synchronized String getHzNo(){
		return String.valueOf("HZ"+new Date().getTime()+""+(new Random().nextInt(9999-1000+1)+1000));
	}
	
	public static final synchronized String getZbwxNo(){
		return String.valueOf("WX"+new Date().getTime()+""+(new Random().nextInt(9999-1000+1)+1000));
	}
	
	public static final synchronized String getZbwzwNo(){
		return String.valueOf("WZW"+new Date().getTime()+""+(new Random().nextInt(9999-1000+1)+1000));
	}
	
	public static final synchronized String getBfNo(String dept){
		return String.valueOf(dept+DateConverter.dateToString(new Date()));
	}
}
