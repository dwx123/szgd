/**
 * 
 */
package com.szgd.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author ecdata04
 *
 */
public class PropertiesConfigurer extends PropertyPlaceholderConfigurer {

	private static Map<String,String> propertiesMap;
	
	@Override
	protected void processProperties(
			ConfigurableListableBeanFactory beanFactoryToProcess,
			Properties props) throws BeansException {
		super.processProperties(beanFactoryToProcess, props);
		propertiesMap = new HashMap<String,String>();
		for(Object key : props.keySet()){
			String keyStr = key.toString();
			propertiesMap.put(keyStr, props.getProperty(keyStr));
		}		
	}
	
	public static String getProperty(String name){
		return propertiesMap.get(name);
	}
	
	
}
