package com.szgd.util;

public class RequestHelp {
	/**
	 * 根据Request获取客户端IP
	 * （增强了客户端使用代理工具后获取客户真实IP的方式）
	 * @param req
	 * @return
	 */
	public static String getRemoteHost(javax.servlet.http.HttpServletRequest req){
		String ip = req.getHeader("x-forwarded-for");
		if(null == ip || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
			ip = req.getHeader("Proxy-Client-IP");
		}
		if(null == ip || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
			ip = req.getHeader("WL-Proxy-Client-IP");
		}
		if(null == ip || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
			ip = req.getRemoteAddr();
		}
		return ip.equals("0:0:0:0:0:0:0:1")?"127.0.0.1":ip;
	}
}
