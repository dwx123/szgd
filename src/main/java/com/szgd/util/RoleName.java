package com.szgd.util;

/**
 * 过滤数据权限时用到的角色名
 * @author Administrator
 *
 */
public enum RoleName {
	
	bmfzr("bmfzr","支队领导"),
	zbc("zbc","装备处领导"),
	jck("jck","计财科"),
	sqr("sqr","计财科"),
	bf_zdld("bf-zdld","报废支队领导")
	;
	private RoleName (String value, String text) {
		this.value = value;
		this.text = text;
	}

	private String text;
	
	private String value;

	public String getText (){
		return this.text;
	}
	public String getValue (){
		return this.value;
	}
}
