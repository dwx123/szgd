package com.szgd.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;  
  
@Target(ElementType.METHOD)  
@Retention(RetentionPolicy.RUNTIME)  
/**
 * 此类只用在要清session的方法中
 * @author Administrator
 *
 */
public @interface SetLogBefore {  
    
    String opt() default "";  
  
}   