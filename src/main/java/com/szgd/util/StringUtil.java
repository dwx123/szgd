package com.szgd.util;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.io.BufferedReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.sql.Clob;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StringUtil {
	public final static Boolean isBlank(String para) {
		return para == null || para.equals("") || para.equals("null");
	}

	public final static Boolean isNotBlank(String para) {
		return !isBlank(para);
	}

	public static String escape(String source) {
		StringBuilder resultBuilder = new StringBuilder(source.length() * 6);
		char[] sourceChar = source.toCharArray();
		for (char c : sourceChar) {
			if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z')
					|| (c >= 'A' && c <= 'Z')) {
				resultBuilder.append(c);
			} else if (c < 256) {
				resultBuilder.append("%");
				if (c < 16) {
					resultBuilder.append("0");
				}
				resultBuilder.append(Integer.toString(c, 16));
			} else {
				resultBuilder.append("%u");
				resultBuilder.append(Integer.toString(c, 16));
			}
		}
		return resultBuilder.toString();
	}

	public static String unescape(String source) {
		StringBuilder resultBuilder = new StringBuilder(source.length());
		int lastPos = 0, pos = 0;
		int sourceLength = source.length();
		char ch;
		while (lastPos < sourceLength) {
			pos = source.indexOf("%", lastPos);
			if (pos == lastPos) {
				if (source.charAt(pos + 1) == 'u') {
					ch = (char) Integer.parseInt(
							source.substring(pos + 2, pos + 6), 16);
					resultBuilder.append(ch);
					lastPos = pos + 6;
				} else {
					ch = (char) Integer.parseInt(
							source.substring(pos + 1, pos + 3), 16);
					resultBuilder.append(ch);
					lastPos = pos + 3;
				}
			} else {
				if (pos == -1) {
					resultBuilder.append(source.substring(lastPos));
					lastPos = source.length();
				} else {
					resultBuilder.append(source.substring(lastPos, pos));
					lastPos = pos;
				}
			}
		}
		return resultBuilder.toString();
	}

	public static String unescape(String source, String startTag) {
		StringBuilder resultBuilder = new StringBuilder(source.length());
		int lastPos = 0, pos = 0;
		int sourceLength = source.length();
		char ch;
		while (lastPos < sourceLength) {
			pos = source.indexOf(startTag, lastPos);
			if (pos == lastPos) {
				if (source.charAt(pos + 1) == 'u') {
					ch = (char) Integer.parseInt(
							source.substring(pos + 2, pos + 6), 16);
					resultBuilder.append(ch);
					lastPos = pos + 6;
				} else {
					ch = (char) Integer.parseInt(
							source.substring(pos + 1, pos + 3), 16);
					resultBuilder.append(ch);
					lastPos = pos + 3;
				}
			} else {
				if (pos == -1) {
					resultBuilder.append(source.substring(lastPos));
					lastPos = source.length();
				} else {
					resultBuilder.append(source.substring(lastPos, pos));
					lastPos = pos;
				}
			}
		}
		return resultBuilder.toString();
	}

	public static final String xmlToJson(String xml) {
		try {

			Document document = DocumentHelper.parseText(xml);
			Element root = document.getRootElement();
			StringBuilder stringCache = new StringBuilder(xml.length() * 2);

			analyseElement(root, stringCache);

			return stringCache.toString();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return "{}";
	}

	private static final void analyseElement(Element element,
			StringBuilder stringCache) {
		List<Element> child = element.elements();
		Set<String> nameSet = new HashSet<String>();
		for (Element chi : child) {
			nameSet.add(chi.getName());
		}
		String[] nameArray = new String[0];
		nameArray = nameSet.toArray(nameArray);
		int nameSize = nameSet.size();
		String name;
		int m = 0;
		StringBuilder tmpCach = new StringBuilder(512);
		boolean use = false;
		for (int k = 0; k < nameSize; k++) {
			name = nameArray[k];
			List<Element> typeList = element.elements(name);
			int typeSize = typeList.size();
			if (typeSize > 1) {
				tmpCach.append("[");
				use = true;
			} else {
				tmpCach.append(name).append(":");
			}
			if (typeSize > 1) {
				int t = 0;
				for (Element type : typeList) {
					tmpCach.append("{");
					if (type.elements().size() == 0) {
						tmpCach.append(name).append(":'")
								.append(type.getText()).append("'");
					} else {
						tmpCach.append(name).append(":");
						analyseElement(type, tmpCach);
					}
					tmpCach.append("}");
					t++;
					if (t < typeSize) {
						tmpCach.append(",");
					}
				}
			} else {
				Element type = typeList.get(0);
				if (type.elements().size() == 0) {
					tmpCach.append("'").append(type.getText()).append("'");
				} else {
					analyseElement(type, tmpCach);
				}
			}
			m++;
			if (m < nameSize) {
				tmpCach.append(",");
			}
			if (typeSize > 1) {
				tmpCach.append("]");
			}
		}
		if (use) {
			stringCache.append(tmpCach);
		} else {
			stringCache.append("{").append(tmpCach).append("}");
		}

	}

	public static boolean isNumeric(String str) {
		for (int i = str.length(); --i >= 0;) {
			int chr = str.charAt(i);
			if (chr < 48 || chr > 57)
				return false;
		}
		return true;
	}

	public static String arrayToStr(String[] str) {
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < str.length; i++) {
			sb.append(str[i]);
		}
		return sb.toString();
	}

	/**
	 * 格式化数字为千分位显示；
	 * 
	 * @param text
	 *            ；
	 * @return
	 */
	public static String fmtMicrometer(String text, boolean needDecimal) {
		if(StringUtil.isBlank(text)){
			return "0";
		}
		DecimalFormat df = null;
		if (text.indexOf(".") > 0 && needDecimal) {
			
			df = new DecimalFormat("###,##0.00");
			
			/*if (text.length() - text.indexOf(".") - 1 == 0) {
				df = new DecimalFormat(format);
			} else if (text.length() - text.indexOf(".") - 1 == 1) {
				df = new DecimalFormat("###,##0.00");
			} else {
				df = new DecimalFormat("###,##0.00");
			}*/
		} else {
			df = new DecimalFormat("###,##0");
		}
		double number = 0.0;
		try {
			number = Double.parseDouble(text);
		} catch (Exception e) {
			number = 0.0;
		}
		return df.format(number);
	}
	public static String fmtMicrometer(String text) {
		return fmtMicrometer(text,true);
	}
	public static String fmtMicrometer(double d) {
		return fmtMicrometer(String.valueOf(d),true);
	}
	public static String fmtMicrometer(double d, boolean needDecimal) {
		return fmtMicrometer(String.valueOf(d),needDecimal);
	}
	public static String fmtMicrometer(BigDecimal b, boolean needDecimal) {
		if(b==null){
			return "0";
		}
		return fmtMicrometer(String.valueOf(b.doubleValue()),needDecimal);
	}
	public static String ClobToString(Clob clob)throws Exception {
		String reString="";
		Reader is=clob.getCharacterStream();
		BufferedReader br=new BufferedReader(is);
		String s=br.readLine();
		StringBuffer sb=new StringBuffer();
		while(s!=null){
			sb.append(s);
			s=br.readLine();
		}
		reString=sb.toString();
		return reString;
	}

	/**
	 * 字符串s1和s2拼接
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static  String splitJoint(String s1,String s2)
	{
		if (s1.length()==0)
			s1 = s2;
		else
			s1 = s1 + " ； " + s2;
		return s1;
	}
	public static void main(String[] args) {
		double d = 10000000.0;
		
		System.out.println(fmtMicrometer(d));
		System.out.println(System.currentTimeMillis());
	}
}