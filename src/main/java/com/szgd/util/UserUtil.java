package com.szgd.util;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.userdetails.User;


/**
 * 用户工具类
 *
 * @author HenryYan
 */
public class UserUtil {

    public static final String USER = "user";
    public static final String USERINFOMATION = "userInfomation";

    /**
     * 设置用户到session
     *
     * @param session
     * @param user
     */
    public static void saveUserToSession(HttpSession session, String user) {
        session.setAttribute(USER, user);
    }

    public static void saveObjectToSession(HttpSession session, String key,Object value) {
        session.setAttribute(key, value);
    }


    public static User getUserInfomationBeanFromSession(HttpSession session) {
        if (session == null)
            return  null;
        Object attribute = session.getAttribute(USERINFOMATION);
        System.out.println(session.getId());
        User u  = (User)attribute;
        return u;
    }

}
