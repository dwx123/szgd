package com.szgd.util;

import java.net.URL;
import javax.xml.namespace.QName;

import org.apache.axis.client.Service;
import org.apache.axis.client.Call;
import org.apache.log4j.Logger;


/**
 * WebService辅助类（axis）
 * @author lish
 *
 */
public class WebServiceUtil {
	private static Logger logger = Logger.getLogger(WebServiceUtil.class);
	
	/**
	 * 调用WS接口
	 * @param wsurl    WS访问路径
	 * @param method   方法名
	 * @param args     参数
	 * @return
	 */
	public static Object callWs(String wsurl,String method,Object...args){
		Object result = null;
		try{
			Service service = new Service();
			Call call = (Call) service.createCall();
			QName opraname = new QName(wsurl,method);
			call.setTargetEndpointAddress(new URL(wsurl));
			call.setOperationName(opraname);
			result = call.invoke(args);
		}catch(Exception e){
			logger.error("调用WS接口异常！",e);
		}
		return result;
	}

}

