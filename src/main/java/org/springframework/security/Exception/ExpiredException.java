package org.springframework.security.Exception;

import org.springframework.security.core.AuthenticationException;

public class ExpiredException  extends AuthenticationException {
    public ExpiredException(String msg) {
        super(msg);
    }
    public ExpiredException(String msg, Throwable t) {
        super(msg, t);
    }
}
