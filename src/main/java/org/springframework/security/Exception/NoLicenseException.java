package org.springframework.security.Exception;

import org.springframework.security.core.AuthenticationException;

public class NoLicenseException extends AuthenticationException {
    public NoLicenseException(String msg) {
        super(msg);
    }
    public NoLicenseException(String msg, Throwable t) {
        super(msg, t);
    }
}
