package org.springframework.security.Exception;

import org.springframework.security.core.AuthenticationException;

public class NoLocalMachineCodeException extends AuthenticationException {
    public NoLocalMachineCodeException(String msg) {
        super(msg);
    }

    public NoLocalMachineCodeException(String msg, Throwable t) {
        super(msg, t);
    }
}
