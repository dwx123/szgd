<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>苏州轨道6号线智能管理APP下载</title>
    <link type="text/css" href="${ctx}/assets/bootstrap/css/bootstrap.css" rel="Stylesheet">
    <link type="text/css" href="${ctx}/assets/bootstrap-extension/font-awesome/css/font-awesome.min.css" rel="Stylesheet">
    <link type="text/css" href="${ctx}/assets/css/login.css" rel="Stylesheet">
    <script src="${ctx}/assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="${ctx}/assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!--[if lt IE 9]>
    <link type="text/css" href="${ctx}/assets/css/ielayout.css" rel="Stylesheet">
    <script src="${ctx}/assets/js/respond.min.js" type="text/javascript"></script>
    <script src="${ctx}/assets/js/html5shiv.js" type="text/javascript"></script>
    <![endif]-->
</head>

<body>
<div class="loginMain">
    <div class="loginpanel">
        <div class="titlepanel" style="margin-top: 100px;">
            <span style="font-size: 20px;">苏州轨道6号线智能管理APP</span>
        </div>
        <div class="contentPanel" style="font-size: 22px;font-weight: lighter;">
            <%--APK名称：<span id="apkName">${apkName}</span>--%>
            <%--<br>--%>
            <%--<br>--%>
            版本号：<span id="version">${version}</span>
        </div>
        <button type="button" onclick="downloadApk()" class="btn btn-primary" style="margin-top: 35px;"><i class="fa fa-download"></i> APP下载</button>

        <a href="<%=request.getContextPath() %>/app/toAppHistory" class="btn btn-primary" style="margin-top: 35px;margin-left: 10px;"><i class="fa fa-search"></i> 查看发布履历</a>

    </div>
</div>
<script type="text/javascript">

    $(function() {
        var apkName = "${apkName}";
        var version = "${version}";
        $("#apkName").val(apkName);
        $("#version").val(version);
    });
    function downloadApk() {
        var name = "${apkName}";
        if(name=="null"||name==null||name==""){
            alert("暂无文件");
            return;
        }
        var d = encodeURI(encodeURI(name,"utf-8"),"utf-8")
        window.open('<%=request.getContextPath()%>/app/downloadUserFileApp?fileName=' + d + '')
    }
</script>
</body>
</html>
