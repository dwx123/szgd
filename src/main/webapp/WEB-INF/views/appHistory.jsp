<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>苏州轨道6号线智能管理APP发布履历</title>
    <link type="text/css" href="${ctx}/assets/bootstrap/css/bootstrap.css" rel="Stylesheet">
    <link type="text/css" href="${ctx}/assets/bootstrap-extension/font-awesome/css/font-awesome.min.css" rel="Stylesheet">
    <script src="${ctx}/assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="${ctx}/assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!--[if lt IE 9]>
    <link type="text/css" href="${ctx}/assets/css/ielayout.css" rel="Stylesheet">
    <script src="${ctx}/assets/js/respond.min.js" type="text/javascript"></script>
    <script src="${ctx}/assets/js/html5shiv.js" type="text/javascript"></script>
    <![endif]-->
</head>

<body>

    <div style="color:#000">
        <div style="margin-top: 3%;text-align: center;">
            <span style="font-size: 20px;font-weight:bold;">APP发布履历</span>
        </div>
        <div style="font-size: 18px;font-weight: lighter;">
            <div style="border-bottom: 1px solid #eee;padding: 25px;">
                <div>2019年10月30日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版本1.0</div>
                <div style="margin-top: 10px;">APP上线试运行。</div>
            </div>

            <div style="border-bottom: 1px solid #eee;padding: 25px;">
                <div>2019年11月06日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版本1.01</div>
                <div style="margin-top: 10px;display: inline;">
                    <div style="margin-top: 5px;">
                        1、修正登录问题。
                    </div>
                    <div style="margin-top: 5px;">
                        2、优化列表加载速度。
                    </div>
                    <div style="margin-top: 5px;">
                        3、现场视频画面增加返回按钮。
                    </div>
                    <div style="margin-top: 5px;">
                        4、增加页面切换时动态效果。
                    </div>
                </div>
            </div>

            <div style="border-bottom: 1px solid #eee;padding: 25px;">
                <div>2019年11月21日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版本1.02</div>
                <div style="margin-top: 10px;display: inline;">
                    <div style="margin-top: 5px;">
                        1、优化列表加载速度。
                    </div>
                    <div style="margin-top: 5px;">
                        2、增加盾构机及基坑监测功能。
                    </div>
                    <div style="margin-top: 5px;">
                        3、增加图片预览放大功能。
                    </div>
                </div>
            </div>

            <div style="border-bottom: 1px solid #eee;padding: 25px;">
                <div>2019年11月29日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版本1.03</div>
                <div style="margin-top: 10px;display: inline;">
                    <div style="margin-top: 5px;">
                        1、优化提交按钮。
                    </div>
                    <div style="margin-top: 5px;">
                        2、更改站点、标段排序。
                    </div>
                    <div style="margin-top: 5px;">
                        3、修复一些BUG。
                    </div>
                </div>
            </div>

            <div style="border-bottom: 1px solid #eee;padding: 25px;">
                <div>2020年03月09日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版本1.04</div>
                <div style="margin-top: 10px;display: inline;">
                    <div style="margin-top: 5px;">
                        1、增加权限提示。
                    </div>
                    <div style="margin-top: 5px;">
                        2、隐患上报主题名称可重复。
                    </div>
                    <div style="margin-top: 5px;">
                        3、支持通过公网IP接入摄像头画面。
                    </div>
                    <div style="margin-top: 5px;">
                        4、以及其它一些优化工作。
                    </div>
                </div>
            </div>
            <div style="border-bottom: 1px solid #eee;padding: 25px;">
                <div>2020年03月13日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版本1.05</div>
                <div style="margin-top: 10px;display: inline;">
                    <div style="margin-top: 5px;">
                        1、优化图片上传速度。
                    </div>
                    <div style="margin-top: 5px;">
                        2、优化摄像头画面速度。
                    </div>
                </div>
            </div>
            <div style="border-bottom: 1px solid #eee;padding: 25px;">
                <div>2020年03月18日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版本1.06</div>
                <div style="margin-top: 10px;display: inline;">
                    <div style="margin-top: 5px;">
                        1、新增人员提醒推送功能。
                    </div>
                    <div style="margin-top: 5px;">
                        2、新增文件上传下载功能。
                    </div>
                    <div style="margin-top: 5px;">
                        3、人员模块增加搜索功能。
                    </div>
                </div>
            </div>

            <div style="border-bottom: 1px solid #eee;padding: 25px;">
                <div>2020年03月22日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版本1.07</div>
                <div style="margin-top: 10px;display: inline;">
                    <div style="margin-top: 5px;">
                        1、新增领导带班功能。
                    </div>
                    <div style="margin-top: 5px;">
                        2、新增早晚查功能。
                    </div>
                    <div style="margin-top: 5px;">
                        3、新增监理旁站及推送通知功能。
                    </div>
                </div>
            </div>

            <div style="border-bottom: 1px solid #eee;padding: 25px;">
                <div>2020年03月28日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版本1.08</div>
                <div style="margin-top: 10px;display: inline;">
                    <div style="margin-top: 5px;">
                        1、修复摄像头录屏崩溃问题。
                    </div>
                    <div style="margin-top: 5px;">
                        2、优化崩溃日志。
                    </div>
                </div>
            </div>

            <div style="padding: 25px;">
                <div>2020年04月15日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版本1.09</div>
                <div style="margin-top: 10px;display: inline;">
                    <div style="margin-top: 5px;">
                        1、APP文字修改与界面调整。
                    </div>
                    <div style="margin-top: 5px;">
                        2、优化推送通知功能。
                    </div>
                    <div style="margin-top: 5px;">
                        3、人员信息增加证书照片。
                    </div>
                    <div style="margin-top: 5px;">
                        4、修复黑屏、卡顿问题。
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">

    $(function() {

    });

</script>
</body>
</html>
