<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <link href="${ctx}/assets/zTree/css/zTreeStyle/zTreeStyle.css" type="text/css" rel="stylesheet" />
    <script src="${ctx}/assets/zTree/js/jquery.ztree.all.js" type="text/javascript" charset="UTF-8"></script>

    <title>选择部门</title>

    <script type="text/javascript">
        function cancel_dept() {
            $('#deptModal').modal('hide');
        }

        var choice_deptCd_ID;
        var choice_deptName_ID;
        function admit() {
            var tree1 = $.fn.zTree.getZTreeObj("deptTree");
            var node1 = tree1.getSelectedNodes();
            var name = node1[0].DEPT_NAME;
            if (node1.length > 0) {
                var parentNode = node1[0].getParentNode();
            }
            if(parentNode != null){
                var parentName=parentNode.DEPT_NAME;
                name = parentName + "—" + name;
            }
            /*$('#choiceDeptName').val(node1[0].DEPT_NAME);
            $('#choiceDeptCd').val(node1[0].DEPT_ID);*/
            $('#'+choice_deptCd_ID+'').val(node1[0].DEPT_CD);
            $('#'+choice_deptName_ID+'').val(name);
            //casecode=node1[0].code;
            $('#deptModal').modal('hide');
        }
        //部门树设置
        var deptTreeSet = {
            data : {
                key : {
                    name : "DEPT_NAME" // 节点名称的属性
                },
                simpleData : {
                    enable : true, // 使用简单数据模型（array），但请注意simpleData内的参数（idKey，pIdKey，rootPId）满足父子关系
                    idKey : "DEPT_ID",
                    pIdKey : "PARENTID",
                    rootPId : "-1",
                }
            }
        };

        function initDeptTree(deptCd_ID,deptName_ID) {
            choice_deptCd_ID = deptCd_ID;
            choice_deptName_ID = deptName_ID;
            //加载部门和人员树
            $.ajax({
                async : false,
                cache : false,
                type : 'POST',
                dataType : "json",
                url : path + "/dept/getAllDeptInfo",
                error : function(xhr, textStatus, exception) {
                    notyError('部门数据加载失败!' + xhr.responseText);
                },
                success : function(data) {
                    deptTree = $.fn.zTree.init($("#deptTree"), deptTreeSet,
                        data);
                    // 展开根节点
                    deptTree.expandNode(deptTree.getNodeByParam("PARENTID",
                        "-1", null), true, false, false);
                },
                callback : {
                    onClick : function(e, treeId, treeNode) { // 节点点击事件

                    }
                }
            });
        }

    </script>
</head>
<body>

<div class="modal fade szgdModal midModal" id="deptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:630px;">
        <div class="modal-content" style="height: 300px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">部门结构</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="choiceDeptCd" value="" id="choiceDeptCd">
                <input type="hidden" name="choiceDeptName" value="" id="choiceDeptName">
                <div class="container-fluid">
                    <div class="col-xs-10">
                        <div class="tree_item" style="height:350px; overflow:auto;">
                            <ul id="deptTree" class="ztree"></ul>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <button style="width: 100px;" class="btn btn-primary btn-md" onclick="admit()">确定</button>
                        <button style="width: 100px;margin-top: 10px;" class="btn btn-danger btn-md" onclick="cancel_dept()">取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--</div>--%>
</body>
</html>