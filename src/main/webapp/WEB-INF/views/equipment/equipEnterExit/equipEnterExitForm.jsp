<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>设备进出信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $('#enterTime').datetimepicker({
                format: 'yyyy-mm-dd hh:ii:ss', autoclose: true, minView: 0, minuteStep:1,
                todayHighlight: 1,
                startView: 2,
                language: 'zh-CN'
            });

            $('#exitTime').datetimepicker({
                format: 'yyyy-mm-dd hh:ii:ss', autoclose: true, minView: 0, minuteStep:1,
                todayHighlight: 1,
                startView: 2,
                language: 'zh-CN'
            });
            var opt ="${opt}";
/*            if(opt == "E"){
                $('#siteId').attr("disabled",true);
            }*/
            $('#saveRecordInfo').click(function () {
                var valid = $("#enterExitForm").validationEngine("validate");
                if (!valid)
                    return;
                var param = serializeObject('#enterExitForm');
                var otherParam = {};
                var jsonString = JSON.stringify($.extend(param, otherParam));
                $.ajax({
                    contentType : "application/json",
                    url : '${ctx}/equipEnterExit/saveEnterExit',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);
                        }else{
                            notyError(data.resultMsg);
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                    }
                });
            });
        });

        function fillOtherInfo() {
            var equipId = $("#enterExitForm select[name=equipId]").val();
            var params = {
                'id' : equipId
            };
            $.ajax({
                contentType : "application/json",
                type : 'POST',
                url : '${ctx}/equipmentInfo/getEquipmentInfoListNoPage',
                data : JSON.stringify(params),
                success : function (res) {
                    console.log(res);
                    if(res.resultCode == 1){
                        $('#equipType').val(res.data[0].equipType);
                        $("#name").val([res.data[0].name]).trigger("change");
//                        $("#workSite").val([res.data[0].workSiteId]).trigger("change");
                    }else{
                        notyError("数据请求失败!");
                    }

                },
                error: function () {
                    notyError("数据请求失败!");
                }
            });
        }
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/equipEnterExit/toEquipEnterExitList" title="点击返回至列表">设备进出信息</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        设备进出信息新增
    </c:if>
    <c:if test="${opt == 'E'}">
        设备进出信息编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="enterExitForm">
        <input type="hidden" name="id" value="${equipEnterExit.id}" id="id">
        <input type="hidden" id="opt" name="opt" value="${opt}">

        <h4><i class="fa fa-bars"></i> 设备</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>设备编号</label>
                <div class="col-md-3 pt7">
                    <c:if test="${opt == 'A'}">
                        <select class="form-control input-sm select2 validate[required]" name="equipId" id="equipId" onchange = "fillOtherInfo();">
                    </c:if>
                    <c:if test="${opt == 'E'}">
                        <input type="hidden" name="equipId" value="${equipEnterExit.equipId}">
                        <select disabled class="form-control input-sm select2 validate[required]" name="equipId" id="equipId" onchange = "fillOtherInfo();">
                    </c:if>
                        <option value="">请选择</option>
                        <c:forEach items="${equipList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq equipEnterExit.equipId}">
                                    <option value="${item.id}" selected>${item.equipNumber}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.equipNumber}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">设备型号</label>
                <div class="col-md-3 pt7">
                    <input readonly class="form-control input-sm"  id = "equipType"  name="equipType" placeholder="" value="${equipEnterExit.equipType}" type="text">
                </div>

                <%--<label class="col-md-2 control-label">所属站点</label>--%>
                <%--<div class="col-md-3 pt7">--%>
                    <%--<select class="form-control input-sm select2" name="workSite" id="workSite" disabled>--%>
                        <%--<option value="">请选择</option>--%>
                        <%--<c:forEach items="${sessionScope.workSiteList}" var="item">--%>
                            <%--<c:choose>--%>
                                <%--<c:when test="${item.id eq equipEnterExit.WORK_SITE_ID || item.defaultShow eq '1'}">--%>
                                    <%--<option value="${item.id}" selected>${item.name}</option>--%>
                                <%--</c:when>--%>
                                <%--<c:otherwise>--%>
                                    <%--<option value="${item.id}">${item.name}</option>--%>
                                <%--</c:otherwise>--%>
                            <%--</c:choose>--%>
                        <%--</c:forEach>--%>

                    <%--</select>--%>
                <%--</div>--%>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">设备名称</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="name" id="name" disabled>
                        <option value="">请选择</option>
                        <c:forEach items="${sessionScope.EQUIP_NAME}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq equipEnterExit.equipCode}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>

        <h4><i class="fa fa-bars"></i> 设备进出信息</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-xs-2 control-label"><span class="alarmstar">*</span>站点</label>
                <div class="col-xs-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="siteId" id="siteId">
                        <option value="">请选择</option>
                        <c:forEach items="${sessionScope.siteList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq equipEnterExit.siteId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <%--<c:if test="${opt == 'A'}">--%>
                    <label class="col-xs-2 control-label"><span class="alarmstar">*</span>进入时间</label>
                    <div class="col-xs-3 pt7">
                        <div class="input-group-date">
                            <input readonly class="dateStyle validate[required,past[#enterTime]]"  id = "enterTime"  name="enterTime" placeholder="" value="${equipEnterExit.enterTime}" type="text">
                            <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                    </div>
                <%--</c:if>--%>

                <%--<c:if test="${opt == 'E'}">
                    <label class="col-xs-1 control-label"><span class="alarmstar">*</span>进入时间</label>
                    <div class="col-xs-2">
                        <input disabled class="form-control input-sm validate[required,past[#endTime]]"  id = "beginTime"  name="beginTime" placeholder="" value="${equipEnterExit.enterTime}" type="text">
                    </div>
                </c:if>--%>

            </div>

            <div class="row">
                <label class="col-xs-2 control-label">离开时间</label>
                <div class="col-xs-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[future[#exitTime]]"  id = "exitTime"  name="exitTime" placeholder="" value="${equipEnterExit.exitTime}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveRecordInfo"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/equipEnterExit/toEquipEnterExitList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>
</body>
</html>
