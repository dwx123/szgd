<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>设备进出记录</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var equipRecordTable ;
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear: true
            });

            equipRecordTable = $("#equipRecordTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/equipEnterExit/getEquipEnter_ExitList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'equipmentNumber', 'sTitle': '设备编号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'equipType', 'sTitle': '设备型号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'inCharge', 'sTitle': '联系人', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'enterTime', 'sTitle': '设备进入时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'exitTime', 'sTitle': '设备离开时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'stayHours', 'sTitle': '停留时长', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'siteName', 'sTitle': '站点', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'name', 'sTitle': '图片', 'sWidth': '10%', 'sClass': 'center'},
                    <c:if test="${from ne 'home'}">
                        {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
                    </c:if>

                ],
                //渲染某列格式
                aoColumnDefs: [
//                    {   aTargets: [7],
//                        mRender: function (data, type, row) {
//                            var html = "" ;
//                            if (data != null && data != "") {
//                                html += '<a class="btn-sm btn-smd btn btn-success" href="javascript:;" title="预览图片" onclick="previewPic(\''+ row.name + '\',\''+ row.path + '\')">预览</a>&nbsp;&nbsp;';
//                                html += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="下载图片" onclick="downloadAttach(\''+ row.name + '\',\''+ row.path + '\')">下载</a>';
//                            }
//                            return html;
//
//                        }
//                    },
                    <c:if test="${from ne 'home'}">
                        {
                            "targets":-1,
                            "bSortable": false,
                            mRender: function (data, type, row) {
                                var domHtml = "";
//                                if(row.fromflag == 0){
                                    domHtml += '<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/equipEnterExit/toEquipEnterExitForm?opt=E&id='+ row.id +'" title="编辑">编辑</a>&nbsp;&nbsp;';
                                    domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除" onclick="delItem(\''+ row.id + '\')">删除</a>';
//                                }
                                return domHtml;
                            }
                        }
                    </c:if>

                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#equipRecordForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });

                    var formSelectArray = $("#equipRecordForm select");
                    $.each(formSelectArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val

                        });
                    });
                }

            });
            $("#findEquipRecord").click(function () {
                equipRecordTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('equipRecordForm')
            });

            $('#enterDateFrom').datetimepicker({
                format: 'yyyy-mm-dd hh:ii', autoclose: true, minView: 0, minuteStep:1,
                todayHighlight: 1,
                startView: 2,
                language: 'zh-CN'
            });

            $('#enterDateTo').datetimepicker({
                format: 'yyyy-mm-dd hh:ii', autoclose: true, minView: 0, minuteStep:1,
                todayHighlight: 1,
                startView: 2,
                language: 'zh-CN'
            });

            $('#exitDateFrom').datetimepicker({
                format: 'yyyy-mm-dd hh:ii', autoclose: true, minView: 0, minuteStep:1,
                todayHighlight: 1,
                startView: 2,
                language: 'zh-CN'
            });

            $('#exitDateTo').datetimepicker({
                format: 'yyyy-mm-dd hh:ii', autoclose: true, minView: 0, minuteStep:1,
                todayHighlight: 1,
                startView: 2,
                language: 'zh-CN'
            });
        });

        function downloadAttach(attachName,attachPath) {
            var  queryParams = 'path='+attachPath+'&fileName='+attachName;
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }

        function delItem(id) {
            var r=confirm("确认删除?");
            if (r==false){
                return;
            }
            var param = {
                id: id
            };

            $.ajax({
                contentType: "application/json",
                url : '${ctx}/equipEnterExit/deleteEnterExit',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        equipRecordTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

        function downloadRecord() {
            var action = path + "/equipEnterExit/downloadRecord";
            $('#equipRecordForm').attr('action',action);
            $('#equipRecordForm').submit();
        }

        function previewPic(attachName,attachPath) {
            var  queryParams = 'path='+attachPath+'&fileName='+attachName;
            var url =path + "/upload/previewPic?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>

</head>
<body>

<c:if test="${from ne 'home'}">
    <div class="bg-white item_title">
        设备进出信息
    </div>
</c:if>
<div class="bg-white">
    <form id="equipRecordForm" class="form_search form_table container-fluid">
        <div class="row form-group">
            <%--<div class="col-xs-4">--%>
                <%--<div class="form-item wide2">--%>
                    <%--<label>设备编号</label>--%>
                    <%--<input class="form-control" id="equipNumber" name="equipNumber">--%>
                <%--</div>--%>
            <%--</div>--%>

            <div class="col-xs-4">
                <div class="form-item wide2">
                    <label>设备型号</label>
                    <input class="form-control" id="equipType" name="equipType">
                </div>
            </div>

            <div class="col-xs-4">
                <div class="form-item wide2">
                    <label>联系人</label>
                    <input class="form-control" id="inCharge" name="inCharge">
                </div>
            </div>

            <div class="col-xs-4">
                <div class="form-item wide2">
                    <label>站点</label>
                    <select class="form-control input-sm select2" name="siteId" id="siteId">
                        <option value="">请选择</option>
                        <c:forEach items="${sessionScope.siteList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq siteId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

        </div>

        <div class="row form-group">
            <div class="col-xs-4">
                <div class="form-item wide2">
                    <label>设备进入时间</label>
                    <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">
                        <input style="background-color: #fff;" class="form-control" id="enterDateFrom" name="enterDateFrom" type="text" readonly>
                        <span class="input-group-addon"> 至 </span>
                        <input style="background-color: #fff;" class="form-control" id="enterDateTo" name="enterDateTo" type="text" readonly>
                    </div>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="form-item wide2">
                    <label>设备离开时间</label>
                    <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">
                        <input style="background-color: #fff;" class="form-control" id="exitDateFrom" name="exitDateFrom" type="text" readonly>
                        <span class="input-group-addon"> 至 </span>
                        <input style="background-color: #fff;" class="form-control" id="exitDateTo" name="exitDateTo" type="text" readonly>
                    </div>
                </div>
            </div>


            <div class="col-xs-4 text-right">
                <a href="javascript:;" class="ml5 btn btn-primary btn-md1" id="findEquipRecord">查询</a>&nbsp;&nbsp;
                <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn">重置</button>
            <c:if test="${from ne 'home'}">
                <a href="<%=request.getContextPath() %>/equipEnterExit/toEquipEnterExitForm?opt=A" class="ml15 btn btn-primary btn-md1">新增</a>
                <button type="button" onclick="downloadRecord()" class="ml15 btn btn-md1 btn-warning" title="导出进出记录Excel">导出设备进出记录</button>
            </c:if>

            </div>
        </div>

    </form>
</div>
<div class="bg-white pd_item">
    <table id="equipRecordTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>

</body>
</html>
