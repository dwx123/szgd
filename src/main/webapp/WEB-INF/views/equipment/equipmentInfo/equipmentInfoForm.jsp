<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>设备信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear: true
            });

            $('#releaseDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#effectiveDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#testDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#insuranceExpireDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#saveEquipInfo').click(function () {
                var valid = $("#equipInfoForm").validationEngine("validate");
                if (!valid)
                    return;
                var param = serializeObject('#equipInfoForm');
                var otherParam = {};
                var jsonString = JSON.stringify($.extend(param, otherParam));
                $.ajax({
                    contentType: "application/json",
                    url : '${ctx}/equipmentInfo/saveEquipInfo',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);

                        }else{
                            notyError(data.resultMsg);

                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));

                    }
                });

            });
        });
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/equipmentInfo/toEquipmentInfoList" title="点击返回至列表">设备信息管理</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        设备信息增加
    </c:if>
    <c:if test="${opt == 'E'}">
        设备信息编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="equipInfoForm">
        <input type="hidden" name="id" value="${equip.id}" id="id">

        <input type="hidden" id="opt" name="opt" value="${opt}">
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>设备编号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "equipNumber" name="equipNumber"  placeholder="" value="${equip.equipNumber}" type="text" <c:if test="${opt == 'E'}">readonly</c:if>>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>设备型号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,maxSize[50]]"  id = "equipType" name="equipType"  placeholder="" value="${equip.equipType}" type="text">
                </div>

            </div>

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>设备名称</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="name" id="name">
                        <option value="">请选择</option>
                        <c:forEach items="${sessionScope.EQUIP_NAME}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq equip.name}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">租赁/自有</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2"  id = 'useDept' name="useDept">
                        <option value="">请选择</option>
                        <option value="0" <c:if test="${equip.useDept eq 0}">selected</c:if>>租赁</option>
                        <option value="1" <c:if test="${equip.useDept eq 1}">selected</c:if>>自有</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">操作人</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[maxSize[50]]"  id = "inCharge"  name="inCharge" placeholder="" value="${equip.inCharge}" type="text">
                </div>

                <label class="col-md-2 control-label">检测日期</label>
                <div class="col-md-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle"  id = "testDate"  name="testDate" placeholder=""  value="${equip.testDate}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">保险到期</label>
                <div class="col-md-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle"  id = "insuranceExpireDate"  name="insuranceExpireDate" placeholder=""  value="${equip.insuranceExpireDate}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>
            </div>

        </div>

        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveEquipInfo"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/equipmentInfo/toEquipmentInfoList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>
</body>
</html>
