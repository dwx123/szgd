<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>设备基础信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var equipmentInfoTable ;
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear: true
            });

            equipmentInfoTable = $("#equipmentInfoTable").DataTable({
                iDisplayLength: 10,
               "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                bsort: false,
                sAjaxSource: path + "/equipmentInfo/getEquipmentInfoList",//获取列表数据url,
                sServerMethod: "GET",
                //锁定行
                scrollY:"450px",
                //锁定列
                scrollX:true,
                scrollCollapse: true,
                fixedColumns: {
                    leftColumns: 1,
                    rightColumns:1
                },
                //table 表头
                aoColumns: [
                    {'mDataProp': 'equipNumber', 'sTitle': '设备编号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'equipName', 'sTitle': '设备名称', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'equipType', 'sTitle': '设备型号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'useDept', 'sTitle': '租赁/自有', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'testDate', 'sTitle': '检测日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'insuranceExpireDate', 'sTitle': '保险到期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'haveAccept', 'sTitle': '是否有设备验收单', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'haveTesting', 'sTitle': '是否有检测报告', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'haveHoisting', 'sTitle': '是否有吊装令', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'hoistUploadTime', 'sTitle': '吊装令上传时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'enterTime', 'sTitle': '进场时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'exitTime', 'sTitle': '出场时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        aTargets: [12],
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="'+ path +'/equipmentInfo/toEquipmentInfoView?id='+ row.id +'" title="查看设备信息">查看</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/equipmentInfo/toEquipmentInfoForm?opt=E&id='+ row.id +'" title="编辑设备信息">编辑</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除设备信息" onclick="delEquipmentInfo(\''+ row.id +'\')">删除</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="javascript:;" title="查看附件信息" onclick="detailInfo(\''+ row.id + '\',\''+ row.name + '\')">查看附件信息</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="上传验收单照片" onclick="uploadAccept(\''+ row.id + '\')">上传验收单照片</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-success" href="javascript:;" title="上传检测报告照片" onclick="uploadTesting(\''+ row.id + '\')">上传检测报告照片</a>&nbsp;&nbsp;';
                            <c:if test="${fn:contains(userInfomation.roleIds,'SCDZLRY')}">
                                var equip = row.equipName;
                                if(equip.indexOf("吊") != -1){
                                    domHtml += '<a class="btn-sm btn-smd btn btn-info" href="javascript:;" title="上传吊装条件验收照片" onclick="uploadHoisting(\''+ row.id + '\',\''+ row.equipNumber + '\',\''+ row.equipName + '\')">上传吊装条件验收照片</a>';
                                }
                            </c:if>
                            return domHtml;
                        }
                    },
                    {
                        aTargets: [3],
                        mRender: function (data, type, row) {
                            var useDept = "" ;
                            if (data == 0) {
                                useDept = '租赁';
                            }
                            if (data == 1) {
                                useDept = '自有';
                            }
                            return useDept;
                        }
                    },
                    {
                        aTargets: [6],
                        mRender: function (data, type, row) {
                            var ifUpload = "" ;
                            if (data == 0) {
                                ifUpload = '否';
                            }
                            if (data == 1) {
                                ifUpload = '是';
                            }
                            return ifUpload;
                        }
                    },
                    {
                        aTargets: [7],
                        mRender: function (data, type, row) {
                            var ifUpload ="" ;
                            if (data == 0) {
                                ifUpload = '否';
                            }
                            if (data == 1) {
                                ifUpload = '是';
                            }
                            return ifUpload;
                        }
                    },
                    {
                        aTargets: [8],
                        mRender: function (data, type, row) {
                            var ifUpload = "" ;
                            if (data == 0) {
                                ifUpload = '否';
                            }
                            if (data == 1) {
                                ifUpload = '是';
                            }
                            return ifUpload;
                        }
                    }
                ],
                fnRowCallback : function(nRow, aData, iDisplayIndex) {
                    $('td:eq(12)', nRow).attr('style', 'text-align: left;');
                },
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#equipmentInfoForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });
                    var formSltArray = $("#equipmentInfoForm select");
                    $.each(formSltArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });
                }

            });
            $("#findEquipmentInfo").click(function () {
                equipmentInfoTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('equipmentInfoForm');
            })
        });

        function delEquipmentInfo(id) {
            var r=confirm("确认删除？");
            if (r==false){
                return
            }
            var param = {
                id: id
            };

            $.ajax({
                contentType: "application/json",
                url : '${ctx}/equipmentInfo/delEquipmentInfo',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        equipmentInfoTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

        function checkUploadEquipmentInfoForm() {
            var fileDir = $("#equipmentInfoFile").val();
            if ("" == fileDir) {
                notyWarning("选择需要导入的文件！");
                return false;
            }
            var suffix = fileDir.substr(fileDir.lastIndexOf("."));
            if (".xls" != suffix && ".xlsx" != suffix) {
                notyWarning("选择Excel格式的文件导入！");
                return false;
            }
            return true;
        }

        function uploadEquipmentInfo() {
            if (checkUploadEquipmentInfoForm()) {
                function resutlMsg(msg) {
                    if (msg =="文件导入失败！"){
                        $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                    }else{
                        if(msg.indexOf("#WEBROOT#") != -1){
                            msg  = msg.replace("#WEBROOT#", path +"/upload");
                        }
                        $("#resultMsg").html("<font color='#006400'>" + msg +"</font>");
                    }
                    $("#file").val("");
                    $("#equipmentInfoTable").DataTable().ajax.reload();
                }
                function errorMsg() {
                    $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                }
                $('#equipmentInfoUploadForm').ajaxSubmit({
                    url: path + '/equipmentInfo/batchUploadEquipmentInfo',
                    dataType: 'text',
                    success: resutlMsg,
                    error: errorMsg
                });
            }
        }

        function downloadTemplate() {
            var  queryParams = 'templateName=设备信息批量导入模版.xls';
            var url =path + "/upload/downloadTemplate?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }

        function detailInfo(id,name) {
            openWin('#detailModel', path + '/equipWork/toEquipWorkView', {"id": id,"name": name});
        }

        function uploadAccept(id) {
            openWin('#uploadModel', path + '/equipWork/toEquipWorkUpload', {"id": id,"modalFlag": "accept"});
        }
        function uploadHoisting(id,equipNumber,equipName) {
            openWin('#uploadModel', path + '/equipWork/toEquipWorkUpload', {"id": id,"modalFlag": "hoisting","equipNumber": equipNumber,"equipName": equipName});
        }
        function uploadTesting(id) {
            openWin('#uploadModel', path + '/equipWork/toEquipWorkUpload', {"id": id,"modalFlag": "testing"});
        }

        function downloadEquipInfo(){
            window.open("${ctx}/equipmentInfo/downloadEquipInfo");
        }
    </script>

</head>
<body>

<div class="bg-white item_title" style="padding-right: 0px;">
    设备基础信息管理
    <button type="button" onclick="downloadEquipInfo()" class="btn btn-md1 btn-warning title-btn" title="点击导出设备信息Excel">导出设备信息</button>
    <button type="button" class="btn btn-md1 btn-primary title-btn mr10" data-toggle="modal" data-target="#importInfo" title="点击批量导入设备信息">导入设备信息</button>

</div>
<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">

        <div class="row form-group">
            <form id="equipmentInfoForm">
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>设备编号</label>
                        <input class="form-control" id="equipNumber" name="equipNumber">
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>设备型号</label>
                        <input class="form-control" id="equipType" name="equipType">
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>设备名称</label>
                        <select class="form-control input-sm select2" name="name" id="name">
                            <option value=""> 请选择</option>
                            <c:forEach items="${sessionScope.EQUIP_NAME}" var="item">
                                <option value="${item.code}">${item.dictValue}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3 text-right">
                    <a id="findEquipmentInfo" href="javascript:;" class="ml15 btn btn-primary btn-md1" >查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                    <a id="addEquipmentInfo" href="<%=request.getContextPath() %>/equipmentInfo/toEquipmentInfoForm?opt=A" class="ml15 btn btn-primary btn-md1" >新增</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="equipmentInfoTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>

<div id="detailModel" class="modal fade" data-backdrop="static"></div>

<div id="uploadModel" class="modal fade" data-backdrop="static"></div>

<!-- 模态框（Modal） -->
<div class="modal fade" id="importInfo" tabindex="-1" role="dialog" aria-labelledby="importInfoLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="importInfoLabel">导入设备信息</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form_area" method="post" enctype="multipart/form-data" id="equipmentInfoUploadForm">
                        <div class="col-md-8" style="padding-left: 8%;">
                            <input style="padding: 0;" type="file" name="file" id="equipmentInfoFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        </div>
                        <div class="col-md-4">
                            <button type="button" onclick="uploadEquipmentInfo()" class="btn btn-md1 btn-primary" title="点击批量导入设备信息">导&nbsp;&nbsp;入</button>
                            <button type="button" onclick="downloadTemplate()" class="ml5 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                        </div>
                        <div class="col-md-5 mt10" style="padding-left: 6%"><span class="label" id="resultMsg"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
</body>
</html>
