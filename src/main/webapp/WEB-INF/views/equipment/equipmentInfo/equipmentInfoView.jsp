<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>设备信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear: true
            });
        });

    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/equipmentInfo/toEquipmentInfoList" title="点击返回至列表">设备信息管理</a>
    <i class="fa fa-angle-double-right"></i>
    设备信息查看
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="equipInfoForm">
        <input type="hidden" name="id" value="${equip.id}" id="id">

        <input type="hidden" id="opt" name="opt" value="${opt}">
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>设备编号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "equipNumber" name="equipNumber"  placeholder="" value="${equip.equipNumber}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>设备型号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "equipType" name="equipType"  placeholder="" value="${equip.equipType}" type="text" readonly>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>设备名称</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "name"  name="name" placeholder="" value="${equip.equipName}" type="text"  readonly>
                </div>

                <label class="col-md-2 control-label">租赁/自有</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2"  id = 'useDept' name="useDept" disabled>
                        <option value="">请选择</option>
                        <option value="0" <c:if test="${equip.useDept eq 0}">selected</c:if>>租赁</option>
                        <option value="1" <c:if test="${equip.useDept eq 1}">selected</c:if>>自有</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">操作人</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "inCharge"  name="inCharge" placeholder="" value="${equip.inCharge}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">检测日期</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "testDate"  name="testDate" placeholder="" value="${equip.testDate}" type="text" readonly>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">保险到期</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "insuranceExpireDate"  name="insuranceExpireDate" placeholder="保险到期" value="${equip.insuranceExpireDate}" type="text" readonly>
                </div>
            </div>
        </div>

        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <a href="${ctx}/equipmentInfo/toEquipmentInfoList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>


</body>
</html>
