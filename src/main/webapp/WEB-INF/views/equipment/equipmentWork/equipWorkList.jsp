<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>设备工作信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <%@ include file="/common/include-css.jsp"%>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var equipWorkTable ;
        $(function () {
            equipWorkTable = $("#equipWorkTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/equipWork/getEquipWorkList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'equipNumber', 'sTitle': '设备号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'equipType', 'sTitle': '设备类型', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'useDeptName', 'sTitle': '使用单位', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'haveAccept', 'sTitle': '是否有设备验收单', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'haveHoisting', 'sTitle': '是否有吊装令', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'haveTesting', 'sTitle': '是否有检测报告', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'},
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="上传验收单" onclick="uploadAccept(\''+ row.id + '\')">上传验收单</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-info" href="javascript:;" title="上传吊装令" onclick="uploadHoisting(\''+ row.id + '\')">上传吊装令</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-success" href="javascript:;" title="上传检测报告" onclick="uploadTesting(\''+ row.id + '\')">上传检测报告</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="javascript:;" title="查看详细信息" onclick="detailInfo(\''+ row.id + '\')">查看详细信息</a>';
                            return domHtml;
                        }
                    },
                    {
                        aTargets: [3],
                        mRender: function (data, type, row) {
                            var isKey ;
                            if (data == 0) {
                                isKey = '否';
                            }
                            if (data == 1) {
                                isKey = '是';
                            }
                            return isKey;

                        }
                    },
                    {
                        aTargets: [4],
                        mRender: function (data, type, row) {
                            var isKey ;
                            if (data == 0) {
                                isKey = '否';
                            }
                            if (data == 1) {
                                isKey = '是';
                            }
                            return isKey;

                        }
                    },
                    {
                        aTargets: [5],
                        mRender: function (data, type, row) {
                            var isKey ;
                            if (data == 0) {
                                isKey = '否';
                            }
                            if (data == 1) {
                                isKey = '是';
                            }
                            return isKey;

                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#equipWorkForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });
                }

            });
            $("#findEquipWork").click(function () {
                equipWorkTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('equipWorkForm');
            })
        });

        function addSelectListener() {
            //找到列表中的数据行
            var rows = $("#vehicleInfoTable tr");
            $.each(rows, function (index, row) {
                if (index > 0) {
                    $(row).click(
                        function () {
                            selectedData = $('#vehicleInfoTable').dataTable().fnGetData(this);
                            $("#choicedUserName").html("<strong>" + selectedData.USER_NAME + "</strong> [" + selectedData.LOGIN_ID + "]");
                            $(row).addClass("info");
                            $(row).siblings().removeClass("info");
                        });
                }
            });
        }

        function detailInfo(id) {
            openWin('#detailModel', path + '/equipWork/toEquipWorkView', {"id": id});
        }

        function uploadAccept(id) {
            openWin('#uploadModel', path + '/equipWork/toEquipWorkUpload', {"id": id,"modalFlag": "accept"});
        }
        function uploadHoisting(id) {
            openWin('#uploadModel', path + '/equipWork/toEquipWorkUpload', {"id": id,"modalFlag": "hoisting"});
        }
        function uploadTesting(id) {
            openWin('#uploadModel', path + '/equipWork/toEquipWorkUpload', {"id": id,"modalFlag": "testing"});
        }

    </script>

</head>
<body>

<div class="bg-white item_title">
    设备工作信息管理
</div>
<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">
        <div class="row form-group">
            <form id="equipWorkForm">
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>设备号</label>
                        <input class="form-control" id="equipNumber" name="equipNumber">
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>责任人</label>
                        <input class="form-control" id="inCharge" name="inCharge">
                    </div>
                </div>
                <div class="col-xs-6 text-right">
                    <a id="findEquipWork" href="javascript:;" class="ml15 btn btn-primary btn-md1" >查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="equipWorkTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>

<div id="detailModel" class="modal fade" data-backdrop="static"></div>

<div id="uploadModel" class="modal fade" data-backdrop="static"></div>
</body>
</html>
