<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <title>
        <c:if test="${modalFlag eq 'accept'}"> 上传验收单</c:if>
        <c:if test="${modalFlag eq 'hoisting'}"> 上传吊装令</c:if>
        <c:if test="${modalFlag eq 'testing'}"> 上传检测报告</c:if>
    </title>
    <meta charset="utf-8"/>
    <script>
        $(function () {
//            $('#equipTime').datetimepicker({
//                format: 'yyyy-mm-dd hh:mm:ss',
//                todayHighlight: 1,
//                minView: "month",
//                startView: 2,
//                autoclose: 1,
//                language: 'zh-CN'
//            });
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear: true
            });
            $(".glyphicon-remove").click(function(){
                $($($(this).parent()).prev()).val("");
            });
        });
    </script>
</head>
<body>
<div class="opend_modal" id="uploadModal">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <a data-dismiss="modal"><i class="fa fa-close"></i> </a> <b>
                <c:if test="${modalFlag eq 'accept'}"> 上传验收单</c:if>
                <c:if test="${modalFlag eq 'hoisting'}"> 上传吊装令</c:if>
                <c:if test="${modalFlag eq 'testing'}"> 上传检测报告</c:if>
            </b>
            </div>
            <div class="modal-body container-fluid">
                <form method="post" id="attachForm" >
                    <input type="hidden" name="equipId" value="${equipId}" id="equipId">
                    <input type="hidden" name="modalFlag" value="${modalFlag}" id="modalFlag">
                    <c:if test="${modalFlag eq 'accept'}">
                        <input type="hidden" name="type" value="EQUIP_WORK_ACCEPT" id="type">
                    </c:if>
                    <c:if test="${modalFlag eq 'hoisting'}">
                        <input type="hidden" name="type" value="EQUIP_WORK_HOISTING" id="type">
                    </c:if>
                    <c:if test="${modalFlag eq 'testing'}">
                        <input type="hidden" name="type" value="EQUIP_WORK_TESTING" id="type">
                    </c:if>
                    <input type="hidden" name="upload_equipNumber" value="${equipNumber}" id="upload_equipNumber">
                    <input type="hidden" name="equipName" value="${equipName}" id="equipName">

                    <div class="row">
                        <c:if test="${modalFlag eq 'accept'}">
                            <label class="col-md-2 control-label"><span class="alarmstar">*</span>验收单照片</label>
                        </c:if>
                        <c:if test="${modalFlag eq 'hoisting'}">
                            <label class="col-md-2 control-label"><span class="alarmstar">*</span>吊装验收照片</label>
                        </c:if>
                        <c:if test="${modalFlag eq 'testing'}">
                            <label class="col-md-2 control-label"><span class="alarmstar">*</span>检测报告</label>
                        </c:if>
                        <div id="equipAttach" class="col-md-7">
                            <input class="form-control form-control-inline input-sm validate[required,maxSize[50]]" id = "equipAttachName" name="equipAttachName" size="16" value="" type="text" readonly>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn-md btn btn-primary attachBtn">上传附件</button>
                            <input class="attach" type="file" id="uploadAttachRow"  name="file" accept="" onchange="uploadAttach(this)">
                        </div>
                    </div>
                    <%--<br>--%>
                    <%--<div class="row">--%>
                        <%--<c:if test="${modalFlag eq 'accept'}">--%>
                            <%--<label class="col-md-2 control-label"><span class="alarmstar">*</span>验收时间</label>--%>
                        <%--</c:if>--%>
                        <%--<c:if test="${modalFlag eq 'hoisting'}">--%>
                            <%--<label class="col-md-2 control-label"><span class="alarmstar">*</span>吊装令时间</label>--%>
                        <%--</c:if>--%>
                        <%--<c:if test="${modalFlag eq 'testing'}">--%>
                            <%--<label class="col-md-2 control-label"><span class="alarmstar">*</span>检测时间</label>--%>
                        <%--</c:if>--%>
                        <%--<div class="col-md-3">--%>
                            <%--<div class="input-group-date">--%>
                                <%--<input readonly class="dateStyle validate[required]"  id = "equipTime"  name="equipTime" placeholder="" type="text">--%>
                                <%--<span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                    <%--<br>--%>
                    <%--<div class="row">--%>
                        <%--<label class="col-md-2 control-label"><span class="alarmstar">*</span>上传人</label>--%>
                        <%--<div class="col-md-3">--%>
                            <%--<select class="form-control input-sm select2 validate[required]" name="uploadPerson" id="uploadPerson">--%>
                                <%--<option value="">请选择</option>--%>
                                <%--<c:forEach items="${personList}" var="item">--%>
                                    <%--<option value="${item.ID}">${item.NAME}</option>--%>
                                <%--</c:forEach>--%>
                            <%--</select>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                </form>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn-md btn btn-primary" onclick="postAttach()">提交</button>
                <button type="button" class="btn btn-default btn-md" data-dismiss="modal" id="closeModal">关闭</button>
            </div>
        </div>
    </div>
</div>




<script>
    var isEquipAttachChanged = '0';
    function uploadAttach(e) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onloadend = function () {
            var fileId = [];
            var rawId = e.id;
            fileId.push(e.id);
            $.ajaxFileUpload({
                url: "<%=request.getContextPath() %>/upload/uploadFile",
                type: 'post',
                secureuri: false,
                fileElementId: fileId,
                dataType: 'json',
                success: function (data, status) {
                    if (data.resultCode == 1) {
                        $("#equipAttachName").val(data.data[0]);
                        isEquipAttachChanged = '1';
                    } else {
                        notyError(data.resultMsg);
                    }
                },
                error: function (data, status, e) {
                    console.log(e);
                    notyError(e);
                }
            });
        }
    }

    function postAttach() {
        var valid = $("#attachForm").validationEngine("validate");
        if (!valid)
            return;
        var param = {
            equipId:$("#equipId").val(),
            name:$("#equipAttachName").val(),
            type: $("#type").val(),
//            equipTime: $("#equipTime").val(),
//            uploadPerson: $("#uploadPerson").val(),
            equipTime: null,
            uploadPerson: null,
            isEquipAttachChanged :isEquipAttachChanged,
            equipName:$("#equipName").val(),
            equipNumber:$("#upload_equipNumber").val()
        };
console.log(JSON.stringify(param));
        $.ajax({
            contentType : "application/json",
            url : '${ctx}/equipWorkAttach/saveEquipWorkAttach',
            type : 'POST',
            data: JSON.stringify(param),
            success : function (data) {
                var resultCode = data.resultCode;
                if(resultCode == 1)
                {
                    notySuccess(data.resultMsg);
                    setTimeout("javascript:location.href='${ctx}/equipmentInfo/toEquipmentInfoList'", 1000);
                }else{
                    notyError(data.resultMsg);
                }
            },
            error: function (request, status, error) {
                notyError(JSON.stringify(error));
            }
        });
    }

</script>
</body>
</html>