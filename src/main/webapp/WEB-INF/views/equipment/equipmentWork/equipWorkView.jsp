<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <title>
        附件详细信息
    </title>
    <meta charset="utf-8"/>
    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var acceptTable;
        var hoistingTable;
        var testingTable;
        $(function () {
            var equipId = $('#equipId2').val();

            acceptTable = $("#acceptTable").DataTable({
                iDisplayLength: 5,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/equipWorkAttach/getEquipWorkAttachList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'equipNumber', 'sTitle': '设备编号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'equipType', 'sTitle': '设备型号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'uploadPersonName', 'sTitle': '上传人', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'equipTime', 'sTitle': '设备验收时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'createTime', 'sTitle': '验收单上传时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-success" href="javascript:;" title="预览图片" onclick="previewPic(\''+ row.name + '\',\''+ row.path + '\')">照片预览</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="下载验收单" onclick="downloadAttach(\''+ row.id + '\',\''+ row.name + '\',\''+ row.path + '\')">下载验收单</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除" onclick="delItem(\''+ row.id + '\',\''+ row.type + '\')">删除</a>';
                            return domHtml;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    aoData.push({
                        "name" : "type",
                        "value" : "accept"
                    });
                    aoData.push({
                        "name" : "equipId",
                        "value" : equipId
                    });
                }
            });

            hoistingTable = $("#hoistingTable").DataTable({
                iDisplayLength: 5,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/equipWorkAttach/getEquipWorkAttachList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'equipNumber', 'sTitle': '设备编号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'equipType', 'sTitle': '设备型号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'uploadPersonName', 'sTitle': '上传人', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'equipTime', 'sTitle': '设备吊装令时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'createTime', 'sTitle': '吊装令上传时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-success" href="javascript:;" title="预览图片" onclick="previewPic(\''+ row.name + '\',\''+ row.path + '\')">照片预览</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="下载吊装令" onclick="downloadAttach(\''+ row.id + '\',\''+ row.name + '\',\''+ row.path + '\')">下载吊装令</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除" onclick="delItem(\''+ row.id + '\',\''+ row.type + '\')">删除</a>';
                            return domHtml;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    aoData.push({
                        "name" : "type",
                        "value" : "hoisting"
                    });
                    aoData.push({
                        "name" : "equipId",
                        "value" : equipId
                    });
                }
            });

            testingTable = $("#testingTable").DataTable({
                iDisplayLength: 5,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/equipWorkAttach/getEquipWorkAttachList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'equipNumber', 'sTitle': '设备编号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'equipType', 'sTitle': '设备型号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'uploadPersonName', 'sTitle': '上传人', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'equipTime', 'sTitle': '设备检测时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'createTime', 'sTitle': '检测报告上传时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-success" href="javascript:;" title="预览图片" onclick="previewPic(\''+ row.name + '\',\''+ row.path + '\')">照片预览</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="下载检测报告" onclick="downloadAttach(\''+ row.id + '\',\''+ row.name + '\',\''+ row.path + '\')">下载检测报告</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除" onclick="delItem(\''+ row.id + '\',\''+ row.type + '\')">删除</a>';
                            return domHtml;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    aoData.push({
                        "name" : "type",
                        "value" : "testing"
                    });
                    aoData.push({
                        "name" : "equipId",
                        "value" : equipId
                    });
                }
            });

        });

        function delItem(id,type) {
            var r=confirm("确认删除?");
            if (r==false){
                return;
            }
            var param = {
                id: id,
                equipId: $('#equipId2').val(),
                type: type
            };

            $.ajax({
                contentType: "application/json",
                url : '${ctx}/equipWorkAttach/deleteEquipWorkAttach',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        if(type == "EQUIP_WORK_ACCEPT"){
                            acceptTable.ajax.reload();
                        }else if(type == "EQUIP_WORK_HOISTING"){
                            hoistingTable.ajax.reload();
                        }else if(type == "EQUIP_WORK_TESTING"){
                            testingTable.ajax.reload();
                        }

                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

        function downloadAttach(id,attachName,attachPath) {
            var  queryParams = 'path='+attachPath+'&fileName='+attachName;
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }

        function previewPic(attachName,attachPath) {
            var  queryParams = 'path='+attachPath+'&fileName='+attachName;
            var url =path + "/upload/previewPic?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>
</head>
<body>
<div class="opend_modal">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <a data-dismiss="modal"><i class="fa fa-close"></i> </a> <b>
                附件详细信息
            </b>
            </div>
            <div class="modal-body container-fluid">
                <input type="hidden" name="equipId" value="${id}" id="equipId2">
                <h4><i class="fa fa-bars"></i> 设备验收单</h4>
                <div class="bg-white pd_item">
                    <table id="acceptTable" class="table table-striped table-bordered display nowrap self_table" style="width: 100%;" align="center">
                    </table>
                </div>

                <c:if test="${craneFlag eq 'crane'}">
                    <h4><i class="fa fa-bars"></i> 设备吊装令</h4>
                    <div class="bg-white pd_item">
                        <table id="hoistingTable" class="table table-striped table-bordered display nowrap self_table" style="width: 100%;" align="center">
                        </table>
                    </div>
                </c:if>
                <h4><i class="fa fa-bars"></i> 设备检测报告</h4>
                <div class="bg-white pd_item">
                    <table id="testingTable" class="table table-striped table-bordered display nowrap self_table" style="width: 100%;" align="center">
                    </table>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default btn-md" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>