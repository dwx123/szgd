<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <title>ftp上传</title>

    <script type="text/javascript">
        function upload() {
            if (checkData()) {
                function resutlMsg(msg) {
                    if (msg.indexOf("失败") > -1 ) {
                        $("#upLoadresultMsg").html("<font color='#ff4500'>" + msg + "</font>");
                    } else {
                        //$("#upLoadresultMsg").html("<font color='#006400'>" + msg + "</font>");
                        notySuccess(msg);
                        $("#closeBtn").click();
                    }
                    $("#upfile").val("");
                    //window.location.href = path+'/jfsq/hzsq-list';
                    //$("#uploadform").modal('hide');  //手动关闭
                }

                function errorMsg() {
                    $("#upLoadresultMsg").html("<font color='#ff4500'>" + msg + "</font>");
                }

                $('#uploadform').ajaxSubmit({
                    url: '${ctx}/${uploadUrl}',
                    dataType: 'text',
                    success: resutlMsg,
                    error: errorMsg
                });
            }
        }

        //JS校验form表单信息
        function checkData() {
            var fileDir = $("#upfile").val();
            var suffix = fileDir.substr(fileDir.lastIndexOf("."));
            if ("" == fileDir) {
                alert("选择需要上传的文件！");
                return false;
            }
            return true;
        }

    </script>
</head>
<body>
<%--<div class="bg-white item_title">--%>
    <%--年度预算导入--%>
<%--</div>--%>
<%--<c:if test="${not empty message}">--%>
    <%--<div class="ui-widget">--%>
        <%--<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">--%>
            <%--<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>--%>
                <%--<strong>提示：</strong>${message}</p>--%>
        <%--</div>--%>
    <%--</div>--%>
<%--</c:if>--%>
<%--<div class="bg-white pd_item">--%>
    <form class="form_area form_input" method="post" enctype="multipart/form-data" id="uploadform">
    	<input type="hidden" name="token" value="${token}"/>
    	<input type="hidden" name="id" id="id" value="${id}"/>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a data-dismiss="modal"><i class="fa fa-close"></i> </a>
                    <b id="myModalLabel">附件上传</b>
                </div>
                <div class="modal-body">
                    <span style="" class="label" id="upLoadresultMsg"></span>
                    <input id="upfile" type="file" name="upfile" style="margin: 0 auto;">
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="upload()" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> 上传
                    </button>
                    &nbsp;&nbsp;
                    <button type="button" id="closeBtn" data-dismiss="modal" class="btn btn-sm btn-default"><i class="fa fa-close"></i> 取消</button>
                </div>
            </div>
        </div>

    </form>
<%--</div>--%>
</body>
</html>