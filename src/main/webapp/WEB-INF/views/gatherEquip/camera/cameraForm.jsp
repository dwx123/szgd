<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>摄像头管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {

            $('#beginTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#saveCamera').click(function () {

                var valid = $("#cameraForm").validationEngine("validate");
                if (!valid)
                    return;
                disabledAllBtn(true);
                var param = serializeObject('#cameraForm');
                var otherParam = {};
                var jsonString = JSON.stringify($.extend(param, otherParam));
                $.ajax({
                    contentType: "application/json",
                    url : '${ctx}/camera/saveCamera',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);

                        }else{
                            notyError(data.resultMsg);
                            disabledAllBtn(false);
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                        disabledAllBtn(false);
                    }
                });

            });
        });

    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/camera/toCameraList" title="点击返回至列表">摄像头管理</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        摄像头增加
    </c:if>
    <c:if test="${opt == 'E'}">
        摄像头编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="cameraForm">
        <input type="hidden" name="id" value="${camera.id}" id="id">

        <input type="hidden" id="opt" name="opt" value="${opt}">

        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>摄像头名称</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,maxSize[50]]"  id = "name" name="name"  placeholder="" value="${camera.name}" type="text">
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>所属站点</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="siteId" id="siteId">
                        <option value=""> 请选择</option>
                        <c:forEach items="${siteList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq camera.siteId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row">

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>内网ip</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,custom[ipv4]]"  id = "ip"  name="ip" placeholder="" value="${camera.ip}" type="text">
                </div>

                <label class="col-md-2 control-label">内网端口</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "innerPort"  name="innerPort" placeholder="" value="${camera.innerPort}" type="text">
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>是否可用</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]"  id = 'usable' name="usable">
                        <option value="">请选择</option>
                        <option value="1" <c:if test="${camera.usable eq 1 || camera.usable eq null}">selected</c:if>>是</option>
                        <option value="0" <c:if test="${camera.usable eq 0}">selected</c:if>>否</option>
                    </select>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>是否大屏显示</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]"  id = 'defaultShow' name="defaultShow">
                        <option value="">请选择</option>
                        <option value="1" <c:if test="${camera.defaultShow eq 1}">selected</c:if>>是</option>
                        <option value="0" <c:if test="${camera.defaultShow eq 0  || camera.defaultShow eq null}">selected</c:if>>否</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>登录名</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required]"  id = "loginUserName"  name="loginUserName" placeholder="" value="${camera.loginUserName}" type="text">
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>密码</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required]"  id = "loginPassword"  name="loginPassword" placeholder="" value="${camera.loginPassword}" type="text">
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>位置</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required]"  id = "place"  name="place" placeholder="" value="${camera.place}" type="text">
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>摄像头名称（APP用）</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,maxSize[100]]" id = "appName" name="appName"  placeholder="" value="${camera.appName}" type="text">
                </div>
            </div>
            <div class="row">


                <label class="col-md-2 control-label">摄像头排序（APP用）</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[custom[number]]" id = "appOrder" name="appOrder"  placeholder="" value="${camera.appOrder}" type="text">
                </div>
                <label class="col-md-2 control-label">编号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "number"  name="number" placeholder="" value="${camera.number}" type="text">
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">外网ip（APP用）</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "outerIp"  name="outerIp" placeholder="" value="${camera.outerIp}" type="text">
                </div>

                <label class="col-md-2 control-label">外网端口（APP用）</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "outerPort"  name="outerPort" placeholder="" value="${camera.outerPort}" type="text">
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">通道号（APP用）</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "appChannel"  name="appChannel" placeholder="" value="${camera.appChannel}" type="text">
                </div>

                <label class="col-md-2 control-label">x坐标</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[custom[number]]"  id = "x"  name="x" placeholder="" value="${camera.x}" type="text">
                </div>
            </div>
            <div class="row">


                <label class="col-md-2 control-label">y坐标</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[custom[number]]"  id = "y"  name="y" placeholder="" value="${camera.y}" type="text">
                </div>

            </div>



        </div>
        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveCamera"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/camera/toCameraList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
