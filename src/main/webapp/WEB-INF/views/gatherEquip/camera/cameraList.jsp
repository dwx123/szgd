<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>摄像头管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var cameraTable ;
        $(function () {
            cameraTable = $("#cameraTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                //锁定列
                scrollX:true,
                sAjaxSource: path + "/camera/getCameraList",//获取列表数据url,
                sServerMethod: "GET",
                fixedColumns: {
                    rightColumns:1
                },
                aoColumns: [
                    <c:if test="${from ne 'home'}">
                    {'mDataProp': 'name', 'sTitle': '名称', 'sWidth': '10%', 'sClass': 'center'},
                    </c:if>
                    {'mDataProp': 'place', 'sTitle': '位置', 'sWidth': '10%', 'sClass': 'center'},
                    <c:if test="${from ne 'home'}">
                    {'mDataProp': 'ip', 'sTitle': '内网ip', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'innerPort', 'sTitle': '内网端口', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'siteName', 'sTitle': '所属站点', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'usable', 'sTitle': '是否可用', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'defaultShow', 'sTitle': '是否大屏显示', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'loginUserName', 'sTitle': '登录名', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'loginPassword', 'sTitle': '密码', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'fileName', 'sTitle': '抓拍图片', 'sWidth': '10%', 'sClass': 'center'},

                    {'mDataProp': 'outerIp', 'sTitle': '外网ip（APP用）', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'outerPort', 'sTitle': '外网端口（APP用）', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'appChannel', 'sTitle': '通道号（APP用）', 'sWidth': '10%', 'sClass': 'center'},

                    {'mDataProp': 'appName', 'sTitle': '摄像头名称（APP用）', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'appOrder', 'sTitle': '摄像头排序（APP用）', 'sWidth': '10%', 'sClass': 'center'},
                    </c:if>

                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
                ],
                //渲染某列格式
                aoColumnDefs: [
                    <c:if test="${from ne 'home'}">
                        {
                            aTargets: [5],	//指向某一列，0为第一列
                            mRender: function (data, type, row) {
                                var value = "" ;
                                if (data == 0) {
                                    value = '否';
                                }
                                if (data == 1) {
                                    value = '是';
                                }
                                return value;

                            }
                        },
                        {
                            aTargets: [6],	//指向某一列，0为第一列
                            mRender: function (data, type, row) {
                                var value = "" ;
                                if (data == 0) {
                                    value = '否';
                                }
                                if (data == 1) {
                                    value = '是';
                                }
                                return value;

                            }
                        },
                    </c:if>


                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            <c:if test="${from ne 'home'}">
                                domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="'+ path +'/camera/toCameraView?id='+ row.id +'" title="查看摄像头信息">查看</a>&nbsp;&nbsp;';
                                domHtml += '<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/camera/toCameraForm?opt=E&id='+ row.id +'" title="修改摄像头信息">编辑</a>&nbsp;&nbsp;';
                                domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除摄像头信息" onclick="delCamera(\''+ row.id + '\')">删除</a>&nbsp;&nbsp;';
                            </c:if>
                            <c:if test="${from ne 'home'}">
                                domHtml += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="查看摄像头监控" onclick="openCamera(\''+row.id+ '\','+null+')">监控</a>';
                            </c:if>
                            <c:if test="${from eq 'home'}">
                                domHtml += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="查看摄像头监控" onclick="openCamera(\''+row.id+ '\',\'home\')">监控</a>';
                            </c:if>

                            return domHtml;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#cameraForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val

                        });
                    });

                    var formSelectArray = $("#cameraForm select");
                    $.each(formSelectArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val

                        });
                    });
                }

            });
            $("#findCamera").click(function () {
                cameraTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('cameraForm');
            })
        });

        function delCamera(id) {
            var r=confirm("确认删除？")
            if (r==false){
                return
            }
            var param = {
                id: id
            }

            $.ajax({
                contentType: "application/json",
                url : path+'/camera/deleteCamera',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        cameraTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

        function openCamera(id,from) {
            var uri = '';
            if (from == null)
            {
                uri = path +'/camera/toCameraMonitor?id='+id;
                window.location.href=uri;
            }
            else
            {
                uri = path +'/camera/toCameraMonitor?id='+id+'&from=home';
                window.open(uri);
            }


        }

        function getSites() {
            var bidId= $("#bidId").val();
            var param = {'bidId': bidId};
            $("#siteId").val(null).trigger("change");
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/site/getSites',
                type : 'POST',
                data: JSON.stringify(param),
                success:function (data) {
                    var siteNum= data.length;
                    var option = "<option value=''>请选择</option>";
                    if(siteNum > 0){
                        for(var i = 0; i < siteNum; i++){
                            option += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
                        }
                    }
                    $("#siteId").html(option);
                },
                error:function(e) {
                    alert("系统异常，请稍候重试！");
                }
            });
        }

    </script>

</head>
<body>

<c:if test="${from ne 'home'}">
    <div class="bg-white item_title" style="padding-right: 0px;">
        摄像头管理
        <a href="<%=request.getContextPath() %>/site/toSiteList" class="btn btn-md1 btn-primary title-btn">上传摄像头布局图</a>
    </div>
</c:if>

<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">
        <div class="row form-group">
            <form id="cameraForm">
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>所属标段</label>
                        <select class="form-control input-sm select2" name="bidId" id="bidId" onchange="getSites()">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.bidList}" var="item">
                                <c:choose>
                                    <c:when test="${item.id eq bidId}">
                                        <option value="${item.id}" selected>${item.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.id}">${item.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>所属站点</label>
                        <select class="form-control input-sm select2" name="siteId" id="siteId">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.siteList}" var="item">
                                <c:choose>
                                    <c:when test="${item.id eq siteId}">
                                        <option value="${item.id}" selected>${item.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.id}">${item.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">

                    </div>
                </div>
                <div class="col-xs-3 text-right">
                    <a id="findCamera" href="javascript:;" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-search"></i>&nbsp;&nbsp;--%>查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                    <c:if test="${from ne 'home'}">
                        <a id="addCamera" href="<%=request.getContextPath() %>/camera/toCameraForm?opt=A" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-plus"></i>&nbsp;&nbsp;--%>新增</a>
                    </c:if>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="cameraTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>
</body>

</html>
