<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>摄像头详情</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <script>
        var webVideo;
        $(function () {
            webVideo = new WebVideo();
            webVideo.divPlugin = "divPlugin";
            webVideo.iWndowType =1;
            webVideo.init();
            webVideo.ip = '${camera.ip}';
            webVideo.username = '${camera.loginUserName}';
            webVideo.password = '${camera.loginPassword}';
            webVideo.g_iWndIndex = 10;
            webVideo.clickLogin();

        })
        function closePage() {
            //webVideo.clickCapturePic();
            var param = {
                ip: '${camera.ip}',
                innerPort: '${camera.innerPort}',
                loginUserName: '${camera.loginUserName}',
                loginPassword: '${camera.loginPassword}',
            }
            $("body").mask("抓拍中，请稍等...");
            $.ajax({
                contentType: "application/json",
                url : path+'/camera/saveCapturePic',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    $("body").unmask();
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        //notySuccess(data.resultMsg);
                    }else{
                        //notyError(data.resultMsg);
                    }
                    window.opener=null;window.open('','_self');window.close();
                },
                error: function (request, status, error) {
                    $("body").unmask();
                    notyError(JSON.stringify(error))
                    //window.opener=null;window.open('','_self');window.close();
                }
            });

        }

        function back(uri) {

            var param = {
                ip: '${camera.ip}',
                innerPort: '${camera.innerPort}',
                loginUserName: '${camera.loginUserName}',
                loginPassword: '${camera.loginPassword}',
            }
            $("body").mask("抓拍中，请稍等...");
            $.ajax({
                contentType: "application/json",
                url : path+'/camera/saveCapturePic',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    $("body").unmask();
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        //notySuccess(data.resultMsg);
                    }else{
                        //notyError(data.resultMsg);
                    }
                    window.location.href=uri;
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                    $("body").unmask();
                }
            });
            //
        }

        function closePageAndCapture() {
            webVideo.clickCapturePic();
            var param = {
                ip: '${camera.ip}',
                path:webVideo.path,
                picName:webVideo.picName
            }
            $.ajax({
                contentType: "application/json",
                url : path+'/camera/saveCapturePic',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        //notySuccess(data.resultMsg);
                    }else{
                        //notyError(data.resultMsg);
                    }
                    window.opener=null;window.open('','_self');window.close();
                },
                error: function (request, status, error) {
                    //notyError(JSON.stringify(error))
                    window.opener=null;window.open('','_self');window.close();
                }
            });

        }

        function backAndCapture(uri) {
            var d = new Date;
            var year = d.getFullYear();
            var month = (d.getMonth() + 1) >=10? (d.getMonth() + 1) : "0" + (d.getMonth() + 1);
            var date = d.getDate() >=10 ? d.getDate() : "0" + d.getDate();
            var curDate = year + month + date;

            var baseDiv = document.querySelector('#divPlugin');
            var width = baseDiv.offsetWidth;//div宽
            var height = baseDiv.offsetHeight;//div高
            var canvas = document.createElement("canvas");

            var opts = {
                canvas: canvas,
                logging: false,//是否输出日志
                width: width,
                height: height
            };

            html2canvas(baseDiv,opts).then(function (canvas) {
                var dataImg = canvas.toDataURL();
                var parts = dataImg.split(';base64,');
                var param = {
                    ip: '${camera.ip}',
                    base64:parts[1],
                    picName:'${camera.ip}' + '.png'
                }
                $.ajax({
                    contentType: "application/json",
                    url : path+'/camera/saveCapturePic',
                    type : 'POST',
                    data: JSON.stringify(param),
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            //notySuccess(data.resultMsg);
                        }else{
                            //notyError(data.resultMsg);
                        }
                    },
                    error: function (request, status, error) {
                        //notyError(JSON.stringify(error))
                    }
                });
                window.location.href=uri;
            })
        }

    </script>
</head>
<body>
<div class="bg-white item_title">
    <c:if test="${from ne 'home'}">
        <a href="${ctx}/camera/toCameraList" title="点击返回至列表">摄像头管理</a>
        <i class="fa fa-angle-double-right"></i>
        摄像头监控
    </c:if>

</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="cameraForm">
        <div class="form-body mb10">
            <div id = "divPlugin" class="plugin text-center" style="height: 540px; width: 960px;margin:0 auto">
            </div>
        </div>

        <c:if test="${from ne 'home'}">
            <div class="form-actions mt10">
                <div class="row">
                    <div class="text-center">
                        <a href="javascript:;" onclick="back('${ctx}/camera/toCameraList')" title="返回列表" class="btn btn-md btn-default"><i
                                class="fa fa-reply"></i> 返回并抓拍</a>
                    </div>
                </div>
            </div>
        </c:if>

        <c:if test="${from eq 'home'}">
            <div class="form-actions mt10">
                <div class="row">
                    <div class="text-center">
                        <a onclick="closePage()" title="关闭当前页面" class="btn btn-md btn-default"><i
                                class="fa fa-reply"></i> 关闭并抓拍</a>
                    </div>
                </div>
            </div>
        </c:if>

        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <a href="javascript:window.opener=null;window.open('','_self');window.close();" title="关闭当前页面" class="btn btn-md btn-default"><i
                            class="fa fa-close"></i> 直接关闭</a>
                </div>
            </div>
        </div>


    </form>

</div>

</body>
<%--<script type="text/javascript" src="${ctx }/assets/js/html2canvas.js"></script>--%>
<script type="text/javascript" src="${ctx }/webcomponents/codebase/webVideoCtrl.js"></script>
<script type="text/javascript" src="${ctx }/webcomponents/codebase/webVideo.js"></script>
</html>
