<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>摄像头监控列表</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <script type="text/javascript" src="${ctx }/assets/js/xlPaging.js" charset="UTF-8"></script>
    <style>
        /*.cameralist .thumbnail img{*/
            /*width: 100%;*/
        /*}*/
        .cameralist .thumbnail .imgwrap {
            position: relative;
            padding-bottom: 56.25%;
        }
        .cameralist .thumbnail .imgwrap img {
            width: 100%;
            height: auto;
            max-height: 100%;
            position: absolute;
            left: 0;
            top: 0;
        }
        a.thumbnail:hover, a.thumbnail:focus, a.thumbnail.active {
            border-color: #1e90e5;
        }
        a.thumbnail .title {
            color: #000;
            text-align: center;
            white-space: nowrap;
            text-overflow: ellipsis;
            width: 100%;
            line-height: 28px;
        }
        #page {
            margin: 10px auto;
            color: #666;
            display: block;
            text-align: right;
        }

        #page li {
            display: inline-block;
            min-width: 30px;
            height: 28px;
            cursor: pointer;
            color: #666;
            font-size: 13px;
            line-height: 28px;
            background-color: #f9f9f9;
            border: 1px solid #dce0e0;
            text-align: center;
            margin: 0 4px;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }

        .xl-nextPage,
        .xl-prevPage {
            width: 60px;
            color: #1e90e5;
            height: 28px;
        }

        #page li.xl-disabled {
            opacity: .5;
            cursor: no-drop;
        }

        #page li.xl-active {
            background-color: #1e90e5;
            border-color: #1e90e5;
            color: #FFF
        }
    </style>
    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var cameraTable ;
        var showed = false
        $(function () {
            $("#findCamera").click(function () {
                //showCameras(1, 16, true)
                getCameras();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('cameraForm');
                showCameras(1, 16, true)
            })
            getCameras()
        });
        var cameraList = []
        var showCameraList = []
        function showCameras(pageNo, pageSize, reloadPage) {
            pageNo = pageNo ? pageNo : 1
            pageSize = pageSize ? pageSize : 16
            var siteId = $('#siteId option:checked').val()
            showCameraList = cameraList.filter(function(camera) {
                return siteId == '' ? camera : camera.siteId == siteId
            })
            if (reloadPage) {
                $("#page").paging({
                    pageNum: parseInt(showCameraList.length / 16) + 1, //总页码
                    callback: function(num) { //回调函数,num为当前页码
                        showCameras(num);
                    }
                })
            }
            var html = ''
            var begin = (pageNo - 1) * pageSize
            var end = pageNo * pageSize > showCameraList.length ? showCameraList.length : pageNo * pageSize
            for (var i = begin; i < end; i++) {
                if (isEmpty(showCameraList[i].fileName) )
                    html += '<div class="col-xs-3"><a href="javascript:;" onclick="openCamera('+showCameraList[i].id+')" class="thumbnail"><div class="imgwrap"><img src="${ctx}/assets/img/camera.jpg" title="'+showCameraList[i].name+'"></div><div class="title">'+ showCameraList[i].siteName +'-'+showCameraList[i].place+'</div></a></div>'
                else
                {
                    var d = encodeURI(encodeURI(showCameraList[i].fileName,"utf-8"),"utf-8");
                    html += '<div class="col-xs-3"><a href="javascript:;" onclick="openCamera('+showCameraList[i].id+')" class="thumbnail"><div class="imgwrap"><img src="<%=request.getContextPath()%>/camera/showSnapPic?path='+showCameraList[i].path+'&fileName='+d+'" title="'+showCameraList[i].name+'"></div><div class="title">'+ showCameraList[i].siteName +'-'+showCameraList[i].place+'</div></a></div>'
                }

            }
            $('.cameralist .row').html(html)
        }
        function getCameras() {
            var siteId = $('#siteId option:checked').val()
            var cameraUrl = path + '/camera/getCameraThumbnailList?usable=1&isStarted=1&isFinished=0&siteId='+siteId
            $.ajax({
                contentType: "application/json",
                url : cameraUrl,
                type : 'GET',
                success : function (res) {
                    cameraList = res.data.data
                    showCameras(1, 16, true)
                }
            })
        }

        function openCamera(id,from) {
            var uri = '';
//            if (from == null)
//            {
//                uri = path +'/camera/toCameraMonitor?id='+id;
//                window.location.href=uri;
//            }
//            else
//            {
                uri = path +'/camera/toCameraMonitor?id='+id+'&from=home';
                window.open(uri);
//            }
        }
        function getSites() {
            var bidId= $("#bidId").val();
            var param = {'bidId': bidId};
            $("#siteId").val(null).trigger("change");
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/site/getSites',
                type : 'POST',
                data: JSON.stringify(param),
                success:function (data) {
                    var siteNum= data.length;
                    var option = "<option value=''>请选择</option>";
                    if(siteNum > 0){
                        for(var i = 0; i < siteNum; i++){
                            option += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
                        }
                    }
                    $("#siteId").html(option);
                },
                error:function(e) {
                    alert("系统异常，请稍候重试！");
                }
            });
        }

    </script>

</head>
<body>

<c:if test="${from ne 'home'}">
    <div class="bg-white item_title">
        摄像头监控列表
    </div>
</c:if>

<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">
        <div class="row form-group">
            <form id="cameraForm">
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>所属标段</label>
                        <select class="form-control input-sm select2" name="bidId" id="bidId" onchange="getSites()">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.bidList}" var="item">
                                <c:choose>
                                    <c:when test="${item.id eq bidId}">
                                        <option value="${item.id}" selected>${item.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.id}">${item.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>所属站点</label>
                        <select class="form-control input-sm select2" name="siteId" id="siteId">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.siteList}" var="item">
                                <c:choose>
                                    <c:when test="${item.id eq siteId}">
                                        <option value="${item.id}" selected>${item.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.id}">${item.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">

                    </div>
                </div>
                <div class="col-xs-3 text-right">
                    <a id="findCamera" href="javascript:;" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-search"></i>&nbsp;&nbsp;--%>查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                    <c:if test="${from ne 'home'}">
                        <a id="addCamera" href="<%=request.getContextPath() %>/camera/toCameraForm?opt=A" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-plus"></i>&nbsp;&nbsp;--%>新增</a>
                    </c:if>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="cameraTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
    <div class="cameralist container-fluid">
        <div class="row">
        </div>
        <nav id="page" aria-label="Page navigation">
        </nav>
    </div>
</div>
</body>
</html>
