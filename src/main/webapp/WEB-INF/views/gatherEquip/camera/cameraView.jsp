<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>摄像头详情</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <script>
        $(function () {
        })
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/camera/toCameraList" title="点击返回至列表">摄像头管理</a>
    <i class="fa fa-angle-double-right"></i>
    摄像头信息查看
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="cameraForm">
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>摄像头名称</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,maxSize[50]]"  id = "name" name="name"  placeholder="" value="${camera.name}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>所属站点</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="siteId" id="siteId" disabled>
                        <option value="">请选择</option>
                        <c:forEach items="${siteList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq camera.siteId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>位置</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required]"  id = "place"  name="place" placeholder="" value="${camera.place}" type="text" readonly>
                </div>


                <label class="col-md-2 control-label"><span class="alarmstar">*</span>ip</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,custom[ipv4]]"  id = "ip"  name="ip" placeholder="" value="${camera.ip}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">是否可用</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]"  id = 'usable' name="usable" disabled>
                        <option value="">请选择</option>
                        <option value="1" <c:if test="${camera.usable eq 1}">selected</c:if>>是</option>
                        <option value="0" <c:if test="${camera.usable eq 0}">selected</c:if>>否</option>
                    </select>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>是否大屏显示</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]"  id = 'defaultShow' name="defaultShow" disabled>
                        <option value="">请选择</option>
                        <option value="1" <c:if test="${camera.defaultShow eq 1}">selected</c:if>>是</option>
                        <option value="0" <c:if test="${camera.defaultShow eq 0}">selected</c:if>>否</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">登录名</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required]"  id = "loginUserName"  name="loginUserName" placeholder="" value="${camera.loginUserName}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">密码</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required]"  id = "loginPassword"  name="loginPassword" placeholder="" value="${camera.loginPassword}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">x坐标</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[custom[number]]"  id = "x"  name="x" placeholder="" value="${camera.x}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">y坐标</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[custom[number]]"  id = "y"  name="y" placeholder="" value="${camera.y}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">编号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "number"  name="number" placeholder="" value="${camera.number}" type="text" readonly>
                </div>
                <label class="col-md-2 control-label">内网端口</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "innerPort"  name="innerPort" placeholder="" value="${camera.innerPort}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>摄像头名称（APP用）</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm]" id = "appName" name="appName"  placeholder="" value="${camera.appName}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">摄像头排序（APP用）</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm]" id = "appOrder" name="appOrder"  placeholder="" value="${camera.appOrder}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">外网ip（APP用）</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "outerIp"  name="outerIp" placeholder="" value="${camera.outerIp}" type="text" readonly>
                </div>
                <label class="col-md-2 control-label">外网端口（APP用）</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "outerPort"  name="outerPort" placeholder="" value="${camera.outerPort}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">通道号（APP用）</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "appChannel"  name="appChannel" placeholder="" value="${camera.appChannel}" type="text" readonly>
                </div>
            </div>
        </div>

        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <a href="${ctx}/camera/toCameraList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>
                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
