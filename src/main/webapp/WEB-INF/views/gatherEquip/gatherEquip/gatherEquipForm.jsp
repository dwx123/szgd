<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>采集设备管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null
            });

            $('#beginTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#saveGatherEquip').click(function () {

                var valid = $("#gatherEquipForm").validationEngine("validate");
                if (!valid)
                    return;
                disabledAllBtn(true);
                var param = serializeObject('#gatherEquipForm');
                var otherParam = {};
                var jsonString = JSON.stringify($.extend(param, otherParam));
                $.ajax({
                    contentType: "application/json",
                    url : '${ctx}/gatherEquip/saveGatherEquip',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);

                        }else{
                            notyError(data.resultMsg);
                            disabledAllBtn(false);
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                        disabledAllBtn(false);
                    }
                });

            });
        });

    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/gatherEquip/toGatherEquipList" title="点击返回至列表">采集设备管理</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        采集设备增加
    </c:if>
    <c:if test="${opt == 'E'}">
        采集设备编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="gatherEquipForm">
        <input type="hidden" name="id" value="${gatherEquip.id}" id="id">

        <input type="hidden" id="opt" name="opt" value="${opt}">

        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>采集设备名称</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,maxSize[50]]"  id = "name" name="name"  placeholder="" value="${gatherEquip.name}" type="text">
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>所属站点</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="siteId" id="siteId">
                        <option value=""> 请选择</option>
                        <c:forEach items="${siteList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq gatherEquip.siteId || item.defaultShow eq '1'}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>ip</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,custom[ipv4]]"  id = "ip"  name="ip" placeholder="" value="${gatherEquip.ip}" type="text">
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>进出</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]"  id = 'passFlag' name="passFlag">
                        <option value="">请选择</option>
                        <option value="1" <c:if test="${gatherEquip.passFlag eq 1}">selected</c:if>>进</option>
                        <option value="2" <c:if test="${gatherEquip.passFlag eq 2}">selected</c:if>>出</option>
                    </select>
                </div>

            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>类型</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="type" id="type">
                        <option value="">请选择</option>
                        <c:forEach items="${sessionScope.GATHER_EQUIP_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq gatherEquip.type}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">编号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "equipNumber"  name="equipNumber" placeholder="" value="${gatherEquip.equipNumber}" type="text">
                </div>
            </div>

            <div class="row">

                <label class="col-md-2 control-label">密码</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "password"  name="password" placeholder="" value="${gatherEquip.password}" type="text" readonly>
                </div>
                <label class="col-md-2 control-label">位置</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[]"  id = "place"  name="place" placeholder="" value="${camera.place}" type="text">
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">厂家</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "vender"  name="vender" placeholder="" value="${gatherEquip.vender}" type="text">
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">回调接口</label>
                <div class="col-md-8 pt7">
                    <input class="form-control input-sm"  id = "callBack"  name="callBack" placeholder="" value="${gatherEquip.callBack}" type="text" readonly>
                </div>

            </div>
        </div>
        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveGatherEquip"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/gatherEquip/toGatherEquipList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
