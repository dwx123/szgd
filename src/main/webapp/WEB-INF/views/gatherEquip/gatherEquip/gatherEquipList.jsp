<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>采集设备管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var gatherEquipTable ;
        $(function () {
            gatherEquipTable = $("#gatherEquipTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                //锁定行
                scrollY:"450px",
                //锁定列
                scrollX:true,
                scrollCollapse: true,
                fixedColumns: {
                    leftColumns: 1,
                    rightColumns:1
                },
                sAjaxSource: path + "/gatherEquip/getGatherEquipList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'name', 'sTitle': '名称', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'equipNumber', 'sTitle': '编号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'place', 'sTitle': '位置', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'passFlag', 'sTitle': '进出', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'ip', 'sTitle': 'ip', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'siteName', 'sTitle': '所属站点', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'typeName', 'sTitle': '类型', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'password', 'sTitle': '密码', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'callBack', 'sTitle': '回调接口', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'},
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        aTargets: [3],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var value = "" ;
                            if (data == 1) {
                                value = '进';
                            }
                            if (data == 2) {
                                value = '出';
                            }
                            return value;

                        }
                    },
                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="'+ path +'/gatherEquip/toGatherEquipView?id='+ row.id +'" title="查看采集设备信息">查看</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/gatherEquip/toGatherEquipForm?opt=E&id='+ row.id +'" title="修改采集设备信息">编辑</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除采集设备信息" onclick="delGatherEquip(\''+ row.id + '\')">删除</a>&nbsp;&nbsp;';
                            if(row.type == 'GATHER_EQUIP_FACE')
                            {
                                domHtml += '<a class="btn-sm btn-smd btn btn-reset" href="javascript:;" title="设置密码" onclick="showPasswordModal(\''+ row.id +'\',\''+row.password +'\',\''+row.ip+ '\')">设置密码</a>&nbsp;&nbsp;';
                                domHtml += '<a class="btn-sm btn-smd btn btn-reset" href="javascript:;" title="设置回调接口" onclick="showCallBackModal(\''+ row.password +'\',\''+row.callBack +'\',\''+row.ip+'\',\''+row.id+ '\')">设置回调接口</a>';
                            }
                            return domHtml;
                        }
                    }
                ],
                fnRowCallback : function(nRow, aData, iDisplayIndex) {
                    $('td:eq(9)', nRow).attr('style', 'text-align: left;');
                },
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#gatherEquipForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val

                        });
                    });
                }

            });
            $("#findGatherEquip").click(function () {
                gatherEquipTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('gatherEquipForm');
            })
        });

        function delGatherEquip(id) {
            var r=confirm("确认删除？")
            if (r==false){
                return
            }
            var param = {
                id: id
            }

            $.ajax({
                contentType: "application/json",
                url : path+'/gatherEquip/deleteGatherEquip',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        gatherEquipTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }


        function showPasswordModal(id,password,ip) {
            $('#oldPassword').val(password)
            $('#id').val(id)
            $('#ip').val(ip)
            $('#passwordModal').modal('show');

        }
        function setPassword(id) {
            var r=confirm("确认重新设置密码吗？")
            if (r==false){
                return
            }
            if ($('#oldPassword').val() == '' || $('#newPassword').val() == '')
            {
                alert('原密码和新密码都要输入！');
                return;
            }
            if ($('#ip').val() == '')
            {
                alert('没有设置ip！');
                return;
            }

            var param = {
                oldPassword: $('#oldPassword').val(),
                newPassword: $('#newPassword').val(),
                ip: $('#ip').val(),
                id:$('#id').val()
            }

            $.ajax({
                contentType: "application/json",
                url : path+'/gatherEquip/setPassword',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        gatherEquipTable.ajax.reload();
                        $('#passwordModal').modal('hide');
                    }else{
                        notyError(data.resultMsg);
                    }
                    alert(data.resultMsg);
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

        function showCallBackModal(password,callBack,ip,id) {
            $('#password').val(password)
            $('#callBack').val(callBack)
            $('#euip_ip').val(ip)
            $('#euip_id').val(id)
            $('#callBackModal').modal('show');

        }
        function setCallBack() {
            var r=confirm("确认设置回调接口吗？")
            if (r==false){
                return
            }
            /*if ($('#callBack').val() == '')
            {
                alert('请输入回调接口！');
                return;
            }*/
            if ($('#password').val() == '')
            {
                alert('没有设置密码！');
                return;
            }
            if ($('#euip_ip').val() == '')
            {
                alert('没有设置ip！');
                return;
            }

            var param = {
                callBack: $('#callBack').val(),
                password: $('#password').val(),
                ip: $('#euip_ip').val(),
                id: $('#euip_id').val()
            }

            $.ajax({
                contentType: "application/json",
                url : path+'/gatherEquip/setCallBack',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        gatherEquipTable.ajax.reload();
                        $('#callBackModal').modal('hide');
                    }else{
                        notyError(data.resultMsg);
                    }
                    alert(data.resultMsg);
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

    </script>

</head>
<body>

<div class="bg-white item_title">
    采集设备管理
</div>
<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">
        <div class="row form-group">
            <form id="gatherEquipForm">
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>所属站点</label>
                        <select class="form-control input-sm select2" name="siteId" id="siteId">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.siteList}" var="item">
                                <c:choose>
                                    <c:when test="${item.id eq siteId}">
                                        <option value="${item.id}" selected>${item.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.id}">${item.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>采集设备名称</label>
                        <input class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="col-xs-6 text-right">
                    <a id="findGatherEquip" href="javascript:;" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-search"></i>&nbsp;&nbsp;--%>查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                    <a id="addGatherEquip" href="<%=request.getContextPath() %>/gatherEquip/toGatherEquipForm?opt=A" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-plus"></i>&nbsp;&nbsp;--%>新增</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="gatherEquipTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>
<!-- Modal1 -->
<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">设置密码</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="passwordForm" >
                    <input type="hidden" name="id" value="" id="id">
                    <input type="hidden" name="ip" value="" id="ip">
                    <div class="row">
                        <label class="col-md-2 control-label">原密码</label>
                        <div class="col-md-7">
                            <input class="form-control form-control-inline input-sm" id = "oldPassword" name="oldPassword" size="16" value="" type="text">
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-2 control-label">新密码</label>
                        <div class="col-md-7">
                            <input class="form-control form-control-inline input-sm" id = "newPassword" name="newPassword" size="16" value="" type="text">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-md btn btn-primary" onclick="setPassword()">提交</button>
                <button type="button" class="btn btn-default btn-md" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="callBackModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="callBackModalLabel">设置回调接口</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="callBackForm" >
                    <input type="hidden" name="password" value="" id="password">
                    <input type="hidden" name="ip" value="" id="euip_ip">
                    <input type="hidden" name="id" value="" id="euip_id">
                    <div class="row">
                        <label class="col-md-2 control-label">回调接口</label>
                        <div class="col-md-7">
                            <input class="form-control form-control-inline input-sm" id = "callBack" name="callBack" size="200" value="" type="text">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-md btn btn-primary" onclick="setCallBack()">提交</button>
                <button type="button" class="btn btn-default btn-md" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
