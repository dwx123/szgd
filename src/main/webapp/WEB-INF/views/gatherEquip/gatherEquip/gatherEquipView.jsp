<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>采集设备详情</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <script>
        $(function () {
        })
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/gatherEquip/toGatherEquipList" title="点击返回至列表">采集设备管理</a>
    <i class="fa fa-angle-double-right"></i>
    采集设备信息查看
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="gatherEquipForm">
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar"></span>采集设备名称</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "name" name="name"  placeholder="" value="${gatherEquip.name}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar"></span>所属站点</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "siteName"  name="siteName" placeholder="" value="${gatherEquip.siteName}" type="text" readonly>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar"></span>ip</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "ip"  name="ip" placeholder="" value="${gatherEquip.ip}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar"></span>进出</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2"  id = 'passFlag' name="passFlag" disabled>
                        <option value="">请选择</option>
                        <option value="1" <c:if test="${gatherEquip.passFlag eq 1}">selected</c:if>>进</option>
                        <option value="2" <c:if test="${gatherEquip.passFlag eq 2}">selected</c:if>>出</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar"></span>类型</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "typeName"  name="typeName" placeholder="" value="${gatherEquip.typeName}" type="text" readonly>
                </div>
                <label class="col-md-2 control-label">编号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "equipNumber"  name="equipNumber" placeholder="" value="${gatherEquip.equipNumber}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">密码</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "password"  name="password" placeholder="" value="${gatherEquip.password}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">位置</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "place"  name="place" placeholder="" value="${camera.place}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">厂家</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "vender"  name="vender" placeholder="" value="${gatherEquip.vender}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">回调接口</label>
                <div class="col-md-8 pt7">
                    <input class="form-control input-sm"  id = "callBack"  name="callBack" placeholder="" value="${gatherEquip.callBack}" type="text" readonly>
                </div>

            </div>
        </div>

        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <a href="${ctx}/gatherEquip/toGatherEquipList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>
                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
