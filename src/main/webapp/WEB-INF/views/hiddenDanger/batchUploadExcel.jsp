<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <title>
        <c:if test="${modalFlag eq 'report'}"> 批量隐患上报</c:if>
        <c:if test="${modalFlag eq 'rectify'}"> 批量隐患整改</c:if>
    </title>
    <meta charset="utf-8"/>
</head>
<body>
<div class="opend_modal" id="uploadExcelModel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <c:if test="${modalFlag eq 'report'}">批量隐患上报</c:if>
                    <c:if test="${modalFlag eq 'rectify'}">批量隐患整改</c:if>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form_area" method="post" enctype="multipart/form-data" id="uploadExcelForm">
                        <div class="col-md-8" style="padding-left: 8%;">
                            <input style="padding: 0;" type="file" name="file" id="excelFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        </div>
                        <div class="col-xs-4">
                            <button type="button" onclick="uploadExcel()" class="btn btn-md1 btn-primary" title="批量导入隐患信息" id="impBtn">导&nbsp;&nbsp;入</button>
                            <button type="button" onclick="downloadTemplate()" class="ml5 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                        </div>
                        <div class="col-md-5 mt10" style="padding-left: 7%">
                            <span class="label" id="resultMsg">
                                <span class="label" style="color: black;">
                                    <c:if test="${modalFlag eq 'report'}">
                                        说明：不要改动模板；只需填写A-I列（编号-隐患描述）；将J-O列的内容清除，标题不能删。<br/>
                                        <span style="padding-left: 5.5%;">编号命名规则(保证其唯一性)：当前时间(精确到秒)+数字(依次递增)。</span><br/>
                                        <span style="padding-left: 5.5%;">
                                            例：202033132828<span style="color: red">1</span>、
                                            202033132828<span style="color: red">2</span>、
                                            202033132828<span style="color: red">3</span> (时间为2020-03-03 13:28:28)
                                        </span>
                                    </c:if>
                                    <c:if test="${modalFlag eq 'rectify'}">
                                        说明：不要改动模板；只需填写J-L列（整改人-整改内容）。
                                    </c:if>
                                </span>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>




<script>
    var modalFlag = "${modalFlag}";
    var mUrl = "";
    if(modalFlag == "report"){
        mUrl = path + '/hiddenDanger/batchUploadExcel';
    } else if(modalFlag == "rectify"){
        mUrl = path + '/hiddenDanger/batchUploadRectifyExcel';
    }
    function uploadExcel() {
        if (checkUploadForm()) {
            $("#impBtn").attr({"disabled":"disabled"});
            function resutlMsg(msg) {
                if (msg =="文件导入失败！"){
                    $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                }else{
                    if(msg.indexOf("#WEBROOT#") != -1){
                        msg  = msg.replace("#WEBROOT#", path +"/upload");
                    }
                    $("#resultMsg").html("<font color='#006400'>" + msg +"</font>");
                }
                $("#impBtn").removeAttr("disabled");
                $("#hiddenDangerTable").DataTable().ajax.reload();
            }
            function errorMsg() {
                $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                $("#impBtn").removeAttr("disabled");
            }
            $('#uploadExcelForm').ajaxSubmit({
                url: mUrl,
                dataType: 'text',
                success: resutlMsg,
                error: errorMsg
            });
        }
    }

    function downloadTemplate() {
        var  queryParams = 'templateName=隐患批量导入模板.xls';
        var url =path + "/upload/downloadTemplate?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
        window.open(url);
    }

    function checkUploadForm() {
        var fileDir = $("#excelFile").val();
        if ("" == fileDir) {
            notyWarning("选择需要导入的文件！");
            return false;
        }
        var suffix = fileDir.substr(fileDir.lastIndexOf("."));
        if (".xls" != suffix && ".xlsx" != suffix) {
            notyWarning("选择Excel格式的文件导入！");
            return false;
        }
        return true;
    }
</script>
</body>
</html>