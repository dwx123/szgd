<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <title>
        <c:if test="${modalFlag eq 'report'}"> 批量上传隐患照片</c:if>
        <c:if test="${modalFlag eq 'rectify'}"> 批量上传整改照片</c:if>
    </title>
    <meta charset="utf-8"/>
</head>
<body>
<div class="opend_modal" id="uploadPicModel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <c:if test="${modalFlag eq 'report'}">批量上传隐患照片</c:if>
                    <c:if test="${modalFlag eq 'rectify'}">批量上传整改照片</c:if>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form_area" method="post" enctype="multipart/form-data" id="picForm">
                        <div class="col-md-9" style="padding-left: 8%;">
                            <input style="padding: 0;" type="file" name="picAttach" id="picAttach" class="filePrew form-control" multiple/>
                        </div>
                        <div class="col-md-2">
                            <button id="uploadPic" type="button" onclick="uploadHiddenPic()" class="btn btn-md1 btn-primary" title="点击批量导入隐患照片">上&nbsp;&nbsp;传</button>
                        </div>
                        <div class="col-md-11 mt10" style="padding-left: 8%"><span style="color: red;font-size: 12px;" id="resultMsg2">说明：要求照片的文件名和隐患编号一致。</span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>




<script>
    var modalFlag = "${modalFlag}";
    var mUrl = "";
    if(modalFlag == "report"){
        mUrl = path + '/hiddenDanger/uploadReportPic';
    } else if(modalFlag == "rectify"){
        mUrl = path + '/hiddenDanger/uploadRectifyPic';
    }

    function checkUploadPicForm() {
        var fileDir = $("#picAttach").val();
        if ("" == fileDir) {
            notyWarning("请选择需要导入的照片！");
            return false;
        }
        return true;
    }

    function uploadHiddenPic() {
        if (checkUploadPicForm()) {
            $("#uploadPic").attr({"disabled":"disabled"});
            function resutlMsg(msg) {
                if (msg =="照片导入失败！"){
                    $("#resultMsg2").html("<span style='color: red;'>" + msg +"</span>");
                }else{
                    $("#resultMsg2").html("<span style='color: black;'>" + msg +"</span>");
                }
                $("#uploadPic").removeAttr("disabled");
                $("#hiddenDangerTable").DataTable().ajax.reload();
            }
            function errorMsg() {
                $("#resultMsg").html("<span style='color: red;'>" + msg +"</span>");
                $("#uploadPic").removeAttr("disabled");
            }
            $('#picForm').ajaxSubmit({
                url: mUrl,
                dataType: 'text',
                success: resutlMsg,
                error: errorMsg
            });
        }
    }
</script>
</body>
</html>