<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>隐患整改</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        var isRectifyAttachChanged = '0';
        var flag = 0;
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear: true
            });

            var userId = "${userInfo.userId}";
            $("#rectifyPerson").val(userId).trigger("change");


            $('#rectifyTime').datetimepicker({
                format: 'yyyy-mm-dd hh:ii',
                autoclose: true,
                minView: 0,
                minuteStep:1,
                todayHighlight: 1,
                startView: 2,
                language: 'zh-CN'
            });

            var rectifyTime = "${hiddenDanger.rectifyTime}";
            if(rectifyTime == null || rectifyTime == ""){
                $("#rectifyTime").datetimepicker("setDate", new Date());
            }

            function getRectifyAttach() {
                var obj = {
                    id:$("#rectifyAttachId").val(),
                    parentId:$('#id').val(),
                    name:$("#rectifyName").val(),
                    type: "ATTACH_PIC",
                    source: "ATTACH_DANGER_RECTIFY",
                    isRectifyAttachChanged :isRectifyAttachChanged
                };
                return obj;
            }

            $('#saveHiddenInfo').click(function () {
                var valid = $("#hiddenInfoForm").validationEngine("validate");
                if (!valid)
                    return;
                var param = {
                    "id": $('#id').val(),
                    "rectifyContent": $('#rectifyContent').val(),
                    "rectifyTime": $('#rectifyTime').val(),
                    "rectifyPerson": $('#rectifyPerson').val(),
                    "theme": "${hiddenDanger.theme}",
                    "siteName": "${hiddenDanger.siteName}"
                };
                var otherParam = {};
                otherParam['rectifyAttach'] = getRectifyAttach();
                var jsonString = JSON.stringify($.extend(param, otherParam));
                console.log(jsonString);
                $.ajax({
                    contentType : "application/json",
                    url : '${ctx}/hiddenDanger/rectifyHiddenDanger',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);
                        }else{
                            notyError(data.resultMsg);
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                    }
                });
            });

            $('#uploadAttach').mousemove(function () {
                if(flag == 0){
                    var attachId = $('#rectifyAttachId').val();
                    if(attachId != null && attachId != ""){
                        alert("再次上传附件后，会覆盖原来的附件!");
                    }
                }
                flag = 1;
            });
        });

        function downloadHiddenAttach() {
            var attachId = $("#hiddenAttachId").val();
            if(attachId==null||attachId==""||attachId=="null"){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+$("#hiddenAttachPath").val()+'&fileName='+$("#attachName").val();
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }

        function previewPic() {
            var attachId = $("#hiddenAttachId").val();
            if(attachId==null||attachId==""||attachId=="null"){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+$("#hiddenAttachPath").val()+'&fileName='+$("#attachName").val();
            var url =path + "/upload/previewPic?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
        function previewRCPic() {
            var attachId = $("#rectifyAttachId").val();
            if(attachId==null||attachId==""||attachId=="null"){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+$("#rectifyAttachPath").val()+'&fileName='+$("#rectifyAttachName").val();
            var url =path + "/upload/previewPic?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }

        function downloadRectifyAttach() {
            var attachId = $("#rectifyAttachId").val();
            if(attachId==null||attachId==""||attachId=="null"){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+$("#rectifyAttachPath").val()+'&fileName='+$("#rectifyAttachName").val();
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }

        function uploadrectifyAttach(e) {
            var reader = new FileReader()
            reader.readAsDataURL(event.target.files[0])
            reader.onloadend = function () {
                var fileId = [];
                var rawId = e.id
                fileId.push(e.id)
                $.ajaxFileUpload({
                    url: "<%=request.getContextPath() %>/upload/uploadFile",
                    type: 'post',
                    secureuri: false,
                    fileElementId: fileId,
                    dataType: 'json',
                    success: function (data, status) {
                        if (data.resultCode == 1) {
                            $("#rectifyName").val(data.data[0]);
                            isRectifyAttachChanged = '1';
                        } else {
                            notyError(data.resultMsg);
                        }
                    },
                    error: function (data, status, e) {
                        console.log(e)
                        notyError(e);
                    }
                });
            }
        }
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/hiddenDanger/toHiddenDangerList" title="点击返回至列表">隐患排查</a>
    <i class="fa fa-angle-double-right"></i>
    隐患整改
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="hiddenInfoForm">
        <input type="hidden" name="id" value="${hiddenDanger.id}" id="id">
        <input type="hidden" id="opt" name="opt" value="${opt}">

        <h4><i class="fa fa-bars"></i> 上报信息</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label">隐患主题</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "theme"  name="theme" placeholder="" value="${hiddenDanger.theme}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">整改期限(天)</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "rectifyDeadline"  name="rectifyDeadline" placeholder="" value="${hiddenDanger.rectifyDeadline}" type="text" readonly>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">标段</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="bidId" id="bidId" disabled>
                        <option value="">请选择</option>
                        <c:forEach items="${bidList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq hiddenDanger.bidId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">车站</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="siteId" id="siteId" disabled>
                        <option value="">请选择</option>
                        <c:forEach items="${siteList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq hiddenDanger.siteId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">上报人</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="reportPerson" id="reportPerson" disabled>
                        <option value="">请选择</option>
                        <c:forEach items="${rpList}" var="item">
                            <c:choose>
                                <c:when test="${item.userId eq hiddenDanger.reportPerson}">
                                    <option value="${item.userId}" selected>${item.userName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.userId}">${item.userName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <label class="col-md-2 control-label">上报时间</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "rectifyBeginTime"  name="rectifyBeginTime" placeholder="" value="${hiddenDanger.rectifyBeginTime}" type="text" readonly>
                </div>
            </div>

            <div class="row">

                <%--<label class="col-md-2 control-label">整改人</label>--%>
                <%--<div class="col-md-3 pt7">--%>
                <%--<select class="form-control input-sm select2" name="rectifyPerson" id="rectifyPerson" disabled>--%>
                <%--<option value="">请选择</option>--%>
                <%--<c:forEach items="${rcList}" var="item">--%>
                <%--<c:choose>--%>
                <%--<c:when test="${item.userId eq hiddenDanger.rectifyPerson}">--%>
                <%--<option value="${item.userId}" selected>${item.userName}</option>--%>
                <%--</c:when>--%>
                <%--<c:otherwise>--%>
                <%--<option value="${item.userId}">${item.userName}</option>--%>
                <%--</c:otherwise>--%>
                <%--</c:choose>--%>
                <%--</c:forEach>--%>
                <%--</select>--%>
                <%--</div>--%>


                <label class="col-md-2 control-label">隐患发现时间</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "findTime"  name="findTime" placeholder="" value="${hiddenDanger.findTime}" type="text" readonly>
                </div>

            </div>

            <div class="row">

                <label class="col-md-2 control-label">隐患照片</label>
                <div id="projectAttach" class="col-md-3 pt7">
                    <input type="hidden" name="hiddenAttachId" value="${hiddenDanger.attachId}" id="hiddenAttachId">
                    <input type="hidden" name="hiddenAttachPath" value="${hiddenDanger.path}" id="hiddenAttachPath">
                    <input type="hidden" name="attachName" value="${hiddenDanger.attachName}" id="attachName">

                    <input class="form-control form-control-inline input-sm" id = "hiddenAttachName" name="hiddenAttachName" size="16" value="${hiddenDanger.attachName}" type="text" readonly>
                </div>
                <div class="col-md-2 pt7">
                    <button type="button" onclick="previewPic()" class="btn-md btn btn-success attachBtnHalf">预览</button>
                    <button type="button" id="downloadAttach" onclick="downloadHiddenAttach()" class="btn-md btn btn-primary attachBtnHalf">下载附件</button>
                </div>


            </div>

            <div class="row pb5">
                <label class="col-md-2 control-label">隐患描述</label>
                <div class="col-md-3 pt7">
                    <textarea class="form-control input-sm" rows="5" id = "description"  name="description" placeholder="" readonly>${hiddenDanger.description}</textarea>
                </div>

            </div>
        </div>

        <h4><i class="fa fa-bars"></i> 整改信息</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>整改人</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="rectifyPerson" id="rectifyPerson">
                        <option value="">请选择</option>
                        <c:forEach items="${rcList}" var="item">
                            <c:choose>
                                <c:when test="${item.userId eq hiddenDanger.rectifyPerson}">
                                    <option value="${item.userId}" selected>${item.userName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.userId}">${item.userName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>整改时间</label>
                <div class="col-md-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[required]"  id = "rectifyTime"  name="rectifyTime" placeholder=""  value="${hiddenDanger.rectifyTime}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>整改照片</label>
                <div id="rectifyAttach" class="col-md-3 pt7">
                    <input type="hidden" name="rectifyAttachId" value="${hiddenDanger.rectifyAttachId}" id="rectifyAttachId">
                    <input type="hidden" name="rectifyAttachPath" value="${hiddenDanger.rectifyPath}" id="rectifyAttachPath">
                    <input type="hidden" name="rectifyAttachName" value="${hiddenDanger.rectifyAttachName}" id="rectifyAttachName">

                    <input class="form-control form-control-inline input-sm validate[required,maxSize[200]]" id = "rectifyName" name="rectifyName" value="${hiddenDanger.rectifyAttachName}" size="16" type="text" readonly>
                </div>
                <div class="col-md-2 pt7">
                    <button type="button" class="btn-md btn btn-primary attachBtnHalf">上传照片</button>
                    <input class="attachHalf" type="file" id="uploadAttach"  name="file" accept="" onchange="uploadrectifyAttach(this)">
                    <button type="button" onclick="previewRCPic()" class="btn-md btn btn-success attachBtnHalf">预览</button>
                </div>
            </div>

            <div class="row pb5">
                <label class="col-md-2 control-label">整改内容</label>
                <div class="col-md-3 pt7">
                    <textarea class="form-control input-sm validate[maxSize[500]]" rows="5" id = "rectifyContent"  name="rectifyContent" placeholder="">${hiddenDanger.rectifyContent}</textarea>
                </div>
            </div>
        </div>

        <h4><i class="fa fa-bars"></i> 确认信息</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label">确认人</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="confirmer" id="confirmer" disabled>
                        <option value="">请选择</option>
                        <c:forEach items="${cfList}" var="item">
                            <c:choose>
                                <c:when test="${item.userId eq hiddenDanger.confirmer}">
                                    <option value="${item.userId}" selected>${item.userName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.userId}">${item.userName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">确认时间</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "rectifyEndTime"  name="rectifyEndTime" placeholder="" value="${hiddenDanger.rectifyEndTime}" type="text" disabled>
                </div>

            </div>

            <div class="row pb5">
                <label class="col-md-2 control-label">确认意见</label>
                <div class="col-md-3 pt7">
                    <textarea disabled class="form-control input-sm" rows="5" id = "confirmAdvice"  name="confirmAdvice" placeholder="">${hiddenDanger.confirmAdvice}</textarea>
                </div>
            </div>
        </div>

        <div class="form-actions mt10 mb5">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveHiddenInfo"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/hiddenDanger/toHiddenDangerList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>
                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
