<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>隐患排查</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        var isHiddenAttachChanged = '0';
        var flag = 0;
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $('#findTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                initialDate:new Date(),
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#rectifyBeginTime').datetimepicker({
                format: 'yyyy-mm-dd hh:ii',
                todayHighlight: 1,
                initialDate:new Date(),
                minView: 0,
                minuteStep:1,
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#rectifyEndTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: 0,
                minuteStep:1,
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            var opt = "${opt}";
            if(opt == "A"){
                $("#rectifyDeadline").val(1);
                var userId = "${userInfo.userId}";
                $("#reportPerson").val(userId).trigger("change");

                $("#findTime").datetimepicker("setDate", new Date());
                $("#rectifyBeginTime").datetimepicker("setDate", new Date());
                console.log($("#rectifyBeginTime").val());
            }

            function getHiddenAttach() {
                var obj = {
                    id:$("#hiddenAttachId").val(),
                    parentId:$('#id').val(),
                    name:$("#hiddenAttachName").val(),
                    type: "ATTACH_PIC",
                    source: "ATTACH_DANGER",
                    isHiddenAttachChanged :isHiddenAttachChanged
                };
                return obj;
            }

            $('#uploadAttach').mousemove(function () {
                if(flag == 0){
                    var attachId = $('#hiddenAttachId').val();
                    if(attachId != null && attachId != ""){
                        alert("再次上传附件后，会覆盖原来的附件!");
                    }
                }
                flag = 1;
            });

            $('#saveHiddenInfo').click(function () {
                var valid = $("#hiddenInfoForm").validationEngine("validate");
                if (!valid)
                    return;
                var param = serializeObject('#hiddenInfoForm');
                var otherParam = {};
                otherParam['hiddenAttach'] = getHiddenAttach();
                var siteName = $("#siteId").find("option:selected").text();
                otherParam['siteName'] = siteName;
                var jsonString = JSON.stringify($.extend(param, otherParam));
                console.log(jsonString);
                $.ajax({
                    contentType : "application/json",
                    url : '${ctx}/hiddenDanger/saveHiddenDanger',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        var resultMsg = data.resultMsg;
                        if(resultCode == 1)
                        {
                            if(resultMsg == "duplicated"){
                                notyError("隐患信息保存失败，数据库中已存在该隐患主题，请重新命名隐患主题");
                            }else{
                                notySuccess(resultMsg);
                                setTimeout(function(){
                                    history.go(-1);
                                },1500);
                            }
                        }else{
                            notyError(resultMsg);
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                    }
                });
            });
        });
        function uploadHiddenAttach(e) {
            var reader = new FileReader()
            reader.readAsDataURL(event.target.files[0])
            reader.onloadend = function () {
                var fileId = [];
                var rawId = e.id
                fileId.push(e.id)
                $.ajaxFileUpload({
                    url: "<%=request.getContextPath() %>/upload/uploadFile",
                    type: 'post',
                    secureuri: false,
                    fileElementId: fileId,
                    dataType: 'json',
                    success: function (data, status) {
                        if (data.resultCode == 1) {
                            $("#hiddenAttachName").val(data.data[0]);
                            isHiddenAttachChanged = '1';
                        } else {
                            notyError(data.resultMsg);
                        }
                    },
                    error: function (data, status, e) {
                        console.log(e)
                        notyError(e);
                    }
                });
            }
        }

        function downloadHiddenAttach() {
            var attachId = $('#hiddenAttachId').val();
            if((attachId==null||attachId==""||attachId=="null")){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+$("#hiddenAttachPath").val()+'&fileName='+$("#attachName").val();
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
        function previewPic() {
            var attachId = $("#hiddenAttachId").val();
            if(attachId==null||attachId==""||attachId=="null"){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+$("#hiddenAttachPath").val()+'&fileName='+$("#attachName").val();
            var url =path + "/upload/previewPic?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
        function getSites() {
            var bidId= $("#bidId").val();
            var param = {'bidId': bidId};
            $("#siteId").val(null).trigger("change");
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/site/getSites',
                type : 'POST',
                data: JSON.stringify(param),
                success:function (data) {
                    var siteNum= data.length;
                    var option = "<option value=''>请选择</option>";
                    if(siteNum > 0){
                        for(var i = 0; i < siteNum; i++){
                            option += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";
                        }
                    }
                    $("#siteId").html(option);
                },
                error:function(e) {
                    alert("系统异常，请稍候重试！");
                }
            });
        }

        function getRectifyPerson() {

            <%--var siteId= $("#siteId").val();--%>
            <%--if(siteId != null && siteId != ""){--%>
                <%--var param = {'siteId': siteId, 'roleId': 'hidden_danger_approver'};--%>
                <%--$.ajax({--%>
                    <%--contentType : "application/json",--%>
                    <%--url : '${ctx}/personnelInfo/getRectifyPerson',--%>
                    <%--type : 'POST',--%>
                    <%--data: JSON.stringify(param),--%>
                    <%--success:function (data) {--%>
                        <%--$("#rectifyPerson").val(null).trigger("change");--%>
                        <%--var personNum= data.special.length;--%>
                        <%--var option = "<option value=''>请选择</option>";--%>
                        <%--if(personNum > 0){--%>
                            <%--for(var i = 0; i < personNum; i++){--%>
                                <%--option += "<option value='"+data.special[i].id+"'>"+data.special[i].name+"</option>";--%>
                            <%--}--%>
                            <%--$("#rectifyPerson").html(option);--%>
                            <%--$("#rectifyPerson").val(data.special[0].id).trigger("change");--%>
                        <%--}else {--%>
                            <%--for(var j = 0; j < data.all.length; j++){--%>
                                <%--option += "<option value='"+data.all[j].id+"'>"+data.all[j].name+"</option>";--%>
                            <%--}--%>
                            <%--$("#rectifyPerson").html(option);--%>
                            <%--$("#rectifyPerson").val(data.all[0].id).trigger("change");--%>
                        <%--}--%>

                        <%--$("#rectifyDeadline").val(1);--%>
                    <%--},--%>
                    <%--error:function(e) {--%>
                        <%--alert("系统异常，请稍候重试！");--%>
                    <%--}--%>
                <%--});--%>
            <%--}--%>
        }
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/hiddenDanger/toHiddenDangerList" title="点击返回至列表">隐患排查</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        隐患上报
    </c:if>
    <c:if test="${opt == 'E'}">
        隐患编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="hiddenInfoForm">
        <input type="hidden" name="id" value="${hiddenDanger.id}" id="id">
        <input type="hidden" id="opt" name="opt" value="${opt}">

        <h4><i class="fa fa-bars"></i> 上报信息</h4>
        <div class="form-body mb10 bg-blue">

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>隐患主题</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,maxSize[50]]"  id = "theme"  name="theme" placeholder="" value="${hiddenDanger.theme}" type="text" <c:if test="${opt == 'E'}">readonly</c:if>>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>整改期限(天)</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,custom[number]]"  id = "rectifyDeadline"  name="rectifyDeadline" placeholder="" value="${hiddenDanger.rectifyDeadline}" type="text">
                </div>
            </div>

            <div class="row">
                <%--<label class="col-md-2 control-label"><span class="alarmstar">*</span>整改人</label>--%>
                <%--<div class="col-md-3 pt7">--%>
                    <%--<select class="form-control input-sm select2 validate[required]" name="rectifyPerson" id="rectifyPerson">--%>
                        <%--<option value="">请选择</option>--%>
                        <%--<c:forEach items="${rcList}" var="item">--%>
                            <%--<c:choose>--%>
                                <%--<c:when test="${item.userId eq hiddenDanger.rectifyPerson}">--%>
                                    <%--<option value="${item.userId}" selected>${item.userName}</option>--%>
                                <%--</c:when>--%>
                                <%--<c:otherwise>--%>
                                    <%--<option value="${item.userId}">${item.userName}</option>--%>
                                <%--</c:otherwise>--%>
                            <%--</c:choose>--%>
                        <%--</c:forEach>--%>
                    <%--</select>--%>
                <%--</div>--%>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>标段</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="bidId" id="bidId" onchange="getSites()">
                        <option value="">请选择</option>
                        <c:forEach items="${bidList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq hiddenDanger.bidId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>车站</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="siteId" id="siteId">
                        <option value="">请选择</option>
                        <c:forEach items="${siteList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq hiddenDanger.siteId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>上报人</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="reportPerson" id="reportPerson">
                        <option value="">请选择</option>
                        <c:forEach items="${rpList}" var="item">
                            <c:choose>
                                <c:when test="${item.userId eq hiddenDanger.reportPerson}">
                                    <option value="${item.userId}" selected>${item.userName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.userId}">${item.userName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>上报时间</label>
                <div class="col-md-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[required]"  id = "rectifyBeginTime"  name="rectifyBeginTime" placeholder=""  value="${hiddenDanger.rectifyBeginTime}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>隐患发现时间</label>
                <div class="col-md-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[required]"  id = "findTime"  name="findTime" placeholder=""  value="${hiddenDanger.findTime}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>隐患照片</label>
                <div id="projectAttach" class="col-md-3 pt7">
                    <input type="hidden" name="hiddenAttachId" value="${hiddenDanger.attachId}" id="hiddenAttachId">
                    <input type="hidden" name="hiddenAttachPath" value="${hiddenDanger.path}" id="hiddenAttachPath">
                    <input type="hidden" name="attachName" value="${hiddenDanger.attachName}" id="attachName">

                    <input class="form-control form-control-inline input-sm validate[required,maxSize[200]]" id = "hiddenAttachName" name="hiddenAttachName" size="16" value="${hiddenDanger.attachName}" type="text" readonly>
                </div>
                <div class="col-md-4 pt7">
                    <button style="width: 22%;" type="button" class="btn-md btn btn-primary attachBtnHalf">上传照片</button>
                    <input style="width: 22%;" class="attachHalf" type="file" id="uploadAttach"  name="file" accept="" onchange="uploadHiddenAttach(this)">
                    <c:if test="${opt == 'E'}">
                        <button style="width: 22%;" id="downloadBtn" type="button" onclick="downloadHiddenAttach()" class="btn-md btn btn-primary attachBtnHalf">下载照片</button>
                        <button style="width: 22%;" type="button" onclick="previewPic()" class="btn-md btn btn-success attachBtnHalf">预览</button>
                    </c:if>
                </div>
            </div>

            <div class="row pb5">

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>隐患描述</label>
                <div class="col-md-3 pt7">
                    <textarea class="form-control validate[required,maxSize[200]]" rows="5" id = "description"  name="description" placeholder="">${hiddenDanger.description}</textarea>
                </div>

            </div>
        </div>

        <h4><i class="fa fa-bars"></i> 整改信息</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label">整改人</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="rectifyPerson" id="rectifyPerson" disabled>
                        <option value="">请选择</option>
                        <c:forEach items="${rcList}" var="item">
                            <c:choose>
                                <c:when test="${item.userId eq hiddenDanger.rectifyPerson}">
                                    <option value="${item.userId}" selected>${item.userName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.userId}">${item.userName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">整改时间</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "rectifyTime"  name="rectifyTime" placeholder="" value="${hiddenDanger.rectifyTime}" type="text" disabled>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">整改照片</label>
                <div id="rectifyAttach" class="col-md-3 pt7">
                    <input type="hidden" name="rectifyAttachId" value="${hiddenDanger.rectifyAttachId}" id="rectifyAttachId">
                    <input type="hidden" name="rectifyAttachPath" value="${hiddenDanger.rectifyPath}" id="rectifyAttachPath">
                    <input type="hidden" name="rectifyAttachName" value="${hiddenDanger.rectifyAttachName}" id="rectifyAttachName">

                    <input class="form-control form-control-inline input-sm" id = "rectifyName" name="rectifyName" value="${hiddenDanger.rectifyAttachName}" size="16" type="text" readonly>
                </div>
            </div>

            <div class="row pb5">
                <label class="col-md-2 control-label">整改内容</label>
                <div class="col-md-3 pt7">
                    <textarea class="form-control" rows="5" id = "rectifyContent"  name="rectifyContent" placeholder="" disabled>${hiddenDanger.rectifyContent}</textarea>
                </div>
            </div>
        </div>

        <h4><i class="fa fa-bars"></i> 确认信息</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label">确认人</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="confirmer" id="confirmer" disabled>
                        <option value="">请选择</option>
                        <c:forEach items="${cfList}" var="item">
                            <c:choose>
                                <c:when test="${item.userId eq hiddenDanger.confirmer}">
                                    <option value="${item.userId}" selected>${item.userName}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.userId}">${item.userName}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">确认时间</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "rectifyEndTime"  name="rectifyEndTime" placeholder="" value="${hiddenDanger.rectifyEndTime}" type="text" disabled>
                </div>

            </div>

            <div class="row pb5">
                <label class="col-md-2 control-label">确认意见</label>
                <div class="col-md-3 pt7">
                    <textarea disabled class="form-control input-sm" rows="5" id = "confirmAdvice"  name="confirmAdvice" placeholder="">${hiddenDanger.confirmAdvice}</textarea>
                </div>
            </div>
        </div>

        <div class="form-actions mt10 mb5">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveHiddenInfo"><i class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/hiddenDanger/toHiddenDangerList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>
</body>
</html>
