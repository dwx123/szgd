<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>隐患排查</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var hiddenDangerTable ;
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });
            $("#status").select2({
                placeholder: "待整改+已整改",
                width: null,
                allowClear:true
            });
            $('#rectifyTimeFrom').datetimepicker({
                format: 'yyyy-mm-dd hh:ii',
                autoclose: true,
                minView: 0,
                minuteStep:1,
                todayHighlight: 1,
                startView: 2,
                language: 'zh-CN'
            });

            $('#rectifyTimeTo').datetimepicker({
                format: 'yyyy-mm-dd hh:ii',
                autoclose: true,
                minView: 0,
                minuteStep:1,
                todayHighlight: 1,
                startView: 2,
                language: 'zh-CN'
            });

            hiddenDangerTable = $("#hiddenDangerTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/hiddenDanger/getHiddenDangerList",//获取列表数据url,
                sServerMethod: "GET",
                //锁定行
                scrollY:"450px",
                //锁定列
                scrollX:true,
                scrollCollapse: true,
                fixedColumns: {
                    rightColumns:1
                },
                //table 表头
                aoColumns: [
//                    {'mDataProp': null, 'sTitle': '序号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'theme', 'sTitle': '隐患主题', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'siteName', 'sTitle': '所属站点', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'rectifyDeadline', 'sTitle': '整改期限(天)', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'reportPersonName', 'sTitle': '上报人', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'rectifyBeginTime', 'sTitle': '上报时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'rectifyPersonName', 'sTitle': '整改人', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'rectifyTime', 'sTitle': '整改时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'confirmerName', 'sTitle': '确认人', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'rectifyEndTime', 'sTitle': '确认时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'rectifyHours', 'sTitle': '整改时长', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'isRectify', 'sTitle': '是否已经整改', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'statusName', 'sTitle': '状态', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'approveFlag', 'sTitle': '审批状态', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'ifRead', 'sTitle': '读取状态', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'rectifyNoticePersonName', 'sTitle': '读取人', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'noticeTime', 'sTitle': '读取时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="' + path + '/hiddenDanger/toHiddenDangerView?id='+ row.id +'" title="查看隐患信息">查看</a>';
                            var loginId = "${userInfo.loginId}";
                            var creator = row.creator;
                            if(loginId == creator && row.status == "REPORT"){
                                domHtml += '&nbsp;&nbsp;<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/hiddenDanger/toHiddenDangerForm?opt=E&id='+ row.id +'" title="编辑隐患信息">编辑</a>';
                                domHtml += '&nbsp;&nbsp;<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除隐患" onclick="delItem(\''+ row.id + '\')">删除</a>';
                            }
                            <c:if test="${fn:contains(userInfomation.roleIds,'hidden_danger_approver')}">
                                <%--var loginId = "${userInfomation.personnelId}";--%>
                                var userId = "${userInfo.userId}";
                                var rectifyPerson = row.rectifyPerson;
                                if(row.status != "CONFIRM"){
                                    domHtml += '&nbsp;&nbsp;<a class="btn-sm btn-smd btn btn-warning" href="' + path + '/hiddenDanger/toHiddenDangerApproval?opt=APPROVE&id=' + row.id + '" title="隐患整改">整改</a>';
                                }
                            </c:if>

                            <c:if test="${fn:contains(userInfomation.roleIds,'hidden_danger_confirmer')}">
//                              domHtml += '&nbsp;&nbsp;<a class="btn-sm btn-smd btn btn-success" href="' + path + '/hiddenDanger/toHiddenDangerApproval?opt=COMFIRM&id='+ row.id +'" title="隐患整改确认">整改确认</a>';
//                                var siteIds = "${userInfomation.siteIds}";
//                                var siteId = row.siteId;
//                                if(siteIds.indexOf(siteId) != -1 && row.isRectify == 0){
//                                    domHtml += '&nbsp;&nbsp;<a class="btn-sm btn-smd btn btn-success" href="javascript:;" title="隐患整改确认" onclick="confirmHidden(\''+ row.id + '\')">整改确认</a>';
//                                }

                                if(row.status == "RECTIFY"){
//                                    domHtml += '&nbsp;&nbsp;<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="隐患确认整改" onclick="confirmHidden(\''+ row.id + '\')">确认</a>';
                                    domHtml += '&nbsp;&nbsp;<a class="btn-sm btn-smd btn btn-warning" href="' + path + '/hiddenDanger/toHiddenDangerConfirm?opt=CONFIRM&id=' + row.id + '" title="隐患确认">确认</a>';
                                }
                            </c:if>

//                            if (row.approveFlag == '0')
//                            {
//
//                                domHtml += '<a class="btn-sm btn-smd btn btn-success" href="' + path + '/hiddenDanger/toHiddenDangerForm?opt=E&id='+ row.id +'" title="编辑">编辑</a>&nbsp;&nbsp;';
//                                domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除" onclick="delProjectInfo(\''+ row.id + '\')">删除</a>&nbsp;&nbsp;';
//                                domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="javascript:;" title="发起上报" onclick="uploadProjectAttach(\''+ row.id + '\')">发起申请</a>&nbsp;&nbsp;';
//                            }

                            return domHtml;
                        }
                    }
//                    {
//                        aTargets: [0],
//                        mRender: function (data, type, row,meta) {
//                            return meta.row + 1 + meta.settings._iDisplayStart;
//                        }
//                    }
                ],
                fnRowCallback : function(nRow, aData, iDisplayIndex) {
                    $('td:eq(14)', nRow).attr('style', 'text-align: left;');
                },
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#hiddenDangerForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });

                    var formsltArray = $("#hiddenDangerForm select");
                    $.each(formsltArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });
                }

            });
            $("#findHiddenDanger").click(function () {
                hiddenDangerTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('hiddenDangerForm');
            })
        });

        function delItem(id) {
            var r=confirm("确认删除？");
            if (r==false){
                return
            }
            var param = {
                id: id
            };

            $.ajax({
                contentType: "application/json",
                url : '${ctx}/hiddenDanger/deleteHiddenDanger',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        hiddenDangerTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

        function addSelectListener() {
            //找到列表中的数据行
            var rows = $("#projectInfoTable tr");
            $.each(rows, function (index, row) {
                if (index > 0) {
                    $(row).click(
                        function () {
                            selectedData = $('#projectInfoTable').dataTable().fnGetData(this);
                            $("#choicedUserName").html("<strong>" + selectedData.USER_NAME + "</strong> [" + selectedData.LOGIN_ID + "]");
                            $(row).addClass("info");
                            $(row).siblings().removeClass("info");
                        });
                }
            });
        }

        function confirmHidden(id) {
            var r=confirm("确认该隐患已整改?");
            if (r==false){
                return;
            }
            var param = {
                id: id
            };

            $.ajax({
                contentType: "application/json",
                url : '${ctx}/hiddenDanger/confirmHiddenDanger',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        hiddenDangerTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

        function downloadHiddenDanger() {
            var action = path + "/hiddenDanger/downloadRecord";
            $('#hiddenDangerForm').attr('action',action);
            $('#hiddenDangerForm').submit();
        }

        //批量上报
        function batchReport() {
            openWin('#uploadExcelModel', path + '/hiddenDanger/toBatchUploadExcel', {"modalFlag": "report"});
        }

        //批量整改
        function batchRectify() {
            openWin('#uploadExcelModel', path + '/hiddenDanger/toBatchUploadExcel', {"modalFlag": "rectify"});
        }

        //批量上传隐患照片
        function batchReportPic() {
            openWin('#uploadPicModel', path + '/hiddenDanger/toBatchUploadPic', {"modalFlag": "report"});
        }

        //批量上传整改照片
        function batchRectifyPic() {
            openWin('#uploadPicModel', path + '/hiddenDanger/toBatchUploadPic', {"modalFlag": "rectify"});
        }
    </script>

</head>
<body>

<c:if test="${from ne 'home'}">
    <div class="bg-white item_title" style="padding-right: 0px;">
        隐患排查
    <c:if test="${fn:contains(userInfomation.roleIds,'hidden_danger_approver')}">
        <a class="btn btn-md1 btn-primary title-btn" href="javascript:;" title="批量上传整改照片" onclick="batchRectifyPic()">上传整改照片</a>
        <a class="btn btn-md1 btn-primary title-btn mr10" href="javascript:;" title="批量整改隐患" onclick="batchRectify()">批量整改</a>
    </c:if>
        <a class="btn btn-md1 btn-primary title-btn mr10" href="javascript:;" title="批量上传隐患照片" onclick="batchReportPic()">上传隐患照片</a>
        <a class="btn btn-md1 btn-primary title-btn mr10" href="javascript:;" title="批量隐患上报" onclick="batchReport()">批量上报</a>
        <a href="<%=request.getContextPath() %>/hiddenDanger/toHiddenDangerForm?opt=A" class="btn btn-md1 btn-primary title-btn mr10">隐患上报</a>
    </div>
</c:if>

<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">
        <form id="hiddenDangerForm">
            <div class="row form-group">
                <input type="hidden" name="roleIds" value="${userInfomation.roleIds}" id="roleIds">
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <%--<label>是否已经整改</label>--%>
                        <%--<select class="form-control input-sm select2" name="isRectify" id="isRectify">--%>
                            <%--<option value="">请选择</option>--%>
                            <%--<option value="0">否</option>--%>
                            <%--<option value="1">是</option>--%>
                        <%--</select>--%>
                        <label>状态</label>
                        <select class="form-control input-sm select2" name="status" id="status">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.HIDDEN_DANGER_STATUS}" var="item">
                                <c:choose>
                                    <c:when test="${item.code eq status}">
                                        <option value="${item.code}" selected>${item.dictValue}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.code}">${item.dictValue}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>所属标段</label>
                        <select class="form-control input-sm select2" name="bidId" id="bidId">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.bidList}" var="item">
                                <c:choose>
                                    <c:when test="${item.id eq bidId}">
                                        <option value="${item.id}" selected>${item.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.id}">${item.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>所属站点</label>
                        <select class="form-control input-sm select2" name="siteId" id="siteId">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.siteList}" var="item">
                                <c:choose>
                                    <c:when test="${item.id eq siteId}">
                                        <option value="${item.id}" selected>${item.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.id}">${item.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row form-group">
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>上报时间</label>
                        <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">
                            <input readonly style="background-color: #fff;" class="form-control" id="rectifyTimeFrom" name="rectifyTimeFrom" type="text">
                            <span class="input-group-addon"> 至 </span>
                            <input readonly style="background-color: #fff;" class="form-control" id="rectifyTimeTo" name="rectifyTimeTo" type="text">
                        </div>
                    </div>
                </div>

                <div class="col-xs-8 text-right">
                    <a id="findHiddenDanger" href="javascript:;" class="ml15 btn btn-primary btn-md1" >查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                    <button type="button" onclick="downloadHiddenDanger()" class="ml15 btn btn-md1 btn-warning" title="导出隐患信息Excel">导出隐患信息</button>
                </div>

            </div>
        </form>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="hiddenDangerTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>

<!-- 模态框（Modal）批量上报/整改 -->
<div id="uploadExcelModel" class="modal fade" data-backdrop="static"></div>

<!-- 模态框（Modal） 批量上传隐患照片/整改照片-->
<div id="uploadPicModel" class="modal fade" data-backdrop="static"></div>
</body>
</html>
