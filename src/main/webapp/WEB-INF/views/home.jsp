<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
    <meta http-equiv="Expires" content="0" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="${ctx}/assets/bootstrap-extension/font-awesome/css/font-awesome.min.css"
          rel="Stylesheet">
    <link rel="stylesheet" type="text/css" href="${ctx}/assets/jquery-ui-1.10.4/jquery-ui-1.10.4.custom.min.css">
    <link type="text/css" href="${ctx}/assets/css/custom.css" rel="Stylesheet">
    <script type="text/javascript" src="${ctx }/js/jquery.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
    <script src="${ctx}/assets/jquery-ui-1.10.4/jquery-ui-1.10.4.custom.min.js" type="text/javascript" charset="UTF-8"></script>
    <title>苏州地铁6号线智慧工地实时监控</title>
    <style type="text/css">
        *{
            box-sizing: border-box;
            color: #000;
        }
        *,body{
            margin: 0;
            padding: 0;
        }
        html,body{
            height: 100%;
            min-height: 100%;
            overflow: hidden;
        }
        body{
            background: #ddd;
        }
        a {
            color: #8A6EE9;
        }
        .header{
            height: 80px;
            display: flex;
            display: -webkit-flex;
            position: absolute;
            width: 100%;
            left: 0;
            top: 0;
        }
        .header_l,.header_r{
            width: 23.9%;
            /*background: #183DA1;*/
            /*border-bottom: 1px solid #00C1FF;*/
            background-size: 100% 100%;
            height: 28px;
        }
        .header_l {
            background-image: url('${ctx}/assets/img/new_title1.png');
        }
        .header_r {
            background-image: url('${ctx}/assets/img/new_title3.png');
        }
        .header_c{
            flex-grow: 1;
            -webkit-flex-grow: 1;
            text-align: center;
            color: #fff;
            font-size: 40px;
            padding-top: 10px;
            letter-spacing: 2px;
            background-image: url('${ctx}/assets/img/new_title2.png');
            background-size: 100% 100%;
            z-index: 99;
        }
        .content{
            width: 100%;
            height: 100%;
            position: relative;
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            -webkit-justify-content: space-between;

        }
        .content .item{
            padding-top: calc(1.3% + 28px);
            height: 100%;
            padding-bottom: 1.3%;
            display: flex;
            display: -webkit-flex;
            flex-direction: column;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            justify-content: space-between;
            -webkit-justify-content: space-between;
        }
        .block-item{
            border-radius: 10px;
            position: relative;
            background: #fff;
            padding-top: 40px;
        }
        .block-item.cus_stop{
            background: #8A6EE9;
        }
        .block-item.cus_stop .block-content {
            justify-content: center;
            -webkit-justify-content: center;
            padding-top: 0;
        }
        .cus_stop .item-title,.cus_stop #curStop {
            color: #fff;
        }
        .cus_stop #curStop {
            font-size: 30px;
            margin-top: -5%;
            text-align: center;
        }
        .item-title{
            position: absolute;
            width: 100%;
            top: 0;
            left: 0;
            line-height: 40px;
            font-size: 18px;
            color: #000;
            letter-spacing: 1.2px;
            padding-left: 20px;
            font-weight: bold;
        }
        .item-title a{
            font-size: 14px;
            text-decoration: none;
            position: absolute;
            right: 20px;
            top: 0;
            color:#8A6EE9;
        }
        .item-title a:hover,.item-title a:active,.item-title a:focus{
            color:#8A6EE9;
        }
        .block-content{
            height: 100%;
            /*padding: 0 20px 20px;*/
            padding: 20px;
            display: flex;
            display: -webkit-flex;
            flex-direction: column;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
        }
        .content .content_l{
            /*width: 20.8%;*/
            /*margin-left: 1.3%;*/
            width: 24.3%;
            margin-left: 1%;
        }
        .content_l .block-item:nth-child(1) {
            height: 15.99%;
        }
        .content_l .block-item:nth-child(2) {
            /*height: 55.64%;*/
            height: 31.768%;
        }
        .content_l .block-item:nth-child(3) {
            /*height: 23.976%;*/
            height: 47.848%;
        }
        .program-progress{
            padding-right: 60px;
            position: relative;
        }
        .program{
            justify-content: space-between;
            -webkit-justify-content: space-between;
        }
        .program-progress .title{
            font-size: 16px;
            letter-spacing: 1.2px;
            padding-left: 3px;
            margin-bottom: 6px;
        }
        .program-progress .progress-bar{
            height: 24px;
            background: #D8D8D8;
            position: relative;
            border-radius: 12px;
        }
        .program-progress .progress-bar .inner-bar{
            position: absolute;
            height: 100%;
            left: 0;
            top: 0;
            background: linear-gradient(to right, #FF5400, #F7EE00, #B7EB81);
            background: -webkit-linear-gradient(left, #FF5400, #F7EE00, #B7EB81);
            border-radius: 12px;
            width: 0;
            transition: width 2s;
            -moz-transition: width 2s;
            -webkit-transition: width 2s;
            -o-transition: width 2s;
        }
        .program-progress .num{
            position: absolute;
            width: 60px;
            height: 24px;
            right: 0;
            bottom: 0;
            padding-left: 18px;
        }
        .program-pie{
            display: flex;
            display: -webkit-flex;
            /*justify-content: space-between;*/
            /*-webkit-justify-content: space-between;*/
            /*flex-wrap: wrap;*/
            /*-webkit-flex-wrap: wrap;*/
            flex-direction: column;
            -webkit-flex-direction: column;
            height: 100%;
            justify-content: space-around;
            -webkit-justify-content: space-around;
        }
        .pie-title{
            font-size: 16px;
            font-weight: bold;
            letter-spacing: 1.2px;
            padding-left: 3px;
        }
        .program-pie .pieitem{
            width: 115px;
            height: 140px;
        }
        .program-pie .pie-row{
            display: flex;
            display: -webkit-flex;
            justify-content: space-around;
            -webkit-justify-content: space-around;
        }
        .program-pie .pieitem .title{
            text-align: center;
            font-size: 16px;
            letter-spacing: 1.2px;
        }
        .program-pie canvas {
            cursor: pointer;
        }
        .equip{
            justify-content: space-evenly;
            -webkit-justify-content: space-evenly;
            -moz-justify-content: space-evenly;
        }
        .equip .equip-item{
            font-size: 16px;
            letter-spacing: 1.2px;
            display: flex;
            display: -webkit-flex;
        }
        .equip .equip-item .name{
            font-weight: bold;
            width: 120px;
        }
        .equip .equip-item .status{
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            -webkit-justify-content: space-between;
            flex-grow: 1;
            -webkit-flex-grow: 1;
        }
        .radiowrap{
            width: 55px;
            height: 30px;
            border-radius: 15px;
            border: 2px solid #92A3C1;
            display: inline-block;
            vertical-align: middle;
            background: radial-gradient(#FFFFFF, #D8D8D8);
            background: -webkit-radial-gradient(#FFFFFF, #D8D8D8);
            position: relative;
            cursor: pointer;
        }
        .radiowrap .radio{
            width: 27px;
            height: 27px;
            margin: 0;
            border: 2px solid #92A3C1;
            position: absolute;
            top: 0;
            border-radius: 50%;
            left: auto;
            right: 0;
            background: radial-gradient(#F3FFFF, #9D9D9D);
            background: -webkit-radial-gradient(#F3FFFF, #9D9D9D);
        }
        .radiowrap.active .radio{
            left: 0;
            background: radial-gradient(#B0FDFF, #00B5FF);
            background: -webkit-radial-gradient(#B0FDFF, #00B5FF);
        }
        .content .content_c{
            /* padding-top: calc(1.3% + 80px); */
            /*width: 52%;*/
            width: 47%;
            /* display: flex;
            flex-direction: row;
            flex-wrap: wrap; */
        }
        .content_c .block-item.stops{
            padding-top: 52px;
            border-top: none;
            /*height: 47.654%;*/
            height: 49.955%;
            position: relative;
            width: 100%;
            background: #fff;
        }
        .content_c .stops .img_bor{
            position:absolute;
            width: calc(100% + 4px);
            left: -2px;
            top: -1px;
        }
        .content_c .stops .block-content .list_stops{
            list-style: none;
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            -webkit-justify-content: space-between;
            padding-top: 12px;
            margin-top: 20px;
            border-top: 2px solid #8A6EE9;
        }
        .content_c .stops .block-content .list_stops li {
            width: 25px;
            text-align: center;
            padding: 5px 0;
            border-radius: 12px;
            cursor: pointer;
            position: relative;
            height: 200px;
        }
        .content_c .stops .block-content .list_stops li.active{
            background: #8A6EE9;
            color: #fff;
        }
        .content_c .stops .block-content .list_stops li::before{
            content: '';
            width: 10px;
            height: 10px;
            border: 2px solid #8A6EE9;
            background: #fff;
            position: absolute;
            left: 5.5px;
            top: -20px;
            border-radius: 10px;
        }
        .content_c .block-item{
            width: 32%;
            background: none;
        }
        .content_c .video-block{
            /*height: 23.976%;*/
            height: 47.952%;
            overflow: hidden;
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            -webkit-justify-content: space-between;
        }
        .content_c .video-block .block-item{
            height: 100%;
            width: 100%;
            /*padding-top: 0;*/
            padding-top: 40px;
        }
        .content_c .video-block .block-item .plugin{
            height: 150%;
            margin-top: 0;
        }
        .content_c .video-block .block-item img{
            width: 100%;
            height: 100%;
        }
        .content_c .video-block .block-item .item-title{
            color: #000;
            background: #fff;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
        }
        .content_c .video-block .block-item .item-title a{
            top: 11px;
            right: 10px;
            z-index: 100;
        }
        .content_c .video-block .block-item .item-title i {
            color: #000;
            font-size: 18px;
            margin-right: 5px;
        }
        .content .content_r{
            /*width: 22.3%;*/
            /*margin-right: 1.3%;*/
            width: 24.3%;
            margin-right: 1%;
        }
        .content_r .block-item.people_entrance{
            /*height: 39.96%;*/
            height: 31.768%;
        }
        .euipcontent.block-content,.content_r .people_entrance .block-content,.content_r .vehicle_entrance .block-content{
            display: block;
            overflow: hidden;
        }
        .block-content .list-title{
            background: #647EE1;
            font-weight: bold;
            padding: 9px 0;
            letter-spacing: 1.2px;
            border-radius: 5px;
        }
        .block-content .list-title div {
            color: #fff;
        }
        .block-content .list-title,.block-content .lists .list{
            display: flex;
            display: -webkit-flex;
        }
        .block-content .lists .list{
            padding: 9px 0;
            font-size: 12px;
        }
        .block-content .lists .list:nth-child(2n) {
            background: #E2E5EA;
            border-radius: 5px;
        }
        .block-content .name,.block-content .time,.block-content .timespan{
            text-align: center;
        }
        .block-content .name,.block-content .timespan{
            width: 90px;
        }
        /*.content_r .vehicle_entrance .block-content .name{*/
            /*width: 110px;*/
        /*}*/
        .block-content .time{
            /*flex-grow: 1;*/
            /*-webkit-flex-grow: 1;*/
            width: 120px;
        }
        /*.block-content .timespan{*/
            /*width: 110px;*/
        /*}*/
        .content_r .block-item.vehicle_entrance{
            height: 31.768%;
        }
        .content_r .block-item.alarm{
            /*height: 23.976%;*/
            height: 32.168%;
        }
        .alarm .alaritem{
            /*margin-top: 20px;*/
            margin-left: 20px;
        }
        .alarm .alaritem span{
            font-size: 20px;
            color: #FFDA30;
            margin-left: 10px;
        }
        .content_r .alarm .block-content{
            justify-content: space-evenly;
            -moz-justify-content: space-evenly;
        }

        /*.content_c .block-item.stops.map-stops{*/
            /*padding: 0;*/
        /*}*/
        .content_c .stops.map-stops .img_bor {
            z-index: 9;
        }
        .content_c .block-item.stops.map-stops .block-content{
            padding: 20px;
        }
        .block-content.map-content .list_stops{
            width: 100%;
            height: 100%;
            margin-top: 0!important;
        }
        .BMapLabel {
            color: #000;
        }
    </style>
</head>
<body>

<div class="content">

    <div class="header">
        <div class="header_l"></div>
        <div class="header_c">
            苏州轨道交通6号线信息化管理平台
        </div>
        <div class="header_r"></div>
    </div>
    <div class="content_l item">
        <div class="block-item cus_stop">
            <div class="item-title">
                当前站点
            </div>
            <div class="block-content">
                <div id="curStop"></div>
                <marquee direction="left" scrollamount="2" style="width: 100%;height: 20px;position: absolute; bottom: 0;color: #fff;left: 0;" id="">
                    <a href="#" style="color: #fff;">李四的《电工》证书将在2019-04-30到期，请及时办理！</a>
                </marquee>
            </div>
        </div>
        <div class="block-item">
            <div class="item-title">
                设备信息
                <a href="javascript:;" onclick="openDialogWin('equipment','设备进出记录')">查看详细</a>
            </div>
            <div class="block-content euipcontent">
                <div class="list-title">
                    <div class="name">设备名称</div>
                    <div class="time">进入时间</div>
                    <div class="time">离开时间</div>
                    <div class="timespan">停留时间</div>
                </div>
                <div class="lists"  id="equipEnterExitList">
                </div>
            </div>
        </div>
        <div class="block-item">
            <div class="item-title">
                项目进度
                <%--<a href="javascript:;" onclick="openDialogWin('project','项目信息')">查看详细</a>--%>
            </div>
            <div class="block-content program">
                <div class="program-pie">
                    <div class="pie-row">
                        <div class="pieitem">
                            <canvas id="canvasPie3" width="115" height="115" onclick="openDialogWin('site','主体-围护结构')"></canvas>
                            <div class="alaritem">主体-围护结构</div>
                        </div>
                        <div class="pieitem">
                            <canvas id="canvasPie4" width="115" height="115" onclick="openDialogWin('section','主体-土方开挖')"></canvas>
                            <div class="alaritem">主体-土方开挖</div>
                        </div>
                        <div class="pieitem">
                            <canvas id="canvasPie5" width="115" height="115" onclick="openDialogWin('segment','主体-主体结构、盾构掘进、附属结构')" ></canvas>
                            <div class="alaritem">主体-主体结构</div>
                        </div>
                    </div>
                    <div class="pie-row">
                        <div class="pieitem">
                            <canvas id="canvasPie1" width="115" height="115" onclick="openDialogWin('tunnelling','盾构施工进度')" ></canvas>
                            <div class="alaritem">盾构施工进度</div>
                        </div>
                        <div class="pieitem">
                            <canvas id="canvasPie2" width="115" height="115" onclick="openDialogWin('accessor','附属结构施工进度')" ></canvas>
                            <div class="alaritem">附属结构施工进度</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content_c item">
        <div class="block-item stops map-stops">
            <img src="${ctx}/assets/img/new_map_top.png" class="img_bor" />
            <div class="block-content map-content">
                <ul class="list_stops">
                    <c:forEach items="${siteList}" var="item">
                        <c:choose>
                            <c:when test="${item.defaultShow eq '1'}">
                                <li class="active" data-siteid="${item.id}" data-bidid="${item.bidId}">${item.name}</li>
                            </c:when>
                            <c:otherwise>
                                <li data-siteid="${item.id}" data-bidid="${item.bidId}">${item.name}</li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </ul>
            </div>
        </div>
        <div class="video-block">
            <div class="block-item">
                <div class="item-title">
                    <i class="fa fa-video-camera"></i>实时监控
                    <%--<a href="javascript:;">--%>
                        <%--<i class="fa fa-arrows-alt"></i>--%>
                    <%--</a>--%>
                </div>
                <div id = "divPlugin" class="plugin">
                </div>
            </div>
        </div>
    </div>
    <div class="content_r item">
        <div class="block-item people_entrance">
            <div class="item-title">
                <!-- 施工人员统计信息 -->
                人员在场人数：共<span id="personnelBePresentCount"></span>人
                <a href="javascript:;" onclick="openDialogWin('personnel','人员进出记录')">查看详细</a>
            </div>
            <div class="block-content">
                <div class="list-title">
                    <div class="name">姓名</div>
                    <div class="time">进入时间</div>
                    <div class="time">离开时间</div>
                    <div class="timespan">停留时间</div>
                </div>
                <div class="lists" id="personnelEnterExitList">
                </div>
            </div>
        </div>
        <div class="block-item vehicle_entrance">
            <div class="item-title">
                <!-- 车辆统计信息 -->
                车辆在场数量：共<span id="vehicleBePresentCount"></span>辆
                <a href="javascript:;" onclick="openDialogWin('vehicle','车辆进出记录')">查看详细</a>
            </div>
            <div class="block-content">
                <div class="list-title">
                    <div class="name">车牌号</div>
                    <div class="time">进入时间</div>
                    <div class="time">离开时间</div>
                    <div class="timespan">停留时间</div>
                </div>
                <div class="lists" id="vehicleEnterExitList">
                </div>
            </div>
        </div>
        <div class="block-item alarm">
            <div class="item-title">
                提醒事项
                <!-- <a href="javascript:;">查看详细</a> -->
            </div>
            <div class="block-content">
                <div class="alaritem">
                    人员资质有效期提醒<span><a id="certificationRemindCount" href="javascript:;" onclick="openDialogWin('certification','人员资质有效期提醒')">10</a></span>
                </div>
                <div class="alaritem">
                    人员数量提醒<span><a id="peoplesRemindCount" href="javascript:;" onclick="openDialogWin('peoples','特定时间内人数提醒')">2</a></span>
                </div>
                <div class="alaritem">
                    人员体检提醒<span><a id="physicalExaminationRemindCount" href="javascript:;" onclick="openDialogWin('examination','人员体检提醒')">30</a></span>
                </div>
                <div class="alaritem">
                    设备检测提醒<span><a id="equipmentChkRemindCount" href="javascript:;" onclick="openDialogWin('check','设备检测提醒')">10</a></span>
                </div>
                <div class="alaritem">
                    设备保险提醒<span><a id="equipmentInsureRemindCount" href="javascript:;" onclick="openDialogWin('insurance','设备保险提醒')">10</a></span>
                </div>
                <div class="alaritem">
                    隐患提醒<span><a id="hiddenDangerCount" href="javascript:;" onclick="openDialogWin('hiddenDanger','隐患提醒')">10</a></span>
                </div>
                <%--<div class="alaritem">
                    项目进度提醒<span><a href="javascript:;" onclick="openDialogWin('schedule','项目进度提醒')">10</a></span>
                </div>--%>
            </div>
        </div>
    </div>
</div>

<div style="display:none;overflow:hidden;padding:3px" id="dialog"><iframe frameborder="no" border="0" marginwidth="0" marginheight="0" id="showRemindMessage"  scrolling="yes"  width="100%" height="100%"></iframe></div>
</body>

<script type="text/javascript" src="${ctx }/webcomponents/codebase/webVideoCtrl.js"></script>
<script type="text/javascript" src="${ctx }/webcomponents/codebase/webVideo.js"></script> 
</html>
<script>
    var siteId ='';
    var bidId = '';
    var webVideoArray = new Array();
    function openDialogWin(type,title){
        var url ;
        if (type == 'site')
        {
            url = "${ctx}/mainWorkSchedule/toWHJG?from=home&siteId="+siteId+"&bidId="+bidId;

        }else if (type == 'section')
        {
            url = "${ctx}/mainWorkSchedule/toTFKW?from=home&siteId="+siteId+"&bidId="+bidId;

        }else if (type == 'segment')
        {
            url = "${ctx}/mainWorkSchedule/toZTJG?from=home&siteId="+siteId+"&bidId="+bidId;

        }else if (type == 'tunnelling')
        {
            url = "${ctx}/tunnellingWorkSchedule/toTunnelling?from=home&siteId="+siteId+"&bidId="+bidId;

        }else if (type == 'accessor')
        {
            url = "${ctx}/accessorStructureSchedule/toAccessor?from=home&siteId="+siteId+"&bidId="+bidId;

        }else if (type == 'personnel')
        {
            url = "${ctx}/personnelWork/toPersonnelEnterExitList?from=home&siteId="+siteId;
            title = '人员进出记录';
        }else
        if (type == 'vehicle')
        {
            url = "${ctx}/vehicleEnterExit/toVehicleEnterExitList?from=home&siteId="+siteId;
        }else
        if (type == 'equipment')
        {
            url = "${ctx}/equipEnterExit/toEquipEnterExitList?from=home&siteId="+siteId;
        }else
        if (type == 'project')
        {
            url = "${ctx}/project/toProjectInfoList?from=home";
        }else
        if (type == 'hiddenDanger')
        {
            url = "${ctx}/hiddenDanger/toHiddenDangerList?from=home";
        }else
        {
            url = "${ctx}/remind/"+type+"-list?from=home";

        }

        $("#showRemindMessage").attr("src",url); //设置IFRAME的SRC;
        $("#dialog").dialog({
            bgiframe: true,
            resizable: true, //是否可以重置大小
            height: 600, //高度
            width: 1050, //宽度
            draggable: true, //是否可以拖动。
            title: title,
            modal: true,
            open: function (e) {  //打开的时候触发的事件
                document.body.style.overflow = "hidden"; //隐藏滚动条
            },
            close: function () { //关闭Dialog时候触发的事件
                document.body.style.overflow = "visible";  //显示滚动条
            }
        });
    }

    function drawMain (drawingElem, percent, forecolor, bgcolor) {
        var context = drawingElem.getContext('2d')
        var centerX = drawingElem.width / 2
        var centerY = drawingElem.height / 2
        var rad = Math.PI * 2 / 100
        var speed = 0

        function backgroundCircle () {
            context.save()
            context.beginPath()
            context.lineWidth = 15
            var radius = centerX - context.lineWidth
            context.lineCap = 'round'
            context.strokeStyle = bgcolor
            context.arc(centerX, centerY, radius, 0, Math.PI * 2, false)
            context.stroke()
            context.closePath()
            context.restore()
        }

        function foregroundCircle (n) {
            context.save()
            context.strokeStyle = forecolor
            context.lineWidth = 15
            context.lineCap = 'round'
            var radius = centerX - context.lineWidth
            context.beginPath()
            context.arc(centerX, centerY, radius, -Math.PI / 2, -Math.PI / 2 + n * rad, false) // 用于绘制圆弧context.arc(x坐标，y坐标，半径，起始角度，终止角度，顺时针/逆时针)
            context.stroke()
            context.closePath()
            context.restore()
        }

        function text (n) {
            context.save() // save和restore可以保证样式属性只运用于该段canvas元素
            context.fillStyle = '#000'
            var fontSize = 24
            context.font = fontSize + 'px Helvetica'
            var textWidth = context.measureText(n + '%').width
            context.fillText(n + '%', centerX - textWidth / 2, centerY + fontSize / 2)
            context.restore()
        }

        context.clearRect(0, 0, drawingElem.width, drawingElem.height)
        backgroundCircle()
        foregroundCircle(percent)
        text(percent)
    }

    var canvasPie1 = document.getElementById('canvasPie1')
    var canvasPie2 = document.getElementById('canvasPie2')
    var canvasPie3 = document.getElementById('canvasPie3')
    var canvasPie4 = document.getElementById('canvasPie4')
    var canvasPie5 = document.getElementById('canvasPie5')

    $(function() {
        $('.list_stops li').click(function() {
            if(!$(this).hasClass('active')) {
                $(this).addClass('active').siblings().removeClass('active')
                $('#curStop').text($(this).text())
                siteId = $(this).data("siteid");
                bidId = $(this).data("bidid");
                getTotalSchedulePercentage(siteId,bidId);
                getPersonnelEnterExitList(siteId);
                getVehicleEnterAndExitList(siteId);
                getEquipEnterAndExitList(siteId);

                stopCameraList();
                getCameraList(siteId);

/*                var webVideo = new WebVideo();
                webVideo.ip = '10.10.10.70';
                webVideo.username = 'admin';
                webVideo.password = 'Ecdatainfo.com';
                webVideo.divPlugin = "divPlugin1";
                webVideo.init();
                webVideo.clickLogin();*/

            }
        })
        $('.list_stops li').each(function() {
            if ($(this).hasClass('active')) {
                $('#curStop').text($(this).text());
                siteId = $(this).data("siteid");
                bidId = $(this).data("bidid");
                getTotalSchedulePercentage(siteId,bidId);
                getPersonnelEnterExitList(siteId);
                getVehicleEnterAndExitList(siteId);
                getEquipEnterAndExitList(siteId);
                getRemindCount(siteId);

                var webVideo = new WebVideo();
                webVideo.divPlugin = "divPlugin";
                webVideo.init();

                stopCameraList();
                getCameraList(siteId);
            }
        })
    })
    function getPersonnelEnterExitList(siteid) {
        var divStr = ' <div class="list"><div class="name">{name}</div><div class="time">{enterTime}</div><div class="time">{exitTime}</div><div class="timespan">{stayHours}</div></div>';
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getPersonnelEnterExitList?siteId="+siteid+"&rownum="+5,
            async: false,
            success: function (result) {
                var data = result.personnelEnterExitList;
                $("#personnelEnterExitList").empty();
                for( i = 0; i　< data.length; i++) {
                    var item = data[i];
//                    var exitTime='';
//                    if (item.exitTime)
//                        exitTime = item.exitTime;
                    var enterExit_DIV = divStr.replace('{name}', item.name ? item.name : '').replace('{enterTime}', item.enterTime ? item.enterTime : '').replace('{exitTime}', item.exitTime ? item.exitTime : '').replace('{stayHours}', item.stayHours ? item.stayHours : '');
                    $("#personnelEnterExitList").append(enterExit_DIV);
                }
                $("#personnelBePresentCount").html(result.personnelBePresentCount);
            },
            error: function (request, status, error) {
                alert(error)
            }
        })
    }

    function getVehicleEnterAndExitList(siteid) {
        var divStr = ' <div class="list"><div class="name">{plateNumber}</div><div class="time">{enterTime}</div><div class="time">{exitTime}</div><div class="timespan">{stayHours}</div></div>';
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getVehicleEnterAndExitList?siteId="+siteid+"&rownum="+5,
            async: false,
            success: function (result) {
                var data = result.vehicleEnterExitList;
                $("#vehicleEnterExitList").empty();
                for( i = 0; i　< data.length; i++) {
                    var item = data[i];
//                    var exitTime='';
//                    if (item.exitTime)
//                        exitTime = item.exitTime;
                    var enterExit_DIV = divStr.replace('{plateNumber}', item.plateNumber ? item.plateNumber : '').replace('{enterTime}', item.enterTime ? item.enterTime : '').replace('{exitTime}', item.exitTime ? item.exitTime : '').replace('{stayHours}', item.stayHours ? item.stayHours : '');
                    $("#vehicleEnterExitList").append(enterExit_DIV);
                }
                $("#vehicleBePresentCount").html(result.vehicleBePresentCount);
            },
            error: function (request, status, error) {
                alert(error)
            }
        })
    }

    function getEquipEnterAndExitList(siteid) {
        var divStr = ' <div class="list"><div class="name">{equipName}</div><div class="time">{enterTime}</div><div class="time">{exitTime}</div><div class="timespan">{stayHours}</div></div>';
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getEquipEnterAndExitList?siteId="+siteid+"&rownum="+5,
            async: false,
            success: function (result) {
                var data = result.equipEnterExitList;
                $("#equipEnterExitList").empty();
                for( i = 0; i　< data.length; i++) {
                    var item = data[i];
//                    var exitTime='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
//                    var exitTime='';
//                    if (item.exitTime)
//                        exitTime = item.exitTime;
                    var enterExit_DIV = divStr.replace('{equipName}', item.equipName ? item.equipName : '').replace('{enterTime}', item.enterTime ? item.enterTime : '').replace('{exitTime}', item.exitTime ? item.exitTime : '').replace('{stayHours}', item.stayHours ? item.stayHours : '');
                    $("#equipEnterExitList").append(enterExit_DIV);
                }
            },
            error: function (request, status, error) {
                alert(error)
            }
        })
    }

    function getTotalSchedulePercentage(siteid,bidid) {

        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getTotalSchedulePercentage?siteId="+siteid+"&bidId="+bidid,
            async: false,
            success: function (result) {
                drawMain(canvasPie3, result.envelopePercentage, '#FFCA00', '#E2E5EA')
                drawMain(canvasPie4, result.excavationPercentage, '#8A6EE9', '#E2E5EA')
                drawMain(canvasPie5, result.mainPercentage, '#CAB4FF', '#E2E5EA')
                drawMain(canvasPie1, result.tunnellingPercentage, '#6076FF', '#E2E5EA')
                drawMain(canvasPie2, result.accessorPercentage, '#4A73DC', '#E2E5EA')
            },
            error: function (request, status, error) {
                alert(error)
            }
        })
    }

    function getRemindCount(siteid,bidid) {

        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getRemindCount?siteId="+siteid+"&bidId="+bidid,
            async: false,
            success: function (result) {
                $("#certificationRemindCount").html(result.certificationRemindCount);
                $("#peoplesRemindCount").html(result.peoplesRemindCount);
                $("#physicalExaminationRemindCount").html(result.physicalExaminationRemindCount);
                $("#equipmentChkRemindCount").html(result.equipmentChkRemindCount);
                $("#equipmentInsureRemindCount").html(result.equipmentInsureRemindCount);
                $("#hiddenDangerCount").html(result.hiddenDangerCount);
            },
            error: function (request, status, error) {
                alert(error)
            }
        })
    }

    function stopCameraList() {
        for (var i = 0; i < webVideoArray.length; i++) {
            webVideoArray[i].clickStopRealPlay();
            webVideoArray[i].clickLogout();
        }
    }
    function getCameraList(siteid) {
        var divStr = ' <div class="list"><div class="name">{equipName}</div><div class="time">{enterTime}</div><div class="time">{exitTime}</div><div class="timespan">{stayHours}</div></div>';
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getCameraList?siteId="+siteid+"&defaultShow=1",
            async: false,
            success: function (result) {
                webVideoArray.length = 0;

                var data = result.cameraList;
                for( i = 0; i　< data.length; i++) {
                    var webVideo1 = new WebVideo();
                    webVideoArray.push(webVideo1)
                    var item = data[i];
                    webVideo1.ip = item.ip;
                    webVideo1.username = item.loginUserName;
                    webVideo1.password = item.loginPassword;
                    webVideo1.g_iWndIndex = i;
                    webVideo1.clickLogin();
                }
            },
            error: function (request, status, error) {
                alert(error)
            }
        })
    }

</script>
