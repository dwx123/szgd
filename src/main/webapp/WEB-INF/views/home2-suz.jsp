<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<META HTTP-EQUIV="Refresh" CONTENT="2400">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
    <meta http-equiv="Expires" content="0" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%@ include file="/common/init.jsp"%>
    <title>苏州地铁6号线智慧工地实时监控</title>
    <style type="text/css">
        
    </style>
</head>
<body>

    <div class="content">

        <div class="header">
            <div class="header_l"></div>
            <div class="header_c">
                苏州轨道交通6号线信息化管理平台
            </div>
            <div class="header_r"></div>
        </div>
        <div class="content_c item">
            <div class="tunnelAndPit-block">
                <div class="block-item">
                    <div class="item-title">
                        操作
                    </div>
                    <div class="block-content tunnel">
                        <a href="javascript:;" onClick="javascript:window.open('http://61.155.204.87:8888/monitorData.do?method=showMonitorData&user_type=1&location_id=230','','width=1200,height=500,left=10, top=10,toolbar=no, status=no, menubar=no, resizable=yes, scrollbars=yes');return false;">外部项目登录</a>
                        <a href="javascript:;" onclick="getSuzData1();">取监测数据</a>
                        <a href="javascript:;" onclick="saveSuzData1();">将监测数据保存到本地库</a>
                        <a href="javascript:;" onclick="getSuzData2();">取盾构数据</a>
                        <a href="javascript:;" onclick="saveSuzData2();">将盾构数据保存到本地库</a>
                    </div>
                    <div style="width: 30%;">
                        <label>站点：</label>
                        <select id="siteList" class="form-control input-sm select2">
                            <option value="">请选择</option>
                            <option value="230">中新大道西站</option>
                            <option value="225">徐家浜站</option>
                            <option value="232">星海街站</option>
                            <option value="234">李公堤西站</option>
                            <option value="237">中塘公园站</option>
                            <option value="239">南施街站</option>
                            <option value="245">港田路站</option>
                            <option value="243">中心大道东站</option>
                            <option value="241">苏胜路站</option>
                            <option value="247">金家堰站</option>
                            <option value="251">新昌路站</option>
                            <option value="249">斜步站</option>
                            <option value="253">金尚路站</option>
                            <option value="257">新庆路站</option>
                            <option value="255">桑田岛站</option>
                        </select>
                    </div>
                    <div style="width: 30%;">
                        <label>标段：</label>
                        <select id="bidList" class="form-control input-sm select2">
                            <option value="">请选择</option>
                            <option value="07">7标</option>
                            <option value="08">8标</option>
                            <option value="09">9标</option>
                            <option value="10">10标</option>
                            <option value="11">11标</option>
                            <option value="12">12标</option>
                            <option value="13">13标</option>
                        </select>
                    </div>
                </div>

                <div class="block-item">
                    <div class="item-title">
                        数据展示
                    </div>
                    <div class="block-content pit" id="testWbjc">

                    </div>
                    <div class="block-content pit" id="testWbdg">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function getSuzData() {
            $.ajax({
                type:"post",
                url:"http://61.155.204.87:8888/monitorData.do?method=showMonitorData&user_type=1&location_id=230",
                success:function(data){
                    console.log(data);
                }
            });
        }

        function getSuzData1() {
            var site = $("#siteList").val();
            console.log(site);
            if(site == "" || site == null){
                alert("请先选择一个站点,以获取该站点下的基坑监测数据");
            }else{
                var urlTemp = "http://61.155.204.87:8888/monitorData.do?method=showMonitorData&user_type=1&location_id=" + site + "  .t1";
                $("#testWbjc").load(urlTemp);
            }
        }
        function getSuzData2() {
            var bid = $("#bidList").val();
            console.log(bid);
            if(bid == "" || bid == null){
                alert("请先选择一个标段，以获取该标段下的盾构监测数据");
            }else{
                var urlTemp = "http://61.155.204.87:8888/jsp/mk_new/dungou/dungouAll.jsp?line_id=62&bargain_moment=" + bid + "  .t1";
                $("#testWbdg").load(urlTemp);
            }
        }
        function saveSuzData1() {
            var siteName = $("#siteList").find("option:selected").text();
            var jcList = new Array();
            var trList = $("#testWbjc").find("tbody tr");
            for (var i=0;i<trList.length;i++){
                var trArr = trList.eq(i);
                var Wbjc = new Object();
                Wbjc.sszh = trArr.children("td").eq(0).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbjc.cd = trArr.children("td").eq(1).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbjc.cdlx = trArr.children("td").eq(2).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbjc.dqaqzt = trArr.children("td").eq(3).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbjc.ljbhz = trArr.children("td").eq(4).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbjc.bxsl = trArr.children("td").eq(5).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbjc.zdz = trArr.children("td").eq(6).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbjc.zxz = trArr.children("td").eq(7).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbjc.zhclsj = trArr.children("td").eq(8).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbjc.ljbjz = trArr.children("td").eq(9).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbjc.slbjz = trArr.children("td").eq(10).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbjc.siteName = siteName;
                jcList.push(Wbjc);

            }

            console.log(jcList);

            if(jcList.length > 0){
                $.ajax({
                    "type": 'post',
                    "url": path + '/wbsj/wbjc-save',
                    "sync": true,
                    "contentType": "application/json;charset=utf-8",
                    "data": JSON.stringify(jcList),
                    "success": function (resp) {
                        var obj = JSON.parse(resp);
                        if (obj) {
                            if (obj.errmsg) {
                                notyError(obj.errmsg);
                            } else if (obj.msg) {
                                notySuccess(obj.msg);
                            }
                        }
                    }
                });
            }else{
                notyError("没有监测数据无法保存！");
            }

        }
        function saveSuzData2() {
            var bidName = $("#bidList").find("option:selected").text();
            var dgList = new Array();
            var trList = $("#testWbdg").find("tbody tr");
            for (var i=0;i<trList.length;i++){
                var trArr = trList.eq(i);
                var Wbdg = new Object();
                Wbdg.xh = trArr.children("td").eq(0).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.gdmc = trArr.children("td").eq(1).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.dgxh = trArr.children("td").eq(2).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.dglx = trArr.children("td").eq(3).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.jd = trArr.children("td").eq(4).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.dqh = trArr.children("td").eq(5).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.gk = trArr.children("td").eq(6).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.ylyh = trArr.children("td").eq(7).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.zjyl = trArr.children("td").eq(8).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.zjl = trArr.children("td").eq(9).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.pjylyh = trArr.children("td").eq(10).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.sxxx = trArr.children("td").eq(10).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.bz = trArr.children("td").eq(10).text().replace(/[\r\n\t]/g, "").replace(/(^\s*)|(\s*$)/g, "");
                Wbdg.bidName = bidName;
                dgList.push(Wbdg);

            }

            console.log(dgList);

            if(dgList.length > 0){
                $.ajax({
                    "type": 'post',
                    "url": path + '/wbsj/wbdg-save',
                    "sync": true,
                    "contentType": "application/json;charset=utf-8",
                    "data": JSON.stringify(dgList),
                    "success": function (resp) {
                        var obj = JSON.parse(resp);
                        if (obj) {
                            if (obj.errmsg) {
                                notyError(obj.errmsg);
                            } else if (obj.msg) {
                                notySuccess(obj.msg);
                            }
                        }
                    }
                });
            }else{
                notyError("没有盾构数据无法保存！");
            }
        }

        function wbjcTime(){
            getSuzData1();
            saveSuzData1();
            getSuzData2();
            saveSuzData2();
        }


        var t1 = window.setInterval(wbjcTime,600000);

    </script>
</body>
</html>

