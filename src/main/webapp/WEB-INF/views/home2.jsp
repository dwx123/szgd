<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
    <meta http-equiv="Expires" content="0" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="${ctx}/assets/bootstrap-extension/font-awesome/css/font-awesome.min.css"
          rel="Stylesheet">
    <link rel="stylesheet" type="text/css" href="${ctx}/assets/jquery-ui-1.10.4/jquery-ui-1.10.4.custom.min.css">
    <link type="text/css" href="${ctx}/assets/css/custom.css" rel="Stylesheet">
    <script type="text/javascript" src="${ctx }/js/jquery.min.js"></script>
    <script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
    <script src="${ctx}/assets/jquery-ui-1.10.4/jquery-ui-1.10.4.custom.min.js" type="text/javascript" charset="UTF-8"></script>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=3.0&ak=twMLlqvSS6MGAqWDTVznyOMy"></script>
    <script type="text/javascript" src="http://api.map.baidu.com/library/TextIconOverlay/1.2/src/TextIconOverlay_min.js"></script>
    <script type="text/javascript" src="http://api.map.baidu.com/library/MarkerClusterer/1.2/src/MarkerClusterer_min.js"></script>
    <title>苏州地铁6号线智慧工地实时监控</title>
    <style type="text/css">
        *{
            box-sizing: border-box;
            color: #000;
        }
        *,body{
            margin: 0;
            padding: 0;
        }
        html,body{
            height: 100%;
            min-height: 100%;
            overflow: hidden;
        }
        body{
            background: #ddd;
        }
        a {
            color: #8A6EE9;
        }
        .header{
            height: 80px;
            display: flex;
            display: -webkit-flex;
            position: absolute;
            width: 100%;
            left: 0;
            top: 0;
        }
        .header_l,.header_r{
            width: 23.9%;
            /*background: #183DA1;*/
            /*border-bottom: 1px solid #00C1FF;*/
            background-size: 100% 100%;
            height: 28px;
        }
        .header_l {
            background-image: url('${ctx}/assets/img/new_title1.png');
        }
        .header_r {
            background-image: url('${ctx}/assets/img/new_title3.png');
        }
        .header_c{
            flex-grow: 1;
            -webkit-flex-grow: 1;
            text-align: center;
            color: #fff;
            font-size: 40px;
            padding-top: 10px;
            letter-spacing: 2px;
            background-image: url('${ctx}/assets/img/new_title2.png');
            background-size: 100% 100%;
            z-index: 99;
        }
        .content{
            width: 100%;
            height: 100%;
            position: relative;
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            -webkit-justify-content: space-between;

        }
        .content .item{
            padding-top: calc(1.3% + 28px);
            height: 100%;
            padding-bottom: 1.3%;
            display: flex;
            display: -webkit-flex;
            flex-direction: column;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
            justify-content: space-between;
            -webkit-justify-content: space-between;
        }
        .block-item{
            border-radius: 10px;
            position: relative;
            background: #fff;
            padding-top: 40px;
        }
        .block-item.cus_stop{
            background: #8A6EE9;
        }
        .block-item.cus_stop .block-content {
            padding-top: 0;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
        }
        .block-item.cus_stop .block-content .weather {
            width: 150px;
            text-align: center;
            height: 68px;
        }
        .block-item.cus_stop .block-content .weather img {
            height: 100%;
            margin-top: -5px;
        }
        .block-item.cus_stop .block-content .realtime {
            position: absolute;
            bottom: 20px;
            left:20px;
        }
        .block-item.cus_stop .block-content .realtime span {
            color: #fff;
        }
        .block-item.cus_stop .block-content .realtime span.clock {
            width: 150px;
            display: inline-block;
        }
        .cus_stop .item-title {
            padding-left: 20px;
        }
        .cus_stop .item-title:before {
            display: none;
        }
        .cus_stop .item-title,.cus_stop #curStop {
            color: #fff;
        }
        .cus_stop #curStop {
            font-size: 30px;
            margin-top: -5%;
            text-align: center;
        }
        .item-title{
            position: absolute;
            width: 100%;
            top: 0;
            left: 0;
            line-height: 40px;
            font-size: 18px;
            color: #000;
            letter-spacing: 1.2px;
            padding-left: 30px;
            /*font-weight: bold;*/
        }
        .item-title:before {
            position: absolute;
            height: 24px;
            content: '';
            top: 8px;
            left: 20px;
            width: 5px;
            background: #647EE1;
        }
        .item-title a{
            font-size: 14px;
            text-decoration: none;
            position: absolute;
            right: 20px;
            top: 0;
            color:#8A6EE9;
        }
        .item-title a:hover,.item-title a:active,.item-title a:focus{
            color:#8A6EE9;
        }
        .block-content{
            height: 100%;
            padding: 20px;
            /*padding: 0 20px 20px;*/
            display: flex;
            display: -webkit-flex;
            flex-direction: column;
            -webkit-flex-direction: column;
            -ms-flex-direction: column;
        }
        .content .content_l{
            /*width: 20.8%;*/
            /*margin-left: 1.3%;*/
            width: 24.3%;
            margin-left: 1%;
        }
        .content_l .block-item:nth-child(1) {
            height: 15.99%;
        }
        .content_l .block-item:nth-child(2) {
            /*height: 55.64%;*/
            height: 23%;
        }
        .content_l .block-item:nth-child(3) {
            /*height: 23.976%;*/
            height: 23%;
        }
        .content_l .block-item:nth-child(4) {
            /*height: 23.976%;*/
            height: 34%;
        }
        .program-progress{
            padding-right: 60px;
            position: relative;
        }
        .program{
            justify-content: space-between;
            -webkit-justify-content: space-between;
        }
        .program-progress .title{
            font-size: 16px;
            letter-spacing: 1.2px;
            padding-left: 3px;
            margin-bottom: 6px;
        }
        .program-progress .progress-bar{
            height: 24px;
            background: #D8D8D8;
            position: relative;
            border-radius: 12px;
        }
        .program-progress .progress-bar .inner-bar{
            position: absolute;
            height: 100%;
            left: 0;
            top: 0;
            background: linear-gradient(to right, #FF5400, #F7EE00, #B7EB81);
            background: -webkit-linear-gradient(left, #FF5400, #F7EE00, #B7EB81);
            border-radius: 12px;
            width: 0;
            transition: width 2s;
            -moz-transition: width 2s;
            -webkit-transition: width 2s;
            -o-transition: width 2s;
        }
        .program-progress .num{
            position: absolute;
            width: 60px;
            height: 24px;
            right: 0;
            bottom: 0;
            padding-left: 18px;
        }
        .program-pie{
            display: flex;
            display: -webkit-flex;
            /*justify-content: space-between;*/
            /*-webkit-justify-content: space-between;*/
            /*flex-wrap: wrap;*/
            /*-webkit-flex-wrap: wrap;*/
            flex-direction: column;
            -webkit-flex-direction: column;
            height: 100%;
            justify-content: space-around;
            -webkit-justify-content: space-around;
        }
        .pie-title{
            font-size: 16px;
            font-weight: bold;
            letter-spacing: 1.2px;
            padding-left: 3px;
        }
        .program-pie .pieitem{
            width: 115px;
            height: 140px;
        }
        .program-pie .pie-row{
            display: flex;
            display: -webkit-flex;
            justify-content: space-around;
            -webkit-justify-content: space-around;
        }
        .program-pie .pieitem .title{
            text-align: center;
            font-size: 16px;
            letter-spacing: 1.2px;
        }
        .program-pie canvas {
            cursor: pointer;
        }
        .equip{
            justify-content: space-evenly;
            -webkit-justify-content: space-evenly;
            -moz-justify-content: space-evenly;
        }
        .equip .equip-item{
            font-size: 16px;
            letter-spacing: 1.2px;
            display: flex;
            display: -webkit-flex;
        }
        .equip .equip-item .name{
            font-weight: bold;
            width: 120px;
        }
        .equip .equip-item .status{
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            -webkit-justify-content: space-between;
            flex-grow: 1;
            -webkit-flex-grow: 1;
        }
        .radiowrap{
            width: 55px;
            height: 30px;
            border-radius: 15px;
            border: 2px solid #92A3C1;
            display: inline-block;
            vertical-align: middle;
            background: radial-gradient(#FFFFFF, #D8D8D8);
            background: -webkit-radial-gradient(#FFFFFF, #D8D8D8);
            position: relative;
            cursor: pointer;
        }
        .radiowrap .radio{
            width: 27px;
            height: 27px;
            margin: 0;
            border: 2px solid #92A3C1;
            position: absolute;
            top: 0;
            border-radius: 50%;
            left: auto;
            right: 0;
            background: radial-gradient(#F3FFFF, #9D9D9D);
            background: -webkit-radial-gradient(#F3FFFF, #9D9D9D);
        }
        .radiowrap.active .radio{
            left: 0;
            background: radial-gradient(#B0FDFF, #00B5FF);
            background: -webkit-radial-gradient(#B0FDFF, #00B5FF);
        }
        .content .content_c{
            /* padding-top: calc(1.3% + 80px); */
            /*width: 52%;*/
            width: 47%;
            /* display: flex;
            flex-direction: row;
            flex-wrap: wrap; */
        }
        .content_c .block-item.stops{
            padding-top: 52px;
            border-top: none;
            /*height: 47.654%;*/
            height: 49.955%;
            position: relative;
            width: 100%;
        }
        .content_c .stops .img_bor{
            position:absolute;
            width: calc(100% + 4px);
            left: -2px;
            top: -1px;
        }
        .content_c .stops .block-content .list_stops{
            list-style: none;
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            -webkit-justify-content: space-between;
            padding-top: 12px;
            margin-top: 20px;
        }
        .content_c .stops .block-content .list_stops li {
            width: 25px;
            text-align: center;
            padding: 5px 0;
            border-radius: 12px;
            cursor: pointer;
            position: relative;
        }
        .content_c .stops .block-content .list_stops li.active{
            background: #FFDA30;
            color: #262B4C;
        }
        .content_c .stops .block-content .list_stops li::before{
            content: '';
            width: 10px;
            height: 10px;
            border: 2px solid #FFDA30;
            background: #fff;
            position: absolute;
            left: 5.5px;
            top: -20px;
            border-radius: 10px;
        }
        .content_c .block-item{
            width: 32%;
            background: none;
        }
        .content_c .video-block{
            /*height: 23.976%;*/
            height: 16%;
            overflow: hidden;
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            -webkit-justify-content: space-between;
        }
        .content_c .video-block .block-item{
            height: 100%;
            width: 100%;
            /*padding-top: 0;*/
            padding-top: 40px;
        }
        .content_c .video-block .block-item .plugin{
            height: 150%;
            margin-top: 0;
        }
        .content_c .video-block .block-item img{
            width: 100%;
            height: 100%;
        }
        .content_c .video-block .block-item .item-title{
            color: #000;
            background: #fff;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            /*border-radius: 10px;*/
        }
        .content_c .video-block .block-item .item-title a{
            top: 0;
            right: 10px;
            z-index: 100;
        }
        .content_c .video-block .block-item .item-title i {
            color: #000;
            font-size: 18px;
            margin-right: 5px;
        }

        .content_c .tunnelAndPit-block{
            height: 32.5%;
            overflow: hidden;
            display: flex;
            display: -webkit-flex;
            justify-content: space-between;
            -webkit-justify-content: space-between;
        }
        .content_c .tunnelAndPit-block .block-item{
            height: 100%;
            width: 49%;
            padding-top: 40px;
        }
        .content_c .tunnelAndPit-block .block-item .item-title{
            color: #000;
            background: #fff;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
        }

        .content_c .tunnelAndPit-block .block-item .block-content{
            color: #000;
            background: #fff;
            border-bottom-left-radius: 10px;
            border-bottom-right-radius: 10px;
        }

        .content .content_r{
            /*width: 22.3%;*/
            /*margin-right: 1.3%;*/
            width: 24.3%;
            margin-right: 1%;
        }
        .content_r .block-item.people_entrance{
            /*height: 39.96%;*/
            height: 31.768%;
        }
        .euipcontent.block-content,.content_r .people_entrance .block-content,.content_r .vehicle_entrance .block-content{
            display: block;
            overflow: hidden;
        }
        .block-content .list-title{
            background: #647EE1;
            font-weight: bold;
            padding: 9px 0;
            letter-spacing: 1.2px;
            border-radius: 5px;
        }
        .block-content .list-title div {
            color: #fff;
        }
        .block-content .list-title,.block-content .lists .list{
            display: flex;
            display: -webkit-flex;
        }
        .block-content .lists .list{
            padding: 9px 0;
            font-size: 12px;
        }
        .block-content .lists .list:nth-child(2n) {
            background: #E2E5EA;
            border-radius: 5px;
        }
        .block-content .name,.block-content .time,.block-content .timespan{
            text-align: center;
        }
        .block-content .name,.block-content .timespan{
            width: 90px;
        }
        .block-content .name,.block-content .time,.block-content .timespan2{
            text-align: center;
        }
        .block-content .name,.block-content .timespan2{
            width: 110px;
        }
        /*.content_r .vehicle_entrance .block-content .name{*/
        /*width: 110px;*/
        /*}*/
        .block-content .time{
            /*flex-grow: 1;*/
            /*-webkit-flex-grow: 1;*/
            width: 120px;
        }
        /*.block-content .timespan{*/
        /*width: 110px;*/
        /*}*/
        .content_r .block-item.vehicle_entrance{
            height: 31.768%;
        }
        .content_r .block-item.alarm{
            /*height: 23.976%;*/
            height: 32.168%;
        }
        .alarm .alaritem{
            /*margin-top: 20px;*/
            padding: 0 30px 0 170px;
            height: 22.666%;
            /*border-bottom: 1px solid #ddd;*/
            position: relative;
            width:50%;
        }
        .alarm .alaritem span{
            position: absolute;
            width: 170px;
            top: 50%;
            margin-top: -10px;
        }
        .alarm .alaritem span.title {
            left: 0;
            padding-left: 20px;
        }
        .alarm .alaritem .alarmpercent {
            position: absolute;
            top: 50%;
            margin-top: -10px;
            width: calc(100% - 220px);
            left: 170px;
            height: 19px;
        }
        .alarm .block-content .alaritem:nth-child(1) .alarmCount a{
            color: #8A6EE9;
        }
        .alarm .block-content .alaritem:nth-child(2) .alarmCount a{
            color: #CAB4FF;
        }
        .alarm .block-content .alaritem:nth-child(3) .alarmCount a{
            color: #4A73DC;
        }
        .alarm .block-content .alaritem:nth-child(4) .alarmCount a{
            color: #FFCA00;
        }
        .alarm .block-content .alaritem:nth-child(5) .alarmCount a{
            color: #6076FF;
        }
        .alarm .block-content .alaritem:nth-child(6) .alarmCount a{
            color: #8A6EE9;
        }
        .alarm .block-content .alaritem:nth-child(1) .alarmpercent .percent {
            background: #8A6EE9;
        }
        .alarm .block-content .alaritem:nth-child(2) .alarmpercent .percent {
            background: #CAB4FF;
        }
        .alarm .block-content .alaritem:nth-child(3) .alarmpercent .percent {
            background: #4A73DC;
        }
        .alarm .block-content .alaritem:nth-child(4) .alarmpercent .percent {
            background: #FFCA00;
        }
        .alarm .block-content .alaritem:nth-child(5) .alarmpercent .percent {
            background: #6076FF;
        }
        .alarm .block-content .alaritem:nth-child(6) .alarmpercent .percent {
            background: #8A6EE9;
        }
        .alarm .alaritem .alarmpercent .percent {
            display: block;
            height: 100%;
            border-radius: 19px;
            width: 0;
            transition: width 0.5s;
            -moz-transition: width 0.5s; /* Firefox 4 */
        }
        .alarm .alaritem span.alarmCount {
            width: 50px;
            padding-left: 15px;
            right: 0;
        }
        .alarm .alaritem:last-child {
            border-bottom: none;
        }
        /*.alarm .alaritem span{*/
        /*font-size: 20px;*/
        /*color: #FFDA30;*/
        /*margin-left: 10px;*/
        /*}*/
        .content_r .alarm .block-content{
            justify-content: space-evenly;
            -moz-justify-content: space-evenly;
            padding: 0 20px;
        }

        .content_c .block-item.stops.map-stops{
            padding: 0;
            overflow: hidden;
        }
        .content_c .stops.map-stops .img_bor {
            z-index: 9;
        }
        .content_c .block-item.stops.map-stops .block-content{
            padding: 0;
            height: calc(100% + 50px);
        }
        .block-content.map-content .list_stops{
            width: 100%;
            height: 100%;
            margin-top: 0!important;
        }
        .BMapLabel {
            color: #000;
        }

        #bimImage {
            background-image: url('${ctx}/assets/img/BIM.jpg');
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
    </style>
</head>
<body>

<div class="content">

    <div class="header">
        <div class="header_l"></div>
        <div class="header_c">
            苏州轨道交通6号线信息化管理平台
        </div>
        <div class="header_r"></div>
    </div>
    <div class="content_l item">
        <div class="block-item cus_stop">
            <div class="item-title">
                当前站点
            </div>
            <div class="block-content">
                <div id="curStop"></div>
                <div class="weather">
                </div>
                <div class="realtime">
                    <span class="clock"></span>
                    <span class="tianqi"></span>
                </div>
                <marquee direction="left" scrollamount="2" style="width: 100%;height: 20px;position: absolute; bottom: 0;color: #fff;left: 0;" id="">
                    <a id="marquee" href="#" style="color: #fff;">李四的《电工》证书将在2019-04-30到期，请及时办理！</a>
                </marquee>
            </div>
        </div>
        <div class="block-item">
            <div class="item-title">
                设备信息
                <a href="javascript:;" onclick="openDialogWin('equipment','设备进出记录')">查看详细</a>
            </div>
            <div class="block-content euipcontent">
                <div class="list-title">
                    <div class="name">设备名称</div>
                    <div class="time">进入时间</div>
                    <div class="time">离开时间</div>
                    <div class="timespan">停留时间</div>
                </div>
                <div class="lists"  id="equipEnterExitList">
                </div>
            </div>
        </div>
        <div class="block-item">
            <div class="item-title">
                隐患信息
                <a href="javascript:;" onclick="openDialogWin('hiddenDanger','隐患详细信息')">查看详细</a>
            </div>
            <div class="block-content hiddencontent">
                <div class="list-title">
                    <div class="name">隐患主题</div>
                    <div class="name">上报人</div>
                    <div class="time">上报时间</div>
                    <div class="timespan2">整改时间</div>
                </div>
                <div class="lists"  id="hiddenDangerList">
                </div>
            </div>
        </div>
        <div class="block-item">
            <div class="item-title">
                项目进度
                <%--<a href="javascript:;" onclick="openDialogWin('project','项目信息')">查看详细</a>--%>
            </div>
            <div class="block-content program">
                <div class="program-pie">
                    <div class="pie-row">
                        <div class="pieitem">
                            <canvas id="canvasPie3" width="115" height="115" onclick="openDialogWin('site','主体-围护结构')"></canvas>
                            <div class="alaritem" style="font-size: 13px">主体-围护结构</div>
                        </div>
                        <div class="pieitem">
                            <canvas id="canvasPie4" width="115" height="115" onclick="openDialogWin('section','主体-土方开挖')"></canvas>
                            <div class="alaritem" style="font-size: 13px">主体-土方开挖</div>
                        </div>
                        <div class="pieitem">
                            <canvas id="canvasPie5" width="115" height="115" onclick="openDialogWin('segment','主体-主体结构')" ></canvas>
                            <div class="alaritem" style="font-size: 13px">主体-主体结构</div>
                        </div>
                    </div>
                    <div class="pie-row">
                        <div class="pieitem">
                            <canvas id="canvasPie1" width="115" height="115" onclick="openDialogWin('tunnelling','盾构施工进度')" ></canvas>
                            <div class="alaritem" style="font-size: 13px">盾构施工进度</div>
                        </div>
                        <div class="pieitem">
                            <canvas id="canvasPie2" width="115" height="115" onclick="openDialogWin('accessor','附属结构施工进度')" ></canvas>
                            <div class="alaritem" style="font-size: 13px">附属结构施工进度</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content_c item">
        <div class="block-item stops map-stops">
            <img src="${ctx}/assets/img/new_map_top.png" class="img_bor" />
            <div class="block-content map-content">
                <div class="list_stops" id="listStops"></div>
            </div>
            <%--<div style="position: absolute;width: 78px;height: 25px;background: #fff;bottom: 25px;"></div>--%>
            <%--<div style="position: absolute;width: 490px;height: 25px;background: #fff;bottom: 0;"></div>--%>
        </div>
        <%--<div class="video-block">--%>
        <%--<div class="block-item">--%>
        <%--<div class="item-title">--%>
        <%--&lt;%&ndash;<i class="fa fa-video-camera"></i>&ndash;%&gt;--%>
        <%--实时监控--%>
        <%--<a href="javascript:;" onclick="openDialogWin('camera','摄像头监控列表')">查看详细</a>--%>
        <%--</div>--%>
        <%--<div id = "divPlugin" class="plugin">--%>
        <%--</div>--%>
        <%--</div>--%>
        <%--</div>--%>
        <div class="tunnelAndPit-block">
            <div class="block-item">

                <div class="item-title">
                    <!-- 施工人员统计信息 -->
                    人员在场人数：共<span id="personnelBePresentCount"></span>人
                    <a href="javascript:;" onclick="openDialogWin('personnel','人员进出记录')">查看详细</a>
                </div>
                <div class="block-content">
                    <div class="list-title">
                        <div class="name">姓名</div>
                        <div class="time">进入时间</div>
                        <div class="time">离开时间</div>
                        <div class="timespan">停留时间</div>
                    </div>
                    <div class="lists" id="personnelEnterExitList">
                    </div>
                </div>



            </div>

            <div class="block-item">


                <div class="item-title">
                    <!-- 车辆统计信息 -->
                    车辆在场数量：共<span id="vehicleBePresentCount"></span>辆
                    <a href="javascript:;" onclick="openDialogWin('vehicle','车辆进出记录')">查看详细</a>
                </div>
                <div class="block-content">
                    <div class="list-title">
                        <div class="name">车牌号</div>
                        <div class="time">进入时间</div>
                        <div class="time">离开时间</div>
                        <div class="timespan">停留时间</div>
                    </div>
                    <div class="lists" id="vehicleEnterExitList">
                    </div>
                </div>









            </div>
        </div>

        <div class="block-item alarm" style="height: 16%;background: #ffffff;width: 100%;">
            <div class="item-title">
                提醒事项
            </div>
            <div class="block-content" style="flex-direction: row;flex-wrap: wrap;padding: 5px;">
                <div class="alaritem">
                    <div class="alarmpercent"><div class="percent"></div></div>
                    <span class="title">人员资质有效期提醒</span>
                    <span class="alarmCount"><a id="certificationRemindCount" href="javascript:;" onclick="openDialogWin('certification','人员资质有效期提醒')">0</a></span>
                </div>
                <div class="alaritem">
                    <div class="alarmpercent"><div class="percent"></div></div>
                    <span class="title">人员数量提醒</span>
                    <span class="alarmCount"><a id="peoplesRemindCount" href="javascript:;" onclick="openDialogWin('peoples','特定时间内人数提醒')">0</a></span>
                </div>
                <div class="alaritem">
                    <div class="alarmpercent"><div class="percent"></div></div>
                    <span class="title">人员体检提醒</span>
                    <span class="alarmCount"><a id="physicalExaminationRemindCount" href="javascript:;" onclick="openDialogWin('examination','人员体检提醒')">0</a></span>
                </div>
                <div class="alaritem">
                    <div class="alarmpercent"><div class="percent"></div></div>
                    <span class="title">设备检测提醒</span>
                    <span class="alarmCount"><a id="equipmentChkRemindCount" href="javascript:;" onclick="openDialogWin('check','设备检测提醒')">0</a></span>
                </div>
                <div class="alaritem">
                    <div class="alarmpercent"><div class="percent"></div></div>
                    <span class="title">设备保险提醒</span>
                    <span class="alarmCount"><a id="equipmentInsureRemindCount" href="javascript:;" onclick="openDialogWin('insurance','设备保险提醒')">0</a></span>
                </div>
                <div class="alaritem">
                    <div class="alarmpercent"><div class="percent"></div></div>
                    <span class="title">隐患提醒</span>
                    <span class="alarmCount"><a id="hiddenDangerCount" href="javascript:;" onclick="openDialogWin('hiddenDanger','隐患提醒')">0</a></span>
                </div>
            </div>

        </div>
        <%--<div class="video-block">--%>
        <%--<div class="block-item">--%>
        <%--<div class="item-title">--%>
        <%--&lt;%&ndash;<i class="fa fa-video-camera"></i>&ndash;%&gt;--%>
        <%--实时监控--%>
        <%--<a href="javascript:;" onclick="openDialogWin('camera','摄像头监控列表')">查看详细</a>--%>
        <%--</div>--%>
        <%--&lt;%&ndash;<div id = "divPlugin" class="plugin">&ndash;%&gt;--%>
        <%--&lt;%&ndash;</div>&ndash;%&gt;--%>
        <%--</div>--%>
        <%--</div>--%>
    </div>
    <div class="content_r item">
        <div class="block-item people_entrance">







            <div class="item-title">
                盾构机
            </div>
            <div class="block-content tunnel">
                <div class="list-title" id="tunnel_title">
                    <div class="name">工点名称</div>
                    <div class="name">盾构编号</div>
                    <div class="name">盾构类型</div>
                    <div class="name">进度</div>
                </div>
                <div class="lists"  id="tunnelList">
                </div>
            </div>







        </div>
        <div class="block-item vehicle_entrance">



            <div class="item-title">
                基坑监测
            </div>
            <div class="block-content pit">
                <div class="list-title">
                    <div class="name">测点</div>
                    <div class="name">安全状态</div>
                    <div class="name">累计变化值</div>
                    <div class="name">变形速率</div>
                    <div class="name">最大值</div>
                    <div class="name">最小值</div>
                </div>
                <div class="lists"  id="pitList">
                </div>
            </div>








        </div>
        <div class="block-item alarm">
            <div class="item-title">
                BIM
            </div>
            <div class="block-content" id="bimImage">
                <%--<div class="alaritem">--%>
                <%--<div class="alarmpercent"><div class="percent"></div></div>--%>
                <%--<span class="title">人员资质有效期提醒</span>--%>
                <%--<span class="alarmCount"><a id="certificationRemindCount" href="javascript:;" onclick="openDialogWin('certification','人员资质有效期提醒')">0</a></span>--%>
                <%--</div>--%>
                <%--<div class="alaritem">--%>
                <%--<div class="alarmpercent"><div class="percent"></div></div>--%>
                <%--<span class="title">人员数量提醒</span>--%>
                <%--<span class="alarmCount"><a id="peoplesRemindCount" href="javascript:;" onclick="openDialogWin('peoples','特定时间内人数提醒')">0</a></span>--%>
                <%--</div>--%>
                <%--<div class="alaritem">--%>
                <%--<div class="alarmpercent"><div class="percent"></div></div>--%>
                <%--<span class="title">人员体检提醒</span>--%>
                <%--<span class="alarmCount"><a id="physicalExaminationRemindCount" href="javascript:;" onclick="openDialogWin('examination','人员体检提醒')">0</a></span>--%>
                <%--</div>--%>
                <%--<div class="alaritem">--%>
                <%--<div class="alarmpercent"><div class="percent"></div></div>--%>
                <%--<span class="title">设备检测提醒</span>--%>
                <%--<span class="alarmCount"><a id="equipmentChkRemindCount" href="javascript:;" onclick="openDialogWin('check','设备检测提醒')">0</a></span>--%>
                <%--</div>--%>
                <%--<div class="alaritem">--%>
                <%--<div class="alarmpercent"><div class="percent"></div></div>--%>
                <%--<span class="title">设备保险提醒</span>--%>
                <%--<span class="alarmCount"><a id="equipmentInsureRemindCount" href="javascript:;" onclick="openDialogWin('insurance','设备保险提醒')">0</a></span>--%>
                <%--</div>--%>
                <%--<div class="alaritem">--%>
                <%--<div class="alarmpercent"><div class="percent"></div></div>--%>
                <%--<span class="title">隐患提醒</span>--%>
                <%--<span class="alarmCount"><a id="hiddenDangerCount" href="javascript:;" onclick="openDialogWin('hiddenDanger','隐患提醒')">0</a></span>--%>
                <%--</div>--%>
                <%--&lt;%&ndash;<div class="alaritem">--%>
                <%--项目进度提醒<span><a href="javascript:;" onclick="openDialogWin('schedule','项目进度提醒')">10</a></span>--%>
                <%--</div>&ndash;%&gt;--%>
            </div>
        </div>
    </div>
</div>

<div style="display:none;overflow:hidden;padding:3px" id="dialog"><iframe frameborder="no" border="0" marginwidth="0" marginheight="0" id="showRemindMessage"  scrolling="yes"  width="100%" height="100%"></iframe></div>
</body>

<script type="text/javascript" src="${ctx }/webcomponents/codebase/webVideoCtrl.js"></script>
<script type="text/javascript" src="${ctx }/webcomponents/codebase/webVideo.js"></script>
</html>
<script>
    var siteId ='';
    var bidId = '';
    var webVideoArray = new Array();
    function openDialogWin(type,title){
        var url ;
        if (type == 'camera')
        {
            url = "${ctx}/camera/toCameraMonitorList?from=home&siteId="+siteId+"&bidId="+bidId;


        }else if (type == 'site')
        {
            url = "${ctx}/mainWorkSchedule/toWHJG?from=home&siteId="+siteId+"&bidId="+bidId;

        }else if (type == 'section')
        {
            url = "${ctx}/mainWorkSchedule/toTFKW?from=home&siteId="+siteId+"&bidId="+bidId;

        }else if (type == 'segment')
        {
            url = "${ctx}/mainWorkSchedule/toZTJG?from=home&siteId="+siteId+"&bidId="+bidId;

        }else if (type == 'tunnelling')
        {
            url = "${ctx}/tunnellingWorkSchedule/toTunnelling?from=home&siteId="+siteId+"&bidId="+bidId;

        }else if (type == 'accessor')
        {
            url = "${ctx}/accessorStructureSchedule/toAccessor?from=home&siteId="+siteId+"&bidId="+bidId;

        }else if (type == 'personnel')
        {
            url = "${ctx}/personnelWork/toPersonnelEnterExitList?from=home&siteId="+siteId;
            title = '人员进出记录';
        }else
        if (type == 'vehicle')
        {
            url = "${ctx}/vehicleEnterExit/toVehicleEnterExitList?from=home&siteId="+siteId;
        }else
        if (type == 'equipment')
        {
            url = "${ctx}/equipEnterExit/toEquipEnterExitList?from=home&siteId="+siteId;
        }else
        if (type == 'project')
        {
            url = "${ctx}/project/toProjectInfoList?from=home";
        }else if (type == 'hiddenDanger')
        {
            url = "${ctx}/hiddenDanger/toHiddenDangerList?from=home&siteId="+siteId+"&bidId="+bidId;
        }else
        {
            url = "${ctx}/remind/"+type+"-list?from=home";

        }
        $("#showRemindMessage").attr("src",url); //设置IFRAME的SRC;
        $("#dialog").dialog({
            bgiframe: true,
            resizable: true, //是否可以重置大小
            height: 700, //高度
            width: 1500, //宽度
            draggable: true, //是否可以拖动。
            title: title,
            modal: true,
            open: function (e) {  //打开的时候触发的事件
                document.body.style.overflow = "hidden"; //隐藏滚动条
            },
            close: function () { //关闭Dialog时候触发的事件
                document.body.style.overflow = "visible";  //显示滚动条
            }
        });
    }

    function drawMain (drawingElem, percent, forecolor, bgcolor) {
        var context = drawingElem.getContext('2d')
        var centerX = drawingElem.width / 2
        var centerY = drawingElem.height / 2
        var rad = Math.PI * 2 / 100
        var speed = 0

        function backgroundCircle () {
            context.save()
            context.beginPath()
            context.lineWidth = 15
            var radius = centerX - context.lineWidth
            context.lineCap = 'round'
            context.strokeStyle = bgcolor
            context.arc(centerX, centerY, radius, 0, Math.PI * 2, false)
            context.stroke()
            context.closePath()
            context.restore()
        }

        function foregroundCircle (n) {
            context.save()
            context.strokeStyle = forecolor
            context.lineWidth = 15
            context.lineCap = 'round'
            var radius = centerX - context.lineWidth
            context.beginPath()
            context.arc(centerX, centerY, radius, -Math.PI / 2, -Math.PI / 2 + n * rad, false) // 用于绘制圆弧context.arc(x坐标，y坐标，半径，起始角度，终止角度，顺时针/逆时针)
            context.stroke()
            context.closePath()
            context.restore()
        }

        function text (n) {
            context.save() // save和restore可以保证样式属性只运用于该段canvas元素
            context.fillStyle = '#000'
            var fontSize = 24
            context.font = fontSize + 'px Helvetica'
            var textWidth = context.measureText(n + '%').width
            context.fillText(n + '%', centerX - textWidth / 2, centerY + fontSize / 2)
            context.restore()
        }

        context.clearRect(0, 0, drawingElem.width, drawingElem.height)
        backgroundCircle()
        foregroundCircle(percent)
        text(percent)
    }

    var canvasPie1 = document.getElementById('canvasPie1')
    var canvasPie2 = document.getElementById('canvasPie2')
    var canvasPie3 = document.getElementById('canvasPie3')
    var canvasPie4 = document.getElementById('canvasPie4')
    var canvasPie5 = document.getElementById('canvasPie5')

    //定时器
    var totalSchedulePercentage_Timer = '';
    var personnelEnterExitList_Timer = '';
    var vehicleEnterAndExitList_Timer = '';
    var equipEnterAndExitList_Timer = '';
    var hiddenDangerList_Timer = '';
    var pitList_Timer = '';
    var tunnelList_Timer = '';
    var marquee_Timer = '';
    var remindCount_Timer = '';
    var remindCount_Timely_Timer = '';

    //定时器状态
    var totalSchedulePercentage_Timer_Status = '0';
    var personnelEnterExitList_Timer_Status = '0';
    var vehicleEnterAndExitList_Timer_Status = '0';
    var equipEnterAndExitList_Timer_Status = '0';
    var hiddenDangerList_Timer_Status = '0';
    var pitList_Timer_Status = '0';
    var tunnelList_Timer_Status = '0';
    var marquee_Timer_Status = '0';
    var remindCount_Timer_Status = '0';
    var remindCount_Timely_Timer_Status = '0';

    $(function() {
        initSiteInfo();
        //totalSchedulePercentage_Timer = setInterval(getTotalSchedulePercentage(),86400000); //每24小时刷新一次
        //personnelEnterExitList_Timer = setInterval(getPersonnelEnterExitList(),5000); //每5秒刷新一次
        //vehicleEnterAndExitList_Timer = setInterval(getVehicleEnterAndExitList(),5000); //每5秒刷新一次
        //equipEnterAndExitList_Timer = setInterval(getEquipEnterAndExitList(),86400000); //每24小时刷新一次
        //marquee_Timer = setInterval(getMarquee(),300000); //每5分刷新一次

        //remindCount_Timer = setInterval(getRemindCount(),86400000); //每24小时刷新一次
        //remindCount_Timely_Timer = setInterval(getRemindCount_Timely(),300000); //每5分刷新一次

    })
    function initSiteInfo() {
        var map = new BMap.Map("listStops");
        // 创建地图实例
        // 创建点坐标
        var point = new BMap.Point(120.726201,31.300263);
        map.centerAndZoom(point, 13);
        map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
//        map.addControl(new BMap.NavigationControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT}));
        $.ajax({
            contentType: "application/json",
            type: "post",
            data: JSON.stringify({}),
            url: "<%=request.getContextPath()%>/site/getSites",
            async: false,
            success: function (result) {
                markers = [];
                var labels = [];
                siteInfo = result;
                var pois = []
                var markIcon = new BMap.Icon("${ctx}/assets/img/mapdot.png", new BMap.Size(20,20));
                for (var i = 0; i < result.length; i++) {
                    pois.push(new BMap.Point(result[i].gpsx, result[i].gpsy))
                    var marker = new BMap.Marker(new BMap.Point(result[i].gpsx, result[i].gpsy),{icon: markIcon});
                    markers.push(marker)
                    map.addOverlay(marker);
                    var label = new BMap.Label(result[i].name,{offset:new BMap.Size(0,-20)});
                    label.setStyle({
                        color : "#fff",
                        fontSize : "12px",
                        backgroundColor: "rgb(55, 77, 255)",
                        padding: "2px 5px",
                        borderRadius: '5px',
                        boxShadow: '0 2px 12px 0 rgba(0,0,0,0.3)',
                        border: '2px solid #fff',
                        cursor: 'pointer'
                    });
                    labels.push(label)
                    marker.setLabel(label);
                    if (result[i].defaultShow == 1) {
                        $('#curStop').text(result[i].name);
                        siteId = result[i].id;
                        bidId = result[i].bidId;
                        getTotalSchedulePercentage();
                        getPersonnelEnterExitList();
                        getVehicleEnterAndExitList();
                        getEquipEnterAndExitList();
                        getHiddenDangerInfoList();
                        getPitList();
                        getTunnelList();
                        getRemindCount();
                        getRemindCount_Timely();
//                        var webVideo = new WebVideo();
//                        webVideo.divPlugin = "divPlugin";
//                        webVideo.init();
//                        stopCameraList();
//                        getCameraList();
                        getMarquee();
                    }
                }
                var selfPolyline =new BMap.Polyline(pois, {
                    strokeWeight:'5',//折线的宽度，以像素为单位
                    strokeOpacity: 1,//折线的透明度，取值范围0 - 1
                    strokeColor:"rgb(125, 139, 255)" //折线颜色
                })
                map.addOverlay(selfPolyline);
                markers.map(function (item) {
                    item.addEventListener('click', function () {
                        var currentSite = siteInfo.filter(function (site) {
                            return site.gpsx == item.point.lng && site.gpsy == item.point.lat
                        })[0]
                        $('#curStop').text(currentSite.name);
                        siteId = currentSite.id;
                        bidId = currentSite.bidId;
                        getTotalSchedulePercentage();
                        getPersonnelEnterExitList();
                        getVehicleEnterAndExitList();
                        getEquipEnterAndExitList();
                        getHiddenDangerInfoList();
                        getPitList();
                        getTunnelList();
                        getRemindCount();
                        getRemindCount_Timely();
//                        var webVideo = new WebVideo();
//                        webVideo.divPlugin = "divPlugin";
//                        webVideo.init();
//                        stopCameraList();
//                        getCameraList();
                        getMarquee();
                    })
                })
                labels.map(function (item, index) {
                    item.addEventListener('click', function () {
                        var currentSite = siteInfo.filter(function (site) {
                            return site.gpsx == markers[index].point.lng && site.gpsy == markers[index].point.lat
                        })[0]
                        $('#curStop').text(currentSite.name);
                        siteId = currentSite.id;
                        bidId = currentSite.bidId;
                        getTotalSchedulePercentage();
                        getPersonnelEnterExitList();
                        getVehicleEnterAndExitList();
                        getEquipEnterAndExitList();
                        getHiddenDangerInfoList();
                        getPitList();
                        getTunnelList();
                        getRemindCount();
                        getRemindCount_Timely();
//                        var webVideo = new WebVideo();
//                        webVideo.divPlugin = "divPlugin";
//                        webVideo.init();
//                        stopCameraList();
//                        getCameraList();
                        getMarquee();
                    })
                })
            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
        getWeather()
        setInterval(function () {
            getWeather()
        }, 30 * 60 * 1000)
        $('.realtime .clock').text(getTime())
        setInterval(function () {
            $('.realtime .clock').text(getTime())
        }, 1000)
    }
    function getTime() {
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth() + 1 < 10 ? ('0' + (date.getMonth() + 1)) :date.getMonth() + 1;
        var day = date.getDate() < 10 ? ('0' + date.getDate()) :date.getDate();
        var hour = date.getHours() < 10 ? ('0' + date.getHours()) :date.getHours();
        var min = date.getMinutes() < 10 ? ('0' + date.getMinutes()) :date.getMinutes();
        var sec = date.getSeconds() < 10 ? ('0' + date.getSeconds()) :date.getSeconds();
        return year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec
    }
    function getWeather() {
        $.ajax({
            url:"http://wthrcdn.etouch.cn/weather_mini?city=苏州",
            //dataType:'json',
            type:"get",
            success:function(data){
                $('.realtime .tianqi').text(JSON.parse(data).data.forecast[0].type)
                var weather = JSON.parse(data).data.forecast[0].type.split('转')[0]
                var icon = ''
                if (weather.indexOf('雨') > -1) {
                    icon = 'rainy.png'
                }
                if (weather.indexOf('阴') > -1) {
                    icon = 'mayrainy.png'
                }
                if (weather.indexOf('云') > -1) {
                    icon = 'cloudy.png'
                }
                if (weather.indexOf('晴') > -1) {
                    icon = 'sunny.png'
                }
                if (weather.indexOf('雪') > -1) {
                    icon = 'snowy.png'
                }
                if (weather.indexOf('雷') > -1) {
                    icon = 'thunder.png'
                }
                if (weather.indexOf('风') > -1) {
                    icon = 'windy.png'
                }
                $('.weather').html('<img src="${ctx}/assets/img/weather/' + icon + '">')
            }
        })
    }
    function getPersonnelEnterExitList() {
        var divStr = ' <div class="list"><div class="name">{name}</div><div class="time">{enterTime}</div><div class="time">{exitTime}</div><div class="timespan">{stayHours}</div></div>';
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getPersonnelEnterExitList?siteId="+siteId,
            async: false,
            success: function (result) {
                var data = result.personnelEnterExitList;
                $("#personnelEnterExitList").empty();
                for( i = 0; i　< data.length; i++) {
                    var item = data[i];
//                    var exitTime='';
//                    if (item.exitTime)
//                        exitTime = item.exitTime;
                    var enterExit_DIV = divStr.replace('{name}', item.name ? item.name : '').replace('{enterTime}', item.enterTime ? item.enterTime : '').replace('{exitTime}', item.exitTime ? item.exitTime : '').replace('{stayHours}', item.stayHours ? item.stayHours : '');
                    $("#personnelEnterExitList").append(enterExit_DIV);
                }
                $("#personnelBePresentCount").html(result.personnelBePresentCount);


                if (personnelEnterExitList_Timer_Status == '0')
                {
                    personnelEnterExitList_Timer_Status = '1';
                    personnelEnterExitList_Timer = setInterval(getPersonnelEnterExitList,5000); //每5秒刷新一次
                }
            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
    }

    function getVehicleEnterAndExitList() {
        var divStr = ' <div class="list"><div class="name">{plateNumber}</div><div class="time">{enterTime}</div><div class="time">{exitTime}</div><div class="timespan">{stayHours}</div></div>';
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getVehicleEnterAndExitList?siteId="+siteId,
            async: false,
            success: function (result) {
                var data = result.vehicleEnterExitList;
                $("#vehicleEnterExitList").empty();
                for( i = 0; i　< data.length; i++) {
                    var item = data[i];
//                    var exitTime='';
//                    if (item.exitTime)
//                        exitTime = item.exitTime;
                    var enterExit_DIV = divStr.replace('{plateNumber}', item.plateNumber ? item.plateNumber : '').replace('{enterTime}', item.enterTime ? item.enterTime : '').replace('{exitTime}', item.exitTime ? item.exitTime : '').replace('{stayHours}', item.stayHours ? item.stayHours : '');
                    $("#vehicleEnterExitList").append(enterExit_DIV);
                }
                $("#vehicleBePresentCount").html(result.vehicleBePresentCount);
                if(vehicleEnterAndExitList_Timer_Status == '0')
                {
                    vehicleEnterAndExitList_Timer_Status = '1';
                    vehicleEnterAndExitList_Timer = setInterval(getVehicleEnterAndExitList,5000); //每5秒刷新一次

                }
            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
    }

    function getEquipEnterAndExitList() {
        var divStr = ' <div class="list"><div class="name">{equipName}</div><div class="time">{enterTime}</div><div class="time">{exitTime}</div><div class="timespan">{stayHours}</div></div>';
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getEquipEnterAndExitList?siteId="+siteId,
            async: false,
            success: function (result) {
                var data = result.equipEnterExitList;
                $("#equipEnterExitList").empty();
                for( i = 0; i　< data.length; i++) {
                    var item = data[i];
                    var enterExit_DIV = divStr.replace('{equipName}', item.equipName ? item.equipName : '').replace('{enterTime}', item.enterTime ? item.enterTime : '').replace('{exitTime}', item.exitTime ? item.exitTime : '').replace('{stayHours}', item.stayHours ? item.stayHours : '');
                    $("#equipEnterExitList").append(enterExit_DIV);
                }
                if (equipEnterAndExitList_Timer_Status == '0')
                {
                    equipEnterAndExitList_Timer_Status = '1';
                    equipEnterAndExitList_Timer = setInterval(getEquipEnterAndExitList,86400000); //每24小时刷新一次
                }


            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
    }

    function getHiddenDangerInfoList() {
        var divStr = ' <div class="list"><div class="name">{theme}</div><div class="name">{reportPersonName}</div><div class="time">{rectifyBeginTime}</div><div class="timespan2">{rectifyTimeToDay}</div></div>';
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getHiddenDangerInfoList?siteId="+siteId,
            async: false,
            success: function (result) {
                var data = result.hiddenDangerList;
                $("#hiddenDangerList").empty();
                for( i = 0; i　< data.length; i++) {
                    var item = data[i];
                    var hiddenDanger_DIV = divStr.replace('{theme}', item.theme ? item.theme : '').replace('{reportPersonName}', item.reportPersonName ? item.reportPersonName : '').replace('{rectifyBeginTime}', item.rectifyBeginTime ? item.rectifyBeginTime : '').replace('{rectifyTimeToDay}', item.rectifyTimeToDay ? item.rectifyTimeToDay : '');
                    $("#hiddenDangerList").append(hiddenDanger_DIV);
                }
                if (hiddenDangerList_Timer_Status == '0')
                {
                    hiddenDangerList_Timer_Status = '1';
                    hiddenDangerList_Timer = setInterval(getHiddenDangerInfoList,86400000); //每24小时刷新一次
                }


            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
    }

    function getPitList() {
        var divStr = ' <div class="list"><div class="name">{CD}</div><div class="name">{DQAQZT}</div><div class="name">{LJBHZ}</div><div class="name">{BXSL}</div><div class="name">{ZDZ}</div><div class="name">{ZXZ}</div></div>';
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getPitInfoList?siteId="+siteId,
            async: false,
            success: function (result) {
                var data = result.pitList;
                $("#pitList").empty();
                for( i = 0; i　< data.length; i++) {
                    var item = data[i];
                    var pit_DIV = divStr.replace('{CD}', item.CD ? item.CD : '').replace('{DQAQZT}', item.DQAQZT ? item.DQAQZT : '').replace('{LJBHZ}', item.LJBHZ ? item.LJBHZ : '').replace('{BXSL}', item.BXSL ? item.BXSL : '').replace('{ZDZ}', item.ZDZ ? item.ZDZ : '').replace('{ZXZ}', item.ZXZ ? item.ZXZ : '');
                    $("#pitList").append(pit_DIV);
                }
                if (pitList_Timer_Status == '0')
                {
                    pitList_Timer_Status = '1';
                    pitList_Timer = setInterval(getPitList,86400000); //每24小时刷新一次
                }


            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
    }

    function getTunnelList() {
        var divStr = ' <div class="list"><div class="name">{GDMC}</div><div class="name">{DGXH}</div><div class="name">{DGLX}</div><div class="name">{JD}</div></div>';
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getTunnelInfoList?bidId="+bidId,
            async: false,
            success: function (result) {
                var data = result.tunnelList;
                $("#tunnelList").empty();
                if(data.length == 0){
                    var tunnel_DIV_NODATA = '<div style="margin-top:2%;">目前正在实施的盾构0台<br/>已完工0台<br/>准备实施0台</div>';
                    $("#tunnelList").append(tunnel_DIV_NODATA);
                }
                for( i = 0; i　< data.length; i++) {
                    var item = data[i];
                    var tunnel_DIV = divStr.replace('{GDMC}', item.GDMC ? item.GDMC : '').replace('{DGXH}', item.DGXH ? item.DGXH : '').replace('{DGLX}', item.DGLX ? item.DGLX : '').replace('{JD}', item.JD ? item.JD : '');
                    $("#tunnelList").append(tunnel_DIV);
                }
                if (tunnelList_Timer_Status == '0')
                {
                    tunnelList_Timer_Status = '1';
                    tunnelList_Timer = setInterval(getTunnelList,86400000); //每24小时刷新一次
                }


            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
    }

    function getTotalSchedulePercentage() {

        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getTotalSchedulePercentage?siteId="+siteId+"&bidId="+bidId,
            async: false,
            success: function (result) {
                drawMain(canvasPie3, result.envelopePercentage, '#FFCA00', '#E2E5EA')
                drawMain(canvasPie4, result.excavationPercentage, '#8A6EE9', '#E2E5EA')
                drawMain(canvasPie5, result.mainPercentage, '#CAB4FF', '#E2E5EA')
                drawMain(canvasPie1, result.tunnellingPercentage, '#6076FF', '#E2E5EA')
                drawMain(canvasPie2, result.accessorPercentage, '#4A73DC', '#E2E5EA')

                if (totalSchedulePercentage_Timer_Status == '0')
                {
                    totalSchedulePercentage_Timer_Status = '1';
                    totalSchedulePercentage_Timer = setInterval(getTotalSchedulePercentage,86400000); //每24小时刷新一次
                }

            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
    }
    var certificationRemindCount = 0;
    var physicalExaminationRemindCount = 0;
    var equipmentChkRemindCount = 0;
    var equipmentInsureRemindCount = 0;
    var peoplesRemindCount = 0;
    var hiddenDangerCount = 0;
    var maxCount = 0;
    countArr = [0, 0, 0, 0, 0, 0, 0]
    function getRemindCount() {

        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getRemindCount?siteId="+siteId+"&bidId="+bidId,
            async: false,
            success: function (result) {
                certificationRemindCount = result.certificationRemindCount ? result.certificationRemindCount : 0; //10;
                physicalExaminationRemindCount = result.physicalExaminationRemindCount ? result.physicalExaminationRemindCount : 0;//16;
                equipmentChkRemindCount = result.equipmentChkRemindCount ? result.equipmentChkRemindCount : 0;//7;
                equipmentInsureRemindCount = result.equipmentInsureRemindCount ? result.equipmentInsureRemindCount : 0;//5;
                countArr[0] = certificationRemindCount
                countArr[1] = physicalExaminationRemindCount
                countArr[2] = equipmentChkRemindCount
                countArr[3] = equipmentInsureRemindCount
                $("#certificationRemindCount").html(certificationRemindCount);
//                $("#peoplesRemindCount").html(result.peoplesRemindCount);
                $("#physicalExaminationRemindCount").html(physicalExaminationRemindCount);
                $("#equipmentChkRemindCount").html(equipmentChkRemindCount);
                $("#equipmentInsureRemindCount").html(equipmentInsureRemindCount);
                //$("#hiddenDangerCount").html(result.hiddenDangerCount);

                if (remindCount_Timer_Status == '0')
                {
                    remindCount_Timer_Status = '1';
                    remindCount_Timer = setInterval(getRemindCount,86400000); //每24小时刷新一次
                }

            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
    }

    function getRemindCount_Timely() {

        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getRemindCount_Timely?siteId="+siteId+"&bidId="+bidId,
            async: false,
            success: function (result) {
                /*$("#certificationRemindCount").html(result.certificationRemindCount);
                 $("#physicalExaminationRemindCount").html(result.physicalExaminationRemindCount);
                 $("#equipmentChkRemindCount").html(result.equipmentChkRemindCount);
                 $("#equipmentInsureRemindCount").html(result.equipmentInsureRemindCount);*/
                peoplesRemindCount = result.peoplesRemindCount ? result.peoplesRemindCount : 0;//1;
                hiddenDangerCount = result.hiddenDangerCount ? result.hiddenDangerCount : 0;//10;
                countArr[4] = peoplesRemindCount
                countArr[5] = hiddenDangerCount
                for(var i = 0; i < countArr.length; i++) {
                    if (countArr[i] > maxCount) {
                        maxCount = countArr[i]
                    }
                }
                $('.alarmpercent').eq(0).find('.percent').css('width', certificationRemindCount / maxCount * 100 + '%')
                $('.alarmpercent').eq(1).find('.percent').css('width', peoplesRemindCount / maxCount * 100 + '%')
                $('.alarmpercent').eq(2).find('.percent').css('width', physicalExaminationRemindCount / maxCount * 100 + '%')
                $('.alarmpercent').eq(3).find('.percent').css('width', equipmentChkRemindCount / maxCount * 100 + '%')
                $('.alarmpercent').eq(4).find('.percent').css('width', equipmentInsureRemindCount / maxCount * 100 + '%')
                $('.alarmpercent').eq(5).find('.percent').css('width', hiddenDangerCount / maxCount * 100 + '%')
                $("#peoplesRemindCount").html(peoplesRemindCount);
                $("#hiddenDangerCount").html(hiddenDangerCount);

                if (remindCount_Timely_Timer_Status == '0')
                {
                    remindCount_Timely_Timer_Status = '1';
                    remindCount_Timely_Timer = setInterval(getRemindCount_Timely,5000); //每5s刷新一次
                }

            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
    }

    function stopCameraList() {
        for (var i = 0; i < webVideoArray.length; i++) {
            webVideoArray[i].clickStopRealPlay();
            webVideoArray[i].clickLogout();
        }
    }
    function getCameraList() {
        var divStr = ' <div class="list"><div class="name">{equipName}</div><div class="time">{enterTime}</div><div class="time">{exitTime}</div><div class="timespan">{stayHours}</div></div>';
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getCameraList?siteId="+siteId+"&defaultShow=1",
            async: false,
            success: function (result) {
                webVideoArray.length = 0;
                console.log(result)
                var data = result.cameraList;
                for( i = 0; i　< data.length; i++) {
                    var webVideo1 = new WebVideo();
                    webVideoArray.push(webVideo1)
                    var item = data[i];
                    webVideo1.ip = item.ip;
                    webVideo1.username = item.loginUserName;
                    webVideo1.password = item.loginPassword;
                    webVideo1.g_iWndIndex = i;
                    webVideo1.clickLogin();
                }
            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
    }

    function getMarquee() {

        $.ajax({
            contentType: "application/json",
            type: "get",
            url: "<%=request.getContextPath()%>/main/getMarquee",
            async: false,
            success: function (result) {
                $("#marquee").html(result.marquee);
                //if (marquee_Timer.length == 0)
                //    marquee_Timer = setInterval(getMarquee(),300000); //每5分刷新一次
            },
            error: function (request, status, error) {
                //alert(error)
                alert("连接应用服务器失败，网络断开过或应用服务重启过！ 如果一切已经正常，请刷新页面登录！")
            }
        })
    }

</script>
