<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="org.springframework.security.web.WebAttributes"%>
<%@ page
	import="org.springframework.security.authentication.AuthenticationServiceException"%>
<%@ page
	import="org.springframework.security.authentication.BadCredentialsException"%>
<%@ page import="org.springframework.security.Exception.NoLicenseException" %>
<%@ page import="org.springframework.security.Exception.NoLocalMachineCodeException" %>
<%@ page import="org.springframework.security.Exception.ExpiredException" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<title>苏州轨道交通6号线信息化管理平台 - 登陆</title>
	<link type="text/css" href="${ctx}/assets/bootstrap/css/bootstrap.css" rel="Stylesheet">
	<link type="text/css" href="${ctx}/assets/bootstrap-extension/font-awesome/css/font-awesome.min.css" rel="Stylesheet">
	<link type="text/css" href="${ctx}/assets/css/login.css" rel="Stylesheet">
	<script src="${ctx}/assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="${ctx}/assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<link type="text/css" href="${ctx}/assets/css/ielayout.css" rel="Stylesheet">
	<script src="${ctx}/assets/js/respond.min.js" type="text/javascript"></script>
	<script src="${ctx}/assets/js/html5shiv.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>
	<div class="loginMain">
		<%--<div class="logo">--%>
			<%--<img src="${ctx}/assets/img/login/logo.png">--%>
			<%--<span style="font-family: '华文楷体';font-size: 42px;">上海公安技侦总队</span>--%>
		<%--</div>--%>
		<div class="loginpanel">
			<div class="titlepanel">
				<%--<img src="${ctx}/assets/img/login/logo.png">--%>
				<span>苏州轨道交通6号线信息化管理平台</span>
			</div>
			<div class="formpanle">
				<div class="formwrapper">
					<div class="title">
						登录账户
					</div>
					<div class="inputarea">
						<form id="loginForm" action="${ctx}/j_spring_security_check"
							  method="post">
							<%
								Object error = session
										.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
								if (error != null) {
									if (error instanceof BadCredentialsException) {
										BadCredentialsException obj = (BadCredentialsException) error;
							%>
							<script type="text/javascript">
                                var msg = '<%=obj.getMessage()%>';
                                alert("登录失败，登录账号或密码错误！");
							</script>
							<%
									} else if (error instanceof NoLicenseException) {
										NoLicenseException obj = (NoLicenseException) error;
							%>
							<script type="text/javascript">
								var msg = '<%=obj.getMessage()%>';
								alert(msg);
							</script>
							<%
									}else if (error instanceof NoLocalMachineCodeException) {
										NoLocalMachineCodeException obj = (NoLocalMachineCodeException) error;
							%>
							<script type="text/javascript">
								var msg = '<%=obj.getMessage()%>';
								alert(msg);
							</script>
							<%
									}else if (error instanceof ExpiredException) {
										ExpiredException obj = (ExpiredException) error;
							%>
							<script type="text/javascript">
								var msg = '<%=obj.getMessage()%>';
								alert(msg);
							</script>
							<%
									}else if (error instanceof AuthenticationServiceException) {
										AuthenticationServiceException obj = (AuthenticationServiceException) error;

										out.println("内部错误：" + obj.getMessage());
									}

								}
							%>
							<div class="form-group">
								<%--<p>账号</p>--%>
								<i class="fa fa-user"></i>
								<input name="j_username" id="username" placeholder="用户名"
									   class="form-control validate[required]" type="text" />
							</div>
							<div class="form-group">
								<%--<p>密码</p>--%>
								<i class="fa fa-lock"></i>
								<input name="j_password" id="password" placeholder="密码"
									   class="form-control validate[required]"
									   type="password" />
							</div>
							<div class="form-group">
								<button type="button" onclick="checkForm()" class="btn blue"
										name="login_btn">登 录</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		
		$(function() {
//			$("#username").focus();
			<%--if(isIE()){--%>
				<%--$('.invisible_form').append("<div style='text-align: center;margin: 10px 0 0;color: #f00;'>若使用复听功能,请使用<a href='${ctx}/sys/file!downloadTool.ec?download_url=/temp/Chrome40.rar' style='text-decoration: underline;' title='点击下载该浏览器'>Chrome40+版本</a>的浏览器</div>");--%>
			<%--}--%>
		});
		$("#username").keypress(function(e) {
			if (e.keyCode == 13) {
				checkForm();
			}
		});
		$("#password").keypress(function(e) {
			if (e.keyCode == 13) {
				checkForm();
			}
		});
		function checkForm() {
			var username = $("#username").val();
			var password = $("#password").val();
			if (username == null || username == "") {
				alert("请输入用户名");
				$("#username").focus();
				return false;
			}
			if (password == null || password == "") {
				alert("请输入密码");
				$("#password").focus();
				return false;
			}
			$("#loginForm").submit();
		}
	</script>
</body>
</html>
