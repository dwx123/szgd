<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>操作页面</title>
</head>
<body>
<a href="${pageContext.request.contextPath}/workflow/newModel">新建流程模型</a>
<a href="${pageContext.request.contextPath}/workflow/process-list">已部署流程模型</a>
<a href="${pageContext.request.contextPath}/workflow/processinstance/running">所有运行中的流程实例</a>
<a href="${pageContext.request.contextPath}/workflow/model-list">所有流程模型</a>
<a href="${pageContext.request.contextPath}/jfsq/jfsq-list">经费申请列表</a>
<a href="${pageContext.request.contextPath}/jfsq/jfsq-add">经费申请</a>
<a href="${pageContext.request.contextPath}/workflow/getUserTask">经费代办任务</a>
<a href="${pageContext.request.contextPath}/jfsq/getRunningProcess">经费运行中流程实例</a>
<a href="${pageContext.request.contextPath}/jfsq/getFinishedProcess">经费已结束流程实例</a>
<a href="${pageContext.request.contextPath}/jfsq/ndysdr-view">年度预算导入和查询</a>
<a href="${pageContext.request.contextPath}/jfsq/hzsq-list">汇总查询</a>
<a href="${pageContext.request.contextPath}/jfsq/hzsq-start">汇总申请</a>
<a href="${pageContext.request.contextPath}/user/toList">用户管理</a>
<a href="${pageContext.request.contextPath}/dict/index">数据字典管理</a>
<a href="${pageContext.request.contextPath}/sysLog/getAllSysLog">系统日志管理</a>
<a href="${pageContext.request.contextPath}/jFProjectProcess/getProjectLevelTop">经费申请项目管理</a>
<a href="${pageContext.request.contextPath}/jfExecute">经费执行记录管理</a>



</body>
</html>