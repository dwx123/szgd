<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>苏州轨道交通6号线信息化管理平台</title>
    <link type="text/css" href="${ctx}/assets/bootstrap/css/bootstrap.css" rel="Stylesheet">
    <link href="${ctx}/assets/jquery-notific8/jquery.notific8.min.css"
          rel="stylesheet" type="text/css" />
    <link type="text/css" href="${ctx}/assets/bootstrap-extension/font-awesome/css/font-awesome.min.css"
          rel="Stylesheet">
    <link type="text/css" href="${ctx}/assets/css/layout.css" rel="Stylesheet">
    <script src="${ctx}/assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="${ctx}/assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
    <script src="${ctx}/assets/jquery-notific8/jquery.notific8.min.js" type="text/javascript"></script>

    <!--[if lt IE 9]>
    <link type="text/css" href="${ctx}/assets/css/ielayout.css" rel="Stylesheet">
    <script src="${ctx}/assets/js/respond.min.js" type="text/javascript"></script>
    <script src="${ctx}/assets/js/html5shiv.js" type="text/javascript"></script>
    <![endif]-->
    <script src="${ctx}/assets/js/custom.js" type="text/javascript"></script>
</head>
<body style="overflow-y: hidden;">
<header class="main_header">
    <div class="header_top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-5 title">
                    <%--<img src="${ctx}/assets/img/logo.png">--%>
                    <span style="font-size: x-large">苏州轨道交通6号线信息化管理平台</span>
                </div>
                <div class="col-xs-7 user_info pull-right">
                    <ul class="list-inline">

                        <%--<c:if test="${fn:contains(userInfomation.roleIds,'hidden_danger_approver') || fn:contains(userInfomation.roleIds,'hidden_danger_confirmer')}">
                            <li class="notice">
                                <a href="javascript:;" onclick="gotoTaskList(0,'${ctx}//hiddenDanger/toHiddenDangerList')"  title="待处理隐患数量" >
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge badge-green" id="headerTodoBadge" >${taskCount}</span>
                                </a>
                            </li>
                        </c:if>--%>

                        <%--<li>
                            |
                        <</li>
                        <li class="notice">
                            <a href="javascript:;" onclick="gotoTaskList(0,'${ctx}/workflow/getUserTask')">
                                <i class="fa fa-bell-o"></i>
                                <span class="badge badge-green" id="headerNotifactionBadge" style="display: none">0</span>
                            </a>
                        </li>
                        <li>
                            |
                        </li>--%>

                        <li class="dropdown">
                            <a href="javascript:;">
                                <i class="fa fa-user"></i> ${userInfomation.userCnName} <i class="fa fa-chevron-down"></i>
                                <%--<i class="fa fa-user"></i> ${userInfomation.userCnName} (${userInfomation.deptName})<i class="fa fa-chevron-down"></i>--%>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="javascript:;" onclick="changePwdFun()">修改密码</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="logout()">退出</a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="javascript:;">
                                <i class="fa fa-file"></i> 文件下载<i class="fa fa-chevron-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="javascript:;" onclick="downloadUserFile('海康摄像头推荐浏览器.zip')">海康摄像头推荐浏览器</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="downloadUserFile('用户手册.docx')">用户手册</a>
                                </li>
                                <li>
                                    <a href="javascript:;" onclick="downloadUserFile('szgd20191122_v1.1.apk')">安卓APP安装包</a>
                                </li>
                            </ul>
                        </li>

                        <li class="time">
                            <p class="date"></p>
                            <p class="timedata"></p>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header_menu">
        <ul class="list-inline" id="navMenusBanner">
        </ul>

    </div>
</header>

<section>
    <iframe id="mainFrame" name="mainFrame" src="" frameborder="0" width="100%" height="100%"></iframe>
</section>

<div id="_changePwdWin" class="modal fade" data-backdrop="static"></div>
</body>
</html>
<script>
    function getDate() {
        var date = new Date()
        var year = date.getFullYear()
        var month = date.getMonth() < 9 ? ('0' + (date.getMonth() + 1)) : (date.getMonth() + 1)
        var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
        $('.user_info ul li .date').text(year + '/' + month + '/' + day)
    }
    function getTime() {
        var date = new Date()
        var hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
        var min = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
        var sec = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
        $('.user_info ul li .timedata').text(hour + ':' + min + ':' + sec)
    }
    $(function () {
        $('body').on('mouseenter mouseleave','.header_menu > ul > li',function () {
            if($(this).hasClass('show')){
                $(this).removeClass('show').find('.subMenu').hide();
            }else {
                $(this).addClass('show').find('.subMenu').show();
            }
        })
        $('body').on('mouseenter mouseleave', '.subMenu2', function() {
            $(this).find('.subMenu3').toggle()
        })
        getDate()
        getTime()
        setInterval(function () {
            getTime()
        }, 1000)
    })
    var taskTime = null;
    //刷新待办任务数量
    function refreshTodoBadge() {
        /*$.ajax({
            async : false,
            cache : false,
            type : 'POST',
            dataType : "json",
            url : "${ctx}/workflow/getTodoTaskNum",
            error : function() {
                notyError('获取用户待办任务数量失败，请联系系统管理员!');
                if(taskTime !=null){
                    clearInterval(taskTime);
                }
            },
            success : function(data) {
                var total = data.TOTAL;
                if (total == 0) {
                    $("#headerTodoBadge").hide().parent().attr("title","您当前无待办任务！");
                } else {
                    $("#headerTodoBadge").show().html(total).parent().attr("title","您当前有 "+total +" 封待办任务，点击查看详情！");
                }
            }
        });*/
    }

    //获取cookie
    var getCookie = function (name) {
        //获取当前所有cookie
        var strCookies = document.cookie;
        //截取变成cookie数组
        var array = strCookies.split(';');
        //循环每个cookie
        for (var i = 0; i < array.length; i++) {
            //将cookie截取成两部分
            var item = array[i].split("=");
            //判断cookie的name 是否相等
            if (item[0] == name) {
                return item[1];
            }
        }
        return null;
    };

    //跨域无法获取别人的cookie
    var chkCookieFun = function(){
        alert("请先在新的标签页中登录");
        window.open("http://61.155.204.87:8888/login.jsp");
        alert("若已完成登录，请按确认按钮");
        $('iframe').attr('src', $src);
        var $parent = $(e).parent('li');
        $parent.addClass('active').siblings('li').removeClass('active');
//        var cookie_key = "KENTOP_User_Id";
//        var result = getCookie(cookie_key);
//        if(result != null){
//            return;
//        }else{
//            alert("若页面无法显示,请先在新的标签页中登录后再刷新该页面");
//            window.open("http://61.155.204.87:8888/login.jsp");
//        }
    };
    
    function toggleMenu(e) {
        var $parent = $(e).parents('li');
        $(e).parents('li').siblings().removeClass('active')
        $('.subMenu a').removeClass('active')
        $(e).parents('li').addClass('active')
        $(e).addClass('active')
        if ($(e).parents('.subMenu3')) {
            $(e).parents('.subMenu3').siblings('a').addClass('active')
        }
        var $src = $(e).attr('data-src');
        var $name = $(e).attr('data-name');
        if ($src != '/#' && $src != '/szgd/#') {
            var $framesrc = $('iframe').attr('src');
            insertLog($(e).data("id"),$(e).data("name"));
            if ($src == $framesrc) {
//            mainFrame.window.location.reload()
                document.getElementById('mainFrame').contentWindow.location.reload(true)
                return;
            }else {
                if ($name == "基坑监测" || $name == "盾构机") {
//                    chkCookieFun();
                    var step1 = confirm("若未登录,请先在新的标签页中登录;若已登录则点击取消按钮");
                    if(step1){
                        window.open("http://61.155.204.87:8888/login.jsp");
                        var step2 = confirm("若已完成登录，请按确定按钮");
                    }else{

                    }
                    $('iframe').attr('src', $src);
                    var $parent = $(e).parent('li');
                    $parent.addClass('active').siblings('li').removeClass('active');
                }else{
                    $('iframe').attr('src', $src);
                    var $parent = $(e).parent('li');
                    $parent.addClass('active').siblings('li').removeClass('active');
                }
            }
            //下面是系统插入日志
        }



    }
    
    function gotoTaskList(index,src) {
        var $framesrc = $('iframe').attr('src');
        if (src == $framesrc) {
            return;
        }else {
            $('.subMenu a').removeClass('active')
            $('#navMenusBanner li').removeClass('active')
            $('#navMenusBanner li').eq(3).addClass('active').find('.subMenu a').eq(index).addClass('active')
            $('iframe').attr('src', src);
        }
    }


    //点击菜单插入日志
    function insertLog(menuId,menuName) {
        $.ajax({
            type: 'POST',
            data: {
                menuId: menuId,
                menuName: menuName,
                sysFlag:'1'
            },
            dataType: "json",
            url: "${ctx}/log/insertLog",
            error: function () {
                //alert('用户菜单加载失败!');
            },
            success: function (data) {

            }
        });
    }
    //根据用户权限设置系统菜单
    function setUserMenu() {
        $.ajax({
            type: 'POST',
            data: {
                userId: '${userInfomation.userId}'
            },
            dataType: "json",
            url: "${ctx}/right/getAllRoleRightByUserId",
            error: function () {
                alert('用户菜单加载失败!');
            },
            success: function (data) {
                $.each(data, function (index, value) {
                    if(value.RIGHTNAME != '资料上传与下载（APP）'){
                        if (value.PARENTID == 'ROOT' && value.iconSkin == 'icon-menu' && value.checked == 'true') {
                        var html = ''
                        html+="<li id='"+value.RIGHTID+"'><a href='javascript:;'>"+value.RIGHTNAME+"</a>"
                        var childList = value.RIGHTID
                        if(value[childList] != 'null' && typeof (value[childList])!= 'undefined' && value[childList].length > 0) {
                            html += "<div class='subMenu'>"
                            $.each(value[childList], function (index2, value2) {
                                if(value2.iconSkin == 'icon-menu' && value2.checked == 'true') {
                                    var childList2 = value2.RIGHTID
                                    if (value2[childList2] != 'null' && typeof (value2[childList2])!= 'undefined' && value2[childList2].length > 0) {
                                        html += "<div class='subMenu2'>"
                                        html += "<a href=\"javascript:;\" class=\"menuItem\" onclick=\"toggleMenu(this)\" data-id=\""+value2.RIGHTID+"\" data-name=\""+value2.RIGHTNAME+"\" data-src='${ctx}"+value2.RIGHTURL+"'><i class='fa fa-caret-right'></i>"+value2.RIGHTNAME+"</a>"
                                        html += "<div class='subMenu3'>"
                                        $.each(value2[childList2], function (index3, value3) {
                                            html += "<a href=\"javascript:;\" class=\"menuItem2\" onclick=\"toggleMenu(this)\" data-id=\""+value3.RIGHTID+"\" data-name=\""+value3.RIGHTNAME+"\" data-src='${ctx}"+value3.RIGHTURL+"'>"+value3.RIGHTNAME+"</a>"
                                        })
                                        html += "</div></div>"
                                    } else {
                                        html += "<a href=\"javascript:;\" class=\"menuItem\" onclick=\"toggleMenu(this)\"  data-id=\""+value2.RIGHTID+"\" data-name=\""+value2.RIGHTNAME+"\" data-src='${ctx}"+value2.RIGHTURL+"'><i class='fa fa-caret-right'></i>"+value2.RIGHTNAME+"</a>"
                                    }
                                }
                            })
                            html += "</div>"
                        }
                        html += '</li>';
                        $("#navMenusBanner").append(html);
                    }
                    }
                })
                toggleMenu("#navMenusBanner li:nth-child(1) .subMenu > a:nth-child(1)")
                var $width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth
                var lastLiLeft = $("#navMenusBanner li:last-child").offset().left
                if(($width - lastLiLeft)<150){
                    $("#navMenusBanner li:last-child").addClass('lastli')
                }
                //执行任务查询
                refreshTodoBadge();
                //设置定时,30秒一个循环
                //taskTime = setInterval("refreshTodoBadge()", 30 * 1000);
            }
        });

    }
    setUserMenu();


    function changePwdFun() {
        openWin('#_changePwdWin', '${ctx}/user/toChangePwd', null);
    }

    function logout() {
    	location.replace("${ctx}/logout");
    }

    function downloadUserFile(fileName) {
        //var d = encodeURI($(b).val())
        var d = encodeURI(encodeURI(fileName,"utf-8"),"utf-8")
        window.open('<%=request.getContextPath()%>/upload/downloadUserFile?fileName=' + d + '')
    }

</script>
