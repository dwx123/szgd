<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>体检新增</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <script>
        var isHealthAttachChanged = '0';
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $('#physicalDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

        });

        function fillOtherInfo() {
            var personnelId = $("#healthDetail select[name=personnelId]").val();
            if(personnelId != null && personnelId != ""){
                var params = {
                    'id' : personnelId
                };
                $.ajax({
                    url : '${ctx}/personnelInfo/getPersonnelInfoListNoPage',
                    type:"post",
                    contentType : "application/json",
                    data: JSON.stringify(params),
                    success : function (res) {
                        console.log(res);
                        console.log(res.data);
                        var resultCode = res.resultCode;
                        if(resultCode == 1)
                        {
                            $('#ID_NUMBER').val(res.data[0].idNumber);
                            $('#TEL').val(res.data[0].tel);
                            $('#COMPANY').val(res.data[0].companyName);
                            $('#IN_DATE').val(res.data[0].inDate);
                            $('#LAST_PHYSICAL_DATE').val(res.data[0].lastPhysicalDate);
                            $('#SEX').val(res.data[0].sex).trigger("change");
                        }else{
                            notyError("数据请求失败");
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                    }
                });
            }else{
                $('#ID_NUMBER').val("");
                $('#TEL').val("");
                $('#COMPANY').val("");
                $('#IN_DATE').val("");
                $('#LAST_PHYSICAL_DATE').val("");
                $('#SEX').val(null).trigger("change");
            }

        }

        function getHealthAttach() {
            var obj = {
                name:$("#healthAttachName").val(),
                type: "ATTACH_PIC",
                source: "ATTACH_HEALTH",
                isHealthAttachChanged :isHealthAttachChanged
            };
            return obj;
        }

        function addHealthInfo(){
            var valid = $("#healthDetail").validationEngine("validate");
            if (!valid)
                return;
            var param = serializeObject('#healthDetail');
            var otherParam = {};
            otherParam['healthAttach'] = getHealthAttach();
            var jsonString = JSON.stringify($.extend(param, otherParam));

            $.ajax({
                contentType : "application/json",
                url : '${ctx}/health/addHealthInfo',
                type : 'POST',
                data: jsonString,
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        setTimeout(function(){
                            history.go(-1);
                        },1500);

                    }else{
                        notyError(data.resultMsg);

                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error));

                }
            });
        }

        function uploadHealthAttach(e) {
            var reader = new FileReader()
            reader.readAsDataURL(event.target.files[0])
            reader.onloadend = function () {
                var fileId = [];
                var rawId = e.id
                fileId.push(e.id)
                $.ajaxFileUpload({
                    url: "<%=request.getContextPath() %>/upload/uploadFile",
                    type: 'post',
                    secureuri: false,
                    fileElementId: fileId,
                    dataType: 'json',
                    success: function (data, status) {
                        if (data.resultCode == 1) {
                            $("#healthAttachName").val(data.data[0]);
                            isHealthAttachChanged = '1';
                        } else {
                            notyError(data.resultMsg);
                        }
                    },
                    error: function (data, status, e) {
                        console.log(e)
                        notyError(e);
                    }
                });
            }
        }
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/health/toHealthList" title="点击返回至列表">人员体检管理</a>
    <i class="fa fa-angle-double-right"></i>
    体检新增
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="healthDetail">
        <h4><i class="fa fa-bars"></i> 体检人员</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-xs-2 control-label"><span class="alarmstar">*</span>姓名</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="personnelId" id="personnelId" onchange = "fillOtherInfo();">
                        <option value="">请选择</option>
                        <c:forEach var="obj" items="${personList}">
                            <option value="${obj.ID}">${obj.NAME}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">身份证</label>
                <div class="col-md-3 pt7">
                    <input readonly id="ID_NUMBER" name="ID_NUMBER" class="form-control form-control-inline input-sm" size="16" value="" type="text">
                </div>

                <label class="col-md-2 control-label">性别</label>
                <div class="col-md-3 pt7">
                    <select disabled  class="form-control input-sm select2"  id = 'SEX' name="SEX">
                        <option value="">请选择</option>
                        <option value="1">男</option>
                        <option value="0">女</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">电话</label>
                <div class="col-md-3 pt7">
                    <input readonly id="TEL" name="TEL" class="form-control form-control-inline input-sm" size="16" value="" type="text">
                </div>

                <label class="col-md-2 control-label">所属单位</label>
                <div class="col-md-3 pt7">
                    <input readonly id="COMPANY" name="COMPANY" class="form-control form-control-inline input-sm" size="16" value="" type="text">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">上次体检日期</label>
                <div class="col-md-3 pt7">
                    <input readonly id="LAST_PHYSICAL_DATE" name="LAST_PHYSICAL_DATE" class="form-control form-control-inline input-sm" size="16" value="" type="text">
                </div>

                <label class="col-md-2 control-label">入场日期</label>
                <div class="col-md-3 pt7">
                    <input readonly id="IN_DATE" name="IN_DATE" class="form-control form-control-inline input-sm" size="16" value="" type="text">
                </div>
            </div>
        </div>

        <h4><i class="fa fa-bars"></i> 体检信息</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>体检医院</label>
                <div class="col-md-3 pt7">
                    <input id="hospital" name="hospital" class="form-control form-control-inline input-sm validate[required,maxSize[50]]" size="16" value="" type="text">
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>体检日期</label>
                <div class="col-md-3 pt7">
                    <input readonly style="background-color: #fff;" id="physicalDate" name="physicalDate" class="form-control form-control-inline input-sm validate[required]" size="16" value="" type="text">
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">体检结果</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="physicalResult" id="physicalResult">
                        <option value="">请选择</option>
                        <c:forEach var="obj" items="${physicalResultList}" >
                            <option value="${obj.code}">${obj.dictValue}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">体检附件</label>
                <div id="healthAttach" class="col-md-3 pt7">
                    <input class="form-control form-control-inline input-sm" id = "healthAttachName" name="healthAttachName" size="16" value="" type="text" readonly>
                </div>
                <div class="col-md-2 pt7">
                    <button type="button" class="btn-md btn btn-primary attachBtnHalf">上传附件</button>
                    <input class="attachHalf" type="file" id="uploadAttach"  name="file" accept="" onchange="uploadHealthAttach(this)">
                </div>
            </div>
            <div class="row pb5">
                <label class="col-md-2 control-label">备注</label>
                <div class="col-xs-3 pt7">
                    <textarea id="remark" name="remark" class="form-control form-control-inline validate[maxSize[200]]" rows="5"></textarea>
                </div>
            </div>
        </div>
        <div class="form-actions mt15">
            <div class="row">
                <div class="col-xs-offset-5 col-xs-5">
                    <button type="button" id="submitBtn" onclick="addHealthInfo()" class="btn btn-sm btn-success btn-md"><i class="fa fa-save"></i> 保存</button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/health/toHealthList" class="btn btn-md btn-default" title="返回上一页（不保存数据）"><i class="fa fa-reply"></i> 返回</a>
                </div>
            </div>
        </div>
    </form>

</div>
</body>
</html>
