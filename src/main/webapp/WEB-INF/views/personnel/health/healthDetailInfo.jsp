<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>体检详情</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        var isHealthAttachChanged = '0';
        var flag = 0;
        $(function(){
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            var id = $('#ID').val();
            var status = $('#STATUS').val();
            var sex = $("#SEX1").val();
            $("#SEX option[value='"+sex+"']").attr("selected","selected");

            if(status == 1){ //查看
                $('input').attr('disabled','disabled');
                $('select').attr('disabled','disabled');
            }else if(status == 2){ //编辑
                $('#personInfo input').attr('disabled','disabled');
                $('#personInfo select').attr('disabled','disabled');

                $('#uploadAttach').mousemove(function () {
                    if(flag == 0){
                        var attachId = $('#healthAttachId').val();
                        if(attachId != null && attachId != ""){
                            alert("再次上传附件后，会覆盖原来的附件!");
                        }
                    }
                    flag = 1;
                });
            }

            $('#PHYSICAL_DATE').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });
        });

        function getHealthAttach() {
            var obj = {
                id:$("#healthAttachId").val(),
                parentId:$('#ID').val(),
                name:$("#healthAttachName").val(),
                type: "ATTACH_PIC",
                source: "ATTACH_HEALTH",
                isHealthAttachChanged :isHealthAttachChanged
            };
            return obj;
        }
        function save() {

            var valid = $("#healthDetail").validationEngine("validate");
            if (!valid)
                return;
            var param = serializeObject('#healthDetail');
            var otherParam = {};
            otherParam['healthAttach'] = getHealthAttach();
            var jsonString = JSON.stringify($.extend(param, otherParam));
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/health/saveHealthInfo',
                type : 'POST',
                data: jsonString,
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        setTimeout(function(){
                            history.go(-1);
                        },1500);

                    }else{
                        notyError(data.resultMsg);

                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error));

                }
            });
        }
        function uploadHealthAttach(e) {
            var reader = new FileReader()
            reader.readAsDataURL(event.target.files[0])
            reader.onloadend = function () {
                var fileId = [];
                var rawId = e.id
                fileId.push(e.id)
                $.ajaxFileUpload({
                    url: "<%=request.getContextPath() %>/upload/uploadFile",
                    type: 'post',
                    secureuri: false,
                    fileElementId: fileId,
                    dataType: 'json',
                    success: function (data, status) {
                        if (data.resultCode == 1) {
                            $("#healthAttachName").val(data.data[0]);
                            isHealthAttachChanged = '1';
                        } else {
                            notyError(data.resultMsg);
                        }
                    },
                    error: function (data, status, e) {
                        console.log(e)
                        notyError(e);
                    }
                });
            }
        }

        function downloadHealthAttach() {
            var attachId = $("#healthAttachId").val();
            if(attachId==null||attachId==""||attachId=="null"){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+$("#healthAttachPath").val()+'&fileName='+$("#attachName").val();
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/health/toHealthList" title="点击返回至列表">人员体检管理</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${status == 1}">
        人员体检查看
    </c:if>
    <c:if test="${status == 2}">
        人员体检编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form id="healthDetail" class="form-horizontal">
        <input id="ID"  name="id" size="32" value="${id}" type="text" hidden>
        <input id="STATUS" name="status"  size="32" value="${status}" type="text" hidden>
        <input id="personnelId" name="personnelId"  size="32" value="${health.personnelId}" type="text" hidden>
        <h4><i class="fa fa-bars"></i> 人员信息</h4>
        <div class="form-body mb10 bg-blue" id="personInfo">
            <div class="row">
                <label class="col-md-2 control-label">姓名</label>
                <div class="col-md-3 pt7">
                    <input id="name" name="name" class="form-control form-control-inline input-sm" size="16" value="${health.name}" type="text">
                </div>

                <label class="col-md-2 control-label">身份证</label>
                <div class="col-md-3 pt7">
                    <input id="idNumber" name="idNumber" class="form-control form-control-inline input-sm" size="16" value="${health.idNumber}" type="text">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">性别</label>
                <div class="col-md-3 pt7">
                    <input type="hidden" id="SEX1" value="${health.sex}">
                    <select class="form-control input-sm select2" name="sex" id="SEX">
                        <option value=""> 请选择</option>
                        <c:choose>
                            <c:when test="${health.sex == 0}">
                                <option value="0" selected>女</option>
                                <option value="1">男</option>
                            </c:when>
                            <c:otherwise>
                                <option value="0">女</option>
                                <option value="1" selected>男</option>
                            </c:otherwise>
                        </c:choose>
                    </select>
                </div>

                <label class="col-md-2 control-label">电话</label>
                <div class="col-md-3 pt7">
                    <input id="TEL" name="tel" class="form-control form-control-inline input-sm" size="16" value="${health.tel}" type="text">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">岗位</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="workType" id="WORK_TYPE">
                        <option value=""> 请选择</option>
                        <c:forEach var="obj" items="${workTypeList}" >
                            <c:choose>
                                <c:when test="${obj.code eq health.workType}">
                                    <option value="${obj.code}" selected>${obj.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${obj.code}">${obj.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">所属单位</label>
                <div class="col-md-3 pt7">
                    <input id="COMPANY" name="company" class="form-control form-control-inline input-sm" size="16" value="${health.companyName}" type="text">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">入场日期</label>
                <div class="col-md-3 pt7">
                    <input id="IN_DATE" name="inDate" class="form-control form-control-inline input-sm" size="16" value="${health.inDate}" type="text">
                </div>
            </div>
        </div>

        <h4><i class="fa fa-bars"></i> 体检信息</h4>
        <div class="form-body mb10 bg-blue pb5">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>体检医院</label>
                <div class="col-md-3 pt7">
                    <input id="HOSPITAL" name="hospital" class="form-control form-control-inline input-sm validate[required,maxSize[50]]" size="16" value="${health.hospital}" type="text">
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>体检日期</label>
                <div class="col-md-3 pt7">
                <c:if test="${status==1}">
                    <input id="PHYSICAL_DATE" name="physicalDate" class="form-control form-control-inline input-sm validate[required]" size="16" value="${health.physicalDate}" type="text">
                </c:if>
                <c:if test="${status==2}">
                    <input readonly style="background-color: #fff;" id="PHYSICAL_DATE" name="physicalDate" class="form-control form-control-inline input-sm" size="16" value="${health.physicalDate}" type="text">
                </c:if>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">体检结果</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="physicalResult" id="PHYSICAL_RESULT">
                        <option value="">请选择</option>
                        <c:forEach var="obj" items="${physicalResultList}" >
                            <c:choose>
                                <c:when test="${obj.code eq health.physicalResult}">
                                    <option value="${obj.code}" selected>${obj.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${obj.code}">${obj.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">体检附件</label>
                <div id="healthAttach" class="col-md-3 pt7">
                    <input type="hidden" name="healthAttachId" value="${attach.ID}" id="healthAttachId">
                    <input type="hidden" name="healthAttachPath" value="${attach.PATH}" id="healthAttachPath">
                    <input type="hidden" name="attachName" value="${attach.NAME}" id="attachName">

                    <input class="form-control form-control-inline input-sm" id = "healthAttachName" name="healthAttachName" size="16" value="${attach.NAME}" type="text" readonly>
                </div>

                <div class="col-md-2 pt7">
                    <c:if test="${status==1}">
                        <button type="button" id="downloadAttach" onclick="downloadHealthAttach()" class="btn-md btn btn-primary attachBtnHalf">下载附件</button>
                    </c:if>
                    <c:if test="${status==2}">
                        <button type="button" class="btn-md btn btn-primary attachBtnHalf">上传附件</button>
                        <input class="attachHalf" type="file" id="uploadAttach"  name="file" accept="" onchange="uploadHealthAttach(this)">
                        <button type="button" onclick="downloadHealthAttach()" class="btn-md btn btn-primary attachBtnHalf">下载附件</button>
                    </c:if>

                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">备注</label>
                <div class="col-md-3 pt7">
                    <textarea id="REMARK" name="remark" rows="5" class="form-control form-control-inline input-sm validate[maxSize[200]]">${health.remark}</textarea>
                </div>
            </div>
        </div>

        <div class="form-actions mt15">
            <div class="row">
                <div class="col-md-offset-5 col-md-5">
                    <c:if test="${status==2}">
                        <button type="button" id="saveBtn" onclick="save()" class="btn btn-md btn-success"><i class="fa fa-save"></i> 保存</button>
                        &nbsp;&nbsp;
                    </c:if>
                    <a href="${ctx}/health/toHealthList" class="btn btn-md btn-default" title="返回上一页（不保存数据）"><i class="fa fa-reply"></i> 返回</a>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>
