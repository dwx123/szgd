<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>人员体检管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var personnelHealthTable ;
        $(function () {
            personnelHealthTable = $("#personnelHealthTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,

                sAjaxSource: path + "/health/getHealthList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
//                    {'mDataProp': 'contractNumber', 'sTitle': '合同编号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'name', 'sTitle': '姓名', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'sex', 'sTitle': '性别', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'hospital', 'sTitle': '体检医院', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'physicalDate', 'sTitle': '体检日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'physicalResult', 'sTitle': '体检结果', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp':null ,'sTitle' : '操作','sWidth' : '10%','sClass' : 'center'}
                ],
                "aoColumnDefs":[
                    {
                        "targets":-1,
                        "bSortable": false,
                        render: function(data, type, row) {
                            var html ='<a class="btn-sm btn-smd btn btn-primary" href="'+path+'/health/healthDetail?id='+row.id+'&status=1">查看</a>&nbsp;&nbsp;'+
                                '<a class="btn-sm btn-smd btn btn-success" href="'+path+'/health/healthDetail?id='+row.id+'&status=2">编辑</a>&nbsp;&nbsp;'+
                                '<a class="btn-sm btn-smd btn btn-danger" onclick="del('+row.id+')">删除</a>';
                            return html;
                        }
                    },
                    {
                        aTargets: [1],
                        mRender: function (data, type, row) {
                            var sex = "";
                            if (data == 0) {
                                sex = '女';
                            }
                            if (data == 1) {
                                sex = '男';
                            }
                            return sex;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#personnelHealthForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });
                }

            });
            $("#findPersonnelHealth").click(function () {
                personnelHealthTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('personnelHealthForm')
            })

            $('#startDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });
        });

        function del(id) {
            if (!confirm("您是否要删除该记录？")) {
                return;
            }
            var params = {
                'id' : id
            };
            $.ajax({
                type:"post",
                cache : false,
                dataType : "json",
                url : path + "/health/deleteHealth",
                data : params,
                contentType : "application/x-www-form-urlencoded; charset=utf-8",
                success : function(data) {//msg为返回的数据，在这里做数据绑定
                    if (data.success == true) {
                        $('#personnelHealthTable').dataTable().fnDraw();
                        notySuccess(data.message);
                    } else {
                        notyError(data.message);
                    }
                },
                error: function () {
                    notyError("删除失败!");
                }
            });
        }

        function checkUploadHealthForm() {
            var fileDir = $("#healthFile").val();
            if ("" == fileDir) {
                notyWarning("选择需要导入的文件！");
                return false;
            }
            var suffix = fileDir.substr(fileDir.lastIndexOf("."));
            if (".xls" != suffix && ".xlsx" != suffix) {
                notyWarning("选择Excel格式的文件导入！");
                return false;
            }
            return true;
        }
        function uploadHealth() {
            if (checkUploadHealthForm()) {
                function resutlMsg(msg) {
                    if (msg =="文件导入失败！"){
                        $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                    }else{
                        if(msg.indexOf("#WEBROOT#") != -1){
                            msg  = msg.replace("#WEBROOT#", path +"/upload");
                        }
                        $("#resultMsg").html("<font color='#006400'>" + msg +"</font>");
                    }
                    $("#file").val("");
                    $("#personnelHealthTable").DataTable().ajax.reload();
                }
                function errorMsg() {
                    $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                }
                $('#healthUploadForm').ajaxSubmit({
                    url: path + '/health/batchUploadHealth',
                    dataType: 'text',
                    success: resutlMsg,
                    error: errorMsg
                });
            }
        }
        function downloadTemplate() {
            var  queryParams = 'templateName=体检信息批量导入模版.xls';
            var url =path + "/upload/downloadTemplate?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>

</head>
<body>

<div class="bg-white item_title" style="padding-right: 0px;">
    人员体检管理
    <button type="button" class="btn btn-md1 btn-primary title-btn" data-toggle="modal" data-target="#importInfo" title="点击批量导入体检信息">导入体检信息</button>

</div>
<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">

        <div class="row form-group">
            <form id="personnelHealthForm">
                <%--<div class="col-xs-3">--%>
                    <%--<div class="form-item wide2">--%>
                        <%--<label>合同编号</label>--%>
                        <%--<input class="form-control" id="contractNumber" name="contractNumber">--%>
                    <%--</div>--%>
                <%--</div>--%>
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>姓名</label>
                        <input class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-xs-5">
                    <div class="form-item wide2">
                        <label>体检日期</label>
                        <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">
                            <input style="background-color: #fff;" class="form-control" id="startDate" name="startDate" type="text" readonly>
                            <span class="input-group-addon"> 至 </span>
                            <input style="background-color: #fff;" class="form-control" id="endDate" name="endDate" type="text" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3 text-right">
                    <a href="javascript:;" class="ml5 btn btn-primary btn-md1" id="findPersonnelHealth">查询</a>
                    <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn">重置</button>
                    <a id="addHealth" href="<%=request.getContextPath() %>/health/toHealthAdd" class="ml5 btn btn-primary btn-md1" >新增</a>
                </div>
            </form>
        </div>
    </div>

</div>
<div class="bg-white pd_item">
    <table id="personnelHealthTable" class="table table-striped table-bordered display nowrap self_table" align="center" style="width:100%">
    </table>
</div>

<!-- 模态框（Modal） -->
<div class="modal fade" id="importInfo" tabindex="-1" role="dialog" aria-labelledby="importInfoLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="importInfoLabel">导入体检信息</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form_area" method="post" enctype="multipart/form-data" id="healthUploadForm">
                        <div class="col-md-8" style="padding-left: 8%;">
                            <input style="padding: 0;" type="file" name="file" id="healthFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        </div>
                        <div class="col-md-4">
                            <button type="button" onclick="uploadHealth()" class="btn btn-md1 btn-primary" title="点击批量导入体检信息">导&nbsp;&nbsp;入</button>
                            <button type="button" onclick="downloadTemplate()" class="ml5 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                        </div>
                        <div class="col-md-5 mt10" style="padding-left: 6%"><span class="label" id="resultMsg"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
</body>
</html>
