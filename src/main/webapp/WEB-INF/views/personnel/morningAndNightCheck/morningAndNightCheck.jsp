<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <title>早晚查记录</title>
    <style>
        #item_workType .select2-container{
            margin-left: 2%;
            margin-top: -0.5%;
            padding-right: 2%;
        }
    </style>
    <script>
        var path = '<%=request.getContextPath() %>';
        var enterExitTable;
        var enterExitRecordTable;
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            var multiple = $('#workType').select2({
                placeholder: '请选择',
                allowClear: true
            });

            var multiple2 = $('#siteId').select2({
                placeholder: '全部',
                allowClear: true
            });

            $('#workType').val("WORK_ENGINEER").trigger('change');

            $('#workType').click(function(event){
                event.preventDefault();
                var res = [];
                $(this).next('select').find('option').each(function(i,ele){
                    res.push($(ele).val()+",");
                });
                $('#workType').val(res).trigger('change');
            });

            <!--初始化控件-->
            $('#currentMonth').datetimepicker({
                format: 'yyyy-mm',
                autoclose: true,
                minView: 3,
                todayHighlight: 1,
                startView: 3,
                language: 'zh-CN'
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
            });

            //进场次数统计
            enterExitTable = $("#enterExitTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'<'pull-left filterInput'>>t<'row-fluid'<'span4'il><'span7'<'pull-right'p>>>",
                bAutoWidth: false,
                "bSort": false,
                sServerMethod: "GET",
                sAjaxSource: path + "/personnelWork/getMNRecord",//获取列表数据url,
                aoColumns: [
                    {'mDataProp': 'ID', 'sTitle': '序号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'NAME', 'sTitle': '姓名', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'WORK_TYPE_NAME', 'sTitle': '岗位', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'PASS_MONTH', 'sTitle': '月份', 'sWidth': '10%', 'sClass': 'center'},
                    // {'mDataProp': 'PASS_INTERVAL', 'sTitle': '时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'DAYS', 'sTitle': '进场天数', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'SITE_NAME', 'sTitle': '所属站点', 'sWidth': '10%', 'sClass': 'center'}
                ],   //渲染某列格式
                aoColumnDefs: [
                    {
                        aTargets: [0],	//指向某一列，0为第一列
                        "bVisible": false
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var name = $('#name_enterExit').val();
                    var siteId = $('#siteId').val();
                    // var queryType = $('#queryType').val();
                    var currentMonth = $('#currentMonth').val();
                    var workType = $('#workType').val();
                    var workTypes = "";
                    if(workType != null && workType != ""){
                        workTypes = workType.join(",");
                    }
                    aoData.push({"name" : "name", "value" : name});
                    aoData.push({"name" : "siteId", "value" : siteId});
                    aoData.push({"name" : "workType", "value" : workTypes});
                    // aoData.push({"name" : "queryType", "value" : queryType});
                    aoData.push({"name" : "currentMonth", "value" : currentMonth});
                }
            });

            //进出记录查询
            enterExitRecordTable = $("#enterExitRecordTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'<'pull-left filterInput'>>t<'row-fluid'<'span4'il><'span7'<'pull-right'p>>>",
                bAutoWidth: false,
                "bSort": false,
                sServerMethod: "GET",
                sAjaxSource: path + "/personnelWork/getEnter_ExitList",//获取列表数据url,
                aoColumns: [
                    {'mDataProp': 'id', 'sTitle': '序号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'name', 'sTitle': '姓名', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'sex', 'sTitle': '性别', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'workTypeName', 'sTitle': '岗位', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'enterTime', 'sTitle': '进入工地时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'exitTime', 'sTitle': '离开工地时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'stayHours', 'sTitle': '停留时长', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'siteName', 'sTitle': '所属站点', 'sWidth': '10%', 'sClass': 'center'}
                ],   //渲染某列格式
                aoColumnDefs: [
                    {
                        aTargets: [0],	//指向某一列，0为第一列
                        "bVisible": false
                    },
                    {
                        aTargets: [2],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var sex = "" ;
                            if (data == 0) {
                                sex = '女';
                            }
                            if (data == 1) {
                                sex = '男';
                            }
                            return sex;

                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var name = $('#name_enterExit').val();
                    var siteId = $('#siteId').val();
                    // var queryType = $('#queryType').val();
                    var currentMonth = $('#currentMonth').val();
                    var workType = $('#workType').val();
                    var workTypes = "";
                    if(workType != null && workType != ""){
                        workTypes = workType.join(",");
                    }
                    aoData.push({"name" : "name", "value" : name});
                    aoData.push({"name" : "siteId", "value" : siteId});
                    aoData.push({"name" : "workType", "value" : workTypes});
                    // aoData.push({"name" : "queryType", "value" : queryType});
                    aoData.push({"name" : "currentMonth", "value" : currentMonth});
                    aoData.push({"name" : "flag", "value" : "mn"});//sql中判断是否需要union进入时间为空的记录
                }
            });

            $('#findEnterExit').click(function () {
                enterExitTable.ajax.reload();
                enterExitRecordTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('enterExitForm');
            })
        });


        function downloadMNRecord() {
            var workType = $('#workType').val();
            var workTypes = "";
            if(workType != null && workType != ""){
                workTypes = workType.join(",");
            }
            $('#workTypeHide').val(workTypes);
            var action = path + "/personnelWork/downloadMNRecord";
            $('#enterExitForm').attr('action',action);
            $('#enterExitForm').submit();
        }

    </script>
</head>
<body>
<div class="bg-white item_title">
    早晚查记录
</div>
<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">
        <form id="enterExitForm" class="form-horizontal">
            <div class="row form-group">
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>姓名</label>
                        <input class="form-control" id="name_enterExit" name="name">
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>所属站点</label>
                        <select class="form-control input-sm select2" name="siteId" id="siteId">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.siteList}" var="item">
                                <c:choose>
                                    <c:when test="${item.id eq siteId}">
                                        <option value="${item.id}" selected>${item.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.id}">${item.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <%--<div class="col-xs-3">--%>
                    <%--<div class="form-item wide2">--%>
                        <%--<label>早/晚</label>--%>
                        <%--<select class="form-control input-sm select2" name="queryType" id="queryType">--%>
                            <%--<option value="">请选择</option>--%>
                            <%--<option value="Morning">早(6:00-9:00)</option>--%>
                            <%--<option value="Night">晚(18:00-24:00)</option>--%>
                        <%--</select>--%>
                    <%--</div>--%>
                <%--</div>--%>

                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>月份</label>
                        <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd">
                            <input readonly style="background-color: #fff;" class="form-control" id="currentMonth" name="currentMonth" type="text">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row ht46 form-group pt7">

                <div class="col-xs-8">
                    <div class="form-item wide2" id="item_workType">
                        <label>岗位</label>
                        <select multiple="multiple" class="form-control input-sm select2" id="workType">
                            <c:forEach items="${sessionScope.WORK_TYPE}" var="item">
                                <option value="${item.code}">${item.dictValue}</option>
                            </c:forEach>
                        </select>
                        <input type="hidden" id="workTypeHide" name="workType">
                    </div>
                </div>

                <div class="col-md-4 text-right">
                    <a id="findEnterExit" href="javascript:;" class="btn btn-primary btn-md1" >查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml10" >重置</button>
                    <c:if test="${from ne 'home'}">
                        <button type="button" onclick="downloadMNRecord()" class="ml15 btn btn-md1 btn-warning" title="导出早晚查记录Excel">导出早晚查记录</button>
                    </c:if>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="bg-white pd_item" style="padding-top: 5px;">
    <h5><i class="fa fa-bars"></i> 进场天数统计表</h5>
    <table id="enterExitTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>

    <h5><i class="fa fa-bars"></i> 进出记录详情表</h5>
    <table id="enterExitRecordTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>
</body>
</html>
