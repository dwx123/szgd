<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>人员信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <style type="text/css">
        /*样式*/
        .userimage{
            position: relative;
            width: 76.5%;
            height: 273px;
            border: 1px solid #ccc;
            margin-left: 10.4%;
            /*width: ;*/
        }
        .userimage .imgwrap{
            padding: 10px;
            text-align: center;
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
        }
        .userimage .imgwrap img{
            height: 100%;
            max-width: 100%;
        }
        .userimage .imgwrap button,.userimage .imgwrap input{
            position: absolute;
            bottom: 10px;
            right: 10px;
        }
        .userimage .imgwrap button{
            font-size: 12px;
            padding: 5px 8px;
        }
        .userimage .imgwrap input{
            width: 66px;
            height: 29px;
            opacity: 0;
            cursor: pointer;
            z-index: 99;
            font-size: 0;
        }
        .form-group{
            height: auto!important;
        }
        .upbtn.btn{
            padding: 5px 8px;
            font-size: 14px;
        }
        .certificate input{
            position: absolute;
            top: 3.5px;
            left: 15px;
            width: 74px;
            height: 32px;
            opacity: 0;
            cursor: pointer;
            z-index: 99;
            font-size: 0;
        }
    </style>
    <script>

        var isHeadPictureChanged = '0';
        var isCertificateAttachChanged = '0';
        var flag = 0;
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            var opt = "${opt}";
            if(opt == "E"){
                $('#name').attr("disabled",true);
                $('#idType').attr("disabled",true);
                $('#idNumber').attr("disabled",true);
            }

            if ($('#headPictureId').val() !== '') {
                $('.imgwrap').append('<img src="<%=request.getContextPath()%>/attach/showAttach?id=${personnelInfo.headPictureId}"/>')
            }

            $('#inDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });



            $('#outDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#idInvalidTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#birthday').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#effectiveBeginDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#effectiveEndDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            var multiple = $('#bidId').select2({
                placeholder: '请选择',
                allowClear: true
            });

            /*$('#bidId').click(function(event){
                event.preventDefault();
                var res = [];
                $(this).next('select').find('option').each(function(i,ele){
                    res.push($(ele).val())
                });
                $(multiple).val(res).trigger('change');
            });*/

            //获取证书信息
            function getCertificate() {
                var obj = {
                    id: $("#certificateId").val(),
                    personnelId: $("#personnelId").val(),

                    number: $("#number").val(),
                    name: $("#certificateName").val(),
                    certificateType: $("#certificateType").val(),
                    issueName: $("#issueName").val(),
                    certificateType: $("#certificateType").val(),
                    isEffective: $("#isEffective").val(),
                    effectiveBeginDate: $("#effectiveBeginDate").val(),
                    effectiveEndDate: $("#effectiveEndDate").val(),

                };

                return obj;
            }

            //获取头像附件
            function getHeadPictureAttach() {
                var obj = {
                    id: $("#headPictureId").val(),
                    parentId: $("#id").val(),
                    name:$("#headPictureFileName").val(),
                    type: "ATTACH_PIC",
                    source: "ATTACH_PERSONNEL_INFO",
                    isHeadPictureChanged :isHeadPictureChanged

                };
                return obj;
            }

            //获取证书附件
            function getCertificateAttach() {
                var obj = {
                    id: $("#certificateAttachId").val(),
                    parentId: $("#certificateId").val(),
                    name:$("#certificateAttachFileName").val(),
                    type: "ATTACH_DOC",
                    source: "ATTACH_CERTIFICATE",
                    isCertificateAttachChanged :isCertificateAttachChanged
                };
                return obj;
            }

            $('#savePersonnelInfo').click(function () {
                var valid = $("#personnelInfoForm").validationEngine("validate");
                if (!valid)
                    return;
                var param = serializeObject('#personnelInfoForm');
                var otherParam = {};
                otherParam['certificate'] = getCertificate();
                otherParam['headPictureAttach'] = getHeadPictureAttach();
                otherParam['certificateAttach'] = getCertificateAttach();
                var jsonString = JSON.stringify($.extend(param, otherParam));
                $.ajax({
                    contentType: "application/json",
                    url : '${ctx}/personnelInfo/savePersonnelInfo',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
//                        alert(data.resultMsg);
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);

                        }else{
                            notyError(data.resultMsg);
//                            disabledAllBtn(false);
                        }

                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error))
                        alert(JSON.stringify(error));
                    }
                });

            });

            $('#uploadCerti').mousemove(function () {
                if(flag == 0){
                    var attachId = $('#certificateAttachId').val();
                    if(attachId != null && attachId != ""){
                        alert("再次上传附件后，会覆盖原来的附件!");
                    }
                }
                flag = 1;
            });
        });

        function inputClickDept(deptCd_ID,deptName_ID) {
            initDeptTree(deptCd_ID,deptName_ID);
            $('#deptModal').modal('show');

        }

        function uploadHeadPictrue(e) {
            var name = $("#name").val();
            var idNumber = $("#idNumber").val();
            if (name.length == 0)
            {
                alert('姓名不能为空');
                return;
            }

            if (idNumber.length == 0)
            {
                alert('证件号不能为空');
                return;
            }

            var reader = new FileReader()
            reader.readAsDataURL(event.target.files[0])
            reader.onloadend = function () {
                var fileId = [];
                var rawId = e.id
                fileId.push(e.id)
                $.ajaxFileUpload({
                    url: "<%=request.getContextPath() %>/upload/uploadFileWithName",
                    type: 'post',
                    secureuri: false,
                    fileElementId: fileId,
                    data : {
                        "name": name,
                        "idNumber":idNumber
                    },
                    dataType: 'json',
                    success: function (data, status) {
                        if (data.resultCode == 1) {
                            $("#headPictureFileName").val(data.data[0]);
                            isHeadPictureChanged = '1';
                            if($('.imgwrap').find('img').length > 0) {
                                $('.imgwrap').find('img').attr('src', reader.result)
                            } else {
                                $('.imgwrap').append('<img src="' + reader.result +'"/>')
                            }
                        } else {
                            notyError(data.resultMsg);
                        }
                        $('.imgwrap input').val('')
                    },
                    error: function (data, status, e) {
                        console.log(e)
                    }
                });
            }
        }

        function uploadCertificateAttach(e) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onloadend = function () {
                var fileId = [];
                var rawId = e.id;
                fileId.push(e.id);
                $.ajaxFileUpload({
                    url: "<%=request.getContextPath() %>/upload/uploadFile",
                    type: 'post',
                    secureuri: false,
                    fileElementId: fileId,
                    dataType: 'json',
                    success: function (data, status) {
                        if (data.resultCode == 1) {
                            $("#certificateAttachFileName").val(data.data[0]);
                            isCertificateAttachChanged = '1';
                        } else {
                            notyError(data.resultMsg);
                        }
                        $('.imgwrap input').val('')
                    },
                    error: function (data, status, e) {
                        console.log(e);
                        notyError(e);
                    }
                });
            }
        }
        function downloadCertificateAttach() {
            var attachId = $('#certificateAttachId').val();
            console.log(attachId);
            if((attachId==null||attachId==""||attachId=="null")){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+$("#certificateAttachPath").val()+'&fileName='+$("#certificateAttachName").val();
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/personnelInfo/toPersonnelInfoList" title="点击返回至列表">人员信息管理</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        人员信息增加
    </c:if>
    <c:if test="${opt == 'E'}">
        人员信息编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="personnelInfoForm">
        <input type="hidden" name="id" value="${personnelInfo.id}" id="id">
        <input type="hidden" name="certificateId" value="${certificate.id}" id="certificateId">
        <input type="hidden" name="personnelId" value="${certificate.personnelId}" id="personnelId">
        <input type="hidden" name="personId" value="${personnelInfo.personId}" id="personId">
        <input type="hidden" name="faceId" value="${personnelInfo.faceId}" id="faceId">

        <input type="hidden" id="opt" name="opt" value="${opt}">
        <h4><i class="fa fa-bars"></i> 基础信息</h4>
        <div class="form-body mb10">
            <div class="col-md-8 bg-blue">
                <div class="row">
                    <label class="col-md-1-6 control-label"><span class="alarmstar">*</span>姓名</label>
                    <div class="col-md-3-6 pt7">
                        <input class="form-control input-sm validate[required,maxSize[50]]"  id = "name" name="name"  placeholder="" value="${personnelInfo.name}" type="text">
                    </div>

                    <label class="col-md-1-6 control-label"><span class="alarmstar">*</span>证件类型</label>
                    <div class="col-md-3-6 pt7">
                        <select class="form-control input-sm select2 validate[required]" name="idType" id="idType">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.ID_TYPE}" var="item">
                                <c:choose>
                                    <c:when test="${item.code eq personnelInfo.idType || item.code eq 'ID_CARD'}">
                                        <option value="${item.code}" selected>${item.dictValue}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.code}">${item.dictValue}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <label class="col-md-1-6 control-label"><span class="alarmstar">*</span>证件号码</label>
                    <div class="col-md-3-6 pt7">
                        <input class="form-control input-sm validate[required,maxSize[50]]"  id = "idNumber"  name="idNumber" placeholder="" value="${personnelInfo.idNumber}" type="text" >
                    </div>

                    <label class="col-md-1-6 control-label">民族</label>
                    <div class="col-md-3-6 pt7">
                        <input class="form-control input-sm validate[maxSize[50]]"  id = "nation" name="nation"  placeholder="" value="${personnelInfo.nation}" type="text">
                    </div>


                </div>

                <div class="row">
                    <label class="col-md-1-6 control-label">性别</label>
                    <div class="col-md-3-6 pt7">
                        <select  class="form-control input-sm select2"  id = 'sex' name="sex">
                            <option value="">请选择</option>
                            <option value="1" <c:if test="${personnelInfo.sex eq 1}">selected</c:if>>男</option>
                            <option value="0" <c:if test="${personnelInfo.sex eq 0}">selected</c:if>>女</option>
                        </select>
                    </div>

                    <label class="col-md-1-6 control-label">出生日期</label>
                    <div class="col-md-3-6 pt7">
                        <div class="input-group-date">
                            <input readonly class="dateStyle"  id = "birthday"  name="birthday" placeholder=""  value="${personnelInfo.birthday}" type="text">
                            <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <label class="col-md-1-6 control-label">证件有效截止期</label>
                    <div class="col-md-3-6 pt7">
                        <div class="input-group-date">
                            <input readonly class="dateStyle"  id = "idInvalidTime"  name="idInvalidTime" placeholder=""  value="${personnelInfo.idInvalidTime}" type="text">
                            <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                    </div>

                    <label class="col-md-1-6 control-label">婚姻状况</label>
                    <div class="col-md-3-6 pt7">
                        <select  class="form-control input-sm select2" id = 'maritalStatus' name="maritalStatus">
                            <option value="">请选择</option>
                            <option value="0" <c:if test="${personnelInfo.maritalStatus eq 0}">selected</c:if>>未婚</option>
                            <option value="1" <c:if test="${personnelInfo.maritalStatus eq 1}">selected</c:if>>已婚</option>
                        </select>
                    </div>

                </div>

                <div class="row">
                    <label class="col-md-1-6 control-label">学历</label>
                    <div class="col-md-3-6 pt7">
                        <select class="form-control input-sm select2" name="education" id="education">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.EDUCATION_LEVEL}" var="item">
                                <c:choose>
                                    <c:when test="${item.code eq personnelInfo.education}">
                                        <option value="${item.code}" selected>${item.dictValue}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.code}">${item.dictValue}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                    <label class="col-md-1-6 control-label">政治面貌</label>
                    <div class="col-md-3-6 pt7">
                        <select class="form-control input-sm select2" name="politicalStatus" id="politicalStatus">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.POLITICAL_STATUS}" var="item">
                                <c:choose>
                                    <c:when test="${item.code eq personnelInfo.politicalStatus}">
                                        <option value="${item.code}" selected>${item.dictValue}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.code}">${item.dictValue}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-1-6 control-label">户籍地址</label>
                    <div class="col-md-3-6 pt7">
                        <input class="form-control input-sm validate[maxSize[200]]"  id = "censusRegisterAddress"  name="censusRegisterAddress" placeholder="" value="${personnelInfo.censusRegisterAddress}" type="text">
                    </div>

                    <label class="col-md-1-6 control-label">本市暂住地址</label>
                    <div class="col-md-3-6 pt7">
                        <input class="form-control input-sm validate[maxSize[100]"  id = "address"  name="address" placeholder="" value="${personnelInfo.address}" type="text">
                    </div>
                </div>
                <div class="row">
                    <label class="col-md-1-6 control-label"><span class="alarmstar">*</span>本地联系电话</label>
                    <div class="col-md-3-6 pt7">
                        <input class="form-control input-sm validate[required,custom[phone]]"  id = "tel"  name="tel" placeholder="" value="${personnelInfo.tel}" type="text">
                    </div>

                    <label class="col-md-1-6 control-label"><span class="alarmstar">*</span>岗位</label>
                    <div class="col-md-3-6 pt7">
                        <select class="form-control input-sm select2 validate[required]" name="workType" id="workType">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.WORK_TYPE}" var="item">
                                <c:choose>
                                    <c:when test="${item.code eq personnelInfo.workType}">
                                        <option value="${item.code}" selected>${item.dictValue}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.code}">${item.dictValue}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>

                </div>
            </div>

            <div class="col-md-4 bg-blue">
                <div class="userimage">
                    <input type="hidden" name="headPictureId" value="${personnelInfo.headPictureId}" id="headPictureId">
                    <input type="hidden" name="headPictureFileName" value="${personnelInfo.fileName}" id="headPictureFileName">
                    <div class="imgwrap">
                        <input type="file" id="headPicture"  name="file" accept=".png,.jpg" onchange="uploadHeadPictrue(this)">
                        <button type="button" class="btn btn-primary">上传头像</button>
                    </div>
                </div>
            </div>

            <div class="col-md-12 bg-blue">

                <div class="row">
                    <label class="col-md-1-5 control-label"><span class="alarmstar">*</span>入场日期</label>
                    <div class="col-md-2-5 pt7">
                        <div class="input-group-date">
                            <input readonly class="dateStyle validate[required,custom[date],past[#outDate]]"  id = "inDate"  name="inDate" placeholder=""  value="${personnelInfo.inDate}" type="text">
                            <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                    </div>

                    <label class="col-md-1-5 control-label">出场日期</label>
                    <div class="col-md-2-5 pt7">
                        <div class="input-group-date">
                            <input readonly class="dateStyle validate[custom[date],future[#inDate]]"  id = "outDate"  name="outDate" placeholder=""  value="${personnelInfo.outDate}" type="text">
                            <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                        </div>
                    </div>

                    <label class="col-md-1-5 control-label"><span class="alarmstar">*</span>所属标段(可多选)</label>
                    <div class="col-md-2-5 pt7">
                        <select multiple="multiple" class="form-control input-sm select2 validate[required]" name="bidId" id="bidId">
                            <c:forEach items="${sessionScope.userBidList}" var="item">
                                <option value="${item.bidId}" <c:if test="${fn:contains(personnelInfo.bidNames,item.bidName) || personnelInfo.bidNames eq item.bidName}">selected</c:if>>${item.bidName}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="row">

                    <label class="col-md-1-5 control-label"><span class="alarmstar">*</span>是否进入工地</label>
                    <div class="col-md-2-5 pt7">
                        <select  class="form-control input-sm select2 validate[required]" id = 'isEnterSite' name="isEnterSite">
                            <option value="">请选择</option>
                            <option value="0" <c:if test="${personnelInfo.isEnterSite eq 0}">selected</c:if>>否</option>
                            <option value="1" <c:if test="${personnelInfo.isEnterSite eq 1}">selected</c:if>>是</option>
                        </select>
                    </div>

                    <label class="col-md-1-5 control-label">是否办理居住证</label>
                    <div class="col-md-2-5 pt7">
                        <select  class="form-control input-sm select2" id = 'isResidence' name="isResidence">
                            <option value="">请选择</option>
                            <option value="0" <c:if test="${personnelInfo.isResidence eq 0}">selected</c:if>>否</option>
                            <option value="1" <c:if test="${personnelInfo.isResidence eq 1}">selected</c:if>>是</option>
                        </select>
                    </div>

                    <label class="col-md-1-5 control-label"><span class="alarmstar">*</span>所属单位</label>
                    <div class="col-md-2-5 pt7">
                        <input type="hidden" class="form-control " id="company" name="company" value="${personnelInfo.company}">
                        <input onclick ="inputClickDept('company','companyName')" value="${personnelInfo.companyName}"  id="companyName" class="form-control input-sm validate[required]">

                    </div>
                </div>
            </div>
        </div>

        <h4 style="margin-top: 26%;"><i class="fa fa-bars"></i> 证书</h4>
        <div class="form-body mb10 bg-blue">

            <div class="row">
                <label class="col-md-1-5 control-label">证书编号</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm validate[maxSize[50]]"  id = "number"  name="number" placeholder="" value="${certificate.number}" type="text" >
                </div>

                <label class="col-md-1-5 control-label">证书名称</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm validate[maxSize[50]]"  id = "certificateName"  name="certificateName" placeholder="" value="${certificate.name}" type="text" >
                </div>


                <label class="col-md-1-5 control-label">证书类型</label>
                <div class="col-md-2-5 pt7">
                    <select class="form-control input-sm select2 validate[]" name="certificateType" id="certificateType">
                        <option value="">请选择</option>
                        <c:forEach items="${sessionScope.CERTIFICATE_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq certificate.certificateType}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

            </div>
            <div class="row">

                <%--<label class="col-md-1 control-label">是否有效</label>
                <div class="col-md-2">
                    <select  class="form-control input-sm" id = 'isEffective' name="isEffective">
                        <option value="1" <c:if test="${certificate.isEffective eq 1}">selected</c:if>>是</option>
                        <option value="0"　<c:if test="${certificate.isEffective eq 0}">selected</c:if>>否</option>

                    </select>
                </div>

                <label class="col-md-1 control-label">开始生效日期</label>
                <div class="col-md-2">
                    <input class="form-control input-sm"  id = "effectiveBeginDate" name="effectiveBeginDate"  placeholder="开始生效日期" value="${certificate.effectiveBeginDate}" type="text">
                </div>--%>

                <label class="col-md-1-5 control-label">发证单位</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm validate[maxSize[100]]" name="issueName" id="issueName" placeholder="" value="${certificate.issueName}" type="text" >
                </div>

                <label class="col-md-1-5 control-label">证书有效期</label>
                <div class="col-md-2-5 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[]"  id = "effectiveEndDate"  name="effectiveEndDate" placeholder=""  value="${certificate.effectiveEndDate}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>


            </div>

            <div class="row">
                <label class="col-md-1-5 control-label">证书文件名</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm validate[maxSize[50]]"  id = "certificateAttachFileName" name="certificateAttachFileName"  placeholder="" value="${certificate.attachName}" type="text" readonly>
                </div>

                <div class="col-md-3 pt7 certificate" style="position: relative;">
                    <input type="hidden" name="certificateAttachId" value="${certificate.attachId}" id="certificateAttachId">
                    <input type="hidden" name="certificateAttachName" value="${certificate.attachName}" id="certificateAttachName">
                    <input type="hidden" name="certificateAttachPath" value="${certificate.path}" id="certificateAttachPath">

                    <button type="button" id="uploadBtn" class="upbtn btn btn-primary">上传证书</button>
                    <input type="file" id="uploadCerti"  name="file" accept="" onchange="uploadCertificateAttach(this)">
                    <c:if test="${opt == 'E'}">
                        <button type="button" id="downloadCerti" onclick="downloadCertificateAttach()" class="upbtn btn btn-primary">下载证书</button>
                    </c:if>
                </div>
            </div>
            <%--<div class="form-group">

                <label class="col-md-1 control-label">证书文件名</label>
                <div class="col-md-2">
                    <input class="form-control input-sm"  id = "certificateAttachFileName" name="certificateAttachFileName"  placeholder="证书文件名" value="${certificate.attachName}" type="text" readonly>
                </div>

                <div class="col-md-2 certificate" style="position: relative;">
                    <input type="hidden" name="certificateAttachId" value="${personnelInfo.attachId}" id="certificateAttachId">
&lt;%&ndash;                    <input type="hidden" name="certificateAttachFileName" value="${personnelInfo.attachName}" id="certificateAttachFileName">&ndash;%&gt;
                    <input type="hidden" name="certificateAttachPath" value="${personnelInfo.path}" id="certificateAttachPath">

                    <button type="button" class="upbtn btn btn-primary">上传证书</button>
                    <input type="file" id="uploadCerti"  name="file" accept="" onchange="uploadCertificateAttach(this)">
                    <button type="button" id="downloadCerti" onclick="downloadCertificateAttach()" class="upbtn btn btn-primary">下载证书</button>
                </div>

                <div class="col-md-1">
                    <div class="col-md-6">

                    </div>
                </div>
            </div>--%>
        </div>


        <div class="form-actions mt20 mb10">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="savePersonnelInfo"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/personnelInfo/toPersonnelInfoList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>
<%@include file="../../choiceDeptModal.jsp"%>

</body>
</html>
