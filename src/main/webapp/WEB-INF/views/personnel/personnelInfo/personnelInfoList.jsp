<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>人员基础信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <%@ include file="/common/include-css.jsp"%>


    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var jsonString = null;
        var personnelInfoTable ;
        var personnelId='';
        var personId='';
        var isEnterSite = '';
        var  faceId = '';
        var name = '';
        var selectSiteId = '';
        var filePath = '';
        var fileName = '';
        $(function () {

            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            personnelInfoTable = $("#personnelInfoTable").DataTable({
                iDisplayLength: 10,
               "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
               iDisplayStart: 0,
                fnDrawCallback : rowSeltListener,
                sAjaxSource: path + "/personnelInfo/getPersonnelInfoList",//获取列表数据url,
                sServerMethod: "GET",
                //锁定行
                scrollY:"450px",
                //锁定列
                scrollX:true,
                scrollCollapse: true,
                fixedColumns: {
                    leftColumns: 2,
                    rightColumns:1
                },
                //table 表头
                aoColumns: [
                    {'mDataProp': 'id', 'sTitle': '序号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'name', 'sTitle': '姓名', 'sWidth': '10%','sClass': 'center'},
                    {'mDataProp': 'sex', 'sTitle': '性别', 'sWidth': '10%','sClass': 'center'},
                    {'mDataProp': 'idTypeName', 'sTitle': '证件类型', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'idNumber', 'sTitle': '证件号码', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'censusRegisterAddress', 'sTitle': '户籍地址', 'sWidth': '10%', 'sClass': 'center'},
                    // {'mDataProp': 'educationName', 'sTitle': '学历', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    {'mDataProp': 'tel', 'sTitle': '电话', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'workTypeName', 'sTitle': '岗位', 'sWidth': '10%', 'sClass': 'center'},
                    // {'mDataProp': 'certificateName', 'sTitle': '证书', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    // {'mDataProp': 'effectiveEndDate', 'sTitle': '证书有效期', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    {'mDataProp': 'companyName', 'sTitle': '所属单位', 'sWidth': '10%', 'sClass': 'center'},
                    // {'mDataProp': 'isResidence', 'sTitle': '是否办理居住证', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    // {'mDataProp': 'inDate', 'sTitle': '入场日期', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    // {'mDataProp': 'outDate', 'sTitle': '出场日期', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    // {'mDataProp': 'lastPhysicalDate', 'sTitle': '上次体检日期', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    // {'mDataProp': 'lastPhysicalResultName', 'sTitle': '上次体检结果', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    // {'mDataProp': 'lastTrainDate', 'sTitle': '上次培训日期', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    // {'mDataProp': 'lastTrainScoreTypeName', 'sTitle': '上次培训结果', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    // {'mDataProp': 'trainSumTime', 'sTitle': '培训总时间(小时)', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    // {'mDataProp': 'isUploadZjj', 'sTitle': '是否上报住建局', 'sWidth': '10%', 'sClass': 'center','bVisible' : false},
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        aTargets: [0],	//指向某一列，0为第一列
                        "bVisible": false
                    },
                    {
                        aTargets: [9],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            /*if (row.personId == null || row.personId == '') {
                             domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="javascript:;" title="注册人员信息" onclick="personnelCreateToOtherSystem(\''+ row.id +'\')">人员注册</a>&nbsp;&nbsp;';
                             }
                             if (row.faceId == null || row.faceId == '') {
                             domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="javascript:;" title="注册照片" onclick="faceCreateToOtherSystem(\''+ row.id + '\',\''+ row.personId +'\')">照片注册</a>&nbsp;&nbsp;';
                             }*/
                            domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="'+ path +'/personnelInfo/toPersonnelInfoView?id='+ row.id +'" title="查看人员信息">查看</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/personnelInfo/toPersonnelInfoForm?opt=E&id='+ row.id +'" title="修改人员信息">编辑</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除人员信息" onclick="delPersonnelInfo(\''+ row.id + '\',\''+ row.certificateId +'\')">删除</a>&nbsp;&nbsp;';
                            if (row.fileName  != null && row.fileName.length > 0)
                                domHtml += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="下载照片" onclick="downloadPic(\''+ row.path + '\',\''+ row.fileName +'\')">下载照片</a>&nbsp;&nbsp;';
                            /*domHtml += '<a href="javascript:;" title="上传人员照片" onclick="exportForm(\''+ row.ID +'\')"><i class="fa fa-upload"></i></a>&nbsp;&nbsp;';*/
                            return domHtml;
                        }
                    },
                    {
                        aTargets: [2],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var sex = "" ;
                            if (data == 0) {
                                sex = '女';
                            }
                            if (data == 1) {
                                sex = '男';
                            }
                            return sex;

                        }
                    },
                    // {   aTargets: [12],	//指向某一列，0为第一列
                    //     mRender: function (data, type, row) {
                    //         var html = "" ;
                    //         if (data == 0) {
                    //             html = '未办';
                    //         }
                    //         if (data == 1) {
                    //             html = '已办';
                    //         }
                    //         return html;
                    //
                    //     }
                    // },
                    // {   aTargets: [20],	//指向某一列，0为第一列
                    //     mRender: function (data, type, row) {
                    //         var html = "" ;
                    //         if (data == 0) {
                    //             html = '否';
                    //         }
                    //         if (data == 1) {
                    //             html = '是';
                    //         }
                    //         return html;
                    //
                    //     }
                    // }
                    /*{   aTargets: [19],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var html = "" ;
                            if (data == null || data == '') {
                                html = '未注册';
                            }else {
                                html = '已注册';
                            }
                            return html;

                        }
                    },
                    {   aTargets: [20],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var html = "" ;
                            if (data == null || data == '') {
                                html = '未注册';
                            }else {
                                html = '已注册';
                            }
                            return html;

                        }
                    },*/
                ],
                fnRowCallback : function(nRow, aData, iDisplayIndex) {
                    $('td:eq(20)', nRow).attr('style', 'text-align: left;');
                },
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#personnelInfoForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val

                        });
                    });

                    var formSelectArray = $("#personnelInfoForm select");
                    $.each(formSelectArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        if (val != '')
                            aoData.push({
                                "name" : name,
                                "value" : val

                            });
                    });
                }


            });
            $("#findPersonnelInfo").click(function () {
                personnelInfoTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('personnelInfoForm');
            })

            getGatherEquips();
            getGatherEquips_zc();
        });

        function delPersonnelInfo(id,certificateId) {

            var r=confirm("确认删除吗？")
            if (r==false){
                return
            }
            var r=confirm("再次确认删除吗？")
            if (r==false){
                return
            }
            var param = {
                id: id,
                certificateId:certificateId
            }
            disabledAllBtn(true);
            $("body").mask("删除人员中，请稍等...");
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/personnelInfo/delPersonnelInfo',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    $("body").unmask();
                    alert(data.resultMsg);
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        personnelInfoTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                    disabledAllBtn(false);

                },
                error: function (request, status, error) {
                    $("body").unmask();
                    notyError(JSON.stringify(error))
                    alert(JSON.stringify(error));
                    disabledAllBtn(false);

                }
            });
        }

        //向人脸识别设备注册人员信息
        function personnelCreateToOtherSystem() {
            var siteName =$("#siteId_zc option:selected").text();//获取当前选择项
            var s = siteName+'的所有人脸识别设备'

            if (personnelId.length==0)
            {
                alert("请先选择要注册人员信息的人员")
                return;
            }
            if (isEnterSite != '1')
            {
                alert("此人员是非进入工地人员，不能注册人员")
                return;
            }

            var ip = $("#ip").val();
            if(ip=='-1')
            {
                ip = '';
            }else if ($("#ip").val().length > 0)
            {
                s = $("#ip").val();
            }
            var r=confirm("确认向"+s+"注册 \""+name+"\" 的人员信息吗？")
            if (r==false){
                return
            }

            var gatherEquipId = $('#ip option:selected').attr('data-id');
            var siteId = $('#siteId_zc').val();
            var param = {
                id: personnelId,
                ip:ip,
                gatherEquipId:gatherEquipId,
                siteId:siteId
            }
            disabledAllBtn(true);
            $("body").mask("人员注册中，请稍等...");
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/personnelInfo/personnelCreateToOtherSystem',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    $("body").unmask();
                    alert(data.resultMsg);
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        personnelInfoTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                    disabledAllBtn(false);

                },
                error: function (request, status, error) {
                    $("body").unmask();
                    notyError(JSON.stringify(error))
                    alert(JSON.stringify(error));
                    disabledAllBtn(false);

                }
            });

            personnelId='';
            personId='';
            isEnterSite = '';
            faceId = '';
            name = '';
            $('#zcryModal').modal('hide');
        }

        //向人脸识别设备删除人员信息
        function personnelDeleteToOtherSystem() {
            var siteName =$("#siteId_zc option:selected").text();//获取当前选择项
            var s = siteName+'的所有人脸识别设备'

            if (personnelId.length==0)
            {
                alert("请先选择要注册人员信息的人员")
                return;
            }
            if (isEnterSite != '1')
            {
                alert("此人员是非进入工地人员，不能注册人员")
                return;
            }

            var ip = $("#ip").val();
            if(ip=='-1')
            {
                ip = '';
            }else if ($("#ip").val().length > 0)
            {
                s = $("#ip").val();
            }
            var r=confirm("确认向"+s+"删除 \""+name+"\" 的人员信息吗？")
            if (r==false){
                return
            }



            if (personId == null || personId.length==0 || personId == 'null')
            {
                alert("此人员尚未注册")
                return;
            }
            var gatherEquipId = $('#ip option:selected').attr('data-id');
            var siteId = $('#siteId_zc').val();
            var param = {
                id: personnelId,
                ip:ip,
                gatherEquipId:gatherEquipId,
                siteId:siteId
            }
            disabledAllBtn(true);
            $("body").mask("正从人脸设备删除此人员注册信息，请稍等...");
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/personnelInfo/personnelDeleteToOtherSystem',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    $("body").unmask();
                    alert(data.resultMsg);
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        personnelInfoTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                    disabledAllBtn(false);

                },
                error: function (request, status, error) {
                    $("body").unmask();
                    notyError(JSON.stringify(error))
                    alert(JSON.stringify(error));
                    disabledAllBtn(false);

                }
            });
            personnelId='';
            personId='';
            isEnterSite = '';
            faceId = '';
            name = '';
            $('#zcryModal').modal('hide');
        }
        //向人脸识别设备注册照片
        function faceCreateToOtherSystem() {
            var siteName =$("#siteId_zc option:selected").text();//获取当前选择项
            var s = siteName+'的所有人脸识别设备'

            if (personnelId.length==0)
            {
                alert("请先选择要注册图片的人员")
                return;
            }
            if (isEnterSite != '1')
            {
                alert("此人员是非进入工地人员，不能注册图片")
                return;
            }

            if (personId == null || personId.length==0 || personId == 'null')
            {
                alert("请先注册人员信息，再注册照片")
                return;
            }

            var ip = $("#ip").val();
            if(ip=='-1')
            {
                ip = '';
            }else if ($("#ip").val().length > 0)
            {
                s = $("#ip").val();
            }
            var r=confirm("确认向"+s+"注册 \""+name+"\" 的图片吗？")
            if (r==false){
                return
            }

            var gatherEquipId = $('#ip option:selected').attr('data-id');
            var siteId = $('#siteId_zc').val();
            var param = {
                id: personnelId,
                personId:personId,
                ip:ip,
                gatherEquipId:gatherEquipId,
                siteId:siteId
            }
            disabledAllBtn(true);
            $("body").mask("照片注册中，请稍等...");
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/personnelInfo/faceCreateToOtherSystem',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    $("body").unmask();
                    alert(data.resultMsg);
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        personnelInfoTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                    disabledAllBtn(false);

                },
                error: function (request, status, error) {
                    $("body").unmask();
                    notyError(JSON.stringify(error))
                    alert(JSON.stringify(error));
                    disabledAllBtn(false);

                }
            });
            personnelId='';
            personId='';
            isEnterSite = '';
            faceId = '';
            name = '';
            $('#zcryModal').modal('hide');
        }

        //批量向人脸识别设备注册信息，同时注册照片
        function batchPersonnelCreateToOtherSystem() {
            var siteName =$("#siteId_zc option:selected").text();//获取当前选择项
            var s = siteName+'的所有人脸识别设备'

            if (personnelId.length==0)
            {
                alert("请先选择要注册的人员")
                return;
            }
            if (isEnterSite != '1')
            {
                alert("此人员是非进入工地人员，不能注册")
                return;
            }

            var ip = $("#ip").val();
            if(ip=='-1')
            {
                ip = '';
            }else if ($("#ip").val().length > 0)
            {
                s = $("#ip").val();
            }
            var r=confirm("确认向"+s+"注册 \""+name+"\" 的人员信息和图片吗？")
            if (r==false){
                return
            }



            var gatherEquipId = $('#ip option:selected').attr('data-id');
            var siteId = $('#siteId_zc').val();
            var param = {
                name: $('#name').val(),
                idNumber:$('#idNumber').val(),
                isEnterSite:1,
                id: personnelId,
                ip:ip,
                gatherEquipId:gatherEquipId,
                siteId:siteId
            }
            disabledAllBtn(true);
            $("body").mask("人员和照片注册中，请稍等...");
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/personnelInfo/batchPersonnelCreateToOtherSystem',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    $("body").unmask();
                    alert(data.resultMsg);
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        personnelInfoTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                    disabledAllBtn(false);


                },
                error: function (request, status, error) {
                    $("body").unmask();
                    notyError(JSON.stringify(error))
                    alert(JSON.stringify(error));
                    disabledAllBtn(false);

                }
            });
            personnelId='';
            personId='';
            isEnterSite = '';
            faceId = '';
            name = '';
            $('#zcryModal').modal('hide');
        }

        //向人脸识别设备更新照片
        function faceUpdateToOtherSystem() {
            var siteName =$("#siteId_zc option:selected").text();//获取当前选择项
            var s = siteName+'的所有人脸识别设备'

            if (personnelId.length==0)
            {
                alert("请先选择要更新图片的人员")
                return;
            }
            if (isEnterSite != '1')
            {
                alert("此人员是非进入工地人员，不能更新图片")
                return;
            }
            if (faceId == null || faceId.length==0 )
            {
                alert("此人员尚未注册图片，不能更新")
                return;
            }

            var ip = $("#ip").val();
            if(ip=='-1')
            {
                ip = '';
            }else if ($("#ip").val().length > 0)
            {
                s = $("#ip").val();
            }

            var r=confirm("确认向"+s+"更新 \""+name+"\" 的图片吗？")
            if (r==false){
                return
            }

            disabledAllBtn(true);
            $("body").mask("照片更新中，请稍等...");
            var gatherEquipId = $('#ip option:selected').attr('data-id');
            var siteId = $('#siteId_zc').val();
            var param = {
                id: personnelId,
                ip:ip,
                gatherEquipId:gatherEquipId,
                faceId:faceId,
                siteId:siteId
            }

            $.ajax({
                contentType: "application/json",
                url : '${ctx}/personnelInfo/faceUpdateToOtherSystem',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    $("body").unmask();
                    alert(data.resultMsg);
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        personnelInfoTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                    disabledAllBtn(false);

                },
                error: function (request, status, error) {
                    $("body").unmask();
                    notyError(JSON.stringify(error))
                    alert(JSON.stringify(error));
                    disabledAllBtn(false);

                }
            });
            personnelId='';
            personId='';
            isEnterSite = '';
            faceId = '';
            name = '';
            $('#zcryModal').modal('hide');
        }

        function checkUploadPersonnelInfoForm() {
            var fileDir = $("#personnelInfoFile").val();
            if ("" == fileDir) {
                notyWarning("选择需要导入的文件！");
                return false;
            }
            var suffix = fileDir.substr(fileDir.lastIndexOf("."));
            if (".xls" != suffix && ".xlsx" != suffix) {
                notyWarning("选择Excel格式的文件导入！");
                return false;
            }
            return true;
        }
        function uploadPersonnelInfo() {
            if (checkUploadPersonnelInfoForm()) {
                $("#impPersonInfo").attr({"disabled":"disabled"});
                function resutlMsg(msg) {
                    if (msg =="文件导入失败！"){
                        $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                    }else{
                        if(msg.indexOf("#WEBROOT#") != -1){
                            msg  = msg.replace("#WEBROOT#", path +"/upload");
                        }
                        $("#resultMsg").html("<font color='#006400'>" + msg +"</font>");
                    }
//                    $("#file").val("");
                    $("#impPersonInfo").removeAttr("disabled");
                    $("#personnelInfoTable").DataTable().ajax.reload();
                }
                function errorMsg() {
                    $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                    $("#impPersonInfo").removeAttr("disabled");
                }
                $('#personnelInfoUploadForm').ajaxSubmit({
                    url: path + '/personnelInfo/batchUploadPersonnelInfo',
                    dataType: 'text',
                    success: resutlMsg,
                    error: errorMsg
                });
            }
        }

        function addSelectListener() {
            //找到列表中的数据行
            var rows = $("#personnelInfoTable tr");
            $.each(rows, function (index, row) {
                if (index > 0) {
                    $(row).click(
                        function () {
                            selectedData = $('#personnelInfoTable').dataTable().fnGetData(this);
                            $("#choicedUserName").html("<strong>" + selectedData.USER_NAME + "</strong> [" + selectedData.LOGIN_ID + "]");
                            $(row).addClass("info");
                            $(row).siblings().removeClass("info");
                        });
                }
            });
        }

        function checkUploadCertificateForm() {
            var fileDir = $("#certificateAttach").val();
            if ("" == fileDir) {
                notyWarning("请选择需要导入的证书照片！");
                return false;
            }
            return true;
        }

        function uploadCertificate() {
            if (checkUploadCertificateForm()) {
                $("#uploadPic").attr({"disabled":"disabled"});
                function resutlMsg(msg) {
                    if (msg =="照片导入失败！"){
                        $("#resultMsg2").html("<span style='color: red;'>" + msg +"</span>");
                    }else{
                        $("#resultMsg2").html("<span style='color: black;'>" + msg +"</span>");
                    }
                    // $("#certificateAttach").val("");
                    $("#uploadPic").removeAttr("disabled");
                    $("#personnelInfoTable").DataTable().ajax.reload();
                }
                function errorMsg() {
                    $("#resultMsg").html("<span style='color: red;'>" + msg +"</span>");
                    $("#uploadPic").removeAttr("disabled");
                }
                $('#certificateForm').ajaxSubmit({
                    url: path + '/personnelInfo/importCertificate',
                    dataType: 'text',
                    success: resutlMsg,
                    error: errorMsg
                });
            }
        }

        function downloadTemplate() {
            var  queryParams = 'templateName=人员信息批量导入模版.xls';
            var url =path + "/upload/downloadTemplate?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }

        function downloadPersonLog() {
            window.open("${ctx}/personnelInfo/downloadPersonLog");
        }

        function downloadPic(paramPath,paramName) {
            var  queryParams = 'path='+paramPath+'&fileName='+paramName;
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);

            filePath = '';
            fileName = '';
        }

        function downloadAttach() {
            var  queryParams = 'path='+filePath+'&fileName='+fileName;
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);

            filePath = '';
            fileName = '';
        }
        function rowSeltListener() {
            var rows = $("#personnelInfoTable tr");
            $.each(rows, function (index, row) {
                if (index > 0) {
                    $(row).click(function () {
                        seltedData = $('#personnelInfoTable').dataTable().fnGetData(this);
                        personnelId = seltedData.id;
                        personId = seltedData.personId;
                        isEnterSite = seltedData.isEnterSite;
                        faceId = seltedData.faceId;
                        name = seltedData.name;

                        filePath = seltedData.path;
                        fileName = seltedData.fileName;

                        /*if (fileName == null)
                            $('#downLoadPic').hide();
                        else
                            $('#downLoadPic').show();*/
                        $('#zcryTitle').html('注册人员和照片（'+name+'）');
                        var tips = '';
                        $.ajax({
                            contentType: "application/json",
                            url : '${ctx}/personnelInfo/getPersonnelFaceIdByPersonnelIdAndSiteId?id='+personnelId,
                            type : 'GET',
                            success : function (data) {
                                var resultCode = data.resultCode;
                                if(resultCode == 1)
                                {

                                    var resultData = data.data;
                                    for (var i = 0; i < resultData.length; i++) {
                                        var personId = resultData[i].personId;
                                        var faceId = resultData[i].faceId;
                                        var siteName = resultData[i].siteName;
                                        if (tips == '')
                                            tips = siteName+'的人脸设备人员登记号：'+personId+'，照片登记号：'+faceId;
                                        else
                                            tips = tips + '；'+siteName+'的人脸设备人员登记号：'+personId+'，照片登记号：'+faceId;
                                    }
                                    if (tips == '')
                                        tips = '未注册到人脸设备';
                                    else
                                        tips = tips.replace(/null/g,'未注册');
                                    $("#faceTips").html(tips);
                                }else{
                                    notyError(data.resultMsg);
                                    $("#faceTips").html("未获取到注册信息");
                                }

                            },
                            error: function (request, status, error) {
                                $("#faceTips").html("未获取到注册信息");
                            }
                        });

                        /*var tips = '人脸设备人员登记号码：';
                        if (personId==null)
                            tips = tips + '未注册';
                        else
                            tips = tips + personId;
                        if (faceId==null)
                            tips = tips +'，照片登记号：'+ '未注册';
                        else
                            tips = tips +'，照片登记号：'+  faceId;*/

                        //$("#faceTips").html(tips);
                    });
                }
            });
        }

        function getGatherEquips() {
            var siteId= $("#siteId").val();
            if (siteId == '')
            {
                var option = "<option value=''>请选择</option>";
                $("#pEquipId").html(option).trigger("change");
                $("#fEquipId").html(option).trigger("change");
                return;
            }
            var param = {'siteId': siteId};
            $("#pEquipId").val(null).trigger("change");
            $("#fEquipId").val(null).trigger("change");
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/gatherEquip/getPeopleGatherEquipListOfSite',
                type : 'POST',
                data: JSON.stringify(param),
                success:function (data) {
                    var count= data.length;
                    var option = option + "<option value='-1'>全部设备</option>";
                    if(count > 0){
                        for(var i = 0; i < count; i++){
                            option += "<option value='"+data[i].id+"'  data-id='"+data[i].id+"'>"+data[i].ip+" ("+data[i].name+")</option>";
                        }
                    }
                    $("#pEquipId").html(option).trigger("change");
                    $("#fEquipId").html(option).trigger("change");
                },
                error:function(e) {
                    alert("系统异常，请稍候重试！");
                }
            });
        }

        function getGatherEquips_zc() {
            var siteId= $("#siteId_zc").val();
            var param = {'siteId': siteId};
            $("#ip").val(null).trigger("change");
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/gatherEquip/getPeopleGatherEquipListOfSite',
                type : 'POST',
                data: JSON.stringify(param),
                success:function (data) {
                    var count= data.length;
                    var option = option + "<option value='-1'>全部人脸设备</option>";
                    if(count > 0){
                        for(var i = 0; i < count; i++){
                            option += "<option value='"+data[i].ip+"'  data-id='"+data[i].id+"'>"+data[i].ip+" ("+data[i].name+")("+data[i].place+")</option>";
                        }
                    }
                    $("#ip").html(option).trigger("change");
                },
                error:function(e) {
                    alert("系统异常，请稍候重试！");
                }
            });


        }

        //向人脸识别设备更新照片
        function showZcryModal() {
            if (personnelId.length == 0) {
                alert("请先选择要注册的人员")
                return;
            }
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/personnelInfo/getPersonnelFaceIdByPersonnelIdAndSiteId?id='+personnelId+'&siteId='+$('#siteId_zc').val(),
                type : 'GET',
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        var resultData = data.data;
                        for (var i = 0; i < resultData.length; i++) {
                            personId = resultData[i].personId;
                            faceId = resultData[i].faceId;
                        }
                        /*alert(resultData);
                        alert(personId);
                        alert(faceId);*/
                    }else{
                        alert('失败');
                    }
                },
                error: function (request, status, error) {
                    alert('错误');
                }
            });
            $('#zcryModal').modal('show');
        }
    </script>

</head>
<body>

<div class="bg-white item_title" style="padding-right: 0px;">
    人员基础信息管理
    <button type="button" onclick="downloadPersonLog()" class="btn btn-md1 btn-warning title-btn" title="导出人员台账Excel">导出人员台账</button>
    <button type="button" class="btn btn-md1 btn-primary title-btn mr10" data-toggle="modal" data-target="#importCertificate" title="批量上传证书照片">上传证书照片</button>
    <button type="button" class="btn btn-md1 btn-primary title-btn mr10" data-toggle="modal" data-target="#importInfo" title="批量导入人员信息">导入人员信息</button>

</div>
<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">
        <%--<div class="row form-group">
            <form class="form_area" method="post" enctype="multipart/form-data" id="personnelInfoUploadForm">
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label class="control-label">请选择文件</label>
                        <input type="file" name="file" id="personnelInfoFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="button" onclick="uploadPersonnelInfo()" class="ml15 btn btn-md1 btn-primary" title="点击批量导入人员信息">导入人员信息</button>
                    <button type="button" onclick="downloadTemplate()" class="ml15 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                    <button type="button" onclick="downloadPersonLog()" class="ml15 btn btn-md1 btn-warning" title="导出人员台账Excel">导出人员台账</button>
                </div>
                <div class="col-xs-4">
                    &nbsp;&nbsp;<span class="label" id="resultMsg"></span>
                </div>
            </form>
        </div>--%>
        <form id="personnelInfoForm">
            <div class="row form-group">
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>姓名</label>
                        <input class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>证件号码</label>
                        <input class="form-control" id="idNumber" name="idNumber">
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>是否进入工地</label>
                        <select  class="form-control input-sm select2 validate[]" id = 'isEnterSite' name="isEnterSite">
                            <option value="">请选择</option>
                            <option value="0">否</option>
                            <option value="1">是</option>
                        </select>
                    </div>

                </div>
            </div>
            <div class="row form-group">
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>选择站点</label>
                        <select class="form-control input-sm select2" name="siteId" id="siteId"  onchange="getGatherEquips()">
                            <option value="">请选择</option>
                            <c:forEach items="${startedSiteList}" var="item">
                                <c:choose>
                                    <c:when test="${item.id eq siteId}">
                                        <option value="${item.id}" selected>${item.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.id}">${item.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <%--<label>是否注册人员</label>
                        <select  class="form-control input-sm select2 validate[]" id = 'isHavePersonId' name="isHavePersonId">
                            <option value="">请选择</option>
                            <option value="0">未注册</option>
                            <option value="1">已注册</option>
                        </select>--%>
                        <label>未注册人员设备</label>
                        <select  class="form-control input-sm select2 validate[]" id = 'pEquipId' name="pEquipId">
                            <option value="">请选择</option>
                            <option value="-1">全部设备</option>
                            <c:forEach items="${peopleGatherEquipList}" var="item">
                                <c:choose>
                                    <c:when test="${item.ip eq ''}">
                                        <option value="${item.id}" data-id="${item.id}" selected>${item.ip} (${item.name})</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.id}"  data-id="${item.id}" >${item.ip} (${item.name})</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>

                </div>

                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <%--<label>是否注册图片</label>
                        <select  class="form-control input-sm select2 validate[]" id = 'isHaveFaceId' name="isHaveFaceId">
                            <option value="">请选择</option>
                            <option value="0">未注册</option>
                            <option value="1">已注册</option>
                        </select>--%>
                        <label>未注册照片设备</label>
                        <select  class="form-control input-sm select2 validate[]" id = 'fEquipId' name="fEquipId">
                            <option value="">请选择</option>
                            <option value="-1">全部设备</option>
                            <c:forEach items="${peopleGatherEquipList}" var="item">
                                <c:choose>
                                    <c:when test="${item.ip eq ''}">
                                        <option value="${item.id}" data-id="${item.id}" selected>${item.ip} (${item.name})</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.id}"  data-id="${item.id}" >${item.ip} (${item.name})</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row form-group">
                <div class="col-xs-12 text-right">

                    <a id="findPersonnelInfo" href="javascript:;" class="ml15 btn btn-primary btn-md1" >查询</a>
                    <a id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</a>
                    <a id="addPersonnelInfo" href="<%=request.getContextPath() %>/personnelInfo/toPersonnelInfoForm?opt=A" class="ml15 btn btn-primary btn-md1" >新增</a>
                    <%--<a id="downLoadPic" style="display:none;" onclick="downloadAttach()" class="ml15 btn btn-warning btn-md1" >下载照片</a>--%>

                </div>
            </div>
            <%--<div class="row form-group">
                <div class="col-xs-6">
                    <div class="form-item wide2">
                        <label>请选择人脸设备</label>
                        <select  class="form-control input-sm select2 validate[]" id = 'ip' name="ip">
                            <option value="-1">所有人脸识别设备</option>
                            <c:forEach items="${peopleGatherEquipList}" var="item">
                                <c:choose>
                                    <c:when test="${item.ip eq ''}">
                                        <option value="${item.ip}" data-id="${item.id}" selected>${item.ip} (${item.name})(${item.place})</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.ip}"  data-id="${item.id}" >${item.ip} (${item.name})(${item.place})</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>

                        </select>
                    </div>
                </div>

                <div class="col-xs-6 text-right">
                    <div class="form-item wide2">
                        <a id="batchCreateToOtherSystem" onclick="batchPersonnelCreateToOtherSystem()" class="ml15 btn btn-primary btn-smd" title="向人脸识别设备注册人员和图片">注册人员和照片</a>
                        <a id="personnelCreateToOtherSystem"  onclick="personnelCreateToOtherSystem()" class="ml15 btn btn-primary btn-smd" title="向人脸识别设备注册人员">注册人员</a>
                        <a id="faceCreateToOtherSystem"  onclick="faceCreateToOtherSystem()" class="ml15 btn btn-primary btn-smd" title="向人脸识别设备注册照片">注册照片</a>
                        <a id="updateToOtherSystem" onclick="faceUpdateToOtherSystem()" class="ml15 btn btn-primary btn-smd" title="向人脸识别设备更新照片">更新照片</a>
                        <a id="personnelDeleteToOtherSystem"  onclick="personnelDeleteToOtherSystem()" class="ml15 btn btn-primary btn-smd" title="向人脸识别设备删除人员和照片">删除注册人员和照片</a>
                    </div>
                </div>
            </div>--%>
        </form>
    </div>
</div>
<div class="bg-white" style="padding:5px 0 0;">
    <button style="float:right;" type="button" class="btn btn-smd btn-success title-btn" data-toggle="modal" onclick="showZcryModal()" title="选择一个人员记录，注册人员和照片">注册人脸设备</button>
    <span id="faceTips" style="font-size: small;color: red;float:right; margin-top: 5px;padding-bottom: 4px">人脸设备人员登记号码：，照片登记号：</span>
    <table id="personnelInfoTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>

<!-- 模态框（Modal） -->
<div class="modal fade" id="importInfo" tabindex="-1" role="dialog" aria-labelledby="importInfoLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="importInfoLabel">导入人员信息</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form_area" method="post" enctype="multipart/form-data" id="personnelInfoUploadForm">
                        <div class="col-md-8" style="padding-left: 8%;">
                            <input style="padding: 0;" type="file" name="file" id="personnelInfoFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        </div>
                        <div class="col-xs-4">
                            <button type="button" onclick="uploadPersonnelInfo()" class="btn btn-md1 btn-primary" title="点击批量导入人员信息" id="impPersonInfo">导&nbsp;&nbsp;入</button>
                            <button type="button" onclick="downloadTemplate()" class="ml5 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                        </div>
                        <div class="col-md-5 mt10" style="padding-left: 5%"><span class="label" id="resultMsg"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<div class="modal fade szgdModal midModal" id="zcryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document" style="width:1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="zcryTitle" class="modal-title">注册人员和照片</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid bg-white pd_item pdt0 table-form wide3 ">
                    <div class="form-body mb10">
                        <div class="form-group">
                            <label class="col-md-1 control-label">选择站点</label>
                            <div class="col-md-4">
                                <select class="form-control input-sm select2" name="siteId" id="siteId_zc"  onchange="getGatherEquips_zc()">
                                    <c:forEach items="${startedSiteList}" var="item">
                                        <c:choose>
                                            <c:when test="${item.id eq siteId}">
                                                <option value="${item.id}" selected>${item.name}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${item.id}">${item.name}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>

                            <label class="col-md-1 control-label">选择人脸设备</label>
                            <div class="col-md-6">
                                <select  class="form-control input-sm select2 validate[]" id = 'ip' name="ip">
                                    <option value="-1">全部人脸设备</option>
                                    <c:forEach items="${peopleGatherEquipList}" var="item">
                                        <c:choose>
                                            <c:when test="${item.ip eq ''}">
                                                <option value="${item.ip}" data-id="${item.id}" selected>${item.ip} (${item.name})(${item.place})</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${item.ip}"  data-id="${item.id}" >${item.ip} (${item.name})(${item.place})</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button id="batchCreateToOtherSystem" onclick="batchPersonnelCreateToOtherSystem()" class="ml15 btn btn-primary btn-md" title="向人脸识别设备注册人员和图片">注册人员和照片</button>
                <button id="personnelCreateToOtherSystem"  onclick="personnelCreateToOtherSystem()" class="ml15 btn btn-primary btn-md" title="向人脸识别设备注册人员">注册人员</button>
                <button id="faceCreateToOtherSystem"  onclick="faceCreateToOtherSystem()" class="ml15 btn btn-primary btn-md" title="向人脸识别设备注册照片">注册照片</button>
                <button id="updateToOtherSystem" onclick="faceUpdateToOtherSystem()" class="ml15 btn btn-primary btn-md" title="向人脸识别设备更新照片">更新照片</button>
                <button id="personnelDeleteToOtherSystem"  onclick="personnelDeleteToOtherSystem()" class="ml15 btn btn-primary btn-md" title="向人脸识别设备删除人员和照片">删除注册人员和照片</button>
                <button style="width: 70px;" class="btn btn-default btn-md" onclick="$('#zcryModal').modal('hide');">取消</button>
            </div>
        </div>
    </div>
</div>

<!-- 批量上传证书照片 -->
<div class="modal fade" id="importCertificate" tabindex="-1" role="dialog" aria-labelledby="importCertificateLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="importCertificateLabel">批量上传证书照片</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form_area" method="post" enctype="multipart/form-data" id="certificateForm">
                        <div class="col-md-9" style="padding-left: 8%;">
                            <input style="padding: 0;" type="file" name="certificateAttach" id="certificateAttach" class="filePrew form-control" multiple/>
                        </div>
                        <div class="col-md-2">
                            <button id="uploadPic" type="button" onclick="uploadCertificate()" class="btn btn-md1 btn-primary" title="点击批量导入证书照片">上&nbsp;&nbsp;传</button>
                        </div>
                        <div class="col-md-11 mt10" style="padding-left: 8%"><span style="color: red;font-size: 12px;" id="resultMsg2">说明：要求照片的文件名和人员的姓名一致。</span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
