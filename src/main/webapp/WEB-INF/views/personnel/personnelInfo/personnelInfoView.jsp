<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>人员信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <style type="text/css">
        /*样式*/
        .userimage{
            position: relative;
            width: 76.5%;
            height: 273px;
            border: 1px solid #ccc;
            margin-left: 10.4%;
            /*width: ;*/
        }
        .userimage .imgwrap{
            padding: 10px;
            text-align: center;
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
        }
        .userimage .imgwrap img{
            height: 100%;
            max-width: 100%;
        }
        .userimage .imgwrap button,.userimage .imgwrap input{
            position: absolute;
            bottom: 10px;
            right: 10px;
        }
        .userimage .imgwrap button{
            font-size: 12px;
            padding: 5px 8px;
        }
        .userimage .imgwrap input{
            width: 66px;
            height: 29px;
            opacity: 0;
            cursor: pointer;
            z-index: 99;
            font-size: 0;
        }
        .form-group{
            height: auto!important;
        }
        .upbtn.btn{
            padding: 5px 8px;
            font-size: 14px;
        }
        .certificate input{
            position: absolute;
            top: 3.5px;
            left: 15px;
            width: 74px;
            height: 32px;
            opacity: 0;
            cursor: pointer;
            z-index: 99;
            font-size: 0;
        }
    </style>
    <script>
        var certificateTable ;
        $(function () {
//            certificateTable = $("#certificateTable").DataTable({
//                iDisplayLength: 5,
//                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
//                iDisplayStart: 0,
//                "bSort": false,
//                sAjaxSource: path + "/personnelInfo/getCertificateList",//获取列表数据url,
//                sServerMethod: "GET",
//                //table 表头
//                aoColumns: [
//                    {'mDataProp': 'number', 'sTitle': '证书编号', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'name', 'sTitle': '证书名称', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'certificateType', 'sTitle': '证书类型', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'issueName', 'sTitle': '发证单位', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'effectiveEndDate', 'sTitle': '证书有效期', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'isEffective', 'sTitle': '是否有效', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': 'attachName', 'sTitle': '证书附件名', 'sWidth': '10%', 'sClass': 'center'},
//                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
//                ],
//                //渲染某列格式
//                aoColumnDefs: [
//                    {
//                        "targets":-1,
//                        "bSortable": false,
//                        mRender: function (data, type, row) {
//                            var domHtml = "";
//                            domHtml += '<a class="btn-sm btn-smd btn btn-info" href="javascript:;" title="下载证书附件" onclick="downloadCertificateAttach(\''+ row.attachName +'\',\''+ row.path +'\')">下载项目附件</a>';
//                            return domHtml;
//                        }
//                    }
//                ],
//                //向后台提交url携带的参数
//                fnServerParams: function (aoData) {
//                    //获得form表单数据并封装在object中
//                    var personnelId = $("#id").val();
//                    aoData.push({
//                        "name" : "personnelId",
//                        "value" : personnelId
//                    });
//                }
//
//            });

            if ($('#headPictureFileName').val()) {
                $('.imgwrap').append('<img src="<%=request.getContextPath()%>/attach/showAttach?id=${personnelInfo.headPictureId}"/>')
            }
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null
            });

            $('#inDate').datetimepicker({
                format: 'yyyy-mm-dd hh:ii:ss',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });


            $('#outDate').datetimepicker({
                format: 'yyyy-mm-dd hh:ii:ss',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#effectiveBeginDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });


        });


//        function downloadCertificateAttach(attachName,attachpath) {
//            if(attachpath=="null"||attachpath==null||attachpath==""){
//                alert("暂无附件");
//                return;
//            }
//            var  queryParams = 'path='+attachpath+'&fileName='+attachName;
//            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
//            window.open(url);
//        }

        function downloadCertificateAttach() {
            var attachPath = $('#certificateAttachPath').val();
            var attachName = $('#certificateAttachFileName').val();
            if(attachPath=="null"||attachPath==null||attachPath==""){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+attachPath+'&fileName='+attachName;
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/personnelInfo/toPersonnelInfoList" title="点击返回至列表">人员信息管理</a>
    <i class="fa fa-angle-double-right"></i>
    人员信息查看
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="personnelInfoForm">
        <input type="hidden" name="id" value="${personnelInfo.id}" id="id">
        <input type="hidden" name="certificateId" value="${certificate.id}" id="certificateId">
        <input type="hidden" name="personnelId" value="${certificate.personnelId}" id="personnelId">

        <input type="hidden" id="opt" name="opt" value="${opt}">
        <h4><i class="fa fa-bars"></i> 基础信息</h4>
        <div class="form-body mb10">
            <div class="col-md-8 bg-blue">
                <div class="row">
                    <label class="col-md-1-6 control-label"><span class="alarmstar">*</span>姓名</label>
                    <div class="col-md-3-6 pt7">
                        <input readonly class="form-control input-sm validate[required,maxSize[50]]"  id = "name" name="name"  placeholder="" value="${personnelInfo.name}" type="text">
                    </div>

                    <label class="col-md-1-6 control-label"><span class="alarmstar">*</span>证件类型</label>
                    <div class="col-md-3-6 pt7">
                        <select disabled class="form-control input-sm select2 validate[required]" name="idType" id="idType">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.ID_TYPE}" var="item">
                                <c:choose>
                                    <c:when test="${item.code eq personnelInfo.idType || item.code eq 'ID_CARD'}">
                                        <option value="${item.code}" selected>${item.dictValue}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.code}">${item.dictValue}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <label class="col-md-1-6 control-label"><span class="alarmstar">*</span>证件号码</label>
                    <div class="col-md-3-6 pt7">
                        <input readonly class="form-control input-sm validate[required,maxSize[50]]"  id = "idNumber"  name="idNumber" placeholder="" value="${personnelInfo.idNumber}" type="text" >
                    </div>

                    <label class="col-md-1-6 control-label">民族</label>
                    <div class="col-md-3-6 pt7">
                        <input readonly class="form-control input-sm validate[maxSize[50]]"  id = "nation" name="nation"  placeholder="" value="${personnelInfo.nation}" type="text">
                    </div>


                </div>

                <div class="row">
                    <label class="col-md-1-6 control-label">性别</label>
                    <div class="col-md-3-6 pt7">
                        <select disabled  class="form-control input-sm select2"  id = 'sex' name="sex">
                            <option value="">请选择</option>
                            <option value="1" <c:if test="${personnelInfo.sex eq 1}">selected</c:if>>男</option>
                            <option value="0" <c:if test="${personnelInfo.sex eq 0}">selected</c:if>>女</option>
                        </select>
                    </div>

                    <label class="col-md-1-6 control-label">出生日期</label>
                    <div class="col-md-3-6 pt7">
                        <input class="form-control input-sm"  id = "birthday" name="birthday"  placeholder="出生日期" value="${personnelInfo.birthday}" type="text" readonly>
                    </div>

                </div>

                <div class="row">
                    <label class="col-md-1-6 control-label">证件有效截止期</label>
                    <div class="col-md-3-6 pt7">
                        <input class="form-control input-sm"  id = "idInvalidTime"  name="idInvalidTime" placeholder="证件有效截止日期" value="${personnelInfo.idInvalidTime}" type="text" readonly>
                    </div>

                    <label class="col-md-1-6 control-label">婚姻状况</label>
                    <div class="col-md-3-6 pt7">
                        <select disabled class="form-control input-sm select2" id = 'maritalStatus' name="maritalStatus">
                            <option value="">请选择</option>
                            <option value="0" <c:if test="${personnelInfo.maritalStatus eq 0}">selected</c:if>>未婚</option>
                            <option value="1" <c:if test="${personnelInfo.maritalStatus eq 1}">selected</c:if>>已婚</option>
                        </select>
                    </div>

                </div>

                <div class="row">
                    <label class="col-md-1-6 control-label">学历</label>
                    <div class="col-md-3-6 pt7">
                        <select disabled class="form-control input-sm select2" name="education" id="education">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.EDUCATION_LEVEL}" var="item">
                                <c:choose>
                                    <c:when test="${item.code eq personnelInfo.education}">
                                        <option value="${item.code}" selected>${item.dictValue}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.code}">${item.dictValue}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                    <label class="col-md-1-6 control-label">政治面貌</label>
                    <div class="col-md-3-6 pt7">
                        <select disabled class="form-control input-sm select2" name="politicalStatus" id="politicalStatus">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.POLITICAL_STATUS}" var="item">
                                <c:choose>
                                    <c:when test="${item.code eq personnelInfo.politicalStatus}">
                                        <option value="${item.code}" selected>${item.dictValue}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.code}">${item.dictValue}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <label class="col-md-1-6 control-label">户籍地址</label>
                    <div class="col-md-3-6 pt7">
                        <input readonly class="form-control input-sm validate[maxSize[200]]"  id = "censusRegisterAddress"  name="censusRegisterAddress" placeholder="" value="${personnelInfo.censusRegisterAddress}" type="text">
                    </div>

                    <label class="col-md-1-6 control-label">本市暂住地址</label>
                    <div class="col-md-3-6 pt7">
                        <input readonly class="form-control input-sm validate[maxSize[100]"  id = "address"  name="address" placeholder="" value="${personnelInfo.address}" type="text">
                    </div>
                </div>
                <div class="row">

                    <label class="col-md-1-6 control-label">本地联系电话</label>
                    <div class="col-md-3-6 pt7">
                        <input readonly class="form-control input-sm validate[custom[phone]]"  id = "tel"  name="tel" placeholder="" value="${personnelInfo.tel}" type="text">
                    </div>

                    <label class="col-md-1-6 control-label">岗位</label>
                    <div class="col-md-3-6 pt7">
                        <select disabled class="form-control input-sm select2" name="workType" id="workType">
                            <option value="">请选择</option>
                            <c:forEach items="${sessionScope.WORK_TYPE}" var="item">
                                <c:choose>
                                    <c:when test="${item.code eq personnelInfo.workType}">
                                        <option value="${item.code}" selected>${item.dictValue}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${item.code}">${item.dictValue}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-4 bg-blue">
                <div class="userimage">
                    <input type="hidden" name="headPictureId" value="${personnelInfo.headPictureId}" id="headPictureId">
                    <input type="hidden" name="headPictureFileName" value="${personnelInfo.fileName}" id="headPictureFileName">
                    <div class="imgwrap">
                    </div>
                </div>
            </div>

            <div class="col-md-12 bg-blue">
                <div class="row">
                    <label class="col-md-1-5 control-label">入场日期</label>
                    <div class="col-md-2-5 pt7">
                        <input class="form-control input-sm"  id = "inDate" name="inDate"  placeholder="入场日期" value="${personnelInfo.inDate}" type="text" readonly>
                    </div>
                    <label class="col-md-1-5 control-label">出场日期</label>
                    <div class="col-md-2-5 pt7">
                        <input class="form-control input-sm"  id = "outDate"  name="outDate" placeholder="出场日期"  value="${personnelInfo.outDate}" type="text" readonly>
                    </div>

                    <label class="col-md-1-5 control-label"><span class="alarmstar">*</span>所属标段</label>
                    <div class="col-md-2-5 pt7">
                        <select disabled multiple="multiple" class="form-control input-sm select2 validate[required]" name="bidId" id="bidId">
                            <c:forEach items="${sessionScope.userBidList}" var="item">
                                <option value="${item.bidId}" <c:if test="${fn:contains(personnelInfo.bidNames,item.bidName) || personnelInfo.bidIds eq item.bidName}">selected</c:if>>${item.bidName}</option>
                            </c:forEach>
                        </select>
                    </div>

                </div>

                <div class="row">

                    <label class="col-md-1-5 control-label"><span class="alarmstar">*</span>是否进入工地</label>
                    <div class="col-md-2-5 pt7">
                        <select disabled class="form-control input-sm select2 validate[required]" id = 'isEnterSite' name="isEnterSite">
                            <option value="">请选择</option>
                            <option value="0" <c:if test="${personnelInfo.isEnterSite eq 0}">selected</c:if>>否</option>
                            <option value="1" <c:if test="${personnelInfo.isEnterSite eq 1}">selected</c:if>>是</option>
                        </select>
                    </div>
                    <label class="col-md-1-5 control-label">是否办理居住证</label>
                    <div class="col-md-2-5 pt7">
                        <select disabled class="form-control input-sm select2" id = 'isResidence' name="isResidence">
                            <option value="">请选择</option>
                            <option value="0" <c:if test="${personnelInfo.isResidence eq 0}">selected</c:if>>否</option>
                            <option value="1" <c:if test="${personnelInfo.isResidence eq 1}">selected</c:if>>是</option>
                        </select>
                    </div>
                    <label class="col-md-1-5 control-label">所属单位</label>
                    <div class="col-md-2-5 pt7">
                        <input type="hidden" class="form-control validate[]" id="company" name="company">
                        <input  value="${personnelInfo.companyName}"  id="companyName" class="form-control input-sm validate[]" readonly>
                    </div>
                </div>
            </div>
        </div>

        <h4 style="margin-top: 26%;"><i class="fa fa-bars"></i> 体检信息</h4>
        <div class="form-body mb10 bg-blue">

            <div class="row">
                <label class="col-md-1-5 control-label">上次体检日期</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm"  id = "lastPhysicalDate" name="lastPhysicalDate"  placeholder="" value="${personnelInfo.lastPhysicalDate}" type="text" disabled>
                </div>


                <label class="col-md-1-5 control-label">上次体检结果</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm"  id = "lastPhysicalResult" name="lastPhysicalResult"  placeholder="" value="${personnelInfo.lastPhysicalResultName}" type="text" readonly>
                </div>

            </div>
        </div>

        <h4><i class="fa fa-bars"></i> 培训信息</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">

                <label class="col-md-1-5 control-label">上次培训日期</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm"  id = "lastTrainDate"  name="lastTrainDate" placeholder=""  value="${personnelInfo.lastTrainDate}" type="text"  disabled>
                </div>


                <%--<label class="col-md-1 control-label">上次培训分数</label>--%>
                <%--<div class="col-md-2">--%>
                    <%--<input class="form-control input-sm"  id = "lastTrainScore"  name="lastTrainScore" placeholder="上次培训分数"  value="${personnelInfo.lastTrainScore}" type="text" readonly>--%>
                <%--</div>--%>

                <label class="col-md-1-5 control-label">上次培训结果</label>
                <div class="col-md-2-5 pt7">
                    <select class="form-control input-sm select2" name="lastTrainScoreType" id="lastTrainScoreType" disabled>
                        <option value="">请选择</option>
                        <c:forEach items="${sessionScope.SCORE_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq personnelInfo.lastTrainScoreType}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-1-5 control-label">培训总时间(小时)</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm"  id = "trainSumTime"  name="trainSumTime" placeholder=""  value="${personnelInfo.trainSumTime}" type="text" readonly>
                </div>

            </div>
        </div>

        <h4><i class="fa fa-bars"></i> 证书</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">

                <label class="col-md-1-5 control-label">证书编号</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm"  id = "number"  name="number" placeholder="" value="${certificate.number}" type="text"  readonly>
                </div>

                <label class="col-md-1-5 control-label">证书名称</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm"  id = "certificateName"  name="certificateName" placeholder="" value="${certificate.name}" type="text" readonly >
                </div>


                <label class="col-md-1-5 control-label">证书类型</label>
                <div class="col-md-2-5 pt7">
                    <select class="form-control input-sm select2" name="certificateType" id="certificateType" disabled>
                        <option value="">请选择</option>
                        <c:forEach items="${sessionScope.CERTIFICATE_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq certificate.certificateType}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row">
                <%--<label class="col-md-1 control-label">是否有效</label>
                <div class="col-md-2">
                    <select  class="form-control input-sm select2" id = 'isEffective' name="isEffective" disabled>
                        <option value=""> 请选择</option>
                        <option value="1" <c:if test="${certificate.isEffective eq 1}">selected</c:if>>是</option>
                        <option value="0"　<c:if test="${certificate.isEffective eq 0}">selected</c:if>>否</option>

                    </select>
                </div--%>
                <label class="col-md-1-5 control-label">发证单位</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm" name="issueName" id="issueName" placeholder="" value="${certificate.issueName}" type="text" readonly>
                </div>

                <label class="col-md-1-5 control-label">证书有效期</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm"  id = "effectiveEndDate"  name="effectiveEndDate" placeholder=""  value="${certificate.effectiveEndDate}" type="text" disabled>
                </div>

            </div>

            <div class="row">
                <label class="col-md-1-5 control-label">证书文件名</label>
                <div class="col-md-2-5 pt7">
                    <input class="form-control input-sm"  id = "certificateAttachFileName" name="certificateAttachFileName"  placeholder="" value="${certificate.attachName}" type="text" readonly>
                </div>

                <div class="col-md-3 certificate pt7" style="position: relative;">
                    <input type="hidden" name="certificateAttachId" value="${certificate.attachId}" id="certificateAttachId">
                    <input type="hidden" name="certificateAttachPath" value="${certificate.path}" id="certificateAttachPath">

                    <button type="button" id="downloadCerti" onclick="downloadCertificateAttach()" class="upbtn btn btn-primary">下载证书</button>
                </div>
            </div>
            <%--<table id="certificateTable" class="table table-striped table-bordered display nowrap self_table" align="center">--%>
            <%--</table>--%>
        </div>

        <div class="form-actions mb10 mt20">
            <div class="row">
                <div class="text-center">
                    <a href="${ctx}/personnelInfo/toPersonnelInfoList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
