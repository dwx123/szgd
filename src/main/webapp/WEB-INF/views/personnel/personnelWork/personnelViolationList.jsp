<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>人员违规记录</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <script>
        var violationTable;
        var selectedViolationData;
            $(function () {
                $.fn.select2.defaults.set("theme", "bootstrap");
                $(".select2").select2({
                    placeholder: "请选择",
                    width: null,
                    allowClear:true
                });

                $('#occurTime').datetimepicker({
                    format: 'yyyy-mm-dd',
                    todayHighlight: 1,
                    minView: "month",
                    startView: 2,
                    autoclose: 1,
                    language: 'zh-CN'
                });

                $('#occurTimeFrom').datetimepicker({
                    format: 'yyyy-mm-dd',
                    todayHighlight: 1,
                    minView: "month",
                    startView: 2,
                    autoclose: 1,
                    language: 'zh-CN'
                });

                $('#occurTimeTo').datetimepicker({
                    format: 'yyyy-mm-dd',
                    todayHighlight: 1,
                    minView: "month",
                    startView: 2,
                    autoclose: 1,
                    language: 'zh-CN'
                });


                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
                });

                violationTable = $("#violationTable").DataTable({
                    iDisplayLength: 10,
                    "sDom": "<'row-fluid'<'pull-left filterInput'>>t<'row-fluid'<'span4'il><'span7'<'pull-right'p>>>",
                    bAutoWidth: false,
                    "bSort": false,
                    //锁定行
                    scrollY:"450px",
                    //锁定列
                    scrollX:true,
                    scrollCollapse: true,
                    fixedColumns: {
                        leftColumns: 1,
                        rightColumns:1
                    },
                    sServerMethod: "GET",
                    fnDrawCallback : addSelectListener,
                    sAjaxSource: path + "/personnelWork/getViolationList",//获取列表数据url,
                    aoColumns: [
                        {'mDataProp': 'id', 'sTitle': '序号', 'sWidth': '10%', 'sClass': 'center'},
                        {'mDataProp': 'name', 'sTitle': '姓名', 'sWidth': '10%', 'sClass': 'center'},
                        {'mDataProp': 'sex', 'sTitle': '性别', 'sWidth': '10%', 'sClass': 'center'},
                        {'mDataProp': 'occurTime', 'sTitle': '发生日期', 'sWidth': '10%', 'sClass': 'center'},
                        {'mDataProp': 'content', 'sTitle': '违规内容', 'sWidth': '10%', 'sClass': 'center'},
                        {'mDataProp': 'recorder', 'sTitle': '记录人', 'sWidth': '10%', 'sClass': 'center'},
                        {'mDataProp': '', 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
                    ],   //渲染某列格式
                    aoColumnDefs: [
                        {
                            aTargets: [0],	//指向某一列，0为第一列
                            "bVisible": false
                        },
                        {
                            aTargets: [2],	//指向某一列，0为第一列
                            mRender: function (data, type, row) {
                                var sex = "";
                                if (data == 0) {
                                    sex = '女';
                                }
                                if (data == 1) {
                                    sex = '男';
                                }
                                return sex;

                            }
                        },
                        {
                            aTargets: [6],	//指向某一列，0为第一列
                            mRender: function (data, type, row,meta) {
                                var domHtml = "";
                                domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="javascript:;" onclick="openViolationFormModal(\'V\','+meta.row+')" title="查看违规记录">查看</a>&nbsp;&nbsp;';
                                domHtml += '<a class="btn-sm btn-smd btn btn-success" href="javascript:;" onclick="openViolationFormModal(\'E\','+meta.row+')" title="编辑违规记录">编辑</a>&nbsp;&nbsp;';
                                domHtml += '<a class="btn-sm btn-smd btn btn-danger"  href="javascript:;" title="删除违规记录" onclick="delViolation(\''+ row.id +'\')">删除</a>';
                                return domHtml;
                            }
                        }
                    ],
                    //向后台提交url携带的参数
                    fnServerParams: function (aoData) {
                        //获得form表单数据并封装在object中
                        var formIptArray = $("#violationForm input");
                        $.each(formIptArray,function(i , obj){
                            var name = $(obj).attr("name");
                            var val = $(obj).val();
                            aoData.push({
                                "name" : name,
                                "value" : val

                            });
                        });
                    }
                });

                $('#findViolation').click(function () {
                    violationTable.ajax.reload();
                });

                $('#reSetBtn1').click(function () {
                    resetSearchForm('violationForm')
                })

            });

        function openViolationFormModal(opt,rowindex) {
            if(opt == 'A')
                initViolationModal('','A','','','','','${sessionScope.userInfomation.userCnName}','');
            /*id,opt,personnelId,projectId,workSiteId,content,recorder*/
            else
            {

                var row =  $('#violationTable').dataTable().fnGetData()[rowindex];
                initViolationModal(row.id,opt,row.personnelId,row.projectId,row.workSiteId,row.content,row.recorder,row.occurTime);
            }

            $('#violationModal').modal('show');

        }
        function addSelectListener() {
            //找到列表中的数据行
            var rows = $("#violationTable tr");
            $.each(rows, function (index, row) {
                if (index > 0) {
                    $(row).click(
                        function () {
                            selectedViolationData = $('#violationTable').dataTable().fnGetData(this);
                            /*$("#choicedUserName").html("<strong>" + selectedViolationData.USER_NAME + "</strong> [" + selectedData.LOGIN_ID + "]");
                            $(row).addClass("info");
                            $(row).siblings().removeClass("info");*/
                        });
                }
            });
        }

        function delViolation(id) {
            var r=confirm("确认删除？")
            if (r==false){
                return
            }
            var param = {
                id: id
            }

            $.ajax({
                contentType: "application/json",
                url : '${ctx}/personnelWork/delViolation',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        violationTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }
    </script>
</head>
<body>
    <div class="bg-white item_title">
        人员违规记录
    </div>
    <div class="bg-white form_search form_table form_table mb10 container-fluid">
        <form id="violationForm" class="form-horizontal">
            <div class="row form-group">
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>姓名</label>
                        <input class="form-control" id="name_violation" name="name">
                    </div>
                </div>
                <div class="col-xs-5">
                    <div class="form-item wide2">
                        <label>发生日期</label>
                        <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">
                            <input readonly style="background-color: #fff;" class="form-control" id="occurTimeFrom" name="occurTimeFrom" type="text">
                            <span class="input-group-addon"> 至 </span>
                            <input readonly style="background-color: #fff;" class="form-control" id="occurTimeTo" name="occurTimeTo" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-xs-3 text-right">
                    <a id="findViolation" href="javascript:;" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-search"></i>&nbsp;&nbsp;--%>查询</a>
                    <button id="reSetBtn1" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                    <a id="addViolation" onclick="openViolationFormModal('A',0)" href="javascript:;" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-plus"></i>&nbsp;&nbsp;--%>新增</a>
                </div>
            </div>
        </form>
    </div>
    <table id="violationTable" class="table table-striped table-bordered display nowrap self_table"
       align="center">
    </table>

    <%@include file="./violationFormModal.jsp"%>

</body>


</html>
