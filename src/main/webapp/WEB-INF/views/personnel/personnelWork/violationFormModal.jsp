<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript">
        $.fn.modal.Constructor.prototype.enforceFocus = function () { };

        function cancel_violation() {
            $('#violationModal').modal('hide');
        }

        var violationId;
        var violationOpt;
        function ok_violation() {

            var param = {};
            param['id'] = $('#violationId').val();
            param['personnelId'] = $('#personnelId').val();
            // param['projectId'] = $('#projectId').val();
            // param['workSiteId'] = $('#workSiteId').val();
            param['content'] = $('#content').val();
            param['recorder'] = $('#recorder').val();
            param['occurTime'] = $('#occurTime').val();
            var otherParam = {};
/*            otherParam['certificate'] = getCertificate();
            otherParam['headPictureAttach'] = getHeadPictureAttach();
            otherParam['certificateAttach'] = getCertificateAttach();*/
            var jsonString = JSON.stringify($.extend(param, otherParam));
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/personnelWork/saveViolation',
                type : 'POST',
                data: jsonString,
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        $('#violationModal').modal('hide');
                        violationTable.ajax.reload();

                    }else{
                        notyError(data.resultMsg);
                        disabledAllBtn(false);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });

        }

        $('#occurTime').datetimepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: 1,
            minView: "month",
            startView: 2,
            autoclose: 1,
            language: 'zh-CN'
        });

        function initViolationModal(id,opt,personnelId,projectId,workSiteId,content,recorder,occurTime) {
            violationId = id;
            violationOpt = opt;
            $('#violationId').val(violationId);
            $('#personnelId').val(personnelId).trigger("change");
            // $('#projectId').val(projectId).trigger("change");
            // $('#workSiteId').val(workSiteId).trigger("change");
            $('#content').val(content);
            $('#recorder').val(recorder);
            $('#occurTime').val(occurTime);

            if(opt == 'A')
                $('#violationTitle').html('违规记录新增');
            else if(opt == 'E')
                $('#violationTitle').html('违规记录编辑');
            else
                $('#violationTitle').html('违规记录查看');

            if (opt == 'V')
            {
                $("#personnelId").attr('disabled','disabled');
                // $('#projectId').attr('disabled','disabled');
                // $('#workSiteId').attr('disabled','disabled');
                $('#content').attr('disabled','disabled');
                $('#recorder').attr('disabled','disabled');
                $("#occurTime").attr('disabled','disabled');
                $("#occurTime").css('background-color','#eee');
                $('#saveBtn').hide();
            }else if (opt == 'E')
            {
                $('#saveBtn').show();
                $("#personnelId").removeAttr("disabled");
                // $('#projectId').removeAttr("disabled");
                // $('#workSiteId').removeAttr("disabled");
                $('#content').removeAttr("disabled");
                $('#recorder').removeAttr("disabled");
                $('#occurTime').removeAttr("disabled");
                $("#occurTime").css('background-color','#fff');
            }else if (opt == 'A'){
                $('#saveBtn').show();
                $("#personnelId").attr("disabled",false);
                $('#content').removeAttr("disabled");
                $('#recorder').removeAttr("disabled");
                $('#occurTime').removeAttr("disabled");
                $("#occurTime").css('background-color','#fff');
            }


        }

    </script>
</head>
<body>

<div class="modal fade szgdModal midModal" id="violationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 id="violationTitle" class="modal-title">违规记录新增</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="violationId" value="" id="violationId">
                <div class="container-fluid bg-white pd_item pdt0 table-form wide3 ">
                    <div class="form-body mb10 bg-blue">
                        <div class="row pl10 pt10">
                            <label class="col-md-1-5 control-label">人员</label>
                            <div class="col-md-4">
                                <select class="form-control input-sm select2" name="personnelId" id="personnelId">
                                    <option value="">请选择</option>
                                    <c:forEach items="${personnelInfoList}" var="item">
                                        <option value="${item.id}">${item.name}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <label class="col-md-1-5 control-label" style="margin-left: 7%;">发生日期</label>
                            <div class="col-md-4">
                                <input readonly style="background-color: #fff;" class="form-control input-sm"  id = "occurTime" name="occurTime"  placeholder="" value="${personnelInfo.occurTime}" type="text">
                            </div>
                        </div>

                        <%--<div class="form-group">--%>
                            <%--<label class="col-md-1 control-label">项目</label>--%>
                            <%--<div class="col-md-5">--%>
                                <%--<select class="form-control input-sm select2" name="projectId" id="projectId">--%>
                                    <%--<option value=""> 请选择</option>--%>
                                    <%--<c:forEach items="${projectList}" var="item">--%>
                                        <%--<option value="${item.id}">${item.name}</option>--%>
                                    <%--</c:forEach>--%>
                                <%--</select>--%>
                            <%--</div>--%>
                            <%--<label class="col-md-1 control-label">工地</label>--%>
                            <%--<div class="col-md-5">--%>
                                <%--<select class="form-control input-sm select2" name="workSiteId" id="workSiteId">--%>
                                    <%--<option value=""> 请选择</option>--%>
                                    <%--<c:forEach items="${workSiteList}" var="item">--%>
                                        <%--<option value="${item.id}">${item.name}</option>--%>
                                    <%--</c:forEach>--%>
                                <%--</select>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                        <div class="row pl10 pt10">
                            <label class="col-md-1-5 control-label">记录人</label>
                            <div class="col-md-4">
                                <input class="form-control input-sm"  id="recorder"  name="recorder" placeholder="" value="${sessionScope.userInfo.loginId}" type="text" >
                            </div>
                        </div>
                        <div class="row pl10 pt10 pb10">
                            <label class="col-md-1-5 control-label">违规内容</label>
                            <div class="col-md-4">
                                <textarea class="form-control" rows="5" id = "content"  name="content" placeholder="" ></textarea>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button id="saveBtn" style="width: 100px;margin-top: 20px;" class="btn btn-success btn-md" onclick="ok_violation()">保存</button>
                <button style="width: 100px;margin-top: 20px;" class="btn btn-danger btn-md" onclick="cancel_violation()">取消</button>
            </div>
        </div>
    </div>
</div>
<%--</div>--%>
</body>
</html>