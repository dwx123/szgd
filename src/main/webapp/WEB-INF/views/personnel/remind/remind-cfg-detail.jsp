<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.szgd.util.BusinessName"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html lang="en">
<head>
	<title>
		<c:if test="${txlx eq 'APTITUDE_REMIND'}"> 人员资质期限提醒配置</c:if>
		<c:if test="${txlx eq 'PEOPLES_REMIND'}"> 特定时间内人数提醒配置</c:if>
		<c:if test="${txlx eq 'PHYSICAL_EXAMINATION_REMIND'}"> 人员体检提醒配置</c:if>
		<c:if test="${txlx eq 'EQUIPMENT_CHECK_REMIND'}"> 设备检测提醒配置</c:if>
		<c:if test="${txlx eq 'EQUIPMENT_INSURANCE_REMIND'}"> 设备保险提醒配置</c:if>
		<c:if test="${txlx eq 'PROJECT_SCHEDULE_REMIND'}"> 项目进度提醒配置</c:if>
		<c:if test="${txlx eq 'PEOPLES_MONTH_REMIND'}"> 人员月次数提醒配置</c:if>
		<c:if test="${txlx eq 'VEHICLE_SUPERVISE_REMIND'}">监理旁站提醒配置</c:if>
	</title>
	<meta charset="utf-8"/>
	<%--<%@ include file="/common/include-dataTable.jsp"%>--%>
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/validationEngine/css/validationEngine.jquery.css">
	<script type="text/javascript"
			src="${ pageContext.request.contextPath }/assets/validationEngine/js/jquery.validationEngine.js" charset="UTF-8"></script>
	<script type="text/javascript"
			src="${ pageContext.request.contextPath }/assets/validationEngine/js/jquery.validationEngine-zh.js" charset="UTF-8"></script>
</head>
<body>

<div class="opend_modal">
	<div class="modal-dialog" style="width: 800px;">
		<div class="modal-content">
			<div class="modal-header">
				<a data-dismiss="modal"><i class="fa fa-close"></i> </a> <b>
				<c:if test="${txlx eq 'APTITUDE_REMIND'}"> 人员资质期限提醒配置</c:if>
			    <c:if test="${txlx eq 'PEOPLES_REMIND'}"> 特定时间内人数提醒配置</c:if>
			    <c:if test="${txlx eq 'PHYSICAL_EXAMINATION_REMIND'}"> 人员体检提醒配置</c:if>
			    <c:if test="${txlx eq 'EQUIPMENT_CHECK_REMIND'}"> 设备检测提醒配置</c:if>
			    <c:if test="${txlx eq 'EQUIPMENT_INSURANCE_REMIND'}"> 设备保险提醒配置</c:if>
			    <c:if test="${txlx eq 'PROJECT_SCHEDULE_REMIND'}"> 项目进度提醒配置</c:if>
				<c:if test="${txlx eq 'PEOPLES_MONTH_REMIND'}"> 人员月次数提醒配置</c:if>
				<c:if test="${txlx eq 'VEHICLE_SUPERVISE_REMIND'}">监理旁站提醒配置</c:if>
				</b>
			</div>
			<div class="modal-body container-fluid">
				<form method="post" id="txpzForm" >
					<input type="hidden" name="id" value="${tjpz.id}"/>
					<input type="hidden" name="createtime" value="${tjpz.createtime}"/>
				    <input type="hidden" name="updatetime" value="${tjpz.updatetime}"/>
					<input type="hidden" name="creator" value="${tjpz.creator}"/>
				    <input type="hidden" name="uploader" value="${tjpz.uploader}"/>
				    <input type="hidden" name="delflag" value="${tjpz.delflag}"/>
				    <input type="hidden" name="remindType" value="${txlx}"/>
				    <input type="hidden" name="modalFlag" value="${modalFlag}"/>
				    <c:if test="${txlx eq 'APTITUDE_REMIND' || txlx eq 'PHYSICAL_EXAMINATION_REMIND' || txlx eq 'EQUIPMENT_CHECK_REMIND' || txlx eq 'EQUIPMENT_INSURANCE_REMIND'}">
						<div class="form-group row">
		                    <label class="col-xs-2 control-label"><span class="required"> * </span>提醒时限：</label>
		                    <div class="col-xs-4">
			                    <select class="form-control input-sm select2 validate[required]" name="beforeDay">
		                            <option value="15" <c:if test="${15 == tjpz.beforeDay }"> selected</c:if>>提前半个月</option>
		                         	<option value="30" <c:if test="${30 == tjpz.beforeDay }"> selected</c:if>>提前一个月</option>
		                         	<option value="60" <c:if test="${60 == tjpz.beforeDay }"> selected</c:if>>提前二个月</option>
		                         	<option value="90" <c:if test="${90 == tjpz.beforeDay }"> selected</c:if>>提前三个月</option>
		                         	<option value="180" <c:if test="${180 == tjpz.beforeDay }"> selected</c:if>>提前半年</option>
		                         	<option value="360" <c:if test="${360 == tjpz.beforeDay }"> selected</c:if>>提前一年</option>

		                        </select>
		                    </div>
		                    <label class="col-xs-2 control-label"><span class="required"> * </span>是否有效：</label>
		                    <div class="col-xs-4">
			                    <select class="form-control input-sm select2 validate[required]" name="effectiveflag">
		                            <option value="<%=BusinessName.OK.getValue()%>" <c:if test="${'1' == tjpz.effectiveflag }"> selected</c:if>>是</option>
		                         	<option value="<%=BusinessName.NG.getValue()%>" <c:if test="${'0' == tjpz.effectiveflag }"> selected</c:if>>否</option>
		                        </select>
		                    </div>
		                </div>
	                </c:if>
	                <c:if test="${txlx eq 'PEOPLES_REMIND'}">
						<div class="form-group row">
							<label class="col-xs-2 control-label"><span class="required"> * </span>站点名称：</label>
							<div class="col-xs-10">
								<select multiple="multiple" class="form-control input-sm select2 validate[required]" id="siteIds">
									<c:forEach items="${gds}" var="gdxx">
										<%--<option value="${gdxx.ID}" <c:if test="${fn:contains(tjpz.siteIds,gdxx.ID) }"> selected</c:if>>${gdxx.NAME}</option>--%>
										<option value="${gdxx.ID}" <c:if test="${fn:contains(tjpz.siteNames,gdxx.NAME) || (tjpz.siteNames eq gdxx.NAME)}">selected</c:if>>${gdxx.NAME}</option>
									</c:forEach>
								</select>
							</div>
							<input type="hidden" name="siteIds" id="siteIdsHide" class="form-control">
						</div>
						<div class="form-group row pt7">
							<label class="col-xs-2 control-label"><span class="required"> * </span>岗位：</label>
							<div class="col-xs-10">
								<select multiple="multiple" class="form-control input-sm select2 validate[required]" id="workType">
									<c:forEach items="${workTypeList}" var="item">
										<option value="${item.code}" <c:if test="${fn:contains(tjpz.workType,item.code) }"> selected</c:if>>${item.dictValue}</option>
									</c:forEach>
								</select>
							</div>
							<input type="hidden" name="workType" id="workTypeHide" class="form-control">
						</div>

						<div class="form-group row" style="padding-top: 2%">
							<label class="col-xs-2 control-label"><span class="required"> * </span>通知角色：</label>
							<div class="col-xs-10">
								<select multiple="multiple" class="form-control input-sm select2 validate[required]" id="roleIds" disabled>
									<c:forEach items="${roleList}" var="item">
										<%--<option value="${item.ROLE_ID}" <c:if test="${fn:contains(tjpz.roleIds,item.ROLE_ID) }"> selected</c:if>>${item.ROLE_NAME}</option>--%>
										<option value="${item.ROLE_ID}" <c:if test="${fn:contains('app_people_remind',item.ROLE_ID) }"> selected</c:if>>${item.ROLE_NAME}</option>
									</c:forEach>
								</select>
							</div>
							<input type="hidden" name="roleIds" id="roleIdsHide" class="form-control">
						</div>

						<div class="form-group row" style="padding-top:4%;height: auto;">
							<label class="col-xs-2 control-label"><span class="required"> * </span>开始时点：</label>
							<div class="col-xs-4">
								<div class="input-group-date">
									<input readonly class="dateStyle validate[required]"  id = "startTime"  name="startTime" placeholder="请选择执行时间"  value="${tjpz.startTime}" type="text">
									<span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
								</div>
							</div>
							<label class="col-xs-2 control-label"><span class="required"> * </span>结束时点：</label>
							<div class="col-xs-4">
								<div class="input-group-date">
									<input readonly class="dateStyle validate[required]"  id = "endTime"  name="endTime" placeholder="请选择执行时间"  value="${tjpz.endTime}" type="text">
									<span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
								</div>
							</div>
						</div>
						<div class="form-group row pt7" style="height: auto;">
		                    <label class="col-xs-2 control-label"><span class="required"> * </span>是否有效：</label>
		                    <div class="col-xs-4">
			                    <select class="form-control input-sm select2 validate[required]" name="effectiveflag">
		                            <option value="<%=BusinessName.OK.getValue()%>" <c:if test="${'1' == tjpz.effectiveflag }"> selected</c:if>>是</option>
		                         	<option value="<%=BusinessName.NG.getValue()%>" <c:if test="${'0' == tjpz.effectiveflag }"> selected</c:if>>否</option>
		                        </select>
		                    </div>

							<label class="col-xs-2 control-label"><span class="required"> * </span>在场人数：</label>
							<div class="col-xs-4" style="padding-left: 0;">
								<input type="text" name="presentNumber" id="presentNumber" value="${tjpz.presentNumber}" class="form-control validate[required]">
							</div>
		                </div>

	                </c:if>

					<c:if test="${txlx eq 'PEOPLES_MONTH_REMIND'}">
						<div class="form-group row">
							<label class="col-xs-2 control-label"><span class="required"> * </span>站点名称：</label>
							<div class="col-xs-10">
								<select multiple="multiple" class="form-control input-sm select2 validate[required]" id="siteIds">
									<c:forEach items="${gds}" var="gdxx">
										<%--<option value="${gdxx.ID}" <c:if test="${fn:contains(tjpz.siteIds,gdxx.ID) }"> selected</c:if>>${gdxx.NAME}</option>--%>
										<option value="${gdxx.ID}" <c:if test="${fn:contains(tjpz.siteNames,gdxx.NAME) || (tjpz.siteNames eq gdxx.NAME)}">selected</c:if>>${gdxx.NAME}</option>
									</c:forEach>
								</select>
							</div>
							<input type="hidden" name="siteIds" id="siteIdsHide" class="form-control">
						</div>
						<div class="form-group row pt7">
							<label class="col-xs-2 control-label"><span class="required"> * </span>岗位：</label>
							<div class="col-xs-10">
								<select multiple="multiple" class="form-control input-sm select2 validate[required]" id="workType">
									<c:forEach items="${workTypeList}" var="item">
										<option value="${item.code}" <c:if test="${fn:contains(tjpz.workType,item.code) }"> selected</c:if>>${item.dictValue}</option>
									</c:forEach>
								</select>
							</div>
							<input type="hidden" name="workType" id="workTypeHide" class="form-control">
						</div>
						<div class="form-group row" style="padding-top: 2%">
							<label class="col-xs-2 control-label"><span class="required"> * </span>通知角色：</label>
							<div class="col-xs-10">
								<select multiple="multiple" class="form-control input-sm select2 validate[required]" id="roleIds" disabled>
									<c:forEach items="${roleList}" var="item">
										<%--<option value="${item.ROLE_ID}" <c:if test="${fn:contains(tjpz.roleIds,item.ROLE_ID) }"> selected</c:if>>${item.ROLE_NAME}</option>--%>
										<option value="${item.ROLE_ID}" <c:if test="${fn:contains('app_people_remind',item.ROLE_ID) }"> selected</c:if>>${item.ROLE_NAME}</option>
									</c:forEach>
								</select>
							</div>
							<input type="hidden" name="roleIds" id="roleIdsHide" class="form-control">
						</div>
						<div class="form-group row" style="padding-top:4%;height: auto;">
							<label class="col-xs-2 control-label"><span class="required"> * </span>开始日期：</label>
							<div class="col-xs-4">
								<div class="input-group-date">
									<input readonly class="dateStyle validate[required,past[#endDate]]"  id = "startDate"  name="startDate" placeholder="请选择执行时间"  value=<fmt:formatDate value="${tjpz.startDate}" pattern="yyyy-MM-dd"/> <c:if test="${tjpz.startDate == null}">""</c:if> type="text">
									<span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
								</div>
							</div>
							<label class="col-xs-2 control-label"><span class="required"> * </span>结束日期：</label>
							<div class="col-xs-4">
								<div class="input-group-date">
									<input readonly class="dateStyle validate[required,future[#startDate]]"  id = "endDate"  name="endDate" placeholder="请选择执行时间"  value=<fmt:formatDate value="${tjpz.endDate}" pattern="yyyy-MM-dd"/>  <c:if test="${tjpz.endDate == null}">""</c:if> type="text">
									<span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
								</div>
							</div>
						</div>
						<div class="form-group row pt7" style="height: auto;">
							<label class="col-xs-2 control-label"><span class="required"> * </span>是否有效：</label>
							<div class="col-xs-4">
								<select class="form-control input-sm select2 validate[required]" name="effectiveflag">
									<option value="<%=BusinessName.OK.getValue()%>" <c:if test="${'1' == tjpz.effectiveflag }"> selected</c:if>>是</option>
									<option value="<%=BusinessName.NG.getValue()%>" <c:if test="${'0' == tjpz.effectiveflag }"> selected</c:if>>否</option>
								</select>
							</div>

							<label class="col-xs-2 control-label"><span class="required"> * </span>在场天数：</label>
							<div class="col-xs-4" style="padding-left: 0;">
								<input type="text" name="presentDays" id="presentDays" value="${tjpz.presentDays}" class="form-control validate[required]">
							</div>
						</div>

					</c:if>
					<c:if test="${txlx eq 'VEHICLE_SUPERVISE_REMIND'}">
						<div class="form-group row">
							<label class="col-xs-2 control-label"><span class="required">*</span>站点名称：</label>
							<div class="col-xs-10">
								<select multiple="multiple" class="form-control input-sm select2 validate[required]" id="siteIds">
									<c:forEach items="${gds}" var="gdxx">
										<%--<option value="${gdxx.ID}" <c:if test="${fn:contains(tjpz.siteIds,gdxx.ID) }"> selected</c:if>>${gdxx.NAME}</option>--%>
										<option value="${gdxx.ID}" <c:if test="${fn:contains(tjpz.siteNames,gdxx.NAME) || (tjpz.siteNames eq gdxx.NAME)}">selected</c:if>>${gdxx.NAME}</option>
									</c:forEach>
								</select>
							</div>
							<input type="hidden" name="siteIds" id="siteIdsHide" class="form-control">
						</div>
						<div class="form-group row pt7" >
							<label class="col-xs-2 control-label"><span class="required"> * </span>车辆：</label>
							<div class="col-xs-10">
								<select multiple="multiple" class="form-control input-sm select2 validate[required]" id="vehicleTypes">
									<c:forEach items="${vehicleTypeList}" var="item">
										<option value="${item.vehicleType}" <%--vehicleId="${item.vehicleIds}"--%> <c:if test="${fn:contains(tjpz.vehicleTypes,item.vehicleType) || (tjpz.vehicleTypes eq item.vehicleType) }"> selected</c:if>>${item.vehicleTypeName}</option>
									</c:forEach>
								</select>
							</div>
<%--							<input type="hidden" name="vehicleIds" id="vehicleIdsHide"  value="${tjpz.vehicleIds}" class="form-control">--%>
							<input type="hidden" name="vehicleTypes" id="vehicleTypesHide" value="${tjpz.vehicleTypes}" class="form-control">
						</div>
						<div class="form-group row pt7" style="padding-top: 2%">
							<label class="col-xs-2 control-label"><span class="required"> * </span>岗位：</label>
							<div class="col-xs-10">
								<select multiple="multiple" class="form-control input-sm select2 validate[required]" id="workType">
									<c:forEach items="${workTypeList}" var="item">
										<option value="${item.code}" <c:if test="${fn:contains(tjpz.workType,item.code) }"> selected</c:if>>${item.dictValue}</option>
									</c:forEach>
								</select>
							</div>
							<input type="hidden" name="workType" id="workTypeHide" class="form-control">
						</div>

						<div class="form-group row" style="padding-top:4%;height: auto;">
							<label class="col-xs-2 control-label"><span class="required"> * </span>开始日期：</label>
							<div class="col-xs-4">
								<div class="input-group-date">
									<input readonly class="dateStyle validate[required,past[#endDate]]"  id = "startDate"  name="startDate" placeholder="请选择执行时间"  value=<c:if test="${tjpz.startDate != null}"> <fmt:formatDate value="${tjpz.startDate}" pattern="yyyy-MM-dd"/></c:if> <c:if test="${tjpz.startDate == null}">""</c:if> type="text">
									<span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
								</div>
							</div>
							<label class="col-xs-2 control-label"><span class="required"> * </span>结束日期：</label>
							<div class="col-xs-4">
								<div class="input-group-date">
									<input readonly class="dateStyle validate[required,future[#startDate]]"  id = "endDate"  name="endDate" placeholder="请选择执行时间"  value=<c:if test="${tjpz.endDate != null}"> <fmt:formatDate value="${tjpz.endDate}" pattern="yyyy-MM-dd"/></c:if> <c:if test="${tjpz.endDate == null}">""</c:if>  type="text">
									<span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
								</div>
							</div>
						</div>
						<div class="form-group row pt7" style="height: auto;">
							<label class="col-xs-2 control-label"><span class="required"> * </span>是否有效：</label>
							<div class="col-xs-4">
								<select class="form-control input-sm select2 validate[required]" name="effectiveflag">
									<option value="<%=BusinessName.OK.getValue()%>" <c:if test="${'1' == tjpz.effectiveflag }"> selected</c:if>>是</option>
									<option value="<%=BusinessName.NG.getValue()%>" <c:if test="${'0' == tjpz.effectiveflag }"> selected</c:if>>否</option>
								</select>
							</div>

							<label class="col-xs-2 control-label"></label>
							<div class="col-xs-4" style="padding-left: 0;">

							</div>
						</div>
					</c:if>
				</form>
			</div>
			<div class="modal-footer text-center">
                <input type="button" value="提交" onclick="saveTxpz();" class="btn btn-md btn-primary">
            </div>
		</div>
	</div>
</div>




	<script>
	
	    
	    $(function (){
	    	$.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });
	    	var multiple = $('#workType').select2({
                placeholder: '请选择',
                allowClear: true
            });

            $('#workType').click(function(event){
                event.preventDefault();
                var res = [];
                $(this).next('select').find('option').each(function(i,ele){
                    res.push($(ele).val()+",");
                });
                $('#workType').val(res).trigger('change');
            });

            var multiple2 = $('#siteIds').select2({
                placeholder: '请选择',
                allowClear: true
            });

            $('#siteIds').click(function(event){
                event.preventDefault();
                var res = [];
                $(this).next('select').find('option').each(function(i,ele){
                    res.push($(ele).val()+",");
                });
                $('#siteIds').val(res).trigger('change');
            });

            var multiple3 = $('#roleIds').select2({
                placeholder: '请选择',
                allowClear: true
            });

            $('#roleIds').click(function(event){
                event.preventDefault();
                var res = [];
                $(this).next('select').find('option').each(function(i,ele){
                    res.push($(ele).val()+",");
                });
                $('#roleIds').val(res).trigger('change');
            });

			var multiple4 = $('#vehicleTypes').select2({
				placeholder: '请选择',
				allowClear: true
			});


			$('#vehicleTypes').change(function(){
				var all="";
				var allId="";
				$("#vehicleTypes option:selected").each(function(){
					all+=$(this).val()+",";
					/*allId+=$(this).attr("vehicleId")+",";*/
				});
				all = all.substr(0, all.length - 1);//去掉末尾的逗号
				//allId = allId.substr(0, allId.length - 1);//去掉末尾的逗号
				$('#vehicleTypesHide').val(all);
				//$('#vehicleIdsHide').val(allId);
			});


			$('#startTime').datetimepicker({
	        	format: 'hh:ii',
	            todayHighlight: 1,
	            minView: 0,
	            startView: 1,
	            autoclose: 1,
	            language: 'zh-CN'
			});
	    	
	    	$('#endTime').datetimepicker({
	    		format: 'hh:ii',
	            todayHighlight: 1,
	            minView: 0,
	            startView: 1,
	            autoclose: 1,
	            language: 'zh-CN'
			});

            $('#startDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });
            $('#endDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });
	    });
	    function saveTxpz(){
	    	var presentNumber = $('#presentNumber').val();
            var presentDays = $('#presentDays').val();
	    	var startTime = $('#startTime').val();
	    	var endTime = $('#endTime').val();
            var startDate = $('#startDate').val();
            var endDate = $('#endDate').val();
	    	var workType = $('#workType').val();
            var siteIds = $('#siteIds').val();
            var roleIds = $('#roleIds').val();
			var vehicleTypes = $('#vehicleTypes').val();
	    	if(workType != null && workType != ""){
                workTypes = workType.join(",");
                $('#workTypeHide').val(workTypes);
            }
            if(roleIds != null && roleIds != ""){
                roleIdss = roleIds.join(",");
                $('#roleIdsHide').val(roleIdss);
            }
            if(siteIds != null && siteIds != ""){
                siteIdss = siteIds.join(",");
                $('#siteIdsHide').val(siteIdss);
            }
			// if(vehicleTypes != null && vehicleTypes != ""){
			// 	vehicleIdss = vehicleIds.join(",");
			// 	$('#vehicleIdsHide').val(vehicleIdss);
			//
			// 	vehicleTypess = vehicleTypes.join(",");
			// 	$('#vehicleTypesHide').val(vehicleTypess);
			// }
	    	<%--<c:if test="${txlx eq 'PEOPLES_REMIND'}">
	    		if(presentNumber && startTime && endTime){
	    		}else{
	    			notyWarning("必填项不能为空！");
	                return false;
	    		}
    		</c:if>
            <c:if test="${txlx eq 'PEOPLES_MONTH_REMIND'}">
            if(presentDays && startDate && endDate){
            }else{
                notyWarning("必填项不能为空！");
                return false;
            }
            </c:if>--%>
    		var action = path + "/remind/saveTxpz";
    		$('#txpzForm').attr('action',action);

			var valid = $("#txpzForm").validationEngine("validate");
			if (valid)
			{
				$('#txpzForm').submit();
			}
	    }

	</script>
</body>
</html>