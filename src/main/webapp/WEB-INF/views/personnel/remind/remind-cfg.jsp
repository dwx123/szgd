<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page import="com.szgd.util.BusinessName"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>提醒设置</title>
    <meta charset="utf-8"/>
    <%@ include file="/common/init.jsp" %>
    <%--<%@ include file="/common/include-dataTable.jsp"%>--%>
    <!--[if lt IE 9]>
    <script src="${ctx }/js/common/common.js" type="text/javascript"></script>
    <![endif]-->

</head>
<body>
<div class="bg-white item_title">
	提醒设置
</div>
<div class="bg-white pd_item pdt0" style="position: relative;">
    <form id="tblSearchForm" class="pdt0 mb10 form_search form_table container-fluid">
	    <div class="row form-group">
	        <div class="col-xs-2">
	            <div class="text-left">
					<select class="form-control input-sm select2" id="txlxSel" onchange="txlxChange();">
						<c:forEach var="txlx" items="${txlxs}" >
							<option value="${txlx.code}" <c:if test="${remindType eq txlx.code}">selected</c:if>>${txlx.dictValue}</option>
						</c:forEach>
					</select>
	            </div>
	        </div>
	    </div>
	</form>
    <div class="portlet-body">
	    <table class="table table-striped table-bordered table-hover display nowrap self_table" id="remindCfgTable">
	    </table>
	    <div class="row mt10 mb20">
			<div class="col-xs-6 text-center" style="width: 100%">
				<button class="btn btn-info" onclick="addRemindCfg();"><%--<i class="fa fa-plus"></i>--%> 新增设置
				</button>
			</div>
		</div>
	</div>
</div>
<script>
	var columns = [];
    $(document).ready(function () {
	   	$.ajax({
	        url: path + '/remind/getRemindCfgTitle',
	        type: 'post',
	        dataType: 'json',
	        async: false,//使用同步的方式,true为异步方式
	        data: {"typecx":$('#txlxSel option:selected').val()},
	        success: function (res) {
	            console.log(res);
	            columns = res.colList;
	        	createTable($('#txlxSel option:selected').val());
	        },
	        error: function (res) {
	        	console.log(res);
	        }
	        
	    });

        $(".select2").select2({
            allowClear: false
        });
    });
    
    function txlxChange(){
    	$.ajax({
	        url: path + '/remind/getRemindCfgTitle',
	        type: 'post',
	        dataType: 'json',
	        async: false,//使用同步的方式,true为异步方式
	        data: {"typecx":$('#txlxSel option:selected').val()},
	        success: function (res) {
	            columns = res.colList;
	            $("#remindCfgTable").DataTable().destroy();
	            $("#remindCfgTable").empty();
	            createTable($('#txlxSel option:selected').val());
	        },
	        error: function (res) {
	        	console.log(res);
	        }
	        
	    });
    }
    
    function addRemindCfg() {
    	var txlx = $('#txlxSel option:selected').val();
    	if(txlx){
            openWin('#remindCfgAddModel', path + '/remind/remindCfg-detail', {"txlx": txlx,"modalFlag": "<%=BusinessName.MODAL_XZ.getValue()%>"});
    	}else{
    		notyWarning("请选择一项提醒类型！");
    	}
    }
    
    function editRemindCfg(pzid) {
    	var txlx = $('#txlxSel option:selected').val();
    	if(txlx){
            openWin('#remindCfgAddModel', path + '/remind/remindCfg-detail', {"txlx": txlx,"pzid": pzid,"modalFlag": "<%=BusinessName.MODAL_XG.getValue()%>"});
    	}else{
    		notyWarning("请选择一项提醒类型！");
    	}
    }
    
    function createTable(txlx) {
    	if (txlx == 'PEOPLES_MONTH_REMIND' || txlx == 'VEHICLE_SUPERVISE_REMIND' )
		{
			return $('#remindCfgTable').dataTable({
//	            "lengthMenu": [
//	                [5, 15, 20, -1],
//	                [5, 15, 20, "All"] // change per page values here
//	            ],
				dom: "<'row-fluid'<'pull-left filterInput'f>>t<'row-fluid'<'span4'il><'span7'<'pull-right'p>>>",
				"paging": true,
				bFilter:false,
				"columns": columns,
				// set the initial value
				"columnDefs": [
					{
						"targets" : [5,6],
						"data" : null,
						"orderable" :false,
						"render" : function (data,type,row,meta){
							var domHtml = "";
							domHtml +=  toDate(data, "-", false);
							return domHtml;
						}
					},
					{
						"targets" : -1,
						"data" : null,
						"orderable" :false,
						"render" : function (data,type,row,meta){
							var domHtml = "";
							domHtml += "<a  class='btn-sm btn-smd btn btn-success' href='javascript:void(0);' onclick=\"editRemindCfg(\'"+row.id+"\')\" >编辑</a>";
							domHtml += "&nbsp;&nbsp;";
							domHtml += "<a class='btn-sm btn-smd btn btn-danger' href=\"remind-cfg-delete/"+ row.id +"?token=${token}\" onclick=\"if(confirm('确认删除?') == false) return false;\">删除</a>";
							domHtml += "&nbsp;&nbsp;";
							return domHtml;
						}
					}
				],
				"pageLength": 5,
				sScrollXInner: "150%",
				sScrollX: "100%",
				"bServerSide": false,
				"sAjaxSource": path + '/remind/getRemindCfgData?remindType='+$('#txlxSel option:selected').val(),
				"fnServerData": function (sSource, aoData, fnCallback) {

					$.ajax({
						headers: {
							Accept: "application/json; charset=utf-8"
						},
						"type": 'post',
						"url": sSource,
						"sync": false,
						"dataType": "json",
						"success": function (resp) {
							console.log(resp);
							fnCallback(resp);
						}
					});
				}
				/* ,"fnServerParams": function ( aoData ) {
                    aoData.push( { "name": "txlxcx", "value": $('#txlxSel option:selected').val() } );
                }
     */
			});
		}else
		{
			return $('#remindCfgTable').dataTable({
//	            "lengthMenu": [
//	                [5, 15, 20, -1],
//	                [5, 15, 20, "All"] // change per page values here
//	            ],
				dom: "<'row-fluid'<'pull-left filterInput'f>>t<'row-fluid'<'span4'il><'span7'<'pull-right'p>>>",
				"paging": true,
				bFilter:false,
				"columns": columns,
				// set the initial value
				"columnDefs": [
					{
						"targets" : -1,
						"data" : null,
						"orderable" :false,
						"render" : function (data,type,row,meta){
							var domHtml = "";
							domHtml += "<a  class='btn-sm btn-smd btn btn-success' href='javascript:void(0);' onclick=\"editRemindCfg(\'"+row.id+"\')\" >编辑</a>";
							domHtml += "&nbsp;&nbsp;";
							domHtml += "<a class='btn-sm btn-smd btn btn-danger' href=\"remind-cfg-delete/"+ row.id +"?token=${token}\" onclick=\"if(confirm('确认删除?') == false) return false;\">删除</a>";
							domHtml += "&nbsp;&nbsp;";
							return domHtml;
						}
					}
				],
				"pageLength": 5,
				sScrollXInner: "150%",
				sScrollX: "100%",
				"bServerSide": false,
				"sAjaxSource": path + '/remind/getRemindCfgData?remindType='+$('#txlxSel option:selected').val(),
				"fnServerData": function (sSource, aoData, fnCallback) {

					$.ajax({
						headers: {
							Accept: "application/json; charset=utf-8"
						},
						"type": 'post',
						"url": sSource,
						"sync": false,
						"dataType": "json",
						"success": function (resp) {
							console.log(resp);
							fnCallback(resp);
						}
					});
				}
				/* ,"fnServerParams": function ( aoData ) {
                    aoData.push( { "name": "txlxcx", "value": $('#txlxSel option:selected').val() } );
                }
     */
			});
		}

   	}
	function toDate(inputstr, showsplit, showweek) {
		//Wed Mar 22 13:38:37 CST 2017
		inputstr = inputstr + ""; //末尾加一个空格
		var date = "";
		var month = new Array();
		var week = new Array();

		month["Jan"] = 1; month["Feb"] = 2; month["Mar"] = 3; month["Apr"] = 4; month["May"] = 5; month["Jan"] = 6;
		month["Jul"] = 7; month["Aug"] = 8; month["Sep"] = 9; month["Oct"] = 10; month["Nov"] = 11; month["Dec"] = 12;
		week["Mon"] = "一"; week["Tue"] = "二"; week["Wed"] = "三"; week["Thu"] = "四"; week["Fri"] = "五"; week["Sat"] = "六"; week["Sun"] = "日";

		str = inputstr.split(" ");

		date = str[5];
		date += showsplit + month[str[1]] + showsplit + str[2];
		if(showweek){
			date += "    " + " 星期" + week[str[0]];
		}

		return date;
	}
</script>
<div id="remindCfgAddModel" class="modal fade" data-backdrop="static"></div>
</body>
</html>