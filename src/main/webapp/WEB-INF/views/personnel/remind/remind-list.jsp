<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
	<%@ include file="/common/init.jsp"%>
	<%--<script src="${ctx }/js/common/common.js" type="text/javascript"></script>--%>
	<c:if test="${from ne 'home'}">
		<title>
			<c:if test="${pageFlag eq 'APTITUDE_REMIND'}"> 人员资质期限提醒</c:if>
			<c:if test="${pageFlag eq 'PEOPLES_REMIND'}"> 特定时间内人数提醒</c:if>
			<c:if test="${pageFlag eq 'PEOPLES_MONTH_REMIND'}"> 人员到场次数提醒</c:if>
			<c:if test="${pageFlag eq 'PHYSICAL_EXAMINATION_REMIND'}"> 人员体检提醒</c:if>
			<c:if test="${pageFlag eq 'EQUIPMENT_CHECK_REMIND'}"> 设备检测提醒</c:if>
			<c:if test="${pageFlag eq 'EQUIPMENT_INSURANCE_REMIND'}"> 设备保险提醒</c:if>
			<c:if test="${pageFlag eq 'PROJECT_SCHEDULE_REMIND'}"> 项目进度提醒</c:if>
			<c:if test="${pageFlag eq 'VEHICLE_SUPERVISE_REMIND'}">监理旁站提醒</c:if>
		</title>
	</c:if>


    <script type="text/javascript">
		var path = '<%=request.getContextPath() %>';

        $(function (){
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear: true
            });

			remindTable = $("#remindTable").DataTable({
				iDisplayLength: 10,
				"sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
				iDisplayStart: 0,
				"bSort": false,
				sServerMethod: "GET",
                sAjaxSource : path + "/remind/getRemindData",//获取列表数据url,
                <c:if test="${pageFlag eq 'APTITUDE_REMIND'}">
               	 	aoColumns: [
//                            {'mDataProp':'field1' , 'sTitle' : '合同编号','sClass' : 'center'},
                            {'mDataProp':'field2' ,'sTitle' : '姓名','sClass' : 'center'},
                            {'mDataProp':'field3' ,'sTitle' : '证书编号','sClass' : 'center'},
                            {'mDataProp':'field4' ,'sTitle' : '证书名称','sClass' : 'center'},
                            {'mDataProp':'field5' ,'sTitle' : '证书有效日期','sClass' : 'center'},
                            {'mDataProp':'content' ,'sTitle' : '提醒内容','sClass' : 'center'},
                            {'mDataProp':'remindTimeSxStr' ,'sTitle' : '提醒时间','sClass' : 'center'}
                        ],
                </c:if>
                <c:if test="${pageFlag eq 'PEOPLES_REMIND'}">
	           	 	aoColumns: [
	                        {'mDataProp':'siteName' ,'sTitle' : '站点名称','sClass' : 'center'},
	                        {'mDataProp':'content' ,'sTitle' : '提醒内容','sClass' : 'center'},
	                        {'mDataProp':'remindTimeStr' ,'sTitle' : '提醒时间','sClass' : 'center'}
	                    ],
	            </c:if>
                <c:if test="${pageFlag eq 'PEOPLES_MONTH_REMIND'}">
					aoColumns: [
						{'mDataProp':'siteName' ,'sTitle' : '站点名称','sClass' : 'center'},
						{'mDataProp':'content' ,'sTitle' : '提醒内容','sClass' : 'center'},
						{'mDataProp':'remindTimeStr' ,'sTitle' : '提醒时间','sClass' : 'center'}
						],
                </c:if>
				<c:if test="${pageFlag eq 'VEHICLE_SUPERVISE_REMIND'}">
					aoColumns: [
						{'mDataProp':'siteName' ,'sTitle' : '站点名称','sClass' : 'center'},
						{'mDataProp':'content' ,'sTitle' : '提醒内容','sClass' : 'center'},
						{'mDataProp':'remindTimeStr' ,'sTitle' : '提醒时间','sClass' : 'center'}
					],
				</c:if>
                <c:if test="${pageFlag eq 'PHYSICAL_EXAMINATION_REMIND'}">
	           	 	aoColumns: [
//	                        {'mDataProp':'field1' , 'sTitle' : '合同编号','sClass' : 'center'},
                            {'mDataProp':'field2' ,'sTitle' : '姓名','sClass' : 'center'},
                            {'mDataProp':'field3' ,'sTitle' : '体检日期','sClass' : 'center'},
                            {'mDataProp':'content' ,'sTitle' : '提醒内容','sClass' : 'center'},
                            {'mDataProp':'remindTimeSxStr' ,'sTitle' : '提醒时间','sClass' : 'center'}
	                    ],
	            </c:if>
                <c:if test="${pageFlag eq 'EQUIPMENT_CHECK_REMIND'}">
                aoColumns: [
                    {'mDataProp':'field1' , 'sTitle' : '设备编号','sClass' : 'center'},
                    {'mDataProp':'field2' ,'sTitle' : '设备型号','sClass' : 'center'},
                    {'mDataProp':'field3' ,'sTitle' : '检测日期','sClass' : 'center'},
                    {'mDataProp':'content' ,'sTitle' : '提醒内容','sClass' : 'center'},
                    {'mDataProp':'remindTimeSxStr' ,'sTitle' : '提醒时间','sClass' : 'center'}
                ],
                </c:if>
                <c:if test="${pageFlag eq 'EQUIPMENT_INSURANCE_REMIND'}">
                aoColumns: [
                    {'mDataProp':'field1' , 'sTitle' : '设备编号','sClass' : 'center'},
                    {'mDataProp':'field2' ,'sTitle' : '设备型号','sClass' : 'center'},
                    {'mDataProp':'field3' ,'sTitle' : '保险到期日期','sClass' : 'center'},
                    {'mDataProp':'content' ,'sTitle' : '提醒内容','sClass' : 'center'},
                    {'mDataProp':'remindTimeSxStr' ,'sTitle' : '提醒时间','sClass' : 'center'}
                ],
                </c:if>
                <c:if test="${pageFlag eq 'PROJECT_SCHEDULE_REMIND'}">
                aoColumns: [
                    {'mDataProp':'field1' , 'sTitle' : '项目编号','sClass' : 'center'},
                    {'mDataProp':'field2' ,'sTitle' : '项目名称','sClass' : 'center'},
                    {'mDataProp':'field3' ,'sTitle' : '项目阶段','sClass' : 'center'},
                    {'mDataProp':'field4' ,'sTitle' : '阶段完成日期','sClass' : 'center'},
                    {'mDataProp':'content' ,'sTitle' : '提醒内容','sClass' : 'center'},
                    {'mDataProp':'remindTimeSxStr' ,'sTitle' : '提醒时间','sClass' : 'center'}
                ],
                </c:if>
                aoColumnDefs: [
                    
                ],
                //向后台提交url携带的参数
                fnServerParams : function(aoData) {
					aoData.push({"name":"remindType","value":$('#pageFlag').val()});
					//获得form表单数据并封装在object中
					var formIptArray = $("#remindSearchForm input");
					$.each(formIptArray,function(i , obj){
						var name = $(obj).attr("name");
						var val = $(obj).val();
						aoData.push({
							"name" : name,
							"value" : val
						});
					});

					var formSelectArray = $("#remindSearchForm select");
					$.each(formSelectArray,function(i , obj){
						var name = $(obj).attr("name");
						var val = $(obj).val();
						aoData.push({
							"name" : name,
							"value" : val

						});
					});

                }
            });
            
            $('#field5dycx').datetimepicker({
	        	format: 'yyyy-mm-dd',
	            todayHighlight: 1,
	            minView: "month",
	            startView: 2,
	            autoclose: 1,
	            language: 'zh-CN'
			});
	        
	        $('#field5xycx').datetimepicker({
				format: 'yyyy-mm-dd',
	            todayHighlight: 1,
	            minView: "month",
	            startView: 2,
	            autoclose: 1,
	            language: 'zh-CN'
			});
	        
	        $('#field3dycx').datetimepicker({
	        	format: 'yyyy-mm-dd',
	            todayHighlight: 1,
	            minView: "month",
	            startView: 2,
	            autoclose: 1,
	            language: 'zh-CN'
			});
	        
	        $('#field3xycx').datetimepicker({
				format: 'yyyy-mm-dd',
	            todayHighlight: 1,
	            minView: "month",
	            startView: 2,
	            autoclose: 1,
	            language: 'zh-CN'
			});

            $('#beginTime').datetimepicker({
                format: 'yyyy-mm-dd hh:ii:ss', autoclose: true, minView: 0, minuteStep:1,
                todayHighlight: 1,
                startView: 2,
                language: 'zh-CN'
            });

            $('#endTime').datetimepicker({
                format: 'yyyy-mm-dd hh:ii:ss', autoclose: true, minView: 0, minuteStep:1,
                todayHighlight: 1,
                startView: 2,
                language: 'zh-CN'
            });

			$('#beginDate').datetimepicker({
				format: 'yyyy-mm-dd',
				todayHighlight: 1,
				minView: "month",
				startView: 2,
				autoclose: 1,
				language: 'zh-CN'
			});

			$('#endDate').datetimepicker({
				format: 'yyyy-mm-dd',
				todayHighlight: 1,
				minView: "month",
				startView: 2,
				autoclose: 1,
				language: 'zh-CN'
			});

            $('#reSetBtn').click(function () {
                resetSearchForm('remindSearchForm')
            })
			$('#findRemind').click(function () {
				remindTable.ajax.reload();
			})

        });

    </script>
</head>
<body>
<c:if test="${from ne 'home'}">
	<div class="bg-white item_title">
		<c:if test="${pageFlag eq 'APTITUDE_REMIND'}"> 人员资质期限提醒</c:if>
		<c:if test="${pageFlag eq 'PEOPLES_REMIND'}"> 特定时间内人数提醒</c:if>
		<c:if test="${pageFlag eq 'PEOPLES_MONTH_REMIND'}"> 人员到场次数提醒</c:if>
		<c:if test="${pageFlag eq 'VEHICLE_SUPERVISE_REMIND'}">监理旁站提醒</c:if>
		<c:if test="${pageFlag eq 'PHYSICAL_EXAMINATION_REMIND'}"> 人员体检提醒</c:if>
		<c:if test="${pageFlag eq 'EQUIPMENT_CHECK_REMIND'}"> 设备检测提醒</c:if>
		<c:if test="${pageFlag eq 'EQUIPMENT_INSURANCE_REMIND'}"> 设备保险提醒</c:if>
		<c:if test="${pageFlag eq 'PROJECT_SCHEDULE_REMIND'}"> 项目进度提醒</c:if>
	</div>
</c:if>

<c:if test="${not empty message}">
    <div class="ui-widget">
        <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <strong>提示：</strong>${message}</p>
        </div>
    </div>
</c:if>
<form id="remindSearchForm" class="bg-white form_search form_table container-fluid">
	<input type="hidden" name="pageFlag" id="pageFlag" value="${pageFlag}">
	<c:if test="${pageFlag eq 'APTITUDE_REMIND'}">
	    <div class="row form-group">
	        <div class="col-xs-4">
	            <div class="form-item wide2">
	                <label class="control-label">姓名</label>
	                <input type="text" name="field2mhcx" id="field2mhcx" class="form-control">
	            </div>
	        </div>
	        <div class="col-xs-4">
	            <div class="form-item wide2">
	                <label class="control-label">证书编号</label>
	                <input type="text" name="field3mhcx" id="field3mhcx" class="form-control">
	            </div>
	        </div>
	        <div class="col-xs-4">
	            <div class="form-item wide2">
	                <label class="control-label">证书名称</label>
	                <input type="text" name="field4mhcx" id="field4mhcx" class="form-control">
	            </div>
	        </div>
	    </div>
	    <div class="row form-group">
	        <div class="col-xs-4">
	            <div class="form-item wide2">
					<label class="control-label">证书有效日期</label>
					<div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd">
						<input style="background-color: #fff;" class="form-control" id="field5dycx" name="field5dycx" type="text" placeholder="" readonly>
						<span class="input-group-addon"> 至 </span>
						<input style="background-color: #fff;" class="form-control" id="field5xycx" name="field5xycx" type="text" placeholder="" readonly>
					</div>
	            </div>
	        </div>
	        <div class="col-xs-8">
	            <div class="text-right">
					<button type="button"  class="btn btn-md btn-primary mr15" id="findRemind">查询</button>
	                <button type="button" class="btn btn-md btn-reset mr15" id="reSetBtn">重置</button>
	            </div>
	        </div>
	    </div>
	  </c:if>
	<c:if test="${pageFlag eq 'PEOPLES_REMIND'}">
	    <div class="row form-group">
	        <div class="col-xs-4">
	            <div class="form-item wide2">
	                <label class="control-label">站点名称</label>
					<select class="form-control input-sm select2" name="siteId" id="siteId">
						<option value=""> 请选择</option>
						<c:forEach items="${gds }" var="gdxx">
                            <option value="${gdxx.ID }">${gdxx.NAME }</option>
                        </c:forEach>
					</select>
	            </div>
	        </div>
			<div class="col-xs-5">
				<div class="form-item wide2">
					<label class="control-label">提醒时间</label>
					<div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">
						<input style="background-color: #fff;" class="form-control" id="beginTime" name="beginTime" type="text" placeholder="" readonly>
						<span class="input-group-addon"> 至 </span>
						<input style="background-color: #fff;" class="form-control" id="endTime" name="endTime" type="text" placeholder="" readonly>
					</div>
				</div>
			</div>
	        <div class="col-xs-3">
	            <div class="text-right">
					<button type="button"  class="btn btn-md btn-primary mr15" id="findRemind">查询</button>
	                <button type="button" class="btn btn-md btn-reset mr15" id="reSetBtn">重置</button>
	            </div>
	        </div>
	    </div>
	  </c:if>
	<c:if test="${pageFlag eq 'PEOPLES_MONTH_REMIND' || pageFlag eq 'VEHICLE_SUPERVISE_REMIND'}">
		<div class="row form-group">
			<div class="col-xs-4">
				<div class="form-item wide2">
					<label class="control-label">站点名称</label>
					<select class="form-control input-sm select2" name="siteId" id="siteId">
						<option value=""> 请选择</option>
						<c:forEach items="${gds }" var="gdxx">
							<option value="${gdxx.ID }">${gdxx.NAME }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="col-xs-5">
				<div class="form-item wide2">
					<label class="control-label">提醒日期</label>
					<div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd">
						<input style="background-color: #fff;" class="form-control" id="beginDate" name="beginDate" type="text" placeholder="" readonly>
						<span class="input-group-addon"> 至 </span>
						<input style="background-color: #fff;" class="form-control" id="endDate" name="endDate" type="text" placeholder="" readonly>
					</div>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="text-right">
					<button type="button"  class="btn btn-md btn-primary mr15" id="findRemind">查询</button>
					<button type="button" class="btn btn-md btn-reset mr15" id="reSetBtn">重置</button>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${pageFlag eq 'PHYSICAL_EXAMINATION_REMIND'}">
	    <div class="row form-group">
	        <div class="col-xs-4">
	            <div class="form-item wide2">
	                <label class="control-label">姓名</label>
	                <input type="text" name="field2mhcx" id="field2mhcx" class="form-control">
	            </div>
	        </div>
	        <div class="col-xs-5">
	            <div class="form-item wide2">
					<label class="control-label">体检日期</label>
					<div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd">
						<input class="form-control" id="field3dycx" name="field3dycx" type="text" placeholder="">
						<span class="input-group-addon"> 至 </span>
						<input class="form-control" id="field3xycx" name="field3xycx" type="text" placeholder="">
					</div>
	            </div>
	        </div>
			<div class="col-xs-3">
				<div class="text-right">
					<button type="button"  class="btn btn-md btn-primary mr15" id="findRemind">查询</button>
					<button type="button" class="btn btn-md btn-reset mr15" id="reSetBtn">重置</button>
				</div>
			</div>
	    </div>
	  </c:if>
	<c:if test="${pageFlag eq 'EQUIPMENT_CHECK_REMIND'}">
		<div class="row form-group">
			<div class="col-xs-4">
				<div class="form-item wide2">
					<label class="control-label">设备型号</label>
					<input type="text" name="field2mhcx" id="field2mhcx" class="form-control">
				</div>
			</div>
			<div class="col-xs-8">
				<div class="text-right">
					<button type="button"  class="btn btn-md btn-primary mr15" id="findRemind">查询</button>
					<button type="button" class="btn btn-md btn-reset mr15" id="reSetBtn">重置</button>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${pageFlag eq 'EQUIPMENT_INSURANCE_REMIND'}">
		<div class="row form-group">
			<div class="col-xs-4">
				<div class="form-item wide2">
					<label class="control-label">设备型号</label>
					<input type="text" name="field2mhcx" id="field2mhcx" class="form-control">
				</div>
			</div>
			<div class="col-xs-8">
				<div class="text-right">
					<button type="button"  class="btn btn-md btn-primary mr15" id="findRemind">查询</button>
					<button type="button" class="btn btn-md btn-reset mr15" id="reSetBtn">重置</button>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${pageFlag eq 'PROJECT_SCHEDULE_REMIND'}">
		<div class="row form-group">
			<div class="col-xs-4">
				<div class="form-item wide2">
					<label class="control-label">项目编号</label>
					<input type="text" name="field1mhcx" id="field1mhcx" class="form-control">
				</div>
			</div>
			<div class="col-xs-4">
				<div class="form-item wide2">
					<label class="control-label">项目名称</label>
					<input type="text" name="field2mhcx" id="field2mhcx" class="form-control">
				</div>
			</div>
			<div class="col-xs-4">
				<div class="text-right">
					<button type="button"  class="btn btn-md btn-primary mr15" id="findRemind">查询</button>
					<button type="button" class="btn btn-md btn-reset mr15" id="reSetBtn">重置</button>
				</div>
			</div>
		</div>
	</c:if>
</form>
<div class="bg-white mt15">
    <div class="bg-white item_title mb10">
        <i class="fa fa-table"></i> 提醒列表
    </div>
    <table class="table table-striped table-bordered table-hover display nowrap self_table" id="remindTable">
    </table>
</div>

<div id="rkAddModel" class="modal fade"></div>
</body>
</html>