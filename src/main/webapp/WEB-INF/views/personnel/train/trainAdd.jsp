<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>培训新增</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear: true
            });


            $('#beginTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            var multiple = $('#PERSONNEL_ID').select2({
                placeholder: '请选择',
                allowClear: true
            });

            $('#PERSONNEL_ID').click(function(event){
                event.preventDefault();
                var res = [];
                $(this).next('select').find('option').each(function(i,ele){
                    res.push($(ele).val())
                });
                $(multiple).val(res).trigger('change');
            });
        });

        function addTrainInfo(){

            var valid = $("#trainDetail").validationEngine("validate");
            if (!valid)
                return;
            var param = serializeObject('#trainDetail');
            var otherParam = {};
            var jsonString = JSON.stringify($.extend(param, otherParam));

            $.ajax({
                contentType : "application/json",
                url : '${ctx}/train/insertTrainInfo',
                type : 'POST',
                data: jsonString,
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        if(data.resultMsg=="培训信息新增成功!"){
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);
                        }else{
                            notyError(data.resultMsg);
                        }
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error));

                }
            });
        }


    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/train/toTrainList" title="点击返回至列表">人员培训管理</a>
    <i class="fa fa-angle-double-right"></i>
    培训新增
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="trainDetail">
        <h4><i class="fa fa-bars"></i> 培训信息</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row">

                <label class="col-xs-2 control-label"><span class="alarmstar">*</span>培训项目</label>
                <div class="col-xs-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="category" id="category">
                        <option value="">请选择</option>
                        <c:forEach var="obj" items="${categoryList}" >
                            <option value="${obj.code}">${obj.dictValue}</option>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-xs-2 control-label"><span class="alarmstar">*</span>培训地点</label>
                <div class="col-xs-3 pt7">
                    <input id="place" name="place" class="form-control form-control-inline input-sm validate[required,maxSize[200]]" size="16" placeholder="培训地点" type="text">
                </div>
            </div>

            <div class="row">
                <label class="col-xs-2 control-label"><span class="alarmstar">*</span>培训开始日期</label>
                <div class="col-xs-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[required,custom[date],past[#endTime]]"  id = "beginTime"  name="beginTime" placeholder="培训开始日期" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>

                <label class="col-xs-2 control-label"><span class="alarmstar">*</span>培训结束日期</label>
                <div class="col-xs-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[required,custom[date],future[#beginTime]]"  id = "endTime"  name="endTime" placeholder="培训结束日期" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>
            </div>

            <div class="row">

                <label class="col-xs-2 control-label"><span class="alarmstar">*</span>培训时间(小时)</label>
                <div class="col-xs-3 pt7">
                    <input id="totalTime" name="totalTime" class="form-control form-control-inline input-sm validate[required,custom[number]]" placeholder="培训时间(小时)" size="16" type="text">
                </div>

                <label class="col-xs-2 control-label">培训结果</label>
                <div class="col-xs-3 pt7">
                    <select class="form-control input-sm select2" name="scoreType" id="scoreType">
                        <option value="">请选择</option>
                        <c:forEach var="obj" items="${sessionScope.SCORE_TYPE}" >
                            <c:choose>
                                <c:when test="${obj.code eq train.scoreType}">
                                    <option value="${obj.code}" selected>${obj.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${obj.code}">${obj.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <h4><i class="fa fa-bars"></i> 培训人员</h4>
        <div class="form-body mb10 bg-blue">
            <div class="row" style="height: 46px">
                <label class="col-md-1-5 control-label"><span class="alarmstar">*</span>人员(可多选)</label>
                <div class="col-md-10 pt7" style="width: 88%;">
                    <select multiple="multiple" class="form-control input-sm select2 validate[required]" name="PERSONNEL_ID" id="PERSONNEL_ID">
                        <c:forEach var="obj" items="${personList}" >
                            <option value="${obj.ID}" title="${obj.title}">${obj.NAME}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-actions mt15">
            <div class="row">
                <div class="col-xs-offset-5 col-xs-5">
                    <button type="button" id="submitBtn" onclick="addTrainInfo()" class="btn btn-sm btn-success btn-md"><i class="fa fa-save"></i> 保存</button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/train/toTrainList" class="btn btn-md btn-default" title="返回上一页（不保存数据）"><i class="fa fa-reply"></i> 返回</a>
                </div>
            </div>
        </div>
    </form>

</div>
</body>
</html>
