<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>培训详情</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function(){
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $('#beginTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            var id = $('#id').val();
            var status = $('#status').val();

            if(status == 1){ //查看
                $('input').attr('disabled','disabled');
                $('select').attr('disabled','disabled');
            }else if(status == 2){ //编辑
                $('#personnelInfo input').attr('disabled','disabled');
                $('#personnelInfo select').attr('disabled','disabled');
            }
        });

        function save() {
            var valid = $("#trainDetail").validationEngine("validate");
            if (!valid)
                return;
            var inDate = $("#inDate").val();
            var beginTime = $("#beginTime").val();
            if(inDate != null && inDate != ""){
                if(CompareDate(inDate,beginTime) == false){
                    alert("培训开始日期需在入场日期之前!");
                    return;
                }
            }
            var param = serializeObject('#trainDetail');
            var jsonString = JSON.stringify(param);
            $.ajax({
                contentType: "application/json",
                url: path + '/train/saveTrainInfo',
                type : 'POST',
                data: jsonString,
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        setTimeout(function(){
                            history.go(-1);
                        },1500);

                    }else{
                        notyError(data.resultMsg);
                    }

                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error));
                    alert(JSON.stringify(error));
                }
            });
        }
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/train/toTrainList" title="点击返回至列表">人员培训管理</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${status == 1}">
        人员培训查看
    </c:if>
    <c:if test="${status == 2}">
        人员培训编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">

    <form id="personnelInfo" class="form-horizontal">
        <h4><i class="fa fa-bars"></i> 人员信息</h4>

        <div class="form-body mb10 bg-blue">
            <div class="row">

                <label class="col-md-2 control-label">姓名</label>
                <div class="col-md-3 pt7">
                    <input id="NAME" name="NAME" class="form-control form-control-inline input-sm" size="16" value="${train.name}" type="text">
                </div>

                <label class="col-md-2 control-label">身份证</label>
                <div class="col-md-3 pt7">
                    <input id="ID_NUMBER" name="ID_NUMBER" class="form-control form-control-inline input-sm" size="16" value="${train.idNumber}" type="text">
                </div>

            </div>
            <div class="row">

                <label class="col-md-2 control-label">性别</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="SEX" id="SEX">
                        <option value=""> 请选择</option>
                        <c:choose>
                            <c:when test="${train.sex == 0}">
                                <option value="0" selected>女</option>
                                <option value="1">男</option>
                            </c:when>
                            <c:otherwise>
                                <option value="0">女</option>
                                <option value="1" selected>男</option>
                            </c:otherwise>
                        </c:choose>
                    </select>
                </div>

                <label class="col-md-2 control-label">电话</label>
                <div class="col-md-3 pt7">
                    <input id="TEL" name="TEL" class="form-control form-control-inline input-sm" size="16" value="${train.tel}" type="text">
                </div>

            </div>

            <div class="row">

                <label class="col-md-2 control-label">岗位</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="WORK_TYPE" id="WORK_TYPE">
                        <option value="">请选择</option>
                        <c:forEach var="obj" items="${workTypeList}" >
                            <c:choose>
                                <c:when test="${obj.code eq train.workType}">
                                    <option value="${obj.code}" selected>${obj.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${obj.code}">${obj.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">所属单位</label>
                <div class="col-md-3 pt7">
                    <input id="company" name="company" class="form-control form-control-inline input-sm" size="16" value="${train.companyName}" type="text">
                </div>

            </div>
            <div class="row">

                <label class="col-md-2 control-label">入场日期</label>
                <div class="col-md-3 pt7">
                    <input id="inDate" name="inDate" class="form-control form-control-inline input-sm" size="16" value="${train.inDate}" type="text">
                </div>

                <label class="col-md-2 control-label"></label>
                <div class="col-md-3 pt7">
                </div>

            </div>
        </div>

    </form>
    <br>
    <form id="trainDetail" class="form-horizontal">
        <input id="id"  name="id" size="32" value="${id}" type="text" hidden>
        <input id="status" name="status"  size="32" value="${status}" type="text" hidden>
        <input id="personnelId" name="personnelId"  size="32" value="${train.personnelId}" type="text" hidden>
        <h4><i class="fa fa-bars"></i> 培训信息</h4>

        <div class="form-body mb10 bg-blue">
                <div class="row">
                    <label class="col-md-2 control-label"><span class="alarmstar">*</span>培训地点</label>
                    <div class="col-md-3 pt7">
                        <input id="place" name="place" class="form-control form-control-inline input-sm validate[required,maxSize[200]]" size="16" value="${train.place}" type="text">
                    </div>

                    <label class="col-md-2 control-label"><span class="alarmstar">*</span>培训项目</label>
                    <div class="col-md-3 pt7">
                        <select class="form-control input-sm select2 validate[required]" name="category" id="category">
                            <option value="">请选择</option>
                            <c:forEach var="obj" items="${categoryList}" >
                                <c:choose>
                                    <c:when test="${obj.code eq train.category}">
                                        <option value="${obj.code}" selected>${obj.dictValue}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${obj.code}">${obj.dictValue}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>

                </div>
                <div class="row">
                    <c:if test="${status == 1}">
                        <label class="col-md-2 control-label"><span class="alarmstar">*</span>培训开始日期</label>
                        <div class="col-md-3 pt7">
                            <input id="beginTime" name="beginTime" class="form-control form-control-inline input-sm validate[required,custom[date],past[#endTime]]" size="16" value="${train.beginTime}" type="text">
                        </div>

                        <label class="col-md-2 control-label"><span class="alarmstar">*</span>培训结束日期</label>
                        <div class="col-md-3 pt7">
                            <input id="endTime" name="endTime" class="form-control form-control-inline input-sm validate[required,custom[date],future[#beginTime]]" size="16" value="${train.endTime}" type="text">
                        </div>
                    </c:if>

                    <c:if test="${status == 2}">
                        <label class="col-md-2 control-label"><span class="alarmstar">*</span>培训开始日期</label>
                        <div class="col-md-3 pt7">
                            <div class="input-group-date">
                                <input readonly class="dateStyle validate[required,custom[date],past[#endTime]]"  id = "beginTime"  name="beginTime" value="${train.beginTime}" type="text">
                                <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                            </div>
                        </div>

                        <label class="col-md-2 control-label"><span class="alarmstar">*</span>培训结束日期</label>
                        <div class="col-md-3 pt7">
                            <div class="input-group-date">
                                <input readonly class="dateStyle validate[required,custom[date],future[#beginTime]]"  id = "endTime"  name="endTime" value="${train.endTime}" type="text">
                                <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                            </div>
                        </div>
                    </c:if>
                </div>
                <div class="row">
                    <label class="col-md-2 control-label"><span class="alarmstar">*</span>培训时间(小时)</label>
                    <div class="col-md-3 pt7">
                        <input id="totalTime" name="totalTime" class="form-control form-control-inline input-sm validate[required,custom[number]]" size="16" value="${train.totalTime}" type="text">
                    </div>

                    <%--<label class="col-md-1 control-label">考试分数</label>--%>
                    <%--<div class="col-md-2">--%>
                        <%--<input id="score" name="score" class="form-control form-control-inline input-sm validate[custom[number]]" size="16" value="${train.score}" type="text">--%>
                    <%--</div>--%>

                    <label class="col-md-2 control-label">培训结果</label>
                    <div class="col-md-3 pt7">
                        <select class="form-control input-sm select2" name="scoreType" id="scoreType">
                            <option value="">请选择</option>
                            <c:forEach var="obj" items="${scoreTypeList}" >
                                <c:choose>
                                    <c:when test="${obj.code eq train.scoreType}">
                                        <option value="${obj.code}" selected>${obj.dictValue}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${obj.code}">${obj.dictValue}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </div>

                </div>

            </div>

        <div class="form-actions mt45">
            <div class="row">
                <div class="col-md-offset-5 col-md-5">
                    <c:if test="${status==2}">
                        <button type="button" id="saveBtn" onclick="save()" class="btn btn-md btn-success"><i class="fa fa-save"></i> 保存</button>
                        &nbsp;&nbsp;
                    </c:if>
                    <a href="${ctx}/train/toTrainList" class="btn btn-md btn-default" title="返回上一页（不保存数据）"><i class="fa fa-reply"></i> 返回</a>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>
