<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>人员培训管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var personnelTrainTable ;
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            personnelTrainTable = $("#personnelTrainTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/train/getTrainList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'name', 'sTitle': '姓名', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'sex', 'sTitle': '性别', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'categoryName', 'sTitle': '培训项目', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'place', 'sTitle': '培训地点', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'beginTime', 'sTitle': '培训开始日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'endTime', 'sTitle': '培训结束日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'totalTime', 'sTitle': '培训时间(小时)', 'sWidth': '10%', 'sClass': 'center'},
                    /*{'mDataProp': 'score', 'sTitle': '考试分数', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'checkEquipName', 'sTitle': '签到设备', 'sWidth': '10%', 'sClass': 'center'},*/
                    {'mDataProp':null ,'sTitle' : '操作','sWidth' : '10%','sClass' : 'center'}
                ],
                "aoColumnDefs":[
                    {
                        "targets":-1,
                        "bSortable": false,
                        render: function(data, type, row) {
                            var html ='<a class="btn-sm btn-smd btn btn-primary" href="'+path+'/train/trainDetail?id='+row.id+'&status=1">查看</a>&nbsp;&nbsp;'+
                                '<a class="btn-sm btn-smd btn btn-success" href="'+path+'/train/trainDetail?id='+row.id+'&status=2">编辑</a>&nbsp;&nbsp;'+
                                '<a class="btn-sm btn-smd btn btn-danger" onclick="del('+row.id+','+row.personnelId+','+row.totalTime+')">删除</a>&nbsp;&nbsp;'
                                /*+'<a class="btn-sm btn-smd btn btn-info" onclick="checkDoc('+row.id+','+row.personnelId+')">查看签到流水</a>'*/;
                            return html;
                        }
                    },
                    {
                        aTargets: [1],
                        mRender: function (data, type, row) {
                            var sex = "";
                            if (data == 0) {
                                sex = '女';
                            }
                            if (data == 1) {
                                sex = '男';
                            }
                            return sex;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#personnelTrainForm input");
                    console.log(formIptArray);
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
//                        if (name.indexOf("startDate") != -1 )
//                        {
//                            if (val.length > 0)
//                                val = val +" 00:00:01"
//                        }
//                        if (name.indexOf("endDate") != -1)
//                        {
//                            if (val.length > 0)
//                                val = val +" 23:59:59"
//                        }
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });
                    var formSelectArray = $("#personnelTrainForm select");
                    $.each(formSelectArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });
                }

            });
            $("#findPersonnelTrain").click(function () {
                personnelTrainTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('personnelTrainForm')
            });

            $('#startDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endDate').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#startDate1').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endDate1').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });
        });

        function del(id,personnelId,totalTime) {
            if (!confirm("您是否要删除该记录？")) {
                return;
            }
            var params = {
                'id' : id,
                'personnelId': personnelId,
                'totalTime': totalTime
            };
            $.ajax({
                type:"post",
                cache : false,
                dataType : "json",
                url : path + "/train/deleteTrain",
                data : params,
                contentType : "application/x-www-form-urlencoded; charset=utf-8",
                success : function(data) {//msg为返回的数据，在这里做数据绑定
                    if (data.success == true) {
                        $('#personnelTrainTable').dataTable().fnDraw();
                        notySuccess(data.message);
                    } else {
                        notyError(data.message);
                    }
                },
                error: function () {
                    notyError("删除失败!");
                }
            });
        }

        function checkDoc(id,personnelId) {
            $('#myModal').modal('show');
            var checkDocTable ;
            checkDocTable = $("#checkDocTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/train/getTrainRecordList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'passTime', 'sTitle': '通过时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'deviceId', 'sTitle': '设备编号', 'sWidth': '10%', 'sClass': 'center'}
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    aoData.push({
                        "name" : "id",
                        "value" : id
                    });
                    aoData.push({
                        "name" : "personnelId",
                        "value" : personnelId
                    });
                }
            });
        }

        function checkUploadTrainForm() {
            var fileDir = $("#trainFile").val();
            if ("" == fileDir) {
                notyWarning("选择需要导入的文件！");
                return false;
            }
            var suffix = fileDir.substr(fileDir.lastIndexOf("."));
            if (".xls" != suffix && ".xlsx" != suffix) {
                notyWarning("选择Excel格式的文件导入！");
                return false;
            }
            return true;
        }
        function uploadTrain() {
            if (checkUploadTrainForm()) {
                function resutlMsg(msg) {
                    if (msg =="文件导入失败！"){
                        $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                    }else{
                        if(msg.indexOf("#WEBROOT#") != -1){
                            msg  = msg.replace("#WEBROOT#", path +"/upload");
                        }
                        $("#resultMsg").html("<font color='#006400'>" + msg +"</font>");
                    }
                    $("#file").val("");
                    $("#personnelTrainTable").DataTable().ajax.reload();
                }
                function errorMsg() {
                    $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                }
                $('#trainUploadForm').ajaxSubmit({
                    url: path + '/train/batchUploadTrain',
                    dataType: 'text',
                    success: resutlMsg,
                    error: errorMsg
                });
            }
        }
        function downloadTemplate() {
            var  queryParams = 'templateName=培训信息批量导入模版.xls';
            var url =path + "/upload/downloadTemplate?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>

</head>
<body>
<div class="bg-white item_title" style="padding-right: 0px;">
    人员培训管理
    <button type="button" class="btn btn-md1 btn-primary title-btn" data-toggle="modal" data-target="#importInfo" title="点击批量导入培训信息">导入培训信息</button>
</div>
<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">

        <form id="personnelTrainForm" class="form-horizontal">
            <div class="row form-group">

                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>姓名</label>
                        <input class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>培训地点</label>
                        <input class="form-control" id="place" name="place">
                    </div>
                </div>

                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>培训项目</label>
                        <select class="form-control input-sm select2" name="category" id="category">
                            <option value=""> 请选择</option>
                            <c:forEach items="${sessionScope.TRAIN_CATEGORY}" var="item">
                                <option value="${item.code}">${item.dictValue}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row form-group">

                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>培训开始日期</label>
                        <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">
                            <input readonly style="background-color: #fff;" class="form-control" id="startDate" name="startDate" type="text">
                            <span class="input-group-addon"> 至 </span>
                            <input readonly style="background-color: #fff;" class="form-control" id="endDate" name="endDate" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-item wide2">
                        <label>培训结束日期</label>
                        <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">
                            <input readonly style="background-color: #fff;" class="form-control" id="startDate1" name="startDate1" type="text">
                            <span class="input-group-addon"> 至 </span>
                            <input readonly style="background-color: #fff;" class="form-control" id="endDate1" name="endDate1" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 text-right">
                    <a href="javascript:;" class="ml5 btn btn-primary btn-md1" id="findPersonnelTrain">查询</a>
                    <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn">重置</button>
                    <a id="addTrain" href="<%=request.getContextPath() %>/train/toTrainAdd" class="ml5 btn btn-primary btn-md1" >新增</a>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="personnelTrainTable" class="table table-striped table-bordered display nowrap self_table" align="center" style="width:100%">
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">签到流水</h4>
            </div>
            <div class="modal-body">
                <table id="checkDocTable" class="table table-striped table-bordered display nowrap self_table" style="width: 100%;" align="center">
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<!-- 模态框（Modal） -->
<div class="modal fade" id="importInfo" tabindex="-1" role="dialog" aria-labelledby="importInfoLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="importInfoLabel">导入培训信息</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form_area" method="post" enctype="multipart/form-data" id="trainUploadForm">
                        <div class="col-md-8" style="padding-left: 8%;">
                            <input style="padding: 0;" type="file" name="file" id="trainFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        </div>
                        <div class="col-md-4">
                            <button type="button" onclick="uploadTrain()" class="btn btn-md1 btn-primary" title="点击批量导入培训信息">导&nbsp;&nbsp;入</button>
                            <button type="button" onclick="downloadTemplate()" class="ml5 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                        </div>
                        <div class="col-md-5 mt10" style="padding-left: 6%"><span class="label" id="resultMsg"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
</body>
</html>
