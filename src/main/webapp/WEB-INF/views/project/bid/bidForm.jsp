<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>标段管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null
            });

            $('#beginTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#saveBid').click(function () {

                var valid = $("#bidForm").validationEngine("validate");
                if (!valid)
                    return;
                disabledAllBtn(true);
                var param = serializeObject('#bidForm');
                var otherParam = {};
                var jsonString = JSON.stringify($.extend(param, otherParam));
                $.ajax({
                    contentType: "application/json",
                    url : '${ctx}/bid/saveBid',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);

                        }else{
                            notyError(data.resultMsg);
                            disabledAllBtn(false);
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                        disabledAllBtn(false);
                    }
                });

            });
        });

    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/bid/toBidList" title="点击返回至列表">标段管理</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        标段增加
    </c:if>
    <c:if test="${opt == 'E'}">
        标段编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="bidForm">
        <input type="hidden" name="id" value="${bid.id}" id="id">

        <input type="hidden" id="opt" name="opt" value="${opt}">

        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>排序</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,custom[number]]"  id = "sort"  name="sort" placeholder="" value="${bid.sort}" type="text">
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>标号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,custom[number]]"  id = "bidNo"  name="bidNo" placeholder="" value="${bid.bidNo}" type="text">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>标段名称</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,maxSize[50]]"  id = "name" name="name"  placeholder="" value="${bid.name}" type="text">
                </div>

                <label class="col-md-2 control-label">负责人</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "inCharge"  name="inCharge" placeholder="" value="${bid.inCharge}" type="text">
                </div>

            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>开始日期</label>
                <div class="col-md-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[required,custom[date],past[#endTime]]"  id = "beginTime"  name="beginTime" value="${bid.beginTime}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>


                <label class="col-md-2 control-label"><span class="alarmstar">*</span>结束日期</label>
                <div class="col-md-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[required,custom[date],future[#beginTime]]"  id = "endTime"  name="endTime" value="${bid.endTime}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>

            </div>
            <div class="row">
                <label class="col-md-2 control-label">甲方</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "partyA"  name="partyA" placeholder="" value="${bid.partyA}" type="text">
                </div>

                <label class="col-md-2 control-label">乙方</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "partyB"  name="partyB" placeholder="" value="${bid.partyB}" type="text">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">所属线路</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "lineNo"  name="lineNo" placeholder="" value="${bid.lineNo}" type="text"  readonly>
                </div>
            </div>
        </div>
        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveBid"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/bid/toBidList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
