<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>标段管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var bidTable ;
        $(function () {
            bidTable = $("#bidTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/bid/getBidList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'sort', 'sTitle': '排序', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'name', 'sTitle': '标段名称', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'lineNo', 'sTitle': '所属线路', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'inCharge', 'sTitle': '负责人', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'beginTime', 'sTitle': '开始日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'endTime', 'sTitle': '结束日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'},
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="'+ path +'/bid/toBidView?id='+ row.id +'" title="查看标段信息">查看</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/bid/toBidForm?opt=E&id='+ row.id +'" title="修改标段信息">编辑</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除标段信息" onclick="delBid(\''+ row.id + '\')">删除</a>';
                            return domHtml;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#bidForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val

                        });
                    });
                }

            });
            $("#findBid").click(function () {
                bidTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('bidForm');
            })
        });

        function delBid(id) {
            var r=confirm("确认删除？")
            if (r==false){
                return
            }
            var param = {
                id: id
            }

            $.ajax({
                contentType: "application/json",
                url : path+'/bid/deleteBid',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        bidTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

    </script>

</head>
<body>

<div class="bg-white item_title">
    标段管理
</div>
<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">
        <div class="row form-group">
            <form id="bidForm">
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>标段名称</label>
                        <input class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                    </div>
                </div>
                <div class="col-xs-6 text-right">
                    <a id="findBid" href="javascript:;" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-search"></i>&nbsp;&nbsp;--%>查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                    <a id="addBid" href="<%=request.getContextPath() %>/bid/toBidForm?opt=A" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-plus"></i>&nbsp;&nbsp;--%>新增</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="bidTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>

</body>
</html>
