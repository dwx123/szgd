<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>标段详情</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <script>
        $(function () {
        })
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/bid/toBidList" title="点击返回至列表">标段管理</a>
    <i class="fa fa-angle-double-right"></i>
    标段信息查看
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="bidForm">
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>排序</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,custom[number]]"  id = "sort"  name="sort" placeholder="" value="${bid.sort}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>标号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[custom[number]]"  id = "bidNo"  name="bidNo" placeholder="" value="${bid.bidNo}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>标段名称</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,maxSize[50]]"  id = "name" name="name"  placeholder="标段名称" value="${bid.name}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">负责人</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "inCharge"  name="inCharge" placeholder="负责人" value="${bid.inCharge}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">开始日期</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "beginTime"  name="beginTime" placeholder="施工开始日期" value="${bid.beginTime}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">结束日期</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "endTime"  name="endTime" placeholder="施工结束日期" value="${bid.endTime}" type="text" readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">甲方</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "partyA"  name="partyA" placeholder="甲方" value="${bid.partyA}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">乙方</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "partyB"  name="partyB" placeholder="乙方" value="${bid.partyB}" type="text" readonly>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">所属线路</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "lineNo"  name="lineNo" placeholder="所属线路" value="${bid.lineNo}" type="text"  readonly>
                </div>
            </div>
        </div>

        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <a href="${ctx}/bid/toBidList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>
                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
