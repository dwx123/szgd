<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>项目信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        var isProjectAttachChanged = '0';
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $('#beginTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            function getProjectAttach() {
                var obj = {
                    id:$("#projectAttachId").val(),
                    parentId:$('#id').val(),
                    name:$("#projectAttachName").val(),
                    type: "ATTACH_DOC",
                    source: "ATTACH_PROJECT",
                    isProjectAttachChanged :isProjectAttachChanged
                };
                return obj;
            }

            $('#saveProjectInfo').click(function () {
                var valid = $("#projectInfoForm").validationEngine("validate");
                if (!valid)
                    return;
                var param = serializeObject('#projectInfoForm');
                var otherParam = {};
                otherParam['projectAttach'] = getProjectAttach();
                var jsonString = JSON.stringify($.extend(param, otherParam));
                console.log(jsonString);
                $.ajax({
                    contentType : "application/json",
                    url : '${ctx}/project/saveProjectInfo',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);
                        }else{
                            notyError(data.resultMsg);
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                    }
                });
            });
        });

        function uploadProjectAttach(e) {
            var reader = new FileReader()
            reader.readAsDataURL(event.target.files[0])
            reader.onloadend = function () {
                var fileId = [];
                var rawId = e.id
                fileId.push(e.id)
                $.ajaxFileUpload({
                    url: "<%=request.getContextPath() %>/upload/uploadFile",
                    type: 'post',
                    secureuri: false,
                    fileElementId: fileId,
                    dataType: 'json',
                    success: function (data, status) {
                        if (data.resultCode == 1) {
                            $("#projectAttachName").val(data.data[0]);
                            isProjectAttachChanged = '1';
                        } else {
                            notyError(data.resultMsg);
                        }
                    },
                    error: function (data, status, e) {
                        console.log(e)
                        notyError(e);
                    }
                });
            }
        }

        function downloadProjectAttach() {
            var attachId = $('#projectAttachId').val();
            if((attachId==null||attachId==""||attachId=="null")){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+$("#projectAttachPath").val()+'&fileName='+$("#attachName").val();
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/project/toProjectInfoList" title="点击返回至列表">项目信息管理</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        项目信息新增
    </c:if>
    <c:if test="${opt == 'E'}">
        项目信息编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="projectInfoForm">
        <input type="hidden" name="id" value="${project.id}" id="id">
        <input type="hidden" id="opt" name="opt" value="${opt}">

        <div class="form-body mb10">
            <div class="form-group">
                <label class="col-md-1 control-label"><span class="alarmstar">*</span>项目编号</label>
                <div class="col-md-3">
                    <input class="form-control input-sm validate[required,maxSize[50]]"  id = "number" name="number"  placeholder="项目编号" value="${project.number}" type="text">
                </div>

                <label class="col-md-1 control-label"><span class="alarmstar">*</span>项目名称</label>
                <div class="col-md-3">
                    <input class="form-control input-sm validate[required,maxSize[100]]"  id = "name"  name="name" placeholder="项目名称" value="${project.name}" type="text">
                </div>

                <label class="col-md-1 control-label">联系人</label>
                <div class="col-md-3">
                    <input class="form-control input-sm validate[maxSize[50]]"  id = "inCharge"  name="inCharge" placeholder="联系人" value="${project.inCharge}" type="text">
                </div>

            </div>
            <div class="form-group">
                <label class="col-md-1 control-label">甲方</label>
                <div class="col-md-3">
                    <input class="form-control input-sm validate[maxSize[50]]"  id = "partyA"  name="partyA" placeholder="甲方" value="${project.partyA}" type="text">
                </div>

                <label class="col-md-1 control-label">乙方</label>
                <div class="col-md-3">
                    <input class="form-control input-sm validate[maxSize[50]]"  id = "partyB"  name="partyB" placeholder="乙方" value="${project.partyB}" type="text">
                </div>


                <label class="col-md-1 control-label">项目金额</label>
                <div class="col-md-3">
                    <input class="form-control input-sm validate[custom[number]]"  id = "money"  name="money" placeholder="项目金额" value="${project.money}" type="text">
                </div>

            </div>
            <div class="form-group">
                <label class="col-md-1 control-label">项目开始日期</label>
                <div class="col-md-3">
                    <input style="background-color: #fff;" class="form-control input-sm validate[custom[date],past[#endTime]]"  id = "beginTime"  name="beginTime" placeholder="项目开始日期" value="${project.beginTime}" type="text" readonly>
                </div>

                <label class="col-md-1 control-label">项目结束日期</label>
                <div class="col-md-3">
                    <input style="background-color: #fff;" class="form-control input-sm validate[custom[date],future[#beginTime]]"  id = "endTime"  name="endTime" placeholder="项目结束日期" value="${project.endTime}" type="text" readonly>
                </div>

                <label class="col-md-1 control-label">合同编号</label>
                <div class="col-md-3">
                    <input class="form-control input-sm validate[maxSize[50]]"  id = "contractNumber"  name="contractNumber" placeholder="合同编号" value="${project.contractNumber}" type="text">
                </div>

            </div>
            <div class="form-group">
                <label class="col-md-1 control-label">合同名称</label>
                <div class="col-md-3">
                    <input class="form-control input-sm validate[maxSize[100]]"  id = "contractName"  name="contractName" placeholder="合同名称" value="${project.contractName}" type="text">
                </div>

                <label class="col-md-1 control-label">是否为重点项目</label>
                <div class="col-md-3">
                    <select  class="form-control input-sm select2" id = 'isKey' name="isKey">
                        <option value="">请选择</option>
                        <option value="0" <c:if test="${project.isKey eq 0}">selected</c:if>>否</option>
                        <option value="1" <c:if test="${project.isKey eq 1}">selected</c:if>>是</option>
                    </select>
                </div>

                <label class="col-md-1 control-label">项目附件</label>
                <div id="projectAttach" class="col-md-3">
                    <input type="hidden" name="projectAttachId" value="${project.attachId}" id="projectAttachId">
                    <input type="hidden" name="projectAttachPath" value="${project.path}" id="projectAttachPath">
                    <input type="hidden" name="attachName" value="${project.attachName}" id="attachName">

                    <input class="form-control form-control-inline input-sm" id = "projectAttachName" name="projectAttachName" size="16" value="${project.attachName}" type="text" readonly>
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn-md btn btn-primary attachBtnHalf">上传附件</button>
                    <input class="attachHalf" type="file" id="uploadAttach"  name="file" accept="" onchange="uploadProjectAttach(this)">
                    <c:if test="${opt == 'E'}">
                        <button id="downloadBtn" type="button" onclick="downloadProjectAttach()" class="btn-md btn btn-primary attachBtnHalf">下载附件</button>
                    </c:if>
                </div>
            </div>
        </div>
        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveProjectInfo"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/project/toProjectInfoList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>
</body>
</html>
