<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>项目信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <%@ include file="/common/include-css.jsp"%>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var isProjectAttachChanged = '0';
        var projectInfoTable ;
        $(function () {
            projectInfoTable = $("#projectInfoTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/project/getProjectList",//获取列表数据url,
                sServerMethod: "GET",
                //锁定行
                scrollY:"400px",
                //锁定列
                scrollX:true,
                scrollCollapse: true,
                fixedColumns: {
                    leftColumns: 1,
                    rightColumns:1
                },
                //table 表头
                aoColumns: [
                    {'mDataProp': 'number', 'sTitle': '项目编号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'name', 'sTitle': '项目名称', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'inCharge', 'sTitle': '联系人', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'partyA', 'sTitle': '甲方', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'partyB', 'sTitle': '乙方', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'beginTime', 'sTitle': '项目开始日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'endTime', 'sTitle': '项目结束日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'money', 'sTitle': '项目金额', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'contractNumber', 'sTitle': '合同编号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'contractName', 'sTitle': '合同名称', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'isKey', 'sTitle': '是否为重点项目', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'stageName', 'sTitle': '项目阶段', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'completion', 'sTitle': '项目完成百分比', 'sWidth': '10%', 'sClass': 'center'},
                    <c:if test="${from ne 'home'}">
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
                    </c:if>
                ],
                //渲染某列格式
                aoColumnDefs: [
                    <c:if test="${from ne 'home'}">
                        {
                            "targets":-1,
                            "bSortable": false,
                            mRender: function (data, type, row) {
                                var domHtml = "";
                                domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="'+ path +'/project/toProjectInfoView?id='+ row.id +'" title="查看车辆信息">查看</a>&nbsp;&nbsp;';
                                domHtml += '<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/project/toProjectInfoForm?opt=E&id='+ row.id +'" title="修改车辆信息">编辑</a>&nbsp;&nbsp;';
                                domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除项目信息" onclick="delProjectInfo(\''+ row.id + '\')">删除</a>&nbsp;&nbsp;';
                                domHtml += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="项目进度维护" onclick="projectSchedule(\''+ row.id + '\',\''+ row.name + '\',\''+ row.projectStage + '\',\''+ row.completion + '\',\''+ row.remark + '\',\''+ row.sid + '\')">项目进度维护</a>&nbsp;&nbsp;';
                                domHtml += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="项目进度维护" onclick="projectScheduleConfig(\''+ row.id + '\',\''+ row.name + '\',\''+ row.projectStage + '\',\''+ row.completion + '\',\''+ row.remark + '\',\''+ row.sid + '\')">项目进度配置</a>&nbsp;&nbsp;';

                                /*domHtml += '<a class="btn-sm btn-smd btn btn-info" href="javascript:;" title="上传项目附件" onclick="uploadProjectAttach(\''+ row.id + '\',\''+ row.attachId +'\',\''+ row.attachName +'\',\''+ row.path +'\')">上传项目附件</a>&nbsp;&nbsp;';
                                domHtml += '<a class="btn-sm btn-smd btn btn-info" href="javascript:;" title="下载项目附件" onclick="downloadProjectAttach(\''+ row.attachName +'\',\''+ row.path +'\')">下载项目附件</a>';*/
                                return domHtml;
                            }
                        },
                    </c:if>
                    {
                        aTargets: [10],
                        mRender: function (data, type, row) {
                            var isKey ;
                            if (data == 0) {
                                isKey = '否';
                            }
                            if (data == 1) {
                                isKey = '是';
                            }
                            return isKey;

                        }
                    },
                    {
                        aTargets: [12],
                        mRender: function (data, type, row) {
                            var completion;
                            if(data == null || data == ""){
                                completion = "";
                            }else{
                                completion = data + "%" ;
                            }
                            return completion;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#projectInfoForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val

                        });
                    });
                }

            });
            $("#findProjectInfo").click(function () {
                projectInfoTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('projectInfoForm');
            })
        });

        function projectSchedule(id,name,projectStage,completion,remark,sid) {
            openWin('#schedule', path + '/projectSchedule/scheduleModel', {"id": id,"name": name,"projectStage": projectStage,"completion": completion,"remark": remark,"sid": sid});
        }

        function projectScheduleConfig(id,name,projectStage,completion,remark,sid) {
            //openWin('#schedule', path + '/projectSchedule/scheduleModel', {"id": id,"name": name,"projectStage": projectStage,"completion": completion,"remark": remark,"sid": sid});
            gotoMenu(1,'${ctx}/projectScheduleConfig/toProjectScheduleConfigList?projectId='+id)
            /*var  queryParams = 'projectId='+id;
            var url ="${ctx}/projectScheduleConfig/toProjectScheduleConfigList?projectId="+id;
            window.open(url);*/
        }

        function downloadProjectAttach(attachName,attachpath) {
            if(attachpath=="null"||attachpath==null||attachpath==""){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+attachpath+'&fileName='+attachName;
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }

        function uploadProjectAttach(id,attachId,attachName,path) {
            $('#myModal').modal('show');
            $('#projectAttachId').val(attachId);
            $('#attachName').val(attachName);
            $('#projectAttachPath').val(path);
            $('#id').val(id);
            if(attachName!=null&&attachName!=""&&attachName!="null"){
                $('#projectAttachName').val(attachName);
            }
        }

        function uploadAttach(e) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onloadend = function () {
                var fileId = [];
                var rawId = e.id;
                fileId.push(e.id);
                $.ajaxFileUpload({
                    url: "<%=request.getContextPath() %>/upload/uploadFile",
                    type: 'post',
                    secureuri: false,
                    fileElementId: fileId,
                    dataType: 'json',
                    success: function (data, status) {
                        if (data.resultCode == 1) {
                            $("#projectAttachName").val(data.data[0]);
                            isProjectAttachChanged = '1';
                        } else {
                            notyError(data.resultMsg);
                        }
                    },
                    error: function (data, status, e) {
                        console.log(e);
                        notyError(e);
                    }
                });
            }
        }

        function postAttach() {
            var param = {
                id:$("#projectAttachId").val(),
                parentId:$('#id').val(),
                name:$("#projectAttachName").val(),
                type: "ATTACH_DOC",
                source: "ATTACH_PROJECT",
                isProjectAttachChanged :isProjectAttachChanged
            };
            console.log(param);
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/project/saveAttach',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        $('#myModal').modal('hide');
                        projectInfoTable.ajax.reload();

                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error));
                }
            });
        }

        function delProjectInfo(id) {
            var r=confirm("确认删除？")
            if (r==false){
                return
            }
            var param = {
                id: id
            }

            $.ajax({
                contentType: "application/json",
                url : '${ctx}/project/deleteProject',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        projectInfoTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

        function checkUploadProjectInfoForm() {
            var fileDir = $("#projectInfoFile").val();
            if ("" == fileDir) {
                notyWarning("选择需要导入的文件！");
                return false;
            }
            var suffix = fileDir.substr(fileDir.lastIndexOf("."));
            if (".xls" != suffix && ".xlsx" != suffix) {
                notyWarning("选择Excel格式的文件导入！");
                return false;
            }
            return true;
        }
        function uploadProjectInfo() {
            if (checkUploadProjectInfoForm()) {
                function resutlMsg(msg) {
                    if (msg =="文件导入失败！"){
                        $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                    }else{
                        if(msg.indexOf("#WEBROOT#") != -1){
                            msg  = msg.replace("#WEBROOT#", path +"/upload");
                        }
                        $("#resultMsg").html("<font color='#006400'>" + msg +"</font>");
                    }
                    $("#file").val("");
                    $("#projectInfoTable").DataTable().ajax.reload();
                }
                function errorMsg() {
                    $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                }
                $('#projectInfoUploadForm').ajaxSubmit({
                    url: path + '/project/batchUploadProjectInfo',
                    dataType: 'text',
                    success: resutlMsg,
                    error: errorMsg
                });
            }
        }

        function addSelectListener() {
            //找到列表中的数据行
            var rows = $("#projectInfoTable tr");
            $.each(rows, function (index, row) {
                if (index > 0) {
                    $(row).click(
                        function () {
                            selectedData = $('#projectInfoTable').dataTable().fnGetData(this);
                            $("#choicedUserName").html("<strong>" + selectedData.USER_NAME + "</strong> [" + selectedData.LOGIN_ID + "]");
                            $(row).addClass("info");
                            $(row).siblings().removeClass("info");
                        });
                }
            });
        }

        function downloadTemplate() {
            var  queryParams = 'templateName=项目信息批量导入模版.xls';
            var url =path + "/upload/downloadTemplate?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>

</head>
<body>
<c:if test="${from ne 'home'}">
    <div class="bg-white item_title">
        项目信息管理
    </div>
</c:if>

<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">
        <c:if test="${from ne 'home'}">
            <div class="row form-group">
                <form class="form_area" method="post" enctype="multipart/form-data" id="projectInfoUploadForm">
                    <div class="col-xs-4">
                        <div class="form-item wide2">
                            <label class="control-label">请选择文件</label>
                            <input type="file" name="file" id="projectInfoFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <button type="button" onclick="uploadProjectInfo()" class="ml15 btn btn-md1 btn-primary" title="点击批量导入项目信息">导入项目信息</button>
                        <button type="button" onclick="downloadTemplate()" class="ml15 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                    </div>
                    <div class="col-xs-5">
                        &nbsp;&nbsp;&nbsp;&nbsp;<span class="label" id="resultMsg"></span>
                    </div>
                </form>
            </div>
        </c:if>

        <div class="row form-group">
            <form id="projectInfoForm">
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>项目编号</label>
                        <input class="form-control validate[maxSize[50]]" id="number" name="number">
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>联系人</label>
                        <input class="form-control validate[maxSize[50]]" id="inCharge" name="inCharge">
                    </div>
                </div>
                <div class="col-xs-6 text-right">
                    <a id="findProjectInfo" href="javascript:;" class="ml15 btn btn-primary btn-md1" >查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                    <c:if test="${from ne 'home'}">
                        <a id="addProjectInfo" href="<%=request.getContextPath() %>/project/toProjectInfoForm?opt=A" class="ml15 btn btn-primary btn-md1">新增</a>
                    </c:if>

                </div>
            </form>
        </div>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="projectInfoTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>
<!-- Modal1 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">上传项目附件</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="scheduleForm" >
                    <input type="hidden" name="projectAttachId" value="" id="projectAttachId">
                    <input type="hidden" name="projectAttachPath" value="" id="projectAttachPath">
                    <input type="hidden" name="attachName" value="" id="attachName">
                    <input type="hidden" name="id" value="" id="id">
                    <div class="row">
                        <label class="col-md-2 control-label">项目附件</label>
                        <div id="projectAttach" class="col-md-7">
                            <input class="form-control form-control-inline input-sm" id = "projectAttachName" name="projectAttachName" size="16" value="" type="text" readonly>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn-md btn btn-primary attachBtn">上传附件</button>
                            <input class="attach" type="file" id="uploadAttachRow"  name="file" accept="" onchange="uploadAttach(this)">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-md btn btn-primary" onclick="postAttach()">提交</button>
                <button type="button" class="btn btn-default btn-md" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
<!-- ModalOfSchedule -->
<div id="schedule" class="modal fade"></div>
</body>
</html>
