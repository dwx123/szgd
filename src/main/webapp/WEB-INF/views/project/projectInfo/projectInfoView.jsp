<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>项目信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {
        });

        function downloadProjectAttach() {
            var attachId = $("#projectAttachId").val();
            if(attachId==null||attachId==""||attachId=="null"){
                alert("暂无附件");
                return;
            }
            var  queryParams = 'path='+$("#projectAttachPath").val()+'&fileName='+$("#attachName").val();
            var url =path + "/upload/downloadFile?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/project/toProjectInfoList" title="点击返回至列表">项目信息管理</a>
    <i class="fa fa-angle-double-right"></i>
    项目信息查看
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="projectInfoForm">
        <input type="hidden" name="id" value="${project.id}" id="id">

        <input type="hidden" id="opt" name="opt" value="${opt}">
        <div class="form-body mb10">
            <div class="form-group">
                <label class="col-md-1 control-label">项目编号</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "number" name="number"  placeholder="项目编号" value="${project.number}" type="text" readonly>
                </div>

                <label class="col-md-1 control-label">项目名称</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "name"  name="name" placeholder="项目名称" value="${project.name}" type="text"  readonly>
                </div>

                <label class="col-md-1 control-label">联系人</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "inCharge"  name="inCharge" placeholder="联系人" value="${project.inCharge}" type="text"  readonly>
                </div>

            </div>
            <div class="form-group">
                <label class="col-md-1 control-label">甲方</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "partyA"  name="partyA" placeholder="甲方" value="${project.partyA}" type="text"  readonly>
                </div>

                <label class="col-md-1 control-label">乙方</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "partyB"  name="partyB" placeholder="乙方" value="${project.partyB}" type="text" readonly>
                </div>


                <label class="col-md-1 control-label">项目金额</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "money"  name="money" placeholder="项目金额" value="${project.money}" type="text" readonly>
                </div>

            </div>
            <div class="form-group">
                <label class="col-md-1 control-label">项目开始日期</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "beginTime"  name="beginTime" placeholder="项目开始日期" value="${project.beginTime}" type="text" readonly>
                </div>

                <label class="col-md-1 control-label">项目结束日期</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "endTime"  name="endTime" placeholder="项目结束日期" value="${project.endTime}" type="text" readonly>
                </div>

                <label class="col-md-1 control-label">合同编号</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "contractNumber"  name="contractNumber" placeholder="合同编号" value="${project.contractNumber}" type="text" readonly>
                </div>

            </div>
            <div class="form-group">
                <label class="col-md-1 control-label">合同名称</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "contractName"  name="contractName" placeholder="合同名称" value="${project.contractName}" type="text" readonly>
                </div>

                <label class="col-md-1 control-label">是否为重点项目</label>
                <div class="col-md-3">
                    <select  class="form-control input-sm" id = 'isKey' name="isKey" disabled>
                        <option value="0" <c:if test="${project.isKey eq 0}">selected</c:if>>否</option>
                        <option value="1" <c:if test="${project.isKey eq 1}">selected</c:if>>是</option>
                    </select>
                </div>

                <label class="col-md-1 control-label">项目附件</label>
                <div id="projectAttach" class="col-md-3">
                    <input type="hidden" name="projectAttachId" value="${project.attachId}" id="projectAttachId">
                    <input type="hidden" name="projectAttachPath" value="${project.path}" id="projectAttachPath">
                    <input type="hidden" name="attachName" value="${project.attachName}" id="attachName">

                    <button type="button" id="downloadAttach" onclick="downloadProjectAttach()" class="btn-md btn btn-primary attachBtn">下载附件</button>
                </div>
            </div>
        </div>

        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <a href="${ctx}/project/toProjectInfoList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>
                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
