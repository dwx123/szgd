<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <title>
        项目进度维护
    </title>
    <meta charset="utf-8"/>
</head>
<body>
<div class="opend_modal">
    <div class="modal-dialog" style="width: 400px;">
        <div class="modal-content">
            <div class="modal-header">
                <a data-dismiss="modal"><i class="fa fa-close"></i> </a> <b>
                项目进度维护
            </b>
            </div>
            <div class="modal-body container-fluid">
                <form method="post" id="scheduleForm" >
                    <input type="hidden" name="projectId" value="${id}"/>
                    <input type="hidden" name="id" value="${sid}"/>

                    <div class="form-group row">
                        <label class="col-xs-4 control-label">项目名称：</label>
                        <div class="col-xs-8">
                            <input class="form-control form-control-inline input-sm" id = "name" name="name" size="16" value="${name}" type="text" readonly>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-xs-4 control-label">项目阶段：</label>
                        <div class="col-xs-8">
                            <select class="form-control input-sm" name="projectStage" id="projectStage">
                                <option value=""> 请选择</option>
                                <c:forEach items="${sessionScope.PROJECT_STAGE}" var="item">
                                    <c:choose>
                                        <c:when test="${item.code eq projectStage}">
                                            <option title="${item.otherValue}" value="${item.code}" selected>${item.dictValue}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${item.code}" title="${item.otherValue}">${item.dictValue}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-xs-4 control-label">完成百分比：</label>
                        <div class="col-xs-8">
                            <input class="form-control form-control-inline input-sm" id = "completion" name="completion" size="16" value="${completion}" type="text">
                        </div>
                    </div>

                    <div class="form-group" style="height: 75px;">
                        <label class="col-xs-4 control-label">说明：</label>
                        <div class="col-xs-8">
                            <textarea style="height: 70px;" class="form-control" placeholder="(添加相关描述说明)" id = "remark" name="remark" rows="3">${remark}</textarea>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer text-center">
                <input type="button" value="提交" onclick="save();" class="btn btn-md btn-primary">
            </div>
        </div>
    </div>
</div>




<script>

    function save(){
        var standard = $("#projectStage option:selected").attr("title");
        var completion = $('#completion').val();
        console.log(standard+","+completion);
        if(parseInt(completion) > parseInt(standard)){
            alert("最大百分比不能超过"+standard+"%");
            return false;
        }
        var action = path + "/projectSchedule/saveSchedule";
        $('#scheduleForm').attr('action',action);
        $('#scheduleForm').submit();
    }

</script>
</body>
</html>