<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>项目进度配置</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {

            $('#completeTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#saveProjectScheduleConfig').click(function () {
                var valid = $("#projectScheduleConfigForm").validationEngine("validate");
                if (!valid)
                    return;
                var param = serializeObject('#projectScheduleConfigForm');
                var otherParam = {};
                var jsonString = JSON.stringify($.extend(param, otherParam));
                $.ajax({
                    contentType : "application/json",
                    url : '${ctx}/projectScheduleConfig/saveProjectScheduleConfig',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);
                        }else{
                            notyError(data.resultMsg);
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                    }
                });
            });
        });

        function fillOtherInfo() {
            var projectId = $("#projectScheduleConfigForm select[name=projectId]").val();
            var params = {
                'id' : projectId
            };
            $.ajax({
                contentType : "application/json",
                type : 'POST',
                url : '${ctx}/project/getProjectById',
                data : JSON.stringify(params),
                success : function (res) {
                    console.log(res);
                    if(res.resultCode == 1){
                        $("#projectNumber").val(res.data.number);
                    }else{
                        notyError("项目信息请求失败!");
                    }
                },
                error: function () {
                    notyError("项目信息请求失败!");
                }
            });
        }

    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/projectScheduleConfig/toProjectScheduleConfigList" title="点击返回至列表">项目进度配置</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        项目进度配置新增
    </c:if>
    <c:if test="${opt == 'E'}">
        项目进度配置编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="projectScheduleConfigForm">
        <input type="hidden" name="id" value="${projectScheduleConfig.id}" id="id">
        <input type="hidden" id="opt" name="opt" value="${opt}">

        <h4><i class="fa fa-bars"></i> 项目</h4>
        <div class="form-body mb10">
            <div class="form-group">
                <label class="col-md-1 control-label"><span class="alarmstar">*</span>项目名称</label>
                <div class="col-md-2">
                    <c:if test="${opt == 'A'}">
                        <select class="form-control input-sm select2 validate[required]" name="projectId" id="projectId" onchange = "fillOtherInfo();">
                    </c:if>
                    <c:if test="${opt == 'E'}">
                         <select disabled class="form-control input-sm select2 validate[required]" name="projectId" id="projectId" onchange = "fillOtherInfo();">
                    </c:if>
                        <option value="">请选择</option>
                        <c:forEach items="${projectList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq projectScheduleConfig.projectId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-1 control-label">项目编号</label>
                <div class="col-md-2">
                    <input readonly class="form-control input-sm"  id = "projectNumber"  name="projectNumber" placeholder="项目编号" value="${projectScheduleConfig.projectNumber}" type="text">
                </div>

                <label class="col-md-1 control-label"></label>
                <div class="col-md-2">

                </div>

                <label class="col-md-1 control-label"></label>
                <div class="col-md-2">

                </div>
            </div>
        </div>

        <h4><i class="fa fa-bars"></i> 项目进度</h4>
        <div class="form-body mb10">
            <div class="form-group">
                <label class="col-xs-1 control-label"><span class="alarmstar">*</span>项目阶段</label>
                <div class="col-xs-3">
                    <select class="form-control input-sm select2 validate[required]" name="projectStage" id="projectStage">
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.PROJECT_STAGE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq projectScheduleConfig.projectStage}">
                                    <option title="${item.otherValue}" value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}" title="${item.otherValue}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-xs-1 control-label"><span class="alarmstar">*</span>完成百分比</label>
                <div class="col-xs-3">
                    <input class="form-control form-control-inline input-sm  validate[required]" id = "completion" name="completion" size="16" value="${projectScheduleConfig.completion}" type="text">
                </div>

                <label class="col-xs-1 control-label">完成日期</label>
                <div class="col-xs-3">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[future[#completeTime]]"  id = "completeTime"  name="completeTime" placeholder="完成日期" value="${projectScheduleConfig.completeTime}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>
                <label class="col-xs-1 control-label">提前时限</label>
                <div class="col-xs-3">
                    <div class="input-group-date">
                        <select class="form-control input-sm select2" id = "beforeDay" name="beforeDay">
                            <option value="">请选择</option>
                            <option value="15" <c:if test="${15 == projectScheduleConfig.beforeDay }"> selected</c:if>>提前半个月</option>
                            <option value="30" <c:if test="${30 == projectScheduleConfig.beforeDay }"> selected</c:if>>提前一个月</option>
                            <option value="60" <c:if test="${60 == projectScheduleConfig.beforeDay }"> selected</c:if>>提前二个月</option>
                            <option value="90" <c:if test="${90 == projectScheduleConfig.beforeDay }"> selected</c:if>>提前三个月</option>
                            <option value="180" <c:if test="${180 == projectScheduleConfig.beforeDay }"> selected</c:if>>提前半年</option>
                            <option value="360" <c:if test="${360 == projectScheduleConfig.beforeDay }"> selected</c:if>>提前一年</option>

                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveProjectScheduleConfig"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/projectScheduleConfig/toProjectScheduleConfigList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>
</body>
</html>
