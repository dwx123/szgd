<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>项目进度配置</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var projectScheduleConfigTable ;
        $(function () {

            projectScheduleConfigTable = $("#projectScheduleConfigTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/projectScheduleConfig/getProjectScheduleConfigList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'projectName', 'sTitle': '项目名称', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'projectStageName', 'sTitle': '项目阶段', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'completion', 'sTitle': '完成百分比', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'completeTime', 'sTitle': '完成日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'beforeDay', 'sTitle': '提醒时限(天)', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '5%', 'sClass': 'center'}

                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        aTargets: [2],
                        mRender: function (data, type, row) {
                            var completion;
                            if(data == null || data == ""){
                                completion = "";
                            }else{
                                completion = data + "%" ;
                            }
                            return completion;
                        }
                    },
                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/projectScheduleConfig/toProjectScheduleConfigForm?opt=E&id='+ row.id +'" title="编辑">编辑</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除" onclick="delItem(\''+ row.id + '\')">删除</a>';

                            return domHtml;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#projectScheduleConfigForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });

                    var formSelectArray = $("#projectScheduleConfigForm select");
                    $.each(formSelectArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val

                        });
                    });
                }

            });
            $("#findScheduleConfig").click(function () {
                projectScheduleConfigTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('projectScheduleConfigForm')
            });


        });

        function delItem(id) {
            var r=confirm("确认删除?");
            if (r==false){
                return;
            }
            var param = {
                id: id
            };

            $.ajax({
                contentType: "application/json",
                url : '${ctx}/projectScheduleConfig/deleteProjectScheduleConfig',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        projectScheduleConfigTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }
    </script>

</head>
<body>

<div class="bg-white item_title">
    项目进度配置
</div>
<div class="bg-white">
    <form id="projectScheduleConfigForm" class="form_search form_table container-fluid">
        <div class="row form-group">
            <div class="col-xs-3">
                <div class="form-item wide2">
                    <label>项目</label>
                    <select class="form-control input-sm select2" name="projectId" id="projectId">
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.projectList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq projectId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>

                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-item wide2">

                </div>
            </div>

            <div class="col-xs-3 text-right">
                <a href="javascript:;" class="ml5 btn btn-primary btn-md1" id="findScheduleConfig">查询</a>&nbsp;&nbsp;
                <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn">重置</button>
                <a href="<%=request.getContextPath() %>/projectScheduleConfig/toProjectScheduleConfigForm?opt=A" class="ml15 btn btn-primary btn-md1">新增</a>

            </div>
        </div>

    </form>
</div>
<div class="bg-white pd_item">
    <table id="projectScheduleConfigTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>

</body>
</html>
