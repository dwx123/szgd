<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>站点管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null
            });

            $('#beginTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#saveSite').click(function () {

                var valid = $("#siteForm").validationEngine("validate");
                if (!valid)
                    return;
                disabledAllBtn(true);
                var param = serializeObject('#siteForm');
                var otherParam = {};
                var jsonString = JSON.stringify($.extend(param, otherParam));
                $.ajax({
                    contentType: "application/json",
                    url : '${ctx}/site/saveSite',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);

                        }else{
                            notyError(data.resultMsg);
                            disabledAllBtn(false);
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                        disabledAllBtn(false);
                    }
                });

            });
        });

    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/site/toSiteList" title="点击返回至列表">站点管理</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        站点增加
    </c:if>
    <c:if test="${opt == 'E'}">
        站点编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="siteForm">
        <input type="hidden" name="id" value="${site.id}" id="id">

        <input type="hidden" id="opt" name="opt" value="${opt}">

        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>排序</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,custom[number]]"  id = "sort"  name="sort" placeholder="" value="${site.sort}" type="text">
                </div>

                <label class="col-md-2 control-label"></label>
                <div class="col-md-3 pt7">

                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>所属标段</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2 validate[required]" name="bidId" id="bidId">
                        <option value=""> 请选择</option>
                        <c:forEach items="${bidList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq site.bidId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>站点名称</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,maxSize[50]]"  id = "name" name="name"  placeholder="" value="${site.name}" type="text">
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>是否开工</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]" id = 'isStarted' name="isStarted">
                        <option value="">请选择</option>
                        <option value="0" <c:if test="${site.isStarted eq 0}">selected</c:if>>否</option>
                        <option value="1" <c:if test="${site.isStarted eq 1}">selected</c:if>>是</option>
                    </select>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>是否完工</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]" id = 'isFinished' name="isFinished">
                        <option value="">请选择</option>
                        <option value="0" <c:if test="${site.isFinished eq 0}">selected</c:if>>否</option>
                        <option value="1" <c:if test="${site.isFinished eq 1}">selected</c:if>>是</option>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>开工顺序</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,custom[number]]"  id = "startSort"  name="startSort" placeholder="" value="${site.startSort}" type="text">
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>是否默认大屏显示</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]" id = 'defaultShow' name="defaultShow">
                        <option value="">请选择</option>
                        <option value="0" <c:if test="${site.defaultShow eq 0}">selected</c:if>>否</option>
                        <option value="1" <c:if test="${site.defaultShow eq 1}">selected</c:if>>是</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">站点开始日期</label>
                <div class="col-md-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[custom[date],past[#endTime]]"  id = "beginTime"  name="beginTime" value="${site.beginTime}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>


                <label class="col-md-2 control-label">站点结束日期</label>
                <div class="col-md-3 pt7">
                    <div class="input-group-date">
                        <input readonly class="dateStyle validate[custom[date],future[#beginTime]]"  id = "endTime"  name="endTime" value="${site.endTime}" type="text">
                        <span class="input-group-addon font13"><span class="glyphicon glyphicon-remove"></span></span>
                    </div>
                </div>

            </div>
            <div class="row">
                <label class="col-md-2 control-label">面积</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[custom[number]]"  id = "area"  name="area" placeholder="" value="${site.area}" type="text">
                </div>

                <label class="col-md-2 control-label">所属线路</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[custom[number]]"  id = "lineNo"  name="lineNo" placeholder="" value="${site.lineNo}" type="text"  readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">站点地址</label>
                <div class="col-md-8 pt7">
                    <input class="form-control input-sm"  id = "address"  name="address" placeholder="" value="${site.address}" type="text">
                </div>
            </div>
        </div>
        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveSite"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/site/toSiteList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
