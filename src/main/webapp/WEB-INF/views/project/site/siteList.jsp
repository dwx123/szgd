<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>站点管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var siteTable ;
        $(function () {
            siteTable = $("#siteTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/site/getSiteList",//获取列表数据url,
                sServerMethod: "GET",
                //锁定行
                scrollY:"450px",
                //锁定列
                scrollX:true,
                scrollCollapse: true,
                fixedColumns: {
                    leftColumns: 1,
                    rightColumns:1
                },
                //table 表头
                aoColumns: [
                    {'mDataProp': 'sort', 'sTitle': '排序', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'name', 'sTitle': '站点名称', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'address', 'sTitle': '站点地址', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'area', 'sTitle': '面积', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'isStarted', 'sTitle': '是否开工', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'isFinished', 'sTitle': '是否完工', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'startSort', 'sTitle': '开工顺序', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'bidName', 'sTitle': '所属标段', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'defaultShow', 'sTitle': '默认大屏显示', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'beginTime', 'sTitle': '工地开始日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'endTime', 'sTitle': '工地结束日期', 'sWidth': '10%', 'sClass': 'center'},

                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'},
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {   aTargets: [3],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var html = "" ;
                            if (data == 0) {
                                html = '否';
                            }
                            if (data == 1) {
                                html = '是';
                            }
                            return html;

                        }
                    },
                    {   aTargets: [4],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var html = "" ;
                            if (data == 0) {
                                html = '否';
                            }
                            if (data == 1) {
                                html = '是';
                            }
                            return html;

                        }
                    },
                    {   aTargets: [7],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var html = "" ;
                            if (data == 0) {
                                html = '否';
                            }
                            if (data == 1) {
                                html = '是';
                            }
                            return html;

                        }
                    },
                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="'+ path +'/site/toSiteView?id='+ row.id +'" title="查看站点信息">查看</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/site/toSiteForm?opt=E&id='+ row.id +'" title="修改站点信息">编辑</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除站点信息" onclick="delSite(\''+ row.id + '\')">删除</a>&nbsp;&nbsp;';

                            domHtml += '<a class="btn-sm btn-smd btn btn-warning" href="javascript:;" title="上传摄像头布局图" onclick="uploadLayoutPic(\''+ row.id + '\')">上传摄像头布局图</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="javascript:;" title="查看摄像头布局图" onclick="previewPic(\''+ row.fileName + '\',\''+ row.path + '\')">查看摄像头布局图</a>';

                            return domHtml;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#siteForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val

                        });
                    });
                }

            });
            $("#findSite").click(function () {
                siteTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('siteForm');
            })
        });

        function delSite(id) {
            var r=confirm("确认删除？")
            if (r==false){
                return
            }
            var param = {
                id: id
            };

            $.ajax({
                contentType: "application/json",
                url : path+'/site/deleteSite',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        siteTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

        function uploadLayoutPic(id) {
            openWin('#uploadLayoutPic', path + '/site/toUploadLayoutPic', {"id": id});
        }

        function previewPic(attachName,attachPath) {
            if(attachName==null||attachName==""||attachName=="null"||attachPath==null||attachPath==""||attachPath=="null"){
                alert("暂无摄像头布局图");
                return;
            }
            var  queryParams = 'path='+attachPath+'&fileName='+attachName;
            var url =path + "/upload/previewPic?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>

</head>
<body>

<div class="bg-white item_title">
    站点管理
</div>
<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">
        <div class="row form-group">
            <form id="siteForm">
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>站点名称</label>
                        <input class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>站点地址</label>
                        <input class="form-control" id="address" name="address">
                    </div>
                </div>
                <div class="col-xs-6 text-right">
                    <a id="findSite" href="javascript:;" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-search"></i>&nbsp;&nbsp;--%>查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                    <a id="addSite" href="<%=request.getContextPath() %>/site/toSiteForm?opt=A" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-plus"></i>&nbsp;&nbsp;--%>新增</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="siteTable" class="table table-striped table-bordered display nowrap self_table" align="center" style="width:100%">
    </table>
</div>

<div id="uploadLayoutPic" class="modal fade" data-backdrop="static"></div>
</body>
</html>
