<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>站点详情</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <script>
        $(function () {
        })
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/site/toSiteList" title="点击返回至列表">站点管理</a>
    <i class="fa fa-angle-double-right"></i>
    站点信息查看
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="siteForm">
        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>排序</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,custom[number]]"  id = "sort"  name="sort" placeholder="" value="${site.sort}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">编号</label>
                <div class="col-md-3 pt7">
                    <input readonly class="form-control input-sm validate[required,custom[number]]"  id = "id"  name="id" placeholder="" value="${site.id}" type="text">
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>所属标段</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "bidName" name="bidName"  placeholder="" value="${site.bidName}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>站点名称</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "name"  name="name" placeholder="" value="${site.name}" type="text"  readonly>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>是否开工</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]" id = 'isStarted' name="isStarted" disabled>
                        <option value="">请选择</option>
                        <option value="0" <c:if test="${site.isStarted eq 0}">selected</c:if>>否</option>
                        <option value="1" <c:if test="${site.isStarted eq 1}">selected</c:if>>是</option>
                    </select>
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>是否完工</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]" id = 'isFinished' name="isFinished" disabled>
                        <option value="">请选择</option>
                        <option value="0" <c:if test="${site.isFinished eq 0}">selected</c:if>>否</option>
                        <option value="1" <c:if test="${site.isFinished eq 1}">selected</c:if>>是</option>
                    </select>
                </div>

            </div>

            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>开工顺序</label>
                <div class="col-md-3 pt7">
                    <input readonly class="form-control input-sm validate[required,custom[number]]"  id = "startSort"  name="startSort" placeholder="" value="${site.startSort}" type="text">
                </div>

                <label class="col-md-2 control-label"><span class="alarmstar">*</span>是否默认大屏显示</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]" id = 'defaultShow' name="defaultShow" disabled>
                        <option value="">请选择</option>
                        <option value="0" <c:if test="${site.defaultShow eq 0}">selected</c:if>>否</option>
                        <option value="1" <c:if test="${site.defaultShow eq 1}">selected</c:if>>是</option>
                    </select>
                </div>

            </div>
            <div class="row">
                <label class="col-md-2 control-label">站点开始日期</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "beginTime"  name="beginTime" placeholder="" value="${site.beginTime}" type="text" readonly>
                </div>


                <label class="col-md-2 control-label">站点结束日期</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "endTime"  name="endTime" placeholder="" value="${site.endTime}" type="text" readonly>
                </div>

            </div>
            <div class="row">
                <label class="col-md-2 control-label">面积</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "area"  name="area" placeholder="" value="${site.area}" type="text"  readonly>
                </div>

                <label class="col-md-2 control-label">所属线路</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "lineNo"  name="lineNo" placeholder="" value="${site.lineNo}" type="text"  readonly>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">站点地址</label>
                <div class="col-md-8 pt7">
                    <input class="form-control input-sm"  id = "address"  name="address" placeholder="" value="${site.address}" type="text"  readonly>
                </div>


            </div>
        </div>

        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <a href="${ctx}/site/toSiteList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>
                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
