<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <title>
       上传摄像头布局图
    </title>
    <meta charset="utf-8"/>
</head>
<body>
<div class="opend_modal" id="uploadModal">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <a data-dismiss="modal"><i class="fa fa-close"></i> </a> <b>
                上传摄像头布局图
            </b>
            </div>
            <div class="modal-body container-fluid">
                <form method="post" id="attachForm" >
                    <input type="hidden" name="id" value="${id}" id="id">

                    <div class="row">
                        <label class="col-md-2 control-label"><span class="alarmstar">*</span>摄像头布局图</label>
                        <div id="cameraAttach" class="col-md-7">
                            <input class="form-control form-control-inline input-sm validate[required,maxSize[200]]" id = "cameraAttachName" name="cameraAttachName" size="16" value="" type="text" readonly>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn-md btn btn-primary attachBtn">上传图片</button>
                            <input class="attach" type="file" id="uploadAttachRow"  name="file" accept="" onchange="uploadAttach(this)">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn-md btn btn-primary" onclick="postAttach()">提交</button>
                <button type="button" class="btn btn-default btn-md" data-dismiss="modal" id="closeModal">关闭</button>
            </div>
        </div>
    </div>
</div>




<script>
    var isCameraAttachChanged = '0';
    function uploadAttach(e) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onloadend = function () {
            var fileId = [];
            var rawId = e.id;
            fileId.push(e.id);
            $.ajaxFileUpload({
                url: "<%=request.getContextPath() %>/upload/uploadFile",
                type: 'post',
                secureuri: false,
                fileElementId: fileId,
                dataType: 'json',
                success: function (data, status) {
                    if (data.resultCode == 1) {
                        $("#cameraAttachName").val(data.data[0]);
                        isCameraAttachChanged = '1';
                    } else {
                        notyError(data.resultMsg);
                    }
                },
                error: function (data, status, e) {
                    console.log(e);
                    notyError(e);
                }
            });
        }
    }

    function postAttach() {
        var valid = $("#attachForm").validationEngine("validate");
        if (!valid)
            return;
        var param = {
            id:$("#id").val(),
            fileName:$("#cameraAttachName").val(),
            isCameraAttachChanged :isCameraAttachChanged
        };
        $.ajax({
            contentType : "application/json",
            url : '${ctx}/site/uploadLayoutPic',
            type : 'POST',
            data: JSON.stringify(param),
            success : function (data) {
                var resultCode = data.resultCode;
                if(resultCode == 1)
                {
                    notySuccess(data.resultMsg);
                    setTimeout("javascript:location.href='${ctx}/site/toSiteList'", 1000);
                }else{
                    notyError(data.resultMsg);
                }
            },
            error: function (request, status, error) {
                notyError(JSON.stringify(error));
            }
        });
    }

</script>
</body>
</html>