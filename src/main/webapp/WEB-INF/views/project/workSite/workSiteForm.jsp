<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>工地管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null
            });

            $('#beginTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#saveWorkSite').click(function () {
                disabledAllBtn(true);
                var valid = $("#workSiteForm").validationEngine("validate");
                if (!valid)
                    return;
                var param = serializeObject('#workSiteForm');
                var otherParam = {};
                var jsonString = JSON.stringify($.extend(param, otherParam));
                $.ajax({
                    contentType: "application/json",
                    url : '${ctx}/workSite/saveWorkSite',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);

                        }else{
                            notyError(data.resultMsg);
                            disabledAllBtn(false);
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                        disabledAllBtn(false);
                    }
                });

            });
        });

    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/workSite/toWorkSiteList" title="点击返回至列表">工地管理</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        工地信息增加
    </c:if>
    <c:if test="${opt == 'E'}">
        工地信息编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="workSiteForm">
        <input type="hidden" name="id" value="${workSite.id}" id="id">

        <input type="hidden" id="opt" name="opt" value="${opt}">

        <div class="form-body mb10">
            <div class="form-group">
                <label class="col-md-1 control-label">所属项目</label>
                <div class="col-md-3">
                    <select class="form-control input-sm" name="projectId" id="projectId">
                        <option value=""> 请选择</option>
                        <c:forEach items="${projectList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq workSite.projectId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-1 control-label">工地名称</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "name" name="name"  placeholder="工地名称" value="${workSite.name}" type="text">
                </div>

                <label class="col-md-1 control-label">工地地址</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "address"  name="address" placeholder="工地地址" value="${workSite.address}" type="text">
                </div>

            </div>

            <div class="form-group">
                <label class="col-md-1 control-label">面积</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "area"  name="area" placeholder="面积" value="${workSite.area}" type="text">
                </div>

                <label class="col-md-1 control-label">工地开始日期</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "beginTime"  name="beginTime" placeholder="工地开始日期" value="${workSite.beginTime}" type="text">
                </div>


                <label class="col-md-1 control-label">工地结束日期</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "endTime"  name="endTime" placeholder="工地结束日期" value="${workSite.endTime}" type="text">
                </div>

            </div>
        </div>
        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveWorkSite"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/workSite/toWorkSiteList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
