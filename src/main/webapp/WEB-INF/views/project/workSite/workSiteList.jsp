<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>工地管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var workSiteTable ;
        $(function () {
            workSiteTable = $("#workSiteTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/workSite/getWorkSiteList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'projectName', 'sTitle': '所属项目', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'name', 'sTitle': '工地名称', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'address', 'sTitle': '工地地址', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'area', 'sTitle': '面积', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'beginTime', 'sTitle': '工地开始日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'endTime', 'sTitle': '工地结束日期', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'},
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="'+ path +'/workSite/toWorkSiteView?id='+ row.id +'" title="查看工地信息">查看</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/workSite/toWorkSiteForm?opt=E&id='+ row.id +'" title="修改工地信息">编辑</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除工地信息" onclick="delWorkSite(\''+ row.id + '\')">删除</a>';
                            return domHtml;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#workSiteForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val

                        });
                    });
                }

            });
            $("#findWorkSite").click(function () {
                workSiteTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('workSiteForm');
            })
        });

        function delWorkSite(id) {
            var r=confirm("确认删除？")
            if (r==false){
                return
            }
            var param = {
                id: id
            }

            $.ajax({
                contentType: "application/json",
                url : '${ctx}/workSite/deleteWorkSite',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        workSiteTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

        function addSelectListener() {
            //找到列表中的数据行
            var rows = $("#workSiteTable tr");
            $.each(rows, function (index, row) {
                if (index > 0) {
                    $(row).click(
                        function () {
                            selectedData = $('#workSiteTable').dataTable().fnGetData(this);
                            $("#choicedUserName").html("<strong>" + selectedData.USER_NAME + "</strong> [" + selectedData.LOGIN_ID + "]");
                            $(row).addClass("info");
                            $(row).siblings().removeClass("info");
                        });
                }
            });
        }

    </script>

</head>
<body>

<div class="bg-white item_title">
    工地管理
</div>
<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">
        <div class="row form-group">
            <form id="workSiteForm">
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>工地名称</label>
                        <input class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>工地地址</label>
                        <input class="form-control" id="address" name="address">
                    </div>
                </div>
                <div class="col-xs-6 text-right">
                    <a id="findWorkSite" href="javascript:;" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-search"></i>&nbsp;&nbsp;--%>查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                    <a id="addWorkSite" href="<%=request.getContextPath() %>/workSite/toWorkSiteForm?opt=A" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-plus"></i>&nbsp;&nbsp;--%>新增</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="workSiteTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>

</body>
</html>
