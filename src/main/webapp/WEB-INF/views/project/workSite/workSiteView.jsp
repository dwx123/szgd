<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>工地管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <script>
        $(function () {
        })
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/workSite/toWorkSiteList" title="点击返回至列表">工地管理</a>
    <i class="fa fa-angle-double-right"></i>
    工地信息查看
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="workSiteForm">
        <div class="form-body mb10">
            <div class="form-group">
                <label class="col-md-1 control-label">所属项目</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "projectName" name="projectName"  placeholder="所属项目" value="${workSite.projectName}" type="text" readonly>
                </div>

                <label class="col-md-1 control-label">工地名称</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "name"  name="name" placeholder="工地名称" value="${workSite.name}" type="text"  readonly>
                </div>

                <label class="col-md-1 control-label">工地地址</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "address"  name="address" placeholder="工地地址" value="${workSite.address}" type="text"  readonly>
                </div>

            </div>
            <div class="form-group">
                <label class="col-md-1 control-label">面积</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "area"  name="area" placeholder="面积" value="${workSite.area}" type="text"  readonly>
                </div>

                <label class="col-md-1 control-label">工地开始日期</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "beginTime"  name="beginTime" placeholder="工地开始日期" value="${workSite.beginTime}" type="text" readonly>
                </div>


                <label class="col-md-1 control-label">工地结束日期</label>
                <div class="col-md-3">
                    <input class="form-control input-sm"  id = "endTime"  name="endTime" placeholder="工地结束日期" value="${workSite.endTime}" type="text" readonly>
                </div>

            </div>
        </div>

        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <a href="${ctx}/workSite/toWorkSiteList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>
                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
