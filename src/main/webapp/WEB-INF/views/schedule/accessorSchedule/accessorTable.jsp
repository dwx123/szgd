<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <style type="text/css">
        table{width:100%;font-size:15px;line-height:1.2;border-top:1px solid #a8a8a8;border-left:1px solid #a8a8a8;text-align: center;margin:auto;}
        table  caption{padding:26px 0;font-size: 26px;line-height: 1;color:#e63b3b;}
        table td, table th{ border-right:1px solid #a8a8a8; border-bottom:1px solid #a8a8a8;height:20px;}
        table th{background-color:#eeeeee;padding:8px 0;}
        table td{font-size: 12px;line-height:26px;padding:4px 15px 4px 9px;}
        .align-justy{text-align: justify;}
    </style>

    <script>
        var path = '<%=request.getContextPath() %>';
        $(function () {

            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $("#getTableBySite").click(function () {
                $('#accessorTable').empty();
                initTable();
            });

            initTable();

            $('#reSetBtn1').click(function () {
                resetSearchForm('Form')
            });
        });

        function initTable(){
            var param = serializeObject('#Form');
            var jsonString = JSON.stringify(param);
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/accessorStructureSchedule/getChart',
                type : 'POST',
                data: jsonString,
                success : function (res) {
                    console.log(res);
                    $('#accessorTable').append("<tr><td rowspan='2'>序号</td><td rowspan='2'>车站名称</td>" +
                        "<td colspan='4'>出入口（个）</td><td colspan='4'>风亭（座）</td><td colspan='3'>轨顶风道（米）</td>" +
                        "<td colspan='3'>站台板（米）</td><td colspan='3'>站内楼梯（个）</td><td rowspan='2'>备注</td></tr>" +
                        "<tr><td>设计数量</td><td>正在施工</td><td>已完全完成</td><td>已累计完成</td><td>设计数量</td><td>正在施工</td>" +
                        "<td>已完全完成</td><td>已累计完成</td><td>设计数量</td><td>正在施工</td><td>已完成</td><td>设计数量</td>" +
                        "<td>正在施工</td><td>已完成</td><td>设计数量</td><td>正在施工</td><td>已完成</td></tr>"
                    );
                    for(var i=0;i<res.xAxis.length;i++){
                        $('#accessorTable').append("<tr>" +
                            "<td>" + (i+1) + "</td> " +
                            "<td>"+res.xAxis[i]+"</td> " +
                            "<td>"+res.total[0][i]+"</td> " +
                            "<td>"+res.current[0][i]+"</td> " +
                            "<td>"+res.wholeComplete[0][i]+"</td> " +
                            "<td>"+res.cumulative[0][i]+"</td> " +
                            "<td>"+res.total[1][i]+"</td> " +
                            "<td>"+res.current[1][i]+"</td> " +
                            "<td>"+res.wholeComplete[1][i]+"</td> " +
                            "<td>"+res.cumulative[1][i]+"</td> " +
                            "<td>"+res.total[2][i]+"</td> " +
                            "<td>"+res.current[2][i]+"</td> " +
                            "<td>"+res.cumulative[2][i]+"</td> " +
                            "<td>"+res.total[3][i]+"</td> " +
                            "<td>"+res.current[3][i]+"</td> " +
                            "<td>"+res.cumulative[3][i]+"</td> " +
                            "<td>"+res.total[4][i]+"</td> " +
                            "<td>"+res.current[4][i]+"</td> " +
                            "<td>"+res.cumulative[4][i]+"</td> " +
                            "<td>"+res.remark[i]+"</td> " +
                            "</tr>"
                        );
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error));
                }
            });
        }
    </script>
</head>
<body>
<div class="bg-white form_search form_table form_table mb10 container-fluid">
    <form id="Form" class="form-horizontal form_area">
        <div class="row bg-blue ht46">
            <c:if test="${from ne 'home'}">
                <label class="col-md-0-5 control-label text-center">历史记录</label>
                <div class="col-md-2 pt9">
                    <select class="form-control input-sm select2" name="statisticsTime" id="statisticsTime">
                        <option value="">请选择</option>
                        <c:forEach items="${dateList}" var="item">
                            <option value="${item.statisticsTime}">${item.statisticsTime}</option>
                        </c:forEach>
                    </select>
                </div>
            </c:if>


            <label class="col-md-0-5 control-label text-center">所属站点</label>
            <div class="col-md-7 pt7">
                <select multiple="multiple" class="form-control input-sm select2" name="siteId" id="siteId">
                    <c:forEach items="${sessionScope.siteList}" var="item">
                        <c:choose>
                            <c:when test="${item.id eq siteId}">
                                <option value="${item.id}" selected>${item.name}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${item.id}">${item.name}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>

            <div class="col-md-1-5 pt7 text-right">
                <a id="getTableBySite" href="javascript:;" class="btn btn-primary btn-md1" >查询</a>
                <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn1">重置</button>
            </div>
        </div>
    </form>
</div>
<div class="bg-white mb15">
    <table id="accessorTable" cellpadding="0" cellspacing="0" border="0" summary="车站附属结构施工进度">

    </table>
</div>
</body>
</html>
