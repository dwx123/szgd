<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
<head>
    <title>出入段线、明挖区间及控制中心主体施工形象进度</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <!--echarts-->
    <script src="${ctx}/js/echarts.js" type="text/javascript" charset="UTF-8"></script>

    <script>
        $(function () {
            if (location.hash) {
                $('a[href='+location.hash+']').tab('show')
            }
            $(document.body).on('click','a[data-toggle]',function () {
                location.hash = this.getAttribute('href')
            });
            $(window).on('popstate',function () {
                var anchor = location.hash || $('a[data-toggle = tab]').first().attr('href');
                $('a[href="'+anchor+'"').tab('show')
            });

            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $('#statisticsTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                createChart();
            });

            createChart();

            $("#getChartBySite").click(function () {
//                var bidId = $("#chartForm select[name=bidId]").val();
//                if(bidId.length == 0){
//                    createChart();
//                }else{
//                    createChartBySite();
//                }
                createChart();
            });

        });

        function createChart() {
            $("#container").height(document.documentElement.clientHeight);
            var dom = document.getElementById("container");
            var myChart = echarts.init(dom);
            myChart.showLoading();
            var app = {};
            option = null;
            app.title = '出入段线、明挖区间及控制中心主体施工形象进度';

            option = {
                tooltip : {
                    trigger: 'axis',
                    formatter: function(datas) {
                        var myseries = option.series;
                        var res = datas[0].name + '<br/>', val;
                        for(var i = 0, length = datas.length; i < length; i++) {
                            var dataIndex = datas[i].dataIndex;
                            val = datas[i].value + '%';
                            res += datas[i].marker + datas[i].seriesName + '：' + val + ' (本期:' + myseries[i].current[dataIndex] + ' 累计:' + myseries[i].cumulative[dataIndex] + ' 总量:' + myseries[i].total[dataIndex] + ')<br/>';
                        }
                        return res;
                    }
                },
                legend: [],
                toolbox: {
                    show : true,
                    feature : {
                        dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                grid: {
                    top:'15%',
                    left: '3%',
                    right: '4%',
                    bottom: '8%',
                    containLabel: true
                },
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        data : [],
                        axisLabel:{
                            interval: 0,
                            formatter: function (params) {
                                var newParamsName = "";
                                var paramsNameNumber = params.length;
                                var provideNumber = 8;
                                var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                                if (paramsNameNumber > provideNumber) {
                                    for (var p = 0; p < rowNumber; p++) {
                                        var tempStr = "";
                                        var start = p * provideNumber;
                                        var end = start + provideNumber;
                                        if (p == rowNumber - 1) {
                                            tempStr = params.substring(start, paramsNameNumber);
                                        } else {
                                            tempStr = params.substring(start, end) + "\n";
                                        }
                                        newParamsName += tempStr;
                                    }

                                } else {
                                    newParamsName = params;
                                }
                                return newParamsName;
                            }

                        }
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        name: '完成率',
                        min: 0,
                        max: 100,
                        axisLabel: {
                            formatter: '{value} %'
                        }
                    }
                ],
                dataZoom : [
                    {
                        type: 'slider',
                        show: true,
                        start: 0,
                        end: 100
                    },
                    {
                        type: 'inside',
                        start: 0,
                        end: 100
                    }
                ],
                series : []
            };
            var param = serializeObject('#chartForm');
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/mainWorkSchedule/getChart2',
                type : 'POST',
                data: JSON.stringify(param),
                success : function(result) {
                    if (result) {
                        myChart.hideLoading();
                        var legend1 = new Array();
                        var legend2 = new Array();
                        for(var j=0;j<6;j++){
                            legend1.push(result.legend[j]);
                        }
                        for(var k=6;k<result.legend.length;k++){
                            legend2.push(result.legend[k]);
                        }
                        var item1 = {
                            data:legend1
                        };
                        var item2 = {
                            top:'25',
                            data:legend2
                        };
                        option.legend.push(item1);
                        option.legend.push(item2);
//                        option.legend.data = result.legend;
                        option.xAxis[0].data = result.xAxis;
                        for(var i = 0;i<result.series.length;i++) {
                            var item = {
                                name:result.legend[i],
                                type:'bar',
                                data:result.series[i],
                                current:result.current[i],
                                cumulative:result.cumulative[i],
                                total:result.total[i],
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'top'
                                    }
                                }
                            };
                            option.series.push(item);
                        }
                        myChart.setOption(option, true);
                    }
                },
                error: function(){
                }
            });
        }

        function createChartBySite() {
            $("#container").height(document.documentElement.clientHeight);
            var dom = document.getElementById("container");
            var myChart = echarts.init(dom);
            myChart.showLoading();
            var app = {};
            option = null;
            app.title = '出入段线、明挖区间及控制中心主体施工形象进度';

            option = {
                title : {
                    text: ''
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {
                        type : 'shadow'
                    },
                    formatter: function(param) {
                        var myseries = option.series[0];
                        console.log("seris"+JSON.stringify(myseries));
                        console.log("param"+JSON.stringify(param));
                        var res = param[0].name + '<br/>';
                        for (var i = 0; i < param.length; i++) {
                            var dataIndex = param[i].dataIndex;
                            res += param[i].marker + param[i].seriesName + ' : ' + param[i].data + '% (本期:' + myseries.current[dataIndex] + ' 累计:' + myseries.cumulative[dataIndex] + ' 总量:' + myseries.total[dataIndex] + ')';
                        }
                        return res;
                    }
                },
                toolbox: {
                    show : true,
                    feature : {
                        dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis : [
                    {
                        type : 'category',
                        data : [],
                        axisLabel:{
                            interval: 0,
                            formatter: function (params) {
                                var newParamsName = "";
                                var paramsNameNumber = params.length;
                                var provideNumber = 8;
                                var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                                if (paramsNameNumber > provideNumber) {
                                    for (var p = 0; p < rowNumber; p++) {
                                        var tempStr = "";
                                        var start = p * provideNumber;
                                        var end = start + provideNumber;
                                        if (p == rowNumber - 1) {
                                            tempStr = params.substring(start, paramsNameNumber);
                                        } else {
                                            tempStr = params.substring(start, end) + "\n";
                                        }
                                        newParamsName += tempStr;
                                    }

                                } else {
                                    newParamsName = params;
                                }
                                return newParamsName;
                            }

                        },
                        axisTick: {
                            alignWithLabel: true
                        }
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        name: '完成率',
                        min: 0,
                        max: 100,
                        axisLabel: {
                            formatter: '{value} %'
                        }
                    }
                ],
                series : [
                    {
                        name:'完成率',
                        type:'bar',
                        barWidth: '60%',
                        data:[],
                        current:[],
                        cumulative:[],
                        total:[]
                    }
                ]
            };
            var param = serializeObject('#chartForm');
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/mainWorkSchedule/getChartBySite2',
                type : 'POST',
                data: JSON.stringify(param),
                success : function(result) {
                    if (result) {
                        myChart.hideLoading();
                        option.title.text = result.title;
                        option.xAxis[0].data = result.xAxis;
                        option.series[0].data = result.series;
                        option.series[0].current = result.current;
                        option.series[0].cumulative = result.cumulative;
                        option.series[0].total = result.total;
                        myChart.setOption(option, true);
                    }
                },
                error: function(){
                }
            });
        }

        function downloadSectionMainWorkSchedule() {
            if($('#statisticsTime').val().length == 0 ) {
                alert("请选择日期！");
                return;
            }


            $.ajax({
                contentType: "application/json",
                type: "get",
                url: "<%=request.getContextPath()%>/mainWorkSchedule/isHavaSectionMainWorkSchedule?statisticsTime="+$('#statisticsTime').val(),
                async: false,
                success: function (result) {
                    if (result.resultCode == 0) {
                        alert(result.resultMsg)
                    }else
                    {
                        var  queryParams = 'statisticsTime='+$('#statisticsTime').val();
                        var url =path + "/mainWorkSchedule/downloadSectionMainWorkSchedule?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
                        var o = window.open(url);
                    }
                },
                error: function (request, status, error) {
                    alert(error)
                }
            })

        }

        function downloadTemplate() {
            var  queryParams = 'templateName=出入段线、明挖区间及控制中心主体施工形象进度（YYYY.MM.DD）.xlsx';
            var url =path + "/upload/downloadTemplate?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
        function uploadSectionMainWorkSchedule() {
            if($('#sectionMainWorkScheduleFile').val().length == 0 ) {
                alert("请选择文件！");
                return;
            }
            $.ajaxFileUpload({
                url: "<%=request.getContextPath() %>/mainWorkSchedule/uploadSectionMainWorkSchedule",
                type: 'post',
                secureuri: false,
                fileElementId: ["sectionMainWorkScheduleFile"],
                dataType: 'json',
                success: function (data, status) {
                    $('#sectionMainWorkScheduleFile').val('');
                    if (data.resultCode == 1) {
                        $('sectionMainWorkScheduleFile').val('');
                        alert("上传成功");
                    } else {
                        alert(data.resultMsg);
                    }
                },
                error: function (data, status, e) {
                    console.log(e)
                }
            });
        }
    </script>
</head>
<body>

<c:if test="${from ne 'home'}">
    <div class="bg-white item_title">
        出入段线、明挖区间及控制中心主体施工形象进度
    </div>
</c:if>
<!--tab页-->
<div class="bg-white pd_item pdt0">
    <ul class="nav nav-pills mb10">
        <li class="active">
            <a aria-expanded="false" id="chart"  href="#chartPanel" data-toggle="tab"> 柱状图 </a>
        </li>
        <li>
            <a aria-expanded="true" id="table" href="#enterTable" data-toggle="tab"> 表格 </a>
        </li>
    </ul>

    <div class="tab-content">
        <!--chart-->
        <div class="tab-pane fade active in" id="chartPanel">
            <div class="bg-white form_search form_table form_table mb10 container-fluid">
                <form id="chartForm" class="form-horizontal">
                    <c:if test="${from ne 'home'}">
                        <div class="row form-group">
                            <div class="col-xs-3">
                                <div class="form-item wide2">
                                    <label>历史记录</label>
                                    <select class="form-control input-sm select2" name="statisticsTime" id="statisticsTime">
                                        <option value="">请选择</option>
                                        <c:forEach items="${dateList}" var="item">
                                            <option value="${item.statisticsTime}">${item.statisticsTime}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <div class="row form-group ht42">
                        <div class="col-xs-11">
                            <div class="form-item wide2" style="border: none">
                                <label class="pt5">所属标段</label>
                                <select multiple="multiple" class="form-control input-sm select2" name="bidId" id="bidId">
                                    <c:forEach items="${sessionScope.bidList}" var="item">
                                        <c:choose>
                                            <c:when test="${item.id eq bidId}">
                                                <option value="${item.id}" selected>${item.name}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${item.id}">${item.name}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-1 text-right">
                            <a id="getChartBySite" href="javascript:;" class="ml15 mt5 btn btn-primary btn-md1" >查询</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="bg-white pd_item" id="container">

            </div>
        </div>

        <!--第二个tab内容-->
        <div class="tab-pane fade" id="enterTable">
            <%@include file="sectionTable.jsp"%>
        </div>
    </div>

</div>

</body>

</html>
