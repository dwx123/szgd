<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <style type="text/css">
        table{width:100%;font-size:15px;line-height:1.2;border-top:1px solid #a8a8a8;border-left:1px solid #a8a8a8;text-align: center;margin:auto;}
        table  caption{padding:26px 0;font-size: 26px;line-height: 1;color:#e63b3b;}
        table td, table th{ border-right:1px solid #a8a8a8; border-bottom:1px solid #a8a8a8;height:20px;}
        table th{background-color:#eeeeee;padding:8px 0;}
        table td{font-size: 12px;line-height:26px;padding:4px 15px 4px 9px;}
        .align-justy{text-align: justify;}
    </style>

    <script>
        var path = '<%=request.getContextPath() %>';
        $(function () {

            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $("#getTableBySite").click(function () {
                $('#sectionTable').empty();
                initTable();
            });

            initTable();
        });

        function initTable(){
            var param = serializeObject('#Form');
            var jsonString = JSON.stringify(param);
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/mainWorkSchedule/getChart2',
                type : 'POST',
                data: jsonString,
                success : function (res) {
                    console.log(res);
                    $('#sectionTable').append("<tr id='row1'><td rowspan='2'>序号</td><td colspan='2'>出入场线及区间</td></tr>"+
                        "<tr id='row2'><td colspan='2'>项目</td></tr>"+
                        "<tr id='row3'><td>1</td><td rowspan='4'>围护结构</td><td>地下连续墙（幅）</td></tr>"+
                        "<tr id='row4'><td>2</td><td>钻孔灌注桩（含格构柱）（根）</td></tr>"+
                        "<tr id='row5'><td>3</td><td>咬合桩（根）</td></tr>"+
                        "<tr id='row6'><td>4</td><td>SMW桩（根）</td></tr>"+
                        "<tr id='row7'><td>5</td><td rowspan='2'>基底加固</td><td>旋喷桩（根）</td></tr>"+
                        "<tr id='row8'><td>6</td><td>双/三轴搅拌桩（组）</td></tr>"+
                        "<tr id='row9'><td>7</td><td>降水</td><td>降水井（口）</td></tr>"+
                        "<tr id='row10'><td>8</td><td rowspan='2'>土方开挖</td><td>土方开挖（立方）</td></tr>"+
                        "<tr id='row11'><td>9</td><td>钢支撑架设（根）</td></tr>"+
                        "<tr id='row12'><td>10</td><td rowspan='4'>主体结构</td><td>垫层（段）</td></tr>"+
                        "<tr id='row13'><td>11</td><td>底板（段）</td></tr>"+
                        "<tr id='row14'><td>12</td><td>中板（段）</td></tr>"+
                        "<tr id='row15'><td>13</td><td>顶板（段）</td></tr>");
                    if(res.xAxis.length>0){
                        for(var i=0;i<(res.xAxis.length-1);i++){
                            $('#row1').append("<td colspan='3'>"+res.xAxis[i]+"</td>");
                            $('#row2').append("<td>本期</td><td>累计</td><td>总量</td>");
                            for(var k=0;k<res.legend.length;k++){
                                var temp = k + 3;
                                $('#row'+temp).append("<td>"+res.current[k][i]+"</td><td>"+res.cumulative[k][i]+"</td><td>"+res.total[k][i]+"</td>");
                            }
                        }
                        var j = res.xAxis.length - 1;
                        if(res.xAxis[j] == "合计"){
                            $('#row1').append("<td colspan='4'>"+res.xAxis[j]+"</td>");
                            $('#row2').append("<td>本期</td><td>累计</td><td>总量</td><td>完成率</td>");
                            for(var m=0;m<res.legend.length;m++){
                                var temp = m + 3;
                                $('#row'+temp).append("<td>"+res.current[m][j]+"</td><td>"+res.cumulative[m][j]+"</td><td>"+res.total[m][j]+"</td><td>"+res.series[m][j]+"%</td>");
                            }
                        }else{
                            $('#row1').append("<td colspan='3'>"+res.xAxis[j]+"</td>");
                            $('#row2').append("<td>本期</td><td>累计</td><td>总量</td>");
                            for(var n=0;n<res.legend.length;n++){
                                var temp = n + 3;
                                $('#row'+temp).append("<td>"+res.current[n][j]+"</td><td>"+res.cumulative[n][j]+"</td><td>"+res.total[n][j]+"</td>");
                            }
                        }
                    }
                    
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error));
                }
            });
        }
    </script>
</head>
<body>
<div class="bg-white form_search form_table form_table mb10 container-fluid">
    <form id="Form" class="form-horizontal form_area">
        <div class="row form-group">
            <div class="col-xs-3">
                <div class="form-item wide2">
                    <label>历史记录</label>
                    <select class="form-control input-sm select2" name="statisticsTime" id="statisticsTime">
                        <option value="">请选择</option>
                        <c:forEach items="${dateList}" var="item">
                            <option value="${item.statisticsTime}">${item.statisticsTime}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="col-xs-6">
                <div class="form-item wide2">
                    <label class="control-label">请选择文件</label>
                    <input style="padding: 0;" type="file" name="file" id="sectionMainWorkScheduleFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                </div>
            </div>
            <div class="col-xs-3 text-right">
                <button type="button" onclick="uploadSectionMainWorkSchedule()" class="ml15 btn btn-md1 btn-primary" title="导入出入段线、明挖区间及控制中心主体施工形象进度">导入</button>
                <button type="button" onclick="downloadTemplate()" class="ml15 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                <button type="button" onclick="downloadSectionMainWorkSchedule()" class="ml15 btn btn-md1 btn-warning" title="导出出入段线、明挖区间及控制中心主体施工形象进度Excel">导出</button>
            </div>
        </div>
        <div class="row form-group ht42">
            <div class="col-xs-11">
                <div class="form-item wide2" style="border: none">
                    <label class="pt5">所属标段</label>
                    <select multiple="multiple" class="form-control input-sm select2" name="bidId" id="bidId">
                        <c:forEach items="${sessionScope.bidList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq bidId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="col-xs-1 text-right">
                <a id="getTableBySite" href="javascript:;" class="ml15 mt5 btn btn-primary btn-md1" >查询</a>
            </div>
        </div>
    </form>
</div>
<div class="bg-white mb15">
    <table id="sectionTable" cellpadding="0" cellspacing="0" border="0" summary="出入段线、明挖区间及控制中心主体施工形象进度">

    </table>
</div>
</body>
</html>
