<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <style type="text/css">
        table{width:100%;font-size:15px;line-height:1.2;border-top:1px solid #a8a8a8;border-left:1px solid #a8a8a8;text-align: center;margin:auto;}
        table  caption{padding:26px 0;font-size: 26px;line-height: 1;color:#e63b3b;}
        table td, table th{ border-right:1px solid #a8a8a8; border-bottom:1px solid #a8a8a8;height:20px;}
        table th{background-color:#eeeeee;padding:8px 0;}
        table td{font-size: 12px;line-height:26px;padding:4px 15px 4px 9px;}
        .align-justy{text-align: justify;}
    </style>

    <script>
        var path = '<%=request.getContextPath() %>';
        $(function () {

            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $("#getTableBySite").click(function () {
                $('#se').empty()
                initTable();
            });

            initTable();
        });

        function initTable(){
            var param = {};
            var jsonString = JSON.stringify(param);
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/mainWorkSchedule/getSegmentChartTable',
                type : 'POST',
                data: jsonString,
                success : function (res) {
                    for(var i=0;i<res.xAxis.length;i++){
                        $('#segmentTable').append("<tr>" +
                            "<td>" + (i+1) + "</td> " +
                            "<td>"+res.xAxis[i]+"</td> " +
                            "<td>"+res.series[0][i].steelMouldAmount+"</td> " +
                            "<td>"+res.series[0][i].contractAmount+"</td> " +
                            "<td>"+res.series[0][i].currentProduce+"</td> " +
                            "<td>"+res.series[0][i].cumulativeProduce+"</td> " +
                            "<td>"+res.series[0][i].completeRate+"%</td> " +
                            "<td>"+res.series[0][i].currentDespatch+"</td> " +
                            "<td>"+res.series[0][i].cumulativeDespatch+"</td> " +
                            "<td>"+res.series[0][i].stock+"</td> " +
                            "<td>"+res.series[1][i].contractAmount+"</td> " +
                            "<td>"+res.series[1][i].currentProduce+"</td> " +
                            "<td>"+res.series[1][i].cumulativeProduce+"</td> " +
                            "<td>"+res.series[1][i].stock+"</td> "+
                            "<td>"+res.series[1][i].completeRate+"%</td> "+
                            "</tr>"
                        );
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error));
                }
            });
        }
    </script>
</head>
<body>
<div class="bg-white form_search form_table form_table mb10 container-fluid">
    <form id="Form" class="form-horizontal form_area">
        <div class="row form-group">
            <div class="col-xs-3">
                <div class="form-item wide2">
                    <label>历史记录</label>
                    <select class="form-control input-sm select2" name="statisticsTime" id="statisticsTime">
                        <option value="">请选择</option>
                        <c:forEach items="${dateList}" var="item">
                            <option value="${item.statisticsTime}">${item.statisticsTime}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-item wide2">
                    <label class="control-label">请选择文件</label>
                    <input style="padding: 0;" type="file" name="file" id="segmentMainWorkScheduleFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                </div>
            </div>
            <div class="col-xs-3 text-right">
                <button type="button" onclick="uploadSegmentMainWorkSchedule()" class="ml15 btn btn-md1 btn-primary" title="导入盾构管片生产进度">导入</button>
                <button type="button" onclick="downloadTemplate()" class="ml15 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                <button type="button" onclick="downloadSegmentMainWorkSchedule()" class="ml15 btn btn-md1 btn-warning" title="导出盾构管片生产进度Excel">导出</button>
                <a id="getTableBySite" href="javascript:;" class="ml15 btn btn-primary btn-md1" >查询</a>
            </div>
        </div>
    </form>
</div>
<div class="bg-white mb15">
    <table id="segmentTable" cellpadding="0" cellspacing="0" border="0" summary="盾构管片生产进度">
        <tr>
            <td rowspan="2">序号</td>
            <td rowspan="2">管片厂商</td>
            <td rowspan="2">钢模数量（套）</td>
            <td colspan="7">砼管片（环）</td>
            <td colspan="7">钢管片（吨）</td>
        </tr>
        <tr>
            <td>合同量</td>
            <td>本周生产</td>
            <td>累计生产</td>
            <td>生产完成率%</td>
            <td>本周发运</td>
            <td>累计发运</td>
            <td>库存量</td>
            <td>合同量</td>
            <td>本周生产</td>
            <td>累计生产</td>
            <td>库存量</td>
            <td>生产完成率%</td>
        </tr>
    </table>
</div>
</body>
</html>
