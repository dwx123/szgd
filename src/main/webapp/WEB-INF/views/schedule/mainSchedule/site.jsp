<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>


<!DOCTYPE html>
<html>
<head>
    <title>车站主体施工形象进度</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <!--echarts-->
    <script src="${ctx}/js/echarts.js" type="text/javascript" charset="UTF-8"></script>

    <script>
        $(function () {
            if (location.hash) {
                $('a[href='+location.hash+']').tab('show')
            }
            $(document.body).on('click','a[data-toggle]',function () {
                location.hash = this.getAttribute('href')
            });
            $(window).on('popstate',function () {
                var anchor = location.hash || $('a[data-toggle = tab]').first().attr('href');
                $('a[href="'+anchor+'"').tab('show')
            });

            $('#statisticsTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });
            $('#statisticsTime1').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });
            $('#statisticsTime2').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $('#reSetBtn').click();
                createChart();
            });

            createChart();

            var multiple = $('#siteId').select2({
                placeholder: '请选择',
                allowClear: true
            });

            $('#siteId').click(function(event){
                event.preventDefault();
                var res = [];
                $(this).next('select').find('option').each(function(i,ele){
                    res.push($(ele).val())
                });
                $(multiple).val(res).trigger('change');
            });

            $("#getChartBySite").click(function () {
                var siteId = $("#chartForm select[name=siteId]").val();
                console.log(siteId);
                if(siteId==null || siteId==""){
                    createChart();
                }else{
                    createChartBySite();
                }
            });

            $('#reSetBtn').click(function () {
                resetSearchForm('chartForm')
            });
        });

        function createChart() {
            $("#container").height(document.documentElement.clientHeight*0.7);
            var dom = document.getElementById("container");
            var myChart = echarts.init(dom);
            myChart.showLoading();
            var app = {};
            option = null;
            app.title = '车站主体施工形象进度';

            option = {
                tooltip : {
                    trigger: 'axis',
                    formatter: function(datas) {
                        var myseries = option.series;
                        var res = datas[0].name + '<br/>', val;
                        for(var i = 0, length = datas.length; i < length; i++) {
                            var dataIndex = datas[i].dataIndex;
                            val = datas[i].value + '%';
                            res += datas[i].marker + datas[i].seriesName + '：' + val + ' (本期:' + myseries[i].current[dataIndex] + ' 累计:' + myseries[i].cumulative[dataIndex] + ' 总量:' + myseries[i].total[dataIndex] + ')<br/>';
                        }
                        return res;
                    }
                },
                legend: [],
                toolbox: {
                    show : true,
                    feature : {
                        dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                grid: {
                    top:'15%',
                    left: '3%',
                    right: '4%',
                    bottom: '10%',
                    containLabel: true
                },
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        data : [],
                        axisLabel:{
                            interval: 0,
                            formatter: function (params) {
                                var newParamsName = "";
                                var paramsNameNumber = params.length;
                                var provideNumber = 8;
                                var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                                if (paramsNameNumber > provideNumber) {
                                    for (var p = 0; p < rowNumber; p++) {
                                        var tempStr = "";
                                        var start = p * provideNumber;
                                        var end = start + provideNumber;
                                        if (p == rowNumber - 1) {
                                            tempStr = params.substring(start, paramsNameNumber);
                                        } else {
                                            tempStr = params.substring(start, end) + "\n";
                                        }
                                        newParamsName += tempStr;
                                    }

                                } else {
                                    newParamsName = params;
                                }
                                return newParamsName;
                            }

                        }
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        name: '完成率',
                        min: 0,
                        max: 100,
                        axisLabel: {
                            formatter: '{value} %'
                        }
                    }
                ],
                dataZoom : [
                    {
                        type: 'slider',
                        show: true,
                        start: 94,
                        end: 100
                    },
                    {
                        type: 'inside',
                        start: 94,
                        end: 100
                    }
                ],
                series : []
            };
            var param = serializeObject('#chartForm');
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/mainWorkSchedule/getChart',
                type : 'POST',
                data: JSON.stringify(param),
                success : function(result) {
                    if (result) {
                        myChart.hideLoading();
//                        option.legend.data = result.legend;
                        var legend1 = new Array();
                        var legend2 = new Array();
                        for(var j=0;j<(result.legend.length/2);j++){
                            legend1.push(result.legend[j]);
                        }
                        for(var k=(result.legend.length/2);k<result.legend.length;k++){
                            legend2.push(result.legend[k]);
                        }
                        var item1 = {
                            data:legend1
                        };
                        var item2 = {
                            top:'25',
                            data:legend2
                        };
                        option.legend.push(item1);
                        option.legend.push(item2);
                        option.xAxis[0].data = result.xAxis;
                        for(var i = 0;i<result.series.length;i++) {
                            var item = {
                                name:result.legend[i],
                                type:'bar',
                                data:result.series[i],
                                current:result.current[i],
                                cumulative:result.cumulative[i],
                                total:result.total[i],
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'top'
                                    }
                                }
                            };
                            option.series.push(item);
                        }
                        myChart.setOption(option, true);
                    }
                },
                error: function(){
                }
            });
        }

        function createChartBySite() {
            $("#container").height(document.documentElement.clientHeight*0.7);
            var dom = document.getElementById("container");
            var myChart = echarts.init(dom);
            myChart.showLoading();
            var app = {};
            option = null;
            app.title = '车站主体施工形象进度';

            option = {
                tooltip : {
                    trigger: 'axis',
                    formatter: function(datas) {
                        var myseries = option.series;
                        var res = datas[0].name + '<br/>', val;
                        for(var i = 0, length = datas.length; i < length; i++) {
                            var dataIndex = datas[i].dataIndex;
                            val = datas[i].value + '%';
                            res += datas[i].marker + datas[i].seriesName + '：' + val + ' (本期:' + myseries[i].current[dataIndex] + ' 累计:' + myseries[i].cumulative[dataIndex] + ' 总量:' + myseries[i].total[dataIndex] + ')<br/>';
                        }
                        return res;
                    }
                },
                legend: [],
                toolbox: {
                    show : true,
                    feature : {
                        dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                grid: {
                    top:'15%',
                    left: '3%',
                    right: '4%',
                    bottom: '10%',
                    containLabel: true
                },
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        data : [],
                        axisLabel:{
                            interval: 0,
                            formatter: function (params) {
                                var newParamsName = "";
                                var paramsNameNumber = params.length;
                                var provideNumber = 8;
                                var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                                if (paramsNameNumber > provideNumber) {
                                    for (var p = 0; p < rowNumber; p++) {
                                        var tempStr = "";
                                        var start = p * provideNumber;
                                        var end = start + provideNumber;
                                        if (p == rowNumber - 1) {
                                            tempStr = params.substring(start, paramsNameNumber);
                                        } else {
                                            tempStr = params.substring(start, end) + "\n";
                                        }
                                        newParamsName += tempStr;
                                    }

                                } else {
                                    newParamsName = params;
                                }
                                return newParamsName;
                            }

                        }
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        name: '完成率',
                        min: 0,
                        max: 100,
                        axisLabel: {
                            formatter: '{value} %'
                        }
                    }
                ],
                dataZoom : [
                    {
                        type: 'slider',
                        show: true,
                        start: 0,
                        end: 100
                    },
                    {
                        type: 'inside',
                        start: 0,
                        end: 100
                    }
                ],
                series : []
            };
            var param = serializeObject('#chartForm');
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/mainWorkSchedule/getChart',
                type : 'POST',
                data: JSON.stringify(param),
                success : function(result) {
                    if (result) {
                        myChart.hideLoading();
//                        option.legend.data = result.legend;
                        var legend1 = new Array();
                        var legend2 = new Array();
                        for(var j=0;j<(result.legend.length/2);j++){
                            legend1.push(result.legend[j]);
                        }
                        for(var k=(result.legend.length/2);k<result.legend.length;k++){
                            legend2.push(result.legend[k]);
                        }
                        var item1 = {
                            data:legend1
                        };
                        var item2 = {
                            top:'25',
                            data:legend2
                        };
                        option.legend.push(item1);
                        option.legend.push(item2);
                        option.xAxis[0].data = result.xAxis;
                        for(var i = 0;i<result.series.length;i++) {
                            var item = {
                                name:result.legend[i],
                                type:'bar',
                                data:result.series[i],
                                current:result.current[i],
                                cumulative:result.cumulative[i],
                                total:result.total[i],
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'top'
                                    }
                                }
                            };
                            option.series.push(item);
                        }
                        myChart.setOption(option, true);
                    }
                },
                error: function(){
                }
            });
        }

        function downloadSiteMainWorkSchedule() {
            console.log($('#statisticsTime2').val());
            console.log($('#statisticsTime2').val().length);
            if($('#statisticsTime2').val().length == 0 ) {
                alert("请选择日期！");
                return;
            }

            $.ajax({
                contentType: "application/json",
                type: "get",
                url: "<%=request.getContextPath()%>/mainWorkSchedule/isHavaSiteMainWorkSchedule?statisticsTime="+$('#statisticsTime2').val(),
                async: false,
                success: function (result) {
                    if (result.resultCode == 0) {
                        alert(result.resultMsg)
                    }else
                    {
                        var  queryParams = 'statisticsTime='+$('#statisticsTime2').val();
                        var url =path + "/mainWorkSchedule/downloadSiteMainWorkSchedule?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
                        var o = window.open(url);
                    }
                },
                error: function (request, status, error) {
                    alert(error)
                }
            })

        }

        function downloadTemplate() {
            var  queryParams = 'templateName=车站、明挖区间主体施工形象进度（YYYY.MM.DD）.xlsx';
            var url =path + "/upload/downloadTemplate?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }

        function uploadSiteMainWorkSchedule() {
            if($('#siteMainWorkScheduleFile').val().length == 0 ) {
                alert("请选择文件！");
                return;
            }
            $.ajaxFileUpload({
                url: "<%=request.getContextPath() %>/mainWorkSchedule/uploadSiteMainWorkSchedule",
                type: 'post',
                secureuri: false,
                fileElementId: ["siteMainWorkScheduleFile"],
                dataType: 'json',
                success: function (data, status) {
                    $('#siteMainWorkScheduleFile').val('');
                    if (data.resultCode == 1) {
                        $('siteMainWorkScheduleFile').val('');
                        alert("上传成功");
                    } else {
                        alert(data.resultMsg);
                    }
                },
                error: function (data, status, e) {
                    console.log(e);
                }
            });
        }

    </script>
</head>
<body>

<c:if test="${from ne 'home'}">
    <div class="bg-white item_title" style="padding-right: 0px;">
        车站主体施工形象进度
        <button type="button" class="btn btn-md1 btn-warning title-btn" data-toggle="modal" data-target="#exportInfo" title="导出车站主体施工形象进度Excel">导出</button>
        <button type="button" class="mr10 btn btn-md1 btn-primary title-btn" data-toggle="modal" data-target="#importInfo" title="导入车站主体施工形象进度">导入</button>

    </div>
</c:if>
<!--tab页-->
<div class="bg-white pd_item pdt0">
    <ul class="nav nav-pills mb10">
        <li class="active">
            <a aria-expanded="false" id="chart"  href="#chartPanel" data-toggle="tab"> 柱状图 </a>
        </li>
        <li>
            <a aria-expanded="true" id="table" href="#enterTable" data-toggle="tab"> 表格 </a>
        </li>
    </ul>

    <div class="tab-content">
        <!--chart-->
        <div class="tab-pane fade active in" id="chartPanel">
            <div class="bg-white form_search form_table form_table mb10 container-fluid">
                <form id="chartForm" class="form-horizontal">
                    <div class="row bg-blue ht46">
                        <c:if test="${from ne 'home'}">
                            <label class="col-md-0-5 control-label text-center">历史记录</label>
                            <div class="col-md-2 pt9">
                                <select class="form-control input-sm select2" name="statisticsTime" id="statisticsTime">
                                    <option value="">请选择</option>
                                    <c:forEach items="${dateList}" var="item">
                                        <option value="${item.statisticsTime}">${item.statisticsTime}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </c:if>


                        <label class="col-md-0-5 control-label text-center">所属站点</label>
                        <div class="col-md-7 pt7">
                            <select multiple="multiple" class="form-control input-sm select2" name="siteId" id="siteId">
                                <c:forEach items="${sessionScope.siteList}" var="item">
                                    <c:choose>
                                        <c:when test="${item.id eq siteId}">
                                            <option value="${item.id}" selected>${item.name}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${item.id}">${item.name}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-md-1-5 pt7 text-right">
                            <a id="getChartBySite" href="javascript:;" class="btn btn-primary btn-md1" >查询</a>
                            <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn">重置</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="bg-white pd_item" id="container" style="margin-bottom: 20px;">

            </div>
        </div>

        <!--第2个tab内容-->
        <div class="tab-pane fade" id="enterTable">
            <%@include file="siteTable.jsp"%>
        </div>

    </div>

</div>

<!-- 模态框（Modal） -->
<div class="modal fade" id="importInfo" tabindex="-1" role="dialog" aria-labelledby="importInfoLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="importInfoLabel">导入</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="Form" class="form-horizontal form_area">
                        <div class="col-md-8" style="padding-left: 8%;">
                            <input style="padding: 0;" type="file" name="file" id="siteMainWorkScheduleFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        </div>
                        <div class="col-md-4">
                            <button type="button" onclick="uploadSiteMainWorkSchedule()" class="btn btn-md1 btn-primary" title="导入车站主体施工形象进度">导&nbsp;&nbsp;入</button>
                            <button type="button" onclick="downloadTemplate()" class="ml5 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<!-- 模态框（Modal） -->
<div class="modal fade" id="exportInfo" tabindex="-1" role="dialog" aria-labelledby="exportInfoLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" style="width:32%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="exportInfoLabel">导出</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <label class="col-md-3 control-label">历史记录</label>
                    <div class="col-md-6" style="padding-top: 0.5%;padding-left: 0px;">
                        <select class="form-control input-sm select2" name="statisticsTime" id="statisticsTime2">
                            <option value="">请选择</option>
                            <c:forEach items="${dateList}" var="item">
                                <option value="${item.statisticsTime}">${item.statisticsTime}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <button type="button" onclick="downloadSiteMainWorkSchedule()" class="btn btn-md1 btn-warning" title="导出车站主体施工形象进度Excel">导出</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
</body>

</html>
