<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <style type="text/css">
        table{width:100%;font-size:15px;line-height:1.2;border-top:1px solid #a8a8a8;border-left:1px solid #a8a8a8;text-align: center;margin:auto;}
        table  caption{padding:26px 0;font-size: 26px;line-height: 1;color:#e63b3b;}
        table td, table th{ border-right:1px solid #a8a8a8; border-bottom:1px solid #a8a8a8;height:20px;}
        table th{background-color:#eeeeee;padding:8px 0;}
        table td{font-size: 12px;line-height:26px;padding:4px 15px 4px 9px;}
    </style>

    <script>
        var flag = "${flag}";
        $(function () {
            var multipleTable = $('#siteId').select2({
                placeholder: '请选择',
                allowClear: true
            });

            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $("#getTableBySite").click(function () {
                $('#siteTable').empty();
                initTable();
            });

            initTable();

            $('#reSetBtn1').click(function () {
                resetSearchForm('Form')
            });
        });

        function initTable(){
            var param = serializeObject('#Form');
            var jsonString = JSON.stringify(param);
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/mainWorkSchedule/getChart',
                type : 'POST',
                data: jsonString,
                success : function (res) {
                    console.log(res);
                    if(flag == "whjg"){
                        $('#siteTable').append("<tr id='row1'><td rowspan='2'>序号</td><td colspan='2'>站点</td></tr>"+
                            "<tr id='row2'><td colspan='2'>项目</td></tr>"+
                            "<tr id='row3'><td>1</td><td rowspan='3'>围护结构</td><td>地下连续墙（幅）</td></tr>"+
                            "<tr id='row4'><td>2</td><td>钻孔灌注桩（含格构柱）（根）</td></tr>"+
                            "<tr id='row5'><td>3</td><td>SMW桩（根）</td></tr>");
                    }else if(flag == "tfkw"){
                        $('#siteTable').append("<tr id='row1'><td rowspan='2'>序号</td><td colspan='2'>站点</td></tr>"+
                            "<tr id='row2'><td colspan='2'>项目</td></tr>"+
                            "<tr id='row3'><td>1</td><td rowspan='2'>土方开挖</td><td>土方开挖（立方）</td></tr>"+
                            "<tr id='row4'><td>2</td><td>钢支撑架设（根）</td></tr>");
                    }else if(flag == "ztjg"){
                        $('#siteTable').append("<tr id='row1'><td rowspan='2'>序号</td><td colspan='2'>站点</td></tr>"+
                            "<tr id='row2'><td colspan='2'>项目</td></tr>"+
                            "<tr id='row3'><td>1</td><td rowspan='4'>主体结构</td><td>垫层（段）</td></tr>"+
                            "<tr id='row4'><td>2</td><td>底板（段）</td></tr>"+
                            "<tr id='row5'><td>3</td><td>中板（段）</td></tr>"+
                            "<tr id='row6'><td>4</td><td>顶板（段）</td></tr>");
                    }

                    if(res.xAxis.length > 0){
                        for(var i=0;i<(res.xAxis.length-1);i++){
                            $('#row1').append("<td colspan='3'>"+res.xAxis[i]+"</td>");
                            $('#row2').append("<td>本期</td><td>累计</td><td>总量</td>");
                            for(var k=0;k<res.legend.length;k++){
                                var temp = k + 3;
                                $('#row'+temp).append("<td>"+res.current[k][i]+"</td><td>"+res.cumulative[k][i]+"</td><td>"+res.total[k][i]+"</td>");
                            }
                        }
                        var j = res.xAxis.length - 1;
                        if(res.xAxis[j] == "合计"){
                            $('#row1').append("<td colspan='4'>"+res.xAxis[j]+"</td>");
                            $('#row2').append("<td>本期</td><td>累计</td><td>总量</td><td>完成率</td>");
                            for(var m=0;m<res.legend.length;m++){
                                var temp = m + 3;
                                $('#row'+temp).append("<td>"+res.current[m][j]+"</td><td>"+res.cumulative[m][j]+"</td><td>"+res.total[m][j]+"</td><td>"+res.series[m][j]+"%</td>");
                            }
                        }else{
                            $('#row1').append("<td colspan='3'>"+res.xAxis[j]+"</td>");
                            $('#row2').append("<td>本期</td><td>累计</td><td>总量</td>");
                            for(var n=0;n<res.legend.length;n++){
                                var temp = n + 3;
                                $('#row'+temp).append("<td>"+res.current[n][j]+"</td><td>"+res.cumulative[n][j]+"</td><td>"+res.total[n][j]+"</td>");
                            }
                        }
                    }


                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error));
                }
            });
        }
    </script>
</head>
<body>
<div class="bg-white form_search form_table form_table mb10 container-fluid">
    <form id="Form" class="form-horizontal form_area">
        <input type="hidden" name="flag" value="${flag}" id="flag">

        <div class="row bg-blue ht46">
            <c:if test="${from ne 'home'}">
                <label class="col-md-0-5 control-label text-center">历史记录</label>
                <div class="col-md-2 pt9">
                    <select class="form-control input-sm select2" name="statisticsTime" id="statisticsTimeTable">
                        <option value="">请选择</option>
                        <c:forEach items="${dateList}" var="item">
                            <option value="${item.statisticsTime}">${item.statisticsTime}</option>
                        </c:forEach>
                    </select>
                </div>
            </c:if>

            <label class="col-md-0-5 control-label text-center">所属站点</label>
            <div class="col-md-7 pt7">
                <select multiple="multiple" class="form-control input-sm select2" name="siteId" id="siteId">
                    <c:forEach items="${sessionScope.siteList}" var="item">
                        <c:choose>
                            <c:when test="${item.id eq siteId}">
                                <option value="${item.id}" selected>${item.name}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${item.id}">${item.name}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>

            <div class="col-md-1-5 pt7 text-right">
                <a id="getTableBySite" href="javascript:;" class="btn btn-primary btn-md1" >查询</a>
                <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn1">重置</button>
            </div>
        </div>
    </form>
</div>
<div class="bg-white mb15">
    <table id="siteTable" cellpadding="0" cellspacing="0" border="0" summary="车站主体施工形象进度">

    </table>
</div>
</body>
</html>
