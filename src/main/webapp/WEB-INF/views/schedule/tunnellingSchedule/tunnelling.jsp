<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
<head>
    <title>盾构施工进度</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <!--echarts-->
    <script src="${ctx}/js/echarts.js" type="text/javascript" charset="UTF-8"></script>
    <script>
        $(function () {
            if (location.hash) {
                $('a[href='+location.hash+']').tab('show')
            }
            $(document.body).on('click','a[data-toggle]',function () {
                location.hash = this.getAttribute('href')
            });
            $(window).on('popstate',function () {
                var anchor = location.hash || $('a[data-toggle = tab]').first().attr('href');
                $('a[href="'+anchor+'"').tab('show')
            });

            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $('#statisticsTime').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });
            $('#statisticsTime1').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });

            createChart();

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $('#reSetBtn').click();
                createChart();
            });

            $("#getChartBySite").click(function () {
                var bidId = $("#chartForm select[name=bidId]").val();
                console.log(bidId);
                if(bidId==null||bidId==""){
                    createChart();
                }else{
                    createChartBySite();
                }
            });

            $('#reSetBtn').click(function () {
                resetSearchForm('chartForm')
            });

        });

        function createChart() {
            $("#container").height(document.documentElement.clientHeight*0.7);
            var dom = document.getElementById("container");
            var myChart = echarts.init(dom);
            myChart.showLoading();
            var app = {};
            option = null;
            app.title = '盾构施工进度';

            option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross'
                    }
                },
                toolbox: {
                    feature: {
                        dataView: {show: true, readOnly: false},
                        magicType: {show: true, type: ['line', 'bar']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                legend: {
                    data:[]
                },
                grid: {
                    top:'15%',
                    left: '3%',
                    right: '4%',
                    bottom: '10%',
                    containLabel: true
                },
                calculable : true,
                xAxis: [
                    {
                        type: 'category',
                        data: [],
                        axisTick: {
                            alignWithLabel: true
                        },
                        axisLabel:{
                            interval: 0,
                            formatter: function (params) {
                                var newParamsName = "";
                                var paramsNameNumber = params.length;
                                var provideNumber = 8;
                                var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                                if (paramsNameNumber > provideNumber) {
                                    for (var p = 0; p < rowNumber; p++) {
                                        var tempStr = "";
                                        var start = p * provideNumber;
                                        var end = start + provideNumber;
                                        if (p == rowNumber - 1) {
                                            tempStr = params.substring(start, paramsNameNumber);
                                        } else {
                                            tempStr = params.substring(start, end) + "\n";
                                        }
                                        newParamsName += tempStr;
                                    }
                                } else {
                                    newParamsName = params;
                                }
                                return newParamsName;
                            }
                        },
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '长度',
                        min: 0,
                        max: 2500,
                        interval: 500,
                        axisLabel: {
                            formatter: '{value} m'
                        }
                    },
                    {
                        type: 'value',
                        name: '隧道施工进度',
                        min: 0,
                        max: 100,
                        interval: 20,
                        axisLabel: {
                            formatter: '{value} %'
                        }
                    }
                ],
                dataZoom : [
                    {
                        type: 'slider',
                        show: true,
                        start: 94,
                        end: 100
                    },
                    {
                        type: 'inside',
                        start: 94,
                        end: 100
                    }
                ],
                series: []
            };
            var param = serializeObject('#chartForm');
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/tunnellingWorkSchedule/getChart',
                type : 'POST',
                data: JSON.stringify(param),
                success : function(result) {
                    if (result) {
                        myChart.hideLoading();
                        option.legend.data = result.legend;
                        option.xAxis[0].data = result.xAxis;
                        for(var i = 0;i<(result.series.length-1);i++) {
                            var item = {
                                name:result.legend[i],
                                type:'bar',
                                data:result.series[i],
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'top'
                                    }
                                }
                            };
                            option.series.push(item);
                        }
                        var j = result.series.length-1;
                        var item2 = {
                            name:result.legend[j],
                            type:'line',
                            yAxisIndex: 1,
                            data:result.series[j],
                            label: {
                                normal: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        };
                        option.series.push(item2);
                        myChart.setOption(option, true);
                    }
                },
                error: function(){
                }
            });
        }

        function createChartBySite() {
            $("#container").height(document.documentElement.clientHeight*0.7);
            var dom = document.getElementById("container");
            var myChart = echarts.init(dom);
            myChart.showLoading();
            var app = {};
            option = null;
            app.title = '盾构施工进度';

            option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross'
                    }
                },
                toolbox: {
                    feature: {
                        dataView: {show: true, readOnly: false},
                        magicType: {show: true, type: ['line', 'bar']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                legend: {
                    data:[]
                },
                grid: {
                    top:'15%',
                    left: '3%',
                    right: '4%',
                    bottom: '10%',
                    containLabel: true
                },
                calculable : true,
                xAxis: [
                    {
                        type: 'category',
                        data: [],
                        axisTick: {
                            alignWithLabel: true
                        },
                        axisLabel:{
                            interval: 0,
                            formatter: function (params) {
                                var newParamsName = "";
                                var paramsNameNumber = params.length;
                                var provideNumber = 8;
                                var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                                if (paramsNameNumber > provideNumber) {
                                    for (var p = 0; p < rowNumber; p++) {
                                        var tempStr = "";
                                        var start = p * provideNumber;
                                        var end = start + provideNumber;
                                        if (p == rowNumber - 1) {
                                            tempStr = params.substring(start, paramsNameNumber);
                                        } else {
                                            tempStr = params.substring(start, end) + "\n";
                                        }
                                        newParamsName += tempStr;
                                    }
                                } else {
                                    newParamsName = params;
                                }
                                return newParamsName;
                            }
                        },
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '长度',
                        min: 0,
                        max: 2500,
                        interval: 500,
                        axisLabel: {
                            formatter: '{value} m'
                        }
                    },
                    {
                        type: 'value',
                        name: '隧道施工进度',
                        min: 0,
                        max: 100,
                        interval: 20,
                        axisLabel: {
                            formatter: '{value} %'
                        }
                    }
                ],
                dataZoom : [
                    {
                        type: 'slider',
                        show: true,
                        start: 70,
                        end: 100
                    },
                    {
                        type: 'inside',
                        start: 70,
                        end: 100
                    }
                ],
                series: []
            };
            var param = serializeObject('#chartForm');
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/tunnellingWorkSchedule/getChart',
                type : 'POST',
                data: JSON.stringify(param),
                success : function(result) {
                    if (result) {
                        myChart.hideLoading();
                        option.legend.data = result.legend;
                        option.xAxis[0].data = result.xAxis;
                        for(var i = 0;i<(result.series.length-1);i++) {
                            var item = {
                                name:result.legend[i],
                                type:'bar',
                                data:result.series[i],
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'top'
                                    }
                                }
                            };
                            option.series.push(item);
                        }
                        var j = result.series.length-1;
                        var item2 = {
                            name:result.legend[j],
                            type:'line',
                            yAxisIndex: 1,
                            data:result.series[j],
                            label: {
                                normal: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        };
                        option.series.push(item2);
                        myChart.setOption(option, true);
                    }
                },
                error: function(){
                }
            });
        }

        function createChartBySite1() {
            $("#container").height(document.documentElement.clientHeight);
            var dom = document.getElementById("container");
            var myChart = echarts.init(dom);
            myChart.showLoading();
            var app = {};
            option = null;
            app.title = '盾构施工进度';

            option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross'
                    }
                },
                toolbox: {
                    feature: {
                        dataView: {show: true, readOnly: false},
                        magicType: {show: true, type: ['line', 'bar']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                legend: {
                    data:[]
                },
                grid: {
                    top:'15%',
                    left: '3%',
                    right: '4%',
                    bottom: '8%',
                    containLabel: true
                },
                calculable : true,
                xAxis: [
                    {
                        type: 'category',
                        data: [],
                        axisTick: {
                            alignWithLabel: true
                        },
                        axisLabel:{
                            interval: 0,
                            formatter: function (params) {
                                var newParamsName = "";
                                var paramsNameNumber = params.length;
                                var provideNumber = 8;
                                var rowNumber = Math.ceil(paramsNameNumber / provideNumber);
                                if (paramsNameNumber > provideNumber) {
                                    for (var p = 0; p < rowNumber; p++) {
                                        var tempStr = "";
                                        var start = p * provideNumber;
                                        var end = start + provideNumber;
                                        if (p == rowNumber - 1) {
                                            tempStr = params.substring(start, paramsNameNumber);
                                        } else {
                                            tempStr = params.substring(start, end) + "\n";
                                        }
                                        newParamsName += tempStr;
                                    }
                                } else {
                                    newParamsName = params;
                                }
                                return newParamsName;
                            }
                        },
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '长度',
                        min: 0,
                        max: 2500,
                        interval: 500,
                        axisLabel: {
                            formatter: '{value} m'
                        }
                    },
                    {
                        type: 'value',
                        name: '隧道施工进度',
                        min: 0,
                        max: 100,
                        interval: 20,
                        axisLabel: {
                            formatter: '{value} %'
                        }
                    }
                ],
                dataZoom : [
                    {
                        type: 'slider',
                        show: true,
                        start: 0,
                        end: 100
                    },
                    {
                        type: 'inside',
                        start: 0,
                        end: 100
                    }
                ],
                series: []
            };
            var param = serializeObject('#chartForm');
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/tunnellingWorkSchedule/getChart',
                type : 'POST',
                data: JSON.stringify(param),
                success : function(result) {
                    if (result) {
                        myChart.hideLoading();
                        option.legend.data = result.legend;
                        option.xAxis[0].data = result.xAxis;
                        for(var i = 0;i<(result.series.length-1);i++) {
                            var item = {
                                name:result.legend[i],
                                type:'bar',
                                data:result.series[i],
                                label: {
                                    normal: {
                                        show: true,
                                        position: 'top'
                                    }
                                }
                            };
                            option.series.push(item);
                        }
                        var j = result.series.length-1;
                        var item2 = {
                            name:result.legend[j],
                            type:'line',
                            yAxisIndex: 1,
                            data:result.series[j],
                            label: {
                                normal: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        };
                        option.series.push(item2);
                        myChart.setOption(option, true);
                    }
                },
                error: function(){
                }
            });
        }

        function downloadTunnellingWorkSchedule() {
            if($('#statisticsTime1').val().length == 0 ) {
                alert("请选择日期！");
                return;
            }


            $.ajax({
                contentType: "application/json",
                type: "get",
                url: "<%=request.getContextPath()%>/tunnellingWorkSchedule/isHavaTunnellingWorkSchedule?statisticsTime="+$('#statisticsTime1').val(),
                async: false,
                success: function (result) {
                    if (result.resultCode == 0) {
                        alert(result.resultMsg)
                    }else
                    {
                        var  queryParams = 'statisticsTime='+$('#statisticsTime1').val();
                        var url =path + "/tunnellingWorkSchedule/downloadTunnellingWorkSchedule?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
                        var o = window.open(url);
                    }
                },
                error: function (request, status, error) {
                    alert(error)
                }
            })

        }

        function downloadTemplate() {
            var  queryParams = 'templateName=盾构施工进度（YYYY.MM.DD）.xlsx';
            var url =path + "/upload/downloadTemplate?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
        function uploadTunnellingWorkSchedule() {
            if($('#tunnellingWorkScheduleFile').val().length == 0 ) {
                alert("请选择文件！");
                return;
            }
            $.ajaxFileUpload({
                url: "<%=request.getContextPath() %>/tunnellingWorkSchedule/uploadTunnellingWorkSchedule",
                type: 'post',
                secureuri: false,
                fileElementId: ["tunnellingWorkScheduleFile"],
                dataType: 'json',
                success: function (data, status) {
                    $('#tunnellingWorkScheduleFile').val('');
                    if (data.resultCode == 1) {
                        $('tunnellingWorkScheduleFile').val('');
                        alert("上传成功");
                    } else {
                        alert(data.resultMsg);
                    }
                },
                error: function (data, status, e) {
                    console.log(e)
                }
            });
        }

    </script>
</head>
<body>

<c:if test="${from ne 'home'}">
    <div class="bg-white item_title" style="padding-right: 0px;">
        盾构施工进度
        <button type="button" class="btn btn-md1 btn-warning title-btn" data-toggle="modal" data-target="#exportInfo" title="导出盾构掘进施工形象进度Excel">导出</button>
        <button type="button" class="mr10 btn btn-md1 btn-primary title-btn" data-toggle="modal" data-target="#importInfo" title="导入盾构掘进施工形象进度">导入</button>

    </div>
</c:if>
<!--tab页-->
<div class="bg-white pd_item pdt0">
    <ul class="nav nav-pills mb10">
        <li class="active">
            <a aria-expanded="false" id="chart"  href="#chartPanel" data-toggle="tab"> 柱状图 </a>
        </li>
        <li>
            <a aria-expanded="true" id="table" href="#enterTable" data-toggle="tab"> 表格 </a>
        </li>
    </ul>

    <div class="tab-content">
        <!--第2个tab内容-->
        <div class="tab-pane fade" id="enterTable">
            <%@include file="tunnellingTable.jsp"%>
        </div>

        <!--chart-->
        <div class="tab-pane fade active in" id="chartPanel">
            <div class="bg-white form_search form_table form_table mb10 container-fluid">
                <form id="chartForm" class="form-horizontal">
                    <div class="row bg-blue ht46">
                        <c:if test="${from ne 'home'}">
                            <label class="col-md-0-5 control-label text-center">历史记录</label>
                            <div class="col-md-2 pt9">
                                <select class="form-control input-sm select2" name="statisticsTime" id="statisticsTime">
                                    <option value="">请选择</option>
                                    <c:forEach items="${dateList}" var="item">
                                        <option value="${item.statisticsTime}">${item.statisticsTime}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </c:if>


                        <label class="col-md-0-5 control-label text-center">所属标段</label>
                        <div class="col-md-7 pt7">
                            <select multiple="multiple" class="form-control input-sm select2" name="bidId" id="bidId">
                                <c:forEach items="${sessionScope.bidList}" var="item">
                                    <c:choose>
                                        <c:when test="${item.id eq bidId}">
                                            <option value="${item.id}" selected>${item.name}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${item.id}">${item.name}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="col-md-1-5 pt7 text-right">
                            <a id="getChartBySite" href="javascript:;" class="btn btn-primary btn-md1" >查询</a>
                            <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn">重置</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="bg-white pd_item" id="container">

            </div>
        </div>
    </div>

</div>

<!-- 模态框（Modal） -->
<div class="modal fade" id="importInfo" tabindex="-1" role="dialog" aria-labelledby="importInfoLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="importInfoLabel">导入</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="Form" class="form-horizontal form_area">
                        <div class="col-md-8" style="padding-left: 8%;">
                            <input style="padding: 0;" type="file" name="file" id="tunnellingWorkScheduleFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        </div>
                        <div class="col-md-4">
                            <button type="button" onclick="uploadTunnellingWorkSchedule()" class="btn btn-md1 btn-primary" title="导入盾构掘进施工形象进度">导&nbsp;&nbsp;入</button>
                            <button type="button" onclick="downloadTemplate()" class="ml5 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

<!-- 模态框（Modal） -->
<div class="modal fade" id="exportInfo" tabindex="-1" role="dialog" aria-labelledby="exportInfoLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" style="width:32%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="exportInfoLabel">导出</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <label class="col-md-3 control-label">历史记录</label>
                    <div class="col-md-6" style="padding-top: 0.5%;padding-left: 0px;">
                        <select class="form-control input-sm select2" name="statisticsTime" id="statisticsTime1">
                            <option value="">请选择</option>
                            <c:forEach items="${dateList}" var="item">
                                <option value="${item.statisticsTime}">${item.statisticsTime}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <button type="button" onclick="downloadTunnellingWorkSchedule()" class="btn btn-md1 btn-warning" title="导出盾构掘进施工形象进度Excel">导出</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
</body>

</html>
