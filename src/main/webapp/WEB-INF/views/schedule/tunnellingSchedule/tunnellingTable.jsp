<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <style type="text/css">
        table{width:100%;font-size:15px;line-height:1.2;border-top:1px solid #a8a8a8;border-left:1px solid #a8a8a8;text-align: center;margin:auto;}
        table  caption{padding:26px 0;font-size: 26px;line-height: 1;color:#e63b3b;}
        table td, table th{ border-right:1px solid #a8a8a8; border-bottom:1px solid #a8a8a8;height:20px;}
        table th{background-color:#eeeeee;padding:8px 0;}
        table td{font-size: 12px;line-height:26px;padding:4px 15px 4px 9px;}
        .align-justy{text-align: justify;}
    </style>

    <script>
        var path = '<%=request.getContextPath() %>';
        $(function () {

            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $("#getTableBySite").click(function () {
                $('#tunnellingTable').empty();
                initTable();
            });

            initTable();

            $('#reSetBtn1').click(function () {
                resetSearchForm('Form')
            });
        });

        function initTable(){
            var param = serializeObject('#Form');
            var jsonString = JSON.stringify(param);
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/tunnellingWorkSchedule/getChartTable',
                type : 'POST',
                data: jsonString,
                success : function (res) {
                    console.log(res);
                    $('#tunnellingTable').append("<tr><td>标段</td><td>区间名称</td><td>端头井加固</td>" +
                        "<td>（拟）始发时间</td><td>区间长度</td><td>本周完成（m）</td><td>累计完成（m）</td>" +
                        "<td>隧道施工进度（%）</td><td>备注</td></tr>"
                    );
                    for(var i=0; i<(res.length-1); i++){
                        $('#tunnellingTable').append("<tr>" +
                            "<td rowspan='"+res[i].length+"'>"+res[i][0].bidName+"</td> " +
                            "<td>"+res[i][0].intervalName+"</td> " +
                            "<td>"+res[i][0].endShaftReinforce+"</td> " +
                            "<td>"+res[i][0].startTime+"</td> " +
                            "<td>"+res[i][0].intervalLength+"</td> " +
                            "<td>"+res[i][0].currentComplete+"</td> " +
                            "<td>"+res[i][0].cumulativeComplete+"</td> " +
                            "<td>"+res[i][0].completeRate+"</td> " +
                            "<td>"+res[i][0].remark+"</td> " +
                            "</tr>"
                        );
                        for(var j=1; j<res[i].length; j++){
                            $('#tunnellingTable').append("<tr>" +
                                "<td>"+res[i][j].intervalName+"</td> " +
                                "<td>"+res[i][j].endShaftReinforce+"</td> " +
                                "<td>"+res[i][j].startTime+"</td> " +
                                "<td>"+res[i][j].intervalLength+"</td> " +
                                "<td>"+res[i][j].currentComplete+"</td> " +
                                "<td>"+res[i][j].cumulativeComplete+"</td> " +
                                "<td>"+res[i][j].completeRate+"</td> " +
                                "<td>"+res[i][j].remark+"</td> " +
                                "</tr>"
                            );
                        }
                    }
                    var k = res.length - 1;
                    var isSum = res[k][0].isSum;
                    if(isSum == 1){
                        $('#tunnellingTable').append("<tr>" +
                            "<td colspan='4'>"+res[k][0].bidName+"</td> " +
                            "<td>"+res[k][0].intervalLength+"</td> " +
                            "<td>"+res[k][0].currentComplete+"</td> " +
                            "<td>"+res[k][0].cumulativeComplete+"</td> " +
                            "<td>"+res[k][0].completeRate+"</td> " +
                            "<td>"+res[k][0].remark+"</td> " +
                            "</tr>"
                        );
                    }else{
                        $('#tunnellingTable').append("<tr>" +
                            "<td rowspan='"+res[k].length+"'>"+res[k][0].bidName+"</td> " +
                            "<td>"+res[k][0].intervalName+"</td> " +
                            "<td>"+res[k][0].endShaftReinforce+"</td> " +
                            "<td>"+res[k][0].startTime+"</td> " +
                            "<td>"+res[k][0].intervalLength+"</td> " +
                            "<td>"+res[k][0].currentComplete+"</td> " +
                            "<td>"+res[k][0].cumulativeComplete+"</td> " +
                            "<td>"+res[k][0].completeRate+"</td> " +
                            "<td>"+res[k][0].remark+"</td> " +
                            "</tr>"
                        );
                        for(var j=1; j<res[k].length; j++){
                            $('#tunnellingTable').append("<tr>" +
                                "<td>"+res[k][j].intervalName+"</td> " +
                                "<td>"+res[k][j].endShaftReinforce+"</td> " +
                                "<td>"+res[k][j].startTime+"</td> " +
                                "<td>"+res[k][j].intervalLength+"</td> " +
                                "<td>"+res[k][j].currentComplete+"</td> " +
                                "<td>"+res[k][j].cumulativeComplete+"</td> " +
                                "<td>"+res[k][j].completeRate+"</td> " +
                                "<td>"+res[k][j].remark+"</td> " +
                                "</tr>"
                            );
                        }
                    }


                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error));
                }
            });
        }
    </script>
</head>
<body>
<div class="bg-white form_search form_table form_table mb10 container-fluid">
    <form id="Form" class="form-horizontal form_area">
        <div class="row bg-blue ht46">

            <c:if test="${from ne 'home'}">
                <label class="col-md-0-5 control-label text-center">历史记录</label>
                <div class="col-md-2 pt9">
                    <select class="form-control input-sm select2" name="statisticsTime" id="statisticsTime">
                        <option value="">请选择</option>
                        <c:forEach items="${dateList}" var="item">
                            <option value="${item.statisticsTime}">${item.statisticsTime}</option>
                        </c:forEach>
                    </select>
                </div>
            </c:if>


            <label class="col-md-0-5 control-label text-center">所属标段</label>
            <div class="col-md-7 pt7">
                <select multiple="multiple" class="form-control input-sm select2" name="bidId" id="bidId">
                    <c:forEach items="${sessionScope.bidList}" var="item">
                        <c:choose>
                            <c:when test="${item.id eq bidId}">
                                <option value="${item.id}" selected>${item.name}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${item.id}">${item.name}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>

            <div class="col-md-1-5 pt7 text-right">
                <a id="getTableBySite" href="javascript:;" class="btn btn-primary btn-md1" >查询</a>
                <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn1">重置</button>
            </div>
        </div>

    </form>
</div>

<div class="bg-white mb15">
    <table id="tunnellingTable" cellpadding="0" cellspacing="0" border="0" summary="盾构施工进度">

    </table>
</div>
</body>
</html>
