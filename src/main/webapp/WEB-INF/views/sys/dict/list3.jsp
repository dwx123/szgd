<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>流程列表</title>
    <%@ include file="/common/global.jsp"%>
    <%@ include file="/common/include-css.jsp"%>
</head>
<body>

<table width="100%" class="need-border">
    <thead>
    <tr>
        <th>数据字典值</th>
        <th>数据字典描述</th>
        <th>顺序</th>
        <th>父级</th>
        <th>时间</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${sysDicts }" var="dict">
        <tr>
            <td>${dict.dictValue }</td>
            <td>${dict.dictDesc }</td>
            <td>${dict.dictSort }</td>
            <td>${dict.dictParentId }</td>
            <td>${dict.optDate }</td>
            <td>
                <a href="${ctx}/dict/toInsert/${dict.id}" >增加</a>
            </td>
            <td>
                <a href="${ctx}/jFProjectProcess/getProjectLevelTwo/${project.id}" >修改</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>
