<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>


<!DOCTYPE html>
<html>
<head>
    <title>日志管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {
            if (location.hash) {
                $('a[href='+location.hash+']').tab('show')
            }
            $(document.body).on('click','a[data-toggle]',function () {
                location.hash = this.getAttribute('href')
            });
            $(window).on('popstate',function () {
                var anchor = location.hash || $('a[data-toggle = tab]').first().attr('href');
                $('a[href="'+anchor+'"').tab('show')
            });

            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear:true
            });

            $('#beginOpTime1').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });
            $('#endOpTime1').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });
            $('#beginOpTime2').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });
            $('#endOpTime2').datetimepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: 1,
                minView: "month",
                autoclose: 1,
                language: 'zh-CN'
            });

            initLogTable();
            initOpLogTable();

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

            });

            $("#btnFindMenuLog").click(function () {
                menuLogTable.ajax.reload();
            });

            $('#reSetBtn1').click(function () {
                resetSearchForm('menuLogForm')
                $("#sysFlag").val('');
            });

            $("#btnFindOpLog").click(function () {
                opLogTable.ajax.reload();
            });


            $('#reSetBtn2').click(function () {
                resetSearchForm('opLogForm')
            });
        });

        function initLogTable(){
            menuLogTable = $("#menuLogTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'<'pull-left filterInput'>>t<'row-fluid'<'span4'il><'span7'<'pull-right'p>>>",
                bAutoWidth: false,
                "bSort": false,
                sServerMethod: "GET",
                sAjaxSource: path + "/log/getLogList",//获取列表数据url,
                aoColumns: [
                    {'mDataProp': 'menuName', 'sTitle': '模块名称', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'opName', 'sTitle': '操作人', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'opTime', 'sTitle': '操作时间', 'sWidth': '10%', 'sClass': 'center'},
                ],   //渲染某列格式
                aoColumnDefs: [
                    {
                        aTargets: [0],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {

                            return data;

                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var sysFlag = $('#sysFlag').val();
                    var menuId = $('#menuId').val();
                    var opLoginId = $('#opLoginId1').val();
                    var beginOpTime = $('#beginOpTime1').val();
                    var endOpTime = $('#endOpTime1').val();

                    aoData.push({"name" : "sysFlag", "value" : sysFlag});
                    aoData.push({"name" : "menuId", "value" : menuId});
                    aoData.push({"name" : "opLoginId", "value" : opLoginId});
                    aoData.push({"name" : "beginOpTime", "value" : beginOpTime});
                    aoData.push({"name" : "endOpTime", "value" : endOpTime});

                }
            });
        }


        function initOpLogTable(){
            opLogTable = $("#opLogTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'<'pull-left filterInput'>>t<'row-fluid'<'span4'il><'span7'<'pull-right'p>>>",
                bAutoWidth: false,
                "bSort": false,
                sServerMethod: "GET",
                sAjaxSource: path + "/log/getOpLogList",//获取列表数据url,
                aoColumns: [
                    {'mDataProp': 'dataType', 'sTitle': '数据类型', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'name', 'sTitle': '名称', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'opName', 'sTitle': '操作人', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'OPTIME', 'sTitle': '操作时间', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'opType', 'sTitle': '操作类型', 'sWidth': '10%', 'sClass': 'center'},
                ],   //渲染某列格式
                aoColumnDefs: [
                    {
                        aTargets: [0],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var html = '';
                            if (data=='1')
                                html = '人员信息';
                            if (data=='2')
                                html = '培训信息';
                            if (data=='3')
                                html = '体检信息';
                            if (data=='4')
                                html = '设备信息';
                            if (data=='5')
                                html = '车辆信息';
                            if (data=='6')
                                html = '隐患信息';

                            return html;

                        }
                    },
                    {
                        aTargets: [4],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var html = '';
                            if (data=='add')
                                html = '新增';
                            if (data=='edit')
                                html = '编辑';
                            return html;

                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var dataType = $('#dataType').val();
                    var opType = $('#opType').val();
                    var opLoginId = $('#opLoginId2').val();
                    var beginOpTime = $('#beginOpTime2').val();
                    var endOpTime = $('#endOpTime2').val();

                    aoData.push({"name" : "dataType", "value" : dataType});
                    aoData.push({"name" : "opType", "value" : opType});
                    aoData.push({"name" : "opLoginId", "value" : opLoginId});
                    aoData.push({"name" : "beginOpTime", "value" : beginOpTime});
                    aoData.push({"name" : "endOpTime", "value" : endOpTime});


                }
            });
        }

        function downloadTemplate() {
            var  queryParams = 'templateName=车站附属结构施工进度（YYYY.MM.DD）.xlsx';
            var url =path + "/upload/downloadTemplate?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }

        function downloadAccessorStructureSchedule() {
            if($('#statisticsTime1').val().length == 0 ) {
                alert("请选择日期！");
                return;
            }


            $.ajax({
                contentType: "application/json",
                type: "get",
                url: "<%=request.getContextPath()%>/accessorStructureSchedule/isHavaAccessorStructureSchedule?statisticsTime="+$('#statisticsTime1').val(),
                async: false,
                success: function (result) {
                    if (result.resultCode == 0) {
                        alert(result.resultMsg)
                    }else
                    {
                        var  queryParams = 'statisticsTime='+$('#statisticsTime1').val();
                        var url =path + "/accessorStructureSchedule/downloadAccessorStructureSchedule?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
                        var o = window.open(url);
                    }
                },
                error: function (request, status, error) {
                    alert(error)
                }
            })
        }

        function uploadAccessorStructureSchedule() {
            if($('#accessorStructureScheduleFile').val().length == 0 ) {
                alert("请选择文件！");
                return;
            }
            $.ajaxFileUpload({
                url: "<%=request.getContextPath() %>/accessorStructureSchedule/uploadAccessorStructureSchedule",
                type: 'post',
                secureuri: false,
                fileElementId: ["accessorStructureScheduleFile"],
                dataType: 'json',
                success: function (data, status) {
                    $('#accessorStructureScheduleFile').val('');
                    if (data.resultCode == 1) {
                        $('accessorStructureScheduleFile').val('');
                        alert("上传成功");
                    } else {
                        alert(data.resultMsg);
                    }
                },
                error: function (data, status, e) {
                    console.log(e)
                }
            });
        }

        function getMenuList() {
            var sysFlag= $("#sysFlag").val();
            if (sysFlag == '')
            {
                var option = "<option value=''>请选择</option>";
                $("#menuId").html(option).trigger("change");
                return;
            }
            var param = {'sysFlag': sysFlag};
            $("#menuId").val(null).trigger("change");
            $.ajax({
                contentType : "application/json",
                url : '${ctx}/log/getLeafMenuList',
                type : 'POST',
                data: JSON.stringify(param),
                success:function (data) {
                    var option = "<option value=''>请选择</option>";
                    var count= data.length;
                    if(count > 0){
                        for(var i = 0; i < count; i++){
                            if (sysFlag == '1')
                                option += "<option value='"+data[i].menuId+"'  data-id='"+data[i].menuId+"'>"+data[i].parentMenuName+"->"+data[i].menuName+"</option>";
                            if (sysFlag == '2')
                                option += "<option value='"+data[i].menuId+"'  data-id='"+data[i].menuId+"'>"+data[i].menuName+"</option>";
                        }
                    }
                    $("#menuId").html(option).trigger("change");
                },
                error:function(e) {
                    alert("系统异常，请稍候重试！");
                }
            });
        }
    </script>
</head>
<body>

<c:if test="${from ne 'home'}">
    <div class="bg-white item_title" style="padding-right: 0px;">
        日志管理
    </div>
</c:if>
<!--tab页-->
<div class="bg-white pd_item pdt0">
    <ul class="nav nav-pills mb10">
        <li class="active">
            <a aria-expanded="false" id="menuLog"  href="#tabMenuLog" data-toggle="tab">模块访问</a>
        </li>
        <li>
            <a aria-expanded="true" id="opLog" href="#tabOpLog" data-toggle="tab">数据修改</a>
        </li>
    </ul>

    <div class="tab-content">
        <!--chart-->
        <div class="tab-pane fade active in" id="tabMenuLog">
            <div class="bg-white form_search form_table form_table mb10 container-fluid">
                <form id="menuLogForm" class="form-horizontal">
                    <div class="row form-group">
                        <div class="col-xs-4">
                            <div class="form-item wide2">
                                <label>系统</label>
                                <select class="form-control input-sm select2" name="sysFlag" id="sysFlag"  onchange="getMenuList()">
                                    <option value="">请选择</option>
                                    <option value="1" selected>信息化管理平台</option>
                                    <option value="2">APP</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-item wide2">
                                <label>模块名称</label>
                                <select  class="form-control input-sm select2 validate[]" id = 'menuId' name="menuId">
                                    <option value="">请选择</option>
                                    <c:forEach items="${menuList}" var="item">
                                        <c:choose>
                                            <c:when test="${item.menuId eq menuId}">
                                                <option value="${item.menuId}" data-id="${item.menuId}" selected>${item.parentMenuName}->${item.menuName}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${item.menuId}"  data-id="${item.menuId}" >${item.parentMenuName}->${item.menuName}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-item wide2">
                                <label>操作人</label>
                                <select  class="form-control input-sm select2 validate[]" id = 'opLoginId1' name="opLoginId1">
                                    <option value="">请选择</option>
                                    <c:forEach items="${userList}" var="item">
                                        <c:choose>
                                            <c:when test="${item.loginId eq loginId}">
                                                <option value="${item.loginId}">${item.userName}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${item.loginId}" >${item.userName}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row form-group">
                        <div class="col-xs-4">
                            <div class="form-item wide2">
                                <label>操作时间</label>
                                <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">
                                    <input style="background-color: #fff;" class="form-control" id="beginOpTime1" name="beginOpTime1" type="text" readonly>
                                    <span class="input-group-addon"> 至 </span>
                                    <input style="background-color: #fff;" class="form-control" id="endOpTime1" name="endOpTime1" type="text" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-item wide2">
                                <label></label>
                                <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">

                                </div>
                            </div>
                        </div>


                        <div class="col-xs-4 text-right">
                            <a id="btnFindMenuLog" href="javascript:;" class="btn btn-primary btn-md1" >查询</a>
                            <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn1">重置</button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="bg-white pd_item">
                <table id="menuLogTable" class="table table-striped table-bordered display nowrap self_table" align="center">
                </table>
            </div>
        </div>
        <!--第2个tab内容-->
        <div class="tab-pane fade" id="tabOpLog">
            <div class="bg-white form_search form_table form_table mb10 container-fluid">
                <form id="opLogForm" class="form-horizontal form_area">
                    <div class="row form-group">
                        <div class="col-xs-4">
                            <div class="form-item wide2">
                                <label>数据类别</label>
                                <select class="form-control input-sm select2" name="dataType" id="dataType">
                                    <option value="">请选择</option>
                                    <option value="1">人员信息</option>
                                    <option value="2">培训信息</option>
                                    <option value="3">体检信息</option>
                                    <option value="4">设备信息</option>
                                    <option value="5">车辆信息</option>
                                    <option value="6">隐患信息</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-item wide2">
                                <label>操作类型</label>
                                <select  class="form-control input-sm select2 validate[]" id = 'opType' name="opType">
                                    <option value="">请选择</option>
                                    <option value="add">新增</option>
                                    <option value="edit">编辑</option>

                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-item wide2">
                                <label>操作人</label>
                                <select  class="form-control input-sm select2 validate[]" id = 'opLoginId2' name="opLoginId2">
                                    <option value="">请选择</option>
                                    <c:forEach items="${userList}" var="item">
                                        <c:choose>
                                            <c:when test="${item.loginId eq loginId}">
                                                <option value="${item.loginId}">${item.userName}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${item.loginId}" >${item.userName}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row form-group">
                        <div class="col-xs-4">
                            <div class="form-item wide2">
                                <label>操作时间</label>
                                <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">
                                    <input style="background-color: #fff;" class="form-control" id="beginOpTime2" name="beginOpTime2" type="text" readonly>
                                    <span class="input-group-addon"> 至 </span>
                                    <input style="background-color: #fff;" class="form-control" id="endOpTime2" name="endOpTime2" type="text" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-item wide2">
                                <label></label>
                                <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd hh:ii:ss">

                                </div>
                            </div>
                        </div>


                        <div class="col-xs-4 text-right">
                            <a id="btnFindOpLog" href="javascript:;" class="btn btn-primary btn-md1" >查询</a>
                            <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn2">重置</button>
                        </div>
                    </div>

                </form>

            </div>
            <div class="bg-white pd_item">
                <table id="opLogTable" class="table table-striped table-bordered display nowrap self_table" align="center">
                </table>
            </div>
        </div>
    </div>

</div>

</body>

</html>
