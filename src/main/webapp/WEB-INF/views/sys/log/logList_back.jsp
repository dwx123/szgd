<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>日志管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <%@ include file="/common/include-css.jsp"%>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var jsonString = null;
        var sysLogTable ;
        $(function () {
       sysLogTable = $("#sysLogTable").DataTable({
                iDisplayLength: 8,
               "sDom": "<'row-fluid'<'pull-left filterInput'f>>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
               iDisplayStart: 0,
              "bSort": true,
               fnDrawCallback: addSelectListener,
                sAjaxSource: path + "/sysLog/getSysLogData",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'account', 'sTitle': '帐号', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'username', 'sTitle': '用户名', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'ip', 'sTitle': 'ip地址', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'opt', 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'loginTime', 'sTitle': '操作时间', 'sWidth': '10%', 'sClass': 'center'}
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {
                        aTargets: [4],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var subData = data;
                            if (data) {
                                var date = new Date(data)
                                var dateyear = date.getFullYear()
                                var datemonth = (date.getMonth()+1) >9 ? (date.getMonth()+1) : '0'+(date.getMonth()+1)
                                var dateday = date.getDate() > 9 ? date.getDate() : '0' + date.getDate()
                                var datehour = date.getHours() > 9 ? date.getHours() : '0' + date.getHours()
                                var dateMin = date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes()
                                var dateSec = date.getSeconds() > 9 ? date.getSeconds() : '0' + date.getSeconds()
                                subData =  dateyear+ '-' +datemonth+ '-' +dateday + ' '+datehour+':'+dateMin+':'+dateSec
                            }
                            return subData;

                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    aoData.push({
                        "name" : "jsonString",
                        "value": JSON.stringify(jsonString)
                    });
                }

            });
            $("#findByModel").click(function () {
                jsonString = {
                    'account': $("#account").val(),
                    'ip': $("#ip").val(),
                    'username': $('#username').val()
                };
                sysLogTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('logSearchForm')
            })
        });


        function addSelectListener() {
            //找到列表中的数据行
            var rows = $("#sysLogTable tr");
            $.each(rows, function (index, row) {
                if (index > 0) {
                    $(row).click(
                        function () {
                            selectedData = $('#sysLogTable').dataTable().fnGetData(this);
                            $("#choicedUserName").html("<strong>" + selectedData.USER_NAME + "</strong> [" + selectedData.LOGIN_ID + "]");
                            $(row).addClass("info");
                            $(row).siblings().removeClass("info");
                        });
                }
            });
        }
    </script>

</head>
<body>

<div class="bg-white item_title">
    日志管理
</div>
<div class="bg-white">
    <form id="logSearchForm" class="form_search form_table container-fluid">
        <div class="row form-group">
            <div class="col-xs-3">
                <div class="form-item wide2">
                    <label>帐号</label>
                    <input class="form-control" id="account">
                </div>
            </div>
            <div class="col-xs-3">
                <div class="form-item wide2">
                    <label>用户名</label>
                    <input class="form-control" id="username">
                </div>
            </div>
            <div class="col-xs-3">
                <div class="form-item wide2">
                    <label>ip地址</label>
                    <input class="form-control" id="ip">
                </div>
            </div>
            <div class="col-xs-3">
                <a href="javascript:;" class="ml15 btn btn-primary btn-md" id="findByModel"><%--<i class="fa fa-search"></i>--%> 查询</a>
                <button type="button" class="btn btn-md btn-reset ml15" id="reSetBtn">重置</button>
            </div>
        </div>
    </form>
</div>
<div class="bg-white pd_item">
    <table id="sysLogTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>

</body>
</html>
