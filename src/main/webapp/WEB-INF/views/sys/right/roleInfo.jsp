<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!doctype html>
<html>
	<head>
		<title>角色详情</title>
		<link href="${ctx}/assets/css/zTreeStyle/zTreeStyle.css" type="text/css" rel="stylesheet" />
		<script src="${ctx}/assets/js/jquery.ztree.all.js" type="text/javascript"></script>
		<script src="${ctx}/assets/js/dataTables.custom.js"></script>
		<script type="text/javascript">
			var $roleID_tmp='<%=request.getParameter("ROLE_ID") %>';
			function initRoleInfoData(){
				$.ajax({
			        async : false,  
			        cache:false,  
			        type: 'POST',
			        data: {"ROLE_ID":$roleID_tmp},
			        dataType : "json",  
			        url: path+"/sys/right!loadRole.ec",
			        error: function () {
			        	notyError('加载角色详情失败!');  
			        },  
			        success:function(data){
			        	$(".box-content input[name='ROLE_ID_ReadOnly']").val(data.ROLE_ID);
				    	$(".box-content input[name='ROLE_NAME_ReadOnly']").val(data.ROLE_NAME);
				    	$("#REMARK_ReadOnly").val(data.REMARK); 
			        }  
			    });
				
			}
			// 页面加载完成后，完成初始化界面数据操作...
			$(function(){
				// -- 用户查询  --
			    var tableConf = {
			    		sDom: "<'row-fluid'<'span12'rt>><'row-fluid'<'span4'i><'span8'<'pull-right'p>>>",
			        	mTableDivID:"#roleUserList",//页面table的dom ID
			        	mDtTableName:"ROLE_USER",//数据表名
			            sAjaxSource: path+"/sys/user!getRoleGrantUsers.ec",//获取列表数据url
			            fnServerParams: function ( aoData ) {
					        aoData.push(
							        {"name": "ROLE_ID","value":$roleID_tmp}
							);
					    }
			        };
			    buildTable(tableConf);
			    initRoleInfoData();
			});
		</script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12"> 	
					 <div class="row-fluid">
					 	 <!-- 组织架构 and用户-->
					 	 <div  class="span12 box" style="min-height:600px;overflow-y: auto;">
					 	 	<div class="box-header">
								<i class="icon-group"></i><span class="break"></span>角色详情
								<div class="box-icon">
									<button class="btn btn-mini grey-mint" onclick="history.go(-1)"><i class="icon-share-alt"></i> 返回</button>
								</div>
							</div>
							<div class="box-content">
						 	 	<div class="span5"> 	
									<div class="row-fluid">
							            <label class="yf-control-label" >角色Code：</label>
							            <input name="ROLE_ID_ReadOnly" type="text" readonly="readonly">
							        </div>
							 	</div>
							 	<div class="span5"> 
								 	<div class="row-fluid">	
								        <label class="yf-control-label"  >角色描述：</label>
								        <input name="ROLE_NAME_ReadOnly" type="text" readonly="readonly">
								    </div>
							 	</div>
							 	<div class="span5" style="margin-left:0px;"> 
								 	<div class="row-fluid">	
								        <label class="yf-control-label" >备注信息：</label>
								        <textarea rows="3" id="REMARK_ReadOnly" readonly="readonly">${REMARK}</textarea>
								    </div>
							 	</div>
							 	
								
						 	 	<table id="roleUserList" class="table table-striped bootstrap-datatable datatable">
						     	</table>
						     	
						     	
						     
						     </div>
					 	 </div>
					 </div>
		             
				</div>
			</div>
		</div>
	</body>
</html>
