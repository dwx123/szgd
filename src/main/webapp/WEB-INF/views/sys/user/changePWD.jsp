<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>



<form id="changePwdform" class="form-horizontal opend_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <b>修改密码</b>
                <%--<a data-dismiss="modal"><i class="fa fa-close"></i> </a>--%>
            </div>

            <div class="modal-body container-fluid">
                <div class="note note-info">
                    <span><strong>提示：</strong> 只接受英文字母、数字和下划线，不接受特殊字符，长度3~16个字符。</span><br/>
                    <!-- <span class="result-note"></span>	 -->
                </div>
                &nbsp;
                <div class="form-group">
                    <label class="col-xs-2 control-label"><span class="required"> * </span>原始密码</label>

                    <div class="col-xs-6">
                        <input type="password" name="oldpwd" placeholder="请输入原密码" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-2 control-label"><span class="required"> * </span>新密码</label>
                    <div class="col-xs-6">
                        <input type="password" id="newPwd" name="newPwd" placeholder="请输入新密码" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-2 control-label"><span class="required"> * </span>密码确认</label>
                    <div class="col-xs-6">
                        <input type="password" name="PASSWORD" id="PASSWORD" placeholder="请重复新密码" class="form-control">
                    </div>
                </div>
                <div class="text-center">
                    <button type="button" onclick="saveNewPwd()" class="btn btn-success"><i class="fa fa-save"></i> 确定</button>
                    &nbsp;&nbsp;
                    <button type="button" data-dismiss="modal" class="btn btn-default"><i class="fa fa-close"></i> 取消</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
    $(function(){
        $('input[name=oldpwd]').val('').focus();
    });
    function saveNewPwd() {
        var options = {
            url:       '${ctx}/user/setPassword',
            type:      'post',
            dataType:  'json',
            beforeSubmit:  showRequest,
            success:       showResponse
        };
        //表单验证

        $('#changePwdform').ajaxSubmit(options);
    }
    function showRequest(){
        return true;
    }
    function showResponse(data){
        if(data.message=='success'){
            if($('input[name=oldpwd]').val()==$('input[name=newPwd]').val()){
                notyWarning("新旧密码不能相同！");
                $('input[name=oldpwd]').val('').focus();
                $('input[name=newPwd]').val('');
                $('input[name=PASSWORD]').val('');
                return;
            }
            else{
                notySuccess("修改密码成功！");
                $("#_changePwdWin").modal('hide');
            }
        }else if(data.message=='error'){
            notyError("原密码输入不正确，请重新输入！");
            $('input[name=oldpwd]').val('').focus();
            $('input[name=newPwd]').val('');
            $('input[name=PASSWORD]').val('');
        }else{
            notyError("修改密码失败，请稍后重试或联系管理员！");
            $("#_changePwdWin").modal('hide');
        }
    }
</script>