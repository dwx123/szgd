<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!doctype html>
<html>
<head>
<title>用户管理</title>
	<jsp:include page="/common/init.jsp"></jsp:include>
	<link href="${ctx}/assets/zTree/css/zTreeStyle/zTreeStyle.css" type="text/css" rel="stylesheet" />
	<script src="${ctx}/assets/zTree/js/jquery.ztree.all.js" type="text/javascript" charset="UTF-8"></script>
	<script src="${ctx }/js/common.js" type="text/javascript"></script>
	<script type="text/javascript">
        var path='<%=request.getContextPath() %>';
		var selectedData;
		var editMark = true;
		var editMarkUser = true;
        var userTable;
        var selDeptID;
        var selDeptCd;
        var selDeptName;
        var selDeptShortName;
        var set;
    	function addToSet(rowno){
    		if($('#ckb'+rowno).is(":checked")){
    			set.add(rowno);
    		}else{
    			set.remove(rowno);
    		}
    	}
    	
    	function personDataFresh(){
    		$("#personInfoTable").DataTable().ajax.reload();
    	}

		var deptTree = null;
		var deptTreeNodes = null;
	$(function() {
		selDeptID = "";
		selDeptCd = "";
		selDeptName = "";
		selDeptShortName = "";
		set = new Set();
		// -- 用户查询  --
		 userTable = $("#userList").DataTable({
//			 "bSort": true,
             "sDom" : "<'row-fluid'<'pull-left filterInput'f>>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
             fnDrawCallback : addSelectListener,
			sAjaxSource : path + "/user/getUserList",//获取列表数据url,
			//table 表头
            aoColumns: [
                {'mDataProp':'LOGIN_ID' , 'sTitle' : '登录名','sClass' : 'center'},
                {'mDataProp':'USER_NAME' ,'sTitle' : '用户','sClass' : 'center'},
                {'mDataProp':'SORT_NUMBER' ,'sTitle' : '排序','sClass' : 'center'},
                {'mDataProp':'DEPT_NAME' ,'sTitle' : '部门','sClass' : 'center',"sDefaultContent" : ""}
                ],
			//渲染某列格式
            aoColumnDefs: [
                {
                    aTargets: ["_all"],	//指向某一列，0为第一列
                    mRender: function (data, type, row) {
                        var subData = data;
                        if (data instanceof Object) {
                            try {
                                return new Date(data.time).Format("yyyy-MM-dd hh:mm:ss");
                            } catch (e) {
                                subData = data;
                            }
                        }
                        return subData;

                    }
                }
            ],
			//向后台提交url携带的参数
			fnServerParams : function(aoData) {
				aoData.push({
					"name" : "dept_id",
					"value" : selDeptID

				});

			}
		});
		
		 $("#personInfoTable").DataTable({
             iDisplayLength: 5,
             "sDom" : "<'row-fluid'<'pull-left filterInput'f>>t<'row-fluid'<'span4'il><'span7'<'pull-right'p>>>",
             //dom:'tiprl',
             iDisplayStart: 0,
             //"sScrollY": 450, //高度
             /* sScrollXInner: "150%",
             sScrollX: "100%", */
             bAutoWidth: false,
             "bSort": false,
             bFilter:false,
             sAjaxSource : path + "/personnelInfo/getPersonnelByUser",//获取列表数据url,
             
             aoColumns: [
                {'mDataProp':'ID' ,'sTitle' : '<input type = "checkbox" id="all_checked" />','sClass' : 'center','bSortable':false},
                {'mDataProp':'NAME' , 'sTitle' : '姓名','sClass' : 'center'},
                {'mDataProp':'ID_NUMBER' ,'sTitle' : '证件号码','sClass' : 'center'},
                {'mDataProp':'SEX_NAME' ,'sTitle' : '性别','sClass' : 'center'},
                {'mDataProp':'TEL' ,'sTitle' : '电话','sClass' : 'center',"sDefaultContent" : ""},
                {'mDataProp':'WORK_TYPE_NAME' ,'sTitle' : '岗位','sClass' : 'center',"sDefaultContent" : ""},
                {'mDataProp':'SFCZYH' ,'sTitle' : '是否已经是用户','sClass' : 'center',"sDefaultContent" : ""}
             ],
             
             aoColumnDefs: [
				{
				    aTargets: [0],	//指向某一列，0为第一列
				    mRender: function (data, type, row) {
				        var domHtml = "";
				        domHtml += "<input type = 'checkbox' id='ckb"+data+"' onclick='addToSet(this.value);' name='ckb' class='checkRow' value='"+data+"'>";
				        return domHtml;
				    }
				}
             ],
             "fnDrawCallback" : function(){
 				$("#all_checked").attr("checked",false);
// 				$("#jfsqTable_paginate a").on("click", function () {
//     				var ckb = $("input:checkbox[name='ckb']:checked");
//     				for(var i = 0, iLen = ckb.length; i < iLen; i++){
//     					set.add(ckb.eq(i).val());
//     				}
//     		    });
				console.log(set);
 				var array = set.valuesLegacy();
 				for(var i = 0, iLen = array.length; i < iLen; i++){
 					$("#ckb"+array[i]).attr('checked',true);
 				}
 			},
             //向后台提交url携带的参数
             fnServerParams : function(aoData) {
                 aoData.push({"name":"xmcx","value":$('#xmcx').val()}
                 ,{"name":"zjhmcx","value":$('#zjhmcx').val()}
                 ,{"name":"gwcx","value":$('#gwcx').val()}
                 ,{"name":"sfczyhcx","value":$('#sfczyhcx option:selected').val()}
                 );
             }
         });
		
          $('#all_checked').click(function (){
  			if($('#all_checked').is(":checked")){
  				$('input.checkRow').prop('checked',true);
                  $('input.checkRow').each(function () {
  					set.add($(this).val())
                  })
  			}else{
  				$('input.checkRow').prop('checked',false);
                  $('input.checkRow').each(function () {
                      set.remove($(this).val())
                  })
  			}
  		});
		
		// -- 部门组织架构树  --
		try {
			//部门树设置
			var deptTreeSet = {
				async: {
					enable: true,
					autoParam: ['DEPT_ID'],
					contentType: 'application/x-www-form-urlencoded',
					url: path + "/dept/getAllChildById",
					dataType: 'json',
					type: 'post'
				},
				data : {
					key : {
						name : "DEPT_NAME" // 节点名称的属性
					},
					simpleData : {
						enable : true, // 使用简单数据模型（array），但请注意simpleData内的参数（idKey，pIdKey，rootPId）满足父子关系
						idKey : "DEPT_ID",
						pIdKey : "PARENTID",
						rootPId : "-1"
					}
				},
				callback : {
					onClick : function(e, treeId, treeNode) { // 节点点击事件
						/* if(!treeNode.isParent){ // 叶子节点
							deptTreeNodes = treeNode;
							selDeptCD = "'" +treeNode.DEPT_CD+ "'";
							// 刷新用户列表
							$('#userList').dataTable().fnReloadAjax();
						}else{				// 非叶子节点
							var leafDeptCDArray = [];
							recursionSearch(treeNode,leafDeptCDArray);
							selDeptCD = leafDeptCDArray.toString();
						} */
						selDeptID = treeNode.DEPT_ID;
						selDeptCd = treeNode.DEPT_CD;
						selDeptName = treeNode.DEPT_NAME;
						selDeptShortName = treeNode.DEPT_SHORT_NAME;
						// 刷新用户列表
						userTable.ajax.reload();
					}
				}
			};
			//加载部门和人员树
			$.ajax({
				async : false,
				cache : false,
				type : 'POST',
				dataType : "json",
				url : path + "/dept/getAllDeptInfo",
				error : function(xhr, textStatus, exception) {
					notyError('部门数据加载失败!' + xhr.responseText);
				},
				success : function(data) {
					deptTree = $.fn.zTree.init($("#deptTree"), deptTreeSet,
							data);
					// 展开根节点
					deptTree.expandNode(deptTree.getNodeByParam("PARENTID",
							"-1", null), true, false, false);
				}
			});
		} catch (e) {
			//notyError (e);
		}
		$("#handsyn").click(function() {
			notyInfo("同步用户请求已发送，稍后会通知是否成功！");
			$.ajax({
				async : true,
				cache : false,
				type : 'POST',
				dataType : "json",
				url : path + "/user/handsyn",
				error : function(xhr, textStatus, exception) {
					notyError ('失败!' + xhr.responseText);
				},
				success : function(data) {
					var result = eval(data);
					notySuccess(result.msg);
                    userTable.ajax.reload();
				}
			});
		});

		$("#resetPwBtn").click(function(){
			if (!selectedData) {
				notyInfo('请选择一条记录');
				return;
			}
            if (!confirm("您确定要重置该用户的密码？")) {
                return;
            }
			$.ajax({
				async : true,
				cache : false,
				type : 'POST',
				dataType : "json",
				data :{ "LOGIN_ID" : selectedData.LOGIN_ID,
						"USER_ID" : selectedData.USER_ID
					},
				url : path + "/user/resetPwd",
				error : function(xhr, textStatus, exception) {
					notyError ('失败!' + xhr.responseText);
				},
				success : function(data) {
					var result = eval(data);
					if("success" == result.message){
                        notySuccess("成功重置用户["+selectedData.USER_NAME+"]的密码为123456");
                        userTable.ajax.reload();
					}else{
						notyError ("重置用户["+selectedData.USER_NAME+"]的密码失败！");
					}
				}
			});
		});

        $("#delUser").click(function(){
            if (!selectedData) {
                notyInfo('请选择一条记录');
                return;
            }
            if (!confirm("您确定要删除该用户？")) {
                return;
            }
            var param = {
                "loginId": selectedData.LOGIN_ID,
                "userId": selectedData.USER_ID
            };
            $.ajax({
                contentType: "application/json",
                url : '${ctx}/user/delUser',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        userTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error));
                }
            });
        });
		
		
		$("#btn_addDept").click(function(){
			if (!selDeptID) {
				notyInfo('请选择父部门！');
				return;
			}
			editMark = false;
			$("#DEPT_ID").val("");
			$("#DEPT_CD").val("");
			$("#DEPT_NAME").val("");
			$("#DEPT_SHORT_NAME").val("");
            $("#deptEditModel").modal('show');

		});
		
		$("#btn_editDept").click(function(){
			if (!selDeptID) {
				notyInfo('请选择父部门！');
				return;
			}
			editMark = true;
			$("#DEPT_ID").val(selDeptID);
			$("#DEPT_CD").val(selDeptCd);
			$("#DEPT_NAME").val(selDeptName);
			$("#DEPT_SHORT_NAME").val(selDeptShortName);
            $("#deptEditModel").modal('show');

		});
		
		$("#addUser").click(function(){
			if (!selDeptID) {
				notyInfo('请选择一个部门！');
				return;
			}
			editMarkUser = false;
			$("#USER_ID").val("");
			$("#LOGIN_ID").val("");
			$("#LOGIN_ID").removeAttr("readonly");
			$("#USER_PWD").val("");
			$("#USER_NAME").val("");
            $("#DEPT_CD_USER").val("");
            $("#FLAG_USER").val("");
            $("#col_company").hide();
            $("#userEditModel").modal('show');

		});
		
		$("#addUserFromPerson").click(function(){
			if (!selDeptID) {
				notyInfo('请选择一个部门！');
				return;
			}
			$("#xmcx").val("");
			$("#zjhmcx").val("");
			$("#gwcx").val("");
			set = new Set();
			$('#personInfoTable').DataTable().ajax.reload();
            $("#userSelModel").modal('show');

		});
		
		$("#editUser").click(function(){
			if (!selectedData) {
				notyInfo('请选择一条记录');
				return;
			}
			editMarkUser = true;
			$("#USER_ID").val(selectedData.USER_ID);
			$("#LOGIN_ID").val(selectedData.LOGIN_ID);
			$("#LOGIN_ID").attr("readonly","readonly");
			$("#USER_PWD").val(selectedData.USER_PWD);
			$("#USER_NAME").val(selectedData.USER_NAME);
            $("#DEPT_CD_USER").val(selectedData.DEPT_CD);
			$("#company").val(selectedData.DEPT_CD);
			$("#company").trigger("change");
            $("#FLAG_USER").val("1");
            $("#userEditModel").modal('show');

		});
		
	});

	function inputClickDept(deptCd_ID,deptName_ID) {
		initDeptTree(deptCd_ID,deptName_ID);
		$('#deptModal').modal('show');

	}

	function addSelectListener() {
		//找到列表中的数据行
		var rows = $("#userList tr");
		$.each(rows, function(index, row) {
			if (index > 0) {
				$(row).click(
						function() {
							selectedData = $('#userList').dataTable().fnGetData(this);
							$("#choicedUserName").html("<strong>"+selectedData.USER_NAME+"</strong> ["+selectedData.LOGIN_ID+"]");
							$(row).addClass("info");
							$(row).siblings().removeClass("info");
						});
			}
		});
	}
	/**
	 *根据部门查询用户，部门层级不确定，用户只关联到最底层叶子节点部门
	 *因此：选中非叶子节点部门后，递归查询节点下所有叶子节点
	 * -- 暂不使用，采用了oracle自带的递归方法 start with...connect by prior
	 */
	function recursionSearch(treeNode, leafDeptCDArray) {
		var nodes = treeNode.children;
		$.each(nodes, function(index, node) {
			if (node.isParent) {
				leafDeptCDArray = recursionSearch(node, leafDeptCDArray);
			} else {
				leafDeptCDArray.push("'" + node.DEPT_CD + "'");
			}
		});
		return leafDeptCDArray;
	}

	
	function checkForm() {
        if($("#DEPT_NAME").val() == "" || $("#DEPT_CD").val() == ""){
            return false;
        }else{
            return true;
		}
    }
	
	function checkUserForm() {
        if($("#LOGIN_ID").val() == "" || $("#USER_PWD").val() == "" || $("#USER_NAME").val() == ""){
            return false;
        }else{
            return true;
		}
    }
	
	// 新增/更新流程角色
	function saveDeptInfo() {
        if(!checkForm()){
            notyWarning("必填项不能为空！");
		}else{
			$('#DEPT_PARENT_ID').val(selDeptID);
			$('#FLAG').val("1");
			var url = path + '/dept/insertDept';
			if (editMark) {
				url = path + '/dept/updateDept';
			}
			var options = {
				url : url,
				type : 'post',
				dataType : 'json',
				//data:{'ROLE_ID':$('#ROLE_ID').val(),'ROLE_NAME':$('#ROLE_NAME').val(),'REMARK':$('#REMARK').val()},
				beforeSubmit : showWfRequest,
				success : editWfRoleSuccessCallback
			};
			$('#deptForm').ajaxForm(options);
			
        }
	}
	
	// 新增/更新用户
	function saveUserInfo() {
        if(!checkUserForm()){
            notyWarning("必填项不能为空！");
		}else{
			$('#DEPT_CD_USER').val(selDeptCd);
			$('#FLAG_USER').val("1");
			var url = path + '/user/insertUser';
			if (editMarkUser) {
				url = path + '/user/updateUser';
			}
			var options = {
				url : url,
				type : 'post',
				dataType : 'json',
				//data:{'ROLE_ID':$('#ROLE_ID').val(),'ROLE_NAME':$('#ROLE_NAME').val(),'REMARK':$('#REMARK').val()},
				beforeSubmit : showWfRequest,
				success : editWfRoleSuccessCallbackUser
			};
			$('#userForm').ajaxForm(options);
			
        }
	}
	
	function savePersonInfo() {
		var array = set.valuesLegacy();
        if(array.length > 0){
        	$('#DEPT_CD_USER_PERSON').val(selDeptCd);
			$('#FLAG_USER_PERSON').val("1");
			$('#PERSONIDS').val(array);
			var url = path + '/user/insertUserFromPerson';
			var options = {
				url : url,
				type : 'post',
				dataType : 'json',
				/* data:{'FLAG_USER':$('#FLAG_USER').val(),'DEPT_CD_USER':$('#DEPT_CD_USER').val(),'PERSONIDS':array}, */
				beforeSubmit : showWfRequest,
				success : personWfRoleSuccessCallbackUser
			};
			$('#userSelForm').ajaxForm(options);
		}else{
            notyWarning("必须选择至少一条数据！");
        }
	}

	function showWfRequest() {
		return true;
	}

	function editWfRoleSuccessCallback(data) {
		if (data.success == true) {
			//$('#sysTable').hideLoading();  
			$('#deptEditModel').modal('hide');
			$('#userList').DataTable().ajax.reload();
			notySuccess(data.message);
			var $nodes = deptTree.getSelectedNodes();
			console.log($nodes)
			for (var i=0; i<$nodes.length; i++) {
				deptTree.reAsyncChildNodes($nodes[i],'refresh',false);

				$nodes[i].DEPT_CD = $("#DEPT_CD").val();
				$nodes[i].DEPT_NAME = $("#DEPT_NAME").val();
				$nodes[i].DEPT_SHORT_NAME = $("#DEPT_SHORT_NAME").val();
				deptTree.updateNode($nodes[i]);
			}

		//更新节点值


		} else {
			notyError(data.message);
		}
	}
	
	function editWfRoleSuccessCallbackUser(data) {
		if (data.success == true) {
			//$('#sysTable').hideLoading();  
			$('#userEditModel').modal('hide');
			$('#userList').DataTable().ajax.reload();
			notySuccess(data.message);
		} else {
			notyError(data.message);
		}
	}
	
	function personWfRoleSuccessCallbackUser(data) {
		if (data.success == true) {
			//$('#sysTable').hideLoading();  
			$('#userSelModel').modal('hide');
			$('#userList').DataTable().ajax.reload();
			$('#personInfoTable').DataTable().ajax.reload();
			notySuccess(data.message);
		} else {
			notyError(data.message);
		}
	}

</script>
	<style>
		.opend_modal .modal-body .form-group .form-control{
			width:100%;
		}
	</style>
</head>
<body>
<div class="bg-white item_title">
	用户管理
</div>
<div class="bg-white">
	<div class="page-content case-info-content">
		<div class="row">
			<!-- 组织架构 -->
			<div class="col-xs-3">
				<div class="box">
					<div class="box-header">
						<span><h4><i class="fa fa-sitemap"></i> 组织机构树</h4></span>
						<div class="box-icon" style="text-align: right;">
							<button class="btn btn-md btn-primary" id="btn_addDept">
								<%--<i class="fa fa-plus"></i> --%>新增
							</button>
							&nbsp;&nbsp;
							<button class="btn btn-md btn-success" id="btn_editDept">
								<%--<i class="fa fa-edit"></i> --%>编辑
							</button>
						</div>
					</div>
					<br/>
					<div class="box-content">
						<ul id="deptTree" class="ztree"></ul>
					</div>
				</div>
			</div>
			<!-- 警员信息 -->
			<div class="col-xs-9">
				<div class="box">
					<div class="box-header">
						<span><h4><i class="fa fa-users"></i> 用户列表</h4></span>
						&nbsp;&nbsp;
						 <span id="choicedUserName"></span>

						<span class="pull-right">
							<a href="javascript:void(0);" id="addUserFromPerson" class="btn btn-md btn-primary" title="选取人员快速新增用户（推荐）"> 选取人员快速新增用户（推荐）</a>
							&nbsp;&nbsp;
							<a href="javascript:void(0);" id="addUser" class="btn btn-md btn-primary" title="手工新增用户"> 手工新增用户</a>
							&nbsp;&nbsp;
							<a href="javascript:void(0);" id="editUser" class="btn btn-md btn-success" title="点击按钮编辑用户数据"> 编辑用户</a>
							&nbsp;&nbsp;
							<%--<a href="javascript:void(0);" id="handsyn" class="btn btn-md btn-info" title="点击按钮同步用户数据"><i class="fa fa-exchange"></i> 同步全部用户</a>
							&nbsp;&nbsp;--%>
							<a href="javascript:void(0);" id="resetPwBtn" class="btn btn-md btn-danger" title="重置选中用户的密码"><!--<i class="fa fa-retweet"></i>--> 重置密码</a>
							&nbsp;&nbsp;
							<a href="javascript:void(0);" id="delUser" class="btn btn-md btn-danger" title="点击按钮删除用户数据"> 删除用户</a>
						</span>
					</div>
					<br/>
					<div class="box-content">
						<table id="userList" class="table table-striped table-bordered display nowrap self_table" cellspacing="0" width="100%">
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- 部门新增更新Modal -->
<div id="deptEditModel" class="modal fade" data-backdrop="static">
	<form id="deptForm" name="deptForm" class="form-horizontal opend_modal" onsubmit="return checkForm();">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<a data-dismiss="modal"><i class="fa fa-close"></i> </a>
					组织机构
				</div>
				<div class="modal-body container-fluid">
						<div class="form-group">
							<label class="col-xs-3 control-label"><span class="required"> * </span>部门名称</label>
							<div class="col-xs-9">
								<input id="DEPT_NAME" name="DEPT_NAME" type="text" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-3 control-label">&nbsp;&nbsp;部门简称</label>
							<div class="col-xs-9">
								<input id="DEPT_SHORT_NAME" name="DEPT_SHORT_NAME" type="text" class="form-control" />
								<div class="help-block"></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-3 control-label"><span class="required"> * </span>部门编码</label>
							<div class="col-xs-9">
								<input id="DEPT_CD" name="DEPT_CD" type="text" class="form-control"/>
								<div class="help-block"></div>
							</div>
						</div>
						<input id="DEPT_PARENT_ID" name="DEPT_PARENT_ID" type="hidden" class="form-control"/>
						<input id="DEPT_ID" name="DEPT_ID" type="hidden" class="form-control"/>
						<input id="FLAG" name="FLAG" type="hidden" class="form-control"/>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-md btn-success" onclick="saveDeptInfo()"><i class="fa fa-save"></i> 保存</button>
					<button type="reset" class="btn btn-md btn-warning" style="display: none">重置</button>
					&nbsp;&nbsp;
					<button class="btn btn-md btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> 取消</button>
				</div>
			</div>
		</div>
	</form>
</div>

<!-- 用户新增更新Modal -->
<div id="userEditModel" class="modal fade" data-backdrop="static">
	<form id="userForm" name="userForm" class="form-horizontal opend_modal" onsubmit="return checkUserForm();">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<a data-dismiss="modal"><i class="fa fa-close"></i> </a>
					用户信息
				</div>
				<div class="modal-body container-fluid">
					<div class="form-group">
						<label class="col-xs-3 control-label"><span class="required"> * </span>登录用户</label>
						<div class="col-xs-9">
							<input id="LOGIN_ID" name="LOGIN_ID" type="text" class="form-control"/>
						</div>
					</div>
					<%--<div class="form-group">--%>
						<%--<label class="col-xs-3 control-label"><span class="required"> * </span>用户密码</label>--%>
						<%--<div class="col-xs-9">--%>
							<%--<input type="password" id="USER_PWD" name="USER_PWD" class="form-control">--%>
							<%--<div class="help-block"></div>--%>
						<%--</div>--%>
					<%--</div>--%>
					<div class="form-group">
						<label class="col-xs-3 control-label"><span class="required"> * </span>用户名称</label>
						<div class="col-xs-9">
							<input id="USER_NAME" name="USER_NAME" type="text" class="form-control"/>
						</div>
					</div>
					<div class="form-group" id="col_company">
						<label class="col-xs-3 control-label">&nbsp;&nbsp;所属部门</label>
						<div class="col-xs-9">
							<select class="form-control input-sm select2" name="company" id="company">
								<option value="">请选择</option>
								<c:forEach items="${deptList}" var="item">
									<option value="${item.DEPT_CD}">${item.DEPT_NAME}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<input id="DEPT_CD_USER" name="DEPT_CD" type="hidden" class="form-control"/>
					<input id="FLAG_USER" name="FLAG" type="hidden" class="form-control"/>
					<input id="USER_ID" name="USER_ID" type="hidden" class="form-control"/>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-md btn-success" onclick="saveUserInfo()"><i class="fa fa-save"></i> 保存</button>
					<button type="reset" class="btn btn-md btn-warning" style="display: none">重置</button>
					&nbsp;&nbsp;
					<button class="btn btn-md btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> 取消</button>
				</div>
			</div>
		</div>
	</form>
</div>


<div id="userSelModel" class="modal fade" data-backdrop="static">
	<form id="userSelForm" name="userSelForm" class="form-horizontal opend_modal">
		<div class="modal-dialog" style="width: 800px;">
			<div class="modal-content">
				<div class="modal-header">
					<a data-dismiss="modal"><i class="fa fa-close"></i> </a>
					人员信息
				</div>
				<div class="modal-body container-fluid">
						<div class="form-group">
							<label class="col-xs-3 control-label">姓名</label>
							<div class="col-xs-3">
								<input id="xmcx" type="text" class="form-control"/>
							</div>
							<label class="col-xs-3 control-label">证件号码</label>
							<div class="col-xs-3">
								<input id="zjhmcx" type="text" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-3 control-label">岗位</label>
							<div class="col-xs-3">
								<input type="text" id="gwcx" class="form-control">
							</div>
							<label class="col-xs-3 control-label">是否已经是用户</label>
							<div class="col-xs-3">
								<select id="sfczyhcx" class="form-control" >
									<option value="">全部</option>
									<option value="是">是</option>
									<option value="否">否</option>
								</select>
							</div>
						</div>
						<input id="DEPT_CD_USER_PERSON" name="DEPT_CD_USER" type="hidden" class="form-control"/>
						<input id="FLAG_USER_PERSON" name="FLAG_USER" type="hidden" class="form-control"/>
						<input id="PERSONIDS" name="PERSONIDS" type="hidden" class="form-control"/>
						<div class="form-group" style="text-align: right;">
							<input type="button" onclick="personDataFresh();" class="btn btn-md btn-primary mr15" value="查询"/>
						</div>
						<div class="form-group">
							<table class="table table-striped table-hover table-bordered self_table display" id="personInfoTable">
							</table>
						</div>
					
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-md btn-success" onclick="savePersonInfo()"><i class="fa fa-save"></i> 新增用户</button>
					<button type="reset" class="btn btn-md btn-warning" style="display: none">重置</button>
					&nbsp;&nbsp;
					<button class="btn btn-md btn-default" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i> 取消</button>
				</div>
			</div>
		</div>
	</form>
</div>

</body>
</html>
