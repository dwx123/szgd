<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>车辆信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear: true
            });

            var opt = "${opt}";
            if(opt == "E"){
                $('#plateNumber').attr("disabled",true);
            }

            $('#saveVehicleInfo').click(function () {
                var valid = $("#vehicleInfoForm").validationEngine("validate");
                if (!valid)
                    return;
                var param = serializeObject('#vehicleInfoForm');
                var otherParam = {};
                var jsonString = JSON.stringify($.extend(param, otherParam));
                $.ajax({
                    contentType: "application/json",
                    url : '${ctx}/vehicle/saveVehicleInfo',
                    type : 'POST',
                    data: jsonString,
                    success : function (data) {
                        var resultCode = data.resultCode;
                        if(resultCode == 1)
                        {
                            notySuccess(data.resultMsg);
                            setTimeout(function(){
                                history.go(-1);
                            },1500);

                        }else{
                            notyError(data.resultMsg);
                        }
                    },
                    error: function (request, status, error) {
                        notyError(JSON.stringify(error));
                    }
                });

            });
        });

        function inputClickDept(deptCd_ID,deptName_ID) {
            initDeptTree(deptCd_ID,deptName_ID);
            $('#deptModal').modal('show');
        }
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/vehicle/toVehicleInfoList" title="点击返回至列表">车辆信息管理</a>
    <i class="fa fa-angle-double-right"></i>
    <c:if test="${opt == 'A'}">
        车辆信息增加
    </c:if>
    <c:if test="${opt == 'E'}">
        车辆信息编辑
    </c:if>
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="vehicleInfoForm">
        <input type="hidden" name="id" value="${vehicleInfo.id}" id="id">

        <input type="hidden" id="opt" name="opt" value="${opt}">
        <div class="form-body mb10 bg-blue pb5">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>车牌号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required,maxSize[50]]"  id = "plateNumber" name="plateNumber"  placeholder="" value="${vehicleInfo.plateNumber}" type="text">
                </div>

                <label class="col-md-2 control-label">车型</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="vehicleType" id="vehicleType">
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.VEHICLE_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq vehicleInfo.vehicleType}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>

                    </select>
                </div>
                <%--<label class="col-md-2 control-label">车牌类型</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="plateType" id="plateType">
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.PLATE_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq vehicleInfo.plateType}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>--%>

            </div>
            <div class="row">
                <label class="col-md-2 control-label">联系人</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[maxSize[50]]"  id = "inCharge"  name="inCharge" placeholder="" value="${vehicleInfo.inCharge}" type="text">
                </div>

                <label class="col-md-2 control-label">联系人电话</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[maxSize[50],custom[phone]]"  id = "inChargeTel"  name="inChargeTel" placeholder="" value="${vehicleInfo.inChargeTel}" type="text">
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 control-label">所属单位</label>
                <div class="col-md-3 pt7">
                    <input type="hidden" class="form-control validate[]" id="useDept" name="useDept">
                    <input onclick ="inputClickDept('useDept','deptName')" value="${vehicleInfo.deptName}"  id="deptName" class="form-control validate[]">
                </div>
                <label class="col-md-2 control-label"></label>
                <div class="col-md-3 pt7">
                    <%--<select  class="form-control input-sm select2 validate[required]" id = 'isOnSiteSupervise' name="isOnSiteSupervise">
                        <option value="">请选择</option>
                        <option value="0" <c:if test="${vehicleInfo.isOnSiteSupervise eq 0}">selected</c:if>>否</option>
                        <option value="1" <c:if test="${vehicleInfo.isOnSiteSupervise eq 1}">selected</c:if>>是</option>
                    </select>--%>
                </div>
                <%--<label class="col-md-2 control-label">车牌颜色</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="plateColor" id="plateColor">
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.COLOR_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq vehicleInfo.plateColor}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>--%>
            </div>
            <%--<div class="form-group">


                <label class="col-md-2 control-label">品牌</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="vehicleBrand" id="vehicleBrand" >
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.VEHICLE_BRAND}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq vehicleInfo.vehicleBrand}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">车辆颜色</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="vehicleColor" id="vehicleColor" >
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.COLOR_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq vehicleInfo.vehicleColor}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

            </div>--%>
            <%--<div class="form-group">
                <label class="col-md-2 control-label">车辆大小</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2" id = 'vehicleSize' name="vehicleSize" >
                        <option value=""> 请选择</option>
                        <option value="1" <c:if test="${vehicleInfo.vehicleSize eq 1}">selected</c:if>>大</option>
                        <option value="2" <c:if test="${vehicleInfo.vehicleSize eq 2}">selected</c:if>>中</option>
                        <option value="3" <c:if test="${vehicleInfo.vehicleSize eq 3}">selected</c:if>>小</option>
                    </select>
                </div>
                <label class="col-md-2 control-label"></label>
                <div class="col-md-3 pt7">
                </div>

                <label class="col-md-2 control-label"></label>
                <div class="col-md-3 pt7">
                </div>
            </div>--%>
        </div>
        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <button type="button" class="btn btn-md btn-success" id="saveVehicleInfo"><i
                            class="fa fa-save"></i> 保存
                    </button>
                    &nbsp;&nbsp;
                    <a href="${ctx}/vehicle/toVehicleInfoList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>
<%@include file="../../choiceDeptModal.jsp"%>
</body>
</html>
