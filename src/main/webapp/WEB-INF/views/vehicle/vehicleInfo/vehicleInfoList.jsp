<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>车辆基础信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>
    <%@ include file="/common/include-css.jsp"%>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        var vehicleInfoTable ;
        $(function () {
            vehicleInfoTable = $("#vehicleInfoTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/vehicle/getVehicleInfoList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'plateNumber', 'sTitle': '车牌', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'vehicleTypeName', 'sTitle': '车型', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'deptName', 'sTitle': '所属单位', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'inCharge', 'sTitle': '联系人', 'sWidth': '10%', 'sClass': 'center'},
                    {'mDataProp': 'inChargeTel', 'sTitle': '联系电话', 'sWidth': '10%', 'sClass': 'center'},
                    /*{'mDataProp': 'isOnSiteSupervise', 'sTitle': '是否监理旁站', 'sWidth': '10%', 'sClass': 'center'},*/
                    {'mDataProp': null, 'sTitle': '操作', 'sWidth': '10%', 'sClass': 'center'}
                ],
                //渲染某列格式
                aoColumnDefs: [
                    /*{
                        aTargets: [5],	//指向某一列，0为第一列
                        mRender: function (data, type, row) {
                            var v = "";
                            if (data == 0) {
                                v = '否';
                            }
                            if (data == 1) {
                                v = '是';
                            }
                            return v;

                        }
                    },*/
                    {
                        "targets":-1,
                        "bSortable": false,
                        mRender: function (data, type, row) {
                            var domHtml = "";
                            domHtml += '<a class="btn-sm btn-smd btn btn-primary" href="'+ path +'/vehicle/toVehicleInfoView?id='+ row.id +'" title="查看车辆信息">查看</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-success"  href="' + path + '/vehicle/toVehicleInfoForm?opt=E&id='+ row.id +'" title="修改车辆信息">编辑</a>&nbsp;&nbsp;';
                            domHtml += '<a class="btn-sm btn-smd btn btn-danger" href="javascript:;" title="删除车辆信息" onclick="delVehicleInfo(\''+ row.id + '\')">删除</a>';
                            return domHtml;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#vehicleInfoForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val

                        });
                    });
                }

            });
            $("#findVehicleInfo").click(function () {
                vehicleInfoTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('vehicleInfoForm');
            })
        });

        function delVehicleInfo(id) {
            var r=confirm("确认删除？")
            if (r==false){
                return
            }
            var param = {
                id: id
            }

            $.ajax({
                contentType: "application/json",
                url : '${ctx}/vehicle/deleteVehicleInfo',
                type : 'POST',
                data: JSON.stringify(param),
                success : function (data) {
                    var resultCode = data.resultCode;
                    if(resultCode == 1)
                    {
                        notySuccess(data.resultMsg);
                        vehicleInfoTable.ajax.reload();
                    }else{
                        notyError(data.resultMsg);
                    }
                },
                error: function (request, status, error) {
                    notyError(JSON.stringify(error))
                }
            });
        }

        function checkUploadVehicleInfoForm() {
            var fileDir = $("#vehicleInfoFile").val();
            if ("" == fileDir) {
                notyWarning("选择需要导入的文件！");
                return false;
            }
            var suffix = fileDir.substr(fileDir.lastIndexOf("."));
            if (".xls" != suffix && ".xlsx" != suffix) {
                notyWarning("选择Excel格式的文件导入！");
                return false;
            }
            return true;
        }
        function uploadVehicleInfo() {
            if (checkUploadVehicleInfoForm()) {
                function resutlMsg(msg) {
                    if (msg =="文件导入失败！"){
                        $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                    }else{
                        if(msg.indexOf("#WEBROOT#") != -1){
                            msg  = msg.replace("#WEBROOT#", path +"/upload");
                        }
                        $("#resultMsg").html("<font color='#006400'>" + msg +"</font>");
                    }
                    $("#file").val("");
                    $("#vehicleInfoTable").DataTable().ajax.reload();
                }
                function errorMsg() {
                    $("#resultMsg").html("<font color='#ff4500'>" + msg +"</font>");
                }
                $('#vehicleInfoUploadForm').ajaxSubmit({
                    url: path + '/vehicle/batchUploadVehicleInfo',
                    dataType: 'text',
                    success: resutlMsg,
                    error: errorMsg
                });
            }
        }

        function addSelectListener() {
            //找到列表中的数据行
            var rows = $("#vehicleInfoTable tr");
            $.each(rows, function (index, row) {
                if (index > 0) {
                    $(row).click(
                        function () {
                            selectedData = $('#vehicleInfoTable').dataTable().fnGetData(this);
                            $("#choicedUserName").html("<strong>" + selectedData.USER_NAME + "</strong> [" + selectedData.LOGIN_ID + "]");
                            $(row).addClass("info");
                            $(row).siblings().removeClass("info");
                        });
                }
            });
        }

        function downloadTemplate() {
            var  queryParams = 'templateName=车辆信息批量导入模版.xls';
            var url =path + "/upload/downloadTemplate?"+encodeURI(encodeURI(queryParams,"utf-8"),"utf-8");
            window.open(url);
        }
    </script>

</head>
<body>

<div class="bg-white item_title" style="padding-right: 0px;">
    车辆基础信息管理
    <button type="button" class="btn btn-md1 btn-primary title-btn" data-toggle="modal" data-target="#importInfo" title="点击批量导入车辆信息">导入车辆信息</button>

</div>
<div class="bg-white">
    <div class="form_search form_table container-fluid" style="margin-bottom: 1px">

        <div class="row form-group">
            <form id="vehicleInfoForm">
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>车牌号</label>
                        <input class="form-control" id="plateNumber" name="plateNumber">
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>所属单位</label>
                        <input class="form-control" id="useDept" name="useDept">
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-item wide2">
                        <label>联系人</label>
                        <input class="form-control" id="inCharge" name="inCharge">
                    </div>
                </div>
                <div class="col-xs-3 text-right">
                    <a id="findVehicleInfo" href="javascript:;" class="ml15 btn btn-primary btn-md1" >查询</a>
                    <button id="reSetBtn" type="button" class="btn btn-md1 btn-reset ml15" >重置</button>
                    <a id="addVehicleInfo" href="<%=request.getContextPath() %>/vehicle/toVehicleInfoForm?opt=A" class="ml15 btn btn-primary btn-md1" ><%--<i class="fa fa-plus"></i>&nbsp;&nbsp;--%>新增</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="bg-white pd_item">
    <table id="vehicleInfoTable" class="table table-striped table-bordered display nowrap self_table" align="center" style="width:100%">
    </table>
</div>

<!-- 模态框（Modal） -->
<div class="modal fade" id="importInfo" tabindex="-1" role="dialog" aria-labelledby="importInfoLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="importInfoLabel">导入车辆信息</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form class="form_area" method="post" enctype="multipart/form-data" id="vehicleInfoUploadForm">
                        <div class="col-md-8" style="padding-left: 8%;">
                            <input style="padding: 0;" type="file" name="file" id="vehicleInfoFile" class="form-control" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        </div>
                        <div class="col-md-4">
                            <button type="button" onclick="uploadVehicleInfo()" class="btn btn-md1 btn-primary" title="点击批量导入车辆信息">导&nbsp;&nbsp;入</button>
                            <button type="button" onclick="downloadTemplate()" class="ml5 btn btn-md1 btn-info" title="点击下载该模版文件">下载模版</button>
                        </div>
                        <div class="col-md-5 mt10" style="padding-left: 6%"><span class="label" id="resultMsg"></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-md1" data-dismiss="modal">关闭</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
</body>
</html>
