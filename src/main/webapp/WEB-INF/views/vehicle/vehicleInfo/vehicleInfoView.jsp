<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>车辆信息管理</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script>
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null
            });
        });
    </script>
</head>
<body>
<div class="bg-white item_title">
    <a href="${ctx}/vehicle/toVehicleInfoList" title="点击返回至列表">车辆信息管理</a>
    <i class="fa fa-angle-double-right"></i>
    车辆信息查看
</div>
<div class="bg-white pd_item pdt0 table-form wide3 container-fluid">
    <form class="form-horizontal" role="form" id="vehicleInfoForm">
        <input type="hidden" name="id" value="${vehicleInfo.id}" id="id">
        <input type="hidden" id="opt" name="opt" value="${opt}">

        <div class="form-body mb10 bg-blue">
            <div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>车牌号</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm validate[required]"  id = "plateNumber" name="plateNumber"  placeholder="" value="${vehicleInfo.plateNumber}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">车型</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="vehicleType" id="vehicleType" disabled>
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.VEHICLE_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq vehicleInfo.vehicleType}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">车牌类型</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="plateType" id="plateType" disabled>
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.PLATE_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq vehicleInfo.plateType}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">车牌颜色</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="plateColor" id="plateColor" disabled>
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.COLOR_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq vehicleInfo.plateColor}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">所属单位</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "useDept"  name="useDept" placeholder="" value="${vehicleInfo.deptName}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">联系人</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "inCharge"  name="inCharge" placeholder="" value="${vehicleInfo.inCharge}" type="text" readonly>
                </div>

            </div>

            <div class="row">
                <label class="col-md-2 control-label">联系人电话</label>
                <div class="col-md-3 pt7">
                    <input class="form-control input-sm"  id = "inChargeTel"  name="inChargeTel" placeholder="" value="${vehicleInfo.inChargeTel}" type="text" readonly>
                </div>

                <label class="col-md-2 control-label">品牌</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="vehicleBrand" id="vehicleBrand" disabled>
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.VEHICLE_BRAND}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq vehicleInfo.vehicleBrand}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">车辆颜色</label>
                <div class="col-md-3 pt7">
                    <select class="form-control input-sm select2" name="vehicleColor" id="vehicleColor" disabled>
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.COLOR_TYPE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq vehicleInfo.vehicleColor}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-md-2 control-label">车辆大小</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2" id = 'vehicleSize' name="vehicleSize" disabled>
                        <option value=""> 请选择</option>
                        <c:forEach items="${sessionScope.VEHICLE_SIZE}" var="item">
                            <c:choose>
                                <c:when test="${item.code eq vehicleInfo.vehicleSize}">
                                    <option value="${item.code}" selected>${item.dictValue}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.code}">${item.dictValue}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <%--<div class="row">
                <label class="col-md-2 control-label"><span class="alarmstar">*</span>是否监理旁站</label>
                <div class="col-md-3 pt7">
                    <select  class="form-control input-sm select2 validate[required]" id = 'isOnSiteSupervise' name="isOnSiteSupervise" disabled>
                        <option value="">请选择</option>
                        <option value="0" <c:if test="${vehicleInfo.isOnSiteSupervise eq 0}">selected</c:if>>否</option>
                        <option value="1" <c:if test="${vehicleInfo.isOnSiteSupervise eq 1}">selected</c:if>>是</option>
                    </select>
                </div>

                <label class="col-md-2 control-label"></label>
                <div class="col-md-3 pt7">

                </div>
            </div>--%>

        </div>

        <div class="form-actions mt10">
            <div class="row">
                <div class="text-center">
                    <a href="${ctx}/vehicle/toVehicleInfoList" title="返回列表" class="btn btn-md btn-default"><i
                            class="fa fa-reply"></i> 返回</a>

                </div>
            </div>
        </div>
    </form>

</div>

</body>
</html>
