<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>监理旁站列表</title>
    <jsp:include page="/common/init.jsp"></jsp:include>

    <script type="text/javascript">
        var path = '<%=request.getContextPath() %>';
        $(function () {
            $.fn.select2.defaults.set("theme", "bootstrap");
            $(".select2").select2({
                placeholder: "请选择",
                width: null,
                allowClear: true
            });

            vehicleOnSiteSuperviseTable = $("#vehicleOnSiteSuperviseTable").DataTable({
                iDisplayLength: 10,
                "sDom": "<'row-fluid'>t<'row-fluid bgpage'<'span4'il><'span7'<'pull-right'p>>>",
                iDisplayStart: 0,
                "bSort": false,
                sAjaxSource: path + "/vehicleOnSiteSupervise/getVehicleOnSiteSuperviseList",//获取列表数据url,
                sServerMethod: "GET",
                //table 表头
                aoColumns: [
                    {'mDataProp': 'siteName', 'sTitle': '站点',  'sClass': 'center'},
                    {'mDataProp': 'vehiclePlateNumber', 'sTitle': '车牌',  'sClass': 'center'},
                    {'mDataProp': 'workTypeName', 'sTitle': '岗位',  'sClass': 'center'},
                    {'mDataProp': 'presentFlag', 'sTitle': '是否在岗',  'sClass': 'center'},
                    {'mDataProp': 'createTime', 'sTitle': '时间',  'sClass': 'center'}
                ],
                //渲染某列格式
                aoColumnDefs: [
                    {   aTargets: [3],
                        mRender: function (data, type, row) {
                            var html = "" ;
                            if (data == 0)
                                html = '否';
                            else if (data == 1)
                                html = '是';
                            return html;
                        }
                    }
                ],
                //向后台提交url携带的参数
                fnServerParams: function (aoData) {
                    //获得form表单数据并封装在object中
                    var formIptArray = $("#vehicleOnSiteSuperviseForm input");
                    $.each(formIptArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });

                    var formSelectArray = $("#vehicleOnSiteSuperviseForm select");
                    $.each(formSelectArray,function(i , obj){
                        var name = $(obj).attr("name");
                        var val = $(obj).val();
                        aoData.push({
                            "name" : name,
                            "value" : val
                        });
                    });
                }

            });
            $("#findVehicleOnSiteSupervise").click(function () {
                vehicleOnSiteSuperviseTable.ajax.reload();
            });
            $('#reSetBtn').click(function () {
                resetSearchForm('vehicleOnSiteSuperviseForm')
            });

            $('#beginDate').datetimepicker({
                format: 'yyyy-mm-dd', autoclose: true, minView: 0, minuteStep:1,
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });

            $('#endDate').datetimepicker({
                format: 'yyyy-mm-dd', autoclose: true, minView: 0, minuteStep:1,
                todayHighlight: 1,
                minView: "month",
                startView: 2,
                autoclose: 1,
                language: 'zh-CN'
            });


        });


    </script>

</head>
<body>

<c:if test="${from ne 'home'}">
    <div class="bg-white item_title">
        监理旁站
    </div>
</c:if>
<div class="bg-white">
    <form id="vehicleOnSiteSuperviseForm" class="form_search form_table container-fluid">
        <div class="row form-group">
            <div class="col-xs-4">
                <div class="form-item wide2">
                    <label>站点</label>
                    <select class="form-control input-sm select2" name="siteId" id="siteId">
                        <option value="">请选择</option>
                        <c:forEach items="${sessionScope.siteList}" var="item">
                            <c:choose>
                                <c:when test="${item.id eq siteId}">
                                    <option value="${item.id}" selected>${item.name}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${item.id}">${item.name}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="form-item wide2">
                    <label>日期</label>
                    <div class="input-group input-group-sm date-picker input-daterange" data-date-format="yyyy-mm-dd">
                        <input style="background-color: #fff;" class="form-control" id="beginDate" name="beginDate" type="text" readonly>
                        <span class="input-group-addon"> 至 </span>
                        <input style="background-color: #fff;" class="form-control" id="endDate" name="endDate" type="text" readonly>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 text-right">
                <a href="javascript:;" class="ml5 btn btn-primary btn-md1" id="findVehicleOnSiteSupervise">查询</a>
                <button type="button" class="btn btn-md1 btn-reset ml5" id="reSetBtn">重置</button>
            </div>
        </div>
    </form>
</div>
<div class="bg-white pd_item">
    <table id="vehicleOnSiteSuperviseTable" class="table table-striped table-bordered display nowrap self_table" align="center">
    </table>
</div>

</body>
</html>
