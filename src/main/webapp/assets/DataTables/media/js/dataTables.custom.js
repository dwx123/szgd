/**
 * 扩展jquery datatable差距
 */
/* Set the defaults for DataTables initialisation */
$.extend( true, $.fn.dataTable.defaults, {
    //"sDom" : "<'row-fluid'f>t<'row-fluid'<'span4'i><'span2'r><'span8'<'pull-right'p>>>",
    // "sDom" : "<'row-fluid'<'pull-left'f>>t<'row-fluid'<'span4'i><'span8'<'pull-right'p>>>",
	"pagingType": "full_numbers",
    "bDestroy": true,
    "bFilter": true,      //快速查询，自动检索向后台提交的参数，参数名为sSearch
    "bProcessing": true,
    "bPaginate": true,
    "bAutoWidth": true,
    "bServerSide": true,
    "bSort":false,
    "ordering":false,   //允许列排序
    "lengthChange" : false,	//是否允许用户自定义表格每页显示数量
    "bLengthChange":false,
    // "sScrollX": "100%",
    // "sScrollXInner": "100%",
    // "bScrollCollapse":true,
    "sServerMethod": "POST",
    "bStateSave":true,
    "iDisplayLength": 10,     //每页显示数量
    "iDisplayStart": 0,       //第几页开始
	"oLanguage": {
        "sSearch" : "检索：",  //过滤label
		"sLengthMenu": "_MENU_ 条/页",
        "sProcessing": "加载中...",
        "sZeroRecords":'没有匹配的数据',
        "sEmptyTable": "没有匹配的数据",
        "sInfo": '显示 _START_ 到 _END_ 条记录  共 _TOTAL_ 条记录',
        "sInfoEmpty" : "暂无数据",
        "oPaginate" : {
		    "sFirst" : "首页",
            "sPrevious" : "上一页",
            "sNext" : "下一页",
            "sLast" : "尾页"
        }
	}
} );


/* Default class modification */
$.extend( $.fn.dataTableExt.oStdClasses, {
	"sWrapper": "dataTables_wrapper form-inline"
} );


/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
	return {
		"iStart":         oSettings._iDisplayStart,
		"iEnd":           oSettings.fnDisplayEnd(),
		"iLength":        oSettings._iDisplayLength,
		"iTotal":         oSettings.fnRecordsTotal(),
		"iFilteredTotal": oSettings.fnRecordsDisplay(),
		"iPage":          oSettings._iDisplayLength === -1 ?
			0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
		"iTotalPages":    oSettings._iDisplayLength === -1 ?
			0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
	};
};

/* Bootstrap style pagination control 控制分页并显示*/
$.extend( $.fn.dataTableExt.oPagination, {
    "bootstrap": {
        "fnInit": function( oSettings, nPaging, fnDraw ) {
            var oLang = oSettings.oLanguage.oPaginate;
            // 点击事件处理：首页、前一页、后一页、尾页
            var fnClickHandler = function ( e ) {
                e.preventDefault();
                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                    fnDraw( oSettings );
                }
            };
            $(nPaging).append(
                '<ul class="pagination">'+
                	'<li class="first disabled"><a href="#">'+oLang.sFirst+'</a></li>'+
                    '<li class="prev disabled"><a href="#">'+oLang.sPrevious+'</a></li>'+
                    '<li class="next disabled"><a href="#">'+oLang.sNext+'</a></li>'+
                    '<li class="last disabled"><a href="#">'+oLang.sLast+'</a></li>'+
                '</ul>'
            );
            var els = $('a', nPaging);
            $(els[0]).bind( 'click.DT', { action: "first" }, fnClickHandler );
            $(els[1]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
            $(els[2]).bind( 'click.DT', { action: "next" }, fnClickHandler );
            $(els[3]).bind( 'click.DT', { action: "last" }, fnClickHandler );
        },
        "fnUpdate": function ( oSettings, fnDraw ) {
            var iListLength = 5;  // 默认每页显示的记录数
            var oPaging = oSettings.oInstance.fnPagingInfo();  // 分页参数信息
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
            if ( oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            }else if ( oPaging.iPage <= iHalf ) {
                iStart = 1;
                iEnd = iListLength;
            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }
            for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                // Remove the middle elements
            	 $('li:gt(1)', an[i]).filter(':lt(-2)').remove();
                // Add the new list items and their event handlers
                for ( j=iStart ; j<=iEnd ; j++ ) {
                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                        .insertBefore( $('li:eq(-2)', an[i])[0] )//
                        .bind('click', function (e) {
                            e.preventDefault();
                            oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                            fnDraw( oSettings );
                        } );
                }
                // Add / remove disabled classes from the static elements
                if ( oPaging.iPage === 0 ) {
                	 $('li:lt(2)', an[i]).addClass('disabled'); 
                } else {
                	$('li:lt(2)', an[i]).removeClass('disabled'); 
                }
                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                	$('li:gt(-3)', an[i]).addClass('disabled');
                } else {
                	$('li:gt(-3)', an[i]).removeClass('disabled');
                }
            }
        }
    }
} );

