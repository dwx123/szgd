/**
 * 操作错误的通知提示
 * @param message 通知的文本内容
 */
function notyError(message){
    var settings = {
        life: 5000,
        theme: 'ruby',
        heading:"操作提示",
        sticky: false,
        verticalEdge: 'right',
        horizontalEdge: 'top',
        zindex: 11500
    };
    $.notific8(message,settings);
}

/**
 * 警告的通知提示
 * @param message 通知的文本内容
 */
function notyWarning(message){
    var settings = {
        life: 5000,
        theme: 'tangerine',
        heading:"操作提示",
        sticky: false,
        verticalEdge: 'right',
        horizontalEdge: 'top',
        zindex: 11500
    };
    $.notific8(message,settings);
}

/**
 * 操作成功的通知
 * @param message 通知的文本内容
 */
function notySuccess(message){
    var settings = {
        life: 5000,
        theme: 'lime',
        heading:"操作提示",
        sticky: false,
        verticalEdge: 'right',
        horizontalEdge: 'top',
        zindex: 11500
    };
    $.notific8(message,settings);
}

/**
 * 普通提示的通知
 * @param message 通知的文本内容
 */
function notyInfo(message){
    var settings = {
        life: 5000,
        theme: 'teal',
        heading:"操作提示",
        sticky: false,
        verticalEdge: 'right',
        horizontalEdge: 'top',
        zindex: 11500
    };
    $.notific8(message,settings);
}

/**
 * 弹出窗口
 * @param id 窗口所使用的DIV，该DIV在body即可，可参考： <div id="_changePwdWin" class="modal fade"></div>
 * @param url 携带上下文路径的url，传值可参考 ${ctx}/user/toChangePwd
 * @param params url需携带的参数，json格式，可参考{"id" : "123"}
 */
function openWin(id,url,params){

    $(id).load(url,params,function(data){
        $(this).fadeIn('fast');
    });
    $(id).modal();
}

function changeSrc(src) {
    $('#mainFrame',parent.document).attr('src',src)
}
function changeSrcAlink() {
    $('body').on('click', 'a', function () {
        var $a = $(this)
        var $href = $a.attr('href')
        if ($a.attr('href') && $a.attr('href').indexOf('szgd') > -1) {
            changeSrc($href)
            return false
        }
    })
}

//金额输入校验
function checkFloatNum(str) {
    var re = /^(([1-9][0-9]*\.[0-9][0-9]*)|([0]\.[0-9][0-9]*)|([1-9][0-9]*)|([0]{1}))$/;
    if(!re.test(str))
        return false;
    else
        return true;
}
function checkIntNum(str) {
    var re = /^\d+$/;
    if(!re.test(str))
        return false;
    else
        return true;
}


function resetSearchForm(id) {
    // if (selArr && selArr.length > 0) {
    //     selArr.forEach(function (item) {
    //         $('#' +item).select2('val', 'all')
    //     })
    // }
    if($('#' + id).find('select.select2')) {
        $('#' + id).find('select.select2').each(function (index, item) {
            $(this).select2('val', 'all')
        })
    }
    $('#' + id)[0].reset()
}

/**
 * 比较两个日期的大小
 * @param d1,d2 日期
 */
function CompareDate(d1,d2)
{
    return ((new Date(d1.replace(/-/g,"\/"))) > (new Date(d2.replace(/-/g,"\/"))));
}