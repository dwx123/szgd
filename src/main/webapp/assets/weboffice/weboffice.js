var WebOfficeObj = null;

/**
 * 隐藏菜单选项
 */
function hideMenu() {
    WebOfficeObj.SetToolBarButton2("Menu Bar", 1, 1);	//隐藏文件菜单
    //WebOfficeObj.SetToolBarButton2("Standard",1,8);	//隐藏常用工具栏（保存/打印等）
    WebOfficeObj.SetToolBarButton2("Standard", 3, 1);
    WebOfficeObj.setKeyCtrl(595, -1, 0);

    /*
         vNew  = 0x01, 			 //新建
        vOpen = 0x02,  			 //打开
        vSaveAs = 0x04, 		 //保存
        vPrint = 0x10,          //打印
        vPrintView = 0x20,      //打印预览
        vReturn = 0x1000,       //全屏
        vFullScrean = 0x2000    //返回

    */
    //设置要隐藏的按钮
    WebOfficeObj.HideMenuItem(0x01);
    WebOfficeObj.HideMenuItem(0x02); //Hide it
    WebOfficeObj.HideMenuItem(0x04); //Hide it
    //创建自定义的保存按钮，便于将文件保存至服务器
    //WebOfficeObj.SetCustomToolBtn(0,"保存文档");
}

function notifyToolBarClick(iIndex,officeLoadUrl){
    if(iIndex>=32776){
        saveToServer(officeLoadUrl)
    }
}

/**
 * 打开office文档
 * @param filePath 文档路径
 * @param fileType 文档类型 "doc","docx"
 */
function openRemoteTemp(filePath, fileType) {
    WebOfficeObj.loadOriginalFile(filePath, fileType);
}


function openTempleteFile(fileType, fileClass) {
    var path = "";
    path = "http://" + window.location.host + basePath + "/sys/weboffice!getTempFile.ec?wz="
        + fileClass + "&wjlx=" + fileType;
    openRemoteTemp(path, "doc");
}

/**
 * @param uploadPath 上传路径
 * 保存文档到服务器，成功后返回FILEID，失败返回"false",其他返回"ok"
 */
function saveToServer(uploadPath) {
    var webObj = WebOfficeObj;
    var fileID = "";
    //if(webObj.IsSaved()==0){//已被修改
    webObj.HttpInit();
    webObj.HttpAddPostCurrFile("docContent", "");
    fileID = new Date().getTime();
    webObj.HttpAddPostString("id", fileID);  //传参数
    var returnVal = webObj.HttpPost(uploadPath);
    if ("success" == returnVal) {
        return fileID;
    } else {
        alert("正文保存失败！");
        return "false";
    }
    //}else{
    //	return "ok";
    //}
}

/**
 * 初始化WebOffice控件
 * @param webofficeName webOffice控件ID
 * @param filePath 文档路径
 * @param fileType 文档类型 "doc","docx"
 */
function initWebOffice(webofficeName, filePath, fileType) {
    try {
        WebOfficeObj = document.getElementById(webofficeName);
        hideMenu();
        openRemoteTemp(filePath, fileType);//打开默认文档
    } catch (e) {
        alert("发送错误："+e);
    }
}

/**
 * 给书签赋值
 * @param strFieldName --书签名称
 * @param strValue --书签值
 */
function SetFieldValue(strFieldName, strValue) {
    WebOfficeObj.SetFieldValue(strFieldName, strValue, "");
}

function IsOpened() {
    var value = WebOfficeObj.IsOpened();
    return value == 0 ? false : true;
}