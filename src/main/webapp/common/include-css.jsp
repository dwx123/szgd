<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%--<link href="${ctx }/js/common/plugins/jui/themes/${themeName }/jquery-ui-${themeVersion }.custom.css" type="text/css" rel="stylesheet" />
<link href="${ctx }/js/common/plugins/jui/extends/timepicker/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />--%>
<link href="${ctx }/js/common/plugins/qtip/jquery.qtip.min.css" type="text/css" rel="stylesheet" />
<link href="${ctx }/css/style.css" type="text/css" rel="stylesheet"/>
<%--<script type="text/javascript" src="${ctx }/js/jquery.min.js"></script>  --%>
<%--<script src="${ctx }/js/common/jquery-1.8.3.js" type="text/javascript"></script>--%>
<%--<script src="${ctx }/js/common/plugins/jui/jquery-ui-${themeVersion}.min.js" type="text/javascript"></script>--%>
<%--<script src="${ctx }/js/common/plugins/jui/extends/timepicker/jquery-ui-timepicker-addon.js" type="text/javascript"></script>--%>
<script src="${ctx }/js/common/plugins/jui/extends/i18n/jquery-ui-date_time-picker-zh-CN.js" type="text/javascript"></script>
<script src="${ctx }/js/common/plugins/qtip/jquery.qtip.pack.js" type="text/javascript"></script>
<script src="${ctx }/js/common/plugins/html/jquery.outerhtml.js" type="text/javascript"></script>