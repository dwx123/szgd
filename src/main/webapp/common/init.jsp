<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<script src="${ctx}/assets/js/jquery.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx }/js/common/dataTable/jquery.serialize-object.min.js" type="text/javascript" charset="UTF-8"></script>

<link type="text/css" href="${ctx}/assets/bootstrap/css/bootstrap.css" rel="Stylesheet">
<link href="${ctx}/assets/jquery-notific8/jquery.notific8.min.css"  rel="stylesheet" type="text/css" />

<link type="text/css" href="${ctx}/assets/bootstrap-extension/select2/css/select2.css" rel="Stylesheet">
<link type="text/css" href="${ctx}/assets/bootstrap-extension/select2/css/select2-bootstrap.min.css" rel="Stylesheet">

<%--<link href="${ctx}/assets/zTree/css/zTreeStyle/zTreeStyle.css" type="text/css" rel="stylesheet" />--%>
<link type="text/css" href="${ctx}/assets/bootstrap-extension/font-awesome/css/font-awesome.min.css"
      rel="Stylesheet">
<link type="text/css"
      href="${ctx}/assets/bootstrap-extension/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css"
      rel="Stylesheet">
<link type="text/css" href="${ctx}/assets/DataTables/media/css/jquery.dataTables.min.css" rel="Stylesheet">

<link type="text/css" href="${ctx}/css/dataTable/buttons.dataTables.css" rel="Stylesheet">
<link rel="stylesheet" type="text/css" href="${ctx}/assets/jquery.editable-select/jquery.editable-select.min.css" />

<link type="text/css" href="${ctx}/assets/css/custom.css" rel="Stylesheet">
<link type="text/css" href="${ctx}/assets/css/customelse.css" rel="Stylesheet">
<link rel="stylesheet" type="text/css" href="${ctx}/assets/validationEngine/css/validationEngine.jquery.css">

<script type="text/javascript" src="${ctx }/js/jquery.form.js" charset="UTF-8"></script>
<script src="${ctx}/assets/DataTables/media/js/jquery.dataTables.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx }/js/common/dataTable/datatables.bootstrap.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx}/assets/bootstrap/js/bootstrap.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx }/js/common/dataTable/dataTables.buttons.js" type="text/javascript" charset="UTF-8"></script>

<script src="${ctx }/js/common/dataTable/formatDate.js" type="text/javascript" charset="UTF-8"></script>


<%--<script src="${ctx }/js/common/dataTable/jszip.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx }/js/common/dataTable/pdfmake.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx }/js/common/dataTable/vfs_fonts.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx }/js/common/dataTable/buttons.flash.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx }/js/common/dataTable/buttons.html5.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx }/js/common/dataTable/buttons.print.js" type="text/javascript" charset="UTF-8"></script>--%>

<script src='${ctx}/assets/DataTables/media/js/dataTables.fixedColumns.min.js' charset="UTF-8"></script>
<script src='${ctx}/assets/DataTables/media/js/dataTables.custom.js'type="text/javascript" charset="UTF-8"></script>
<%--<script src="${ctx}/assets/zTree/js/jquery.ztree.all.js" type="text/javascript" charset="UTF-8"></script>--%>

<script src="${ctx}/assets/bootstrap-extension/bootstrap-datepicker/moment.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx}/assets/bootstrap-extension/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"
        type="text/javascript" charset="UTF-8"></script>
<script src="${ctx}/assets/bootstrap-extension/bootstrap-datepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"
        type="text/javascript" charset="UTF-8"></script>

<script src="${ctx}/assets/jquery-notific8/jquery.notific8.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx}/assets/bootstrap-extension/select2/js/select2.full.js" type="text/javascript" charset="UTF-8"></script>

<script type="text/javascript" src="${ctx}/assets/jquery.editable-select/jquery.editable-select.min.js" charset="UTF-8"></script>

<script type="text/javascript"
        src="${ pageContext.request.contextPath }/assets/validationEngine/js/jquery.validationEngine.js" charset="UTF-8"></script>
<script type="text/javascript"
        src="${ pageContext.request.contextPath }/assets/validationEngine/js/jquery.validationEngine-zh.js" charset="UTF-8"></script>
<script type="text/javascript" src="${ pageContext.request.contextPath }/assets/js/ajaxfileupload.js" charset="UTF-8"></script>

<!--[if lt IE 9]>
<link type="text/css" href="${ctx}/assets/css/iecustom.css" rel="Stylesheet">
<script src="${ctx}/assets/js/respond.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${ctx}/assets/js/html5shiv.js" type="text/javascript" charset="UTF-8"></script>
<![endif]-->

<script src="${ctx}/assets/js/custom.js" type="text/javascript" charset="UTF-8"></script>
<%--<script src="${ctx }/js/common/common.js" type="text/javascript"></script>--%>
<!--echarts-->
<%--<script src="${ctx}/js/echarts.js" type="text/javascript" charset="UTF-8"></script>--%>

<script type="text/javascript" src="${ctx}/assets/loadmask/jquery.loadmask.js" ></script>
<link rel="stylesheet" href="${ctx}/assets/loadmask/jquery.loadmask.css">
<script>
    function gotoMenu(index,src) {
        var $framesrc = $(window.parent.document).find("#mainFrame").attr('src');
        if (src == $framesrc) {
            return;
        }else {
            $('.subMenu a').removeClass('active')
            $('#navMenusBanner li').removeClass('active')
            $('#navMenusBanner li').eq(3).addClass('active').find('.subMenu a').eq(index).addClass('active')
            $(window.parent.document).find("#mainFrame").attr('src', src);
        }
    }

    var path = '<%=request.getContextPath() %>';
    $(function () {
        $.fn.select2.defaults.set("theme", "bootstrap");
        $(".select2").select2({
            placeholder: "请选择",
            width: null,
            allowClear: true
        });

        //清除的代码
        $(".glyphicon-remove").click(function(){
            $($($(this).parent()).prev()).val("");
        });

        changeSrcAlink()
        $('body').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                $('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } )
    })
    
    function disabledAllBtn(disabled) {
        $("button").each(function () {
            if (disabled ==true)
                $(this).attr("disabled","disabled");
            else
                $(this).removeAttr("disabled");
        })
    }

    function isEmpty(obj) {
        if(typeof obj == "undefined" || obj == null || obj == "")
        {
            return true;
        }else {
            return false;
        }
    }

    function serializeObject(form) {
        var o = {};
        var a = $(form).serializeArray();
        $.each(a,function () {
            if(o[this.name]){
                if(! o[this.name].push){
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            }else
            {
                o[this.name] =  this.value || '';
            }
        })
        return o;
    }
    $(document).ajaxComplete(function(event,obj,settings){
    	if(obj.responseText == 'timeout'){
    		window.location.replace(path+"/login");
    	}
    });
</script>
