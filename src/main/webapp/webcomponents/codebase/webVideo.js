// 初始化插件
function WebVideo() {
    this.status = -10;
    this.g_iWndIndex = 0
    this.iWndowType =3
    this.divPlugin = "divPlugin"
    this.szDeviceIdentify = ''
    this.rtspPort = ''
    this.deviceport = ''
    this.channels = []
    this.ip = '10.10.1.1'
    this.port = '80'
    this.username = 'admin'
    this.password = '123456'
    this.path = ''
    this.picName = ''
    // 登录
    this.clickLogin = function () {
        var self = this
        if ("" == self.ip || "" == self.port) {
            return;
        }
        self.szDeviceIdentify = self.ip + "_" + self.port;
        WebVideoCtrl.I_Login(self.ip, 1, self.port, self.username, self.password, {
            success: function (xmlDoc) {
                setTimeout(function () {
                    if(self.channels.length==0)
                        self.getChannelInfo();
                    if(self.rtspPort.length == 0)
                        self.getDevicePort();
                }, 10);
                setTimeout(function() {
                    self.clickStartRealPlay()
                }, 500)
            },
            error: function (status, xmlDoc) {

            }
        });
    }
    this.init = function() {
        var self = this;
        // 检查插件是否已经安装过
        var iRet = WebVideoCtrl.I_CheckPluginInstall();
        if (-1 == iRet) {
            alert("您还未安装过插件，双击开发包目录里的WebComponentsKit.exe安装！");
            return;
        }
        // 初始化插件参数及插入插件
        WebVideoCtrl.I_InitPlugin('100%', '100%', {
            bWndFull: true,
            szColorProperty:"plugin-background:ffffff;sub-background:ffffff;sub-border:647EE1;sub-border-select:8A6EE9",
            iPackageType: 2,
            iWndowType: self.iWndowType,
            cbSelWnd: function (xmlDoc) {
                self.g_iWndIndex = parseInt($(xmlDoc).find("SelectWnd").eq(0).text(), 10);
                var szInfo = "当前选择的窗口编号：" + self.g_iWndIndex;
                //showCBInfo(szInfo);
            },
            cbDoubleClickWnd: function (iWndIndex, bFullScreen) {
                var szInfo = "当前放大的窗口编号：" + iWndIndex;
                if (!bFullScreen) {
                    szInfo = "当前还原的窗口编号：" + iWndIndex;
                }

            },
            cbInitPluginComplete: function () {
                WebVideoCtrl.I_InsertOBJECTPlugin(self.divPlugin);
            }
        });
    }
    // 退出
    this.clickLogout = function() {
        var self = this
        if (null == self.szDeviceIdentify) {
            return;
        }
        var iRet = WebVideoCtrl.I_Logout(self.szDeviceIdentify);
        if (0 == iRet) {
            self.getChannelInfo();
            self.getDevicePort();
        }
    }
    // 获取通道
    this.getChannelInfo = function() {
        var self = this
        self.channels = []
        if (null == self.szDeviceIdentify) {
            return;
        }

        // 模拟通道
        WebVideoCtrl.I_GetAnalogChannelInfo(self.szDeviceIdentify, {
            async: false,
            success: function (xmlDoc) {
                var oChannels = $(xmlDoc).find("VideoInputChannel");
                $.each(oChannels, function (i) {
                    var id = $(this).find("id").eq(0).text(),
                        name = $(this).find("name").eq(0).text();
                    if ("" == name) {
                        name = "Camera " + (i < 9 ? "0" + (i + 1) : (i + 1));
                    }
                    self.channels.push({
                        id: id,
                        name: name
                    })
                });
            },
            error: function (status, xmlDoc) {
            }
        });
    }
    // 获取端口
    this.getDevicePort = function() {
        var self = this
        if (null == self.szDeviceIdentify) {
            return;
        }
        var oPort = WebVideoCtrl.I_GetDevicePort(self.szDeviceIdentify);
        if (oPort != null) {
            self.deviceport = oPort.iDevicePort;
            self.rtspPort = oPort.iRtspPort;
        }
    }

    // 开始预览
    this.clickStartRealPlay = function() {
        var self = this
        var oWndInfo = WebVideoCtrl.I_GetWindowStatus(self.g_iWndIndex),
            iChannelID = self.channels[0].id
        if (null == self.szDeviceIdentify) {
            return;
        }

        var startRealPlay = function () {
            WebVideoCtrl.I_StartRealPlay(self.szDeviceIdentify, {
                iRtspPort: self.rtspPort,
                iWndIndex:self.g_iWndIndex,
                iStreamType: '1',
                iChannelID: iChannelID,
                bZeroChannel: false,
                success: function () {
                    self.status = 1;
                },
                error: function (status, xmlDoc) {
                    if (403 === status) {
                        //szInfo = "设备不支持Websocket取流！";
                        self.status = 0;
                    } else {
                        //szInfo = "开始预览失败！";
                        self.status = -1
                    }
                }
            });
        };

        if (oWndInfo != null) {// 已经在播放了，先停止
            WebVideoCtrl.I_Stop({
                success: function () {
                    startRealPlay();
                }
            });
        } else {
            startRealPlay();
        }
    }
    // 停止预览
    this.clickStopRealPlay = function() {
        var self = this
        var oWndInfo = WebVideoCtrl.I_GetWindowStatus(self.g_iWndIndex)
        if (oWndInfo != null) {
            WebVideoCtrl.I_Stop(self.g_iWndIndex,{
                success: function () {
                },
                error: function () {
                }
            });
        }
    }

    // 抓图
    this.clickCapturePic = function() {
        var self = this

        var oWndInfo = WebVideoCtrl.I_GetWindowStatus(self.g_iWndIndex),
            szInfo = "";

        if (oWndInfo != null) {
            var xmlDoc = WebVideoCtrl.I_GetLocalCfg();
            var szCaptureFileFormat = "0";
            if (xmlDoc != null) {
                szCaptureFileFormat = $(xmlDoc).find("CaptureFileFormat").eq(0).text();
            }

            var szChannelID = self.channels[0].id;
            //var szPicName = oWndInfo.szDeviceIdentify + "_" + szChannelID + "_" + new Date().getTime();
            var szPicName = self.szDeviceIdentify + "_" + szChannelID + "_" + new Date().getTime();

            szPicName += ("0" === szCaptureFileFormat) ? ".jpg": ".bmp";

            var iRet = WebVideoCtrl.I_CapturePic(szPicName, {
                bDateDir: true  //是否生成日期文件
            });
            if (0 == iRet) {
                //szInfo = "抓图成功！";
                self.path = $(xmlDoc).find("CapturePath").eq(0).text();
                self.picName = szPicName;
            } else {
                szInfo = "抓图失败！";
            }
            //alert(szInfo);
        }
    }
}
