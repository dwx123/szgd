package szgd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.szgd.util.OutlayNo;

public class TestExcel {

	public static void main(String[] args) {

		System.out.println(OutlayNo.getNo());
	}

	/** 
	 *  
	 * @Description Map中value对应Excel列名，key对应上面解析出来Map数据的key 
	 * @return 
	 * Map<String,String> 
	 * @exception: 
	 * @author: fengjk 
	 * @time:2017年4月14日 下午2:14:02 
	 */  
	public Map<String,String> getExcelColumMap(){  
	    Map<String,String> columMap = new LinkedHashMap<String,String>();  
	    columMap.put("name", "地区");  
	    columMap.put("longilatitude", "经纬度");  
	      
	    return columMap;  
	} 

}
